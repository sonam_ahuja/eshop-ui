## E-shop UI

### Development

#### Run development server
- npm run start

#### Run Test Cases
```bash
yarn run lint                   # Find problematic patterns in code
yarn run check                  # Check source code for type errors
yarn run test                   # Run unit tests once
yarn run test-watch             # Run unit tests in watch mode
```

### Nomenclature
- Component should always be default exported

## Naming convention
- Component folder name and component name should start with capital letter (PascalCase)


### Production

#### Create Production Build
- npm run build

#### Run Production Build
- npm run serve
