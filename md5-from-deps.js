const crypto = require('crypto');
const pjson = require('./package.json');

const depsStr = JSON.stringify(pjson.dependencies);
const devDepsStr = JSON.stringify(pjson.devDependencies);
const allDepsStr = depsStr + devDepsStr;

console.log(
  crypto
    .createHash('md5')
    .update(allDepsStr)
    .digest('hex')
);
