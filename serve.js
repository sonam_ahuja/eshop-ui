const express = require('express');
const path = require('path');

const port = 3001;

const app = express();
app.use(express.static(path.resolve(__dirname, 'build/public')));
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  next();
});
app.listen(port, function() {
  console.log('Server is running on ' + port + ' port');
});
