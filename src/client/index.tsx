if (!__DEV__) {
  __webpack_public_path__ = `${
    (window as IWindow).__SECRETS__.ESHOP_CDN_BASE_URL
  }/assets/`;
}
import { Provider } from 'react-redux';
import { ThemeProvider } from 'dt-components';
import deepForceUpdate from 'react-deep-force-update';
import Loadable from 'react-loadable';
import { History } from 'history';
import ReactDOM from 'react-dom';
import React, { ReactNode } from 'react';
import { Router } from 'react-router-dom';
import rootSaga from '@common/store/sagas';
import store from '@common/store';
import { setPublickKeyForEncryption } from '@common/routes/authentication/utils/encryption';
import appConstants, { ONEAPP_PARAMS } from '@common/constants/appConstants';
import APP_TYPE from '@common/constants/appType';
import { logError } from '@utils/index';
import smoothscroll from 'smoothscroll-polyfill';
import { trackClickEvent } from '@events/listener';
import { initiateConnection } from '@common/mqt';
import { addWebSiteManifest } from '@common/utils';
import { isBrowser } from '@src/common/utils';
import CommonActions from '@src/common/store/actions/common';
import { initGoogleConnect } from '@authentication/utils/googleConnect';
import { createUniqueIdentifier } from '@src/common/utils/uniqueIdentifier';
import OneApp from '@common/OneApp';
import App from '@common/App';

smoothscroll.polyfill();

import history from './history';

if (smoothscroll) {
  smoothscroll.polyfill();
}

declare const __DEV__: boolean;

let bootstrapOneApp = false;
const search = (history as History).location.search;
const searchParam = new URLSearchParams(search);
if (
  String((searchParam.get(ONEAPP_PARAMS.CHANNEL) || '').toLowerCase()) ===
  APP_TYPE.ONEAPP
) {
  bootstrapOneApp = true;
}

/** USED ON PROLONGATION */
export interface IWindow extends Window {
  __INITIAL_STATE__: object;
  __SECRETS__: {
    ESHOP_BASE_URL: string;
    COUNTRY_CODE: string;
    CONTENT_LANGUAGE: string;
    LANGUAGE_CODE: string;
    ENV: string;
    CHANNEL: string;
    RUM_SERVER_URL: string;
    RUM_SERVER_NAME: string;
    RUM_VERSION: string;
    S3_IMAGE_URL: string;
    WEB_VIEW_CMS_BASE_URL: string;
    WEB_VIEW_TRANSLATION_API_KEY: string;
    WEB_VIEW_CONFIGURATION_API_KEY: string;
    ONEAPP_BASE_URL: string;
    CLIENT_ID: string;
    MOENGAGE_BASE_URL: string;
    SECRET_KEY: string;
    WEB_VIEW_MARKETING_URL: string;
    MEGA_MENU_CMS_BASE_URL: string;
    MEGA_MENU_HEADER_KEY: string;
    MEGA_MENU_FOOTER_KEY: string;
    ESHOP_CDN_BASE_URL: string;
    ESHOP_CDN_STATIC_URL: string;
  };
  dataLayer: object[];
  binkies: IBinkies;
  BinkiesIntegrationId: number;
  gapi: {
    auth2: {
      init(params: {
        client_id: string;
        scope: string;
        cookie_policy: string;
      }): void;
      getAuthInstance(): {
        isSignedIn(): boolean;
        grantOfflineAccess(): Promise<{ error: string } | { code: string }>;
      };
    };
    load(clientType: string, callback: () => void): void;
  };
  OneApp: {
    closeOneAppWebview(): void;
    logEventInOneApp(eventName: string, jsonMap: string): void;
  };
  BinkiesReportLoadingProgress(): void;
  ga(): void;
}

interface IBinkies {
  onPageStyleSheetUrl?: string;
  inModalStyleSheetUrl?: string;
  contentIdentifier?: string | null;
  onLoadingProgress(contentIdentifier: number, progress: string): void;
  show(): void;
  hide(): void;
}

const container = document.getElementById('app');
let currentLocation = (history as History).location;
let appInstance: ReactNode | void;

appConstants.ESHOP_BASE_URL = (window as IWindow).__SECRETS__.ESHOP_BASE_URL;
appConstants.LANGUAGE_CODE = (window as IWindow).__SECRETS__.LANGUAGE_CODE;
appConstants.COUNTRY_CODE = (window as IWindow).__SECRETS__.COUNTRY_CODE;
appConstants.ENVIRONMENT = (window as IWindow).__SECRETS__.ENV;
appConstants.CONTENT_LANGUAGE = (window as IWindow).__SECRETS__.CONTENT_LANGUAGE;
appConstants.CHANNEL = (window as IWindow).__SECRETS__.CHANNEL;
appConstants.RUM_SERVER_URL = (window as IWindow).__SECRETS__.RUM_SERVER_URL;
appConstants.RUM_SERVER_NAME = (window as IWindow).__SECRETS__.RUM_SERVER_NAME;
appConstants.RUM_VERSION = (window as IWindow).__SECRETS__.RUM_VERSION;
appConstants.S3_IMAGE_URL = (window as IWindow).__SECRETS__.S3_IMAGE_URL;
appConstants.WEB_VIEW_TRANSLATION_API_KEY = (window as IWindow).__SECRETS__.WEB_VIEW_TRANSLATION_API_KEY;
appConstants.WEB_VIEW_CONFIGURATION_API_KEY = (window as IWindow).__SECRETS__.WEB_VIEW_CONFIGURATION_API_KEY;
appConstants.ONEAPP_BASE_URL = (window as IWindow).__SECRETS__.ONEAPP_BASE_URL;
appConstants.MOENGAGE_BASE_URL = (window as IWindow).__SECRETS__.MOENGAGE_BASE_URL;
appConstants.CLIENT_ID = (window as IWindow).__SECRETS__.CLIENT_ID;
appConstants.SECRET_KEY = (window as IWindow).__SECRETS__.SECRET_KEY;
appConstants.MEGA_MENU_CMS_BASE_URL = (window as IWindow).__SECRETS__.MEGA_MENU_CMS_BASE_URL;
appConstants.MEGA_MENU_HEADER_KEY = (window as IWindow).__SECRETS__.MEGA_MENU_HEADER_KEY;
appConstants.MEGA_MENU_FOOTER_KEY = (window as IWindow).__SECRETS__.MEGA_MENU_FOOTER_KEY;
appConstants.WEB_VIEW_MARKETING_URL = (window as IWindow).__SECRETS__.WEB_VIEW_MARKETING_URL;
appConstants.ESHOP_CDN_STATIC_URL = (window as IWindow).__SECRETS__.ESHOP_CDN_STATIC_URL;
appConstants.ESHOP_CDN_BASE_URL = (window as IWindow).__SECRETS__.ESHOP_CDN_BASE_URL;

trackClickEvent();

store.runSaga(rootSaga);

try {
  if (isBrowser && !bootstrapOneApp) {
    addWebSiteManifest();
    createUniqueIdentifier();
    initGoogleConnect();
    initiateConnection();
  }
} catch (error) {
  logError(error);
}

(window as IWindow).addEventListener('offline', handleConnection);
(window as IWindow).addEventListener('online', handleConnection);

function handleConnection(): void {
  // tslint:disable-next-line:no-all-duplicated-branches
  if (navigator.onLine) {
    store.dispatch(CommonActions.setOfflineToast(false));
    if (
      document.getElementsByTagName('HTML') &&
      document.getElementsByTagName('HTML')[0]
    ) {
      document.getElementsByTagName('HTML')[0].classList.remove('offline');
    }
  } else {
    store.dispatch(CommonActions.setOfflineToast(true));
    if (
      document.getElementsByTagName('HTML') &&
      document.getElementsByTagName('HTML')[0]
    ) {
      document.getElementsByTagName('HTML')[0].classList.add('offline');
    }
  }
}

// Re-render the app when window.location changes
// tslint:disable-next-line:cognitive-complexity
async function onLocationChange(
  location: History['location'],
  action?: string
): Promise<void> {
  currentLocation = location;

  const isInitialRender = !action;
  try {
    const renderReactApp =
      isInitialRender && !__DEV__ ? ReactDOM.hydrate : ReactDOM.render;
    if (isInitialRender) {
      await Loadable.preloadReady();
    }
    appInstance = renderReactApp(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <Router history={history as History}>
            {bootstrapOneApp ? <OneApp /> : <App />}
          </Router>
        </Provider>
      </ThemeProvider>,
      container,
      () => {
        if (
          isInitialRender &&
          window.history &&
          'scrollRestoration' in window.history
        ) {
          // Switch off the native scroll restoration behavior and handle it manually
          // https://developers.google.com/web/updates/2015/09/history-api-scroll-restoration
          window.history.scrollRestoration = 'manual';
        }
      }
    );
  } catch (error) {
    if (__DEV__) {
      throw error;
    }

    // Do a full page reload if error occurs during client-side navigation
    if (!isInitialRender && currentLocation.key === location.key) {
      logError('RSK will reload your page after error');
      window.location.reload();
    }
  }
}

if (__DEV__) {
  import('mimic').then(() => {
    // Handle client-side navigation by using HTML5 History API
    // For more information visit https://github.com/mjackson/history#readme
    (history as History).listen(onLocationChange);
    onLocationChange(currentLocation).catch(error => {
      logError(error);
    });
  });
} else {
  // Handle client-side navigation by using HTML5 History API
  // For more information visit https://github.com/mjackson/history#readme
  (history as History).listen(onLocationChange);
  onLocationChange(currentLocation).catch(error => {
    logError(error);
  });
}

setPublickKeyForEncryption().catch(error => {
  logError(error);
});
// Enable Hot Module Replacement (HMR)
if (module.hot) {
  module.hot.accept('./index', () => {
    // tslint:disable-next-line:no-string-literal
    if (appInstance && appInstance['updater'].isMounted(appInstance)) {
      // Force-update the whole tree, including components that refuse to update
      deepForceUpdate(appInstance);
    }
    onLocationChange(currentLocation).catch(error => {
      logError(error);
    });
  });
}
