import Authentication from '@common/routes/authentication';
import GlobalStyle from '@common/reset';
import CommonGlobalStyle from '@common/common';
import Toast from '@common/Toast';
import styled from 'styled-components';
import PreLoader from '@common/Loader';
import React from 'react';
import RouteDetector from '@src/common/RouteDetector';
import { renderRoutes } from 'react-router-config';
import routes from '@common/routes';
import InitApis from '@common/initApis';
import OfflineToast from '@common/components/OfflineToast';

import { breakpoints, colors } from './variables';
import LandscapeModePlaceholder from './components/LandscapeModePlaceholder';

const StyledAppInner = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  justify-content: space-between;
  max-width: ${breakpoints.desktopLarge}px;
  background: ${colors.white};
  margin: auto;


  .proceedAsGuestModal {
    .overlay {
      overflow: auto;
    }
    .overlay,
    .outsideClick,
    .contentWrap,
    .StyledLoginFlowWithProceedAsGuest {
      height: 100%;
      width: 100%;
    }
  }

  .progressModalWrap {
    .overlay {
      /* background: ${colors.black}; */
    }
  }

  .progressModal {
    .overlay {
      align-items: flex-end;

      .outsideClick,
      .contentWrap {
        width: 100%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .progressModal {
      .overlay {
        align-items: center;
        .outsideClick,
        .contentWrap {
          width: 57rem;
        }
      }
    }
  }
`;

export default () => {
  return (
    <>
      <LandscapeModePlaceholder className='landscapeModePlaceholder' />
      <StyledAppInner className='styledAppInner'>
        <Toast />
        <PreLoader />
        <OfflineToast />
        <InitApis />
        <Authentication />
        <RouteDetector />
        {renderRoutes(routes)}
        <GlobalStyle />
        <CommonGlobalStyle isOpen={false} />
      </StyledAppInner>
    </>
  );
};
