import { jsonLdData } from '@src/common/JsonLd';
import { BASE_PATH_URLS } from '@src/common/JsonLd/utils/basePathUrl';
import { mapStateToProps } from '@src/common/JsonLd/mapProps';
import { RootState } from '@src/common/store/reducers';
import appState from '@store/states/app';

describe('JSON ld ', () => {
  test('Json Ld render with home path', async () => {
    expect(
      await jsonLdData(BASE_PATH_URLS.HOME, BASE_PATH_URLS.HOME)
    ).toMatchSnapshot();
  });

  test('Json Ld render with basket path', async () => {
    expect(
      await jsonLdData(
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.BASKET}`,
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.BASKET}`
      )
    ).toMatchSnapshot();
  });

  test('Json Ld render with checkout path', async () => {
    expect(
      await jsonLdData(
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.CHECKOUT}`,
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.CHECKOUT}`
      )
    ).toMatchSnapshot();
  });

  test('Json Ld render with tariff path', async () => {
    expect(
      await jsonLdData(
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.TARIFF}`,
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.TARIFF}`
      )
    ).toMatchSnapshot();
  });

  test('Json Ld render with search path', async () => {
    expect(
      await jsonLdData(
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.SEARCH}`,
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.SEARCH}`
      )
    ).toMatchSnapshot();
  });

  test('Json Ld render with category path', async () => {
    expect(
      await jsonLdData(
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.CATEGORY}`,
        `${BASE_PATH_URLS.HOME}${BASE_PATH_URLS.CATEGORY}`
      )
    ).toMatchSnapshot();
  });

  test('Json Ld render with product detail path', async () => {
    expect(
      await jsonLdData(
        `${
          BASE_PATH_URLS.HOME
        }/categoryslug/variant-name/product-id/variant-id/checkout/roderDetails/demo`,
        `/categoryslug/variant-name/product-id/variant-id/checkout/roderDetails/demo`
      )
    ).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    const initStateValue: RootState = appState();
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
});
