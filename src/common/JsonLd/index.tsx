import WebApplication from '@common/JsonLd/units/WebApplication';
import MobileApplication from '@common/JsonLd/units/MobileApplication';
import SitelinksSearchbox from '@common/JsonLd/units/SitelinksSearchbox';
import Breadcrumb from '@common/JsonLd/units/Breadcrumb';
import CorporateContactAndLogo from '@common/JsonLd/units/CorporateContactAndLogo';
import WebPage from '@common/JsonLd/units/WebPage';
import store from '@src/common/store';
import { jsonLD } from '@productDetailed/store/services';
import {
  checkPermisson,
  getDefaultPermission,
  getProductUrlsParams
} from '@src/common/JsonLd/utils/permissions';
import { IGlobalTranslation } from '@src/common/store/types/translation';
import { IJsonLd } from '@src/common/store/types/configuration';
import {
  IBreadcrumb,
  IPermission,
  IReturnJSONLdType,
  ISitelinksSearchbox,
  ISocialProfile
} from '@common/JsonLd/types';

import { logError } from '../utils';

export const jsonLdData = async (
  hitUri: string,
  pathName: string
): Promise<IReturnJSONLdType> => {
  const hitUrl = decodeURIComponent(hitUri);
  const path = decodeURIComponent(pathName);
  const urlPaths = hitUrl && hitUrl.length > 0 ? hitUrl.split('/') : [];
  const JsonDataArray = [];
  if (urlPaths.length > 0) {
    const jsonLd: IJsonLd = store.getState().configuration.cms_configuration
      .global.jsonLd;
    const {
      corporateContactAndLogo,
      iosApplication,
      androidApplication,
      webApplication,
      sitelinksSearchbox,
      sameAs,
      socialProfile,
      webPage
    } = jsonLd;
    const globalTranslation: IGlobalTranslation = store.getState().translation
      .cart.global;
    let breadcrumbLevel = 0;
    let isProductDetails = false;
    let url = '';
    let urlName = '';
    const productUrlsParams = getProductUrlsParams(path);
    let permission: IPermission = getDefaultPermission();
    breadcrumbLevel = urlPaths.length;

    if (urlPaths.length > 0) {
      const {
        permission: checkPermission,
        url: checkUrl,
        urlName: checkNameUrl,
        isProductDetails: isCheckProductDetails
      } = checkPermisson(urlPaths, hitUrl, path);
      permission = checkPermission;
      url = checkUrl;
      urlName = checkNameUrl;
      isProductDetails = isCheckProductDetails;
    }

    if (permission.isBreadcrumb) {
      const breadcrumbParams: IBreadcrumb = {
        globalTranslation,
        categorySlug: productUrlsParams.categoryId,
        categoryName: productUrlsParams.categoryId,
        productName: productUrlsParams.variantName,
        isProductDetails,
        breadcrumbLevel,
        url,
        urlName
      };
      JsonDataArray.push(Breadcrumb(breadcrumbParams));
    }

    if (permission.isProduct) {
      try {
        const productJsonLd = await jsonLD({
          variantId: productUrlsParams.variantId,
          categoryId: productUrlsParams.categoryId
        });

        if (
          productJsonLd &&
          productJsonLd['@graph'] &&
          productJsonLd['@graph'].length > 0 &&
          productJsonLd['@graph'][0] &&
          productJsonLd['@graph'][0].length > 0
        ) {
          JsonDataArray.push(productJsonLd['@graph'][0][0]);
        }
      } catch (error) {
        logError(error);
      }
    }

    if (corporateContactAndLogo.show && permission.isCorporateContactAndLogo) {
      if (permission.isSocialProfile && socialProfile.show) {
        const sameAsLinks: string[] = [];
        Object.keys(sameAs).forEach(key => {
          sameAsLinks.push(sameAs[key]);
        });
        const reDefinedSocialProfile: ISocialProfile = {
          ...socialProfile,
          sameAs: sameAsLinks,
          url: ''
        };
        JsonDataArray.push(
          CorporateContactAndLogo(
            corporateContactAndLogo,
            reDefinedSocialProfile
          )
        );
      } else {
        JsonDataArray.push(CorporateContactAndLogo(corporateContactAndLogo));
      }
    }
    if (permission.isMobileApplication) {
      if (iosApplication.show) {
        JsonDataArray.push(MobileApplication(iosApplication));
      }
      if (androidApplication.show) {
        JsonDataArray.push(MobileApplication(androidApplication));
      }
    }

    if (webApplication.show && permission.isWebApplication) {
      JsonDataArray.push(WebApplication(webApplication));
    }

    if (sitelinksSearchbox.show && permission.isSitelinksSearchbox) {
      const sameAsLinks: string[] = [];
      Object.keys(sameAs).forEach(key => {
        sameAsLinks.push(sameAs[key]);
      });
      const redefinedsitelinksSearchbox: ISitelinksSearchbox = {
        ...sitelinksSearchbox,
        id: sitelinksSearchbox.id,
        sameAs: sameAsLinks
      };
      JsonDataArray.push(SitelinksSearchbox(redefinedsitelinksSearchbox));
    }

    if (webPage.show && permission.isWebPage) {
      JsonDataArray.push(WebPage(webPage));
    }
  }

  return {
    '@context': 'https://schema.org',
    '@graph': JsonDataArray
  };
};
