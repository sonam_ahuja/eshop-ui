import { RootState } from '@common/store/reducers';
import { IComponentStateToProps } from '@src/common/JsonLd/types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  globalTranslation: state.translation.cart.global,
  globalConfiguration: state.configuration.cms_configuration.global
});
