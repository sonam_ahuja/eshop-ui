import { IGlobalTranslation } from '@src/common/store/types/translation';
import { IGlobalConfiguration } from '@src/common/store/types/configuration';

export type operatingSystem = 'Windows' | 'OSX' | 'Android';

export interface IBreadcrumb {
  categorySlug: string;
  categoryName: string;
  productName: string;
  breadcrumbLevel: number;
  url: string;
  urlName: string;
  isProductDetails: boolean;
  globalTranslation: IGlobalTranslation;
}

export interface IListItem {
  '@type': string;
  item: IItem;
  position: number;
}

export interface IItem {
  '@id': string;
  name: string;
}

export interface IBreadcrumbReturnType {
  '@context': string;
  '@type': string;
  itemListElement: IListItem[];
}

export interface ICorporateContactAndLogoReturnType {
  '@type': string;
  url: string;
  logo: string;
  contactPoint: {
    '@type': string;
    telephone: string;
    contactType: string;
  }[];
  image?: {
    '@type': 'ImageObject';
    '@id': string;
    url: string;
    caption: string;
  };
  sameAs?: string[];
  description?: string;
}

export interface IMobileApplication {
  name: string;
  url: string;
  operatingSystem: string;
  applicationUrl: string;
  downloadUrl: string;
  aggregateRating: {
    ratingValue: string;
    ratingCount: string;
  };
  softwareVersion: string;
  releaseNotes: string;
}

export interface IMobileApplicationReturnType {
  '@type': string;
  name: string;
  operatingSystem: string;
  applicationCategory: string;
  downloadUrl: string;
  softwareVersion: string;
  aggregateRating: {
    '@type': string;
    ratingValue: string;
    ratingCount: string;
  };
  releaseNotes: string;
}

export interface IProduct {
  productName: string;
  image: string;
  description: string;
  brand: string;
}

export interface IProductReturnType {
  '@type': string;
  name: string;
  image: string;
  description: string;
  brand: {
    '@type': string;
    name: string;
  };
}

export interface ISitelinksSearchbox {
  id: string;
  name: string;
  publisherId: string;
  sameAs: string[];
}

export interface ISitelinksSearchboxReturnType {
  '@type': string;
  '@id': string;
  url: string;
  name: string;
  publisher: {
    '@id': string;
  };
  potentialAction: {
    '@type': string;
    target: string;
    'query-input': string;
  };
}

export interface ISocialProfile {
  id: string;
  name: string;
  url: string;
  description: string;
  caption: string;
  sameAs: string[];
}

export interface ISocialProfileReturnType {
  '@type': string;
  name: string;
  image: {
    '@type': 'ImageObject';
    '@id': string;
    url: string;
    caption: string;
  };
  description: string;
  sameAs: string[];
}

export interface IWebApplication {
  name: string;
  url: string;
  operatingSystem: operatingSystem;
  applicationUrl: string;
  aggregateRating: {
    ratingValue: string;
    ratingCount: string;
  };
  softwareVersion: string;
  releaseNotes: string;
}

export interface IWebApplicationReturnType {
  '@type': string;
  name: string;
  applicationCategory: string;
  softwareVersion: string;
  operatingSystem: string[];
}

export interface IWebPageReturnType {
  '@type': string;
  '@id': string;
  url: string;
  inLanguage: string;
  name: string;
  isPartOf: {
    '@id': string;
  };
  about: {
    '@id': string;
  };
  description: string;
}
export interface IReturnJSONLdType {
  '@context': string;
  '@graph': IJsonLdUnitsReturnType[];
}

export type IJsonLdUnitsReturnType =
  | IProductReturnType
  | IWebApplicationReturnType
  | ISocialProfileReturnType
  | ISitelinksSearchboxReturnType
  | IBreadcrumbReturnType
  | ICorporateContactAndLogoReturnType
  | IMobileApplicationReturnType
  | IWebPageReturnType;

export interface IComponentStateToProps {
  globalTranslation: IGlobalTranslation;
  globalConfiguration: IGlobalConfiguration;
}

export interface IPermission {
  isBreadcrumb: boolean;
  isCorporateContactAndLogo: boolean;
  isMobileApplication: boolean;
  isProduct: boolean;
  isSitelinksSearchbox: boolean;
  isSocialProfile: boolean;
  isWebApplication: boolean;
  isWebPage: boolean;
}

export interface ICheckPermissonReturnType {
  url: string;
  urlName: string;
  productUrlsParams: IProductUrlsParams;
  isProductDetails: boolean;
  permission: IPermission;
}

export interface IProductUrlsParams {
  categoryId: string;
  variantName: string;
  productId: string;
  variantId: string;
}

export interface IPermissionWithProduct {
  isBreadcrumb: boolean;
  isCorporateContactAndLogo: boolean;
  isMobileApplication: boolean;
  isSitelinksSearchbox: boolean;
  isSocialProfile: boolean;
  isWebApplication: boolean;
  isWebPage: boolean;
}
export interface IPermission extends IPermissionWithProduct {
  isProduct: boolean;
}
