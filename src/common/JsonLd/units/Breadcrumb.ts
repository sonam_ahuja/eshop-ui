import {
  IBreadcrumb,
  IBreadcrumbReturnType,
  IListItem
} from '@common/JsonLd/types';
import { getFrontendUrl } from '@common/JsonLd/utils/basePathUrl';

const Breadcrumb = (params: IBreadcrumb): IBreadcrumbReturnType => {
  const frontendUrl = getFrontendUrl();
  const newItemList: IListItem[] = [
    {
      '@type': 'ListItem',
      item: {
        '@id': `${frontendUrl}`,
        name: params.globalTranslation.home
      },
      position: 1
    }
  ];

  if (!params.isProductDetails) {
    newItemList.push({
      '@type': 'ListItem',
      item: {
        '@id': `${frontendUrl}${params.url}`,
        name: params.urlName
      },
      position: 2
    });
  } else {
    newItemList.push({
      '@type': 'ListItem',
      item: {
        '@id': `${frontendUrl}category/Devices`,
        name: params.globalTranslation.products
      },
      position: 2
    });
    if (params.categoryName) {
      newItemList.push({
        '@type': 'ListItem',
        item: {
          '@id': `${frontendUrl}${params.categorySlug}`,
          name: params.categoryName
        },
        position: 3
      });
    }
    if (params.productName) {
      newItemList.push({
        '@type': 'ListItem',
        item: {
          '@id': `${frontendUrl}${params.url}`,
          name: params.productName
        },
        position: 4
      });
    }
  }

  return {
    '@context': 'http://schema.org',
    '@type': 'BreadcrumbList',
    itemListElement: newItemList
  };
};

export default Breadcrumb;
