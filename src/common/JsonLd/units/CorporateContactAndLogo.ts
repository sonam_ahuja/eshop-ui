import {
  ICorporateContactAndLogoReturnType,
  ISocialProfile
} from '@common/JsonLd/types';
import { ICorporateContactAndLogo } from '@common/store/types/configuration';
import { getFrontendUrl } from '@common/JsonLd/utils/basePathUrl';

const CorporateContactAndLogo = (
  params: ICorporateContactAndLogo,
  socialParams?: ISocialProfile
): ICorporateContactAndLogoReturnType => {
  let organization = {
    '@type': 'Organization',
    url: getFrontendUrl(),
    logo: params.logo,
    contactPoint: [
      {
        '@type': 'ContactPoint',
        telephone: params.telephone,
        contactType: params.contactType
      }
    ]
  };

  if (socialParams) {
    organization = {
      ...organization,
      ...{
        image: {
          '@type': 'ImageObject',
          '@id': socialParams.id,
          url: socialParams.url,
          caption: socialParams.caption
        },
        sameAs: socialParams.sameAs,
        description: socialParams.description
      }
    };
  }

  return organization;
};

export default CorporateContactAndLogo;
