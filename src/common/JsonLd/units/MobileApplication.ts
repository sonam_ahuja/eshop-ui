import { IMobileApplicationReturnType } from '@common/JsonLd/types';
import { IMobileApplication } from '@common/store/types/configuration';

export const MobileApplication = (
  params: IMobileApplication
): IMobileApplicationReturnType => {
  return {
    '@type': 'MobileApplication',
    name: params.name,
    operatingSystem: params.operatingSystem,
    applicationCategory: params.applicationUrl,
    aggregateRating: {
      '@type': 'AggregateRating',
      ratingValue: params.aggregateRating.ratingValue,
      ratingCount: params.aggregateRating.ratingCount
    },
    downloadUrl: params.downloadUrl,
    softwareVersion: params.softwareVersion,
    releaseNotes: params.releaseNotes
  };
};

export default MobileApplication;
