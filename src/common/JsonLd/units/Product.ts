import { IProduct, IProductReturnType } from '@common/JsonLd/types';

export const Product = (params: IProduct): IProductReturnType => {
  return {
    '@type': 'Product',
    name: params.productName,
    image: params.image,
    description: params.description,
    brand: {
      '@type': 'Thing',
      name: params.brand
    }
  };
};

export default Product;
