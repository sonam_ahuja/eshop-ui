import { ISitelinksSearchbox, ISitelinksSearchboxReturnType } from '@common/JsonLd/types';
import { getFrontendUrl } from '@common/JsonLd/utils/basePathUrl';
import APP_CONSTANTS from '@common/constants/appConstants';

export const SitelinksSearchbox = (params: ISitelinksSearchbox): ISitelinksSearchboxReturnType => {
  const frontendUrl = getFrontendUrl();

  return {
    '@type': 'WebSite',
    '@id': `${frontendUrl}#${params.id}`,
    url: `${frontendUrl}`,
    name: params.name,
    publisher: {
      '@id': `${frontendUrl}#${params.publisherId}`,
    },
    potentialAction: {
      '@type': 'SearchAction',
      target: `${APP_CONSTANTS.ESHOP_BASE_URL}?query={search_term_string}`,
      'query-input': 'required name=search_term_string'
    }
  };
};

export default SitelinksSearchbox;
