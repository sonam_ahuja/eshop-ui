import { ISocialProfile, ISocialProfileReturnType } from '@common/JsonLd/types';

export const SocialProfile = (params: ISocialProfile): ISocialProfileReturnType => {

  return {
    '@type': 'Person',
    name: params.name,
    image: {
      '@type': 'ImageObject',
      '@id': params.id,
      url: params.url,
      caption: params.caption
    },
    sameAs: params.sameAs,
    description: params.description
  };
};

export default SocialProfile;
