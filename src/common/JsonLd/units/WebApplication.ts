import { IWebApplicationReturnType } from '@common/JsonLd/types';
import { IWebApplication } from '@common/store/types/configuration';

const WebApplication = (params: IWebApplication): IWebApplicationReturnType => {
  return {
    '@type': 'WebApplication',
    name: params.name,
    applicationCategory: params.applicationUrl,
    softwareVersion: params.softwareVersion,
    operatingSystem: ['windows', 'mac']
  };
};

export default WebApplication;
