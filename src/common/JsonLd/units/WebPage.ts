import { IWebPageReturnType } from '@common/JsonLd/types';
import { IWebPage } from '@common/store/types/configuration';
import { getFrontendUrl } from '@common/JsonLd/utils/basePathUrl';

export const WebPage = (params: IWebPage): IWebPageReturnType => {
  const frontendUrl = getFrontendUrl();

  return {
    '@type': 'WebPage',
    '@id': `${frontendUrl}#${params.id}`,
    url: `${frontendUrl}`,
    inLanguage: params.inLanguage,
    name: params.name,
    isPartOf: {
      '@id': `${frontendUrl}#${params.isPartOf}`
    },
    about: {
      '@id': params.about
    },
    description: params.description
  };
};

export default WebPage;
