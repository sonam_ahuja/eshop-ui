import Breadcrumb from '@src/common/JsonLd/units/Breadcrumb';
import { IBreadcrumb } from '@common/JsonLd/types';
import store from '@common/store';

describe('<Breadcrumb />', () => {
  const params: IBreadcrumb = {
    categorySlug: 'string',
    categoryName: 'string',
    productName: 'string',
    breadcrumbLevel: 3,
    url: 'string',
    urlName: 'string',
    isProductDetails: true,
    globalTranslation: store.getState().translation.cart.global
  };

  test('when Breadcrumb is loaded, on product url', () => {
    expect(Breadcrumb(params)).toMatchSnapshot();
  });

  test('when Breadcrumb is loaded, on product url', () => {
    const newParams: IBreadcrumb = { ...params, isProductDetails: false };
    expect(Breadcrumb(newParams)).toMatchSnapshot();
  });

  test('when Breadcrumb is loaded, on  categoryName, productName blank', () => {
    const newParams: IBreadcrumb = {
      ...params,
      categoryName: '',
      productName: ''
    };
    expect(Breadcrumb(newParams)).toMatchSnapshot();
  });
});
