import Product from '@src/common/JsonLd/units/Product';
import { IProduct } from '@common/JsonLd/types';

describe('<Product />', () => {
  const params: IProduct = {
    productName: 'string',
    image: 'string',
    description: 'string',
    brand: 'string'
  };

  test('should render properly', () => {
    expect(Product(params)).toMatchSnapshot();
  });
});
