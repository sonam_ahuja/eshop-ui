import APP_CONSTANTS, { REGEX } from '@common/constants/appConstants';

export enum BASE_PATH_URLS {
  HOME = '/',
  BASKET = 'basket',
  CHECKOUT = 'checkout',
  TARIFF = 'tariff',
  SEARCH = 'search',
  CATEGORY = 'category'
}

export const getFrontendUrl = () => {
  return `${APP_CONSTANTS.ESHOP_BASE_URL.replace(REGEX.EXTRACT_BASE_URL, '')}/`;
};
