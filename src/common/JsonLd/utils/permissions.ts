import { BASE_PATH_URLS } from '@common/JsonLd/utils/basePathUrl';
import {
  ICheckPermissonReturnType,
  IPermission,
  IProductUrlsParams
} from '@common/JsonLd/types';

export const getDefaultPermission = (): IPermission => {
  return {
    isBreadcrumb: false,
    isCorporateContactAndLogo: false,
    isMobileApplication: false,
    isProduct: false,
    isSitelinksSearchbox: false,
    isSocialProfile: false,
    isWebApplication: false,
    isWebPage: false
  };
};

export const getProductUrlsParams = (pathname: string): IProductUrlsParams => {
  const arrPath = pathname && pathname.split('/');
  if (arrPath && arrPath[1] && arrPath[2] && arrPath[3] && arrPath[4]) {
    return {
      categoryId: arrPath[1],
      variantName: arrPath[2],
      productId: arrPath[3],
      variantId: arrPath[4]
    };
  }

  return {
    categoryId: '',
    variantName: '',
    productId: '',
    variantId: ''
  };
};

export const checkPermisson = (
  urlPaths: string[],
  hitUrl: string,
  path: string
): ICheckPermissonReturnType => {
  const permission = getDefaultPermission();
  let url = '';
  let urlName = '';
  let isProductDetails = false;
  const productUrlsParams = getProductUrlsParams(path);
  if (urlPaths.length > 0) {
    url = hitUrl;
    urlName = urlPaths[1];
    if (urlPaths[1] === '') {
      permission.isCorporateContactAndLogo = true;
      permission.isWebPage = true;
      permission.isSitelinksSearchbox = true;
      permission.isMobileApplication = true;
      permission.isSocialProfile = true;
      permission.isWebApplication = true;
    } else if (
      urlPaths[1].length > 0 &&
      urlPaths[1] !== BASE_PATH_URLS.CHECKOUT
    ) {
      permission.isCorporateContactAndLogo = true;
      permission.isWebPage = true;
      permission.isBreadcrumb = true;
    }

    if (urlPaths.length === 5) {
      permission.isProduct = true;
      permission.isBreadcrumb = true;
      isProductDetails = true;
    }
  }

  return {
    url,
    urlName,
    productUrlsParams,
    isProductDetails,
    permission
  };
};
