import { Component, ReactNode } from 'react';
import { EnterLoginCredentials } from '@authentication/LoginWithUser';

export default class PreLoader extends Component<{}> {
  componentDidMount(): void {
    EnterLoginCredentials.preload();
  }

  render(): ReactNode {
    return null;
  }
}
