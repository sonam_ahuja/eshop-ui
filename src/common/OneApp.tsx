import React from 'react';
import GlobalStyle from '@common/reset';
import OneAppRouteDetector from '@src/common/OneAppRouteDetector';
import Toast from '@common/Toast';

export default () => {
  return (
    <div>
      <Toast />
      <GlobalStyle />
      <OneAppRouteDetector />
    </div>
  );
};
