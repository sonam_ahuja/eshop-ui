import { renderRoutes } from 'react-router-config';
import { connect } from 'react-redux';
import actions from '@authentication/store/actions';
import { Component, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Dispatch } from 'redux';
import oneAppRoutes from '@common/oneAppRoutes';
import appConstants, { ONEAPP_PARAMS } from '@common/constants/appConstants';
import history from '@client/history';
import productListActions from '@productList/store/actions';
import ReactGA from 'react-ga';
import { IWindow } from '@src/client';

import { RootState } from './store/reducers';
import { isBrowser } from './utils';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IState {
  paramsMap: {
    [key: string]: string;
  };
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  gaCode: string;
  redirectToFB(facebookCode: string, callbackUrl: string): void;
  setProlongationOpenedFrom(url: string): void;
  getAccessToken(): void;
}

export class RouteDetector extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.getQueryParamsForOneApp = this.getQueryParamsForOneApp.bind(this);
    this.getSearchParamsForOneApp = this.getSearchParamsForOneApp.bind(this);
    this.doesAppContainsAllOneAppQueryParams = this.doesAppContainsAllOneAppQueryParams.bind(
      this
    );
    const paramsMap: IState['paramsMap'] = {};
    const params = new URLSearchParams(this.props.location.search);
    this.getQueryParamsForOneApp().forEach(param => {
      paramsMap[param] =
        param === ONEAPP_PARAMS.LANGUAGE_CODE
          ? params.get(param) || appConstants.LANGUAGE_CODE
          : params.get(param) || '';
    });
    this.state = {
      paramsMap
    };
  }

  componentDidMount(): void {
    ReactGA.initialize(this.props.gaCode);

    const viewportHeight = document.documentElement.clientHeight;

    document.documentElement.setAttribute(
      'style', `height: ${viewportHeight}px; overflow: auto;`);
    document.body.style.height = `${viewportHeight}px`;

    this.props.getAccessToken();
    this.props.setProlongationOpenedFrom(window.location.href);
    const params = new URLSearchParams(this.props.location.search);
    if (params.get('code')) {
      this.props.redirectToFB(
        params.get('code') as string,
        `${window.location.origin}${this.props.location.pathname}`
      );
    }
  }

  getQueryParamsForOneApp(): string[] {
    return Object.keys(ONEAPP_PARAMS).map(key => ONEAPP_PARAMS[key]);
  }

  getSearchParamsForOneApp(): string {
    let search = '';
    Object.keys(this.state.paramsMap).forEach(key => {
      if (search) {
        search += '&';
      }
      search += `${key}=${this.state.paramsMap[key]}`;
    });

    return search;
  }

  doesAppContainsAllOneAppQueryParams(search: string): boolean {
    const decodedSearch = decodeURIComponent(search);
    let containsAllParams = true;
    this.getQueryParamsForOneApp()
      .filter(param => param !== ONEAPP_PARAMS.LANGUAGE_CODE)
      .forEach(param => {
        if (decodedSearch.indexOf(param) === -1) {
          containsAllParams = false;

          return;
        }
      });

    return containsAllParams;
  }

  componentDidUpdate(prevProps: IProps): void {
    if (isBrowser && !(window as IWindow).ga) {
      ReactGA.initialize(this.props.gaCode);
    }

    if (
      (this.props.location.pathname === prevProps.location.pathname &&
        this.props.location.search === prevProps.location.search) ||
      this.doesAppContainsAllOneAppQueryParams(this.props.location.search)
    ) {
      return;
    }
    if (history) {
      if (this.props.location.search) {
        history.replace(
          `${this.props.location.pathname}${
          this.props.location.search
          }&${this.getSearchParamsForOneApp()}`
        );
      } else {
        history.replace(
          `${this.props.location.pathname}?${this.getSearchParamsForOneApp()}`
        );
      }
    }
  }

  render(): ReactNode {
    return renderRoutes(oneAppRoutes);
  }
}

export const mapStateToProps = (state: RootState) => ({
  gaCode: state.configuration.cms_configuration.modules.prolongation.gaCode
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  redirectToFB(facebookCode: string, callbackUrl: string): void {
    dispatch(
      actions.sendSocialMediaCodeToBackend({ callbackUrl, facebookCode })
    );
  },
  setProlongationOpenedFrom(url: string): void {
    dispatch(productListActions.setProlongationOpenedFrom(url));
  },
  getAccessToken(): void {
    dispatch(productListActions.generateToken());
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RouteDetector)
);
