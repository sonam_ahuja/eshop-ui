import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import { Component, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { commonAction } from '@store/actions';
export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  redirectToFB(facebookCode: string, callbackUrl: string): void;
  openLogin(): void;
  fetchThidrdPartyDetails(): void;
}

export class RouteDetector extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  componentDidMount(): void {
    const params = new URLSearchParams(this.props.location.search);
    if (params.get('code')) {
      this.props.redirectToFB(
        params.get('code') as string,
        `${window.location.origin}${this.props.location.pathname}`
      );
    }
    if (params.get('authentication')) {
      this.props.fetchThidrdPartyDetails();
      this.props.openLogin();
    }
  }

  render(): ReactNode {
    return null;
  }
}

export const mapStateToProps = () => ({});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  redirectToFB(facebookCode: string, callbackUrl: string): void {
    dispatch(
      actions.sendSocialMediaCodeToBackend({ callbackUrl, facebookCode })
    );
  },
  openLogin(): void {
    dispatch(commonAction.routeToLoginFromBasket(true));
  },
  fetchThidrdPartyDetails(): void {
    dispatch(commonAction.fetchIdentityVerificationDetails());
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RouteDetector)
);
