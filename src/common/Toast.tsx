import React, { ReactNode } from 'react';
import { connect } from 'react-redux';
import { Paragraph, Toast } from 'dt-components';
import { messageType } from 'dt-components/lib/es/components/atoms/strip-message';
import actions from '@common/store/actions/common';
import { Dispatch } from 'redux';
import { RootState } from '@store/reducers';

interface IProps {
  message: string;
  isOpen: boolean;
  closeToast(): void;
}

class Index extends React.Component<IProps> {
  render(): ReactNode {
    const { isOpen, message, closeToast } = this.props;

    return (
      <Toast
        icon='ec-emergency-triangle'
        isOpen={isOpen}
        type={'error' as messageType}
        onDismiss={closeToast}
        timer={2000}
        onTimerExpire={closeToast}
        style={{ zIndex: 9999 }}
      >
        <Paragraph weight='medium' size='small' transform={'capitalize'}>
          {message}
        </Paragraph>
      </Toast>
    );
  }
}

export const mapStateToProps = (state: RootState) => {
  return {
    message: state.common.errorToast.message,
    isOpen: state.common.errorToast.isOpen
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  closeToast(): void {
    dispatch(actions.removeError());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);
