import configureStore from 'redux-mock-store';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import {
  IProps as IComponentProps,
  mapDispatchToProps,
  mapStateToProps,
  RouteDetector
} from '@src/common/RouteDetector';
import { RootState } from '@src/common/store/reducers';
import appState from '@store/states/app';
import constants from '@authentication/store/constants';
import actionConstants from '@store/constants/actionConstants';
import { IParameterRequiredForSocialMedia } from '@authentication/store/types';

describe('<RouteDetector />', () => {
  const props: IComponentProps = {
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    location: {
      pathname: '/basket',
      search: 'code=URLUtils',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    },
    openLogin: jest.fn(),
    fetchThidrdPartyDetails: jest.fn(),
    redirectToFB: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <RouteDetector {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('component render with authentication params', () => {
    const newParams: IComponentProps = { ...props };
    newParams.location.search = 'authentication=URLUtils';
    const component = componentWrapper(newParams);
    expect(component).toMatchSnapshot();
  });

  test('should render properly with search params null', () => {
    const newProps: IComponentProps = { ...props };
    newProps.location = { ...newProps.location, search: '' };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps()).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const expected: {
      type: string;
      payload: IParameterRequiredForSocialMedia;
    } = {
      type: constants.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND,
      payload: { callbackUrl: 'facebookCode', facebookCode: 'facebookString' }
    };

    const openLoginExpected: {
      type: string;
      payload: boolean;
    } = {
      type: actionConstants.ROUTE_TO_LOGIN_FROM_BASKET,
      payload: true
    };
    const fetchThidrdPartyDetailsExpected: {
      type: string;
      payload?: string;
    } = {
      type: actionConstants.FETCH_IDENTITY_VERIFICATION_DETAILS,
      payload: undefined
    };
    mapDispatchToProps(dispatch).redirectToFB('facebookString', 'facebookCode');
    mapDispatchToProps(dispatch).openLogin();
    mapDispatchToProps(dispatch).fetchThidrdPartyDetails();
    expect(dispatch.mock.calls[0][0]).toEqual(expected);
    expect(dispatch.mock.calls[1][0]).toEqual(openLoginExpected);
    expect(dispatch.mock.calls[2][0]).toEqual(fetchThidrdPartyDetailsExpected);
  });
});
