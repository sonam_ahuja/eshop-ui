import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { RootState } from '@common/store/reducers';

import App from '../App';

describe('<App />', () => {
  test('should render properly', () => {
    const props = {};
    const mockStore = configureStore();
    const initStateValue: RootState = {
      basket: appState().basket,
      translation: appState().translation,
      englishTranslation: appState().englishTranslation,
      configuration: appState().configuration,
      common: appState().common,
      authentication: appState().authentication,
      checkout: appState().checkout,
      categories: appState().categories,
      productList: appState().productList,
      orderConfirmation: appState().orderConfirmation,
      productDetailed: appState().productDetailed,
      tariff: appState().tariff,
      search: appState().search,
      landingPage: appState().landingPage
    };
    const store = mockStore(initStateValue);
    const component = mount(
      <ThemeProvider theme={{}}>
        <StaticRouter context={{}}>
          <Switch>
            <Provider store={store}>
              <App {...props} />
            </Provider>
          </Switch>
        </StaticRouter>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
