import createGlobalStyle from '@src/common/common';

describe('<common />', () => {
  test('should retrun function', () => {
    expect(typeof createGlobalStyle).toBe('function');
  });
});
