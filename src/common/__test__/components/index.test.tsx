import React from 'react';
import { StyledHorizontalScrollRow } from '@common/components/styles';
import { ThemeProvider } from 'dt-components';
import { mount } from 'enzyme';

describe('<Components />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <div>
          <StyledHorizontalScrollRow />
        </div>
      </ThemeProvider>
    );

    expect(component).toMatchSnapshot();
  });
});
