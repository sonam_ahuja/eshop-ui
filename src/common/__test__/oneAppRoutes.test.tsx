import routes, { IRoutes } from '@src/common/oneAppRoutes';

describe('<oneAppRoutes />', () => {
  test('routes test', () => {
    routes.forEach((route: IRoutes) => {
      expect(typeof route.exact).toBe('boolean');
    });
  });
});
