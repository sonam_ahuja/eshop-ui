import configureStore from 'redux-mock-store';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import SwitchRenderRoute, { IProps } from '@src/common/oneAppSwitchRenderRoute';
import { RootState } from '@src/common/store/reducers';
import appState from '@store/states/app';

describe('<SwitchRenderRoute />', () => {
  const props: IProps = {
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    location: {
      pathname: '/basket',
      search: 'code=URLUtils',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    }
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SwitchRenderRoute {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly with search params null', () => {
    const newProps: IProps = { ...props };
    newProps.location = { ...newProps.location, search: '' };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when pathname is product details', () => {
    const newProps: IProps = { ...props };
    newProps.location = {
      ...newProps.location,
      pathname: 'www.eshop.com/variant-name/product-id/variant-id'
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when pathname is product details with prolongation', () => {
    const newProps: IProps = { ...props };
    newProps.location.pathname =
      'www.eshop.com/categoryslug/variant-name/product-id/variant-id/checkout/roderDetails';
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
