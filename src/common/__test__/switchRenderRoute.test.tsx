import configureStore from 'redux-mock-store';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import {
  IProps as IComponentProps,
  SwitchRenderRoute
} from '@common/switchRenderRoute';
import { RootState } from '@src/common/store/reducers';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';

describe('<RouteDetector />', () => {
  window.scrollTo = jest.fn();
  const props: IComponentProps = {
    ...histroyParams
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SwitchRenderRoute {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when pathname has dynamic url', () => {
    const newProps: IComponentProps = { ...props };
    newProps.history.location = {
      ...newProps.history.location,
      ...{ pathname: '/basket/phone/samsumg' }
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when nop pathname in location', () => {
    const renderProps: IComponentProps = { ...props };
    renderProps.history.location = {
      ...renderProps.history.location,
      ...{ pathname: '' }
    };
    const component = componentWrapper(renderProps);
    expect(component).toMatchSnapshot();
  });

  test('load product details', () => {
    const renderProps: IComponentProps = { ...props };
    renderProps.history.location = {
      ...renderProps.history.location,
      ...{ pathname: '' }
    };
    const component = componentWrapper(renderProps);
    component.setState({
      isProductDetailedRoutePresent: true
    });
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line: no-identical-functions
  test('should render properly when pathname is product detail', () => {
    const newProps: IComponentProps = { ...props };
    newProps.history.location = {
      ...newProps.history.location,
      ...{
        pathname: 'www.eshop.com/variant-name/product-id/variant-id/checkout'
      }
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line: no-identical-functions
  test('should render properly when pathname is tariff', () => {
    const newProps: IComponentProps = { ...props };
    newProps.history.location = {
      ...newProps.history.location,
      ...{
        pathname: 'www.eshop.com/tariff'
      }
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
