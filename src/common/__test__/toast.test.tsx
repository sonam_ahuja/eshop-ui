import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import configurationState from '@store/states/configuration';
import translationState from '@store/states/translation';
import commonState from '@store/states/common';
import IState from '@basket/store/state';
import Toast from '@common/Toast';

describe('<Toast />', () => {
  test('should render properly', () => {
    const props = {};
    const mockStore = configureStore();
    const initStateValue = {
      basket: IState(),
      translation: translationState(),
      configuration: configurationState(),
      common: commonState()
    };
    const store = mockStore(initStateValue);
    const component = mount(
      <ThemeProvider theme={{}}>
        <StaticRouter context={{}}>
          <Switch>
            <Provider store={store}>
              <Toast {...props} />
            </Provider>
          </Switch>
        </StaticRouter>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
