import { mapDispatchToProps } from '../Toast';

describe('<Basket />', () => {
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).closeToast();
    expect(dispatch.mock.calls[0][0]).toEqual({
      payload: undefined,
      type: 'ESHOP_REMOVE_API_ERROR'
    });
  });
});
