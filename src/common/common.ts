import { createGlobalStyle } from 'styled-components';
import { StyledDialogBoxError } from '@common/components/Error/DialogBoxError/styles';

import { breakpoints, colors } from './variables';

export default createGlobalStyle`
  html{
    max-width: 100vw;
    overflow-x: hidden;
    background: ${colors.fogGray};
    overscroll-behavior: initial !important;
    scrollbar-width: none
    /* scroll-behavior: smooth; removed coz listing and detaild page behave wiredremoved cuz listing and detail page behave weird */
  }
  body{
    overflow: initial !important;
    scrollbar-width: none
  }
  body.hasFlowPanel{
    .isSticky,
    .summaryFakeButton{
      display: none;
    }
  }
  body.overlay-visible{
    overflow:hidden;
    position:fixed;
    width:100%;
  }
  html.offline{
    pointer-events: none;
    filter: grayscale(0.8);
    .toastStripMessage{
      background:${colors.white};
      color:${colors.magenta};
      box-shadow: rgba(0, 0, 0, 0.3) 0 2px 6px 0;
      max-width: 25rem;
      margin: auto;
    }
  }

  .dt_popover.variatTitle{
    padding: 0.25rem 0.5rem;
    border-radius: 4px;
    font-size: 0.7rem;
    color: ${colors.white};
    button{
      display: none;
    }
  }

  /* when MegaMenu open hide filter START*/
  .filterLayout{transition: 0s linear 0.2s};
  .megaMenuOpen .filterLayout{opacity:0; visibility: hidden; transition: 0.2s ease-in 0.2s;}
  /* when MegaMenu open hide filter END*/

  @media (min-width: ${breakpoints.desktop}px) {
    body.hasErrorFlowPanel{
      ${StyledDialogBoxError}{
        z-index: 1001;
      }
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
  }
`;
