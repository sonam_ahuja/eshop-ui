import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import AccordionLink, { IProps } from './';

describe('<AccordionLink />', () => {
  const props: IProps = {
    className: '',
    iconName: 'ec-arrow-right',
    text: '',
    href: '',
    onClick: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <AccordionLink {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, open is false', () => {
    const newProps = { ...props, href: 'dummy', iconName: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
