import React from 'react';
import { Icon, Paragraph } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';

import { StyledAccordionLink } from './styles';

export interface IProps {
  className?: string;
  iconName?: iconNamesType;
  text: string;
  href?: string;
  onClick?(): void;
}

const AccordionLink = (props: IProps) => {
  const {
    className,
    iconName = 'ec-pdf-download',
    text,
    href,
    onClick
  } = props;

  return (
    <StyledAccordionLink
      className={className}
      href={href ? href : undefined}
      onClick={onClick}
      target={'_blank'}
    >
      <div className='header'>
        <Paragraph className='title' size='large' weight='bold'>
          {text}
        </Paragraph>
        <Icon name={iconName} size='inherit' />
      </div>
    </StyledAccordionLink>
  );
};

export default AccordionLink;
