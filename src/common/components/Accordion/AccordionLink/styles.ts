import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledAccordionLink = styled.a`
  /* styles are almost similer to AccordionWithBorderBottom*/
  cursor: pointer;
  display: block;
  .header {
    background: none;
    padding: 1.375rem 0 1.3125rem;
    border-bottom: 1px solid ${colors.lightGray};
    position: relative;

    .title {
      display: inline-flex;
      color: ${colors.darkGray};
      margin-right: 2rem;
    }
    .dt_icon {
      color: ${colors.lightGray};
      position: absolute;
      right: 0;
      top: 1.25rem;
      font-size: 1.5rem;
    }
  }
`;
