import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledAccordionWithBorderBottom = styled.div`
  /* default classes of accordion comming from dt-components start */
  .accordionHeader {
    background: none;
    padding: 1.375rem 0 1.3125rem;
    border-bottom: 1px solid ${colors.lightGray};
    align-items: flex-start;
    position: relative;

    .accordionTitle {
      display: inline-flex;
      color: ${colors.darkGray};
      margin-right: 2rem;
    }
    .dt_icon {
      color: ${colors.lightGray};
      position: absolute;
      right: 0;
      top: 1.3rem;
    }
  }

  .isOpen {
    .accordionHeader {
      border-bottom: none;
      padding-bottom: 0.5rem;
    }
  }
  /* default classes of accordion comming from dt-components end */

  .accordionBody {
    padding-bottom: 1.5rem;
    line-height: 1.5;
    border-bottom: 1px solid ${colors.lightGray};
    color: ${colors.darkGray};
  }
`;
