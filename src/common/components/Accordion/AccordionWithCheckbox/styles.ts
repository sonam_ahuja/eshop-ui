import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledAccordionWithCheckbox = styled.div`
  .dt_accordion {
    /* Header Start */
    .accordionHeader {
      background: none;
      padding: 1.25rem 2rem 1.25rem 0;
      align-items: flex-start;
      position: relative;
    }
    .accordionHeader:after {
      content: '';
      position: absolute;
      width: calc(100% - 40px - 1.25rem);
      height: 1px;
      bottom: 0;
      right: 0;
      background: ${colors.lightGray};
    }

    .accordionHeader .accordionTitle {
      display: inline-flex;
      color: ${colors.mediumGray};
      margin: 0;
      font-size: 1.25rem;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;

      .dt_checkbox {
        margin-right: 1.25rem;
      }
      .dt_checkbox + .text {
        align-self: center;
      }
    }
    .accordionHeader .accordionTitle + .dt_icon {
      color: ${colors.lightGray};
      position: absolute;
      right: 0;
      top: 1.75rem;
    }
    /* Header End */

    /* Accordion Body Start */
    .accordionHeader + div .accordionBody {
      position: relative;
      padding-bottom: 0.25rem;

      .textWrapper {
        display: flex;
        padding-left: 1.5rem;
        padding-bottom: 1rem;

        font-size: 1rem;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        color: ${colors.mediumGray};

        .link {
          /* text-transform: capitalize; */
          color: ${colors.magenta};
          transition: 0.2s linear 0s;
          cursor: pointer;
          &:hover {
            opacity: 0.8;
          }
        }
      }
      .textWrapper .dt_checkbox {
        margin-right: 1.25rem;
        margin-top: 2px;

        & + .text {
          align-self: center;
        }

        .dt_icon {
          font-size: 19px;
        }

        label div {
          width: 30px;
          height: 30px;
        }
      }

      &:after {
        content: '';
        position: absolute;
        width: calc(100% - 40px - 1.25rem);
        height: 1px;
        bottom: 0;
        right: 0;
        background: ${colors.lightGray};
      }
    }
    /* Accordion Body End */
  }

  .dt_accordion.isOpen {
    .accordionHeader {
      background: none;
      padding-bottom: 1.125rem;
      align-items: flex-start;
      position: relative;
    }
    .accordionHeader:after {
      display: none;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
  }
`;
