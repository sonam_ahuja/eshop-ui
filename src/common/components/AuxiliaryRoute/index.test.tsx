import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import AuxiliaryRouteWithRouter, {
  AuxiliaryRoute,
  IProps as IAuxiliaryRouteProps
} from '@common/components/AuxiliaryRoute';
import { histroyParams } from '@mocks/common/histroy';

describe('<AuxiliaryRoute />', () => {
  const props: IAuxiliaryRouteProps = {
    ...histroyParams,
    hashPath: 'summary',
    children: <h1>React node</h1>,
    isChildrenRender: true,
    isModal: true,
    onClose: jest.fn(),
    onOpen: jest.fn()
  };
  const componentWrapper = (newpProps: IAuxiliaryRouteProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <AuxiliaryRouteWithRouter {...newpProps} />
        </ThemeProvider>
      </StaticRouter>
    );

  test('component should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, childern hidden', () => {
    const newpProps: IAuxiliaryRouteProps = {
      ...props,
      isChildrenRender: false
    };
    const component = componentWrapper(newpProps);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when children is not modal', () => {
    const newpProps: IAuxiliaryRouteProps = {
      ...props,
      isModal: false
    };
    const component = componentWrapper(newpProps);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when callback is not passed', () => {
    const newpProps: IAuxiliaryRouteProps = {
      ...props,
      onClose: undefined,
      onOpen: undefined
    };
    const component = componentWrapper(newpProps);
    expect(component).toMatchSnapshot();
  });

  test('childern render based on props passed, when component update', () => {
    const newProps: IAuxiliaryRouteProps = {
      ...props
    };
    const component = componentWrapper(newProps);
    const updatedProps: IAuxiliaryRouteProps = {
      ...props,
      isChildrenRender: false
    };
    (component
      .find('AuxiliaryRoute')
      .instance() as AuxiliaryRoute).showCustomRoute();
    (component
      .find('AuxiliaryRoute')
      .instance() as AuxiliaryRoute).hideCustomRoute();
    component.setProps({ updatedProps });
    component.update();
    (component
      .find('AuxiliaryRoute')
      .instance() as AuxiliaryRoute).hashChangeDetect();
    expect(component).toMatchSnapshot();
  });

  test('component will unmount', () => {
    const newProps: IAuxiliaryRouteProps = {
      ...props
    };
    const component = componentWrapper(newProps);
    component.unmount();
    expect(component).toMatchSnapshot();
  });
});
