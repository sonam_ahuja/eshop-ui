import React, { Component, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
export interface IProps {
  hashPath: string;
  children: ReactNode;
  isChildrenRender: boolean;
  isModal?: boolean;
  onClose?(): void;
  onOpen?(): void;
  auxUnmount?(): void;
}

export interface IState {
  isChildrenRender: boolean;
}

export type IComponentProps = IProps & RouteComponentProps;
/** USED ON PROLONGATION */
export class AuxiliaryRoute extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    const { isChildrenRender } = this.props;
    this.state = { isChildrenRender };
    this.hashChangeDetect = this.hashChangeDetect.bind(this);
    this.showCustomRoute = this.showCustomRoute.bind(this);
    this.hideCustomRoute = this.hideCustomRoute.bind(this);
  }

  componentDidMount(): void {
    window.addEventListener('hashchange', this.hashChangeDetect, true);
  }

  componentDidUpdate(prevProps: IComponentProps): void {
    if (prevProps.isChildrenRender !== this.props.isChildrenRender) {
      if (this.props.isChildrenRender) {
        this.showCustomRoute();
      } else {
        this.hideCustomRoute();
      }
    }
  }

  componentWillReceiveProps(nextProps: IComponentProps): void {
    if (
      this.props.isChildrenRender !== nextProps.isChildrenRender &&
      !nextProps.isChildrenRender &&
      this.props.auxUnmount
    ) {
      this.props.auxUnmount();
    }
  }

  showCustomRoute(): void {
    const { hashPath, onOpen } = this.props;
    this.setState({ isChildrenRender: true }, () => {
      if (window.location.hash !== `#${hashPath}`) {
        if (this.props.history.location.search.length > 0) {
          this.props.history.push(
            `${this.props.history.location.pathname}${
              this.props.history.location.search
            }#${hashPath}`
          );
        } else {
          this.props.history.push(
            `${this.props.history.location.pathname}#${hashPath}`
          );
        }
      }
      if (onOpen) {
        onOpen();
      }
    });
  }

  hideCustomRoute(): void {
    const { onClose } = this.props;
    if (this.state.isChildrenRender !== false) {
      this.setState({ isChildrenRender: false }, () => {
        this.removeHash();
        if (onClose) {
          onClose();
        }
      });
    }
  }

  removeHash = () => {
    const uri = window.location.toString();
    const { hashPath } = this.props;
    if (uri.indexOf('#') > 0 && window.location.hash === `#${hashPath}`) {
      window.history.back();
    }
  }

  componentWillUnmount(): void {
    window.removeEventListener('hashchange', this.hashChangeDetect, false);
  }

  hashChangeDetect(): void {
    const { hashPath, onOpen } = this.props;
    if (window.location.hash === `#${hashPath}`) {
      this.setState(
        {
          isChildrenRender: true
        },
        () => {
          if (onOpen) {
            onOpen();
          }
        }
      );
    } else {
      this.hideCustomRoute();
    }
  }

  render(): ReactNode {
    const { children, isModal } = this.props;
    const { isChildrenRender } = this.state;

    return isModal ? (
      isChildrenRender ? (
        <>{children}</>
      ) : null
    ) : (
      <>{children}</>
    );
  }
}

export default withRouter(AuxiliaryRoute);
