import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import Amount, {
  IProps as IAmountProps
} from '@src/common/components/BasketStripWrapper/BasketStrip/Amount';
import appState from '@store/states/app';

describe('<Amount />', () => {
  const props: IAmountProps = {
    className: 'someClass',
    basket: appState().basket,
    currency: 'USD',
    translation: appState().translation.cart.basket
  };
  const componentWrapper = (newpProps: IAmountProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Amount {...newpProps} />
        </ThemeProvider>
      </StaticRouter>
    );

  test('component should render properly', () => {
    const newProps = { ...props };
    newProps.basket.cartSummary = [
      {
        totalPrice: 70,
        price: 250,
        priceType: 'upfrontPrice',
        priceAlterations: [
          {
            duration: {
              timePeriod: '1',
              type: 'qewq'
            },
            details: [{ name: '', value: '' }],
            priceType: 'discount',
            price: -180,
            subPriceAlterations: [
              {
                priceType: 'discount',
                name: 'TariffPlan discount',
                price: -180,
                label: ''
              }
            ]
          }
        ]
      },
      {
        totalPrice: 70,
        price: 250,
        priceType: 'recurringFee',
        priceAlterations: [
          {
            duration: {
              timePeriod: '1',
              type: 'qewq'
            },
            details: [{ name: '', value: '' }],
            priceType: 'discount',
            price: -180,
            subPriceAlterations: [
              {
                priceType: 'discount',
                name: 'TariffPlan discount',
                price: -180,
                label: ''
              }
            ]
          }
        ]
      }
    ];
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, childern className is undefined', () => {
    const newpProps: IAmountProps = {
      ...props,
      className: undefined
    };
    const component = componentWrapper(newpProps);
    expect(component).toMatchSnapshot();
  });
});
