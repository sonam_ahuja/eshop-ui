import React from 'react';
import { Paragraph, Section } from 'dt-components';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { IBasketState } from '@basket/store/types';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';

import { StyledAmount, StyledMonthly, StyledTotalDue } from './styles';

export interface IProps {
  className?: string;
  basket: IBasketState;
  currency: string;
  translation: IBasketTranslation;
}

const Amount = (props: IProps) => {
  const { className, basket, currency, translation } = props;
  const monthlyPrice = basket.cartSummary.filter(({ priceType }) => {
    return priceType === ITEM_PRICE_TYPE.RECURRING_FEE;
  })[0];

  const upFronPrice = basket.cartSummary.filter(({ priceType }) => {
    return priceType === ITEM_PRICE_TYPE.UPFRONT;
  })[0];

  return (
    <StyledAmount className={className}>
      <StyledMonthly>
        <li>
          <Section size='small' transform='uppercase'>
            {translation.totalMonthly}
          </Section>
        </li>
        <li>
          <Paragraph size='large' weight='bold'>
            {monthlyPrice && monthlyPrice.totalPrice
              ? monthlyPrice.totalPrice
              : 0}{' '}
            {currency}
          </Paragraph>
        </li>
      </StyledMonthly>
      <StyledTotalDue>
        <li>
          <Section size='small' transform='uppercase'>
            {translation.totalDueDate}
          </Section>
        </li>
        <li>
          <Paragraph size='medium'>
            {upFronPrice && upFronPrice.totalPrice ? upFronPrice.totalPrice : 0}{' '}
            {currency}
          </Paragraph>
        </li>
      </StyledTotalDue>
    </StyledAmount>
  );
};

export default Amount;
