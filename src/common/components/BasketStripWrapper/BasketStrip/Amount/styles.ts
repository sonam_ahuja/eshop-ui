import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledAmount = styled.div`
  padding-right: 1rem;
`;

export const StyledMonthly = styled.ul`
  display: flex;
  justify-content: space-between;
  color: ${colors.magenta};
`;
export const StyledTotalDue = styled.ul`
  display: flex;
  justify-content: space-between;
  margin-top: 0.5rem;

  li:first-child {
    position: relative;
    top: -5px;
  }
`;
