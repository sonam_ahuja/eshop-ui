import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import { AnyAction, Store } from 'redux';
import configureStore from 'redux-mock-store';
import {
  BasketStrip,
  IProps as IBasketStripProps
} from '@src/common/components/BasketStripWrapper/BasketStrip';
import appState from '@store/states/app';

describe('<BasketStrip />', () => {
  const props: IBasketStripProps = {
    open: true,
    currency: 'USD',
    basket: appState().basket,
    basketImages: ['http://www.google.com/google.jpg'],
    basketItems: 3,
    translation: appState().translation.cart.basket,
    showItemCount: true,
    clearBasketModalState: appState().basket.clearBasket,
    setDeleteBasketModal: jest.fn(),
    clearBasket: jest.fn(),
    redirectToBasket: jest.fn(),
    proceedToCheckout: jest.fn(),
    showBasketSideBar: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (
    newpProps: IBasketStripProps,
    // tslint:disable-next-line: no-any
    newStore: Store<any, AnyAction>
  ) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={newStore}>
            <BasketStrip {...newpProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

  test('component should render properly', () => {
    const component = componentWrapper(props, store);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when open is undefined', () => {
    const newpProps: IBasketStripProps = {
      ...props,
      open: undefined
    };
    const component = componentWrapper(newpProps, store);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when showBasketSideBar is true', () => {
    const initState: RootState = {
      ...initStateValue,
      basket: {
        ...initStateValue.basket,
        showBasketSideBar: true
      }
    };
    const newStore = mockStore(initState);
    const newpProps: IBasketStripProps = {
      ...props,
      basket: {
        ...props.basket,
        showBasketSideBar: true
      }
    };
    const component = componentWrapper(newpProps, newStore);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when basket is loading', () => {
    const initState: RootState = {
      ...initStateValue,
      basket: {
        ...initStateValue.basket,
        loading: true
      }
    };
    const newStore = mockStore(initState);
    const newpProps: IBasketStripProps = {
      ...props,
      basket: {
        ...props.basket,
        loading: true
      }
    };
    const component = componentWrapper(newpProps, newStore);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when open is false', () => {
    const newpProps: IBasketStripProps = {
      ...props,
      open: false
    };
    const component = componentWrapper(newpProps, store);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when basketImages is undefiend', () => {
    const newpProps: IBasketStripProps = {
      ...props,
      basketImages: undefined
    };
    const component = componentWrapper(newpProps, store);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when basketItems is undefiend', () => {
    const newpProps: IBasketStripProps = {
      ...props,
      basketItems: undefined
    };
    const component = componentWrapper(newpProps, store);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when showItemCount is undefiend', () => {
    const newProps: IBasketStripProps = {
      ...props,
      showItemCount: undefined
    };
    newProps.basketImages = ['1', '2', '3', '4', '5', '6'];
    const component = componentWrapper(newProps, store);
    expect(component).toMatchSnapshot();
  });

  test('component should render properly, when basket loading is on ', () => {
    const newBasket = { ...props.basket };
    newBasket.loading = true;
    newBasket.showBasketSideBar = true;
    const newProps: IBasketStripProps = {
      ...props,
      basket: newBasket
    };
    delete newProps.basketItems;

    const component = componentWrapper(newProps, store);
    expect(component).toMatchSnapshot();
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketStrip {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('BasketStrip').instance() as BasketStrip).onCloseDrawer();
    (component.find('BasketStrip').instance() as BasketStrip).onStripClick();
    (component
      .find('BasketStrip')
      .instance() as BasketStrip).handleClearBasket();
    (component
      .find('BasketStrip')
      .instance() as BasketStrip).handleProceedToCheckout();
    (component.find('BasketStrip').instance() as BasketStrip).scrollDetect();
    (component
      .find('BasketStrip')
      .instance() as BasketStrip).componentWillUnmount();
  });
});
