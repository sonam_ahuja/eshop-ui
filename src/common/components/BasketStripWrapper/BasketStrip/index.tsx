import React, { ReactNode } from 'react';
import { Button, Loader } from 'dt-components';
import PreloadImage from '@src/common/components/Preload';
import BasketWrap from '@basket/BasketWrap/index';
import EmptyBasket from '@basket/EmptyBasket/index';
import BasketHeader from '@basket/BasketHeader/index';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { IBasketState } from '@basket/store/types';
import EVENT_NAME from '@events/constants/eventName';
import { sendOnBasketLoadEvent, sendonBasketLoadTrigger } from '@events/basket';

import {
  StyledBasketFlowPanel,
  StyledBasketStrip,
  StyledDots,
  StyledImage,
  StyledImagesWrap,
  StyledVerticalText
} from './styles';
import Amount from './Amount';

export interface IProps {
  open?: boolean;
  currency: string;
  basket: IBasketState;
  basketImages?: string[];
  basketItems?: number;
  translation: IBasketTranslation;
  showItemCount?: boolean;
  clearBasketModalState: IBasketState['clearBasket'];
  setDeleteBasketModal(visibility: boolean): void;
  clearBasket(): void;
  redirectToBasket(): void;
  proceedToCheckout(): void;
  showBasketSideBar(payload: boolean): void;
}

export interface IState {
  open: boolean;
  scrollTop: number;
}

export class BasketStrip extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      open: !!props.open,
      scrollTop: 0
    };
    this.scrollDetect = this.scrollDetect.bind(this);
  }
  onCloseDrawer = (): void => {
    this.props.showBasketSideBar(false);
  }

  onStripClick = (): void => {
    sendOnBasketLoadEvent();
    sendonBasketLoadTrigger();
    this.props.showBasketSideBar(true);
  }

  handleClearBasket = (): void => {
    this.props.clearBasket();
  }

  componentWillUnmount(): void {
    this.props.showBasketSideBar(false);
  }

  handleProceedToCheckout = (): void => {
    this.props.proceedToCheckout();
    this.props.showBasketSideBar(false);
  }

  componentDidMount(): void {
    window.addEventListener('scroll', this.scrollDetect);
  }

  scrollDetect(): void {
    const scrollLimit = 126;
    const scrollTopPos =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;

    if (scrollTopPos < scrollLimit) {
      this.setState({ scrollTop: scrollTopPos });
    } else {
      this.setState({ scrollTop: scrollLimit });
    }
  }

  // tslint:disable-next-line:cognitive-complexity
  render(): ReactNode {
    const { basketImages, basketItems, basket, currency } = this.props;
    const { clearBasketModalState, redirectToBasket } = this.props;
    const { setDeleteBasketModal, showItemCount } = this.props;
    const { translation: basketTranslation } = this.props;

    return (
      <>
        {basket.showBasketSideBar ? (
          <StyledBasketFlowPanel
            isOpen={basket.showBasketSideBar}
            onClose={() => this.onCloseDrawer()}
            onBackdropClick={() => this.onCloseDrawer()}
          >
            {basket.loading ? (
              <div className='sidebarLoaderWrap'>
                <Loader open={true} />
              </div>
            ) : null}
            <div className='basketItems'>
              <div className='basketItemHeader-Count'>
                <BasketHeader
                  showBasketItemCount={showItemCount}
                  basketItemCount={basketItems}
                  enableClear={false}
                  clearBasket={this.handleClearBasket}
                  translation={basketTranslation}
                  clearBasketModalState={clearBasketModalState}
                  setDeleteBasketModal={setDeleteBasketModal}
                />
              </div>

              {!!basketItems ? (
                <>
                  <BasketWrap showMoreFlag={true} />
                  <Amount
                    basket={basket}
                    translation={basketTranslation}
                    currency={currency}
                  />
                </>
              ) : (
                <EmptyBasket translation={basketTranslation} />
              )}
            </div>
            {!!basketItems ? (
              <div className='buttonWrap'>
                <Button
                  type='complementary'
                  onClickHandler={() => redirectToBasket()}
                  data-event-id={EVENT_NAME.BASKET.EVENTS.VIEW_BASKET}
                  data-event-message={basketTranslation.viewBasket}
                >
                  {basketTranslation.viewBasket}
                </Button>
                <Button
                  className='button'
                  size='medium'
                  loading={false}
                  type='primary'
                  onClickHandler={() => this.handleProceedToCheckout()}
                  data-event-id={EVENT_NAME.BASKET.EVENTS.PROCEED_TO_CHECKOUT}
                  data-event-message={basketTranslation.proceedToCheckout}
                >
                  {basketTranslation.proceedToCheckout}
                </Button>
              </div>
            ) : null}
          </StyledBasketFlowPanel>
        ) : (
          <StyledBasketStrip
            scrollTop={this.state.scrollTop}
            onClick={() => this.onStripClick()}
          >
            {basketImages && basketImages.length > 0 ? (
              <StyledImagesWrap>
                {basketImages.map((imageUrl: string, index: number) => {
                  if (index < 4) {
                    return (
                      <StyledImage key={index}>
                        <PreloadImage
                          imageUrl={imageUrl}
                          imageHeight={10}
                          imageWidth={10}
                        />
                      </StyledImage>
                    );
                  } else {
                    return null;
                  }
                })}
              </StyledImagesWrap>
            ) : (
              <StyledVerticalText>
                {basketTranslation.startShopping}
              </StyledVerticalText>
            )}
            {basketImages && basketImages.length > 4 && (
              <StyledDots>
                <li />
                <li />
                <li />
              </StyledDots>
            )}
          </StyledBasketStrip>
        )}
      </>
    );
  }
}

export default BasketStrip;
