import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledTitleWrapper } from '@src/common/routes/basket/BasketHeader/styles';
import { StyledEmptyCard } from '@src/common/routes/basket/EmptyBasket/styles';
import {
  Card,
  CardContent,
  ProductName
} from '@src/common/routes/basket/BasketItemCard/styles';
import {
  ActionsWrap,
  DeleteIconWrap,
  ItemTotalPrice
} from '@src/common/routes/basket/BasketWrap/styles';

import { AppFlowPanel } from '../../FlowPanel/styles';
import { StyledInformationText } from '../../InformationText/styles';

import { StyledAmount, StyledMonthly, StyledTotalDue } from './Amount/styles';

export const StyledBasketFlowPanel = styled(AppFlowPanel)`
  .sidebarLoaderWrap > .dt_overlay {
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  /* position fixed with transfrom - bug hack */
  .dt_overlay .dt_overlay {
    justify-content: center;
    /* background: rgba(0,0,0,0) !important; */
    .dt_outsideClick {
      height: auto;
    }
  }
  .flowPanelInner {
    width: 34rem;
    background: ${colors.fogGray};
    /* position fixed with transfrom - bug hack */
    animation-fill-mode: none !important;

    .closeIcon {
      z-index: 10 !important;
    }

    .flowPanelContent {
      overflow: initial;
      display: flex;
      flex-direction: column;
    }
  }

  .basketItems {
    padding: 2.5rem 2rem 0;
    height: 100%;
    overflow-y: auto;
    ${StyledTitleWrapper} {
      margin-bottom: 1.6rem;
      .dt_title {
        font-size: 1.5rem;
        font-weight: 900;
        line-height: 1.17;
      }
    }
    .basketItemHeader-Count {
      & + {
        ${StyledEmptyCard} {
          margin-bottom: 5.8rem;
          margin-top: 15.4rem;
        }
      }
      & + {
        ${StyledInformationText} {
          align-items: flex-start;
        }
      }
    }
    .monthlyLabel,
    .upfrontLabel {
      margin-bottom: 0;
    }
  }
  .buttonWrap {
    display: flex;

    button {
      flex: 1;
      flex-shrink: 0;
      width: 50%;
    }
  }
  ${StyledAmount} {
    margin: 2rem 0 1.5rem;
    ${StyledMonthly} {
      align-items: flex-end;
    }
    ${StyledTotalDue} {
      color: ${colors.ironGray};
      align-items: flex-end;
    }
  }

  ${Card} {
    padding: 0.75rem;
    .productImage {
      min-width: 4rem;
      height: 4rem;
      width: 4rem;
      margin-right: 0.5rem;
    }
  }
  ${CardContent} {
    flex-basis: 11rem;
    max-width: 11rem;
    .dt_paragraph {
      font-size: 15px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.33;
    }
    ${ProductName} {
      word-break: break-word;
      margin-bottom: 0;

      .dt_paragraph {
        font-size: 0.875rem;
        font-weight: 500;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.43;
        letter-spacing: normal;
      }
    }
  }
  ${ItemTotalPrice} {
    padding: 0.5rem;
    .monthly-lists {
      padding-bottom: 1.25rem;
      .dt_title {
        font-size: 22.5px;
        font-weight: bold;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.11;
        letter-spacing: normal;
      }
    }
    .upfront-lists {
      padding-bottom: 2.7rem;
      .dt_title {
        font-size: 20px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
        letter-spacing: normal;
        text-align: right;
      }
    }

    ${ActionsWrap} {
      .dt_stepper {
        height: 20px;
        .stepperIconWrap {
          width: 20px;
          height: 20px;
          .dt_icon {
            font-size: 10px;
          }
        }
        .stepperValue {
          font-size: 15px;
          margin-left: 10px;
          margin-right: 10px;
        }
      }
    }
    ${DeleteIconWrap} {
      .dt_icon {
        font-size: 0.75rem;
      }
    }
  }
`;

// imagesWrap
export const StyledImage = styled.div`
  width: 3.75rem;
  height: 3.75rem;
  margin-top: 1rem;
`;
export const StyledImagesWrap = styled.div`
  flex: 1;
  overflow-y: auto;
`;

// Vertical text
export const StyledVerticalText = styled.p`
  font-weight: 900;
  color: ${colors.mediumGray};
  writing-mode: tb-rl;
  align-items: center;
  justify-content: center;
  font-size: 1.875rem;
  line-height: 1.07;
  letter-spacing: normal;
  padding-top: 0.5rem;
`;

// three dots
export const StyledDots = styled.ul`
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 1.5rem;

  li {
    width: 0.3rem;
    height: 0.3rem;
    border-radius: 0.3rem;
    background: ${colors.shadowGray};
    margin: 0 0.125rem;
  }
`;

// Main Strip Wrapper
export const StyledBasketStrip = styled.div.attrs({ id: 'basketStrip' })<{
  scrollTop: number;
}>`
  display: none;
  position: sticky;
  flex-shrink: 0;
  top: 0;
  right: 0;
  flex-direction: column;
  align-items: center;
  background: ${colors.cloudGray};
  letter-spacing: -1.2px;
  height: 100vh;
  padding-bottom: 6.25rem;
  padding-top: 3rem;
  transition: all 1s ease;
  transition: padding 0.2s linear 0s;
  cursor: pointer;
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: flex;
    width: 5.75rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    width: 4.75rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 5.75rem;
    display: none;
  }
`;
