import basketActions from '@basket/store/actions';
import { RootState } from '@src/common/store/reducers';
import { Dispatch } from 'redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import React, { ReactNode } from 'react';
import { commonAction } from '@store/actions';
import { connect } from 'react-redux';
import BasketStrip from '@src/common/components/BasketStripWrapper/BasketStrip';
import { IBasketItem, IBasketState } from '@basket/store/types';
import { ITranslationState } from '@store/types/translation';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps {
  showBasketIcon?: boolean;
  isBasketRoute?: boolean;
  showItemCount?: boolean;
}
export interface IComponentDispatchToProps {
  setDeleteBasketModal(visibility: boolean): void;
  clearBasket(): void;
  showBasketSideBar(payload: boolean): void;
  possibleNextRouteWhenSummaryButtonClick(): void;
  routeToLoginFromBasket(status: boolean): void;
}

export interface IComponentStateToProps {
  currency: string;
  isUserLoggedIn: boolean;
  basketItems: number;
  translation: ITranslationState;
  basket: IBasketState;
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps<IRouterParams>;

// tslint:disable
export class BasketStripWrapper extends React.Component<IComponentProps> {
  constructor(props: IComponentProps) {
    super(props);
  }

  getBasketImages = (): string[] | (string | undefined)[] => {
    const { basketItems } = this.props.basket;
    if (basketItems && basketItems.length) {
      return basketItems
        .map((item: IBasketItem) => {
          return item.product.imageUrl;
        })
        .filter(url => url);
    }

    return [];
  };

  redirectToBasket = (): void => {
    this.props.history.push('/basket');
  };

  proceedToCheckout = (): void => {
    this.props.possibleNextRouteWhenSummaryButtonClick();
  };

  render(): ReactNode {
    const { translation, clearBasket, currency, basket } = this.props;

    return (
      <BasketStrip
        basket={basket}
        basketItems={basket.pagination.total}
        basketImages={this.getBasketImages() as string[]}
        translation={translation.cart.basket}
        clearBasketModalState={basket.clearBasket}
        clearBasket={clearBasket}
        setDeleteBasketModal={this.props.setDeleteBasketModal}
        redirectToBasket={this.redirectToBasket}
        proceedToCheckout={this.proceedToCheckout}
        currency={currency}
        showItemCount={true}
        showBasketSideBar={this.props.showBasketSideBar}
      />
    );
  }
}

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  isUserLoggedIn: state.common.isLoggedIn,
  basketItems: state.basket.pagination.total,
  currency: state.common.currency,
  translation: state.translation,
  basket: state.basket
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  clearBasket(): void {
    dispatch(basketActions.clearBasket());
  },
  setDeleteBasketModal(visibility: boolean): void {
    dispatch(basketActions.setDeleteBasketModal(visibility));
  },
  showBasketSideBar(payload: boolean): void {
    dispatch(basketActions.showBasketSideBar(payload));
  },
  possibleNextRouteWhenSummaryButtonClick(): void {
    dispatch(basketActions.validateCartPrice());
  },

  routeToLoginFromBasket(status: boolean): void {
    dispatch(commonAction.routeToLoginFromBasket(status));
  }
});

export default withRouter(
  connect<IComponentStateToProps, IComponentDispatchToProps, IProps, RootState>(
    mapStateToProps,
    mapDispatchToProps
  )(BasketStripWrapper)
);
