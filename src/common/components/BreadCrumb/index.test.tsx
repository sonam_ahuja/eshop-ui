import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import AppBreadCrumb, { IProps } from '@common/components/BreadCrumb';
import appState from '@store/states/app';

describe('<AppBreadCrumb />', () => {
  const props: IProps = {
    translation: appState().translation
  };

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <AppBreadCrumb {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
