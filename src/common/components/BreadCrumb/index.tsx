import React, { FunctionComponent } from 'react';
import { BreadcrumbItem, Section } from 'dt-components';
import { ITranslationState } from '@store/types/translation';
import { Link } from 'react-router-dom';
import EVENT_NAME from '@events/constants/eventName';

import { StyledBreadCrumb } from './styles';

export interface IProps {
  translation: ITranslationState;
}

const AppBreadCrumb: FunctionComponent<IProps> = (props: IProps) => {
  const { basket: basketTranslation } = props.translation.cart;

  return (
    <StyledBreadCrumb>
      <BreadcrumbItem>
        <Section
          size='large'
          weight='ultra'
          data-event-id={EVENT_NAME.COMMON.EVENTS.BREAD_CRUMB_CLICK}
          data-event-message={basketTranslation.home}
          data-event-path={'cart.basket.home'}
        >
          <Link to='/'>{basketTranslation.home}</Link>
        </Section>
      </BreadcrumbItem>
      <BreadcrumbItem active={true}>
        <Section size='large' weight='ultra'>
          {basketTranslation.pageHeading}
        </Section>
      </BreadcrumbItem>
    </StyledBreadCrumb>
  );
};

export default AppBreadCrumb;
