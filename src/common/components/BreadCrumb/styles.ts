import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { Breadcrumb } from 'dt-components';

export const StyledBreadCrumb = styled(Breadcrumb)`
  padding: 1.25rem 0 1rem;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  li {
    color: ${colors.mediumGray};
    &.active {
      color: ${colors.darkGray};
    }
  }
  li > div {
    display: flex;
    align-items: center;
  }
  i {
    font-size: 0.75rem !important;
    margin: 0 0.5rem;
  }
`;
