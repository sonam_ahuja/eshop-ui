import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'dt-components';
import { IProps as IBreadcrumbProps } from 'dt-components/lib/es/components/atoms/breadcrumb';
import { Link } from 'react-router-dom';
import EVENT_NAME from '@events/constants/eventName';

import { StyledBreadcrumbWrap } from './styles';

interface IBreadCrumbItem {
  link?: string;
  active: boolean;
  title: string;
  name?: string;
}
interface IProps extends IBreadcrumbProps {
  breadCrumbItems: IBreadCrumbItem[];
}

export default (props: IProps) => {
  const { breadCrumbItems, onBreadcrumbItemClick, ...rest } = props;

  return (
    <StyledBreadcrumbWrap>
      <Breadcrumb {...rest}>
        {props.breadCrumbItems.map((item, index) => {
          return (
            <BreadcrumbItem key={index} active={item.active}>
              <div className='text'>
                {item.link ? (
                  <Link
                    to={item.link}
                    onClick={() =>
                      onBreadcrumbItemClick && onBreadcrumbItemClick(item)
                    }
                    data-event-id={EVENT_NAME.COMMON.EVENTS.BREAD_CRUMB_CLICK}
                    data-event-message={item.title}
                  >
                    {item.title}
                  </Link>
                ) : (
                  <span className='currentPage'>{item.title}</span>
                )}
              </div>
            </BreadcrumbItem>
          );
        })}
      </Breadcrumb>
    </StyledBreadcrumbWrap>
  );
};
