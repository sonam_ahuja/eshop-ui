import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledBreadcrumbWrap = styled.div`
  /* padding: 0rem 0 4rem 0; */
  /* vaishali */
  /* padding: 1rem 0 2.5rem 0; */
  /* RESPONSIVE_CATEGORY_LANDING_PAGE */
  padding: 0.75rem 0 2.35rem 0;

  .folder-wrap {
    border-radius: 8px;
    min-width: 5rem;
    z-index: 101;
    box-shadow: 0 0.2rem 1rem 0 rgba(0, 0, 0, 0.2);
    li a,
    li .currentPage {
      font-size: 14px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.43;
      letter-spacing: normal;
      padding: 0.25rem 0.75rem;
      display: block;
    }

    li a:hover {
      background: ${colors.fogGray};
    }
  }

  .dt_breadcrumb {
    align-items: center;

    > .dt_icon {
      transform: scale(0.8);
      margin: 0 0.4rem;
      font-size: 0.938rem;
    }

    > .dt_breadcrumbItem {
      color: ${colors.mediumGray};
      /* text-transform: capitalize; */
      &.active {
        color: ${colors.darkGray};
      }

      .text {
        display: flex;
        align-items: center;
        font-size: 0.75rem;
        line-height: 1rem;
        font-weight: 900;
        letter-spacing: normal;
      }
    }
  }
`;
