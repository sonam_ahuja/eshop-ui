import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { DialogBox } from 'dt-components';

export const DialogBoxApp = styled(DialogBox)`
  /* common dialogBox styling START */
  .dialogBoxContentWrap {
    padding: 3.25rem 1.25rem 2rem;

    .dialogBoxHeader {
      padding-bottom: 2rem;
      .imageWrap {
        img {
          bottom: auto;
        }
      }
    }
    .dialogBoxBody {
      padding-bottom: 2.5rem;
      .dt_title {
        max-width: 400px;
        word-break: break-word;
      }
    }

    .dialogBoxFooter {
      .dt_button {
        width: 100%;
        /* text-transform: capitalize; */
      }

      .dt_button + .dt_button {
        margin-top: 0.75rem;
      }
    }
  }
  .closeIcon {
    z-index: 1;
    @keyframes fadeIn {
      from {
        opacity: 0;
      }
      to {
        opacity: 1;
      }
    }
    animation-name: fadeIn;
    animation-duration: 100ms;
    animation-delay: 400ms;
    animation-fill-mode: both;
  }
  /* common dialogBox styling END */

  /* flexibleHeight dialogBox styling START */
  &.dt_dialogBox.flexibleHeight {
    .dt_overlay {
      align-items: flex-end;
      /* overflow: auto; */
      overflow: initial; /* Global fixes for position on bottom */
    }

    .dt_outsideClick {
      /* margin: 0; */
      margin-bottom: 0;
      width: 100%;
      margin-top: inherit;
    }

    .dialogBoxContentWrap {
      border-radius: 0.5rem 0.5rem 0 0;
      /*********** Added for auto scroll when exceeds the limit *************/
      overflow-y: auto;
      max-height: 80vh;

      /*********** Added for auto scroll when exceeds the limit *************/
    }
  }
  /* flexibleHeight dialogBox styling END */

  /* fullHeight dialogBox styling START */
  &.dt_dialogBox.fullHeight {
    .dt_overlay {
      align-items: flex-end;
      overflow: auto;
    }

    .dt_outsideClick {
      margin: 0;
      width: 100%;
      height: 100%;

      .dialogBoxContentWrap {
        width: 100%;
        height: 100%;

        .dialogBoxContent {
          display: flex;
          flex-direction: column;
          flex: 1;
          height: 100%;
        }
      }
    }
  }
  /* fullHeight dialogBox styling END */
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .dt_outsideClick {
      position: relative;
    }

    /* flexibleHeight dialogBox styling START */
    &.dt_dialogBox.flexibleHeight {
      .dt_overlay {
        align-items: center;
        padding: 1rem 0;
      }

      .dt_outsideClick {
        margin: auto;
        width: auto;
      }
      .closeIcon {
        top: 1rem;
        right: 1rem;
      }
      .dialogBoxContentWrap {
        padding: 3.25rem 2rem 2rem;
        border-radius: 0;
        width: 22.5rem;
        padding: 3.25rem 2rem 2rem;
        /*********** Added for auto scroll when exceeds the limit *************/
        overflow-y: auto;
        max-height: 94vh;
        /*********** Added for auto scroll when exceeds the limit *************/
        .dialogBoxHeader {
          margin-top: 2rem;
        }

        .dialogBoxFooter .dt_button {
          width: auto;
        }
      }
    }
    /* flexibleHeight dialogBox styling END */

    /* flexibleHeight dialogBox styling START */
    &.dt_dialogBox.fullHeight {
      .dt_overlay {
        align-items: center;
        padding: 1rem 0;
      }

      .dt_outsideClick {
        margin: auto;
        width: auto;
        height: auto;
      }

      .dt_overlay .dt_outsideClick .dialogBoxContentWrap {
        padding: 3.25rem 2rem 2rem;
        width: 22.5rem;
        .dialogBoxHeader {
          margin-top: 2rem;
        }

        .dialogBoxFooter .dt_button {
          width: auto;
        }
      }
    }
    /* flexibleHeight dialogBox styling END */

    .dialogBoxFooter {
      .dt_button + .dt_button {
        margin-left: 0.75rem;
      }
    }

    &.dt_dialogBox {
      .closeIcon {
        right: 1rem;
        top: 1rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    /* flexibleHeight dialogBox styling START */
    &.dt_dialogBox.flexibleHeight {
      .closeIcon {
        top: 2rem;
        right: 2rem;
      }
      .dialogBoxContentWrap {
        width: 30rem;
        .dialogBoxHeader {
          width: 18.75rem;
        }
      }
    }
    /* flexibleHeight dialogBox styling END */

    /* flexibleHeight dialogBox styling START */
    &.dt_dialogBox.fullHeight {
      .dt_overlay .dt_outsideClick .dialogBoxContentWrap {
        width: 30rem;
        .dialogBoxHeader {
          width: 18.75rem;
        }
      }
    }
    /* flexibleHeight dialogBox styling END */

    &.dt_dialogBox {
      .closeIcon {
        right: 2rem;
        top: 2rem;
      }
    }
  }
`;
