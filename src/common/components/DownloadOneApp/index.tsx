import React from 'react';
import { PreloadImage } from 'dt-components';
import { Link } from 'react-router-dom';

import {
  StyledDownloadOneApp,
  StyledHeading,
  StyledImageWrap,
  StyledSubHeading,
  StyledTextWrap
} from './styles';

interface IProps {
  className?: string;
  heading?: string;
  subHeading?: string;
  downloadText?: string;
  downloadLink?: string;
}

const DownloadOneApp = (props: IProps) => {
  const { heading, subHeading, downloadText, downloadLink, className } = props;

  return (
    <StyledDownloadOneApp className={className}>
      <StyledTextWrap>
        <StyledHeading>{heading}</StyledHeading>
        <StyledSubHeading>
          <Link to={downloadLink || ''} className='linkText'>
            {downloadText}{' '}
          </Link>
          {subHeading}
        </StyledSubHeading>
      </StyledTextWrap>
      <StyledImageWrap>
        <PreloadImage
          imageUrl='https://i.ibb.co/Fmwnj4m/Screenshot-2019-09-06-at-1-09-02-PM.png'
          imageWidth={10}
          imageHeight={10}
        />
      </StyledImageWrap>
    </StyledDownloadOneApp>
  );
};

export default DownloadOneApp;
