import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Title } from 'dt-components';

// ImageWrapper
export const StyledImageWrap = styled.div`
  height: 2.25rem;
  width: 2.25rem;
  background: ${colors.magenta};
  border-radius: 0.25rem;
  padding: 0.5625rem;
`;

// TextWrapper
export const StyledHeading = styled(Title).attrs({
  weight: 'bold'
})`
  color: ${colors.darkGray};
  font-size: 0.875rem;
  line-height: 1.25rem;
  letter-spacing: 0;
  @media (min-width: ${breakpoints.desktop}px) {
    font-size: 1rem;
    line-height: 1.5rem;
  }
`;
export const StyledSubHeading = styled(Title).attrs({
  weight: 'medium'
})`
  color: ${colors.ironGray};
  font-size: 0.875rem;
  line-height: 1.25rem;
  letter-spacing: 0;
  margin-left: 0;
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    margin-left: 2px;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    font-size: 1rem;
    line-height: 1.5rem;
    margin-left: 2px;
  }
`;
export const StyledTextWrap = styled.div`
  display: flex;
  flex-direction: column;
  .linkText {
    &:hover {
      text-decoration: underline;
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    flex-direction: row;
  }
`;

// MainWrapper
export const StyledDownloadOneApp = styled.div`
  background: ${colors.silverGray};
  position: fixed;
  z-index: 100;
  left: 0;
  right: 0;
  bottom: 1.75rem;
  margin: 0 1.25rem;
  box-shadow: 0 9px 45px 0 rgba(0, 0, 0, 0.5);
  border-radius: 0.5rem;
  height: 3.75rem;
  margin: 0 1.25rem;
  padding: 0.75rem 0.75rem 0.75rem 1.5rem;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    margin: 0 2.25rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    margin: 0 3rem;
    right: 5.75rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    bottom: 1.75rem;
    margin: 0 4.9rem;
    right: 4.75rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    margin: 0 5.5rem;
    right: 5.75rem;
  }
`;
