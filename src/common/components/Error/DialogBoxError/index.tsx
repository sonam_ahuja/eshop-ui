import React from 'react';
import FullPageError, { IProps } from '@common/components/Error/FullPageError';

import { StyledDialogBoxError } from './styles';

const DialogBoxError = (props: IProps) => {
  const { bodyClassName } = props;

  return (
    <StyledDialogBoxError
      bodyClassName={bodyClassName}
      isOpen
      showCloseButton={false}
    >
      <FullPageError {...props} />
    </StyledDialogBoxError>
  );
};

export default DialogBoxError;
