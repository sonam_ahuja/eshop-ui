import styled from 'styled-components';
import {
  StyledActions,
  StyledFullPageError,
  StyledMainContentWrapper
} from '@common/components/Error/FullPageError/styles';
import { breakpoints } from '@src/common/variables';

import { AppFlowPanel } from '../../FlowPanel/styles';

export const StyledDialogBoxError = styled(AppFlowPanel)`
  .flowPanelInner .flowPanelContent {
    display: flex;
    flex-direction: column;
  }
  ${StyledFullPageError} {
    flex-direction: column;
    overflow-y: auto;
    @media (min-width: ${breakpoints.desktop}px) {
      /* width: 28.5rem; */
      width: 28.8rem;
      ${StyledMainContentWrapper} {
        padding: 3rem 4.5rem;
      }
      ${StyledActions} {
        display: flex;
      }
    }
  }
`;
