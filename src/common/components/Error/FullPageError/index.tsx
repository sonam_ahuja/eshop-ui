import React, { ReactNode } from 'react';
import { Button } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import SvgErrorGeneral from '@common/components/Svg/error-general';
import EVENT_NAME from '@events/constants/eventName';

import {
  StyledActions,
  StyledCode,
  StyledDescription,
  StyledFullPageError,
  StyledHeading,
  StyledImageWrapper,
  StyledMainContentWrapper,
  StyledMessage
} from './styles';

export interface IProps {
  className?: string;
  code: string;
  message: string;
  description: string;
  svgNode?: ReactNode;
  leftButtonText?: string;
  rightButtonText?: string;
  rightButtonIcon?: iconNamesType;
  bodyClassName?: string;
  rightPath?: string;
  leftPath?: string;
  leftButtonAction?(): void;
  rightButtonAction?(): void;
}

const FullPageError = (props: IProps) => {
  const {
    className,
    code,
    message,
    description,
    svgNode,
    leftButtonText,
    rightButtonText,
    rightButtonIcon,
    rightButtonAction,
    leftButtonAction,
    leftPath,
    rightPath
  } = props;

  const svgEl = svgNode ? svgNode : <SvgErrorGeneral />;
  const leftButtonEl = leftButtonText && (
    <Button
      className='simpleButton'
      data-event-id={EVENT_NAME.COMMON.EVENTS.ERROR_EVENT}
      data-event-message={leftButtonText}
      data-event-path={leftPath}
      onClickHandler={() => leftButtonAction && leftButtonAction()}
    >
      {leftButtonText}
    </Button>
  );
  const rightButtonEl = rightButtonText && (
    <Button
      isIconRight={!!rightButtonIcon}
      iconName={rightButtonIcon}
      data-event-id={EVENT_NAME.COMMON.EVENTS.ERROR_EVENT}
      data-event-message={rightButtonText}
      data-event-path={rightPath}
      type='complementary'
      size='medium'
      onClickHandler={() => rightButtonAction && rightButtonAction()}
    >
      {rightButtonText}
    </Button>
  );

  return (
    <StyledFullPageError className={className}>
      <StyledImageWrapper>{svgEl}</StyledImageWrapper>
      <StyledMainContentWrapper>
        <div className='inner'>
          <div className='info'>
            <StyledHeading>
              <StyledMessage>{message}</StyledMessage>
              <StyledCode>{code}</StyledCode>
            </StyledHeading>
            <StyledDescription>{description}</StyledDescription>
          </div>

          <StyledActions>
            {leftButtonEl}
            {rightButtonEl}
          </StyledActions>
        </div>
      </StyledMainContentWrapper>
    </StyledFullPageError>
  );
};

export default FullPageError;
