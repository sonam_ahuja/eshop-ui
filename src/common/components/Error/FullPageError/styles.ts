import styled from 'styled-components';
import { Paragraph, Title } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

// ImageWrapper
export const StyledImageWrapper = styled.div`
  font-size: 14.5rem;
  line-height: 0;
  display: flex;
  flex: 1;
  flex-basis: 50%;
  flex-shrink: 0;
  padding: 0.25rem;
  align-items: center;
  justify-content: center;

  svg {
    width: 1em;
    height: 1em;
  }
`;

// Main Text
export const StyledMainContentWrapper = styled.div`
  background: ${colors.coldGray};
  padding: 2rem 1.25rem 2.1875rem;

  display: flex;
  flex-direction: column;
  flex: 1;
  flex-basis: 50%;
  flex-shrink: 0;

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2rem 4.9rem 2rem 4.75rem;
    justify-content: center;
  }
`;
export const StyledHeading = styled.div`
  color: ${colors.mediumGray};
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;
export const StyledMessage = styled(Title).attrs({
  size: 'large',
  weight: 'ultra',
  transform: 'capitalize'
})``;
export const StyledCode = styled(Paragraph).attrs({
  size: 'medium',
  weight: 'bold'
})`
  padding-left: 0.5rem;
`;
export const StyledDescription = styled(Paragraph).attrs({
  size: 'large',
  weight: 'normal'
})`
  color: ${colors.mediumGray};
  margin-top: 1rem;
`;
export const StyledActions = styled.div`
  display: flex;
  flex: 1;
  margin: 2.25rem -0.5rem 0;
  padding: 0 0;

  > button {
    flex: 1;
    flex-shrink: 0;
    margin: 0 0.5rem;
  }

  .simpleButton {
    background: none;
    color: ${colors.darkGray};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    display: block;
  }
`;

// Main Wrapper
export const StyledFullPageError = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  /* height: 100%; */
  flex: 1;
  background: ${colors.white};

  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: row;
  }
`;
