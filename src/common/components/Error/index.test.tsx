import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { RootState } from '@common/store/reducers';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import Error, { ErrorPage } from '@common/components/Error/index';
import { IProps } from '@common/components/Error//types';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@common/components/Error/mapProps';

// tslint:disable-next-line:no-big-function
describe('<ErrorPage />', () => {
  const mockStore = configureStore();
  const props: IProps = {
    globalTranslation: appState().translation.cart.global,
    common: appState().common,
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    location: {
      pathname: '/basket',
      search: 'code=URLUtils',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    },
    removeGenricError: jest.fn()
  };
  const initStateValue: RootState = appState();
  initStateValue.common.error.httpStatusCode = 500;
  initStateValue.common.error.showFullPageError = true;
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Error {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const wrapper = componentWrapper(props);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly with show full page error false', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 440;
    state.common.error.showFullPageError = false;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    wrapper
      .find('button')
      .at(1)
      .simulate('click');
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly with error code 404', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 404;
    state.common.error.showFullPageError = true;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly with error code 400', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 400;
    let localStore = mockStore(state);
    let wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    wrapper.find('button').simulate('click');
    expect(wrapper).toMatchSnapshot();
    state.common.error.httpStatusCode = 404;
    localStore = mockStore(state);
    wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    wrapper.find('button').simulate('click');
    expect(wrapper).toMatchSnapshot();
    state.common.error.httpStatusCode = 500;
    localStore = mockStore(state);
    wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    wrapper
      .find('button')
      .at(0)
      .simulate('click');
    expect(wrapper).toMatchSnapshot();
    wrapper
      .find('button')
      .at(1)
      .simulate('click');
    expect(wrapper).toMatchSnapshot();
    state.common.error.httpStatusCode = 408;
    localStore = mockStore(state);
    wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    wrapper
      .find('button')
      .at(1)
      .simulate('click');
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly without http status code', () => {
    const state: RootState = appState();
    delete state.common.error.httpStatusCode;
    delete state.common.error.message;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('should render properly with error code 502', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 502;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    wrapper
      .find('button')
      .at(1)
      .simulate('click');
    expect(wrapper).toMatchSnapshot();
  });

  test('call component will unmount ', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 502;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wrapper.find('ErrorPage').instance() as ErrorPage).componentWillUnmount();
    (wrapper
      .find('ErrorPage')
      .instance() as ErrorPage).componentWillReceiveProps(props);
    const newProps = { ...props };
    newProps.common.error.httpStatusCode = '500';
    newProps.common.error.showFullPageError = true;
    (wrapper
      .find('ErrorPage')
      .instance() as ErrorPage).componentWillReceiveProps(newProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('left button action when show full page error', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 500;
    state.common.error.showFullPageError = true;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    try {
      (wrapper.find('ErrorPage').instance() as ErrorPage).leftButtonAction();
    } catch (err) {
      // tslint:disable-next-line:no-console
      console.log(err);
    }
  });

  test('Update Http status with unkonown error code', () => {
    const state: RootState = appState();
    state.common.error.httpStatusCode = 499;
    delete state.common.error.message;
    const localStore = mockStore(state);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={localStore}>
            <Error />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('dispatch action test', () => {
    const dispatch = jest.fn();
    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.removeGenricError();
  });
});
