import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '@src/common/store/reducers';
import { RouteComponentProps, withRouter } from 'react-router';
import FullPageError, {
  IProps as IErrorPageProps
} from '@src/common/components/Error/FullPageError';
import DialogBoxError from '@common/components/Error/DialogBoxError';
import SvgErrorTimeOut from '@common/components/Svg/error-time-out';
import { updateQueryParam } from '@src/common/utils/parseQuerySrting';
import {
  IMapDispatchToProps,
  IMapStateToProps,
  IProps,
  IState
} from '@common/components/Error/types';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@common/components/Error/mapProps';
import { sendErrorEvent } from '@src/common/events/common';

export class ErrorPage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      style: { display: 'auto' }
    };
    this.removeErrorInURL = this.removeErrorInURL.bind(this);
  }

  componentWillUnmount(): void {
    this.props.removeGenricError();
  }

  componentDidMount(): void {
    const { error } = this.props.common;
    if (!error.skipGenericError) {
      if (error && error.httpStatusCode) {
        sendErrorEvent(error.httpStatusCode, this.getUserMessage());
      }
      document.body.classList.add('hasErrorFlowPanel');
    }
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const {
      httpStatusCode,
      showFullPageError,
      skipGenericError
    } = nextProps.common.error;
    const { error } = this.props.common;
    if (
      !skipGenericError &&
      (httpStatusCode !== error.httpStatusCode ||
        showFullPageError !== error.showFullPageError)
    ) {
      const style = {
        display: !(httpStatusCode && showFullPageError) ? 'auto' : 'none'
      };
      sendErrorEvent(httpStatusCode, this.getUserMessage());
      this.setState({ style });
    }
  }

  getUserMessage = () => {
    const { error } = this.props.common;

    return error.message ? error.message.userMessage || '' : '';
  }
  leftButtonAction = () => {
    const { showFullPageError } = this.props.common.error;
    if (showFullPageError) {
      this.props.history.goBack();
    } else {
      this.removeErrorInURL();
      this.props.removeGenricError();
    }
  }

  removeErrorInURL(): void {
    const newURL = updateQueryParam(this.props.location.search, true);
    this.props.history.replace({
      pathname: this.props.history.location.pathname,
      search: newURL
    });
  }

  rightButtonAction = () => {
    const { httpStatusCode } = this.props.common.error;
    switch (httpStatusCode) {
      case 404:
        this.props.history.push('/');
        this.props.removeGenricError();
        break;
      case 500:
      case 408:
      case 502:
        this.removeErrorInURL();
        window.location.reload();
        break;
      default:
        this.props.removeGenricError();
    }
  }

  getSvgImage = (payload: IErrorPageProps) => {
    const { httpStatusCode } = this.props.common.error;
    if (httpStatusCode === 408 || httpStatusCode === 440) {
      payload.svgNode = <SvgErrorTimeOut />;
    }
  }

  getButtonTexts = (payload: IErrorPageProps) => {
    const { globalTranslation, common } = this.props;
    const errorTranslation = globalTranslation.errors;
    const { httpStatusCode } = common.error;

    switch (httpStatusCode) {
      case 404:
        payload.rightButtonText = errorTranslation.backToHomePage;
        payload.rightPath = 'cart.global.errors.backToHomePage';
        break;
      case 500:
      case 408:
      case 502:
        payload.rightButtonText = errorTranslation.retry;
        payload.rightPath = 'cart.global.errors.retry';
        payload.rightButtonIcon = 'ec-reload';
        payload.leftButtonText = errorTranslation.back;
        payload.leftPath = 'cart.global.errors.back';
        break;
      case 440:
        payload.rightButtonText = errorTranslation.confirm;
        payload.rightPath = 'cart.global.errors.confirm';
        payload.leftButtonText = errorTranslation.cancel;
        payload.leftPath = 'cart.global.errors.cancel';
        break;
      // case 400:
      //   payload.rightButtonText = errorTranslation.back;
      //   payload.path = 'global.errors.back';
      //   break;
      default:
        payload.rightButtonText = errorTranslation.back;
        payload.rightPath = 'cart.global.errors.back';
    }
  }
  getErrorProps = () => {
    const { common, globalTranslation } = this.props;
    const { httpStatusCode, message = {} } = common.error;

    const errorTranslation = globalTranslation.errors;

    const payload: IErrorPageProps = {
      code: httpStatusCode as string,
      description:
        message.userMessage ||
        errorTranslation[`description${httpStatusCode}`] ||
        errorTranslation.default,
      message: errorTranslation[httpStatusCode as string],
      rightButtonText: '',
      leftButtonText: '',
      leftButtonAction: this.leftButtonAction,
      rightButtonAction: this.rightButtonAction
    };

    this.getButtonTexts(payload);
    this.getSvgImage(payload);

    return payload;
  }

  render(): React.ReactNode {
    const {
      httpStatusCode,
      showFullPageError,
      skipGenericError
    } = this.props.common.error;

    if (skipGenericError) {
      return null;
    }

    return (
      <>
        {httpStatusCode && showFullPageError ? (
          <FullPageError {...this.getErrorProps()} />
        ) : null}
        {httpStatusCode && !showFullPageError ? (
          <DialogBoxError
            {...this.getErrorProps()}
            bodyClassName='hasErrorFlowPanel'
          />
        ) : null}
      </>
    );
  }
}

export default withRouter(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(ErrorPage)
);
