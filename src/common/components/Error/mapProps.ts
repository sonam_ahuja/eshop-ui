import commonAction from '@common/store/actions/common';
import { Dispatch } from 'redux';
import { RootState } from '@src/common/store/reducers';
import {
  IMapDispatchToProps,
  IMapStateToProps
} from '@common/components/Error/types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  globalTranslation: state.translation.cart.global,
  common: state.common
});
export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  removeGenricError(): void {
    dispatch(commonAction.removeGenricError());
  }
});
