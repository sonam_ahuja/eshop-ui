import React from 'react';
import { ICommonState } from '@common/store/types/common';
import { IGlobalTranslation } from '@common/store/types/translation';
import { RouteComponentProps } from 'react-router';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export interface IMapStateToProps {
  globalTranslation: IGlobalTranslation;
  common: ICommonState;
}
export interface IMapDispatchToProps {
  removeGenricError(): void;
}
export interface IState {
  style: React.CSSProperties;
}

export type IProps = RouteComponentProps<IRouterParams> &
  IMapStateToProps &
  IMapDispatchToProps;
