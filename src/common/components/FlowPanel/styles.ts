import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { FlowPanel } from 'dt-components';

export const AppFlowPanel = styled(FlowPanel)`
  .dt_outsideClick,
  .flowPanelInner {
    height: 100%;
    width: 100%;
  }
  .dt_overlay .dt_outsideClick .flowPanelInner {
    padding: 0;

    .closeIcon {
      position: absolute;
      top: 1.25rem;
      right: 1.25rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .dt_overlay {
      align-items: flex-start;
      justify-content: flex-end;

      .dt_outsideClick {
        margin: 0 !important;
        width: auto;
      }
    }
    .dt_overlay .dt_outsideClick .flowPanelInner .closeIcon {
      top: 1rem;
      right: 1rem;
    }
  }
`;
