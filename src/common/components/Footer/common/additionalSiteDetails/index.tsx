import React from 'react';
import { IFooterTranslation } from '@src/common/store/types/translation';
import { IFooter as IFooterConfiguaration } from '@src/common/store/types/configuration';
import { Icon } from 'dt-components';
import EVENT_NAME from '@events/constants/eventName';

import {
  StyledAdditionalDetailsForSite,
  StyledAppListWrap,
  StyledFooterAppListsWrap,
  StyledStoreList
} from './styles';

export interface IProps {
  footerConfiguration: IFooterConfiguaration;
  footerTranslation: IFooterTranslation;
  pageName: string;
}

// This function returns element which as contact us/ store locator / newsletter subscription links

export const AdditionalDetailsForSite = (props: IProps) => {
  const { appStoreLink, playStoreLink } = props.footerConfiguration;

  return (
    <StyledAdditionalDetailsForSite>
      <div className='para-txt'>{props.footerTranslation.downloadAppText}</div>
      <StyledFooterAppListsWrap>
        <StyledAppListWrap>
          <a
            data-event-id={EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS}
            data-event-message={playStoreLink}
            href={decodeURIComponent(playStoreLink)}
          >
            <StyledStoreList>
              <div className='storeIconWrap'>
                <Icon
                  className='icons'
                  color='currentColor'
                  name='ec-google-play'
                />
              </div>
            </StyledStoreList>
          </a>
        </StyledAppListWrap>
        <StyledAppListWrap>
          <a
            data-event-id={EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS}
            data-event-message={appStoreLink}
            href={decodeURIComponent(appStoreLink)}
          >
            <StyledStoreList>
              <div className='storeIconWrap'>
                <Icon
                  className='icons'
                  color='currentColor'
                  name='ec-app-store'
                />
              </div>
            </StyledStoreList>
          </a>
        </StyledAppListWrap>
      </StyledFooterAppListsWrap>
    </StyledAdditionalDetailsForSite>
  );
};

export default AdditionalDetailsForSite;
