import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledAdditionalDetailsForSite = styled.div`
  .para-txt {
    font-size: 1rem;
    line-height: 1.25rem;
    color: ${colors.mediumGray};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .para-txt {
      font-size: 0.875rem;
    }
  }
`;

export const StyledAppListWrap = styled.div`
  a {
    position: relative;
    border-radius: 0.5rem;
    height: 3.75rem;
    width: 100%;
    background: ${colors.zBlack};
    font-weight: bold;
    display: flex;
    margin-bottom: 0.25rem;
    border: 1px solid ${colors.shadowGray};
    opacity: 1;
    transition: all 0.5s ease;
    &:focus {
      box-shadow: none;
      outline: none;
    }
    &:hover {
      opacity: 0.8;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    a {
      height: 3rem;
    }
  }
`;

export const StyledStoreList = styled.div`
  width: 100%;
  .storeIconWrap {
    .icons {
      font-size: 3rem;
      width: 100%;
      color: ${colors.mediumGray};
      svg {
        width: 65%;
        height: 3.75rem;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .storeIconWrap {
      .icons {
        svg {
          width: 100%;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .storeIconWrap {
      .icons {
        svg {
          width: 60%;
          height: 3rem;
        }
      }
    }
  }
`;

export const StyledFooterAppListsWrap = styled.div`
  margin-top: 1rem;
`;
