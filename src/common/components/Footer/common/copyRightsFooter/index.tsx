import React from 'react';
import { IGlobalTranslation } from '@src/common/store/types/translation';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import EVENT_NAME from '@events/constants/eventName';

import { StyledCopyRightsFooter } from './styles';

const date = () => {
  return new Date().getFullYear();
};

export interface IProps {
  globalTranslation: IGlobalTranslation;
  termsAndConditionsUrl: string;
  pageName: string;
}

// static links related to site liek about us, contacts and cookies etc

export const CopyRightsFooter = (props: IProps) => {
  return (
    <StyledCopyRightsFooter>
      <ul>
        <li>
          <span>
            &copy; {date()} {props.globalTranslation.companyName}
          </span>
        </li>
        <li>
          <HTMLTemplate
            template={`<a data-event-id="${
              EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
            }"
              data-event-message="${props.globalTranslation.termsAndConditions}"
              data-event-message="cart.global.termsAndConditions"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt" >{name}</a>`}
            templateData={{
              name: props.globalTranslation.termsAndConditions,
              link: decodeURIComponent(props.termsAndConditionsUrl)
            }}
            className='termsConditionTitle'
          />
        </li>
      </ul>
    </StyledCopyRightsFooter>
  );
};

export default CopyRightsFooter;
