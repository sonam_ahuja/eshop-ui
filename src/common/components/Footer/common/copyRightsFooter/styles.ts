import { breakpoints, colors } from '@src/common/variables';
import styled from 'styled-components';

export const StyledCopyRightsFooter = styled.div`
  background: ${colors.black};
  height: 3rem;
  padding: 1rem 1.25rem;
  position: relative;
  ul {
    display: flex;
    justify-content: space-between;
    align-items: center;
    li {
      color: ${colors.mediumGray};
      span {
        font-size: 0.75rem;
        line-height: 1rem;
      }
      .link-txt {
        font-size: 0.75rem;
        line-height: 1rem;
        color: ${colors.ironGray};
        text-decoration: none;
        &:hover,
        &:focus {
          text-decoration: underline;
        }
      }
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-left: 2.25rem;
    padding-right: 2.25rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-left: 3rem;
    padding-right: 3rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding-left: 4.9rem;
    padding-right: 4.9rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-left: 5.5rem;
    padding-right: 5.5rem;

    &:after {
      content: '';
      position: absolute;
      height: 100%;
      top: 0;
      left: -9999px;
      right: -9999px;
      margin: auto;
      z-index: -1;
      height: 100%;
      background: inherit;
      max-width: 100vw;
    }
  }
`;
