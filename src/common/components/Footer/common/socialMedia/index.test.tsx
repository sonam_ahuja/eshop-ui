import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import SocialMediaFooterLinks, {
  IProps
} from '@common/components/Footer/common/socialMedia';
import appState from '@store/states/app';

describe('<SocialMediaFooterLinks />', () => {
  const props: IProps = {
    footerConfiguration: appState().configuration.cms_configuration.global
      .footer,
    pageName: 'home'
  };
  props.footerConfiguration.socialMedia.facebook.show = false;
  const componentWrapper = (newpProps: IProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SocialMediaFooterLinks {...newpProps} />
        </ThemeProvider>
      </StaticRouter>
    );

  test('component should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
