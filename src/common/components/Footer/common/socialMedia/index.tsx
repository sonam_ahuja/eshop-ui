import React from 'react';
import { Icon } from 'dt-components';
import { IFooter as IFooterConfiguaration } from '@src/common/store/types/configuration';
import { SOCIAL_MEDIA_ICON } from '@src/common/store/enums';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import EVENT_NAME from '@events/constants/eventName';

import { StyledSocialMediaFooterLinks } from './styles';

export interface IProps {
  footerConfiguration: IFooterConfiguaration;
  pageName: string;
}
// social media links for the site

export const SocialMediaFooterLinks = (props: IProps) => {
  const socialMedias = props.footerConfiguration.socialMedia;

  return (
    <StyledSocialMediaFooterLinks>
      <ul>
        {Object.keys(socialMedias).map((socialMediaName, index: number) => {
          if (socialMedias[socialMediaName].show) {
            return (
              <li key={index}>
                <div className='iconsWrap'>
                  <a
                    data-event-id={EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS}
                    data-event-message={socialMediaName}
                    data-event-page-name={props.pageName}
                    href={decodeURIComponent(socialMedias[socialMediaName].link)}
                  >
                    <Icon
                      className='icons'
                      color='currentColor'
                      data-event-id={EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS}
                      data-event-message={socialMediaName}
                      data-event-page-name={props.pageName}
                      name={
                        SOCIAL_MEDIA_ICON[
                          socialMedias[socialMediaName].key
                        ] as iconNamesType
                      }
                    />
                  </a>
                </div>
              </li>
            );
          }

          return null;
        })}
      </ul>
    </StyledSocialMediaFooterLinks>
  );
};

export default SocialMediaFooterLinks;
