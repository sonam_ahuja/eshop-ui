import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSocialMediaFooterLinks = styled.div`
  margin-top: 0.9375rem;
  li {
    margin-right: 1.75rem;
    display: inline-block;
    &:last-child {
      margin-right: 0rem;
    }
    .iconsWrap {
      i {
        font-size: 1.25rem;
        color: ${colors.mediumGray};
        opacity: 1;
        transition: all 1s ease;
        svg {
          path {
            fill: ${colors.mediumGray};
          }
        }
        &:hover {
          opacity: 0.6;
        }
      }
    }
    a {
      margin-bottom: 0;
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    margin-top: 1.875rem;
    text-align: left;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 0;
    li {
      margin-right: 1.5rem;
    }
  }
`;
