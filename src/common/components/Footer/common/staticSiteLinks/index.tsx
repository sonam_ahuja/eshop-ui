import React from 'react';
import { IFooter as IFooterConfiguaration } from '@src/common/store/types/configuration';
import { IFooterTranslation } from '@src/common/store/types/translation';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import EVENT_NAME from '@events/constants/eventName';

import { StyledStaticSiteLinks } from './styles';

export interface IProps {
  footerConfiguration: IFooterConfiguaration;
  footerTranslation: IFooterTranslation;
  pageName: string;
}

// static links related to site liek about us, contacts and cookies etc

export const StaticSiteLinks = (props: IProps) => {
  const {
    aboutUs,
    contact,
    privacyPolicy,
    cookies,
    support,
    secure
  } = props.footerConfiguration.footerStaticLinks;

  return (
    <StyledStaticSiteLinks>
      <ul>
        {props.footerConfiguration.footerStaticLinks.aboutUs && (
          <li>
            <HTMLTemplate
              template={`<a data-event-id="${
                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
              }"
              data-event-message="${props.footerTranslation.aboutUs}"
              data-event-path="cart.global.footer.aboutUs"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt">{name}</a>`}
              templateData={{
                name: props.footerTranslation.aboutUs,
                link: decodeURIComponent(aboutUs)
              }}
              className='footerStaticLists'
            />
          </li>
        )}
        {props.footerConfiguration.footerStaticLinks.contact && (
          <li>
            <HTMLTemplate
              template={`<a data-event-id="${
                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
              }"
              data-event-message="${props.footerTranslation.contact}"
              data-event-path="cart.global.footer.contact"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt">{name}</a>`}
              templateData={{
                name: props.footerTranslation.contact,
                link: decodeURIComponent(contact)
              }}
              className='footerStaticLists'
            />
          </li>
        )}
        {props.footerConfiguration.footerStaticLinks.privacyPolicy && (
          <li>
            <HTMLTemplate
              template={`<a data-event-id="${
                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
              }"
              data-event-message="${props.footerTranslation.privacyPolicy}"
              data-event-path="cart.global.footer.privacyPolicy"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt">{name}</a>`}
              templateData={{
                name: props.footerTranslation.privacyPolicy,
                link: decodeURIComponent(privacyPolicy)
              }}
              className='footerStaticLists'
            />
          </li>
        )}
        {props.footerConfiguration.footerStaticLinks.cookies && (
          <li>
            <HTMLTemplate
              template={`<a data-event-id="${
                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
              }"
              data-event-message="${props.footerTranslation.cookies}"
              data-event-path="cart.global.footer.cookies"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt">{name}</a>`}
              templateData={{
                name: props.footerTranslation.cookies,
                link: decodeURIComponent(cookies)
              }}
              className='footerStaticLists'
            />
          </li>
        )}
        {props.footerConfiguration.footerStaticLinks.support && (
          <li>
            <HTMLTemplate
              template={`<a data-event-id="${
                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
              }"
              data-event-message="${props.footerTranslation.support}"
              data-event-path="cart.global.footer.support"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt">{name}</a>`}
              templateData={{
                name: props.footerTranslation.support,
                link: decodeURIComponent(support)
              }}
              className='footerStaticLists'
            />
          </li>
        )}
        {props.footerConfiguration.footerStaticLinks.secure && (
          <li>
            <HTMLTemplate
              template={`<a data-event-id="${
                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
              }"
              data-event-message="${props.footerTranslation.secure}"
              data-event-path="cart.global.footer.secure"
              data-event-page-name="${
                props.pageName
              }" href={link} class="link-txt">{name}</a>`}
              templateData={{
                name: props.footerTranslation.secure,
                link: decodeURIComponent(secure)
              }}
              className='footerStaticLists'
            />
          </li>
        )}
      </ul>
    </StyledStaticSiteLinks>
  );
};

export default StaticSiteLinks;
