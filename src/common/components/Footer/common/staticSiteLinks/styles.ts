import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledStaticSiteLinks = styled.div`
  li {
    margin-right: 1.5rem;
    opacity: 1;
    transition: all 1s ease;
    display: inline-block;
    margin-bottom: 1rem;
    min-width: 3.25rem;
    text-transform: uppercase;
    a {
      color: ${colors.warmGray};
      font-size: 0.75rem;
      line-height: 1rem;
      margin-bottom: 0.75rem;
      font-weight: bold;
      /* text-transform: uppercase; */
    }
    &:last-child {
      margin-right: 0rem;
    }
    &:hover {
      opacity: 0.6;
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    li {
      margin-bottom: 0.625rem;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    li {
      margin-right: 2.5rem;
      margin-bottom: 0;
    }
  }
`;
