import React from 'react';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import {
  Footer,
  IComponentProps,
  IComponentStateToProps,
  mapStateToProps
} from '@common/components/Footer';
import { RootState } from '@src/common/store/reducers';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { RouteComponentProps } from 'react-router-dom';
import { IMenusCategory } from '@src/common/store/types/common';
export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export type IRouteProps = RouteComponentProps<IRouterParams>;

// tslint:disable-next-line:no-big-function
describe('<Footer />', () => {
  const newMegaMenu: IMenusCategory[] = [
    {
      id: '2',
      currentNodeMetaData: {
        type: 'type',
        tags: ['tages'],
        distanceFromResolution: 23,
        data: {
          title: 'title',
          description: 'description',
          openNewTab: false,
          deeplink: '/'
        }
      },
      childNodes: [
        {
          id: '1',
          currentNodeMetaData: {
            type: 'type',
            tags: ['tages'],
            distanceFromResolution: 23,
            data: {
              title: 'title',
              description: 'description',
              openNewTab: false,
              deeplink: '/'
            }
          },
          childNodes: [
            {
              id: '2',
              currentNodeMetaData: {
                type: 'type',
                tags: ['tages'],
                distanceFromResolution: 23,
                data: {
                  title: 'title',
                  description: 'description',
                  openNewTab: false,
                  deeplink: '/'
                }
              },
              childNodes: [
                {
                  id: '4',
                  currentNodeMetaData: {
                    type: 'type',
                    tags: ['tages'],
                    distanceFromResolution: 23,
                    data: {
                      title: 'title',
                      description: 'description',
                      openNewTab: false,
                      deeplink: '/'
                    }
                  },
                  childNodes: []
                }
              ]
            }
          ]
        },
        {
          id: '2',
          currentNodeMetaData: {
            type: 'type',
            tags: ['tages'],
            distanceFromResolution: 23,
            data: {
              title: 'title',
              description: 'description',
              openNewTab: false,
              deeplink: '/'
            }
          },
          childNodes: [
            {
              id: '2',
              currentNodeMetaData: {
                type: 'type',
                tags: ['tages'],
                distanceFromResolution: 23,
                data: {
                  title: 'title',
                  description: 'description',
                  openNewTab: false,
                  deeplink: '/'
                }
              },
              childNodes: [
                {
                  id: '4',
                  currentNodeMetaData: {
                    type: 'type',
                    tags: ['tages'],
                    distanceFromResolution: 23,
                    data: {
                      title: 'title',
                      description: 'description',
                      openNewTab: false,
                      deeplink: '/'
                    }
                  },
                  childNodes: []
                }
              ]
            }
          ]
        },
        {
          id: '3',
          currentNodeMetaData: {
            type: 'type',
            tags: ['tages'],
            distanceFromResolution: 23,
            data: {
              title: 'title',
              description: 'description',
              openNewTab: false,
              deeplink: '/'
            }
          },
          childNodes: [
            {
              id: '2',
              currentNodeMetaData: {
                type: 'type',
                tags: ['tages'],
                distanceFromResolution: 23,
                data: {
                  title: 'title',
                  description: 'description',
                  openNewTab: false,
                  deeplink: '/'
                }
              },
              childNodes: [
                {
                  id: '4',
                  currentNodeMetaData: {
                    type: 'type',
                    tags: ['tages'],
                    distanceFromResolution: 23,
                    data: {
                      title: 'title',
                      description: 'description',
                      openNewTab: false,
                      deeplink: '/'
                    }
                  },
                  childNodes: []
                }
              ]
            }
          ]
        },
        {
          id: '4',
          currentNodeMetaData: {
            type: 'type',
            tags: ['tages'],
            distanceFromResolution: 23,
            data: {
              title: 'title',
              description: 'description',
              openNewTab: false,
              deeplink: '/'
            }
          },
          childNodes: [
            {
              id: '2',
              currentNodeMetaData: {
                type: 'type',
                tags: ['tages'],
                distanceFromResolution: 23,
                data: {
                  title: 'title',
                  description: 'description',
                  openNewTab: false,
                  deeplink: '/'
                }
              },
              childNodes: [
                {
                  id: '4',
                  currentNodeMetaData: {
                    type: 'type',
                    tags: ['tages'],
                    distanceFromResolution: 23,
                    data: {
                      title: 'title',
                      description: 'description',
                      openNewTab: false,
                      deeplink: '/'
                    }
                  },
                  childNodes: []
                }
              ]
            }
          ]
        },
        {
          id: '5',
          currentNodeMetaData: {
            type: 'type',
            tags: ['tages'],
            distanceFromResolution: 23,
            data: {
              title: 'title',
              description: 'description',
              openNewTab: false,
              deeplink: '/'
            }
          },
          childNodes: [
            {
              id: '2',
              currentNodeMetaData: {
                type: 'type',
                tags: ['tages'],
                distanceFromResolution: 23,
                data: {
                  title: 'title',
                  description: 'description',
                  openNewTab: false,
                  deeplink: '/'
                }
              },
              childNodes: [
                {
                  id: '4',
                  currentNodeMetaData: {
                    type: 'type',
                    tags: ['tages'],
                    distanceFromResolution: 23,
                    data: {
                      title: 'title',
                      description: 'description',
                      openNewTab: false,
                      deeplink: '/'
                    }
                  },
                  childNodes: []
                }
              ]
            }
          ]
        }
      ]
    }
  ];
  const props: IComponentProps = {
    megaMenuFooter: newMegaMenu,
    pageName: 'home',
    ...histroyParams,
    termsAndConditionsUrl: '',
    footerConfiguration: appState().configuration.cms_configuration.global
      .footer,
    footerTranslation: appState().translation.cart.global.footer,
    globalTranslation: appState().translation.cart.global
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Footer {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with search params includes footer1', () => {
    const newHistoryParams: IRouteProps = { ...histroyParams };
    newHistoryParams.location.search = 'footer1';
    const newProps: IComponentProps = { ...props, ...newHistoryParams };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Footer {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with search params includes footer2', () => {
    const newHistoryParams: IRouteProps = { ...histroyParams };
    newHistoryParams.location.search = 'footer2';
    newHistoryParams.location.key = 'wse1';
    const newProps: IComponentProps = { ...props, ...newHistoryParams };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Footer {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('passing props via update component', () => {
    const initStateValue: RootState = appState();
    const newHistoryParams: IRouteProps = { ...histroyParams };
    const newProps: IComponentProps = { ...props, ...newHistoryParams };
    const component = shallow(<Footer {...newProps} />);
    component.setProps({ ...initStateValue });
    component.update();
    component.setState({ isFooterVisible: false });
    component.update();
    expect(component).toMatchSnapshot();
    component.setProps({ location: { search: '?footer=hide' } });
    component.update();
    expect(component).toMatchSnapshot();
  });

  test('action trigger for render component with bank value ', () => {
    const newProps = { ...props };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Footer {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );

    newProps.location.search = '?footer=hide';
    (component
      .find('Footer')
      .instance() as Footer).hideHeaderFooterOnQueryStringKey();
  });

  test('mapStateToProps test', () => {
    const initStateValue: RootState = appState();
    const expected: IComponentStateToProps = {
      footerConfiguration: appState().configuration.cms_configuration.global
        .footer,
      footerTranslation: appState().translation.cart.global.footer,
      globalTranslation: appState().translation.cart.global,
      termsAndConditionsUrl: appState().configuration.cms_configuration.global
        .termsAndConditionsUrl,
        megaMenuFooter: appState().common.megaMenuFooter
    };
    expect(mapStateToProps(initStateValue)).toEqual(expected);
  });
  // tslint:disable-next-line:max-file-line-count
});
