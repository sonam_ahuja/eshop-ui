import {
  StyledFooter,
  StyledFooterOthersWrapper,
  StyledFooterPanel,
  StyledFooterWrap,
  StyledListsWrap,
  StyledTextList,
  WrapperPanel
} from '@src/common/components/Footer/styles';
import {
  IFooterTranslation,
  IGlobalTranslation
} from '@src/common/store/types/translation';
import React, { ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { RootState } from '@src/common/store/reducers';
import { IFooter as IFooterConfiguaration } from '@src/common/store/types/configuration';
import { connect } from 'react-redux';
import {
  getValueFromLocalStorage,
  removeItemFromLocalStorage,
  setValueInLocalStorage
} from '@src/common/utils/localStorage';
import appConstants from '@src/common/constants/appConstants';
import { IMenusCategory } from '@src/common/store/types/common';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import {
  IParsedQueryString,
  parseQueryString
} from '@src/common/utils/parseQuerySrting';
import EVENT_NAME from '@events/constants/eventName';

import { Column, Row } from '../Grid/styles';

import { FooterMobile } from './mobile';
import StaticSiteLinks from './common/staticSiteLinks';
import SocialMediaFooterLinks from './common/socialMedia';
import CopyRightsFooter from './common/copyRightsFooter';
import AdditionalDetailsForSite from './common/additionalSiteDetails';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export interface IComponentStateToProps {
  footerConfiguration: IFooterConfiguaration;
  footerTranslation: IFooterTranslation;
  globalTranslation: IGlobalTranslation;
  termsAndConditionsUrl: string;
  megaMenuFooter: IMenusCategory[];
}

export interface IProps {
  isBasketRoute?: boolean;
  pageName: string;
  className?: string;
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  RouteComponentProps<IRouterParams>;

export interface IState {
  footer: number;
  isFooterVisible: boolean;
}

export class Footer extends React.Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      footer: 0,
      isFooterVisible: true
    };
  }
  private basketStripWidth = 0;

  componentDidMount(): void {
    if (this.props.location.search.includes('footer1')) {
      removeItemFromLocalStorage(appConstants.FOOTER);
      this.setState({
        footer: 0
      });
    } else if (
      this.props.location.search.includes('footer2') ||
      getValueFromLocalStorage(appConstants.FOOTER)
    ) {
      setValueInLocalStorage(appConstants.FOOTER, 'footer2');
      this.setState({
        footer: 1
      });
    }
    this.hideHeaderFooterOnQueryStringKey();

    const basketStrip = document.getElementById('basketStrip');
    basketStrip
      ? (this.basketStripWidth = basketStrip.clientWidth)
      : (this.basketStripWidth = 0);
  }

  componentWillReceiveProps(nextProps: IComponentProps): void {
    if (nextProps.location.search !== this.props.location.search) {
      this.hideHeaderFooterOnQueryStringKey();
    }
  }

  hideHeaderFooterOnQueryStringKey(): void {
    const parsedUrl: IParsedQueryString = parseQueryString(
      this.props.location.search
    );

    if (parsedUrl.footer === 'hide') {
      this.setState({ isFooterVisible: false });
    }
  }

  // Menu items for footer
  getMenuItemsElement(): ReactNode {
    const { footerConfiguration, footerTranslation, pageName } = this.props;
    const { megaMenuFooter } = this.props;

    return (
      <StyledFooterWrap>
        <Row className='row'>
          <div className='columnLeftWrap'>
            {megaMenuFooter && megaMenuFooter.length
              ? megaMenuFooter.map((item, index) => {
                  if (index < 3) {
                    return (
                      <Column colTabletPortrait={4} key={`main-${index}`}>
                        <HTMLTemplate
                          className='head-title footerHeadTitles'
                          template={`<a  data-event-id="${
                            EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
                          }"
                      data-event-message="${
                        item.currentNodeMetaData.data.title
                      }"
                      data-event-page-name="${pageName}" class="link-txt" href={link}>{name}</a>`}
                          key={`data-event${index}`}
                          templateData={{
                            name: item.currentNodeMetaData.data.title,
                            link: item.currentNodeMetaData.data.deeplink
                          }}
                          sameTabOnDifferentDomain={
                            !item.currentNodeMetaData.data.openNewTab
                          }
                        />
                        {item.childNodes &&
                        Array.isArray(item.childNodes) &&
                        item.childNodes.length ? (
                          <StyledListsWrap key={`child${index}`}>
                            {item.childNodes.map((subchild, idx) => {
                              return (
                                <StyledTextList key={`${index}${idx}`}>
                                  <HTMLTemplate
                                    className='lists footerListTitles'
                                    template={`<a
                                data-event-id="${
                                  EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
                                }"
                                data-event-message="${
                                  subchild.currentNodeMetaData.data.title
                                }"
                                data-event-page-name="${pageName}"
                                class="link-txt" href={link}>{name}</a>`}
                                    templateData={{
                                      name:
                                        subchild.currentNodeMetaData.data.title,
                                      link:
                                        subchild.currentNodeMetaData.data
                                          .deeplink
                                    }}
                                    sameTabOnDifferentDomain={
                                      !subchild.currentNodeMetaData.data
                                        .openNewTab
                                    }
                                  />
                                </StyledTextList>
                              );
                            })}
                          </StyledListsWrap>
                        ) : null}
                      </Column>
                    );
                  }

                  return null;
                })
              : null}
          </div>
          <div className='columnRightWrap'>
            <Column colTabletPortrait={12}>
              <AdditionalDetailsForSite
                pageName={pageName}
                footerConfiguration={footerConfiguration}
                footerTranslation={footerTranslation}
              />
            </Column>
          </div>
        </Row>
      </StyledFooterWrap>
    );
  }

  render(): ReactNode {
    const { footerConfiguration, footerTranslation, pageName } = this.props;
    const { globalTranslation } = this.props;
    const { termsAndConditionsUrl, isBasketRoute, className } = this.props;

    return (
      <>
        {this.state.isFooterVisible ? (
          <StyledFooterPanel className={className}>
            {!this.state.footer ? (
              <WrapperPanel>
                <div className='desktop-wrapper'>
                  {isBasketRoute !== true && (
                    <StyledFooter pageHasbasketStrip={this.basketStripWidth}>
                      {this.getMenuItemsElement()}
                      <StyledFooterOthersWrapper>
                        <StaticSiteLinks
                          footerConfiguration={footerConfiguration}
                          footerTranslation={footerTranslation}
                          pageName={pageName}
                        />
                        <SocialMediaFooterLinks
                          pageName={pageName}
                          footerConfiguration={footerConfiguration}
                        />
                      </StyledFooterOthersWrapper>
                    </StyledFooter>
                  )}
                  <CopyRightsFooter
                    termsAndConditionsUrl={termsAndConditionsUrl}
                    globalTranslation={globalTranslation}
                    pageName={pageName}
                  />
                </div>
                <div className='mobile-wrapper'>
                  <FooterMobile {...this.props} />
                </div>
              </WrapperPanel>
            ) : null}
          </StyledFooterPanel>
        ) : null}
      </>
    );
  }
}

// tslint:disable:max-file-line-count

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  footerConfiguration: state.configuration.cms_configuration.global.footer,
  footerTranslation: state.translation.cart.global.footer,
  globalTranslation: state.translation.cart.global,
  termsAndConditionsUrl:
    state.configuration.cms_configuration.global.termsAndConditionsUrl,
  megaMenuFooter: state.common.megaMenuFooter
});

export default withRouter(
  connect<IComponentStateToProps, undefined, IProps, RootState>(
    mapStateToProps,
    undefined
  )(Footer)
);
