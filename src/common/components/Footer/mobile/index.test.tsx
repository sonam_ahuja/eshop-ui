import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import {
  FooterMobile,
  IComponentProps
} from '@common/components/Footer/mobile';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { RouteComponentProps } from 'react-router-dom';
export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export type IRouteProps = RouteComponentProps<IRouterParams>;

describe('<FooterMobile />', () => {
  const props: IComponentProps = {
    megaMenuFooter: appState().common.megaMenuFooter,
    isBasketRoute: true,
    pageName: 'home',
    ...histroyParams,
    termsAndConditionsUrl: '',
    footerConfiguration: appState().configuration.cms_configuration.global
      .footer,
    footerTranslation: appState().translation.cart.global.footer,
    globalTranslation: appState().translation.cart.global
  };
  const componentWrapper = (newProps: IComponentProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <FooterMobile {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
