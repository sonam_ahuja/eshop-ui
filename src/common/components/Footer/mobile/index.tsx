import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  StyledAccordionWrap,
  StyledFooterCopyrightMobile,
  StyledFooterMobile,
  StyledFooterMobilePanel,
  StyledFooterOthersWrap,
  StyledFooterWrap,
  StyledListItem,
  StyledListWrap,
  StyledNewsletterWrap
} from '@src/common/components/Footer/mobile/styles';
import { Accordion } from 'dt-components';
import { IFooter as IFooterConfiguaration } from '@src/common/store/types/configuration';
import {
  IFooterTranslation,
  IGlobalTranslation
} from '@src/common/store/types/translation';
import { IMenusCategory } from '@src/common/store/types/common';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import EVENT_NAME from '@events/constants/eventName';

import StaticSiteLinks from '../common/staticSiteLinks';
import CopyRightsFooter from '../common/copyRightsFooter';
import SocialMediaFooterLinks from '../common/socialMedia';
import AdditionalDetailsForSite from '../common/additionalSiteDetails';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IComponentStateToProps {
  footerConfiguration: IFooterConfiguaration;
  footerTranslation: IFooterTranslation;
  globalTranslation: IGlobalTranslation;
  termsAndConditionsUrl: string;
  megaMenuFooter: IMenusCategory[];
}

export interface IProps {
  isBasketRoute?: boolean;
  pageName: string;
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  RouteComponentProps<IRouterParams>;

export const FooterMobile = (props: IComponentProps) => {
  const { footerConfiguration, footerTranslation, pageName } = props;
  const { globalTranslation, termsAndConditionsUrl } = props;
  const { megaMenuFooter, isBasketRoute } = props;

  return (
    <StyledFooterMobilePanel>
      {isBasketRoute !== true && (
        <StyledFooterMobile>
          <StyledFooterWrap>
            <StyledAccordionWrap>
              {megaMenuFooter && megaMenuFooter.length
                ? megaMenuFooter.map((item, index) => {
                    if (index < 3) {
                      return (
                        <div className='cols-md-12' key={index}>
                          {item.childNodes &&
                          Array.isArray(item.childNodes) &&
                          item.childNodes.length ? (
                            <Accordion
                              className='accordion-title'
                              isOpen={false}
                              headerTitle={
                                <HTMLTemplate
                                  template={`<a data-event-id="${
                                    EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
                                  }"
                      data-event-message="${
                        item.currentNodeMetaData.data.title
                      }"
                      data-event-page-name="${pageName}" href={link} className="link-txt">{name}</a>`}
                                  templateData={{
                                    name: item.currentNodeMetaData.data.title,
                                    link: item.currentNodeMetaData.data.deeplink
                                  }}
                                  sameTabOnDifferentDomain={
                                    !item.currentNodeMetaData.data.openNewTab
                                  }
                                />
                              }
                            >
                              <section className='panel-body'>
                                <StyledListWrap>
                                  {item.childNodes &&
                                  Array.isArray(item.childNodes) &&
                                  item.childNodes.length
                                    ? item.childNodes.map(subchild => {
                                        return (
                                          <StyledListItem key={subchild.id}>
                                            <HTMLTemplate
                                              template={`<a data-event-id="${
                                                EVENT_NAME.FOOTER.EVENTS
                                                  .FOOTER_CLICKS
                                              }"
                      data-event-message="${
                        subchild.currentNodeMetaData.data.title
                      }"
                      data-event-page-name="${pageName}" href={link} className="link-txt">{name}</a>`}
                                              templateData={{
                                                name:
                                                  subchild.currentNodeMetaData
                                                    .data.title,
                                                link:
                                                  subchild.currentNodeMetaData
                                                    .data.deeplink
                                              }}
                                              sameTabOnDifferentDomain={
                                                !subchild.currentNodeMetaData
                                                  .data.openNewTab
                                              }
                                            />
                                          </StyledListItem>
                                        );
                                      })
                                    : null}
                                </StyledListWrap>
                              </section>
                            </Accordion>
                          ) : (
                            <HTMLTemplate
                              className='default-list'
                              template={`<a data-event-id="${
                                EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS
                              }"
                      data-event-message="${
                        item.currentNodeMetaData.data.title
                      }"
                      data-event-page-name="${pageName}" href={link} className="link-txt">{name}</a>`}
                              templateData={{
                                name: item.currentNodeMetaData.data.title,
                                link: item.currentNodeMetaData.data.deeplink
                              }}
                              sameTabOnDifferentDomain={
                                !item.currentNodeMetaData.data.openNewTab
                              }
                            />
                          )}
                        </div>
                      );
                    }

                    return null;
                  })
                : null}
            </StyledAccordionWrap>
            <StyledNewsletterWrap>
              <AdditionalDetailsForSite
                pageName={pageName}
                footerConfiguration={footerConfiguration}
                footerTranslation={footerTranslation}
              />
            </StyledNewsletterWrap>
          </StyledFooterWrap>
          <StyledFooterOthersWrap>
            <StaticSiteLinks
              footerConfiguration={footerConfiguration}
              footerTranslation={footerTranslation}
              pageName={pageName}
            />
            <SocialMediaFooterLinks
              pageName={pageName}
              footerConfiguration={footerConfiguration}
            />
          </StyledFooterOthersWrap>
        </StyledFooterMobile>
      )}
      <StyledFooterCopyrightMobile>
        <CopyRightsFooter
          pageName={pageName}
          termsAndConditionsUrl={termsAndConditionsUrl}
          globalTranslation={globalTranslation}
        />
      </StyledFooterCopyrightMobile>
    </StyledFooterMobilePanel>
  );
};

export default withRouter(FooterMobile);
