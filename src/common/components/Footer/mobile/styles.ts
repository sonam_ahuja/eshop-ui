import styled from 'styled-components';
import { colors } from '@src/common/variables';

import { Column } from '../../Grid/styles';
import { StyledAdditionalDetailsForSite } from '../common/additionalSiteDetails/styles';

// tslint:disable:max-file-line-count
export const StyledFooterMobilePanel = styled.div``;

export const StyledListWrap = styled.ul``;

export const StyledListItem = styled.li`
  color: ${colors.mediumGray};
  font-size: 1rem;
  line-height: 1.5rem;
  margin-bottom: 0.5rem;
  a {
    opacity: 1;
    transition: all 1s ease;
    display: inline-block;
    &:hover {
      opacity: 0.6;
    }
  }
  &:last-child {
    a {
      margin-bottom: 0;
    }
  }
`;

export const StyledAccordionWrap = styled.div`
  margin-bottom: 3.75rem;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  -webkit-tap-highlight-color: ${colors.transparent};
  &:empty {
    margin-bottom: 0;
  }
  .accordion-title {
    cursor: pointer;
    border-left: 1.25rem solid ${colors.transparent};
    border-right: 1.25rem solid ${colors.transparent};
    & > div {
      background: ${colors.transparent};
    }
    & > .accordionHeader {
      border-bottom: 1px solid ${colors.shadowGray};
      padding: 1.875rem 0rem;
      transition: none !important;
    }
    .accordionTitle {
      color: ${colors.cloudGray};
      font-size: 1.125rem;
      font-weight: bold;
      line-height: 1.25rem;
      margin-right: 1.25rem;
    }
    &.isOpen {
      border-left: 1.25rem solid ${colors.charcoalGray};
      border-right: 1.25rem solid ${colors.charcoalGray};
      & > div {
        background: ${colors.charcoalGray};
      }
      & > .accordionHeader {
        border-bottom: 1px solid ${colors.transparent};
      }
    }
    .panel-body {
      padding-bottom: 1.75rem;
    }
  }
  .default-list {
    border-bottom: 1px solid ${colors.shadowGray};
    padding: 1.875rem 0rem;
    border-left: 1.25rem solid ${colors.transparent};
    border-right: 1.25rem solid ${colors.transparent};
    color: ${colors.cloudGray};
    font-size: 1.125rem;
    font-weight: bold;
  }
`;

export const StyledNewsletterWrap = styled.div`
  margin: 0 1.25rem;
  ${Column} {
    padding: 0;
  }
`;

export const StyledFooterWrap = styled.div`
  padding-bottom: 2.5rem;
  .cols-md-12 {
    width: 100%;
  }
  h4 {
    color: ${colors.warmGray};
    font-size: 0.75rem;
    line-height: 1rem;
    margin-bottom: 0.75rem;
    font-weight: bold;
  }
  ${StyledAdditionalDetailsForSite} {
    .para-txt {
      font-size: 1rem;
      line-height: 1.5rem;
      max-width: 100%;
      .highllighted-txt {
        .span-txt {
          display: inline-block;
        }
        a {
          color: ${colors.white};
          font-weight: bold;
          font-size: 1rem;
          transition: all 1s ease;
          &:focus,
          &:hover {
            text-decoration: underline;
          }
        }
      }
      /* &::first-letter {
        text-transform: uppercase;
      } */
    }
  }
`;

export const StyledFooterOthersWrap = styled.div`
  border-top: 1px solid ${colors.shadowGray};
  padding: 1.25rem 1.25rem 0;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`;

export const StyledFooterMobile = styled.div`
  background: ${colors.darkGray};
  padding: 2.5rem 0rem 2.375rem;
`;

export const StyledFooterCopyrightMobile = styled.div``;
