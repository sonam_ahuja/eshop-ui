import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { Column, Row } from '../Grid/styles';
// tslint:disable:max-file-line-count

export const StyledListsWrap = styled.ul``;

export const StyledTextList = styled.li`
  color: ${colors.mediumGray};
  font-size: 1rem;
  line-height: 1.5rem;
  a {
    margin-bottom: 0.5rem;
    opacity: 1;
    transition: all 1s ease;
    display: inline-block;
    &:hover {
      opacity: 0.6;
    }
  }
  &:last-child {
    a {
      margin-bottom: 0;
    }
  }
`;

export const StyledFooterPanel = styled.div``;

export const StyledFooterOthersWrapper = styled.div`
  border-top: 1px solid ${colors.shadowGray};
  padding: 1.25rem 0 2.25rem;
  display: flex;
  align-items: center;
  flex-direction: column;
  align-items: end;
  justify-content: start;

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 1.5rem 0;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
`;

export const StyledFooterWrap = styled.div`
  .head-title {
    color: ${colors.warmGray};
    font-size: 0.75rem;
    line-height: 1rem;
    margin-bottom: 0.75rem;
    font-weight: bold;
    /* text-transform: uppercase; */
    .link-txt{
      color:inherit!important;
    }
  }

  ${Column} {
    margin-bottom: 2rem;
    padding: 0 0.625rem;
  }
  ${Row} {
    justify-content: space-between;
    .columnLeftWrap {
      width: 75%;
      display: flex;
      flex-wrap: wrap;
    }
    .columnRightWrap {
      width: 25%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${Row} {
      margin: 0 -0.5rem 0;
      ${Column} {
        padding: 0 0.5rem;
        margin-bottom: 2.95rem;
      }
    }
  }
`;

export const StyledFooter = styled.div<{ pageHasbasketStrip?: number }>`
  position: relative;
  background: ${colors.darkGray};
  padding: 2.5rem 2.25rem 0;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-left: 2.25rem;
    padding-right: 2.25rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-left: 3rem;
    padding-right: 3rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding-left: 4.9rem;
    padding-right: 4.9rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-left: 5.5rem;
    padding-right: 5.5rem;

    &:after {
      content: '';
      position: absolute;
      height: 100%;
      top: 0;
      left: -9999px;
      right: -9999px;
      margin: auto;
      z-index: -1;
      height: 100%;
      background: inherit;
      max-width: 100vw;
    }
  }
`;

export const WrapperPanel = styled.div`
  .mobile-wrapper {
    display: block;
  }
  .desktop-wrapper {
    display: none;
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .mobile-wrapper {
      display: none;
    }
    .desktop-wrapper {
      display: block;
    }
  }
`;
