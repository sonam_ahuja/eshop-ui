import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import Footer, { IProps } from '.';

describe('<Payment Footer />', () => {
  const props: IProps = {
    globalTranslation: appState().translation.cart.global,
    termsAndConditionsUrl: 'terms',
    shouldTermsAndConditionsOpenInNewTab: true
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Footer {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
