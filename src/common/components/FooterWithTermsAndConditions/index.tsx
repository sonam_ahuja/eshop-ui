import React, { FunctionComponent } from 'react';
import { Anchor } from 'dt-components';
import { IGlobalTranslation } from '@src/common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import appConstants from '@src/common/constants/appConstants';

import { StyledFooter } from './styles';

export interface IProps {
  globalTranslation: IGlobalTranslation;
  termsAndConditionsUrl: string;
  shouldTermsAndConditionsOpenInNewTab: boolean;
  className?: string;
}

const Footer: FunctionComponent<IProps> = (props: IProps) => {
  return (
    <StyledFooter className={props.className}>
      <div className='copyText'>
        &copy;{new Date().getFullYear()} {props.globalTranslation.companyName}{' '}
      </div>
      <Anchor
        type='secondary'
        hreflang={appConstants.LANGUAGE_CODE}
        title={props.globalTranslation.termsAndConditions}
        target={props.shouldTermsAndConditionsOpenInNewTab ? '_blank' : '_self'}
        href={props.termsAndConditionsUrl}
        data-event-id={EVENT_NAME.CHECKOUT.EVENTS.TERMS_CONDITION}
        data-event-message={props.globalTranslation.termsAndConditions}
      >
        {props.globalTranslation.termsAndConditions}
      </Anchor>
    </StyledFooter>
  );
};

export default Footer;
