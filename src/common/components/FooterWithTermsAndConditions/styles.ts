import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledFooter = styled.div`
  display: flex;
  width: 100%;
  font-size: 0.78125rem;
  line-height: 0.9375rem;
  letter-spacing: -0.2px;
  font-weight: normal;
  margin-top: auto;

  .copyText {
    font-size: inherit;
    letter-spacing: -0.2px;
    color: ${colors.mediumGray};
  }
  .dt_anchor {
    font-size: inherit;
    color: ${colors.ironGray};
    letter-spacing: 0px;
    &:hover {
      text-decoration: underline;
    }
  }

  &.darkBg {
    background: ${colors.charcoalGray};
    font-size: 0.75rem;
    line-height: 1rem;
    justify-content: space-between;
    align-items: center;
    height: 3rem;
    padding: 1rem 1.25rem;
    .copyText {
      letter-spacing: 0.24px;
    }

    @media (min-width: ${breakpoints.tabletPortrait}px) {
      padding: 1rem 2.25rem;
    }

    @media (min-width: ${breakpoints.tabletLandscape}px) {
      &.footerCheckout {
        display: none;
      }
    }
  }
`;
