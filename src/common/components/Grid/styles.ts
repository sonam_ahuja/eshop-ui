import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

interface IProps {
  colMobile?: number;
  colTabletPortrait?: number;
  colTabletLandscape?: number;
  colDesktop?: number;
  colDesktopLarge?: number;

  orderMobile?: number;
  orderTabletPortrait?: number;
  orderTabletLandscape?: number;
  orderDesktop?: number;
  orderDesktopLarge?: number;
}
const getWidthString = (span: number) => {
  const width = (span / 12) * 100;

  return `flex-basis: ${width}%; max-width: ${width}%`;
};

/** USED ON PROLONGATION */
export const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -0.75rem 0;
`;

/** USED ON PROLONGATION */
export const Column = styled.div<IProps>`
  input {
    width: 100%;
  }

  flex-basis: 100%;
  max-width: 100%;
  flex: 0 0 auto;
  padding: 0 0.75rem;
  flex-grow: 1;

  @media (max-width: ${breakpoints.mobile}px) {
    &.columns {
      &:first-child {
        .imgWrap {
          width: 50%;
        }
        .headingAndDecsWrap .description {
          width: 50%;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.mobile}px) {
    ${({ colMobile }) => (colMobile ? getWidthString(colMobile) : '')};

    order: ${({ orderMobile }) => (orderMobile ? orderMobile : '')};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    ${({ colTabletPortrait }) =>
      colTabletPortrait ? getWidthString(colTabletPortrait) : ''};

    order: ${({ orderTabletPortrait }) =>
      orderTabletPortrait ? orderTabletPortrait : ''};
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    ${({ colTabletLandscape }) =>
      colTabletLandscape ? getWidthString(colTabletLandscape) : ''};

    order: ${({ orderTabletLandscape }) =>
      orderTabletLandscape ? orderTabletLandscape : ''};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${({ colDesktop }) => (colDesktop ? getWidthString(colDesktop) : '')};

    order: ${({ orderDesktop }) => (orderDesktop ? orderDesktop : '')};
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    ${({ colDesktopLarge }) =>
      colDesktopLarge ? getWidthString(colDesktopLarge) : ''};

    order: ${({ orderDesktopLarge }) =>
      orderDesktopLarge ? orderDesktopLarge : ''};
  }
`;
