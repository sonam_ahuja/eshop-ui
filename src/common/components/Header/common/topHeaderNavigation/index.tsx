import React from 'react';
import { IHeaderTranslation } from '@src/common/store/types/translation';
import { IHeader } from '@src/common/store/types/configuration';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import EVENT_NAME from '@events/constants/eventName';

export interface IProps {
  headerConfig: IHeader;
  headerTranslation: IHeaderTranslation;
}

export const TopHeaderNavigation = (props: IProps) => {
  const { headerConfig, headerTranslation } = props;

  return (
    <ul className='menu-wrap list-inline'>
      {headerConfig.privateLink && (
        <li>
          <HTMLTemplate
            template={`<a class="nav-item active" href={link}  data-event-id="${
              EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
            }"
            data-event-message="${
              headerTranslation.privateText
            }" style="font-size:0.75rem;line-height:1rem;color:#a3a3a3;text-transform: uppercase;">{name}</a>`}
            templateData={{
              name: headerTranslation.privateText,
              link: headerConfig.privateLink
            }}
          />
        </li>
      )}
      {headerConfig.businessLink && (
        <li>
          <HTMLTemplate
            template={`<a class="nav-item" href={link} data-event-id="${
              EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
            }"
            data-event-message="${
              headerTranslation.businessText
            }" style="font-size:0.75rem;line-height:1rem;color:#a3a3a3;text-transform: uppercase;">{name}</a>`}
            templateData={{
              name: headerTranslation.businessText,
              link: headerConfig.businessLink
            }}
          />
        </li>
      )}
      {headerConfig.touristLink && (
        <li>
          <HTMLTemplate
            template={`<a class="nav-item" href={link} data-event-id="${
              EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
            }"
            data-event-message="${
              headerTranslation.touristText
            }" style="font-size:0.75rem;line-height:1rem;color:#a3a3a3;text-transform: uppercase;">{name}</a>`}
            templateData={{
              name: headerTranslation.touristText,
              link: headerConfig.touristLink
            }}
          />
        </li>
      )}
    </ul>
  );
};

export default TopHeaderNavigation;
