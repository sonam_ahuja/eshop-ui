import { RootState } from '@common/store/reducers';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { Provider } from 'react-redux';
import searchActions from '@search/store/actions';
import {
  Header,
  IComponentProps as IHeaderProps,
  mapDispatchToProps,
  mapStateToProps
} from '@src/common/components/Header';
import { commonAction } from '@store/actions';
import authenticationAction from '@authentication/store/actions';
import basketActions from '@basket/store/actions';
import { histroyParams } from '@mocks/common/histroy';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

// tslint:disable-next-line:no-big-function
describe('<Header />', () => {
  const mockStore = configureStore();
  const componentProps: IHeaderProps = {
    ...histroyParams,
    userDetails: {
      id: '',
      status: '',
      relatedParties: [
        {
          id: '',
          role: '',
          name: 'name'
        }
      ],
      contactMediums: [
        {
          type: '',
          role: {
            name: ''
          },
          medium: {},
          preferred: true
        }
      ]
    },
    basketItems: 2,
    headerTranslation: appState().translation.cart.global.header,
    headerConfig: {
      headertype: 2,
      specialOfferImageUrl: '',
      specialOfferLink: '',
      showHeaderStrip: true,
      headerColumns: 2,
      dtLogo: {
        isMobileLogoDifferent: true,
        desktop: {
          imageAltText: '',
          url: 'url',
          aspectRatio: 12,
          height: '12',
          width: '23'
        },
        mobile: {
          imageAltText: '',
          url: 'url',
          aspectRatio: 12,
          height: '12',
          width: '23'
        },
        heightWidthUnit: 'fn'
      },
      privateLink: 'link',
      businessLink: 'link',
      touristLink: 'link'
    },
    search: appState().search,
    searchTranslation: appState().translation.cart.global.search,
    searchConfiguration: appState().configuration.cms_configuration.global
      .search,
    megaMenu: appState().common.megaMenu,
    fetchSuggestion: jest.fn(),
    translation: appState().translation,
    basket: appState().basket,
    fetchBasket: jest.fn(),
    goToLogin: jest.fn(),
    routeToLoginFromBasket: jest.fn(),
    checkoutButtonStatus: jest.fn(),
    logout: jest.fn(),
    setTempText: jest.fn(),
    fetchCategories: jest.fn(),
    setInputContent: jest.fn(),
    setSearchModal: jest.fn(),
    setListingEnable: jest.fn(),
    setSearchPriorityText: jest.fn(),
    showBasketSideBar: jest.fn(),
    setDeleteBasketModal: jest.fn(),
    clearBasket: jest.fn(),
    possibleNextRouteWhenSummaryButtonClick: jest.fn(),
    isUserLoggedIn: true,
    currency: ''
  };

  const basketItems = [
    {
      id: '2c9683ae6c3133e1016c32e4eb490001',
      isModifiable: false,
      status: 'Active',
      totalPrice: [
        {
          priceAlterations: [
            {
              priceType: 'upfrontPrice',
              duration: {
                timePeriod: '12',
                type: 'monthly'
              },
              price: 420
            }
          ],
          priceType: 'upfrontPrice',
          totalPrice: 420,
          price: 400
        }
      ],
      quantity: 2,
      group: 'device',
      product: {
        specId: '123',
        categorySlug: '12333',
        id: 'iphone_7p_V_1',
        imageUrl:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=bb_9d_17128c16c26a30c2f2b92dbffaa0.jpeg',
        name: 'abc',
        brandName: 'addidas',
        description: 'description',
        shortDescription: 'hi',
        productGroupId: 'groupidproduct'
      },
      cartTerms: [],
      itemPrice: [],
      cartItems: []
    }
  ];

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IHeaderProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Header {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly when user is logged In', () => {
    const wapper = componentWrapper(componentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when user is logged In', () => {
    const wapper = componentWrapper(componentProps);
    expect(wapper).toMatchSnapshot();
  });
  test('should render properly, when user is not logged In', () => {
    const newProps: IHeaderProps = { ...componentProps, isUserLoggedIn: false };
    const wapper = componentWrapper(newProps);
    expect(wapper).toMatchSnapshot();
  });

  test('passing props via update component', () => {
    const newProps: IHeaderProps = { ...componentProps };
    const component = shallow(<Header {...newProps} />);
    component.setProps({ ...initStateValue });
    component.update();
    component.setState({ isShowHeader: false });
    component.update();
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const newProps: IHeaderProps = { ...componentProps };
    newProps.location.search =
      '?productOfferingBenefit=0&productOfferingTerm=agreement12';
    newProps.basket.basketItems = basketItems;
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Header {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('Header').instance() as Header).openMenuInMobile(true);
    (component.find('Header').instance() as Header).openMenuInMobile(false);
    (component.find('Header').instance() as Header).redirectToBasket();
    (component.find('Header').instance() as Header).proceedToCheckout();
    (component.find('Header').instance() as Header).componentWillUnmount();
    newProps.showBasketStrip = true;
    (component.find('Header').instance() as Header).toggleBodyClass();
    const reciveProps = { ...newProps };
    reciveProps.location.search = 'abc';
    (component.find('Header').instance() as Header).componentWillReceiveProps(
      reciveProps
    );

    (component.find('Header').instance() as Header).getBasketImages();
    (component
      .find('Header')
      .instance() as Header).hideHeaderFooterOnQueryStringKey();
  });

  test('handleChange test for input field with is Mobile true ', () => {
    const newProps: IHeaderProps = { ...componentProps };
    newProps.location.search =
      '?productOfferingBenefit=0&productOfferingTerm=agreement12';
    newProps.basket.basketItems = basketItems;
    newProps.isUserLoggedIn = false;
    newProps.userDetails.relatedParties[0].name = 'name';

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Header {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('Header').instance() as Header).proceedToCheckout();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).goToLogin();
    mapDispatchToProps(dispatch).logout();
    mapDispatchToProps(dispatch).clearBasket();
    mapDispatchToProps(dispatch).setDeleteBasketModal(true);
    mapDispatchToProps(dispatch).showBasketSideBar(true);
    mapDispatchToProps(dispatch).checkoutButtonStatus(
      BASKET_ALL_ITEM.cartItems
    );
    mapDispatchToProps(dispatch).fetchBasket({ isCalledFromCheckout: true });
    mapDispatchToProps(dispatch).setSearchModal(true);
    mapDispatchToProps(dispatch).setSearchPriorityText({
      activeText: 'sam',
      inActiveText: 'samsung'
    });
    mapDispatchToProps(dispatch).setInputContent('samsung');
    mapDispatchToProps(dispatch).setTempText('samsung');
    mapDispatchToProps(dispatch).setListingEnable(true);
    mapDispatchToProps(dispatch).fetchCategories({
      query: 'string',
      url: 'string',
      type: 'string',
      isNewHit: true
    });
    mapDispatchToProps(dispatch).possibleNextRouteWhenSummaryButtonClick();
    mapDispatchToProps(dispatch).fetchSuggestion('abc');
    expect(dispatch.mock.calls[0][0]).toEqual(
      commonAction.setIfLoginButtonClicked(true)
    );
    expect(dispatch.mock.calls[1][0]).toEqual(authenticationAction.logout());
    expect(dispatch.mock.calls[2][0]).toEqual(basketActions.clearBasket());
    expect(dispatch.mock.calls[3][0]).toEqual(
      basketActions.setDeleteBasketModal(true)
    );
    expect(dispatch.mock.calls[4][0]).toEqual(
      basketActions.showBasketSideBar(true)
    );
    expect(dispatch.mock.calls[6][0]).toEqual(
      basketActions.fetchBasket({ isCalledFromCheckout: true })
    );
    expect(dispatch.mock.calls[7][0]).toEqual(
      searchActions.setSearchModal(true)
    );
    expect(dispatch.mock.calls[8][0]).toEqual(
      searchActions.setSearchPriorityText({
        activeText: 'sam',
        inActiveText: 'samsung'
      })
    );
    expect(dispatch.mock.calls[9][0]).toEqual(
      searchActions.setInputContent('samsung')
    );
    expect(dispatch.mock.calls[10][0]).toEqual(
      searchActions.setTempText('samsung')
    );
    expect(dispatch.mock.calls[11][0]).toEqual(
      searchActions.setListingEnable(true)
    );
    expect(dispatch.mock.calls[12][0]).toEqual(
      searchActions.fetchCategories({
        query: 'string',
        url: 'string',
        type: 'string',
        isNewHit: true
      })
    );
  });
  // tslint:disable-next-line:max-file-line-count
});
