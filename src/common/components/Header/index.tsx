import {
  IHeaderTranslation,
  ISearchTranslation
} from '@src/common/store/types/translation';
import {
  IHeader,
  ISearchConfiguration
} from '@src/common/store/types/configuration';
import basketActions from '@basket/store/actions';
import authenticationAction from '@authentication/store/actions';
import { RootState } from '@src/common/store/reducers';
import { Dispatch } from 'redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import React, { ReactNode } from 'react';
import { commonAction } from '@store/actions';
import SearchModal from '@search/Modals/SearchModal';
import { connect } from 'react-redux';
import { IMenusCategory, IUserDetails } from '@src/common/store/types/common';
import { Icon } from 'dt-components';
import { IFetchBasketArgs } from '@src/common/routes/basket/store/types';
import { isMobile, logError, noopFn } from '@src/common/utils';
import searchActions from '@search/store/actions';
import {
  IParsedQueryString,
  parseQueryString
} from '@src/common/utils/parseQuerySrting';
import DesktopSpeechRecognitionInput from '@common/components/SpeechRecognition/desktop';
import { IProps as IDesktopSpeechRecognitionInputProps } from '@common/components/SpeechRecognition/utils/SpeechRecognitionHOC';
import { IFetchCategoriesParams, ISearchState } from '@search/store/types';
import BasketStrip from '@src/common/components/BasketStripWrapper/BasketStrip';
import { IBasketItem, IBasketState } from '@basket/store/types';
import { ITranslationState } from '@store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import { sendHeaderClickEvent } from '@events/common/index';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';
import ROUTE_CONSTANTS from '@common/constants/routes';

import {
  StyledActions,
  StyledContentWrapper,
  StyledFadeBgLeft,
  StyledFadeBgRight,
  StyledHeaderWrapper,
  StyledLogo,
  StyledLogoutDropdownEl,
  StyledLogoutDropdownWrap,
  StyledNavigation,
  StyledRoundButtonCart,
  StyledRoundButtonHamburger,
  StyledRoundButtonLogin,
  StyledRoundButtonSearch,
  StyledTopHeader
} from './styles';
import { Nav } from './nav/index';
import { MobileHeaderComponent } from './mobile/index';
import TopHeaderNavigation from './common/topHeaderNavigation';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps {
  showBasketIcon?: boolean;
  isBasketRoute?: boolean;
  showBasketStrip?: boolean;
  showItemCount?: boolean;
}
export interface IComponentDispatchToProps {
  goToLogin(): void;
  logout(): void;
  fetchBasket(data: IFetchBasketArgs): void;
  fetchCategories(params: IFetchCategoriesParams): void;
  setInputContent(value: string): void;
  setTempText(value: string): void;
  setSearchModal(isEnable: boolean): void;
  setListingEnable(isEnable: boolean): void;
  setSearchPriorityText(search: {
    activeText: string;
    inActiveText: string;
  }): void;
  setDeleteBasketModal(visibility: boolean): void;
  clearBasket(): void;
  showBasketSideBar(payload: boolean): void;
  checkoutButtonStatus(payload: IBasketItem[]): void;
  possibleNextRouteWhenSummaryButtonClick(): void;
  fetchSuggestion(query: string): void;
  routeToLoginFromBasket(status: boolean): void;
}

export interface IComponentStateToProps {
  currency: string;
  isUserLoggedIn: boolean;
  headerConfig: IHeader;
  headerTranslation: IHeaderTranslation;
  search: ISearchState;
  basketItems: number;
  searchTranslation: ISearchTranslation;
  searchConfiguration: ISearchConfiguration;
  translation: ITranslationState;
  basket: IBasketState;
  userDetails: IUserDetails;
  // tslint:disable-next-line:no-any
  megaMenu: IMenusCategory[] | any[];
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps<IRouterParams>;

export interface IState {
  navContainerWidth: number;
  isBrowserReady: boolean;
  openMenusInMobile: boolean;
  isShowHeader: boolean;
  showLogoutText: boolean;
}
// tslint:disable
export class Header extends React.Component<IComponentProps, IState> {
  desktopSpeechRecognitionInput: IDesktopSpeechRecognitionInputProps = {
    autoStart: false,
    continuous: false,
    // tslint:disable-next-line: no-empty
    onInputChange: () => {},
    // tslint:disable-next-line: no-empty
    onInputValueChange: (value: string) => {
      // tslint:disable-next-line: no-console
      console.log(value);
    }
  };

  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      openMenusInMobile: false,
      isShowHeader: true,
      isBrowserReady: false,
      navContainerWidth: 0,
      showLogoutText: false
    };

    this.openMenuInMobile = this.openMenuInMobile.bind(this);
    this.showLogoutText = this.showLogoutText.bind(this);
  }

  openMenuInMobile(isMObileMenuOpen: boolean): void {
    this.setState({
      openMenusInMobile: isMObileMenuOpen
    });
  }

  toggleBodyClass = (): void => {
    if (this.props.showBasketStrip) {
      document.body.classList.add('hasBasketStrip');
    } else {
      document.body.classList.remove('hasBasketStrip');
    }
  };

  componentDidMount(): void {
    this.setState({
      isBrowserReady: true
    });
    this.toggleBodyClass();
    if (
      this.props.location.pathname !== ROUTE_CONSTANTS.BASKET &&
      !this.props.location.pathname.includes('checkout') &&
      !this.props.location.pathname.includes('order-confirmation')
    ) {
      this.props.fetchBasket({});
    }
    this.hideHeaderFooterOnQueryStringKey();
  }

  componentWillReceiveProps(nextProps: IComponentProps): void {
    if (nextProps.location.search !== this.props.location.search) {
      this.hideHeaderFooterOnQueryStringKey();
    }
  }

  componentWillUnmount(): void {
    this.toggleBodyClass();
  }

  getBasketImages = (): string[] | (string | undefined)[] => {
    const { basketItems } = this.props.basket;
    if (basketItems && basketItems.length) {
      return basketItems
        .map((item: IBasketItem) => {
          return item.product.imageUrl;
        })
        .filter(url => url);
    }

    return [];
  };

  hideHeaderFooterOnQueryStringKey(): void {
    const parsedUrl: IParsedQueryString = parseQueryString(
      this.props.location.search
    );

    if (parsedUrl.header === 'hide') {
      this.setState({ isShowHeader: false });
    }
  }
  redirectToBasket = (): void => {
    this.props.history.push(ROUTE_CONSTANTS.BASKET);
  };

  proceedToCheckout = (): void => {
    const { isUserLoggedIn, routeToLoginFromBasket } = this.props;

    if (isUserLoggedIn) {
      this.props.possibleNextRouteWhenSummaryButtonClick();
    } else {
      routeToLoginFromBasket(true);
    }
  };

  showLogoutText(): void {
    if (isMobile.phone && this.props.location.pathname.includes('basket')) {
      this.setState({ showLogoutText: !this.state.showLogoutText });
    } else {
      noopFn();
    }
  }

  loginComponent(initials: string | null): ReactNode {
    const { isUserLoggedIn, goToLogin, logout } = this.props;
    const { header: headerTranslation } = this.props.translation.cart.global;
    const logoutEle = (
      <StyledLogoutDropdownEl onClick={logout}>
        <div className="topArrow" />
        <div className="dropdownListWrap">{headerTranslation.logoutText}</div>
      </StyledLogoutDropdownEl>
    );
    return (
      <StyledRoundButtonLogin
        onClick={isUserLoggedIn ? this.showLogoutText : goToLogin}
        data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
        data-event-message={'login icon'}
      >
        {isUserLoggedIn ? (
          <>
            <div className="circleWrap">
              <span className="initials-wrap">{initials}</span>
            </div>
            <StyledLogoutDropdownWrap className="activeOnHover">
              {isMobile.phone
                ? this.state.showLogoutText
                  ? logoutEle
                  : null
                : logoutEle}
            </StyledLogoutDropdownWrap>
          </>
        ) : (
          <div className="circleWrap">
            <Icon
              className="icons"
              color="currentColor"
              name="ec-user-account"
            />
          </div>
        )}
      </StyledRoundButtonLogin>
    );
  }

  render(): ReactNode {
    const { isUserLoggedIn, goToLogin, logout } = this.props;
    const {
      headerConfig,
      setSearchModal,
      isBasketRoute,
      basket,
      setSearchPriorityText,
      searchTranslation,
      searchConfiguration,
      fetchCategories,
      search,
      setListingEnable
    } = this.props;
    const { megaMenu } = this.props;
    const { translation, clearBasket, currency } = this.props;
    const { header: headerTranslation } = translation.cart.global;
    const { total: basketItems } = basket.pagination;
    const { userDetails } = this.props;
    let initials = null;
    let userName = null;
    try {
      userName = isUserLoggedIn ? userDetails.relatedParties[0].name : null;
      const allInitials = (userName && userName.match(/\b\w/g)) || [];
      initials = (
        (allInitials.shift() || '') + (allInitials.pop() || '')
      ).toUpperCase();
    } catch (error) {
      logError(`username did not come from API ${error}`);
    }

    const columnOrder = headerConfig.headerColumns - 1;

    return (
      <>
        {isMobile.phone ? (
          <AuxiliaryRoute
            isChildrenRender={search.searchModal.modalOpen}
            hashPath={auxillaryRouteConfig.SEARCH.MOBILE_SEARCH}
            isModal={true}
            onClose={() => this.props.setSearchModal(false)}
          >
            {search.searchModal.modalOpen && this.state.isBrowserReady ? (
              <SearchModal
                setTempText={this.props.setTempText}
                setInputContent={this.props.setInputContent}
                setSearchPriorityText={setSearchPriorityText}
                searchConfiguration={searchConfiguration}
                searchTranslation={searchTranslation}
                search={search}
                fetchSuggestion={this.props.fetchSuggestion}
                setListingEnable={setListingEnable}
                fetchCategories={fetchCategories}
                setSearchModal={setSearchModal}
              />
            ) : null}
          </AuxiliaryRoute>
        ) : null}
        {this.state.isShowHeader ? (
          <StyledHeaderWrapper>
            {headerConfig.showHeaderStrip && (
              <StyledTopHeader>
                <TopHeaderNavigation
                  headerConfig={headerConfig}
                  headerTranslation={headerTranslation}
                />
              </StyledTopHeader>
            )}

            <StyledContentWrapper>
              <StyledLogo>
                <Link
                  data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
                  data-event-message="logo"
                  to="/"
                >
                  {headerConfig.dtLogo.desktop.url.length ? (
                    <img
                      src={headerConfig.dtLogo.desktop.url}
                      width={headerConfig.dtLogo.desktop.width}
                      height={headerConfig.dtLogo.desktop.height}
                      alt={headerConfig.dtLogo.desktop.imageAltText}
                    />
                  ) : (
                    <Icon
                      color="currentColor"
                      size="inherit"
                      name="ec-dt-logo"
                    />
                  )}
                </Link>
              </StyledLogo>

              {/*  navigation for desktop */}
              {Array.isArray(megaMenu) &&
              megaMenu.length &&
              !(isBasketRoute === true) ? (
                <StyledNavigation>
                  <StyledFadeBgLeft />
                  <Nav
                    columnOrder={columnOrder}
                    megaMenu={megaMenu}
                    headerTranslation={headerTranslation}
                    headerConfig={headerConfig}
                  />
                  <StyledFadeBgRight />
                </StyledNavigation>
              ) : null}

              {/*  Actions/Right Buttons wrapper */}
              <StyledActions>
                {!this.props.location.pathname.includes('checkout') &&
                  !this.props.location.pathname.includes('basket') &&
                  isMobile.phone && (
                    <StyledRoundButtonSearch
                      onClick={(
                        event: React.MouseEvent<HTMLDivElement, MouseEvent>
                      ) => {
                        event.persist();
                        event.stopPropagation();
                        sendHeaderClickEvent('search icon');
                        this.props.setSearchModal(true);
                      }}
                    >
                      <div className="circleWrap">
                        <Icon
                          className="icons"
                          color="currentColor"
                          name="ec-search"
                        />
                      </div>
                    </StyledRoundButtonSearch>
                  )}
                {this.state.isBrowserReady &&
                  !this.props.location.pathname.includes('checkout') &&
                  !this.props.location.pathname.includes('basket') &&
                  !isMobile.phone &&
                  DesktopSpeechRecognitionInput &&
                  DesktopSpeechRecognitionInput !== undefined &&
                  DesktopSpeechRecognitionInput !== null && (
                    <DesktopSpeechRecognitionInput
                      {...this.desktopSpeechRecognitionInput}
                    />
                  )}
                {/* login component */}
                {!isMobile.phone ? this.loginComponent(initials) : null}

                {/* basket icon on header */}
                {this.props.showBasketIcon !== false && (
                  <StyledRoundButtonCart>
                    <Link
                      data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
                      data-event-message="basket"
                      to="/basket"
                    >
                      <div className="circleWrap">
                        <Icon
                          className="icons"
                          color="currentColor"
                          name="ec-solid-shopping-cart"
                        />
                        {basketItems !== 0 && (
                          <div className="notificationCount">
                            <span>{basketItems}</span>
                          </div>
                        )}
                      </div>
                    </Link>
                  </StyledRoundButtonCart>
                )}

                {/* mobile hamburger */}
                {this.props.location.pathname.includes('basket') &&
                isMobile.phone ? (
                  this.loginComponent(initials)
                ) : (
                  <StyledRoundButtonHamburger
                    onClick={() => {
                      this.openMenuInMobile(true);
                      sendHeaderClickEvent('hamburger clicked"');
                    }}
                  >
                    <div className="circleWrap">
                      <Icon
                        className="icons"
                        color="currentColor"
                        name="dt-menu"
                      />
                    </div>
                  </StyledRoundButtonHamburger>
                )}
                {/* mobile nav component */}
                {isMobile.phone &&
                Array.isArray(megaMenu) &&
                megaMenu.length ? (
                  <AuxiliaryRoute
                    isChildrenRender={this.state.openMenusInMobile}
                    hashPath={auxillaryRouteConfig.HEADER.MENU}
                    isModal={true}
                    onClose={() => this.openMenuInMobile(false)}
                  >
                    <MobileHeaderComponent
                      openMenu={this.state.openMenusInMobile}
                      headerTranslation={headerTranslation}
                      headerConfig={headerConfig}
                      openMenuInMobile={this.openMenuInMobile}
                      isUserLoggedIn={isUserLoggedIn}
                      megaMenu={megaMenu}
                      goToLogin={goToLogin}
                      logout={logout}
                      userName={userName}
                      nameInitials={initials}
                    />
                  </AuxiliaryRoute>
                ) : null}
              </StyledActions>
            </StyledContentWrapper>

            {this.props.showBasketStrip === true ? (
              <BasketStrip
                basket={basket}
                basketItems={basket.pagination.total}
                basketImages={this.getBasketImages() as string[]}
                translation={translation.cart.basket}
                clearBasketModalState={basket.clearBasket}
                clearBasket={clearBasket}
                setDeleteBasketModal={this.props.setDeleteBasketModal}
                redirectToBasket={this.redirectToBasket}
                proceedToCheckout={this.proceedToCheckout}
                currency={currency}
                showItemCount={true}
                showBasketSideBar={this.props.showBasketSideBar}
              />
            ) : null}
          </StyledHeaderWrapper>
        ) : null}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  isUserLoggedIn: state.common.isLoggedIn,
  headerConfig: state.configuration.cms_configuration.global.header,
  headerTranslation: state.translation.cart.global.header,
  basketItems: state.basket.pagination.total,
  megaMenu: state.common.megaMenu,
  search: state.search,
  searchTranslation: state.translation.cart.global.search,
  searchConfiguration: state.configuration.cms_configuration.global.search,
  currency: state.common.currency,
  translation: state.translation,
  basket: state.basket,
  userDetails: state.common.userDetails
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  goToLogin(): void {
    dispatch(commonAction.setIfLoginButtonClicked(true));
  },
  logout(): void {
    dispatch(authenticationAction.logout());
  },
  fetchBasket(data: IFetchBasketArgs): void {
    dispatch(basketActions.fetchBasket(data));
  },
  setSearchModal(showModal: boolean): void {
    dispatch(searchActions.setSearchModal(showModal));
  },
  setSearchPriorityText(search: {
    activeText: string;
    inActiveText: string;
  }): void {
    dispatch(searchActions.setSearchPriorityText(search));
  },
  setInputContent(value: string): void {
    dispatch(searchActions.setInputContent(value));
  },
  setTempText(value: string): void {
    dispatch(searchActions.setTempText(value));
  },
  setListingEnable(value: boolean): void {
    dispatch(searchActions.setListingEnable(value));
  },
  fetchCategories(params: IFetchCategoriesParams): void {
    dispatch(searchActions.fetchCategories(params));
  },
  clearBasket(): void {
    dispatch(basketActions.clearBasket());
  },
  setDeleteBasketModal(visibility: boolean): void {
    dispatch(basketActions.setDeleteBasketModal(visibility));
  },
  showBasketSideBar(payload: boolean): void {
    dispatch(basketActions.showBasketSideBar(payload));
  },
  checkoutButtonStatus(payload: IBasketItem[]): void {
    dispatch(basketActions.setCheckoutButtonStatus(payload));
  },
  possibleNextRouteWhenSummaryButtonClick(): void {
    dispatch(commonAction.goToNextRoute());
  },
  fetchSuggestion(query: string): void {
    dispatch(searchActions.fetchSuggestion(query));
  },
  routeToLoginFromBasket(status: boolean): void {
    dispatch(commonAction.routeToLoginFromBasket(status));
  }
});

export default withRouter(
  connect<IComponentStateToProps, IComponentDispatchToProps, IProps, RootState>(
    mapStateToProps,
    mapDispatchToProps
  )(Header)
);
