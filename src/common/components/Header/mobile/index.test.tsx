import React from 'react';
import { RootState } from '@common/store/reducers';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { IMenusCategory } from '@src/common/store/types/common';
import {
  IComponentProps,
  MobileHeaderComponent
} from '@src/common/components/Header/mobile';
import { histroyParams } from '@mocks/common/histroy';

describe('<MobileHeaderComponent />', () => {
  const newMegaMenu: IMenusCategory[] = [
    {
      id: '2',
      currentNodeMetaData: {
        type: 'type',
        tags: ['tages'],
        distanceFromResolution: 23,
        data: {
          title: 'title',
          description: 'description',
          openNewTab: false,
          deeplink: '/'
        }
      },
      childNodes: [
        {
          id: '4',
          currentNodeMetaData: {
            type: 'type',
            tags: ['tages'],
            distanceFromResolution: 23,
            data: {
              title: 'title',
              description: 'description',
              openNewTab: false,
              deeplink: '/'
            }
          },
          childNodes: [
            {
              id: '2',
              currentNodeMetaData: {
                type: 'type',
                tags: ['tages'],
                distanceFromResolution: 23,
                data: {
                  title: 'title',
                  description: 'description',
                  openNewTab: false,
                  deeplink: '/'
                }
              },
              childNodes: [
                {
                  id: '4',
                  currentNodeMetaData: {
                    type: 'type',
                    tags: ['tages'],
                    distanceFromResolution: 23,
                    data: {
                      title: 'title',
                      description: 'description',
                      openNewTab: false,
                      deeplink: '/'
                    }
                  },
                  childNodes: []
                }
              ]
            }
          ]
        }
      ]
    }
  ];
  const mockStore = configureStore();
  const componentProps: IComponentProps = {
    ...histroyParams,
    nameInitials: '',
    userName: '',
    openMenu: false,
    headerConfig: {
      headerColumns: 2,
      showHeaderStrip: true,
      headertype: 2,
      specialOfferImageUrl: '',
      specialOfferLink: '',
      dtLogo: {
        isMobileLogoDifferent: true,
        desktop: {
          url: 'url',
          aspectRatio: 12,
          height: '12',
          width: '23',
          imageAltText: ''
        },
        mobile: {
          url: 'url',
          aspectRatio: 12,
          height: '12',
          width: '23',
          imageAltText: ''
        },
        heightWidthUnit: 'fn'
      },
      privateLink: 'link',
      businessLink: 'link',
      touristLink: 'link'
    },
    headerTranslation: appState().translation.cart.global.header,
    isUserLoggedIn: true,
    megaMenu: newMegaMenu,
    openMenuInMobile: jest.fn(),
    goToLogin: jest.fn(),
    logout: jest.fn()
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileHeaderComponent {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly when user is logged In', () => {
    const props: IComponentProps = { ...componentProps };
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when user not logged In', () => {
    const props: IComponentProps = { ...componentProps, isUserLoggedIn: false };
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when menu is open', () => {
    const props: IComponentProps = { ...componentProps, openMenu: true };
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when mega menu lenght is zero', () => {
    const props: IComponentProps = { ...componentProps, megaMenu: [] };
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('action triggers', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileHeaderComponent {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    // tslint:disable-next-line: no-any
    const event: any = {
      currentTarget: ''
    };

    (component
      .find('MobileHeaderComponent')
      .instance() as MobileHeaderComponent).openMenu(event, {
      id: 'string',
      currentNodeMetaData: {
        type: 'string',
        tags: ['string'],
        distanceFromResolution: 1,
        data: {
          title: 'string',
          description: 'string',
          openNewTab: false,
          deeplink: '/'
        }
      }
    });
    (component
      .find('MobileHeaderComponent')
      .instance() as MobileHeaderComponent).onBack();
    (component
      .find('MobileHeaderComponent')
      .instance() as MobileHeaderComponent).onMenuClose();
  });
});
