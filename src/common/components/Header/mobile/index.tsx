import { IHeader } from '@src/common/store/types/configuration';
import React, { ReactNode } from 'react';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import { Accordion, Icon } from 'dt-components';
import { colors } from '@src/common/variables';
import styled from 'styled-components';
import { IHeaderTranslation } from '@src/common/store/types/translation';
import { IMenusCategory } from '@src/common/store/types/common';
import EVENT_NAME from '@events/constants/eventName';
import { sendHeaderClickEvent } from '@events/common/index';
import history from '@src/client/history';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';
import { noopFn } from '@src/common/utils';

import TopHeaderNavigation from '../common/topHeaderNavigation';

import {
  AppFlowPanel,
  StyledMobileBodyWrap,
  StyledMobileLogoutDropdownEl,
  StyledTopMobileHeader
} from './styles';

export const StyledMainMobileHeader = styled.div``;

export const StyledMobileHeader = styled.div`
  padding: 2rem 1.25rem 2rem 3rem;
  background: ${colors.coldGray};
`;

export interface IComponentProps {
  openMenu: boolean;
  headerConfig: IHeader;
  headerTranslation: IHeaderTranslation;
  isUserLoggedIn: boolean;
  megaMenu: IMenusCategory[];
  nameInitials: string | null;
  userName: string | null;
  openMenuInMobile(isMObileMenuOpen: boolean): void;
  goToLogin(): void;
  logout(): void;
}

export interface IState {
  openMenuId: string;
}

// tslint:disable
export class MobileHeaderComponent extends React.Component<
  IComponentProps,
  IState
> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      openMenuId: ''
    };
    this.onBack = this.onBack.bind(this);
    this.onMenuClose = this.onMenuClose.bind(this);
    this.goToLink = this.goToLink.bind(this);
  }

  openMenu(event: React.MouseEvent, menu: IMenusCategory): void {
    const title = menu.currentNodeMetaData.data.title;
    const target = event.currentTarget;
    if (
      target &&
      target.getAttribute &&
      target.getAttribute('data-event-message') !== title &&
      menu.childNodes &&
      Array.isArray(menu.childNodes) &&
      menu.childNodes.length
    ) {
      this.setState({
        openMenuId: menu.id
      });
    }
  }

  onBack(): void {
    if (this.state && this.state.openMenuId.length) {
      this.setState({
        openMenuId: ''
      });
    }
  }

  onMenuClose(): void {
    this.setState({
      openMenuId: ''
    });
    sendHeaderClickEvent('header close');
    this.props.openMenuInMobile(false);
  }

  goToLink(menu: IMenusCategory): void {
    if (history) {
      history.push(menu.currentNodeMetaData.data.deeplink);
      this.onMenuClose();
    }
  }

  getLogoutEle(): ReactNode {
    return this.props.isUserLoggedIn ? (
      <a
        onClick={this.props.logout}
        data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
        data-event-message={'logout text'}
      >
        <StyledMobileLogoutDropdownEl>
          <div className="dropdownListWrap">
            {this.props.headerTranslation.logoutText}
          </div>
        </StyledMobileLogoutDropdownEl>
      </a>
    ) : null;
  }

  render(): ReactNode {
    const { isUserLoggedIn, goToLogin } = this.props;
    const { openMenu, megaMenu, userName } = this.props;
    const { nameInitials } = this.props;
    const { headerTranslation, headerConfig } = this.props;

    const specialOffer = (
      <div className="dynamic-content-wrap">
        <HTMLTemplate
          template={`<style> .specialOffer {display: block;} .specialOffer .specialOfferContent {width: 100%;
            border-radius: 0.5rem; background-color: #e20074;
            background-image: url({bgImage});
            background-size: 200px auto; background-position: bottom right; background-repeat: no-repeat; padding: 2rem 1.5rem 2.75rem;
            color: #fff; 	  text-decoration: none; display: block;} .specialOffer .specialOfferContent .heading {font-size: 0.75rem;
            line-height: 1.33; letter-spacing: 0.2px; margin-bottom: 1rem;} .specialOffer .specialOfferContent .description {font-size: 1.5rem;
            line-height: 1.17; font-weight: 900; max-width: 13rem;} @media (min-width: 1024px) {.specialOffer .specialOfferContent {background-size: 230px auto;}
            .specialOffer .heading {font-size: 0.9375rem;} .specialOffer .description {font-size: 1.375rem;} 	.specialOffer .specialOfferContent
            .description{max-width: 16rem;}}</style>
            <a href={link} class='specialOffer'>
            <div class='specialOfferContent'>
            <h2 class='heading'>{title}</h2>
            <div class='description'>{description}</div>
            </div></a>`}
          templateData={{
            title: headerTranslation.specialOfferTitle,
            description: headerTranslation.specialOfferDescription,
            bgImage: headerConfig.specialOfferImageUrl,
            link: headerConfig.specialOfferLink
          }}
          data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
          data-event-message="offer in header"
        />
      </div>
    );

    return (
      <AppFlowPanel
        isOpen={openMenu}
        showCloseButton={true}
        showBackButton={!!this.state.openMenuId.length}
        className="flow-panel-wrap"
        onClose={() => this.onMenuClose()}
        onBack={() => this.onBack()}
      >
        <div className="main-mobile-menu-panel">
          <StyledTopMobileHeader>
            {headerConfig.showHeaderStrip && (
              <div className="top-mobile-header-wrap">
                <TopHeaderNavigation
                  headerConfig={this.props.headerConfig}
                  headerTranslation={this.props.headerTranslation}
                />
              </div>
            )}
            <div className="user-login-wrap">
              <a
                onClick={isUserLoggedIn ? noopFn : goToLogin}
                data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
                data-event-message={'login icon'}
              >
                <div className="userCredentialsWrap">
                  <div className="user-logo-wrap">
                    {isUserLoggedIn ? (
                      <div className="circular-wrap">
                        <span className="initials-wrap">{nameInitials}</span>
                      </div>
                    ) : (
                      <div className="circular-wrap">
                        <Icon
                          className="icons"
                          color="currentColor"
                          name="ec-user-account"
                        />
                      </div>
                    )}
                  </div>
                  <div className="user-name-wrap">
                    <span>
                      {isUserLoggedIn ? userName : headerTranslation.signInText}
                    </span>
                  </div>
                </div>
              </a>
            </div>
          </StyledTopMobileHeader>
          {Array.isArray(megaMenu) && megaMenu.length ? (
            <StyledMobileBodyWrap>
              <div className="flyout-list-wrapper">
                <ul className="list-panel">
                  {megaMenu.map((menu: IMenusCategory) => {
                    return (
                      <li
                        key={menu.id}
                        onClick={(event: React.MouseEvent) =>
                          this.openMenu(event, menu)
                        }
                      >
                        <div className="ordered-list clearfix">
                          <HTMLTemplate
                            className="title"
                            template={`<a href={link} data-event-id="${
                              EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
                            }"
                            data-event-message="${
                              menu.currentNodeMetaData.data.title
                            }" style="font-size: 1.5rem;line-height: 1.7rem;color:#383838;font-weight:bold">{menuName}</a>`}
                            templateData={{
                              menuName: menu.currentNodeMetaData.data.title,
                              link: menu.currentNodeMetaData.data.deeplink
                            }}
                            sameTabOnDifferentDomain={
                              !menu.currentNodeMetaData.data.openNewTab
                            }
                          />
                          {menu.childNodes &&
                            Array.isArray(menu.childNodes) &&
                            menu.childNodes.length && (
                              <div className="iconsWrap">
                                <Icon
                                  className="icons"
                                  color="currentColor"
                                  name="ec-arrow-right"
                                />
                              </div>
                            )}
                        </div>
                      </li>
                    );
                  })}
                  <li>{this.getLogoutEle()}</li>
                </ul>
              </div>
            </StyledMobileBodyWrap>
          ) : null}
        </div>

        {Array.isArray(megaMenu) && megaMenu.length
          ? megaMenu.map((menu: IMenusCategory) => {
              return (
                <AuxiliaryRoute
                  isChildrenRender={this.state.openMenuId === menu.id}
                  hashPath={auxillaryRouteConfig.HEADER.SUBMENU}
                  isModal={true}
                  onClose={this.onBack}
                >
                  <div
                    key={menu.id}
                    className={
                      this.state.openMenuId === menu.id
                        ? ' detailed-mobile-menu-panel open-fly'
                        : 'detailed-mobile-menu-panel'
                    }
                  >
                    <StyledTopMobileHeader>
                      <div className="category-title-wrap">
                        <div className="category-name-wrap">
                          <span
                            data-event-id={
                              EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
                            }
                            data-event-message={
                              menu.currentNodeMetaData.data.title
                            }
                          >
                            {menu.currentNodeMetaData.data.title}
                          </span>
                        </div>
                      </div>
                    </StyledTopMobileHeader>
                    {menu.childNodes &&
                    Array.isArray(menu.childNodes) &&
                    menu.childNodes.length
                      ? menu.childNodes.map((submenu: IMenusCategory) => {
                          return (
                            <StyledMobileBodyWrap key={submenu.id}>
                              <div className="flyout-list-wrapper">
                                {submenu.childNodes &&
                                Array.isArray(submenu.childNodes) &&
                                submenu.childNodes.length ? (
                                  <div className="accordion-list-wrap">
                                    <div className="accordion-panel">
                                      <Accordion
                                        className="accordion-title"
                                        isOpen={false}
                                        headerTitle={
                                          <HTMLTemplate
                                            key={submenu.id}
                                            className="lists"
                                            template={`<a href={link} data-event-id="${
                                              EVENT_NAME.HEADER.EVENTS
                                                .HEADER_CLICKs
                                            }"
                                          data-event-message="${
                                            submenu.currentNodeMetaData.data
                                              .title
                                          }" >{menuName}</a>`}
                                            templateData={{
                                              menuName:
                                                submenu.currentNodeMetaData.data
                                                  .title,
                                              link:
                                                submenu.currentNodeMetaData.data
                                                  .deeplink
                                            }}
                                            sameTabOnDifferentDomain={
                                              !submenu.currentNodeMetaData.data
                                                .openNewTab
                                            }
                                          />
                                        }
                                      >
                                        <div className="panel-body">
                                          <ul>
                                            <li>
                                              {submenu.childNodes.map(
                                                (subChild: IMenusCategory) => {
                                                  return (
                                                    <HTMLTemplate
                                                      key={subChild.id}
                                                      className="lists"
                                                      template={`<a href={link} data-event-id="${
                                                        EVENT_NAME.HEADER.EVENTS
                                                          .HEADER_CLICKs
                                                      }"
                                                    data-event-message="${
                                                      subChild
                                                        .currentNodeMetaData
                                                        .data.title
                                                    }">{menuName}</a>`}
                                                      templateData={{
                                                        menuName:
                                                          subChild
                                                            .currentNodeMetaData
                                                            .data.title,
                                                        link:
                                                          subChild
                                                            .currentNodeMetaData
                                                            .data.deeplink
                                                      }}
                                                      sameTabOnDifferentDomain={
                                                        !subChild
                                                          .currentNodeMetaData
                                                          .data.openNewTab
                                                      }
                                                    />
                                                  );
                                                }
                                              )}
                                            </li>
                                          </ul>
                                        </div>
                                      </Accordion>
                                    </div>
                                  </div>
                                ) : (
                                  <HTMLTemplate
                                    key={submenu.id}
                                    className="lists default-list-wrap"
                                    template={`<a href={link} data-event-id="${
                                      EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
                                    }"
                                  data-event-message="${
                                    submenu.currentNodeMetaData.data.title
                                  }">{menuName}</a>`}
                                    templateData={{
                                      menuName:
                                        submenu.currentNodeMetaData.data.title,
                                      link:
                                        submenu.currentNodeMetaData.data
                                          .deeplink
                                    }}
                                    sameTabOnDifferentDomain={
                                      !submenu.currentNodeMetaData.data
                                        .openNewTab
                                    }
                                  />
                                )}
                              </div>
                            </StyledMobileBodyWrap>
                          );
                        })
                      : null}
                    {this.getLogoutEle()}
                    {specialOffer}
                  </div>
                </AuxiliaryRoute>
              );
            })
          : null}
        {specialOffer}
      </AppFlowPanel>
    );
  }
}
