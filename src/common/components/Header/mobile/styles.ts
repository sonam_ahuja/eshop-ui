import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { FlowPanel } from 'dt-components';
// tslint:disable

export const StyledMobileLogoutDropdownEl = styled.div`
  padding: 1.625rem 1.7rem 1.625rem 0rem;
  border-bottom: 1px solid ${colors.silverGray};
  display: flex;
  align-items: center;
  cursor: pointer;
  font-size: 1.5rem;
  line-height: 1.7rem;
  color: ${colors.darkGray};
  font-weight: bold;
  text-align: left;
`;

export const AppFlowPanel = styled(FlowPanel)`
  z-index: 999;
  .dt_outsideClick {
    width: 100%;
    height: 100%;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    -webkit-tap-highlight-color: transparent;
    .flowPanelInner {
      height: 100%;
      padding: 0;
      background: ${colors.coldGray};
      .backButton {
        position: absolute;
        top: 1.5rem;
        left: 1rem;
        z-index: 100;
      }
      .closeIcon {
        top: 1.25rem;
        right: 1.25rem;
        z-index: 100;
      }
      .flowPanelContent {
        display: flex;
        flex-direction: column;
      }
    }
    .detailed-mobile-menu-panel {
      display: none;
      overflow-y: auto;
      background: ${colors.white};

      &.open-fly {
        -webkit-animation: slideInLeft 400ms both;
        animation: slideInLeft 400ms both;
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
      }
      &.close-fly {
        -webkit-animation: slideInRight 400ms both;
        animation: slideInRight 400ms both;
        /* display: none; */
      }
      ${StyledMobileLogoutDropdownEl}{
        margin-left:3rem;
      }
    }
  }
  @keyframes slideInLeft {
    from {
      transform: translate3d(100%, 0, 0);
      visibility: visible;
    }
    to {
      transform: translate3d(0%, 0, 0);
    }
  }
  @keyframes slideInRight {
    from {
      transform: translate3d(-100%, 0, 0);
      visibility: hidden;
    }
    to {
      transform: translate3d(100%, 0, 0);
    }
  }
  .dynamic-content-wrap {
    /* height: 20rem;
    background: ${colors.magenta};
    color: ${colors.white};
    display: flex;
    align-items: center;
    justify-content: center; */
    text-align: left;

    .specialOfferContent{
      padding: 2rem 3rem;
      border-radius: 0;
    }
  }
`;
export const StyledTopMobileHeader = styled.div`
  -webkit-transition: height 0.3s;
  -moz-transition: height 0.3s;
  transition: height 0.3s;
  /* height: 196px; */
  &.scroll-wrap {
    position: -webkit-sticky;
    position: sticky;
    top: -1px;
    background: ${colors.coldGray};
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    transition: all 0.3s;
    /* height: 120px; */
    display: flex;
    width: 100%;
    align-items: center;
    flex-wrap: wrap;
    .category-title-wrap,
    .user-login-wrap {
      height: auto;
      padding: 1rem 1.25rem 1rem 3.5rem;
      width: 100%;
    }
    .user-login-wrap {
      padding: 0rem 1.25rem 0.2rem 3rem;
    }
    .top-mobile-header-wrap {
      padding: 1rem 1.25rem 0.75rem 3rem;
    }
    .closeIcon {
      top: 0.6rem;
      right: 1.25rem;
      z-index: 100;
    }
  }
  .hide {
    display: none !important;
  }
  .top-mobile-header-wrap {
    display: flex;
    align-items: center;
    padding: 2rem 1.25rem 0.75rem 3rem;
    .list-inline {
      display: flex;
      li {
        display: inline-block;
        margin-right: 2rem;
        line-height: 1rem;
        &:last-child {
          margin-right: 0;
        }
        a {
          font-size: 0.75rem;
          font-weight: bold;
          color: ${colors.mediumGray};
          /* text-transform: uppercase; */
          &:hover,
          &:focus,
          &.active {
            color: ${colors.darkGray};
          }
        }
      }
    }
  }
  .user-login-wrap {
    padding: 4.5rem 1.25rem 1.5rem 3rem;
    display: flex;
    align-items: flex-end;
    height: 12.25rem;
    .userCredentialsWrap {
      width: 100%;
      display: flex;
      cursor: pointer;
    }
    .user-name-wrap {
      font-weight: 900;
      display: flex;
      margin-left: 1rem;
      text-align: left;
      align-items: center;
      span {
        font-size: 1.875rem;
        line-height: 2rem;
        color: ${colors.mediumGray};
        width: 100%;
        display: inline-block;
        /* text-transform: capitalize; */
      }
    }
    .user-logo-wrap {
      display: flex;
    }
    .circular-wrap {
      width: 2.5rem;
      height: 2.5rem;
      display: flex;
      align-items: center;
      justify-content: center;
      background: ${colors.highLightGray};
      border-radius: 50px;
      i {
        color: ${colors.magenta};
        font-size: 1.4375rem;
      }
      .initials-wrap {
        color: ${colors.ironGray};
        font-size: 1.25rem;
        font-weight: normal;
      }
      .profile-img {
        width: 100%;
        height: 100%;
      }
    }
  }
  .category-title-wrap {
    padding: 4.5rem 1.25rem 1.5rem 3rem;
    display: flex;
    align-items: flex-end;
    height: 12.25rem;
    background: ${colors.coldGray};
    .category-name-wrap {
      float: left;
      width: 100%;
      text-align: left;
      font-weight: 600;
      display: flex;
      align-items: center;
      word-break: break-word;
      span {
        font-size: 1.875rem;
        line-height: 2rem;
        color: ${colors.darkGray};
        width: 100%;
        display: inline-block;
        font-weight: bolder;
      }
    }
  }
`;
export const StyledMobileBodyWrap = styled.div`
  background: ${colors.white};
  .clearfix:after {
    content: ' ';
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .flyout-list-wrapper {
    li {
      padding-left: 3rem;
    }
    .ordered-list {
      padding: 1.625rem 1.7rem 1.625rem 0rem;
      border-bottom: 1px solid ${colors.silverGray};
      display: flex;
      align-items: center;
      cursor: pointer;
      justify-content: space-between;
      text-align: left;
      .title {
        font-size: 1.5rem;
        line-height: 1.7rem;
        color: ${colors.darkGray};
        font-weight: bold;
        float: left;
        width: calc(100% - 2rem);
        text-align: left;
      }
      .iconsWrap {
        width: 2rem;
        margin-left: auto;
        .icons {
          font-size: 1.25rem;
          color: ${colors.darkGray};
        }
      }
    }
    .blocked-wrap {
      padding: 2.75rem 3rem 0;
      ul {
        li {
          text-align: left;
          font-size: 1.5rem;
          line-height: 1.75rem;
          font-weight: bold;
          color: ${colors.ironGray};
          margin-bottom: 1rem;
          padding: 0;
          .list-head-title {
            font-size: 0.875rem;
            line-height: 1.25rem;
            color: ${colors.magenta};
            font-weight: bold;
            /* text-transform: uppercase; */
          }
          &:last-child {
            margin-bottom: 0;
          }
        }
      }
      &:last-child {
        padding-bottom: 2.75rem;
      }
    }

    .accordion-list-wrap {
      padding: 0rem 0rem 0rem 3rem;
      .accordion-title {
        border-bottom: 1px solid ${colors.silverGray};
        .accordionHeader {
          background: ${colors.white};
          text-align: left;
          padding: 1.75rem 1.875rem 1.75rem 0;
          .accordionTitle {
            font-size: 1.25rem;
            line-height: 1.5rem;
            color: ${colors.darkGray};
            font-weight: normal;
          }
        }
        .panel-body {
          ul {
            li {
              text-align: left;
              font-size: 1.5rem;
              line-height: 1.75rem;
              font-weight: bold;
              color: ${colors.ironGray};
              padding: 0;
              .list-head-title {
                font-size: 0.875rem;
                line-height: 1.25rem;
                color: ${colors.magenta};
                font-weight: bold;
                /* text-transform: uppercase; */
              }
              &:last-child {
                margin-bottom: 0;
              }
              .lists {
                margin-bottom: 1rem;
                &:last-child {
                  margin-bottom: 0rem;
                }
              }
            }
          }
          &:last-child {
            padding-bottom: 2rem;
          }
        }
        &.isOpen {
          .accordionHeader {
            .accordionTitle {
              color: ${colors.magenta};
            }
            i {
              color: ${colors.magenta};
            }
          }
        }
      }
    }
    .default-list-wrap {
      background: #ffffff;
      text-align: left;
      border-bottom: 1px solid #e6e6e6;
      padding: 1.75rem 1.875rem 1.75rem 0rem;
      font-size: 1.25rem;
      line-height: 1.5rem;
      color: ${colors.darkGray};
      font-weight: normal;
      margin-left: 3rem;
    }
  }
`;
