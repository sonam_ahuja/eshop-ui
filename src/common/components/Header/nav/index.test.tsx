import React from 'react';
import { RootState } from '@common/store/reducers';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { IProps, Nav } from '@src/common/components/Header/nav';
import { IMenusCategory } from '@src/common/store/types/common';

describe('<Nav />', () => {
  const mockStore = configureStore();
  const newMegaMenu: IMenusCategory[] = [
    {
      id: '1',
      currentNodeMetaData: {
        type: 'menu',
        tags: ['tag1', 'tag2'],
        distanceFromResolution: 3,
        data: {
          title: 'name',
          description: 'description',
          openNewTab: true,
          deeplink: '/'
        }
      },
      childNodes: [
        {
          id: '2',
          currentNodeMetaData: {
            type: 'menu',
            tags: ['tag1', 'tag2'],
            distanceFromResolution: 3,
            data: {
              title: 'name',
              description: 'description',
              openNewTab: true,
              deeplink: '/'
            }
          },
          childNodes: []
        }
      ]
    }
  ];
  const componentProps: IProps = {
    megaMenu: newMegaMenu,
    columnOrder: 4,
    headerConfig: {
      headerColumns: 2,
      specialOfferImageUrl: '',
      specialOfferLink: '',
      showHeaderStrip: true,
      headertype: 2,
      dtLogo: {
        isMobileLogoDifferent: true,
        desktop: {
          url: 'url',
          aspectRatio: 12,
          height: '12',
          width: '23',
          imageAltText: ''
        },
        mobile: {
          url: 'url',
          aspectRatio: 12,
          height: '12',
          width: '23',
          imageAltText: ''
        },

        heightWidthUnit: 'fn'
      },
      privateLink: 'link',
      businessLink: 'link',
      touristLink: 'link'
    },
    headerTranslation: appState().translation.cart.global.header
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Nav {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly when user is logged In', () => {
    const wapper = componentWrapper(componentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when user is logged In', () => {
    jest.mock('@src/common/utils', () => ({
      isMobile: {
        phone: true
      }
    }));
    const wapper = componentWrapper(componentProps);
    expect(wapper).toMatchSnapshot();
  });
});
