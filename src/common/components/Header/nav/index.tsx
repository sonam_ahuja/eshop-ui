// tslint:disable
import React from 'react';
import HTMLTemplate from '@src/common/components/HtmlTemplate';
import { IMenusCategory } from '@src/common/store/types/common';
import EVENT_NAME from '@events/constants/eventName';
import { IHeaderTranslation } from '@src/common/store/types/translation';
import { IHeader } from '@src/common/store/types/configuration';

import {
  StyledNavLinks,
  StyledNavLink,
  StyledList,
  StyledMegaMenuContent,
  StyledListWrapper,
  StyledColumn,
  StyledBannerWrapper
} from './styles';
import { colors } from '@src/common/variables';
import { Icon } from 'dt-components';

export interface IProps {
  megaMenu: IMenusCategory[];
  columnOrder: number;
  headerConfig: IHeader;
  headerTranslation: IHeaderTranslation;
}

export class Nav extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseEnter = (): void => {
    document.body.classList.add('megaMenuOpen');
  };
  onMouseLeave = (): void => {
    document.body.classList.remove('megaMenuOpen');
  };
  componentWillUnmount(): void {
    document.body.classList.remove('megaMenuOpen');
  }

  render(): React.ReactNode {
    const { megaMenu, headerConfig, headerTranslation } = this.props;

    return (
      <StyledNavLinks>
        {megaMenu.map((menu: any) => {
          return (
            <StyledNavLink
              key={menu.id}
              onMouseEnter={this.onMouseEnter}
              onMouseLeave={this.onMouseLeave}
            >
              <div className="menuLink">
                <HTMLTemplate
                  className="linkText"
                  template={`<a href={link}  data-event-id="${
                    EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
                    }"
                          data-event-message="${
                    menu.currentNodeMetaData.data.title
                    }"  className="nav-item">{menuName}</a>`}
                  templateData={{
                    menuName: menu.currentNodeMetaData.data.title,
                    link: menu.currentNodeMetaData.data.deeplink
                  }}
                />
                {menu.childNodes && Array.isArray(menu.childNodes) && (
                  <Icon
                    className="caret"
                    color="currentColor"
                    size="inherit"
                    name="ec-expand"
                  />
                )}
              </div>
              {Array.isArray(menu.childNodes) && menu.childNodes.length ? (
                <StyledMegaMenuContent>
                  <StyledListWrapper>
                    {menu.childNodes.map((submenu: any) => {
                      return (
                        <StyledColumn
                          columnOrder={this.props.columnOrder}
                          key={submenu.id}
                        >
                          <HTMLTemplate
                            className="columnHeading"
                            style={{
                              whiteSpace: 'pre-wrap',
                              color: `${colors.magenta}`,
                              fontSize: '0.75rem',
                              fontWeight: 'bold',
                              fontStyle: 'normal',
                              fontStretch: 'normal',
                              lineHeight: '1.33',
                              letterSpacing: 'normal',
                              marginBottom: '0.5rem',
                              textTransform: 'uppercase'
                            }}
                            template={`<a data-event-id="${
                              EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
                              }"data-event-message="${
                              submenu.currentNodeMetaData.data.title
                              }" head-title" href={link}>{menuName}</a>`}
                            templateData={{
                              menuName: submenu.currentNodeMetaData.data.title,
                              link: submenu.currentNodeMetaData.data.deeplink
                            }}
                          />
                          {Array.isArray(submenu.childNodes) &&
                            submenu.childNodes.length ? (
                              <StyledList>
                                {submenu.childNodes.map((subchild: any) => {
                                  return (
                                    <li key={subchild.id}>
                                      <HTMLTemplate
                                        template={`<a style='line-height:1.2; display: inline-block' href={link} data-event-id="${
                                          EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs
                                          }"
                                                            data-event-message="${
                                          subchild
                                            .currentNodeMetaData
                                            .data.title
                                          }">{menuName}</a>`}
                                        templateData={{
                                          menuName:
                                            subchild.currentNodeMetaData.data
                                              .title,
                                          link:
                                            subchild.currentNodeMetaData.data
                                              .deeplink
                                        }}
                                      />
                                    </li>
                                  );
                                })}
                              </StyledList>
                            ) : null}
                        </StyledColumn>
                      );
                    })}
                  </StyledListWrapper>

                  <StyledBannerWrapper>
                    <HTMLTemplate
                      template={`<style> .specialOffer {display: block;} .specialOffer .specialOfferContent {width: 100%;
                      border-radius: 0.5rem; background-color: #e20074;
                      background-image: url({bgImage});
                      background-size: 200px auto; background-position: bottom right; background-repeat: no-repeat; padding: 2rem 1.5rem 2.75rem;
                      color: #fff; 	  text-decoration: none; display: block;} .specialOffer .specialOfferContent .heading {font-size: 0.75rem;
                      line-height: 1.33; letter-spacing: 0.2px; margin-bottom: 1rem;} .specialOffer .specialOfferContent .description {font-size: 1.5rem;
                      line-height: 1.17; font-weight: 900; max-width: 13rem;} @media (min-width: 1024px) {.specialOffer .specialOfferContent {background-size: 230px auto;}
                      .specialOffer .heading {font-size: 0.9375rem;} .specialOffer .description {font-size: 1.375rem;} 	.specialOffer .specialOfferContent
                      .description{max-width: 16rem;}}</style>
                      <a href={link} class='specialOffer'>
                      <div class='specialOfferContent'>
                      <h2 class='heading'>{title}</h2>
                      <div class='description'>{description}</div>
                      </div></a>`}
                      templateData={{
                        title: headerTranslation.specialOfferTitle,
                        description: headerTranslation.specialOfferDescription,
                        bgImage: headerConfig.specialOfferImageUrl,
                        link: headerConfig.specialOfferLink
                      }}
                      data-event-id={EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs}
                      data-event-message="special offer in header"
                    />
                  </StyledBannerWrapper>
                </StyledMegaMenuContent>
              ) : null}
            </StyledNavLink>
          );
        })}
      </StyledNavLinks>
    );
  }
}
