import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledMegaMenuContent = styled.div`
  position: absolute;
  /* max-height: 450px; */
  background: ${colors.fogGray};
  visibility: hidden;
  opacity: 0;
  display: flex;
  top: 100%;
  left: 0;
  width: 100%;
  padding: 2rem 3rem;
  cursor: default;
  transition: 0s linear 0.2s;

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: calc(100% - 5.75rem);
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: calc(100% - 4.75rem);
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    /* width: calc(100% - 5.75rem); */
    width: 100%;
  }
`;

export const StyledNavLinks = styled.div`
  display: flex;
  max-width: inherit;
  overflow: auto;

  &::-webkit-scrollbar {
    display: none;
    width: 0 !important ;
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
  &,
  * {
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
`;

// new Start
export const StyledNavLink = styled.div`
  font-size: 0.75rem;
  font-weight: bold;
  line-height: 1.33;
  color: ${colors.mediumGray};
  height: 100%;
  display: flex;
  align-items: center;
  cursor: pointer;

  .menuLink {
    text-transform: uppercase;
    white-space: nowrap;
    margin: 0 0.5rem;
    display: flex;
    align-items: center;
    .caret {
      margin-left: 0.25rem;
    }
  }

  &:hover {
    .menuLink {
      color: ${colors.magenta};
      .caret {
        transform: rotate(180deg);
        color: ${colors.magenta};
      }
    }

    ${StyledMegaMenuContent} {
      /* transition: opacity 0.1s ease-in 0.2s, transform 0.1s linear 0.25s; */
      transition: 0.2s ease-in 0.2s;
      visibility: visible;
      opacity: 1;
    }
  }
`;

export const StyledListWrapper = styled.div`
  display: flex;
  text-align: left;
  flex: 1;
  flex-wrap: wrap;
  max-height: 400px;
  overflow: auto;
`;
export const StyledList = styled.ul`
  li {
    margin-bottom: 0.25rem;
    transition: 0.1s linear 0s;
    font-size: 0.875rem;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: ${colors.darkGray};
    &:hover {
      color: ${colors.magenta};
    }
  }
`;
export const StyledColumn = styled.div<{ columnOrder: number }>`
  ${({ columnOrder }) => (columnOrder ? `width: ${100 / columnOrder}%` : '')};
  padding: 0 0.6rem;
  margin-bottom: 1.25rem;

  /* .columnHeading {
    white-space: pre-wrap;
    color: ${colors.magenta};
    font-size: 0.75rem;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    margin-bottom: 0.25rem;
  } */
`;

export const StyledBannerWrapper = styled.div`
  background: ${colors.magenta};
  text-align: left;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    margin: -2rem -3rem -2rem 2rem;
  }
`;
