import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
// tslint:disable:max-file-line-count
export const StyledLogoutDropdownEl = styled.div`
  font-size: 0.85rem;
  line-height: 1rem;
  font-weight: 500;
  color: ${colors.stoneGray};
  width: 100%;
  box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.2);
  border-radius: 3px;
  min-width: 6rem;
  max-width: 10rem;
  background: ${colors.white};

  &:hover {
    color: ${colors.magenta};
  }
  .dropdownListWrap {
    display: flex;
    padding: 0.35rem 0.55rem;
    cursor: pointer;
  }
`;

export const StyledLogoutDropdownWrap = styled.div`
  top: 4rem;
  right: 0;
  position: absolute;
  justify-content: flex-start;
  z-index: 1000;
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.2s ease-in;

  .topArrow,
  .topArrow:after {
    width: 0;
    height: 0;
    border: solid transparent;
    position: absolute;
  }
  .topArrow {
    bottom: 100%;
    border-bottom-color: ${colors.fogGray};
    border-width: 0.6rem;
    transform: translateX(-0.6rem);
    left: inherit;
    right: 0.25rem;
    &:after {
      content: '';
      border-bottom-color: ${colors.white};
      border-width: 0.5rem;
      transform: translateX(0.5rem);
      bottom: -0.6rem;
      left: inherit;
      right: 0;
    }
  }
`;

export const StyledFadeBgRight = styled.div`
  width: 20px;
  height: 100%;
  flex-shrink: 0;
  position: relative;
  z-index: 1;
  background: linear-gradient(
    90deg,
    rgba(237, 237, 237, 0.7) 0%,
    rgba(237, 237, 237, 1) 100%
  );
  box-shadow: -5px -5px 8px rgba(237, 237, 237, 1);
`;

export const StyledFadeBgLeft = styled.div`
  width: 20px;
  height: 100%;
  flex-shrink: 0;
  position: relative;
  z-index: 1;
  background: linear-gradient(
    90deg,
    rgba(237, 237, 237, 0.7) 0%,
    rgba(237, 237, 237, 1) 100%
  );
  box-shadow: 8px -5px 8px rgba(237, 237, 237, 0.5);
`;

export const StyledTopHeader = styled.div`
  padding: 0.15rem 4.9rem 0.2rem;
  min-height: 25px;
  display: flex;
  align-items: center;
  background: ${colors.charcoalGray};
  position: relative;
  z-index: 10;

  .list-inline {
    display: flex;
    li {
      display: inline-block;
      margin-right: 2.5rem;
      font-weight: bold;
      &:last-child {
        margin-right: 0;
      }
      a {
        font-size: 0.625rem;
        font-weight: bold;
        color: ${colors.mediumGray};
        transition: all 0.1s ease;
      }
      &:hover {
        a {
          opacity: 0.8;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    z-index: 2;
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    &:after {
      content: '';
      position: absolute;
      height: 100%;
      top: 0;
      left: -9999px;
      right: -9999px;
      margin: auto;
      z-index: -1;
      height: 100%;
      background: inherit;
    }
  }
`;

export const StyledRoundButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.75rem;
  font-weight: bold;
  color: ${colors.mediumGray};
  padding: 0;
  margin-left: 1rem;
  position: relative;

  .circleWrap {
    width: 2.5rem;
    height: 2.5rem;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    background: ${colors.highLightGray};
    border-radius: 50%;
    .dt_icon {
      font-size: 1.25rem;
      color: ${colors.ironGray};
    }
    .initials-wrap {
      color: ${colors.ironGray};
      font-size: 1.25rem;
      font-weight: normal;
    }
    &:hover {
      background: ${colors.white};
      .dt_icon,
      .initials-wrap {
        color: ${colors.magenta};
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .circleWrap {
      width: 3rem;
      height: 3rem;
      .dt_icon {
        font-size: 1.5rem;
      }
    }
  }
`;
export const StyledRoundButtonCart = styled(StyledRoundButton)`
  .notificationCount {
    width: 0.85rem;
    height: 0.85rem;
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${colors.white};
    font-size: 0.625rem;
    line-height: 0.75rem;
    font-weight: bold;
    right: 0.4rem;
    position: absolute;
    bottom: 0.4rem;
    background: ${colors.magenta};
    border-radius: 50%;
    padding: 2px;
  }
`;

export const StyledRoundButtonLogin = styled(StyledRoundButton)`
  display: flex;
  &:hover{
    ${StyledLogoutDropdownWrap}{
      display: flex;
      opacity: 1;
      visibility: visible;
    }
  }
  /* @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
  } */
`;
export const StyledRoundButtonHamburger = styled(StyledRoundButton)`
  display: flex;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: none;
  }
`;
export const StyledRoundButtonSearch = styled(StyledRoundButton)``;

export const StyledActions = styled.div`
  display: flex;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-right: inherit;
    position: absolute;
    right: 0;
  }
  @media (min-width: ${breakpoints.desktop}px) {
  }
`;

export const StyledNavigation = styled.div<{
  maxWidth?: number;
  paddingRight?: number;
}>`
  margin-right: auto;
  height: 100%;
  display: none;
  flex: 1;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    max-width: 100%;
    padding-right: 380px;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-right: 380px;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding-right: 480px;
  }
`;

export const StyledLogo = styled.div`
  float: left;
  min-width: 5rem;
  height: 100%;
  text-align: left;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  i {
    font-size: 3.4rem;
    color: ${colors.magenta};
    cursor: pointer;
    display: flex;
    align-items: center;
  }
  a {
    display: flex;
    width: 100%;
    height: 100%;
    align-items: center;
    img {
      max-width: 100%;
      max-height: 100%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    i {
      font-size: 3.2rem;
    }
  }
`;

export const StyledContentWrapper = styled.div`
  position: relative;
  z-index: 1;
  height: 5rem;
  width: 100%;
  background: ${colors.cloudGray};
  text-align: center;
  color: ${colors.mediumGray};
  align-items: center;
  justify-content: space-between;
  display: flex;
  padding: 1rem 1.25rem;
  font-size: 0.75rem;
  line-height: 1rem;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 0rem 1.5rem 0rem 2.25rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 0rem 1.625rem 0rem 3rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0rem 1rem 0rem 4.9rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 0rem 1.35rem 0rem 5.5rem;
    height: 6rem;

    &:after {
      content: '';
      position: absolute;
      height: 100%;
      top: 0;
      left: -9999px;
      right: -9999px;
      margin: auto;
      z-index: -1;
      height: 100%;
      background: inherit;
      max-width: 100vw;
    }
  }
`;

export const StyledHeaderWrapper = styled.header`
  position: relative;
  /* zindex 101 and basketStrip zindex 100 */
  z-index: 13;

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    z-index: 101;
  }
`;
