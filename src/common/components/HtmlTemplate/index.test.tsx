import React from 'react';
import { mount } from 'enzyme';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import appState from '@store/states/app';
import HTMLTemplateComponent, {
  HtmlTemplate
} from '@src/common/components/HtmlTemplate';
import { histroyParams } from '@mocks/common/histroy';

describe('<HtmlTemplate>', () => {
  const props = {
    template: '',
    templateData: {},
    onRouteChange: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <HTMLTemplateComponent {...props} />
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();
  });

  test('componentWillUnmount', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <HTMLTemplateComponent {...props} />
      </StaticRouter>
    );
    component.unmount();
  });

  test('action triggers', () => {
    const newProps = { ...props, ...histroyParams };
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <HtmlTemplate {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('HtmlTemplate').instance() as HtmlTemplate).setRef(null);
    (component.find('HtmlTemplate').instance() as HtmlTemplate).onClick({
      preventDefault: jest.fn(),
      target: {
        value: 'value'
      }
      // tslint:disable-next-line: no-any
    } as any);
  });
});
