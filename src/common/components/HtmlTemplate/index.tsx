import React, { Component, ReactNode } from 'react';
import templateParser from '@utils/templateParser';
import { REGEX, VALIDATIONS } from '@src/common/constants/appConstants';
import { RouteComponentProps, withRouter } from 'react-router';
import root from 'react-shadow';
import he from 'he';
import globalCss from '@common/globalCss';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export interface IProps extends React.HTMLProps<HTMLElement> {
  decodeTemplate?: boolean;
  template: string;
  templateData: object;
  sameTabOnDifferentDomain?: boolean;
  className?: string;
  onRouteChange?(route: string): void;
  sendEvents?(): void;
}

interface IState {
  el: HTMLElement | null;
}

type IComponentProps = IProps & RouteComponentProps<IRouterParams>;

/** USED ON PROLONGATION */
export class HtmlTemplate extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.setRef = this.setRef.bind(this);
    this.onClick = this.onClick.bind(this);
    this.shouldUseShadowDom = this.shouldUseShadowDom.bind(this);
    this.state = {
      el: null
    };
  }

  setRef(ref: HTMLElement | null): void {
    if (!ref) {
      return;
    }
    this.setState(() => ({
      el: ref
    }));
    ref.addEventListener('click', this.onClick);
  }

  // tslint:disable-next-line:cognitive-complexity
  onClick(e: MouseEvent): void {
    if (this.props.sendEvents) {
      this.props.sendEvents();
    }
    (this.composedPath(e.target as HTMLElement) || []).forEach(
      (node: HTMLElement) => {
        if (node.nodeName === 'A') {
          const origin = location.host;
          const href = node.getAttribute('href');
          const toOpenInNewTab = node.getAttribute('target');
          e.preventDefault();
          if (href) {
            if (
              toOpenInNewTab === '_blank' ||
              ((href.replace(REGEX.EXTRACT_BASE_URL, '') !== origin &&
                href.indexOf('http://') === 0) ||
                href.indexOf('https://') === 0)
            ) {
              if (this.props.sameTabOnDifferentDomain) {
                window.open(href, '_self');
              } else {
                window.open(href, '_blank');
              }
            } else if (href.replace(REGEX.EXTRACT_BASE_URL, '') === origin) {
              this.props.history.push(href.slice(location.origin.length));
            } else if (href.startsWith('/')) {
              this.props.history.push(href);
            } else if (href.match(VALIDATIONS.url)) {
              window.open(`https://${href}`, '_blank');
            }
          }
        }
      }
    );
  }

  composedPath(el: HTMLElement): (HTMLElement)[] {
    const path: (HTMLElement)[] = [];

    while (el) {
      path.push(el);

      if (el.tagName === 'HTML') {
        return path;
      }

      el = el.parentElement as HTMLElement;
    }

    return path;
  }

  componentWillUnmount(): void {
    if (this.state.el) {
      this.state.el.removeEventListener('click', this.onClick);
    }
  }

  shouldUseShadowDom(): boolean {
    if (
      typeof window !== 'undefined' &&
      document &&
      document.body &&
      document.body.attachShadow
    ) {
      return typeof document.body.attachShadow === 'function';
    }

    return false;
  }

  render(): ReactNode {
    const {
      decodeTemplate = true,
      template,
      templateData,
      onRouteChange,
      ref,
      staticContext,
      dangerouslySetInnerHTML,
      className,
      ...rest
    } = this.props;

    return (
      <>
        {this.shouldUseShadowDom() ? (
          <root.div>
            <style>{globalCss}</style>
            <div
              className={className}
              ref={this.setRef}
              dangerouslySetInnerHTML={{
                __html: templateParser(
                  decodeTemplate ? he.decode(template) : template,
                  templateData
                )
              }}
              {...rest}
            />
          </root.div>
        ) : (
          <div
            className={className}
            ref={this.setRef}
            dangerouslySetInnerHTML={{
              __html: templateParser(
                decodeTemplate ? he.decode(template) : template,
                templateData
              )
            }}
            {...rest}
          />
        )}
      </>
    );
  }
}

export default withRouter(HtmlTemplate);
