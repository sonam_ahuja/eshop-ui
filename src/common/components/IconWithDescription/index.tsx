import React, { ReactNode } from 'react';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import { IconSizeType } from 'dt-components/lib/es/components/atoms/icon';
import { Icon, Section } from 'dt-components';

import { StyledIconWithDescription } from './styles';

interface IProps {
  className?: string;
  svgNode?: ReactNode;
  iconName?: iconNamesType;
  iconSize?: IconSizeType;
  description: string;
}

const IconWithDescription = (props: IProps) => {
  const { className, svgNode, iconName, description, iconSize } = props;

  const svgEl = svgNode ? svgNode : null;
  const IconEl = iconName ? <Icon size={iconSize} name={iconName} /> : null;

  return (
    <StyledIconWithDescription className={className}>
      <div className='iconWrap'>
        {svgEl}
        {IconEl}
      </div>
      <div className='description'>
        <Section size='large'>{description}</Section>
      </div>
    </StyledIconWithDescription>
  );
};

export default IconWithDescription;
