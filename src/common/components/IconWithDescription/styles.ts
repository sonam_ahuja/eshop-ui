import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledIconWithDescription = styled.div`
  display: flex;
  align-items: center;
  color: ${colors.ironGray};

  .iconWrap {
    margin-right: 1.25rem;
    font-size: 3.75rem;
    line-height: 0;
    svg {
      height: 1em;
      width: 1em;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    flex-direction: column;
    .iconWrap {
      margin: 0;
      margin-bottom: 1.75rem;
      font-size: 10rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .iconWrap {
      font-size: 9.75rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .iconWrap {
      font-size: 12.75rem;
    }
  }
`;
