import React from 'react';
import { Icon, Title } from 'dt-components';

import { StyledInformationText } from './styles';

export interface IProps {
  description?: string;
  keyMessage?: string;
}

const InformationText = (props: IProps) => {
  return (
    <StyledInformationText>
      <Icon color='currentColor' size='inherit' name='ec-eyebrow' />
      <Title size='normal' weight='bold'>
        {props.description}{' '}
        <strong>{props.keyMessage}</strong>
      </Title>
    </StyledInformationText>
  );
};

export default InformationText;
