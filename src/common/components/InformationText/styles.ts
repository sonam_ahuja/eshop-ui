import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledInformationText = styled.div`
  margin-bottom: 1rem;

  .dt_icon {
    font-size: 3.5rem;
    margin-bottom: 0.5rem;
    color: ${colors.darkGray};
  }

  .dt_title {
    color: ${colors.mediumGray};

    strong {
      color: ${colors.darkGray};
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 1.25rem;
    display: flex;
    align-items: center;

    .dt_icon {
      font-size: 4.5rem;
      margin-bottom: 0;
      margin-right: 1.5rem;
    }
  }
`;
