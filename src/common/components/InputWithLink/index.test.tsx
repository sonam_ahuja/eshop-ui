import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import InputWithLink, {
  IProps as IFloatLableWithBottomLinkProps
} from '@common/components/InputWithLink';

describe('<CloseButton />', () => {
  const props: IFloatLableWithBottomLinkProps = {
    inputLabel: 'Username, Email or Phone Number',
    linkText: 'Forgot Credentials?',
    onInputChange: jest.fn(),
    goToRecoveryScreen: jest.fn(),
    errorMessage: 'error',
    isError: true
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <InputWithLink {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
