import React from 'react';
import { Anchor, FloatLabelInput, Section } from 'dt-components';
import cx from 'classnames';
import appConstants from '@src/common/constants/appConstants';

import { StyledEye, StyledInputWithLink } from './style';

export interface IProps {
  type?: string;
  inputLabel: string;
  linkText: string;
  isLoading?: boolean;
  errorMessage: string;
  isError: boolean;
  showEye?: boolean;
  autoComplete?: string;
  isFocused?: boolean;
  className?: string;
  value?: string;
  getRef?(el: HTMLElement | null): void;
  onInputChange?(event: React.ChangeEvent<HTMLInputElement>): void;
  goToRecoveryScreen(): void;
  changeInputType?(): void;
}

const InputWithLink = (props: IProps) => {
  const {
    inputLabel,
    linkText,
    onInputChange,
    goToRecoveryScreen,
    value
  } = props;
  const { errorMessage, type, showEye, changeInputType, className } = props;
  const { autoComplete, isFocused, getRef, isLoading, isError } = props;

  const eyeEl = showEye && (
    <StyledEye
      name={type === 'text' ? 'ec-solid-invisible' : 'ec-visible'}
      onClick={changeInputType}
      color='currentColor'
    />
  );

  const classes = cx(className, { showEye });

  return (
    <StyledInputWithLink className={classes}>
      <FloatLabelInput
        type={type}
        label={inputLabel}
        onChange={onInputChange}
        error={isError}
        errorMessage={errorMessage}
        autoComplete={autoComplete}
        isFocused={isFocused}
        getRef={getRef}
        value={value}
      />
      {eyeEl}
      <Anchor
        hreflang={appConstants.LANGUAGE_CODE}
        title={linkText}
        className='link'
        onClickHandler={goToRecoveryScreen}
        isLoading={isLoading}
      >
        <Section size='small'>{linkText} </Section>
      </Anchor>
    </StyledInputWithLink>
  );
};

export default InputWithLink;
