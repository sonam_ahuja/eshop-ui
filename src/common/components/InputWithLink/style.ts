import styled from 'styled-components';
import { Icon } from 'dt-components';
import { colors } from '@src/common/variables';

export const StyledEye = styled(Icon).attrs({
  size: 'xsmall'
})`
  position: absolute;
  right: 0;
  bottom: 1.5rem;
  cursor: pointer;
  user-select: none;

  &.show-disbaled-eye {
    color: grey;
  }
  color: ${colors.mediumGray};
`;

export const StyledInputWithLink = styled.div`
  position: relative;
  &.showEye input {
    padding-right: 1.5rem;
  }
  &.showEye .hasCloseIcon .closeInputWrap input {
    padding-right: 3rem;
  }
  &.showEye .hasCloseIcon .closeInputWrap span .dt_icon {
    right: 1.5rem;
    bottom: 1.5rem;
  }

  .link {
    position: absolute;
    right: 0;
    bottom: 0;
  }
`;
