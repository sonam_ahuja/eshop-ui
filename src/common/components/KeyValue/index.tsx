import React from 'react';
import { Section } from 'dt-components';
import cx from 'classnames';

import { StyledKeyValue } from './styles';

interface IProps {
  labelName: string;
  labelValue: React.ReactNode | string;
  transform?: 'capitalize' | 'none';
  className?: string;
}

const KeyValue = (props: IProps) => {
  const { labelName, labelValue, className, transform = 'capitalize' } = props;
  const classes = cx(transform, className);

  return (
    <StyledKeyValue className={classes}>
      <Section className='key' size='small'>
        {labelName}
      </Section>
      <Section className='value' size='large'>
        {labelValue}
      </Section>
    </StyledKeyValue>
  );
};

export default KeyValue;
