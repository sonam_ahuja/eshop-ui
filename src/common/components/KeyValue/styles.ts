import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledKeyValue = styled.div`
  .key {
    margin-bottom: 0.4375rem;
    color: ${colors.mediumGray};
  }
  .value {
    color: ${colors.darkGray};
  }

  &.capitalize {
    /* text-transform: capitalize; */
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .key {
      margin-bottom: 0.4rem;
    }
  }
`;
