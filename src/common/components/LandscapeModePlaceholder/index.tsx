import React from 'react';
import store from '@common/store';
import appConstants from '@common/constants/appConstants';

import {
  StyledHeading,
  StyledImageWrap,
  StyledLandscapeModePlaceholder,
  StyledSubHeading,
  StyledTextWrap
} from './styles';

interface IProps {
  className?: string;
}

const LandscapeModePlaceholder = (props: IProps) => {
  const {
    heading,
    subHeading
  } = store.getState().translation.cart.global.landScapeMode;

  const { className } = props;

  return (
    <StyledLandscapeModePlaceholder className={className}>
      <StyledImageWrap
        backgroundImage={`${
          appConstants.ESHOP_CDN_BASE_URL
        }/assets/placeholder_black.gif`}
      />
      <StyledTextWrap>
        <StyledHeading>{heading}</StyledHeading>
        <StyledSubHeading>{subHeading}</StyledSubHeading>
      </StyledTextWrap>
    </StyledLandscapeModePlaceholder>
  );
};

export default LandscapeModePlaceholder;
