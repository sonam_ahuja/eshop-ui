import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';
import { Title } from 'dt-components';

// ImageWrapper
export const StyledImageWrap = styled.div<{backgroundImage: string; }>`
  height: 8.5625rem;
  width: 8.5625rem;
  margin-right: 2rem;
  background-image: url('${props => props.backgroundImage}');
  background-size: cover;

  img {
    max-width: 100%;
    max-height: 100%;
  }
`;

// TextWrapper
export const StyledHeading = styled(Title).attrs({
  size: 'xsmall',
  weight: 'bold'
})`
  color: ${colors.white};
  display: block;
`;
export const StyledSubHeading = styled(Title).attrs({
  size: 'xsmall',
  weight: 'normal'
})`
  color: ${hexToRgbA(colors.white, 0.6)};
`;
export const StyledTextWrap = styled.div`
  max-width: 13.75rem;
`;

// MainWrapper
export const StyledLandscapeModePlaceholder = styled.div`
  background: ${colors.black};
  justify-content: center;
  align-items: center;
  position: fixed;
  z-index: 2147483647;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  display: none;
`;
