import React, { FunctionComponent } from 'react';
import { Icon, Section } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';

import { StyledLinkWithIcon } from './styles';

export interface IProps {
  text?: string;
  iconName?: iconNamesType;
  className?: string;
  onClick?(): void;
}

const LinkWithIcon: FunctionComponent<IProps> = (props: IProps) => {
  const { text, className, onClick, iconName = 'ec-edit' } = props;

  return (
    <StyledLinkWithIcon onClick={onClick} className={className}>
      <Section size='small' weight='normal' className='edit'>
        {text}
      </Section>
      <Icon name={iconName} size='inherit' color='currentColor' />
    </StyledLinkWithIcon>
  );
};

export default LinkWithIcon;
