import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledLinkWithIcon = styled.div`
  display: inline-flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  padding: 0.25rem;
  color: ${colors.magenta};

  .dt_icon {
    margin-left: 5px;
    margin-top: -2px;
  }
`;
