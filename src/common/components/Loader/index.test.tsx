import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SimpleLoader, { IProps } from '@common/components/Loader';

describe('<SimpleLoader />', () => {
  const props: IProps = {
    className: 'string',
    open: true,
    onClose: jest.fn()
  };
  const componentWrapper = (newProps: IProps) => mount(
    <ThemeProvider theme={{}}>
      <SimpleLoader {...newProps} />
    </ThemeProvider>
  );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, open is false', () => {
    const newProps = { ...props, open: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
