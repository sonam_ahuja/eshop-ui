import React from 'react';
import { Loader } from 'dt-components';

import { StyledSimpleLoader } from './styles';

export interface IProps {
  className?: string;
  open?: boolean;
  onClose?(): void;
}
/** USED ON PROLONGATION */
const SimpleLoader = (props: IProps) => {
  const { className, open, onClose } = props;

  return (
    <StyledSimpleLoader className={className}>
      <Loader open={open} onClose={onClose} />
    </StyledSimpleLoader>
  );
};

export default SimpleLoader;
