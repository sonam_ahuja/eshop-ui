import styled from 'styled-components';

export const StyledSimpleLoader = styled.div`
  .dt_overlay {
    background: rgba(255, 255, 255, 0.5);
    .dt_outsideClick {
      div:first-child {
        box-shadow: none;
        background: transparent;
      }
      .dt_icon {
        font-size: 2.5rem;
      }
    }
    p {
      display: none;
    }
  }
`;
