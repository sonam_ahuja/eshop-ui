import React, { ReactNode } from 'react';
import { connect } from 'react-redux';
import { Paragraph, Toast } from 'dt-components';
import { messageType } from 'dt-components/lib/es/components/atoms/strip-message';
import actions from '@common/store/actions/common';
import { Dispatch } from 'redux';
import { RootState } from '@store/reducers';
import styled from 'styled-components';

interface IProps {
  offlineMessage: string;
  showOfflineToast: boolean;
  closeToast(): void;
}

const WrapperToast = styled(Toast)`
  .rightIcon {
    display: none;
  }
`;

class OfflineToast extends React.Component<IProps> {
  render(): ReactNode {
    const { showOfflineToast, offlineMessage, closeToast } = this.props;

    return (
      <WrapperToast
        icon='ec-emergency-triangle'
        isOpen={showOfflineToast}
        type={'error' as messageType}
        onDismiss={closeToast}
        timer={5000000}
        onTimerExpire={closeToast}
        style={{
          zIndex: 9999,
          maxWidth: 'inherit'
        }}
      >
        <Paragraph weight='medium' size='small' transform={'capitalize'}>
          {offlineMessage}
        </Paragraph>
      </WrapperToast>
    );
  }
}

export const mapStateToProps = (state: RootState) => {
  return {
    offlineMessage: state.translation.cart.global.offlineMessage,
    showOfflineToast: state.common.showOfflineToast
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  closeToast(): void {
    dispatch(actions.setOfflineToast(false));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfflineToast);
