import styled from 'styled-components';

export const StyledOfflineNotification = styled.div`
  border: 1px solid red;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9999;
`;
