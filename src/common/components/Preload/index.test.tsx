import React from 'react';
import { mount } from 'enzyme';
import { IMAGE_TYPE } from '@common/store/enums';
import PreloadImage, {
  IWrappedComponentProps
} from '@src/common/components/Preload';
import { StaticRouter } from 'react-router';
import { ThemeProvider } from 'dt-components';

describe('<PreloadHOC/>', () => {
  const props: IWrappedComponentProps = {
    imageUrl: '',
    disablePreloadEffect: true,
    isObserveOnScroll: true,
    style: undefined,
    imageHeight: 20,
    imageWidth: 20,
    loadDefaultImage: false,
    preloadImageUrl: '',
    imageType: 'JPEG',
    width: 20,
    quality: 80,
    mobileWidth: 400,
    disableAnimation: true,
    type: IMAGE_TYPE.WEBP,
    intersectionObserverOption: undefined,
    onerror: jest.fn()
  };

  const componentWrapper = (newProps: IWrappedComponentProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <PreloadImage {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  test('BasketSummary should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
