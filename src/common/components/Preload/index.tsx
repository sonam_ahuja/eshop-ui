import React, { ReactNode } from 'react';
import { PreloadImage } from 'dt-components';
import { IMAGE_TYPE } from '@common/store/enums';
import { isMobile } from '@src/common/utils';
import { getImageQuality } from '@src/common/utils/networkSpeed';
import { checkSafariAndIEBrowser } from '@src/client/DOMUtils';
import appConstants from '@src/common/constants/appConstants';
import cx from 'classnames';

export interface IWrappedComponentProps {
  imageUrl: string;
  disablePreloadEffect?: boolean;
  isObserveOnScroll?: boolean;
  style?: React.CSSProperties;
  imageHeight: number;
  imageWidth: number;
  loadDefaultImage?: boolean;
  preloadImageUrl?: string;
  imageType?: string;
  width?: number;
  quality?: number;
  mobileWidth?: number;
  disableAnimation?: boolean;
  alt?: string;
  className?: string;
  type?: IMAGE_TYPE.WEBP | IMAGE_TYPE.JPEG | IMAGE_TYPE.PNG;
  intersectionObserverOption?: IntersectionObserverInit;
  onerror?(): void;
}

interface IQueryParams {
  [key: string]: string;
}

interface IState {
  imageUrl: string;
}
/** USED ON PROLONGATION */
const PreloadHOC = (
  WrappedComponent: React.ComponentType<IWrappedComponentProps>
) => {
  return class HOC extends React.Component<IWrappedComponentProps, IState> {
    constructor(props: IWrappedComponentProps) {
      super(props);
      this.state = {
        imageUrl: ''
      };
      this.getUpdatedImageURL = this.getUpdatedImageURL.bind(this);
      this.onErrorLoad = this.onErrorLoad.bind(this);
      this.getParams = this.getParams.bind(this);
    }

    componentDidMount(): void {
      if (this.props.imageUrl) {
        const imageUrl = this.getUpdatedImageURL(
          this.props.imageUrl,
          this.props.mobileWidth,
          this.props.width,
          this.props.quality,
          this.props.type,
          this.props.loadDefaultImage
        );
        this.setState({
          imageUrl
        });
      }
    }

    componentWillReceiveProps(nextProps: IWrappedComponentProps): void {
      if (this.props.imageUrl !== nextProps.imageUrl) {
        const imageUrl = this.getUpdatedImageURL(
          nextProps.imageUrl,
          nextProps.mobileWidth,
          nextProps.width,
          nextProps.quality,
          nextProps.type,
          nextProps.loadDefaultImage
        );
        this.setState({
          imageUrl
        });
      }
    }

    getUpdatedImageURL(
      _imageURL: string | undefined,
      mobileWidth = 200,
      width = 400,
      _quality = 80,
      type = IMAGE_TYPE.WEBP,
      loadDefaultImage = false
    ): string {
      let updatedImage = '';
      const imageExtension = _imageURL ? _imageURL.split('.').pop() : undefined;
      const isSAfariAndIEBrowser = checkSafariAndIEBrowser();
      updatedImage = `${_imageURL}&w=${
        isMobile.phone ? mobileWidth : width
      }&q=${getImageQuality()}`;

      if (loadDefaultImage) {
        updatedImage = `${updatedImage}&t=${IMAGE_TYPE.PNG}`;
      } else {
        !isSAfariAndIEBrowser
          ? (updatedImage = `${updatedImage}&t=${type}`)
          : imageExtension && imageExtension.toLowerCase() === IMAGE_TYPE.PNG
          ? (updatedImage = `${updatedImage}&t=${IMAGE_TYPE.PNG}`)
          : (updatedImage = `${updatedImage}&t=${IMAGE_TYPE.JPEG}`);
      }

      return updatedImage;
    }

    getParams(url: string): IQueryParams {
      const params = {};
      const parser = document.createElement('a');
      parser.href = url;
      const query = parser.search.substring(1);
      const vars = query.split('&');
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < vars.length; i++) {
        const pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
      }

      return params;
    }

    onErrorLoad(): void {
      if (this.props.imageUrl) {
        const params = this.getParams(this.props.imageUrl);
        const imageName = params.f;
        const imageUrl = `${appConstants.S3_IMAGE_URL}/${imageName}`;
        this.setState({
          imageUrl
        });
      }
    }

    render(): ReactNode | null {
      const { imageUrl } = this.state;
      const onerror = this.onErrorLoad;

      const props = {
        ...this.props,
        disablePreloadEffect: false,
        imageUrl,
        preloadImageUrl: `${
          appConstants.S3_IMAGE_URL
        }/a8_90_8a92ab2babbe944446107f773a6f.png`,
        onerror
      };

      const classes = cx('es_preloadImage', props.className);

      if (this.state.imageUrl) {
        return <WrappedComponent {...props} className={classes} />;
      }

      return null;
    }
  };
};

export default PreloadHOC(PreloadImage);
