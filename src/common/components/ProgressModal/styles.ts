import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { ProgressModal } from 'dt-components';

export const AppProgressModal = styled(ProgressModal)`
  .dt_overlay {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;

    .dt_outsideClick,
    .dt_button {
      width: 100%;
    }

    .contentWrap {
      width: 100%;
      border-radius: 0.5rem 0.5rem 0 0;
      padding: 3.25rem 1.25rem 2rem 2rem;
      color: ${colors.darkGray};

      .mainContent {
        word-break: break-word;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .dt_overlay {
      justify-content: center;

      .dt_outsideClick,
      .dt_button {
        width: auto;
      }
      .contentWrap {
        padding: 3.25rem 2rem 4rem;
        width: 30rem;
        border-radius: 0;
      }
    }
  }
`;
