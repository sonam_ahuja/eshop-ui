import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import PlanSelect, { IProps } from '@src/common/components/Select/PlanSelect';
import { tariffCategoryMock } from '@src/common/mock-api/category/category.mock';

describe('<PlanSelect />', () => {
  test('should render properly', () => {
    const props: IProps = {
      tariffCategory: tariffCategoryMock,
      planList: [
        {
          id: '',
          title: '',
          rest: {
            value: '',
            label: '',
            description: '',
            agreements: [],
            amount: '',
            active: true,
            monthlySymbol: '',
            characteristics: []
          }
        }
      ],
      selectedItem: {
        rest: {
          description: 'dummy',
          agreements: ['dummy']
        },
        id: '',
        title: ''
      },
      buyDeviceOnly: true,
      productDetailedTranslation: appState().translation.cart.productDetailed,
      onClick: jest.fn(),
      onAgreementChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <PlanSelect {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-identical-functions
  test('should render properly', () => {
    const props: IProps = {
      tariffCategory: tariffCategoryMock,
      selectedItem: {
        rest: {
          description: 'dummy',
          agreements: ['dummy']
        },
        id: '-1',
        title: ''
      },
      planList: [
        {
          id: '',
          title: '',
          rest: {
            value: '',
            label: '',
            description: '',
            agreements: [],
            amount: '',
            active: true,
            monthlySymbol: '',
            characteristics: []
          }
        }
      ],
      buyDeviceOnly: true,
      productDetailedTranslation: appState().translation.cart.productDetailed,
      onClick: jest.fn(),
      onAgreementChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <PlanSelect {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
