import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import React from 'react';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import SimpleSelect from '@common/components/Select/SimpleSelect';
import {
  ICharacteristic,
  ILoyaltyAgreement
} from '@src/common/routes/productDetailed/store/types';
import { Icon, Paragraph, Section, Select } from 'dt-components';
import { isMobile } from '@common/utils';
import { sendDropdownClickEvent } from '@src/common/events/common';
import { ICategory } from '@category/store/types';
import { showDropdown } from '@tariff/utils';
import htmlKeys from '@common/constants/appkeys';
import cx from 'classnames';
import he from 'he';

import {
  StyledPlanSelect,
  StyledSectionBottom,
  StyledSectionTop
} from './styles';

export const onItemClick = (
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
) => {
  return (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event.persist();
    const targetEl = event.target as HTMLElement;
    const parentEl = targetEl.parentElement;
    if (
      targetEl.classList.contains('optionsList') ||
      (targetEl && parentEl && parentEl.classList.contains('optionsList')) ||
      targetEl.classList.contains('dt_outsideClick') ||
      (targetEl && parentEl && parentEl.classList.contains('dt_outsideClick'))
    ) {
      event.stopPropagation();
    } else if (onClick) {
      onClick(event);
    }
  };
};

export interface IProps {
  selectedItem: IListItem | null;
  buyDeviceOnly: boolean;
  tariffCategory: ICategory;
  deviceOnlyPrice?: string;
  productDetailedTranslation: IProductDetailedTranslation;
  planList: {
    id: string;
    title: string;
    rest: {
      value: string;
      label: string;
      description: string;
      agreements: ILoyaltyAgreement[];
      amount: string;
      active: boolean;
      monthlySymbol: string;
      characteristics: ICharacteristic[];
    };
  }[];
  onClick?(event: React.MouseEvent<HTMLDivElement, MouseEvent>): void;
  onAgreementChange(agreementId: string | null): void;
}
const PlanSelect = (props: IProps) => {
  const { onClick, planList } = props;

  const { selectedItem, onAgreementChange, tariffCategory } = props;

  const selectedAgreement = (selectedItem && selectedItem.rest.agreements
    ? selectedItem.rest.agreements
    : []
  ).find((item: ILoyaltyAgreement) => {
    return item.active;
  });

  const showLoyality = showDropdown(
    tariffCategory,
    htmlKeys.TARIFF_CATEGORY_HIDE_LOYALTY_DROPDOWN
  );
  const buyDeviceOnly = selectedItem && selectedItem.id === '-1';

  const classes = cx({ noLoyality: showLoyality === 'false', buyDeviceOnly });

  return (
    <StyledPlanSelect
      onClick={onItemClick(onClick)}
      className={classes}
    >
      <StyledSectionTop>
        <div className='ddWrap'>
          <Paragraph size='small' weight='medium'>
            {selectedItem && selectedItem.rest.label}
          </Paragraph>
          <Paragraph className='dd' size='large' weight='medium'>
            {selectedItem && selectedItem.rest.amount}
            {planList && planList.length === 1 ? null : (
              <Icon name='ec-triangle-down' />
            )}
          </Paragraph>
        </div>
      </StyledSectionTop>
      {selectedItem && selectedItem.id !== '-1' && (
        <StyledSectionBottom>
          <Section className='description' size='large'>
            {selectedItem.rest.description ? (
              <div
                dangerouslySetInnerHTML={{
                  __html: he.decode(selectedItem.rest.description.trim())
                }}
              />
            ) : null}
          </Section>
          {showLoyality === 'true' && (
            <div className='textRightBottom'>
              <Select
                useNativeDropdown={isMobile.phone}
                SelectedItemComponent={SelectedProps => {
                  return (
                    <>
                      <SimpleSelect
                        className='simpleSelect'
                        caretICon='ec-edit'
                        {...SelectedProps}
                      />
                    </>
                  );
                }}
                onItemSelect={item => {
                  sendDropdownClickEvent(item.title);
                  onAgreementChange(String(item.id));
                }}
                selectedItem={selectedAgreement}
                items={selectedItem.rest.agreements}
              />
            </div>
          )}
        </StyledSectionBottom>
      )}
    </StyledPlanSelect>
  );
};

export default PlanSelect;
