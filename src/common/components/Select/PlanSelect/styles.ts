import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSectionTop = styled.div`
  .ddWrap {
    color: ${colors.darkGray};
    display: flex;
    justify-content: space-between;

    .dd {
      display: inline-flex;
      flex-shrink: 0;
      .dt_icon {
        padding-top: 0.2rem;
        padding-left: 12px;
        font-size: 12px;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .ddWrap {
      align-items: center;
      .dt_paragraph {
        font-size: 0.75rem;
        line-height: 1.33;
        letter-spacing: normal;
        font-weight: normal;
        color: ${colors.ironGray};
      }
      .dt_paragraph.dd {
        font-size: 0.875rem;
        font-weight: 500;
        line-height: 1.43;
        min-height: 1.25rem;
        letter-spacing: normal;
        color: ${colors.darkGray};

        .dt_icon {
          padding-top: 0.3rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .ddWrap {
      align-items: center;
      .dt_paragraph {
        font-size: 1rem;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.5;
      }
      .dt_paragraph.dd {
        font-size: 1.125rem;
        line-height: 1.11;

        .dt_icon {
          padding-top: 0.3rem;
        }
      }
    }
  }
`;

export const StyledSectionBottom = styled.div`
  padding-top: 0.25rem;
  color: ${colors.mediumGray};
  display: flex;
  flex: 1;
  justify-content: space-between;
  .description {
    max-width: 8.3125rem;
    font-size: 0.75rem;
    line-height: 1rem;
    color: ${colors.mediumGray};
    letter-spacing: 0.24px;
    word-break: break-word;
  }
  .textRightBottom {
    padding-right: 0;
    display: flex;
    align-self: flex-end;

    .dt_paragraph {
      font-size: 0.75rem;
      line-height: 1rem;
      color: ${colors.darkGray};
      font-weight: 400;
    }

    /* .optionsList {
      width: 8.3rem;
      border-radius: 0.4rem;
      left: inherit;
      right: 0;
      margin-top: -1rem;
      div {
        color: ${colors.darkGray};
        padding: 0.25rem 0.75rem;
        font-size: 0.875rem;
        line-height: 1.25rem;
        font-weight: bold;
        height: auto;
        &:hover {
          background-color: ${colors.fogGray};
        }
      }
    }
    .simpleSelectWrap {
      .dt_icon {
        padding-top: 3px;
      }
    } */
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .description {
      max-width: 8rem;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .dt_section {
      max-width: 11.6rem;
      font-size: 0.625rem;
      line-height: 0.75rem;
    }
    .styledSimpleSelect {
      .dt_paragraph {
        font-size: 0.625rem;
      }
      .dt_icon {
        padding-top: 2px;
      }
    }

    .textRightBottom {
      .dt_select .styledSimpleSelect .dt_paragraph{
        font-size: 0.75rem;
        line-height: 1.33;
        color: ${colors.mediumGray};
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .dt_section {
      max-width: 9rem;
      font-size:0.75rem;
      line-height:1rem;
    }
  }
`;

export const StyledPlanSelect = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  border-radius: 0.5rem;
  background: ${colors.white};
  padding: 0.55rem 0.75rem 0.5rem 1rem;
  min-height: 5.5rem;
  position: relative;
  &.buyDeviceOnly,
  &.noLoyality {
    min-height: auto;
    padding: 1rem 0.75rem 1rem 1rem;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 1.125rem 0.8rem 1rem 1rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0.4rem 0.75rem 0.5rem;
    min-height: 4.5rem;
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    &.buyDeviceOnly,
    &.noLoyality {
      padding: 0.75rem 0.75rem 0.75rem 1rem;
    }
  }
`;
