import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';

import PlanSelectList, { IProps } from '.';

describe('<PlanSelectList />', () => {
  const props: IProps = {
    buyDeviceOnly: true,
    items: [],
    isOpen: true,
    selectedItem: null,
    dropDownType: 'down',
    deviceOnlyPrice: '12',
    productDetailedTranslation: appState().translation.cart.productDetailed,
    showTariffInfo: true,
    onItemSelect: jest.fn(),
    showAllPlans: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PlanSelectList {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, with items', () => {
    const item: IListItem[] = [
      {
        id: '12',
        title: 'title',
        disabled: true,
        demoKey: {},
        rest: { amount: 12 }
      }
    ];
    const newProps: IProps = { ...props, items: item };
    const component = mount(
      <ThemeProvider theme={{}}>
        <PlanSelectList {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
