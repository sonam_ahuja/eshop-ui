import React from 'react';
import { Paragraph } from 'dt-components';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';

import { StyledPlanSelectList } from './styles';

export interface IProps {
  buyDeviceOnly: boolean;
  items: IListItem[];
  isOpen: boolean;
  selectedItem?: IListItem | null;
  dropDownType: string;
  deviceOnlyPrice?: string;
  productDetailedTranslation: IProductDetailedTranslation;
  showTariffInfo?: boolean;
  onItemSelect(item: IListItem): void;
  showAllPlans(): void;
}
// tslint:disable-next-line:no-any
const PlanSelectList = (props: IProps) => {
  const {
    items,
    selectedItem,
    productDetailedTranslation,
    deviceOnlyPrice,
    isOpen,
    onItemSelect,
    showAllPlans,
    showTariffInfo
  } = props;
  if (!isOpen) {
    return null;
  }

  const dropdownItems = items.map((item, index) => (
    <li
      onClick={() => onItemSelect(item)}
      key={index}
      className={selectedItem && selectedItem.id === item.id ? 'active' : ''}
    >
      {item.id === '-1' ? (
        <Paragraph size='small' weight='bold'>
          <span className='text'>
            {productDetailedTranslation.buyDeviceOnly}
          </span>
          <span className='price'>{deviceOnlyPrice}</span>
        </Paragraph>
      ) : (
        <Paragraph size='small' weight='bold'>
          <span className='text'>{item.title}</span>
          <span className='price'>
            {item.rest.amount}
            <em> / {item.rest.monthlySymbol}</em>
          </span>
        </Paragraph>
      )}
    </li>
  ));

  return (
    <StyledPlanSelectList>
      <ul>
        {dropdownItems}
        {showTariffInfo && (
          <li className='seeAllPlans' onClick={showAllPlans}>
            <Paragraph size='small' weight='bold'>
              {productDetailedTranslation.seeAllPlans}
            </Paragraph>
          </li>
        )}
      </ul>
    </StyledPlanSelectList>
  );
};

export default PlanSelectList;
