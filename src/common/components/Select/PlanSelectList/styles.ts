import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledPlanSelectList = styled.div`
  position: absolute;
  z-index: 3;
  width: 100%;
  border-radius: 0.5rem;
  background: ${colors.white};
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.09);
  overflow: hidden;
  top: 50%;
  transform: translateY(-50%);

  li .dt_paragraph {
    padding: 0.625rem 0.8125rem 0.625rem 1rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: ${colors.darkGray};
    em {
      color: ${colors.mediumGray};
    }
    &:hover {
      background: ${colors.fogGray};
    }
  }

  li {
    &.seeAllPlans {
      .dt_paragraph {
        background: ${colors.charcoalGray};
        color: ${colors.white};
      }
    }
    &.active {
      .dt_paragraph {
        color: ${colors.magenta};
      }
    }
    &.default {
      background: ${colors.charcoalGray};
      .dt_paragraph {
        color: ${colors.white};
        &:hover {
          background: ${colors.charcoalGray};
        }
      }
    }
  }

  li .dt_paragraph {
    padding: 0.5rem 0.55rem 0.55rem 0.75rem;
    line-height: 1.14;
  }
`;
