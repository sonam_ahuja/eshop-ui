import React, { ReactNode } from 'react';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import { IconSizeType } from 'dt-components/lib/es/components/atoms/icon';
import { IProps as ISelectedItemProps } from 'dt-components/lib/es/components/molecules/select/components/selected-item';
import { Icon, Paragraph } from 'dt-components';

import { StyledSelectWithIcon } from './styles';

interface IProps extends ISelectedItemProps {
  className?: string;
  svgNode?: ReactNode;
  iconName?: iconNamesType;
  imageUrl?: string;
  iconSize?: IconSizeType;
  description: string;
}

const SelectWithIcon = (props: IProps) => {
  const {
    className,
    svgNode,
    iconName,
    imageUrl,
    description,
    iconSize,
    isOpen
  } = props;

  const svgEl = svgNode ? svgNode : null;
  const IconEl = iconName ? <Icon size={iconSize} name={iconName} /> : null;
  const ImageEl = imageUrl ? <img src={imageUrl} /> : null;

  return (
    <StyledSelectWithIcon className={className}>
      <div className='iconWrap'>
        {svgEl}
        {IconEl}
        {ImageEl}
      </div>
      <div className='description'>
        <Paragraph size='large'>{description}</Paragraph>
      </div>
      <Icon
        className='caret'
        size='inherit'
        name={isOpen ? 'ec-triangle-up' : 'ec-triangle-down'}
      />
    </StyledSelectWithIcon>
  );
};

export default SelectWithIcon;
