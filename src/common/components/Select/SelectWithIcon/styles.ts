import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSelectWithIcon = styled.div`
  display: flex;
  align-items: center;
  min-height: 6.25rem;
  color: ${colors.ironGray};

  .caret {
    justify-content: flex-end;
    margin-left: auto;
  }

  .iconWrap {
    margin-right: 1.25rem;
    display: inline-flex;
    width: 3.75rem;
    /* hack because image height stretched */
    flex-direction: column;

    svg {
      width: 100%;
      height: 100%;
    }

    img {
      display: block;
      max-width: 100%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: column;

    .iconWrap {
      margin: 0;
      margin-bottom: 1.75rem;
      width: 100%;
    }
  }
`;
