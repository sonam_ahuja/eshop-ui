import React, { FunctionComponent } from 'react';
import { Icon, Paragraph } from 'dt-components';
import cx from 'classnames';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';

import { StyledSimpleSelect } from './styles';
interface IListItem {
  id: string | number;
  title: string;
}
interface IProps {
  isOpen: boolean;
  selectedItem: IListItem | null;
  inheritFontStyles?: boolean;
  hideIcon: boolean;
  caretICon?: iconNamesType;
  onClick?(event: React.MouseEvent<HTMLInputElement, MouseEvent>): void;
}
/** USED ON PROLONGATION */
const SimpleSelect: FunctionComponent<IProps> = (props: IProps) => {
  const {
    onClick,
    selectedItem,
    inheritFontStyles,
    hideIcon,
    caretICon = 'ec-triangle-down'
  } = props;

  const classes = cx('styledSimpleSelect', { inheritFontStyles });

  return (
    <StyledSimpleSelect
      className={classes}
      onClick={(event: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
        if (event && onClick) {
          event.stopPropagation();
          onClick(event);
        }
      }}
    >
      <Paragraph size='large' weight='medium'>
        {selectedItem && selectedItem.title}
      </Paragraph>
      {!hideIcon && <Icon size='inherit' name={caretICon} />}
    </StyledSimpleSelect>
  );
};

export default SimpleSelect;
