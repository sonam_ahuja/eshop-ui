import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSimpleSelect = styled.div`
  display: inline-flex;

  .dt_icon {
    padding-top: 0.2rem;
    padding-left: 12px;
    font-size: 12px;
  }

  & + .optionsList {
    width: 8rem;
    right: 0;
    font-size: 0.7rem;
    border-radius: 0.35rem;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: ${colors.darkGray};

    > div {
      padding: 0.25rem 0.6rem;
      height: auto;
    }
  }

  &.inheritFontStyles {
    .dt_paragraph {
      font-size: inherit;
      font-weight: inherit;
      line-height: inherit;
      letter-spacing: inherit;
      color: ${colors.magenta};
    }
    .dt_icon {
      padding-top: 0;
      padding-left: 3px;
      align-self: center;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .dt_paragraph {
      font-size: 0.875rem;
      font-weight: 500;
      line-height: 1.43;
      letter-spacing: normal;
      color: ${colors.darkGray};
    }
    .dt_icon {
      padding-top: 0.3rem;
    }

    & + .optionsList {
      width: auto;
      white-space: nowrap;
      font-size: 0.875rem;
      line-height: 1.43;
      > div {
        padding: 0.25rem 0.75rem;
      }
    }
  }
`;
