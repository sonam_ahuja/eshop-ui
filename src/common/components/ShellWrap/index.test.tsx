import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import StyledShell from '.';

describe('<StyledShell />', () => {
  test('should render properly', () => {
    const props = {};

    const component = mount(
      <ThemeProvider theme={{}}>
        <StyledShell {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
