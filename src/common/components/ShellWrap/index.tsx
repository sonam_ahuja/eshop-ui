import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import {
  StyledFilterHeaderDesktopShell,
  StyledFilters
} from '@src/common/routes/search/index.shell';

/** USED ON PROLONGATION */
const StyledShell = styled.div`
  width: auto;
  /* background: ${colors.white}; */
  .shine {
    overflow: hidden;
    position: relative;
  }
  .shine:after {
    content: '';
    top: 0;
    left: 0;
    transform: translateX(100%);
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 1;
    animation: slide 1s infinite;
  }

  &.primary {
    .shine {
      background: ${colors.cloudGray};
    }
    .shine:after {
      background: ${colors.silverGray};
    }
  }

  &.secondary {
    .shine {
      background: ${colors.shadowGray};
    }
    .shine:after {
      background: ${colors.darkGray};
    }
  }

  .shine.primary {
    background: ${colors.cloudGray};
    &:after {
      background: ${colors.silverGray};
    }
  }
  .shine.secondary {
    background: ${colors.shadowGray};
    &:after {
      background: ${colors.darkGray};
    }
  }

  @keyframes slide {
    0% {
      transform: translateX(-100%);
    }
    100% {
      transform: translateX(100%);
    }
  }
  .visible-xs {
    ${StyledFilterHeaderDesktopShell} {
      display: block;
      padding: 1.351rem 0rem 1.4rem 1.25rem;
      .message {
        display: none;
      }
    }
    ${StyledFilters} {
      display: block;
      height: 4rem;
      background: ${colors.white};
      padding-left: 1.25rem;
      .filter {
        height: 100%;
        margin-bottom: 0;
      }
    }
  }
  .hidden-xs {
    ${StyledFilterHeaderDesktopShell},
    ${StyledFilters} {
      display: none;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .visible-lg {
      ${StyledFilterHeaderDesktopShell},
      ${StyledFilters} {
        display: block;
      }
    }
    .hidden-lg {
      ${StyledFilterHeaderDesktopShell},
      ${StyledFilters} {
        display: none;
      }
    }
  }
`;
export default StyledShell;
