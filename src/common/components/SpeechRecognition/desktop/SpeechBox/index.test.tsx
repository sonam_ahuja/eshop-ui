import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import SpeechBox, {
  IProps
} from '@src/common/components/SpeechRecognition/desktop/SpeechBox';

describe('<SpeechBox />', () => {
  const props: IProps = {
    searchTranslation: appState().translation.cart.global.search,
    listening: true,
    isNoMatch: true,
    isError: true,
    isListeningStop: true,
    isMicNotAllowed: true,
    stopListening: jest.fn(),
    startListening: jest.fn(),
    isNoSpeech: true,
    isStartSpeaking: true,
    onMicOffWithTimer: jest.fn(),
    clearAllTimer: jest.fn(),
    onMicOff: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  const componentWrapper = (newProps: IProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SpeechBox {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

  test('should render properly', () => {
    const wrapper = componentWrapper(props);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when listening true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: true,
      isNoMatch: false,
      isError: false,
      isListeningStop: false,
      isMicNotAllowed: false,
      isNoSpeech: false,
      isStartSpeaking: false
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when isNoMatch true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: false,
      isNoMatch: true,
      isError: false,
      isListeningStop: false,
      isMicNotAllowed: false,
      isNoSpeech: false,
      isStartSpeaking: false
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when isError true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: false,
      isNoMatch: false,
      isError: true,
      isListeningStop: false,
      isMicNotAllowed: false,
      isNoSpeech: false,
      isStartSpeaking: false
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when isListeningStop true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: false,
      isNoMatch: false,
      isError: false,
      isListeningStop: true,
      isMicNotAllowed: false,
      isNoSpeech: false,
      isStartSpeaking: false
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when isMicNotAllowed true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: false,
      isNoMatch: false,
      isError: false,
      isListeningStop: false,
      isMicNotAllowed: true,
      isNoSpeech: false,
      isStartSpeaking: false
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when isNoSpeech true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: false,
      isNoMatch: false,
      isError: false,
      isListeningStop: false,
      isMicNotAllowed: false,
      isNoSpeech: true,
      isStartSpeaking: false
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });

  test('should render properly, when isStartSpeaking true', () => {
    const updatedProps: IProps = {
      ...props,
      listening: false,
      isNoMatch: false,
      isError: false,
      isListeningStop: false,
      isMicNotAllowed: false,
      isNoSpeech: false,
      isStartSpeaking: true
    };
    const wrapper = componentWrapper(updatedProps);
    expect(wrapper).toMatchSnapshot();
  });
});
