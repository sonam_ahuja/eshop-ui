import React, { Fragment, FunctionComponent } from 'react';
import { Icon } from 'dt-components';
import { ISearchTranslation } from '@store/types/translation';
import cx from 'classnames';
import { timerEvents } from '@src/common/components/SpeechRecognition/utils/check';

import {
  StyledActionText,
  StyledIcon,
  StyledSpeechBox,
  StyledText
} from './styles';

export interface IProps {
  listening: boolean;
  isNoMatch: boolean;
  isListeningStop: boolean;
  isMicNotAllowed: boolean;
  isNoSpeech: boolean;
  isError: boolean;
  isStartSpeaking: boolean;
  searchTranslation: ISearchTranslation;
  onMicOff(): void;
  onMicOffWithTimer(time: number): void;
  startListening(): void;
  clearAllTimer(enable: boolean): void;
  stopListening(): void;
}

const SpeechBox: FunctionComponent<IProps> = (props: IProps) => {
  const {
    listening,
    startListening,
    isNoMatch,
    isError,
    isMicNotAllowed,
    onMicOff,
    stopListening,
    isListeningStop,
    onMicOffWithTimer,
    isStartSpeaking,
    clearAllTimer,
    isNoSpeech
  } = props;
  const { desktop } = props.searchTranslation.searchField;

  if (isError || isNoMatch) {
    stopListening();
  }

  if (isMicNotAllowed) {
    onMicOffWithTimer(5000);
  }

  if (
    isListeningStop &&
    !listening &&
    !isNoMatch &&
    !isMicNotAllowed &&
    !isError &&
    !isNoSpeech
  ) {
    onMicOff();
  }

  if (
    timerEvents(
      isMicNotAllowed,
      isListeningStop,
      listening,
      isNoMatch,
      isError,
      isNoSpeech
    )
  ) {
    onMicOffWithTimer(timerEvents(
      isMicNotAllowed,
      isListeningStop,
      listening,
      isNoMatch,
      isError,
      isNoSpeech
    ) as number);
  }

  const classes = cx({ listening: !isMicNotAllowed && !isError && !isNoMatch });

  return (
    <StyledSpeechBox className={classes}>
      <StyledIcon>
        <Icon size='inherit' color='currentColor' name={'ec-audio-recording'} />
      </StyledIcon>
      {isMicNotAllowed && (
        <StyledText>{desktop.voiceSearchHasBeenTurnOff}</StyledText>
      )}
      {isNoMatch && (
        <Fragment>
          <StyledText>{desktop.doNotUnderstood}</StyledText>
          <StyledActionText
            onClick={() => {
              startListening();
              clearAllTimer(true);
            }}
          >
            {desktop.repeat}
          </StyledActionText>
        </Fragment>
      )}
      {isNoSpeech && <StyledText>{desktop.checkMicAndAudio}</StyledText>}
      {!isMicNotAllowed && (
        <Fragment>
          {listening && <StyledText>{desktop.listening}</StyledText>}
          {isStartSpeaking && <StyledText>{desktop.startSpeaking}</StyledText>}
        </Fragment>
      )}
    </StyledSpeechBox>
  );
};
export default SpeechBox;
