import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';
import { Paragraph } from 'dt-components';

export const StyledIcon = styled.div`
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${colors.mediumGray};
  font-size: 1rem;
`;

export const StyledText = styled(Paragraph).attrs({
  size: 'medium',
  weight: 'bold'
})`
  color: ${colors.mediumGray};
  margin-left: 0.5rem;
`;
export const StyledActionText = styled.a`
  font-size: 0.875rem;
  font-weight: normal;
  cursor: pointer;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.43;
  letter-spacing: normal;
  color: ${colors.magenta};
  margin-left: 1.15rem;
  /* &::first-letter {
    text-transform: uppercase;
  } */
`;
export const StyledSpeechBox = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: center;
  padding: 0.25rem;
  position: absolute;

  &.listening {
    ${StyledIcon} {
      background-color: ${hexToRgbA(colors.magenta, 0.2)};
      color: ${colors.magenta};
    }
  }
`;
