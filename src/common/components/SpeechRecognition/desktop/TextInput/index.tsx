import React, { FunctionComponent } from 'react';
import { Icon } from 'dt-components';
import { ISearchTranslation } from '@store/types/translation';
import { checkSpeech } from '@src/common/components/SpeechRecognition/utils/check';
import { isMobile } from '@common/utils';
import { ISearchState } from '@search/store/types';

import { StyledInputBox } from './styles';

export interface IProps {
  inputValue: string;
  isSuggestionVisible: boolean;
  searchTranslation: ISearchTranslation;
  search: ISearchState;
  isHiddenText: boolean;
  isMicNotAllowed: boolean;
  resetText(): void;
  openModal(): void;
  startListening(): void;
  setSpeechRecognition(visible: boolean): void;
  getRef?(el: HTMLElement | null): void;
  onChange(event: React.ChangeEvent<HTMLInputElement>): void;
  onFocus(event: React.FocusEvent<HTMLInputElement>): void;
  onKeyDown(event: React.KeyboardEvent<Element>): void;
}

const TextInput: FunctionComponent<IProps> = (props: IProps) => {
  const {
    inputValue,
    onChange,
    onFocus,
    onKeyDown,
    isSuggestionVisible,
    resetText,
    startListening,
    setSpeechRecognition,
    openModal,
    isHiddenText,
    isMicNotAllowed,
    getRef,
    search
  } = props;

  const { desktop } = props.searchTranslation.searchField;

  return (
    <StyledInputBox onClick={openModal}>
      <Icon
        className='searchIcon'
        size='inherit'
        color='currentColor'
        name='ec-solid-search'
      />

      <div className='inputWrap'>
        <input
          type='text'
          ref={getRef}
          placeholder={
            !isMobile.phone && isSuggestionVisible ? desktop.startTyping : ''
          }
          value={inputValue}
          onChange={onChange}
          onFocus={onFocus}
          onKeyDown={onKeyDown}
        />
        {inputValue.length > 0 && isSuggestionVisible && !isHiddenText && (
          <div className='backgroundText'>
            <div className='backgroundTextInner'>
              <span className='hiddenText'>{search.search.activeText}</span>
              <span>{search.search.inActiveText}</span>
            </div>
          </div>
        )}
      </div>

      <div
        className='iconRight'
        onClick={(event: React.MouseEvent<HTMLInputElement>) => {
          if (event) {
            if (inputValue.trim().length) {
              resetText();
            } else {
              if (!isMicNotAllowed) {
                startListening();
                setSpeechRecognition(true);
              }
            }
          }
        }}
      >
        {inputValue.trim().length > 0 ? (
          <Icon className='icons' color='currentColor' name={'ec-error'} />
        ) : (
          <>
            {checkSpeech() && (
              <Icon
                className='icons'
                color='currentColor'
                name={'ec-audio-recording'}
              />
            )}
          </>
        )}
      </div>
    </StyledInputBox>
  );
};
export default TextInput;
