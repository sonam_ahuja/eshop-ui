import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledInputBox = styled.div`
  &,
  .inputWrap,
  .inputWrap input,
  .inputWrap .backgroundText {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    border-radius: inherit;
    border: 0;
    box-shadow: none;
    text-align: left;
    align-items: center;
    display: flex;
    padding: 0 3.25rem;
    background: none;
    outline: none;

    font-size: 1rem;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: ${colors.mediumGray};
  }

  .inputWrap {
    input {
      z-index: 3;
      caret-color: ${colors.transparent};
      color: ${colors.darkGray};
    }
    .backgroundText {
      z-index: 2;
      padding-top: 1px;
      .backgroundTextInner {
        overflow: hidden;
        max-width: 100%;
        white-space: nowrap;
        span.hiddenText {
          visibility: hidden;
        }
      }
    }
  }

  .searchIcon,
  .iconRight {
    font-size: 1.5rem;
    height: 1.5rem;
    line-height: 0;
    z-index: 1;
    position: absolute;
    right: 1rem;
    top: 0;
    bottom: 0;
    margin: auto;
    color: ${colors.ironGray};
  }

  .iconRight {
    display: none;
    z-index: 4;
    cursor: pointer;
  }

  input::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: ${colors.mediumGray};
  }
  input::-moz-placeholder {
    /* Firefox 19+ */
    color: ${colors.mediumGray};
  }
  input:-ms-input-placeholder {
    /* IE 10+ */
    color: ${colors.mediumGray};
  }
  input:-moz-placeholder {
    /* Firefox 18- */
    color: ${colors.mediumGray};
  }
`;
