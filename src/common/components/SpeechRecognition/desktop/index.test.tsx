import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import {
  DesktopSpeechInput,
  IComponentProps
} from '@src/common/components/SpeechRecognition/desktop';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { combineSuggestionResult as dumpResult } from '@src/common/mock-api/search/search.mock';
import { KEY_CODE } from '@search/store/enum';

describe('<DesktopSpeechInput />', () => {
  const props: IComponentProps = {
    ...histroyParams,
    transcript: 'string',
    isNoMatch: true,
    isStartSpeaking: true,
    isNoSpeech: true,
    isListeningStop: true,
    isMicNotAllowed: true,
    isError: true,
    browserSupportsSpeechRecognition: true,
    recognition: {},
    resetTranscript: jest.fn(),
    startListening: jest.fn(),
    abortListening: jest.fn(),
    stopListening: jest.fn(),
    getParentText: jest.fn(),
    clearAllTimerSpeechHoc: jest.fn(),
    listening: true,
    searchTranslation: appState().translation.cart.global.search,
    searchConfiguration: appState().configuration.cms_configuration.global
      .search,
    search: appState().search,
    setSpeechRecognition: jest.fn(),
    fetchSuggestion: jest.fn(),
    fetchCategories: jest.fn(),
    clearSuggestionData: jest.fn(),
    setSearchModal: jest.fn(),
    setListingEnable: jest.fn(),
    updateShowAppShell: jest.fn(),
    setInputWithNoText: jest.fn(),
    setTempText: jest.fn(),
    setInputContent: jest.fn(),
    setSearchPriorityText: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <DesktopSpeechInput {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const onChangeEvent = { target: { value: 'avinash' } };
    const onKeyArrowDownEvent = { keyCode: KEY_CODE.ARROW_DOWN };
    const onKeyArrowRightEvent = { keyCode: KEY_CODE.ARROW_RIGHT };
    const onKeyArrowUpEvent = {
      keyCode: KEY_CODE.ARROW_UP,
      // tslint:disable-next-line: no-empty
      preventDefault: () => {}
    };
    const onKeyDownEvent = { keyCode: KEY_CODE.ENTER };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <DesktopSpeechInput {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).resetSearchPriorityText();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onInputChange(
      onChangeEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onKeyDown(
      onKeyDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setContentOnIndexing(dumpResult, 1);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setState({ currentIndex: 12 });
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onIndexing(
      onKeyArrowDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onIndexing(
      onKeyArrowRightEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onIndexing(
      onKeyArrowUpEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onIndexing(
      onKeyDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).removeSuggestion('google');
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setSuggestionVisibilityWithTimer(
      'google',
      true
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setSelection('google');
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).openModal();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).enableFilterListing(true);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).resetText();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).enableFilterListing(true);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setInputValue('email');
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).suggestionListIndexing(
      onKeyDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).suggestionListIndexing(
      onKeyDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setSuggestionVisibilityWithTimer(
      'email',
      true
    );
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setSuggestionVisibility(true);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setInputValue('string');
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onMicOff();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onMicOffWithTimer(2000);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setRef(null);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).clearAllTimer(true);
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setSpeechRecognition();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).setSuggestionVisible();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).onOutSideClick();
    (component
      .find('DesktopSpeechInput')
      .instance() as DesktopSpeechInput).componentWillUnmount();
  });
});
