import { suggestionVisibilityTypeDebounceFn } from '@common/components/SpeechRecognition/utils/speechDebounce';
import { KEY_CODE, SEARCH_ROUTE } from '@search/store/enum';
import { RouteComponentProps, withRouter } from 'react-router';
import { isMobile } from '@common/utils';
import { getSelectedFiltersFromParams } from '@search/utils';
import cx from 'classnames';
import { removeLocalStorageSuggestion } from '@common/components/SpeechRecognition/utils/localStorageOperation';
import { OutsideClick, Utils } from 'dt-components';
import { connect } from 'react-redux';
import React, { Component, ReactNode } from 'react';
import SpeechRecognitionHOC from '@common/components/SpeechRecognition/utils/SpeechRecognitionHOC';
import SpeechBox from '@src/common/components/SpeechRecognition/desktop/SpeechBox';
import TextInput from '@common/components/SpeechRecognition/desktop/TextInput';
import {
  IComponentDispatchToProps,
  IComponentStateToProps
} from '@common/components/SpeechRecognition/types';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@common/components/SpeechRecognition/mapProps';
import {
  ISuggestionConverter,
  searchSuggestionResolver
} from '@search/store/transformer';
import SearchSuggestion from '@search/Common/SearchSuggestion';
import {
  StyledSearchSuggestion,
  StyledSearchWrapper
} from '@common/components/SpeechRecognition/desktop/styles';
import {
  sendHeaderClickEvent,
  sendSearchClickEvent
} from '@events/common/index';
import { SEARCH_MODE } from '@events/constants/eventName';

export interface IProps {
  transcript: string;
  browserSupportsSpeechRecognition: boolean;
  recognition: object;
  listening: boolean;
  isNoMatch: boolean;
  isStartSpeaking: boolean;
  isNoSpeech: boolean;
  isListeningStop: boolean;
  isMicNotAllowed: boolean;
  isError: boolean;
  clearAllTimerSpeechHoc(): void;
  resetTranscript(): void;
  startListening(): void;
  stopListening(): void;
  abortListening(): void;
  getParentText(text: string): void;
}
export interface IState {
  inputValue: string;
  isInputChange: boolean;
  localStorageUpdated: boolean;
  suggestionVisibility: boolean;
  optionalVisibility: boolean;
  el: HTMLElement | null;
  isSpeechRecognition: boolean;
  isSearchClick: boolean;
  currentIndex: number;
  maxIndex: number;
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps;

export class DesktopSpeechInput extends Component<IComponentProps, IState> {
  map: {
    ref: number | null;
  }[] = [];

  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      inputValue: this.props.transcript,
      isSearchClick: false,
      el: null,
      localStorageUpdated: false,
      suggestionVisibility: false,
      isSpeechRecognition: false,
      optionalVisibility: false,
      isInputChange: false,
      currentIndex: -1,
      maxIndex: 0
    };
    const { searchDebounce } = this.props.searchConfiguration;
    this.onInputChange = this.onInputChange.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.updateUrlPath = this.updateUrlPath.bind(this);
    this.removeSuggestion = this.removeSuggestion.bind(this);
    this.setSuggestionVisibility = this.setSuggestionVisibility.bind(this);
    this.openModal = this.openModal.bind(this);
    this.setSpeechRecognition = this.setSpeechRecognition.bind(this);
    this.enableFilterListing = this.enableFilterListing.bind(this);
    this.setSuggestionVisibilityWithTimer = suggestionVisibilityTypeDebounceFn(
      this.setSuggestionVisibilityWithTimer.bind(this)
    );
    this.setSelection = this.setSelection.bind(this);
    this.resetText = this.resetText.bind(this);
    this.suggestionListIndexing = this.suggestionListIndexing.bind(this);
    this.onIndexing = this.onIndexing.bind(this);
    this.setContentOnIndexing = this.setContentOnIndexing.bind(this);
    this.resetSearchPriorityText = this.resetSearchPriorityText.bind(this);
    this.onMicOff = this.onMicOff.bind(this);
    this.onMicOffWithTimer = this.onMicOffWithTimer.bind(this);
    this.setRef = this.setRef.bind(this);
    this.clearAllTimer = this.clearAllTimer.bind(this);
    this.setSuggestionVisible = this.setSuggestionVisible.bind(this);
    this.onOutSideClick = this.onOutSideClick.bind(this);
    this.suggestionApi = Utils.debounce(
      this.suggestionApi,
      searchDebounce
    ).bind(this);
  }

  componentDidMount(): void {
    const { setTempText } = this.props;
    const { ...params } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    if (params.query && params.query.length > 0) {
      this.setState({ inputValue: params.query, isInputChange: true }, () => {
        setTempText(params.query);
        this.suggestionApi(params.query);
      });
    } else {
      this.setState({ inputValue: '' });
      setTempText('');
    }
  }

  suggestionApi(value: string): void {
    this.props.fetchSuggestion(value.trim());
  }

  componentDidUpdate(prevProps: IComponentProps): void {
    const {
      transcript: newTranscript,
      search: newSearch,
      location: newLocation,
      setTempText
    } = this.props;
    const {
      transcript: oldTranscript,
      search: oldSearch,
      location: oldLocation
    } = prevProps;
    if (oldTranscript !== newTranscript && newTranscript.trim().length) {
      this.setState(
        {
          inputValue: newTranscript,
          isInputChange: true
        },
        () => {
          this.updateUrlPath(newTranscript);
          this.suggestionApi(newTranscript);
          sendSearchClickEvent(newTranscript, SEARCH_MODE.VOICE_MODE);
          setTempText(newTranscript);
          this.enableFilterListing(false);
        }
      );
    }
    if (
      oldSearch.searchFilterSetting.inputContent !==
      newSearch.searchFilterSetting.inputContent
    ) {
      this.setState({ inputValue: newSearch.searchFilterSetting.inputContent });
    }
    const { ...params } = getSelectedFiltersFromParams(
      this.props.location.search
    );

    if (newLocation !== oldLocation) {
      this.setState({ inputValue: params.query });
    }
  }

  setInputValue(inputValue: string): void {
    const { isSpeechRecognition, localStorageUpdated } = this.state;
    if (isSpeechRecognition && inputValue.trim().length) {
      this.suggestionApi(inputValue);
      this.updateUrlPath(inputValue);
      this.setState({
        localStorageUpdated: !localStorageUpdated
      });
    }
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputValue = (event.target as HTMLInputElement).value;
    const { setInputWithNoText, setTempText } = this.props;
    if (inputValue.trim().length) {
      setInputWithNoText(false);
      this.enableFilterListing(false);
      this.suggestionApi(inputValue);
      setTempText(inputValue);
      this.setState({ inputValue, isInputChange: true, currentIndex: -1 });
    } else {
      this.setState({
        inputValue,
        isInputChange: false,
        currentIndex: -1,
        suggestionVisibility: true,
        optionalVisibility: true
      });
      this.enableFilterListing(false);
      setTempText('');
    }
  }

  onKeyDown(event: React.KeyboardEvent<Element>): void {
    const { inputValue, localStorageUpdated } = this.state;
    const { setTempText } = this.props;
    if (event.keyCode === KEY_CODE.ENTER && inputValue.trim().length) {
      sendSearchClickEvent(inputValue.trim(), SEARCH_MODE.TYPED_MODE);
      const { el } = this.state;
      if (el) {
        el.blur();
      }
      this.setState(
        {
          inputValue,
          localStorageUpdated: !localStorageUpdated,
          suggestionVisibility: false,
          optionalVisibility: false,
          isInputChange: true
        },
        () => {
          this.enableFilterListing(false);
          setTempText(inputValue);
          this.updateUrlPath(inputValue.trim());
          this.resetSearchPriorityText();
        }
      );
    }
  }

  setContentOnIndexing(
    combineSuggestionResult: ISuggestionConverter[],
    index: number
  ): void {
    const { setInputContent } = this.props;
    if (combineSuggestionResult.length && combineSuggestionResult[index]) {
      setInputContent(combineSuggestionResult[index].fullText);
    }
  }

  // tslint:disable-next-line: cognitive-complexity
  onIndexing(event: React.KeyboardEvent<Element>): void {
    const { suggestion, searchFilterSetting } = this.props.search;
    const { inActiveText, activeText } = this.props.search.search;
    const { setInputContent, setInputWithNoText } = this.props;
    const { inputValue } = this.state;
    const { currentIndex } = this.state;
    const { searchQueries } = this.props.searchTranslation;
    let newIndex = Number(currentIndex);
    if (!searchFilterSetting.isInputWithNoText && !inputValue.length) {
      setInputWithNoText(true);
    }
    const combineSuggestionResult: ISuggestionConverter[] = searchSuggestionResolver(
      suggestion.data,
      searchQueries,
      searchFilterSetting.originalContent,
      inputValue,
      searchFilterSetting.showListing,
      currentIndex
    );
    if (event.keyCode === KEY_CODE.ARROW_UP && newIndex > 0) {
      this.enableFilterListing(true);
      event.preventDefault();
      newIndex--;
      this.setState({ currentIndex: newIndex, isInputChange: false }, () => {
        this.setContentOnIndexing(combineSuggestionResult, newIndex);
      });
    }
    if (
      event.keyCode === KEY_CODE.ARROW_DOWN &&
      newIndex < combineSuggestionResult.length - 1
    ) {
      this.enableFilterListing(true);
      newIndex++;
      if (newIndex === 0) {
        this.setState({
          currentIndex: newIndex,
          isInputChange: false,
          inputValue: combineSuggestionResult[newIndex].fullText
        });
      } else {
        this.setState({ currentIndex: newIndex, isInputChange: false }, () => {
          this.setContentOnIndexing(combineSuggestionResult, newIndex);
        });
      }
    }
    if (
      event.keyCode === KEY_CODE.ARROW_RIGHT &&
      `${activeText}${inActiveText}`.length &&
      inputValue !== `${activeText}${inActiveText}`
    ) {
      this.setState(
        {
          isInputChange: false,
          inputValue: `${activeText}${inActiveText}`,
          currentIndex: -1
        },
        () => {
          if (
            combineSuggestionResult.length &&
            combineSuggestionResult[newIndex]
          ) {
            setInputContent(`${activeText}${inActiveText}`);
          }
        }
      );
    }
  }

  suggestionListIndexing(event: React.KeyboardEvent<Element>): void {
    this.onIndexing(event);
    this.resetSearchPriorityText();
  }

  resetSearchPriorityText(): void {
    const { setSearchPriorityText } = this.props;
    const { inActiveText, activeText } = this.props.search.search;
    if (
      inActiveText &&
      activeText &&
      inActiveText.length &&
      activeText.length
    ) {
      setSearchPriorityText({
        activeText: '',
        inActiveText: ''
      });
    }
  }

  updateUrlPath(text: string): void {
    this.props.fetchCategories({
      query: text,
      url: this.props.location.pathname,
      type: SEARCH_ROUTE.SEARCH_RESULT,
      isNewHit: true
    });
  }

  removeSuggestion(suggestion: string): void {
    const { localStorageUpdated } = this.state;
    this.enableFilterListing(false);
    const isSuggestionRemoved = removeLocalStorageSuggestion(suggestion);
    if (isSuggestionRemoved) {
      this.setState({
        localStorageUpdated: !localStorageUpdated,
        currentIndex: -1
      });
    }
  }

  setSuggestionVisibilityWithTimer(timerRef: string, visible: boolean): void {
    if (timerRef) {
      this.setState({
        suggestionVisibility: visible,
        optionalVisibility: visible
      });
    }
  }

  setSuggestionVisibility(visible: boolean): void {
    this.setState({
      suggestionVisibility: visible,
      optionalVisibility: visible,
      isSpeechRecognition: false,
      isSearchClick: false
    });
  }

  setSelection(text: string): void {
    const { isSpeechRecognition, localStorageUpdated } = this.state;
    const { setTempText } = this.props;
    if (isSpeechRecognition) {
      this.props.stopListening();
    }
    sendSearchClickEvent(text, SEARCH_MODE.TYPED_MODE);
    setTempText(text);
    this.suggestionApi(text);
    this.setState({
      inputValue: text,
      isSpeechRecognition: false,
      isInputChange: true,
      optionalVisibility: false,
      localStorageUpdated: !localStorageUpdated
    });
    this.resetSearchPriorityText();
    this.updateUrlPath(text);
  }

  openModal(): void {
    if (isMobile.phone) {
      this.props.setSearchModal(true);
    } else {
      this.setState({
        isSearchClick: true,
        suggestionVisibility: true,
        optionalVisibility: true
      });
    }
  }

  enableFilterListing(isEnable: boolean): void {
    const { searchFilterSetting } = this.props.search;
    const { setListingEnable } = this.props;
    if (searchFilterSetting.showListing !== isEnable) {
      setListingEnable(isEnable);
    }
  }

  clearAllTimer(enable: boolean): void {
    if (this.map.length && enable) {
      this.setSuggestionVisible();
    }
    this.map.forEach((value: { ref: number | null }) => {
      if (value && value.ref) {
        clearTimeout(value.ref);
      }
    });
    this.props.clearAllTimerSpeechHoc();
    this.map = [];
  }

  setSuggestionVisible(): void {
    const { suggestionVisibility } = this.state;
    if (!suggestionVisibility) {
      this.setState({ suggestionVisibility: true });
    }
  }

  onMicOffWithTimer(time: number): void {
    const { suggestionVisibility } = this.state;
    if (suggestionVisibility) {
      this.setState(
        { suggestionVisibility: false, optionalVisibility: false },
        () => {
          const onMicOffWithTimerRef = setTimeout(() => {
            this.setState({
              isSpeechRecognition: false
            });
            this.props.stopListening();
            this.clearAllTimer(false);
          }, time);
          this.map.push({ ref: onMicOffWithTimerRef });
        }
      );
    }
  }

  // tslint:disable-next-line: no-identical-functions
  onMicOff(): void {
    this.setState({
      isSpeechRecognition: false,
      suggestionVisibility: false,
      optionalVisibility: false
    });
    this.props.stopListening();
  }

  resetText(): void {
    const { el } = this.state;
    const { setTempText } = this.props;
    if (el) {
      el.focus();
    }
    this.enableFilterListing(false);
    setTempText('');
    this.setState({ inputValue: '', currentIndex: -1, isInputChange: false });
  }

  setSpeechRecognition(): void {
    this.setState({ isSpeechRecognition: true });
  }

  setRef(el: HTMLElement | null): void {
    if (el) {
      this.setState({ el });
    }
  }

  componentWillUnmount(): void {
    this.clearAllTimer(false);
  }

  onOutSideClick(): void {
    const { stopListening, setTempText } = this.props;
    const { searchFilterSetting } = this.props.search;
    const { inputValue } = this.state;
    this.setSuggestionVisibility(false);
    this.clearAllTimer(false);
    stopListening();
    if (inputValue.length && searchFilterSetting.showListing) {
      this.setState({ isInputChange: true });
      setTempText(inputValue);
      this.enableFilterListing(false);
      this.suggestionApi(inputValue);
    }
  }

  render(): ReactNode {
    const {
      inputValue,
      suggestionVisibility,
      optionalVisibility,
      isSpeechRecognition,
      isInputChange,
      currentIndex
    } = this.state;

    const { suggestion, searchFilterSetting } = this.props.search;
    const { searchQueries } = this.props.searchTranslation;
    const {
      setSearchPriorityText,
      search,
      startListening,
      stopListening,
      setInputContent,
      isMicNotAllowed
    } = this.props;
    const combineSuggestionResult: ISuggestionConverter[] = searchSuggestionResolver(
      suggestion.data,
      searchQueries,
      searchFilterSetting.originalContent,
      inputValue,
      searchFilterSetting.showListing,
      currentIndex
    );

    if (!combineSuggestionResult.length) {
      this.resetSearchPriorityText();
    }

    if (
      combineSuggestionResult.length &&
      searchFilterSetting.showListing &&
      combineSuggestionResult[currentIndex]
    ) {
      setInputContent(combineSuggestionResult[currentIndex].fullText);
    }

    const classes = cx({
      isActive:
        suggestionVisibility || isSpeechRecognition || inputValue.length,
      focus: (suggestionVisibility || isSpeechRecognition) && optionalVisibility
    });

    return (
      <>
        <StyledSearchWrapper className={classes}>
          <OutsideClick clickHandler={this.onOutSideClick}>
            <div onKeyDown={this.suggestionListIndexing}>
              {(!isSpeechRecognition || isMicNotAllowed) && (
                <TextInput
                  search={search}
                  inputValue={inputValue}
                  getRef={this.setRef}
                  isHiddenText={searchFilterSetting.showListing}
                  isSuggestionVisible={suggestionVisibility}
                  searchTranslation={this.props.searchTranslation}
                  isMicNotAllowed={isMicNotAllowed}
                  setSpeechRecognition={(visible: boolean) => {
                    this.setState({ isSpeechRecognition: visible });
                    if (!visible) {
                      this.props.stopListening();
                    }
                  }}
                  resetText={this.resetText}
                  openModal={this.openModal}
                  startListening={startListening}
                  onFocus={(event: React.FocusEvent<HTMLInputElement>) => {
                    this.enableFilterListing(false);
                    sendHeaderClickEvent('search input');
                    if (event) {
                      event.stopPropagation();
                    }
                  }}
                  onChange={this.onInputChange}
                  onKeyDown={this.onKeyDown}
                />
              )}
              {isSpeechRecognition && !isMicNotAllowed && (
                <SpeechBox
                  onMicOff={this.onMicOff}
                  onMicOffWithTimer={this.onMicOffWithTimer}
                  startListening={startListening}
                  clearAllTimer={this.clearAllTimer}
                  stopListening={stopListening}
                  isNoSpeech={this.props.isNoSpeech}
                  listening={this.props.listening}
                  isNoMatch={this.props.isNoMatch}
                  isMicNotAllowed={this.props.isMicNotAllowed}
                  isError={this.props.isError}
                  isStartSpeaking={this.props.isStartSpeaking}
                  isListeningStop={this.props.isListeningStop}
                  searchTranslation={this.props.searchTranslation}
                />
              )}
            </div>
            {suggestionVisibility &&
              optionalVisibility &&
              combineSuggestionResult.length &&
              !isMobile.phone ? (
                <StyledSearchSuggestion>
                  <SearchSuggestion
                    combineSuggestionResult={combineSuggestionResult}
                    isInputChange={isInputChange}
                    inputValue={inputValue}
                    isListingEnable={searchFilterSetting.showListing}
                    setSearchPriorityText={setSearchPriorityText}
                    setSelection={this.setSelection}
                    removeSuggestion={this.removeSuggestion}
                  />
                </StyledSearchSuggestion>
              ) : null}
          </OutsideClick>
        </StyledSearchWrapper>
      </>
    );
  }
}

export default SpeechRecognitionHOC({
  autoStart: false,
  continuous: false
})(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(DesktopSpeechInput)
  )
  // tslint:disable-next-line:max-file-line-count
);
