import styled from 'styled-components';
import { colors } from '@src/common/variables';

import { StyledInputBox } from './TextInput/styles';

export const StyledSearchSuggestion = styled.div`
  position: absolute;
  width: 100%;
  height: auto;
  margin-top: 3.25rem;
  border-radius: 0.5rem;
  box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.1);
  background-color: ${colors.white};
  opacity: 0;
  padding: 2rem 1.25rem 2rem;
`;

export const StyledSearchWrapper = styled.div`
  @keyframes slideInUp {
    from {
      opacity: 0;
      transform: translateY(15%);
    }

    to {
      opacity: 1;
      transform: translateY(0%);
    }
  }

  width: 10rem;
  height: 3rem;
  /* margin-right: 1rem; */
  transition: 0.2s linear 0s;
  border-radius: 1.5rem;
  position: relative;
  z-index: 1000;

  background: ${colors.highLightGray};

  &:hover {
    background: ${colors.coldGray};

    ${StyledInputBox} {
      input {
        cursor: pointer;
      }
      .searchIcon {
        color: ${colors.magenta};
      }
    }
  }

  &.isActive.focus {
    transition: 0.2s linear 0s;
  }

  &.isActive {
    transition: 0s linear 0s;
    background: ${colors.white};
    width: 27.5rem;

    ${StyledInputBox} {
      input {
        cursor: text;
        color: ${colors.ironGray};
        caret-color: ${colors.magenta};
      }
      .searchIcon,
      .iconRight {
        font-size: 1rem;
        height: 1rem;
      }
      .searchIcon {
        right: auto;
        left: 1.25rem;
        color: ${colors.mediumGray};
      }
      .iconRight {
        display: block;
        right: 1.25rem;
      }
    }

    ${StyledSearchSuggestion} {
      animation: slideInUp 0.2s linear 0.2s;
      animation-fill-mode: both;
    }
  }

  &.focus {
    ${StyledInputBox} {
      .searchIcon {
        color: ${colors.magenta};
      }
      input {
        color: initial;
      }
    }
  }
`;
