import { Provider } from 'react-redux';
import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import SpeechRecognition from '@src/common/components/SpeechRecognition';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@src/common/components/SpeechRecognition/mapProps';
import actions from '@search/store/actions';

describe('<SpeechRecognition />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = false;
  const mockStore = configureStore();

  test('should render properly', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SpeechRecognition />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    const initStateValue: RootState = appState();
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).fetchSuggestion('query');
    mapDispatchToProps(dispatch).fetchCategories({
      query: 'string',
      url: 'string',
      type: 'string',
      isNewHit: true
    });
    mapDispatchToProps(dispatch).clearSuggestionData();
    mapDispatchToProps(dispatch).setSearchModal(true);
    mapDispatchToProps(dispatch).setListingEnable(true);
    mapDispatchToProps(dispatch).setInputWithNoText(true);
    mapDispatchToProps(dispatch).setInputContent('query');
    mapDispatchToProps(dispatch).setTempText('query');
    mapDispatchToProps(dispatch).setSpeechRecognition({
      autoStart: true,
      continuous: true
    });
    mapDispatchToProps(dispatch).setSearchPriorityText({
      activeText: 'string',
      inActiveText: 'string'
    });
    mapDispatchToProps(dispatch).updateShowAppShell(true);
    expect(dispatch.mock.calls[0][0]).toEqual(actions.fetchSuggestion('query'));
    expect(dispatch.mock.calls[1][0]).toEqual(
      actions.fetchCategories({
        query: 'string',
        url: 'string',
        type: 'string',
        isNewHit: true
      })
    );
    expect(dispatch.mock.calls[2][0]).toEqual(actions.clearSuggestionData());
    expect(dispatch.mock.calls[3][0]).toEqual(actions.setSearchModal(true));
    expect(dispatch.mock.calls[4][0]).toEqual(actions.setListingEnable(true));
    expect(dispatch.mock.calls[5][0]).toEqual(actions.setInputWithNoText(true));
    expect(dispatch.mock.calls[6][0]).toEqual(actions.setInputContent('query'));
    expect(dispatch.mock.calls[7][0]).toEqual(actions.setTempText('query'));
    expect(dispatch.mock.calls[8][0]).toEqual(
      actions.setSpeechRecognition({
        autoStart: true,
        continuous: true
      })
    );
    expect(dispatch.mock.calls[9][0]).toEqual(
      actions.setSearchPriorityText({
        activeText: 'string',
        inActiveText: 'string'
      })
    );
    expect(dispatch.mock.calls[10][0]).toEqual(
      actions.updateShowAppShell(true)
    );
  });
});
