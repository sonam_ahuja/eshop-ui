import React, { FunctionComponent } from 'react';
import DesktopSpeechRecognitionInput from '@common/components/SpeechRecognition/desktop';
import { IProps } from '@common/components/SpeechRecognition/utils/SpeechRecognitionHOC';

const SpeechRecognition: FunctionComponent = () => {
  const props: IProps = {
    autoStart: false,
    continuous: false,
    // tslint:disable-next-line: no-empty
    onInputChange: () => {},
    // tslint:disable-next-line: no-empty
    onInputValueChange: value => {
      // tslint:disable-next-line: no-console
      console.log(value);
    }
  };

  return <DesktopSpeechRecognitionInput {...props} />;
};

export default SpeechRecognition;
