import actions from '@search/store/actions';
import { Dispatch } from 'redux';
import { RootState } from '@common/store/reducers';
import { IFetchCategoriesParams } from '@search/store/types';
import {
  IComponentDispatchToProps,
  IComponentStateToProps
} from '@common/components/SpeechRecognition/types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  searchTranslation: state.translation.cart.global.search,
  searchConfiguration: state.configuration.cms_configuration.global.search,
  search: state.search
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  fetchSuggestion(query: string): void {
    dispatch(actions.fetchSuggestion(query));
  },
  fetchCategories(params: IFetchCategoriesParams): void {
    dispatch(actions.fetchCategories(params));
  },

  clearSuggestionData(): void {
    dispatch(actions.clearSuggestionData());
  },
  setSearchModal(showModal: boolean): void {
    dispatch(actions.setSearchModal(showModal));
  },
  setListingEnable(showModal: boolean): void {
    dispatch(actions.setListingEnable(showModal));
  },
  setInputWithNoText(showModal: boolean): void {
    dispatch(actions.setInputWithNoText(showModal));
  },
  setInputContent(value: string): void {
    dispatch(actions.setInputContent(value));
  },
  setTempText(value: string): void {
    dispatch(actions.setTempText(value));
  },
  setSpeechRecognition(speechParams: {
    autoStart: boolean;
    continuous: boolean;
  }): void {
    dispatch(actions.setSpeechRecognition(speechParams));
  },
  setSearchPriorityText(search: {
    activeText: string;
    inActiveText: string;
  }): void {
    dispatch(actions.setSearchPriorityText(search));
  },
  updateShowAppShell(showAppShell: boolean): void {
    dispatch(actions.updateShowAppShell(showAppShell));
  }
});
