import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { StaticRouter } from 'react-router-dom';
import {
  IComponentProps,
  MobileSpeechInput
} from '@src/common/components/SpeechRecognition/mobile';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { KEY_CODE } from '@search/store/enum';

describe('<MobileSpeechInput />', () => {
  const props: IComponentProps = {
    ...histroyParams,
    transcript: 'string',
    onNoMatch: true,
    onMicNotAllowed: true,
    onError: true,
    onInputValueChange: jest.fn(),
    browserSupportsSpeechRecognition: true,
    recognition: {},
    onInputChange: jest.fn(),
    onRightArrowClick: jest.fn(),
    resetTranscript: jest.fn(),
    startListening: jest.fn(),
    abortListening: jest.fn(),
    stopListening: jest.fn(),
    getParentText: jest.fn(),
    listening: true,
    searchTranslation: appState().translation.cart.global.search,
    searchConfiguration: appState().configuration.cms_configuration.global
      .search,
    search: appState().search,
    setSpeechRecognition: jest.fn(),
    fetchSuggestion: jest.fn(),
    fetchCategories: jest.fn(),
    clearSuggestionData: jest.fn(),
    setSearchModal: jest.fn(),
    setListingEnable: jest.fn(),
    updateShowAppShell: jest.fn(),
    setInputWithNoText: jest.fn(),
    setTempText: jest.fn(),
    setInputContent: jest.fn(),
    setSearchPriorityText: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileSpeechInput {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const onChangeEvent = { target: { value: 'avinash' } };
    const onKeyArrowDownEvent = { keyCode: KEY_CODE.ARROW_DOWN };
    const onKeyArrowRightEvent = { keyCode: KEY_CODE.ARROW_RIGHT };
    const onKeyArrowUpEvent = { keyCode: KEY_CODE.ARROW_UP };
    const onKeyDownEvent = { keyCode: KEY_CODE.ENTER };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileSpeechInput {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).setInputValue('inputValue');
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onInputChange(
      onChangeEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onKeyDown(
      onKeyArrowDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onKeyDown(
      onKeyArrowRightEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onKeyDown(
      onKeyArrowUpEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onKeyDown(
      onKeyDownEvent as React.KeyboardEvent<Element>
    );
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onLastWordClick('string');
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).resetText();
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).onCancel();
    (component
      .find('MobileSpeechInput')
      .instance() as MobileSpeechInput).setInputValue('string');
  });
});
