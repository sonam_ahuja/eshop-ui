import { connect } from 'react-redux';
import { Icon } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { getSelectedFiltersFromParams } from '@search/utils';
import SpeechRecognitionHOC from '@common/components/SpeechRecognition/utils/SpeechRecognitionHOC';
import {
  IComponentDispatchToProps,
  IComponentStateToProps
} from '@common/components/SpeechRecognition/types';
import { isMobile } from '@common/utils';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@common/components/SpeechRecognition/mapProps';
import { KEY_CODE, SEARCH_ROUTE } from '@search/store/enum';
import {
  StyledCancelButton,
  StyledInput,
  StyledSearch,
  StyledSearchHeader
} from '@src/common/components/SpeechRecognition/mobile/styles';
import history from '@src/client/history';

export interface IProps {
  transcript: string;
  browserSupportsSpeechRecognition: boolean;
  recognition: object;
  // tslint:disable-next-line:member-ordering
  listening: boolean;
  onNoMatch: boolean;
  onMicNotAllowed: boolean;
  onError: boolean;
  onInputValueChange(value: string): void;
  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void;
  onRightArrowClick?(value: string): void;
  resetTranscript(): void;
  startListening(): void;
  abortListening(): void;
  stopListening(): void;
  getParentText(text: string): void;
}
export interface IState {
  inputValue: string;
  localStorageUpdated: boolean;
  el: HTMLElement | null;
  isSpeechRecognition: boolean;
  isInputFocus: boolean;
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps;

export class MobileSpeechInput extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      inputValue: this.props.transcript,
      localStorageUpdated: false,
      el: null,
      isSpeechRecognition: false,
      isInputFocus: false
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.updateUrlPath = this.updateUrlPath.bind(this);
    this.resetText = this.resetText.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onLastWordClick = this.onLastWordClick.bind(this);
    this.setRef = this.setRef.bind(this);
    this.renderPlaceHolderText = this.renderPlaceHolderText.bind(this);
    this.onSearchIconClick = this.onSearchIconClick.bind(this);
  }

  componentDidMount(): void {
    const { ...params } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    const query: string = params.query ? params.query : '';
    if (query.length && !isMobile.phone) {
      this.props.onInputValueChange(query);
      this.setState({ inputValue: query });
    }
  }

  componentDidUpdate(prevProps: IComponentProps): void {
    const { search: newSearch } = this.props;
    const { search: oldSearch } = prevProps;
    if (
      oldSearch.searchFilterSetting.inputContent !==
      newSearch.searchFilterSetting.inputContent
    ) {
      this.setState({ inputValue: newSearch.searchFilterSetting.inputContent });
    }
  }

  setInputValue(inputValue: string): void {
    if (inputValue.trim().length) {
      this.setState({ inputValue });
      this.props.onInputValueChange(inputValue);
    }
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputValue = (event.target as HTMLInputElement).value;
    if (inputValue.trim().length) {
      this.setInputValue(inputValue);
    } else {
      this.setState({ inputValue: '' });
    }
    this.props.stopListening();
    this.props.onInputChange(event);
  }

  setRef(el: HTMLElement | null): void {
    if (el) {
      el.focus();
      setTimeout(() => {
        el.blur();
        el.focus();
      }, 450); // 400 = flowpanel sideInLeft animation time
      this.setState({ el });
    }
    const { ...params } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    if (params.query && params.query.length) {
      this.setState({ inputValue: params.query });
      this.props.onInputValueChange(params.query);
    } else {
      this.setState({ inputValue: '' });
      this.props.onInputValueChange('');
    }
  }

  onKeyDown(event: React.KeyboardEvent<Element>): void {
    const { inputValue } = this.state;
    const { inActiveText, activeText } = this.props.search.search;
    const { onRightArrowClick } = this.props;
    if (event.keyCode === KEY_CODE.ENTER && inputValue.trim().length) {
      this.updateUrlPath(inputValue);
    }
    if (event.keyCode === KEY_CODE.ARROW_RIGHT && onRightArrowClick) {
      this.setState({ inputValue: `${activeText}${inActiveText}` });
      onRightArrowClick(`${activeText}${inActiveText}`);
    }
  }

  updateUrlPath(text: string): void {
    this.props.fetchCategories({
      query: text,
      url: this.props.location.pathname,
      type: SEARCH_ROUTE.SEARCH_RESULT,
      isNewHit: true
    });
  }

  onLastWordClick(text: string): void {
    this.updateUrlPath(text);
  }

  resetText(): void {
    const { el } = this.state;
    if (el) {
      el.focus();
    }
    this.props.setSearchPriorityText({
      activeText: '',
      inActiveText: ''
    });
    this.setState({ inputValue: '' });
    this.props.onInputValueChange('');
  }

  onCancel(): void {
    this.props.setSearchModal(false);
  }

  renderPlaceHolderText(
    activeText: string | null,
    inActiveText: string | null
  ): string {
    const { startTyping } = this.props.searchTranslation.searchField.mobile;

    return activeText &&
      inActiveText &&
      (activeText.length && inActiveText.length)
      ? ''
      : startTyping;
  }

  onSearchIconClick(): void {
    const { inputValue } = this.state;
    if (inputValue) {
      this.updateUrlPath(inputValue);
    } else {
      if (history) {
        this.props.setSearchModal(false);
        history.goBack();
      }
    }
  }

  // tslint:disable-next-line: cognitive-complexity
  render(): ReactNode {
    const { inputValue } = this.state;
    const { cancel } = this.props.searchTranslation.searchField.mobile;
    const { search, searchFilterSetting } = this.props.search;
    this.props.getParentText(inputValue);

    return (
      <StyledSearchHeader>
        <StyledSearch>
          <Icon
            className='searchIcon'
            size='inherit'
            name='ec-search'
            onClick={this.onSearchIconClick}
          />
          <StyledInput>
            <input
              type='text'
              ref={this.setRef}
              placeholder={this.renderPlaceHolderText(
                search.activeText,
                search.inActiveText
              )}
              className='input-field'
              value={inputValue}
              onChange={this.onInputChange}
              onFocus={(event: React.FocusEvent<HTMLInputElement>) => {
                if (event) {
                  event.stopPropagation();
                  this.setState({ isInputFocus: true });
                }
              }}
              onBlur={() => {
                this.setState({ isInputFocus: false });
              }}
              onKeyDown={this.onKeyDown}
            />
            {inputValue.length > 0 && searchFilterSetting.showListing && (
              <div className='backgroundText'>
                <span className='activeContent'>{search.activeText}</span>
                {search.inActiveText}
              </div>
            )}
          </StyledInput>
          <div
            className='iconRight'
            onClick={(event: React.MouseEvent<HTMLInputElement>) => {
              if (event && inputValue.length) {
                this.resetText();
              }
            }}
          >
            {inputValue.length > 0 && (
              <Icon
                size='inherit'
                name={'ec-error'}
                color={'currentColor'}
                className='icon-recognition'
              />
            )}
          </div>
        </StyledSearch>
        <StyledCancelButton onClick={this.onCancel}>
          {cancel}
        </StyledCancelButton>
      </StyledSearchHeader>
    );
  }
}

export default SpeechRecognitionHOC({
  autoStart: false,
  continuous: false
})(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(MobileSpeechInput)
  )
  // tslint:disable-next-line:max-file-line-count
);
