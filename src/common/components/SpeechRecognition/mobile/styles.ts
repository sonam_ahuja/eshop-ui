import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledInput = styled.div`
  position: relative;
  z-index: 1;
  width: 100%;

  input,
  .backgroundText {
    height: 1.5rem;
    font-size: 1rem;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: ${colors.mediumGray};
    width: 100%;
    background: none;
    box-shadow: none;
    border-radius: 0;
    border: none;
    outline: none;
    caret-color: ${colors.magenta};
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
  }
  input {
    position: relative;
    z-index: 1;
    color: ${colors.darkGray};
    padding: 0;
  }
  .backgroundText {
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    z-index: 0;
    span.activeContent {
      visibility: hidden;
    }
    span.clickContent {
      z-index: 2;
    }
  }
`;
export const StyledSearch = styled.div`
  position: relative;
  flex: 1;
  display: flex;

  .searchIcon {
    font-size: 1.25rem;
    margin-right: 0.75rem;
    margin-top: 0.15rem;
  }
  .iconRight {
    font-size: 1.25rem;
    margin-left: 0.75rem;
    margin-top: 0.15rem;
    color: ${colors.mediumGray};
    cursor: pointer;
  }
`;
export const StyledCancelButton = styled.div`
  margin-left: 2.5rem;
  font-size: 0.875rem;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.43;
  letter-spacing: normal;
  color: ${colors.magenta};
  cursor: pointer;
`;

export const StyledSearchHeader = styled.div`
  background-color: ${colors.coldGray};
  padding: 0 1.25rem 0 1rem;
  min-height: 5rem;
  display: flex;
  align-items: center;

  input::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: ${colors.mediumGray};
  }
  input::-moz-placeholder {
    /* Firefox 19+ */
    color: ${colors.mediumGray};
  }
  input:-ms-input-placeholder {
    /* IE 10+ */
    color: ${colors.mediumGray};
  }
  input:-moz-placeholder {
    /* Firefox 18- */
    color: ${colors.mediumGray};
  }
`;
