import { IFetchCategoriesParams, ISearchState } from '@search/store/types';
import { ISearchTranslation } from '@store/types/translation';
import { ISearchConfiguration } from '@store/types/configuration';

export interface IComponentStateToProps {
  searchTranslation: ISearchTranslation;
  searchConfiguration: ISearchConfiguration;
  search: ISearchState;
}

export interface IComponentDispatchToProps {
  setSpeechRecognition(speechParams: {
    autoStart: boolean;
    continuous: boolean;
  }): void;
  fetchSuggestion(query: string): void;
  fetchCategories(params: IFetchCategoriesParams): void;
  clearSuggestionData(): void;
  setSearchModal(searchModal: boolean): void;
  setListingEnable(enableListing: boolean): void;
  updateShowAppShell(showAppShell: boolean): void;
  setInputWithNoText(setNoInput: boolean): void;
  setTempText(value: string): void;
  setInputContent(value: string): void;
  setSearchPriorityText(search: {
    activeText: string;
    inActiveText: string;
  }): void;
}
