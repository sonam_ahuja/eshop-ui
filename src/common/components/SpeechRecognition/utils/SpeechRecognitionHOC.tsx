import React, { Component, ReactNode } from 'react';
import { isBrowser, logError } from '@src/common/utils';

export interface IOptions {
  autoStart: boolean;
  continuous: boolean;
}

export interface IProps {
  autoStart: boolean;
  continuous: boolean;
  onInputChange(event?: React.ChangeEvent<HTMLInputElement>): void;
  onInputValueChange(value: string): void;
  onRightArrowClick?(value: string): void;
}
export interface IState {
  interimTranscript: string;
  finalTranscript: string;
  listening: boolean;
  isNoMatch: boolean;
  isNoSpeech: boolean;
  isListeningStop: boolean;
  isStartSpeaking: boolean;
  isMicNotAllowed: boolean;
  isError: boolean;
  finalWords: string;
}

export enum TIMER_VALUES {
  onStartTimerRef = 500,
  listingStopTrueTimerRef = 200,
  listingStopFalseTimerRef = 200,
  isStartSpeakingTimerRef = 500,
  isNoMatchTimerRef = 10000,
  isNoSpeechTimerRef = 2000,
  isMicNotAllowedTimerRef = 5000,
  isErrorTimerRef = 2000
}

export interface ITimerRef {
  onStartTimerRef: number | null;
  listingStopTrueTimerRef: number | null;
  listingStopFalseTimerRef: number | null;
  isStartSpeakingTimerRef: number | null;
  isNoMatchTimerRef: number | null;
  isNoSpeechTimerRef: number | null;
  isMicNotAllowedTimerRef: number | null;
  isErrorTimerRef: number | null;
}

// tslint:disable-next-line:no-big-function
const SpeechRecognition = (options: IOptions) => {
  // tslint:disable:no-big-function no-any
  return (WrappedComponent: any) => {
    const speechProperties: string[] = ['webkitSpeechRecognition'];
    // tslint:disable-next-line:no-any
    let recognition: any;
    let listening = false;
    let pauseAfterDisconnect = false;
    // tslint:disable-next-line:no-any
    let browserSupportsSpeechRecognition: any;

    try {
      // tslint:disable-next-line:no-any
      let BrowserSpeechRecognition: any;
      if (isBrowser) {
        speechProperties.forEach((speechProperty: string) => {
          if (window && window[speechProperty]) {
            BrowserSpeechRecognition = window[speechProperty];

            return;
          }
        });
      }
      recognition = BrowserSpeechRecognition
        ? new BrowserSpeechRecognition()
        : null;
      browserSupportsSpeechRecognition = recognition !== null;
      if (
        !browserSupportsSpeechRecognition ||
        (options && options.autoStart === false)
      ) {
        listening = false;
      } else {
        recognition.start();
        listening = true;
      }
    } catch (error) {
      logError(error);
    }

    let interimTranscript = '';
    let finalTranscript = '';
    const finalWords = '';

    return class SpeechRecognitionContainer extends Component<IProps, IState> {
      map: ITimerRef = {
        onStartTimerRef: null,
        listingStopTrueTimerRef: null,
        listingStopFalseTimerRef: null,
        isStartSpeakingTimerRef: null,
        isNoMatchTimerRef: null,
        isNoSpeechTimerRef: null,
        isMicNotAllowedTimerRef: null,
        isErrorTimerRef: null
      };
      constructor(props: IProps) {
        super(props);
        this.state = {
          interimTranscript,
          finalTranscript,
          listening,
          isNoMatch: false,
          isStartSpeaking: false,
          isNoSpeech: false,
          isListeningStop: false,
          isMicNotAllowed: false,
          isError: false,
          finalWords
        };
        if (browserSupportsSpeechRecognition) {
          if (!recognition) {
            recognition = {};
          }
          recognition.continuous = options && options.continuous !== false;
          recognition.interimResults = true;
          recognition.lang = 'en-US';
          recognition.onstart = this.onStart.bind(this);
          recognition.onresult = this.updateTranscript.bind(this);
          recognition.onend = this.onRecognitionDisconnect.bind(this);
          recognition.onerror = this.onError.bind(this);
        }
        this.disconnect = this.disconnect.bind(this);
        this.onStart = this.onStart.bind(this);
        this.onRecognitionDisconnect = this.onRecognitionDisconnect.bind(this);
        this.updateTranscript = this.updateTranscript.bind(this);
        this.concatTranscripts = this.concatTranscripts.bind(this);
        this.startListening = this.startListening.bind(this);
        this.onError = this.onError.bind(this);
        this.stopListening = this.stopListening.bind(this);
        this.abortListening = this.abortListening.bind(this);
        this.resetTranscript = this.resetTranscript.bind(this);
        this.getParentText = this.getParentText.bind(this);
        this.clearAllTimer = this.clearAllTimer.bind(this);
        this.onDisconnect = this.onDisconnect.bind(this);
      }

      onStart(): void {
        this.setState({
          listening: false,
          isListeningStop: false,
          isMicNotAllowed: false,
          isStartSpeaking: true,
          isNoSpeech: false,
          isError: false,
          isNoMatch: false
        });

        const isStartSpeakingTimerRef = setTimeout(() => {
          this.setState({
            isStartSpeaking: false,
            listening: true
          });
        }, TIMER_VALUES.isStartSpeakingTimerRef);
        this.map.isStartSpeakingTimerRef = isStartSpeakingTimerRef;
      }

      disconnect(disconnectType: string): void {
        if (recognition) {
          switch (disconnectType) {
            case 'ABORT':
              pauseAfterDisconnect = true;
              recognition.abort();
              break;
            case 'RESET':
              pauseAfterDisconnect = false;
              recognition.abort();
              break;
            case 'STOP':
            default:
              pauseAfterDisconnect = true;
              recognition.stop();
          }
        }
      }

      clearAllTimer(): void {
        for (const timerRef in this.map) {
          if (this.map.hasOwnProperty(timerRef)) {
            clearTimeout(this.map[timerRef]);
          }
        }
      }

      onDisconnect(): void {
        listening = false;
        const transcript = this.concatTranscripts(
          finalTranscript,
          interimTranscript
        );
        const { isNoSpeech, isMicNotAllowed } = this.state;
        if (!transcript.length && !isNoSpeech && !isMicNotAllowed) {
          this.setState({ isNoMatch: true, isListeningStop: true, listening });
        } else {
          this.setState({
            listening,
            isListeningStop: true
          });
        }

        if (!transcript.length && !isNoSpeech && !isMicNotAllowed) {
          const isNoMatchTimerRef = setTimeout(() => {
            this.setState({
              isListeningStop: false,
              isNoMatch: false
            });
            this.disconnect('STOP');
          }, TIMER_VALUES.isNoMatchTimerRef);
          this.map.isNoMatchTimerRef = isNoMatchTimerRef;
        } else {
          const listingStopTrueTimerRef = setTimeout(() => {
            this.setState({
              isListeningStop: false
            });
          }, TIMER_VALUES.listingStopTrueTimerRef);
          this.map.listingStopTrueTimerRef = listingStopTrueTimerRef;
        }
      }

      onRecognitionDisconnect(): void {
        if (pauseAfterDisconnect) {
          this.onDisconnect();
        } else if (recognition) {
          if (recognition.continuous) {
            this.startListening();
          } else {
            this.onDisconnect();
          }
        }
        pauseAfterDisconnect = false;
      }

      // tslint:disable-next-line:no-any
      updateTranscript(event: any): void {
        interimTranscript = '';
        for (let i = event.resultIndex; i < event.results.length; ++i) {
          if (event.results[i].isFinal) {
            finalTranscript = this.concatTranscripts(
              finalTranscript,
              event.results[i][0].transcript
            );
          }
          this.setState({
            finalTranscript,
            interimTranscript
          });
        }
      }

      concatTranscripts(...transcriptParts: string[]): string {
        return transcriptParts
          .map(t => t.trim())
          .join(' ')
          .trim();
      }

      resetTranscript(): void {
        interimTranscript = '';
        finalTranscript = '';
        this.disconnect('RESET');
        this.setState({
          interimTranscript,
          finalTranscript
        });
      }

      startListening(): void {
        this.setState({
          listening: false,
          isListeningStop: false,
          isMicNotAllowed: false,
          isStartSpeaking: false,
          isNoSpeech: false,
          isError: false,
          isNoMatch: false
        });
        if (recognition && !listening) {
          if (!recognition.continuous) {
            this.resetTranscript();
          }
          try {
            recognition.start();
          } catch (DOMException) {
            // Tried to start recognition after it has already started - safe to swallow this error
          }
        }
      }

      onError(error: any): void {
        // no-speech please check your mic level
        if (error.error === 'not-allowed') {
          this.setState({
            isError: false,
            isNoMatch: false,
            isNoSpeech: false,
            isMicNotAllowed: true
          });
        } else if (error.error === 'no-speech') {
          this.setState({
            isError: false,
            isNoMatch: false,
            isNoSpeech: true,
            isMicNotAllowed: false
          });
          const isNoSpeechTimerRef = setTimeout(() => {
            this.setState({
              isNoSpeech: false
            });
          }, TIMER_VALUES.isNoSpeechTimerRef);
          this.map.isNoSpeechTimerRef = isNoSpeechTimerRef;
        } else {
          this.setState({
            isError: true,
            isNoMatch: false,
            isMicNotAllowed: false
          });
          const isErrorTimerRef = setTimeout(() => {
            this.setState({
              isError: false
            });
          }, TIMER_VALUES.isErrorTimerRef);
          this.map.isErrorTimerRef = isErrorTimerRef;
        }
      }

      abortListening(): void {
        listening = false;
        this.setState({ listening });
        this.disconnect('ABORT');
      }

      stopListening(): void {
        const { isNoMatch } = this.state;
        if (!isNoMatch) {
          this.setState(
            {
              listening: false,
              isNoMatch: false,
              isListeningStop: true,
              isMicNotAllowed: false,
              isError: false
            },
            () => {
              this.disconnect('STOP');
            }
          );
        }
      }

      getParentText(text: string): void {
        const { listening: listenerActive } = this.state;
        if (!listenerActive) {
          interimTranscript = '';
          finalTranscript = `${text}`;
          this.setState({
            interimTranscript,
            finalTranscript
          });
        } else {
          interimTranscript = '';
          finalTranscript = '';
        }
      }

      componentWillUnmount(): void {
        this.clearAllTimer();
      }

      render(): ReactNode {
        const transcript = this.concatTranscripts(
          finalTranscript,
          interimTranscript
        );

        return (
          <>
            {isBrowser && (
              <WrappedComponent
                resetTranscript={this.resetTranscript}
                startListening={this.startListening}
                abortListening={this.abortListening}
                stopListening={this.stopListening}
                transcript={transcript}
                recognition={recognition}
                getParentText={this.getParentText}
                clearAllTimerSpeechHoc={this.clearAllTimer}
                listening={this.state.listening}
                onMicNotAllowed={this.state.isMicNotAllowed}
                onError={this.state.isError}
                isStartSpeaking={this.state.isStartSpeaking}
                onNoMatch={this.state.isNoMatch}
                isNoSpeech={this.state.isNoSpeech}
                isListeningStop={this.state.isListeningStop}
                browserSupportsSpeechRecognition={
                  browserSupportsSpeechRecognition
                }
                {...this.state}
                {...this.props}
              />
            )}
          </>
        );
      }
    };
  };
};

// tslint:disable-next-line: max-file-line-count
export default SpeechRecognition;
