import {
  filteredSearchOption,
  getLocalStorageSuggestion,
  removeLocalStorageSuggestion,
  setLocalStorageSuggestion
} from '@src/common/components/SpeechRecognition/utils/localStorageOperation';

window.localStorage.setItem(
  'suggestion',
  `[{"suggestionText":"samsung","timestamp":1560789520604},
   {"suggestionText":"samsung1","timestamp":1560789520604}]`
);
describe('Local storge utils', () => {
  test('getLocalStorageSuggestion ::', () => {
    const result = getLocalStorageSuggestion();
    expect(getLocalStorageSuggestion()).toMatchObject(result);
  });

  test('setLocalStorageSuggestion ::', () => {
    expect(setLocalStorageSuggestion('demo')).toBeUndefined();
  });

  test('setLocalStorageSuggestion ::', () => {
    expect(setLocalStorageSuggestion('samsung')).toBeUndefined();
  });

  test('removeLocalStorageSuggestion ::', () => {
    expect(removeLocalStorageSuggestion('demo')).toBe(true);
  });

  test('filteredSearchOption ::', () => {
    expect(
      filteredSearchOption(true, 'searchText', 'suggestionText')
    ).toMatchObject({
      text: null,
      type: null
    });
  });

  test('filteredSearchOption with match words ::', () => {
    const newString = 'samsung';
    expect(filteredSearchOption(true, 'sam', newString)).toMatchObject({
      text: 'sung',
      type: 'modified'
    });
  });

  test('filteredSearchOption with match words, when input is not changing ::', () => {
    const newString = 'samsung';
    expect(filteredSearchOption(false, 'sam', newString)).toMatchObject({
      text: 'sung',
      type: 'modified'
    });
  });

  test('setLocalStorageSuggestion, when not suggestion in local storage ::', () => {
    expect(setLocalStorageSuggestion('samsung')).toBeUndefined();
  });
});
