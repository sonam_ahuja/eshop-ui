import {
  debounceFn,
  suggestionVisibilityTypeDebounceFn
} from '@src/common/components/SpeechRecognition/utils/speechDebounce';

describe('debounce utils', () => {
  test('checkSpeech ::', () => {
    const result = debounceFn(jest.fn());
    expect(result('demo', 'demo')).toBeUndefined();
  });

  test('suggestionVisibilityTypeDebounceFn ::', () => {
    const result = suggestionVisibilityTypeDebounceFn(jest.fn());
    expect(result('demo', true)).toBeUndefined();
  });
});
