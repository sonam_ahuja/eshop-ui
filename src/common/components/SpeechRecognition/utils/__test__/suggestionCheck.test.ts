import { suggestionCheck } from '@src/common/components/SpeechRecognition/utils/suggestionCheck';

window.localStorage.setItem(
  'suggestion',
  `[{"suggestionText":"samsung","timestamp":1560789520604},
   {"suggestionText":"samsung1","timestamp":1560789520604}]`
);
describe('utils', () => {
  test('suggestionCheck ::', () => {
    expect(
      suggestionCheck({ loading: false, error: null, data: ['string'] }, true)
    ).toBe(true);
  });
  // @
  // test('suggestionCheck, without data ::', () => {
  //   expect(
  //     suggestionCheck({ loading: false, error: null, data: [] }, true)
  //   ).toBe(true);
  // });

  test('suggestionCheck, without local suggestion data ::', () => {
    window.localStorage.removeItem('suggestion');
    expect(
      suggestionCheck({ loading: false, error: null, data: [] }, true)
    ).toBe(false);
  });
});
