import { isBrowser } from '@src/common/utils';

export const checkSpeech = (): boolean => {
  /* tslint:disable:no-string-literal */
  if (isBrowser && window && window['webkitSpeechRecognition']) {
    return true;
  }

  return false;
};

export const timerEvents = (
  isMicNotAllowed: boolean,
  isListeningStop: boolean,
  listening: boolean,
  isNoMatch: boolean,
  isError: boolean,
  isNoSpeech: boolean
): number | null => {
  if (
    isMicNotAllowed &&
    !isListeningStop &&
    !listening &&
    !isNoMatch &&
    !isError &&
    !isNoSpeech
  ) {
    return 5000;
  }

  if (
    !isMicNotAllowed &&
    isListeningStop &&
    !listening &&
    isNoMatch &&
    !isError &&
    !isNoSpeech
  ) {
    return 10000;
  }

  if (
    !isMicNotAllowed &&
    isListeningStop &&
    !listening &&
    !isNoMatch &&
    !isError &&
    isNoSpeech
  ) {
    return 2000;
  }

  return null;
};
