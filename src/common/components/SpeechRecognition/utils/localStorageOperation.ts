import {
  getValueFromLocalStorage,
  setValueInLocalStorage
} from '@utils/localStorage';
import { ISuggestionParams } from '@search/store/types';

export const setLocalStorageSuggestion = (text: string) => {
  if (getValueFromLocalStorage('suggestion') && text.length > 0) {
    const suggestion = JSON.parse(getValueFromLocalStorage(
      'suggestion'
    ) as string);
    const newSuggestion: ISuggestionParams[] = [...suggestion];
    let suggestionTextFound = false;
    newSuggestion.forEach((suggestionObj: ISuggestionParams) => {
      if (suggestionObj.suggestionText === text) {
        suggestionTextFound = true;

        return;
      }
    });
    if (suggestionTextFound) {
      return;
    } else {
      if (newSuggestion.length > 2) {
        newSuggestion.shift();
      }
      newSuggestion.unshift({
        suggestionText: text,
        timestamp: new Date().getTime()
      });
      setValueInLocalStorage('suggestion', JSON.stringify(newSuggestion));
    }
  } else {
    const data: ISuggestionParams[] = [
      { suggestionText: text, timestamp: new Date().getTime() }
    ];
    setValueInLocalStorage('suggestion', JSON.stringify(data));
  }
};

export const removeLocalStorageSuggestion = (suggestion: string): boolean => {
  if (getValueFromLocalStorage('suggestion')) {
    const parsaSuggestion = JSON.parse(getValueFromLocalStorage(
      'suggestion'
    ) as string);
    const newSuggestion: ISuggestionParams[] = [...parsaSuggestion];
    const updateSuggestion: ISuggestionParams[] = [];
    newSuggestion.forEach((suggestionObj: ISuggestionParams) => {
      if (suggestion !== suggestionObj.suggestionText) {
        updateSuggestion.push(suggestionObj);
      }
    });
    setValueInLocalStorage('suggestion', JSON.stringify(updateSuggestion));

    return true;
  } else {
    return false;
  }
};

export const getLocalStorageSuggestion = (): ISuggestionParams[] => {
  const suggestion = JSON.parse(getValueFromLocalStorage(
    'suggestion'
  ) as string);

  if (Array.isArray(suggestion)) {
    return [...suggestion];
  } else {
    return [];
  }
};

export const filteredSearchOption = (
  isInputChange: boolean,
  searchText: string,
  suggestionText: string
): { type: string | null; text: string | null } => {
  if (suggestionText.includes(searchText)) {
    return {
      type: 'modified',
      text: suggestionText.replace(searchText, '').toLowerCase()
    };
  } else {
    if (!isInputChange) {
      return {
        type: 'unModified',
        text: suggestionText.toLowerCase()
      };
    } else {
      return {
        type: null,
        text: null
      };
    }
  }
};
