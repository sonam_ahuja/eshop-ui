type proceedButtonType = (field: string, inputValue: string) => void;

export const debounceFn = (fn: proceedButtonType) => {
  const map = {};

  return (field: string, inputValue: string) => {
    if (map[field]) {
      clearTimeout(map[field]);
    }
    const timeout = setTimeout(fn, 150, field, inputValue);
    map[field] = timeout;
  };
};

type setSuggestionVisibilityType = (timerRef: string, visible: boolean) => void;

export const suggestionVisibilityTypeDebounceFn = (
  fn: setSuggestionVisibilityType
) => {
  const map = {};

  return (timerRef: string, visible: boolean) => {
    if (map[timerRef]) {
      clearTimeout(map[timerRef]);
    }
    const timeout = setTimeout(fn, 250, timerRef, visible);
    map[timerRef] = timeout;
  };
};
