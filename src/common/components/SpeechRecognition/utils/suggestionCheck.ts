import { ISuggestion } from '@search/store/types';
import { getValueFromLocalStorage } from '@utils/localStorage';
import { isMobile } from '@common/utils';

export const validCharacter = /^[A-Za-z0-9!$&'*_-]+$/;

export const suggestionCheck = (
  suggestion: ISuggestion,
  suggestionVisibility: boolean
): boolean => {
  if (
    ((suggestion && suggestion.data && suggestion.data.length) ||
      (getValueFromLocalStorage('suggestion') &&
        JSON.parse(getValueFromLocalStorage('suggestion') as string).length)) &&
    suggestionVisibility &&
    !isMobile.phone
  ) {
    return true;
  } else {
    return false;
  }
};
