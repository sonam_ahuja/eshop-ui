import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import appState from '@store/states/app';
import SummaryCompact, {
  IProps as ISummaryCompactProps
} from '@common/components/Summary/SummaryCompact';

describe('<SummaryCompact />', () => {
  test('should render properly', () => {
    const props: ISummaryCompactProps = {
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      translation: appState().translation.cart.basket,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      className: 'className'
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryCompact {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly when price and total Price and priceAlteration  not present', () => {
    const newSummary = { ...BASKET_ALL_ITEM.cartSummary };
    newSummary[0].totalPrice = 0;
    newSummary[1].totalPrice = 0;
    const props: ISummaryCompactProps = {
      cartSummary: newSummary,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      translation: appState().translation.cart.basket,
      className: undefined
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryCompact {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
