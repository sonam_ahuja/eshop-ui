import React from 'react';
import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { ICartSummary } from '@src/common/routes/basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import {
  StyledColumn,
  StyledRow
} from '@common/components/Summary/common/styles';
import { formatCurrency } from '@common/utils/currency';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

export const StyledSummaryCompact = styled.div`
  .lastRow {
    /* padding-bottom: 0; */
  }
  .label {
    color: ${colors.mediumGray};
    font-size: 0.75rem;
    line-height: 1.2;
    letter-spacing: 0.3px;
    /* text-transform: uppercase; */
  }

  .value {
    color: ${colors.cloudGray};
    font-size: 0.8rem;
    line-height: 1.33;
  }
`;

export interface IProps {
  className?: string;
  cartSummary: ICartSummary[];
  translation: IBasket;
  currency: ICurrencyConfiguration;
}

const SummaryCompact = (props: IProps) => {
  const { className } = props;
  const { cartSummary, translation, currency } = props;

  return (
    <StyledSummaryCompact className={className}>
      <div className='summaryCompactInner'>
        <StyledRow className='lastRow'>
          <StyledColumn className='label'>
            <p>{translation.totalMonthly}</p>
          </StyledColumn>
          <StyledColumn className='value'>
            <p>
              {formatCurrency(
                cartSummary[1].totalPrice ? cartSummary[1].totalPrice : 0,
                currency.locale
              )}{' '}
            </p>
          </StyledColumn>
        </StyledRow>
        <StyledRow>
          <StyledColumn className='label'>
            <p>{translation.totalUpFront}</p>
          </StyledColumn>
          <StyledColumn className='value'>
            <p>
              {formatCurrency(
                cartSummary[0].totalPrice ? cartSummary[0].totalPrice : 0,
                currency.locale
              )}{' '}
            </p>
          </StyledColumn>
        </StyledRow>
      </div>
    </StyledSummaryCompact>
  );
};

export default SummaryCompact;
