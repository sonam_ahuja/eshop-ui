import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@store/states/translation';

import SummaryMonthly, { IProps as ISummaryMonthlyProps } from '.';

describe('<SummaryMonthly />', () => {
  test('should render properly', () => {
    const props: ISummaryMonthlyProps = {
      cartSummary: BASKET_ALL_ITEM.cartSummary[0],
      translation: translationState(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      isSummaryOpen: false
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryMonthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly when price and total Price and priceAlteration  not present', () => {
    const summary = { ...BASKET_ALL_ITEM.cartSummary[1] };
    delete summary.price;
    delete summary.totalPrice;
    delete summary.priceAlterations;
    const props: ISummaryMonthlyProps = {
      cartSummary: summary,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      translation: translationState(),
      isSummaryOpen: false
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryMonthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
