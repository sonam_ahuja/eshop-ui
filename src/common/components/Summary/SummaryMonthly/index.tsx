import React, { FunctionComponent, ReactNode } from 'react';
import { Divider, InstallmentAmount, Section, Title } from 'dt-components';
import styled from 'styled-components';
import { ICartSummary, ICartSummaryPriceAlteration } from '@basket/store/types';
import { ITranslationState } from '@store/types/translation';
import MonthlyToolTip from '@basket/Tooltip/monthly/index';
import { ITEM_TYPE } from '@src/common/routes/basket/store/enums';
import DiscountToolTip from '@src/common/routes/basket/Tooltip/discountPriceDetails';
import {
  getCurrencySymbol,
  getFormatedPriceValue
} from '@common/utils/currency';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

import { StyledColumn, StyledRow } from '../common/styles';

export const StyledSummaryMonthly = styled.div`
  .heading {
    text-align: right;
  }

  .total {
    .label {
      display: inline-flex;
      align-items: flex-end;
      span {
        display: inline-flex;
        margin-left: 5px;
        font-size: 1.3em;
      }
    }
  }
`;

export interface IProps {
  isSummaryOpen: boolean;
  currency: ICurrencyConfiguration;
  cartSummary: ICartSummary;
  translation: ITranslationState;
}

const SummaryMonthly: FunctionComponent<IProps> = (props: IProps) => {
  const { basket: basketTranslation } = props.translation.cart;
  const SummaryMonthlyList = () => {
    const array: ReactNode[] = [];
    array.push(
      <StyledRow key={3}>
        <StyledColumn>
          <Section size='large'>{basketTranslation.subTotalMonthly}</Section>
        </StyledColumn>
        <StyledColumn>
          <Section size='large'>
            <InstallmentAmount
              amount={getFormatedPriceValue(
                props.cartSummary.price ? props.cartSummary.price : 0,
                props.currency.locale
              )}
              size='small'
              currencyName={getCurrencySymbol(props.currency.locale)}
              weight='normal'
            />
          </Section>
        </StyledColumn>
      </StyledRow>
    );
    if (
      props.cartSummary &&
      props.cartSummary.priceAlterations &&
      props.cartSummary.priceAlterations.length > 0
    ) {
      props.cartSummary.priceAlterations.forEach(
        (alterationType: ICartSummaryPriceAlteration, index: number) => {
          array.push(
            <StyledRow key={index}>
              <StyledColumn style={{ display: 'flex' }}>
                <Section size='large'>
                  {basketTranslation[alterationType.priceType]}
                </Section>
                {alterationType.priceType === ITEM_TYPE.DISCOUNT &&
                  alterationType.subPriceAlterations &&
                  Array.isArray(alterationType.subPriceAlterations) &&
                  alterationType.subPriceAlterations.length ? (
                    <DiscountToolTip
                      translation={basketTranslation}
                      currency={props.currency.locale}
                      discount={alterationType}
                    />
                  ) : null}
              </StyledColumn>
              <StyledColumn>
                <Section size='large'>
                  <InstallmentAmount
                    amount={getFormatedPriceValue(
                      alterationType.price,
                      props.currency.locale
                    )}
                    size='small'
                    currencyName={getCurrencySymbol(props.currency.locale)}
                    weight='normal'
                  />
                </Section>
              </StyledColumn>
            </StyledRow>
          );
        }
      );
    }

    return array;
  };

  const Installment = () => {
    const array: ReactNode[] = [];
    array.push(
      <InstallmentAmount
        key={1}
        amount={getFormatedPriceValue(
          props.cartSummary.totalPrice ? props.cartSummary.totalPrice : 0,
          props.currency.locale
        )}
        size='small'
        currencyName={getCurrencySymbol(props.currency.locale)}
        weight='normal'
      />
    );

    return array;
  };

  return (
    <StyledSummaryMonthly className='summaryMonthly'>
      <Section
        className='heading'
        size='small'
        weight='normal'
        transform='uppercase'
      >
        {basketTranslation.monthly}
      </Section>

      <Divider dashed={true} />

      {SummaryMonthlyList()}
      <StyledRow className='total'>
        <StyledColumn>
          <Section
            className='label'
            size='small'
            weight='normal'
            transform='uppercase'
          >
            {basketTranslation.totalMonthly}
            {props.cartSummary.cartRecurringBreakUp && props.isSummaryOpen && (
              <MonthlyToolTip
                translation={basketTranslation}
                title={basketTranslation.totalMonthly}
                currency={props.currency.locale}
                cartRecurringBreakUp={props.cartSummary.cartRecurringBreakUp}
              />
            )}
          </Section>
        </StyledColumn>
        <StyledColumn>
          <Title size='xsmall' weight='normal'>
            {Installment()}
          </Title>
        </StyledColumn>
      </StyledRow>
    </StyledSummaryMonthly>
  );
};

export default SummaryMonthly;
