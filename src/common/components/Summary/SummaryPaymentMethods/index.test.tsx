import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

import SummaryPaymentMethods from '.';

describe('<SummaryPaymentMethods />', () => {
  test('should render properly', () => {
    const obj = {
      show: false,
      link: '',
      labelKey: ''
    };

    const props = {
      availablePaymentMethods: {
        upfront: {
          tokenizedCard: obj,
          payOnDelivery: obj,
          payByLink: obj,
          bankAccount: obj,
          manualPayments: obj,
          defaultPaymentMethod: PAYMENT_TYPE.PAY_BY_LINK
        },
        monthly: {
          tokenizedCard: obj,
          payOnDelivery: obj,
          payByLink: obj,
          bankAccount: obj,
          manualPayments: obj,
          defaultPaymentMethod: PAYMENT_TYPE.PAY_BY_LINK
        },
        availablePaymentMode: {
          maestro: {
            show: true
          },
          applePay: {
            show: true
          },
          googlePay: {
            show: true
          },
          visa: {
            show: true
          },
          payPal: {
            show: true
          },
          worldPay: {
            show: true
          }
        }
      }
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryPaymentMethods {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
