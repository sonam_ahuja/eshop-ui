import React, { FunctionComponent } from 'react';
import { PreloadImage } from 'dt-components';
import { IPaymentMethods } from '@src/common/store/types/configuration';

import { StyledPaymentMethods } from './styles';

export interface IProps {
  availablePaymentMethods: IPaymentMethods;
}

const SummaryPaymentMethods: FunctionComponent<IProps> = (props: IProps) => {
  const { availablePaymentMode } = props.availablePaymentMethods;

  return (
    <StyledPaymentMethods className='styledPaymentMethods'>
      {availablePaymentMode.maestro.show && (
        <div className='productImage'>
          <PreloadImage
            imageUrl='/maestro.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
      )}
      {availablePaymentMode.applePay.show && (
        <div className='productImage'>
          <PreloadImage
            imageUrl='/apple-pay.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
      )}
      {availablePaymentMode.visa.show && (
        <div className='productImage'>
          <PreloadImage imageUrl='/visa.png' imageHeight={10} imageWidth={10} />
        </div>
      )}
      {availablePaymentMode.googlePay.show && (
        <div className='productImage'>
          <PreloadImage
            imageUrl='/google-pay.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
      )}
    </StyledPaymentMethods>
  );
};

export default SummaryPaymentMethods;
