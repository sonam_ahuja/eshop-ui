import styled from 'styled-components';

export const StyledPaymentMethods = styled.div`
  opacity: 0.4;

  display: flex;
  width: 100%;
  justify-content: space-between;

  .productImage {
    width: 30px;
    height: 30px;
  }
`;
