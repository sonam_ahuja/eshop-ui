import React, { FunctionComponent, ReactNode } from 'react';
import { Divider, InstallmentAmount, Section, Title } from 'dt-components';
import styled from 'styled-components';
import { ICartSummary, ICartSummaryPriceAlteration } from '@basket/store/types';
import { ITranslationState } from '@store/types/translation';
import { ITEM_TYPE } from '@src/common/routes/basket/store/enums';
import DiscountToolTip from '@src/common/routes/basket/Tooltip/discountPriceDetails';
import {
  getCurrencySymbol,
  getFormatedPriceValue
} from '@common/utils/currency';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

import { StyledColumn, StyledRow } from '../common/styles';

export const StyledSummaryUpFront = styled.div`
  .heading {
    text-align: right;
  }
`;

export interface IProps {
  cartSummary: ICartSummary;
  currency: ICurrencyConfiguration;
  translation: ITranslationState;
}

const SummaryUpfront: FunctionComponent<IProps> = (props: IProps) => {
  const { basket: basketTranslation } = props.translation.cart;
  const SummaryUpfrontList = () => {
    const array: ReactNode[] = [];
    array.push(
      <StyledRow key={3}>
        <StyledColumn>
          <Section size='large'>{basketTranslation.subTotalUpfront}</Section>
        </StyledColumn>
        <StyledColumn>
          <Section size='large'>
            <InstallmentAmount
              amount={getFormatedPriceValue(
                props.cartSummary.price ? props.cartSummary.price : 0,
                props.currency.locale
              )}
              size='small'
              currencyName={getCurrencySymbol(props.currency.locale)}
              weight='normal'
            />
          </Section>
        </StyledColumn>
      </StyledRow>
    );
    if (
      props.cartSummary &&
      props.cartSummary.priceAlterations &&
      props.cartSummary.priceAlterations.length > 0
    ) {
      props.cartSummary.priceAlterations.forEach(
        (alterationType: ICartSummaryPriceAlteration, index: number) => {
          array.push(
            <StyledRow key={index}>
              <StyledColumn
                key={`priceType-${index}`}
                style={{ display: 'flex' }}
              >
                <Section size='large'>
                  {basketTranslation[alterationType.priceType]}
                </Section>
                {alterationType.priceType === ITEM_TYPE.DISCOUNT &&
                alterationType.subPriceAlterations &&
                Array.isArray(alterationType.subPriceAlterations) &&
                alterationType.subPriceAlterations.length ? (
                  <DiscountToolTip
                    translation={basketTranslation}
                    currency={props.currency.locale}
                    discount={alterationType}
                  />
                ) : null}
              </StyledColumn>
              <StyledColumn key={`columnPrice-${index}`}>
                <Section size='large'>
                  <InstallmentAmount
                    amount={getFormatedPriceValue(
                      alterationType.price,
                      props.currency.locale
                    )}
                    size='small'
                    currencyName={getCurrencySymbol(props.currency.locale)}
                    weight='normal'
                  />
                </Section>
              </StyledColumn>
            </StyledRow>
          );
        }
      );
    }

    return array;
  };

  const Installment = () => {
    const array: ReactNode[] = [];
    array.push(
      <InstallmentAmount
        key={2}
        amount={
          props.cartSummary.totalPrice
            ? getFormatedPriceValue(
                props.cartSummary.totalPrice,
                props.currency.locale
              )
            : 0
        }
        size='small'
        currencyName={getCurrencySymbol(props.currency.locale)}
        weight='normal'
      />
    );

    return array;
  };

  return (
    <StyledSummaryUpFront className='summaryUpfront'>
      <Section
        className='heading'
        size='small'
        weight='normal'
        transform='uppercase'
      >
        {basketTranslation.summaryUpfrontHeading}
      </Section>
      <Divider dashed={true} />
      {SummaryUpfrontList()}
      <StyledRow className='total'>
        <StyledColumn>
          <Section size='small' weight='normal' transform='uppercase'>
            {basketTranslation.totalUpFront}
          </Section>
        </StyledColumn>
        <StyledColumn>
          <Title size='small' weight='bold'>
            {Installment()}
          </Title>
        </StyledColumn>
      </StyledRow>
    </StyledSummaryUpFront>
  );
};

export default SummaryUpfront;
