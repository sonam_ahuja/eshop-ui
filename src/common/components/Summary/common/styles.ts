import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledColumn = styled.div`
  flex-basis: 50%;
`;

export const StyledRow = styled.div`
  display: table;
  width: 100%;
  padding-bottom: 0.5rem;

  ${StyledColumn} {
    text-align: left;
    display: table-cell;
    /* width: 50%; */
  }

  ${StyledColumn}:last-child {
    text-align: right;
    padding-left: 0.5rem;
    white-space: nowrap;
  }

  &.total {
    padding-bottom: 0;
    padding-top: 1rem;
    color: ${colors.cloudGray};
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-bottom: 0.3rem;
    &.total {
      padding-top: 0.99rem;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    &.total {
      padding-top: 0.88rem;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    &.total {
      padding-top: 0.66rem;
    }
  }
`;
