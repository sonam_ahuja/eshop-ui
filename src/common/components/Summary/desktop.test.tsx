import React from 'react';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@common/store/states/translation';
import configurationState from '@common/store/states/configuration';
import { StaticRouter } from 'react-router';
import { histroyParams } from '@mocks/common/histroy';
import {
  IProps as IBasketSummaryProps,
  Summary
} from '@src/common/components/Summary/desktop';

describe('<BasketSummary Desktop/>', () => {
  const props: IBasketSummaryProps = {
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    goToLogin: jest.fn(),
    ...histroyParams,
    isTermsConditionChecked: true,
    cartSummary: BASKET_ALL_ITEM.cartSummary,
    translation: translationState(),
    isCheckoutEnable: true,
    configuration: configurationState(),
    isUserLoggedIn: true,
    stickySummary: true,
    proceedToCheckoutLoading: false
  };
  const componentWrapper = (newProps: IBasketSummaryProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Summary {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  test('BasketSummary should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('BasketSummary should render properly without summary price', () => {
    const newSummary = { ...BASKET_ALL_ITEM.cartSummary };
    newSummary[0].totalPrice = 0;
    newSummary[1].totalPrice = 0;
    const newProps = { ...props };
    newProps.cartSummary = newSummary;
    newProps.hideCheckoutButton = true;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isTermsConditionChecked is false', () => {
    const newProps = { ...props, isTermsConditionChecked: false };
    const component = shallow(<Summary {...newProps} />);
    component.setState({ check: true });
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isCheckoutEnable is false', () => {
    const newProps = { ...props, isCheckoutEnable: false };
    const component = shallow(<Summary {...newProps} />);
    component.setState({ check: true });
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isUserLoggedIn is false', () => {
    const newProps = { ...props, isUserLoggedIn: false };
    const component = shallow(<Summary {...newProps} />);
    component.setState({ check: true });
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const component = shallow<Summary>(<Summary {...props} />);
    component.instance().scrollDetect();
    component.instance().proceedToCheckout({
      preventDefault: jest.fn(),
      stopPropagation: jest.fn(),
      currentTarget: { value: 'avinash' }
      // tslint:disable-next-line: no-any
    } as any);
    component.instance().onSummaryClick();
    component.instance().setRef(null);
    component.instance().isElementVisible(3);
  });
});
