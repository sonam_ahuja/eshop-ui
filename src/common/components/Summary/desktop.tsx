import { Button, Icon, Overlay, Title } from 'dt-components';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ICartSummary } from '@basket/store/types';
import { ITranslationState } from '@store/types/translation';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@store/types/configuration';
import {
  StyledButtons,
  StyledFakeButton,
  StyledRefDiv,
  StyledSummary
} from '@src/common/components/Summary/styles';
import { isMobile } from '@src/common/utils';
import React, { Component, ReactNode, SyntheticEvent } from 'react';
import cx from 'classnames';
import { StyledFooterCopyrightMobile } from '@src/common/components/Footer/mobile/styles';
import EVENT_NAME, { PAGE_VIEW } from '@events/constants/eventName';

import CopyRightsFooter from '../Footer/common/copyRightsFooter';

import SummaryUpfront from './SummaryUpfront';
import SummaryMonthly from './SummaryMonthly';
import SummaryPaymentMethods from './SummaryPaymentMethods';
import SummaryCompact from './SummaryCompact';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  translation: ITranslationState;
  cartSummary: ICartSummary[];
  isCheckoutEnable: boolean;
  hideCheckoutButton?: boolean;
  isCheckoutSummary?: boolean;
  currency: ICurrencyConfiguration;
  configuration: IConfigurationState;
  isUserLoggedIn: boolean;
  isSticky?: boolean;
  isTermsConditionChecked: boolean;
  stickySummary?: boolean;
  isIdentityVerificationRequired?: boolean;
  proceedToCheckoutLoading: boolean;
  placeOrderButton?(): void;
  goToLogin(isRoutableFromLogin: boolean): void;
  possibleNextRouteWhenSummaryButtonClick?(): void;
}

export interface IState {
  isOpen: boolean;
  isStaticSummaryVisible: boolean;
  isStaticSummarySlightlyScrolled: boolean;
  removeSticky: boolean;
}

export class Summary extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isOpen: false, // rounded summary is open or not only when clicked!
      isStaticSummaryVisible: false, // check static summary  in viewport or not
      removeSticky: false, // sticky summary should be visible to user or not
      isStaticSummarySlightlyScrolled: false
    };
    this.proceedToCheckout = this.proceedToCheckout.bind(this);
    this.setRef = this.setRef.bind(this);
    this.isElementVisible = this.isElementVisible.bind(this);
    this.scrollDetect = this.scrollDetect.bind(this);
    this.onSummaryClick = this.onSummaryClick.bind(this);
    this.checkStickyCondition = this.checkStickyCondition.bind(this);
  }

  private el: HTMLElement | null = null;
  private lastScroll = 0;
  private timer = 0;

  scrollDetect(): void {
    const isStaticSummaryVisible = this.isElementVisible(0);
    const isStaticSummarySlightlyScrolled = this.isElementVisible(48);

    if (this.state.isStaticSummarySlightlyScrolled !== isStaticSummarySlightlyScrolled) {
      this.setState({
        isStaticSummarySlightlyScrolled
      });
    }

    if (this.state.isStaticSummaryVisible !== isStaticSummaryVisible) {
      this.setState({
        isStaticSummaryVisible,
      });
    }
  }

  checkStickyCondition(): void {
    if (!this.state.isOpen) {
      const currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (
        (currentScroll > 0 && this.lastScroll <= currentScroll) ||
        currentScroll === 0
      ) {
        this.setState({ removeSticky: false }); // removing summary if scrolling down
      } else {
        this.setState({ removeSticky: true }); // showing summary if scrolling down
      }
      this.lastScroll = currentScroll;
    }
  }

  proceedToCheckout(event: SyntheticEvent): void {
    event.preventDefault();
    event.stopPropagation();
    const { placeOrderButton } = this.props;
    const { isCheckoutSummary } = this.props;
    if (
      !isCheckoutSummary &&
      this.props.possibleNextRouteWhenSummaryButtonClick
    ) {
      this.props.possibleNextRouteWhenSummaryButtonClick();
    } else {
      if (placeOrderButton) {
        placeOrderButton();
      }
    }
  }

  componentDidMount(): void {
    window.addEventListener(
      'scroll',
      () => {
        // if sticky element is visible do not use debounce
        // if (this.isElementVisible(0)) {
        // this.checkStickyCondition();
        this.scrollDetect();
        // } else {
        // else use debouce function :-)
        if (this.timer !== null) {
          clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
          // tslint:disable-next-line:no-commented-code
          // this.scrollDetect();
          this.checkStickyCondition();
        }, 500);
        // }
      }
      // false
    );
  }

  onSummaryClick(): void {
    const { isStaticSummaryVisible } = this.state;
    const { removeSticky } = this.state;
    if ((isMobile.phone && !isStaticSummaryVisible) || !removeSticky) {
      this.setState({ isOpen: !this.state.isOpen });
    }
  }

  setRef(ref: HTMLElement | null): void {
    this.el = ref;
    const isElementVisible = this.isElementVisible(0);

    if (this.state.isStaticSummaryVisible !== isElementVisible) {
      this.setState({
        isStaticSummaryVisible: isElementVisible
      });
    }
  }

  isElementVisible(offsetVal: number): boolean {
    if (this.el) {
      const rect = this.el.getBoundingClientRect();

      if (rect.top < 0) {
        return true;
      }

      return (
        rect.top >= 0 &&
        rect.bottom <=
        (window.innerHeight - offsetVal ||
          document.documentElement.clientHeight - offsetVal)
      );
    }

    return false;
  }

  // tslint:disable:cognitive-complexity
  // tslint:disable-next-line:no-big-function
  render(): ReactNode {
    const {
      cartSummary,
      translation,
      isCheckoutEnable,
      isTermsConditionChecked,
      currency
    } = this.props;
    const { hideCheckoutButton, isCheckoutSummary } = this.props;
    const { isSticky, configuration, proceedToCheckoutLoading } = this.props;
    const { basket: basketTranslation } = this.props.translation.cart;
    const { isStaticSummaryVisible } = this.state;

    const classes = cx({
      removeSticky: this.state.removeSticky && !this.state.isOpen,
      hideStickySummary: isStaticSummaryVisible,
      isOpen: this.state.isOpen,
      isSticky: isSticky && isMobile.phone
    });

    const fakeBtnClasses = cx('summaryFakeButton', {
      removeSticky: this.state.removeSticky,
      hideStickySummary: isStaticSummaryVisible
    });

    const summaryContent = (
      <>
        {isSticky && isMobile.phone ? (
          <>
            <div className='summaryTop'>
              <Icon
                onClick={this.onSummaryClick}
                name='dt-summary-collapse'
                size='medium'
                color='currentColor'
                className='collapse-icon'
              />
              <Icon
                onClick={this.onSummaryClick}
                name='dt-summary-expand'
                size='medium'
                color='currentColor'
                className='expand-icon'
              />
            </div>
            <SummaryCompact
              className='summaryCompact'
              translation={basketTranslation}
              cartSummary={cartSummary}
              currency={currency}
            />
          </>
        ) : null}

        <div className='summaryBody'>
          <div className='summaryBodyInner'>
            <Title
              className='title'
              size='small'
              weight='ultra'
              firstLetterTransform='uppercase'
            >
              {basketTranslation.summaryHeading}
            </Title>
            <SummaryMonthly
              translation={translation}
              cartSummary={cartSummary[1]}
              currency={currency}
              key={1}
              isSummaryOpen={isMobile.phone ? this.state.isOpen : true}
            />
            <SummaryUpfront
              translation={translation}
              currency={currency}
              cartSummary={cartSummary[0]}
              key={0}
            />
          </div>
          <SummaryPaymentMethods
            availablePaymentMethods={
              configuration.cms_configuration.global.paymentMethods
            }
          />
        </div>

        <StyledButtons className={cx({ isStaticSummarySlightlyScrolled: this.state.isStaticSummarySlightlyScrolled })}>
          {!!hideCheckoutButton ? null : isCheckoutSummary ? (
            <Button
              disabled={!isTermsConditionChecked && !isCheckoutEnable}
              loading={false}
              data-event-id={EVENT_NAME.BASKET.EVENTS.PLACE_ORDER}
              data-event-message={
                translation.cart.checkout.orderReview.placeOrder
              }
              onClickHandler={this.proceedToCheckout}
            >
              {translation.cart.checkout.orderReview.placeOrder}
            </Button>
          ) : (
              <Button
                disabled={isCheckoutEnable}
                loading={proceedToCheckoutLoading}
                data-event-id={EVENT_NAME.BASKET.EVENTS.PROCEED_TO_CHECKOUT}
                data-event-message={basketTranslation.proceedToCheckout}
                onClickHandler={this.proceedToCheckout}
              >
                {basketTranslation.proceedToCheckout}
              </Button>
            )}
        </StyledButtons>
        {isMobile.phone ? (
          <StyledFooterCopyrightMobile>
            <CopyRightsFooter
              pageName={PAGE_VIEW.BASKET}
              termsAndConditionsUrl={
                this.props.configuration.cms_configuration.global
                  .termsAndConditionsUrl
              }
              globalTranslation={this.props.translation.cart.global}
            />
          </StyledFooterCopyrightMobile>
        ) : null}
      </>
    );

    return (
      <>
        {this.state.isOpen ? (
          <Overlay
            onClick={this.onSummaryClick}
            overlayStyle={{ background: 'rgba( 0, 0 ,0 ,0.6)', zIndex: 997 }}
            open={true}
            backgroundScroll={false}
            onClose={this.onSummaryClick}
          // onBackdropClick={this.onBackdropClick}
          >
            {null}
          </Overlay>
        ) : null}
        {/* this is round summary small one */}
        <StyledSummary
          className={classes}
          isScrollOffSet={!hideCheckoutButton ? 133 : 85}
        >
          {summaryContent}
        </StyledSummary>

        {/* this is responsive rectangular summary */}
        <StyledSummary
          className={'alwaysStatic'}
          isScrollOffSet={!hideCheckoutButton ? 133 : 85}
        >
          <StyledRefDiv ref={this.setRef} />
          {summaryContent}
        </StyledSummary>

        <StyledFakeButton className={fakeBtnClasses}>
          {!!hideCheckoutButton ? null : isCheckoutSummary ? (
            <Button
              loading={proceedToCheckoutLoading}
              disabled={!isTermsConditionChecked}
              data-event-id={EVENT_NAME.BASKET.EVENTS.PLACE_ORDER}
              data-event-message={
                translation.cart.checkout.orderReview.placeOrder
              }
              onClickHandler={this.proceedToCheckout}
            >
              {translation.cart.checkout.orderReview.placeOrder}
            </Button>
          ) : (
              <Button
                disabled={isCheckoutEnable}
                loading={proceedToCheckoutLoading}
                data-event-id={EVENT_NAME.BASKET.EVENTS.PROCEED_TO_CHECKOUT}
                data-event-message={basketTranslation.proceedToCheckout}
                onClickHandler={this.proceedToCheckout}
              >
                {basketTranslation.proceedToCheckout}
              </Button>
            )}
        </StyledFakeButton>
      </>
    );
  }
}

/*tslint:disable:max-file-line-count */
export default withRouter(Summary);
