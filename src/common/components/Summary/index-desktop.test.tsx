import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@common/store/states/translation';
import configurationState from '@common/store/states/configuration';
import { StaticRouter } from 'react-router';

import BasketSummary, { IProps as IBasketSummaryProps } from '.';
import SummaryShell from './index.shell';

jest.mock('@src/common/utils', () => ({
  isMobile: {
    phone: false
  }
}));

describe('<BasketSummary />', () => {
  test('BasketSummary should render properly', () => {
    const props: IBasketSummaryProps = {
      goToLogin: jest.fn(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      isIdentityVerificationRequired: false,
      possibleNextRouteWhenSummaryButtonClick: jest.fn(),
      translation: translationState(),
      isCheckoutEnable: true,
      configuration: configurationState(),
      isUserLoggedIn: true,
      isTermsConditionChecked: true,
      proceedToCheckoutLoading: false
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('SummaryShell should render properly', () => {
    const props = {
      isButtonVisible: false
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SummaryShell {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-identical-functions
  test('BasketSummary should render properly in desktop mode', () => {
    const props: IBasketSummaryProps = {
      goToLogin: jest.fn(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      translation: translationState(),
      isCheckoutEnable: true,
      configuration: configurationState(),
      isUserLoggedIn: true,
      isTermsConditionChecked: true,
      isIdentityVerificationRequired: false,
      proceedToCheckoutLoading: false,
      possibleNextRouteWhenSummaryButtonClick: jest.fn()
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();
  });
});

describe('<BasketSummary mobile/>', () => {
  test('BasketSummary should render properly in mobile mode', () => {
    const props: IBasketSummaryProps = {
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      translation: translationState(),
      isCheckoutEnable: true,
      configuration: configurationState(),
      goToLogin: jest.fn(),
      isUserLoggedIn: true,
      isTermsConditionChecked: true,
      isIdentityVerificationRequired: false,
      proceedToCheckoutLoading: false,
      possibleNextRouteWhenSummaryButtonClick: jest.fn()
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
