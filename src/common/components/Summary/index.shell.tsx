import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { Button, Divider, Section, Title } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

const StyledSummary = styled.div`
  width: 100%;
  position: sticky;
  top: 0;
  z-index: 101;
  background: ${colors.charcoalGray};
  color: ${colors.mediumGray};
  text-align: right;
  height: 100vh;
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  .summaryContent {
    padding: 2.25rem;

    .divider {
      min-height: 3px;
      height: 3px;
      margin: 0.75rem 0;
    }

    .mainHeading {
      padding-bottom: 2.5rem;
    }

    .upfront {
      margin-bottom: 2rem;

      .shine:first-child {
        height: 3.5rem;
        margin-bottom: 1rem;
      }
      .shine:last-child {
        height: 1.5rem;
      }
    }

    .monthly {
      .shine:first-child {
        height: 2.25rem;
        margin-bottom: 1rem;
      }
      .shine:last-child {
        height: 1.5rem;
      }
    }
  }

  .summaryFooter {
    justify-self: flex-end;
    margin-top: auto;

    .styledPaymentMethods {
      padding: 0 2.25rem;
    }

    .dt_button {
      width: 100%;
      margin-top: 1.5rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    width: 17.5rem;
    height: 100vh;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: 18rem;
    .summaryContent {
      padding: 2.1rem 2.25rem;
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 22.5rem;
  }
`;

export interface IProps {
  isButtonVisible: boolean;
}

const SummaryShell = (props: IProps) => {
  const upfront = (
    <div className='upfront'>
      <Section size='small' weight='normal' transform='uppercase'>
        Upfront
      </Section>
      <Divider className='divider' dashed={true} />

      <StyledShell className='secondary'>
        <div className='shine' />
        <div className='shine' />
      </StyledShell>
    </div>
  );

  const monthly = (
    <div className='monthly'>
      <Section size='small' weight='normal' transform='uppercase'>
        Monthly
      </Section>
      <Divider className='divider' dashed={true} />

      <StyledShell className='secondary'>
        <div className='shine' />
        <div className='shine' />
      </StyledShell>
    </div>
  );

  return (
    <StyledSummary>
      <div className='summaryContent'>
        <Title className='mainHeading' size='small' weight='ultra'>
          Summary
        </Title>

        {upfront}
        {monthly}
      </div>
      {props.isButtonVisible && (
        <div className='summaryFooter'>
          <Button size='medium'>Proceed to Checkout</Button>
        </div>
      )}
    </StyledSummary>
  );
};
export default SummaryShell;
