import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

import { StyledFooterCopyrightMobile } from '../Footer/mobile/styles';

export const StyledButtons = styled.div`
  display: none;
  opacity: 0;
  pointer-events: none;
  visibility: hidden;
  justify-content: flex-end;
  margin-top: auto;
  .dt_button {
    width: 100%;
  }
  &:empty {
    display: none;
  }
`;

export const StyledSummary = styled.div<{ isScrollOffSet: number }>`
  position: relative;
  display: flex;
  flex-direction: column;
  max-height: 85vh;

  background: ${colors.charcoalGray};
  color: ${colors.mediumGray};
  border-radius: 0;
  transition: border-radius 0.2s ease;

  .summaryTop,
  .summaryCompact {
    display: none;
  }

  /* body start */
  .summaryBody {
    display: flex;
    flex-direction: column;
    flex: 1;
    height: 100%;
    max-height: inherit;
    overflow-y: auto;

    /* inner scrollable section start */
    .summaryBodyInner {
      padding: 2rem 2.25rem 1.25rem;
      display: flex;
      flex-direction: column;
      padding-top: 2.25rem;
      height: 100%;
      max-height: inherit;
      overflow-y: auto;
      .title {
        padding-bottom: 1.75rem;
      }
      .summaryMonthly + .summaryUpfront {
        margin-top: 2.1rem;
      }
    }
    /* inner scrollable section end */

    .styledPaymentMethods {
      align-self: flex-end;
      margin: 2rem 0 1.5625rem;
      padding: 0 2.25rem;
    }
  }
  /* body end */

  /* ===========================STICKY START===================== */
  &.isSticky {
    border-radius: 0.75rem 0.75rem 0 0;
    transition: transform 0.5s ease !important;
    position: fixed;
    bottom: 0;
    z-index: 998;
    width: 100%;
    transform: translateZ(0)
      translateY(calc(100% - ${({ isScrollOffSet }) => isScrollOffSet}px));

    .summaryTop {
      display: block;
      text-align: center;
      i.expand-icon {
        display: none;
      }
      i.collapse-icon {
        display: block;
      }
    }

    .summaryCompact {
      display: block;
      position: relative;
      transition: 0.2s ease 0s;
      opacity: 1;
      .summaryCompactInner {
        padding: 0rem 2.25rem 1.5rem;
        position: absolute;
        width: 100%;
      }
    }

    .summaryBody {
      opacity: 0;
      transition: 0.2s linear 0s;
      .summaryBodyInner {
        /* height: 100%;
        padding-top: 2.25rem;
        overflow-y: auto; */
      }
    }
  }

  &.isSticky.isOpen {
    transform: translateZ(0) translateY(0%);
    .summaryTop {
      i.collapse-icon {
        display: none;
      }
      i.expand-icon {
        display: block;
      }
    }

    .summaryCompact {
      opacity: 0;
    }

    .summaryBody {
      opacity: 1;
    }
  }

  &.isSticky.removeSticky,
  &.isSticky.hideStickySummary {
    transition: 0.2s ease !important;
    transform: translateZ(0) translateY(100%);
  }

  /* ===========================STICKY START===================== */

  &.alwaysStatic {
    max-height: initial;
    ${StyledButtons} {
      display: block;
      opacity: 1;
      pointer-events: auto;
      visibility: visible;
    }
  }
  @media (max-width: ${breakpoints.tabletPortrait - 1}px) {
    &.alwaysStatic {
      ${StyledButtons} {
        position: fixed;
        bottom: 0;
        width: 100%;
        z-index: 2;

        &.isStaticSummarySlightlyScrolled {
          position: sticky;
        }
      }
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    &:not(.alwaysStatic) {
      display: none;
    }
    &.alwaysStatic {
      max-height: 100vh;
    }

    max-height: 100vh;
    flex-shrink: 0;
    position: sticky;
    z-index: 101;
    top: 0;

    &.hideStickySummary {
      display: none;
    }

    ${StyledButtons} {
      display: block;
      opacity: 1;
      pointer-events: auto;
      visibility: visible;
    }

    .summaryBody {
      min-width: 17.5rem;
      .summaryBodyInner {
        padding: 2rem 2.22rem 1.25rem 2.22rem;
        padding-top: 2.25rem;
        .heading {
          letter-spacing: 0.02rem;
        }
        .title {
          text-align: right;
          padding-bottom: 2.45rem;
        }
        .horizontal {
          margin-bottom: 0.5rem;
        }
        .summaryUpfront {
          margin-top: 2.4rem;
        }
      }
    }
    ${StyledFooterCopyrightMobile} {
      display: none;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .summaryBody {
      min-width: 17.5rem;
      .summaryBodyInner {
        padding: 2rem 2.22rem 1.25rem 2.22rem;
        padding-top: 4.5rem;
        .heading {
          letter-spacing: 0.02rem;
        }
        .title {
          text-align: right;
          padding-bottom: 2.45rem;
        }
        .horizontal {
          margin-bottom: 0.5rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    width: 18rem;
    .summaryBody .styledPaymentMethods {
      margin-top: 1.5rem;
    }
    .summaryBody {
      .summaryBodyInner {
        padding: 2rem 2.33rem 1.25rem 2.22rem;
        padding-top: 2.27rem;
        .heading {
          letter-spacing: 0.02rem;
        }
        .title {
          text-align: right;
          padding-bottom: 2.22rem;
        }
        .horizontal {
          margin-bottom: 0.55rem;
        }
      }
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 22.5rem;
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 22.5rem;
  }
`;

export const StyledFakeButton = styled.div`
  position: fixed;
  bottom: 0;
  width: 100%;
  left: 0;
  z-index: 998;
  transition: transform 0.2s ease !important;

  &.removeSticky,
  &.hideStickySummary {
    transform: translateZ(0) translateY(100%);
  }

  .dt_button {
    width: 100%;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: none;
  }
`;

export const StyledRefDiv = styled.div`
  height: 1px;
  width: 100%;
  position: absolute;
  left: 0;
  top: 0;
  flex-grow: 0;
  flex-shrink: 0;
  pointer-events: none;
`;
