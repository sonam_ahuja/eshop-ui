import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SvgDifferentAddress from '@common/components/Svg/different-address';
import SvgIdCard from '@common/components/Svg/id-card';
import SvgPayByLink from '@common/components/Svg/pay-by-link';
import SvgPayOnDelivery from '@common/components/Svg/pay-on-delivery';
import SvgPaymentCard from '@common/components/Svg/payment-card';
import SvgSameAsShipping from '@common/components/Svg/same-as-shipping';
import SvgSaveAsPersonal from '@common/components/Svg/save-as-personal';
import ErrorTimeOut from '@common/components/Svg/error-time-out';
import BillingSameAsShipping from '@common/components/Svg/billing-same-as-shipping';

describe('<SummaryCompact />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <div>
          <SvgDifferentAddress />
          <SvgIdCard />
          <SvgPayByLink />
          <SvgPayOnDelivery />
          <SvgPaymentCard />
          <SvgSameAsShipping />
          <SvgSaveAsPersonal />
          <ErrorTimeOut />
          <BillingSameAsShipping />
        </div>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
