import React from 'react';

// tslint:disable
export const SvgIdCard = () => {
  return (
    <div
      className="svgIdCard"
      dangerouslySetInnerHTML={{
        __html: `<svg
        xmlns="http://www.w3.org/2000/svg"
        width="226"
        height="160"
        viewBox="0 0 226 160"
      >
        <g fill="none" fillRule="evenodd">
          <path
            fill="#EDEDED"
            d="M185 133H10c-5.5 0-10-4.5-10-10V23c0-5.5 4.5-10 10-10h175c5.5 0 10 4.5 10 10v100c0 5.5-4.5 10-10 10"
          />
          <path
            fill="#C2C2C2"
            d="M185 13H10C4.5 13 0 17.5 0 23v100c0 5.5 4.5 10 10 10h175c5.5 0 10-4.5 10-10V23c0-5.5-4.5-10-10-10m0 3c3.9 0 7 3.1 7 7v100c0 3.9-3.1 7-7 7H10c-3.9 0-7-3.1-7-7V23c0-3.9 3.1-7 7-7h175"
          />
          <g fill="#A4A4A4" fillRule="nonzero">
            <path d="M149 81h24v-4h-24v4zm25-5v6h-26v-6h26zM113 62v4h60v-4h-60zm61-1v6h-62v-6h62zM81 92v4h44v-4H81zm45-1v6H80v-6h46zM81 107v4h52v-4H81zm53-1v6H80v-6h54zM81 77v4h60v-4H81zm61-1v6H80v-6h62zM81 62v4h24v-4H81zm25-1v6H80v-6h26z" />
          </g>
          <g fill="#A4A4A4" fillRule="nonzero">
            <path d="M22 35v12h20V35H22zm21-1v14H21V34h22z" />
            <path d="M32 45a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            <path d="M21 41.5v-1h7.5v1zM35 41.5v-1h8v1z" />
          </g>
          <g transform="translate(154 88)">
            <circle cx="36" cy="36" r="34.5" fill="#FFF" />
            <path
              fill="#E20074"
              fillRule="nonzero"
              d="M36 72C16.1 72 0 55.9 0 36S16.1 0 36 0s36 16.1 36 36-16.1 36-36 36zm0-69C17.8 3 3 17.8 3 36s14.8 33 33 33 33-14.8 33-33S54.2 3 36 3z"
            />
          </g>
          <path
            fill="#E20074"
            fillRule="nonzero"
            d="M185.5 133.9c-.5 0-1-.2-1.4-.6l-6.7-6.4c-.5-.5-.7-1.2-.6-1.9.2-.7.7-1.2 1.4-1.5.7-.3 1.4 0 1.9.5l5.4 5.1 14.3-14.4c.8-.8 2-.8 2.8 0 .8.8.8 2 0 2.8l-15.7 15.8c-.3.4-.9.6-1.4.6z"
          />
          <path
            fill="#C2C2C2"
            fillRule="nonzero"
            d="M43 95.9c5.6-.1 10-4.6 10.1-10.1v-7.6c-.1-5.6-4.5-10-10.1-10.1-5.6.1-10.1 4.6-10.1 10.1v7.6c0 5.5 4.5 10 10.1 10.1zm12.8 3.4H30.3c-5.1 0-9.2 4.1-9.3 9.2v3.5h43.9v-3.5c0-2.4-.9-4.8-2.7-6.5-1.6-1.7-4-2.7-6.4-2.7z"
          />
          <path
            fill="#A4A4A4"
            fillRule="nonzero"
            d="M81 38v4h93v-4H81zm94-1v6H80v-6h95z"
          />
        </g>
      </svg>
    `
      }}
    />
  );
};

export default SvgIdCard;

// tslint:enable
