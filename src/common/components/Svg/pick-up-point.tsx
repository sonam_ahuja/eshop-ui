import React from 'react';

// tslint:disable
export const SvgPickUpPoint = () => {
  return (
    <div
      className="svgPickUpPoint"
      dangerouslySetInnerHTML={{
        __html: `<svg xmlns="http://www.w3.org/2000/svg" width="226" height="226" viewBox="0 0 226 226">
        <g fill="none" fill-rule="evenodd" transform="translate(0 13)">
            <path fill="#F2F2F2" d="M112.163 195.867h61.94V58.593z" opacity=".5"/>
            <path fill="#A4A4A4" fill-rule="nonzero" d="M63.615 142.296s-28.292-14.062-20.424-36.83l1.842 2.68s4.352.669 4.854 2.51c2.512 11.384 18.75 28.292 18.75 28.292l-5.022 3.348z"/>
            <path fill="#C2C2C2" fill-rule="nonzero" d="M36.83 101.281c-5.86-.334-10.547 1.675-11.719 2.512C9.877 112.33 0 135.6 0 135.6h5.022s6.864-10.714 16.741-18.415v33.482h28.46v-39.508c0-8.873-11.217-9.71-13.393-9.878z"/>
            <path fill="#A4A4A4" fill-rule="nonzero" d="M18.75 190.175l4.687 9.04-1.674 1.674-11.719-6.696 5.023-6.697z"/>
            <path fill="#A4A4A4" fill-rule="nonzero" d="M21.763 192.519l-8.37-5.023 20.926-35.155 9.207 10.379z"/>
            <path fill="#C2C2C2" fill-rule="nonzero" d="M58.593 187.496l10.044 1.674.837 1.674-12.555 6.697-3.349-6.697z"/>
            <path fill="#C2C2C2" fill-rule="nonzero" d="M53.57 192.519l-23.437-43.526 13.393-5.023 18.415 43.526z"/>
            <path fill="#EDEDED" d="M0 28.46v30.133h174.104v137.274h26.785V28.459z"/>
            <path fill="#C2C2C2" fill-rule="nonzero" d="M200.889 200.889h-88.726v-5.022h83.704V31.807H0v-5.022h200.889z"/>
            <circle cx="198.378" cy="27.622" r="25.948" fill="#FFF"/>
            <path fill="#C2C2C2" fill-rule="nonzero" d="M198.378 55.244c-15.234 0-27.622-12.388-27.622-27.622S183.144 0 198.378 0 226 12.388 226 27.622s-12.388 27.622-27.622 27.622zm0-51.896c-13.393 0-24.274 10.882-24.274 24.274 0 13.393 10.881 24.274 24.274 24.274 13.392 0 24.274-10.881 24.274-24.274 0-13.392-10.882-24.274-24.274-24.274z"/>
            <ellipse cx="36.829" cy="85.375" fill="#C2C2C2" fill-rule="nonzero" rx="10.044" ry="13.392" transform="rotate(-4.267 36.829 85.375)"/>
            <path fill="#A4A4A4" fill-rule="nonzero" d="M26.953 86.047s19.921 1.842 19.921-9.877v-5.859H35.156s-10.045.67-8.203 15.736z"/>
            <path fill="#A3A3A3" fill-rule="nonzero" d="M172.43 58.593h1.674v137.274h-1.674zM112.163 58.593h1.674v142.296h-1.674z"/>
            <g fill-rule="nonzero" transform="translate(187.496 13.393)">
                <path fill="#E1017A" d="M20.926 10.044c0 8.37-10.045 18.415-10.045 18.415S.837 17.578.837 10.044C.837 4.52 5.357 0 10.881 0c5.525 0 10.045 4.52 10.045 10.044z"/>
                <circle cx="10.881" cy="8.873" r="4.52" fill="#FFF"/>
            </g>
        </g>
    </svg>`
      }}
    />
  );
};

export default SvgPickUpPoint;

// tslint:enable
