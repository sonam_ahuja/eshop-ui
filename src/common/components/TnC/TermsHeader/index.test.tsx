import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import TermsHeader, { IProps } from '@src/common/components/TnC/TermsHeader';

describe('<TermsHeader />', () => {
  const props: IProps = {
    translation: appState().translation.cart.checkout.orderReview
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <TermsHeader {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
