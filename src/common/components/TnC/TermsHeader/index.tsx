import React from 'react';
import { Paragraph, Title } from 'dt-components';
import { IOrderReviewTranslation } from '@common/store/types/translation';

import { StyledTermsHeader } from './styles';

export interface IProps {
  translation: IOrderReviewTranslation;
}
const TermsHeader = (props: IProps) => {
  const { translation } = props;

  return (
    <StyledTermsHeader>
      <Title size='xlarge' weight='ultra'>
        {translation.termsAndCondition}
      </Title>

      <Paragraph size='small' weight='normal'>
        {translation.termsAndConditionsDescriptions}
      </Paragraph>
    </StyledTermsHeader>
  );
};

export default TermsHeader;
