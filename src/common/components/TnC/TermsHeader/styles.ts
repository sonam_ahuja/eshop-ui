import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledTermsHeader = styled.div`
  .dt_title {
    color: ${colors.darkGray};
    padding-right: 7rem;
    margin-bottom: 0.25rem;
  }
  .dt_paragraph {
    color: ${colors.ironGray};
    padding-right: 3.5rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .dt_title {
      margin-bottom: 0.5rem;
    }
    .dt_paragraph {
      padding-right: 20.15rem;
    }
  }
`;
