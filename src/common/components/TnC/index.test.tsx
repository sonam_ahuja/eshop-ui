import React from 'react';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { IProps } from '@checkout/index';

import TnC from './';

describe('<SimpleLoader />', () => {
  const props: IProps = {
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    ...histroyParams,
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    basket: appState().basket,
    personalInfo: appState().checkout.personalInfo,
    checkout: {
      ...appState().checkout,
      checkout: {
        ...appState().checkout.checkout,
        termsAndConditionData: {
          termsAndConditions: [
            {
              id: '',
              name: '',
              description: '',
              version: ''
            }
          ],
          links: [
            {
              name: '',
              relativeUrl: '',
              absoluteUrl: ''
            },
            {
              name: '',
              relativeUrl: '',
              absoluteUrl: 'dummy'
            }
          ]
        }
      }
    },
    translation: appState().translation,
    configuration: appState().configuration,
    isUserLoggedIn: true,
    setAddressBillingType: jest.fn(),
    setBillingType: jest.fn(),
    fetchBasket: jest.fn(),
    setBillingAddress: jest.fn(),
    setNewAddessShipping: jest.fn(),
    updateNewAddessShipping: jest.fn(),
    fetchCheckoutAddress: jest.fn(),
    setSmsNotification: jest.fn(),
    setProceedToShippingButton: jest.fn(),
    updateInputField: jest.fn(),
    editPaymentStepClick: jest.fn(),
    fetchPaymentMethods: jest.fn(),
    placeOrderButton: jest.fn(),
    setStickySummary: jest.fn(),
    downloadPdf: jest.fn(),
    openTnCModal: jest.fn(),
    fetchBasketAndContact: jest.fn(),
    fetchCheckoutAddressLoading: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <TnC {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, open is false', () => {
    const newProps = { ...props, open: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('on terms and condition click', () => {
    const component = shallow<TnC>(<TnC {...props} />);
    component.instance().showTermAndConditions();
    component.instance().relativePdfUrlClick('/basket');
  });
});
