import React, { Component, ReactNode } from 'react';
import { IProps } from '@checkout/index';
import { Accordion } from 'dt-components';
import { ILink, ITnC } from '@src/common/routes/checkout/store/types';
import { StyledAccordionWithBorderBottom } from '@src/common/components/Accordion/AccordionWithBorderBottom/styles';
import AccordionLink from '@src/common/components/Accordion/AccordionLink';

import TermsHeader from './TermsHeader';
import { DialogBoxTnc } from './styles';

export default class TnC extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  showTermAndConditions = () => {
    this.props.openTnCModal(false);
    document.body.style.overflow = null;
  }

  relativePdfUrlClick = (url: string) => {
    return () => {
      this.props.downloadPdf(url);
    };
  }

  render(): ReactNode {
    const { translation, checkout, configuration } = this.props;
    const {
      isTermsAndConditionCMSDriven,
      termsAndConditionsUrl
    } = configuration.cms_configuration.modules.checkout.orderReview;

    const { openTnCModal, termsAndConditionData } = checkout.checkout;

    return (
      <DialogBoxTnc
        isOpen={openTnCModal}
        showCloseButton={true}
        type='fullHeight'
        closeOnEscape={true}
        className=''
        onClose={this.showTermAndConditions}
        onBackdropClick={this.showTermAndConditions}
        closeOnBackdropClick={true}
        onEscape={this.showTermAndConditions}
      >
        {isTermsAndConditionCMSDriven ? (
          <iframe src={termsAndConditionsUrl} />
        ) : (
          <div className='tncWrapper'>
            <div className='tncHeader'>
              <TermsHeader
                translation={translation.cart.checkout.orderReview}
              />
            </div>
            <div className='tncBody'>
              <div className='terms-accordion-wrap'>
                {termsAndConditionData.termsAndConditions.map((faqs: ITnC) => {
                  return (
                    // tslint:disable-next-line:jsx-key
                    <StyledAccordionWithBorderBottom>
                      <Accordion
                        isOpen={false}
                        headerTitle={
                          <>
                            <div key={faqs.id} className='lists'>
                              {faqs.name}
                            </div>
                          </>
                        }
                      >
                        <div className='accordionBody'>
                          <div key={faqs.id} className='lists'>
                            {faqs.description}
                          </div>
                        </div>
                      </Accordion>
                    </StyledAccordionWithBorderBottom>
                  );
                })}
                {termsAndConditionData.links.map((link: ILink) => {
                  if (link.absoluteUrl) {
                    return (
                      <AccordionLink
                        className='accordionLink'
                        href={link.absoluteUrl}
                        text={link.name}
                        key={link.name}
                      />
                    );
                  } else {
                    return (
                      <AccordionLink
                        className='accordionLink'
                        onClick={this.relativePdfUrlClick(
                          link.relativeUrl as string
                        )}
                        text={link.name}
                        key={link.name}
                      />
                    );
                  }
                })}
              </div>
            </div>
          </div>
        )}
      </DialogBoxTnc>
    );
  }
}
