import styled from 'styled-components';
import { DialogBox } from 'dt-components';
import { breakpoints } from '@src/common/variables';

export const DialogBoxTnc = styled(DialogBox)`
  .dialogBoxContentWrap {
    padding: 2rem 1.25rem 0;
    height: 100vh;
    .closeIcon {
      z-index: 10;
    }
    .dialogBoxContent {
      padding-top: 4.25rem;
      padding-bottom: 2.75rem;
      width: 100%;
      height: 100%;
      overflow-y: auto;
    }
    .terms-accordion-wrap {
      margin: 3rem 0 0 0rem;
      /* .accordionLink:last-child {
        .header {
          border-bottom: none;
        }
      } */
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .dt_overlay {
      overflow: auto;
    }
    .closeIcon {
      top: 2rem;
      right: 2rem;
    }
    .dialogBoxContentWrap {
      width: 58.5rem;
      margin: 4.5rem 0;
      padding: 5rem;
      /* max-height: calc(100vh - 9rem); */
      height: auto;
      /* overflow-y: auto; */
      .terms-accordion-wrap {
        padding-left: 8.5rem;
      }
      .dialogBoxContent {
        max-height: inherit;
        padding: 0;
      }
    }
  }
`;
