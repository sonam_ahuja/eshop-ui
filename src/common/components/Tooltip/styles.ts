import styled from 'styled-components';
import { Tooltip } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';
import { IProps } from 'dt-components/lib/es/components/atoms/tooltip/types';
import { ComponentType } from 'react';

export const AppTooltip = styled(Tooltip)`
  &.dt_tooltip {
    padding: 1.5rem;
    width: 13.125rem;

    font-size: 0.75rem;
    line-height: 1.33;
    letter-spacing: normal;
    color: ${colors.cloudGray};
    z-index: 999;
    .section {
      ul {
        margin-bottom: 0.25rem;
      }
    }
    .section + .section {
      margin-top: 1.5rem;
    }

    .iconClose {
      position: absolute;
      top: -0.5rem;
      right: -0.5rem;
      cursor: pointer;
    }
    .titleMain {
      position: relative;
      padding-bottom: 0.5rem;
    }
    .dt_divider {
      color: ${colors.warmGray};
      min-height: 1px;
      height: 1px;
      margin: 0.5rem 0;
    }
    .totalMonths {
      color: ${colors.mediumGray};
    }

    ul {
      display: flex;
      justify-content: space-between;
      margin-bottom: 0.25rem;
      li {
        width: 55%;
        word-break: break-word;
        /* &:first-child {
          text-transform: capitalize;
        } */
        &:last-child {
          text-align: right;
          width: 45%;
        }
      }
    }

    .section {
      ul:last-child {
        margin-bottom: 0;
      }
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    &.dt_tooltip {
      .section {
        ul {
          margin-bottom: 0.35rem;
        }
      }
    }
  }
` as ComponentType<IProps>;
