import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import UnorderedList, { IProps } from '@common/components/UnorderedList';
import appState from '@store/states/app';

describe('<UnorderedList />', () => {
  const props: IProps = {
    className: 'string',
    list: 'string',
    translation: appState().translation.prolongation.productDetails,
    variantId: 'newVariant'
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <UnorderedList {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, className is undefined', () => {
    const newProps = { ...props, className: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
