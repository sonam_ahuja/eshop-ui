import React, { Component } from 'react';
import cx from 'classnames';
import { Anchor } from 'dt-components';
import { IProlongationProductDetails } from '@src/common/store/types/translation';
import he from 'he';
import appConstants from '@src/common/constants/appConstants';

import { StyledHighlights, StyledUnorderedList } from './styles';

export interface IProps {
  className?: string;
  list: string;
  translation: IProlongationProductDetails;
  variantId: string;
}

export interface IState {
  showMore: boolean;
  isShowMoreLinkVisible: boolean;
}

class Highlights extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showMore: false,
      isShowMoreLinkVisible: false
    };

    this.toggleHighlights = this.toggleHighlights.bind(this);
    this.updateHighlights = this.updateHighlights.bind(this);
    this.setRefOuterContainer = this.setRefOuterContainer.bind(this);
    this.setRefInnerContainer = this.setRefInnerContainer.bind(this);
  }
  private outer: HTMLElement | null = null;
  private inner: HTMLElement | null = null;

  toggleHighlights(): void {
    this.setState({
      showMore: !this.state.showMore
    });
  }

  setRefOuterContainer(ref: HTMLElement | null): void {
    this.outer = ref;
  }

  setRefInnerContainer(ref: HTMLElement | null): void {
    this.inner = ref;
  }

  updateHighlights(): void {
    this.setState(
      {
        showMore: false
      },
      () => {
        const scrollableContainer = this.outer;
        const containerInner = this.inner;
        const scrollableContainerHeight = scrollableContainer
          ? scrollableContainer.clientHeight
          : 0;
        const containerInnerHeight = containerInner
          ? containerInner.clientHeight
          : 0;

        if (containerInnerHeight > scrollableContainerHeight) {
          this.setState({
            isShowMoreLinkVisible: true
          });
        } else {
          this.setState({
            isShowMoreLinkVisible: false
          });
        }
      }
    );
  }

  componentDidMount(): void {
    this.updateHighlights();
  }

  componentDidUpdate(prevProps: IProps): void {
    if (this.props.variantId !== prevProps.variantId) {
      this.updateHighlights();
    }
  }

  render(): React.ReactNode {
    const { list, className } = this.props;

    const classes = cx(className);

    const anchorText = this.state.showMore
      ? this.props.translation.showLess
      : this.props.translation.showMore;
    const anchorEl = this.state.isShowMoreLinkVisible ? (
      <div className='anchorWrapper'>
        <Anchor
          hreflang={appConstants.LANGUAGE_CODE}
          title={anchorText}
          onClickHandler={this.toggleHighlights}
        >
          {anchorText}
        </Anchor>{' '}
      </div>
    ) : null;

    return (
      <StyledHighlights showMore={this.state.showMore}>
        <div id='scrollableContainer' ref={this.setRefOuterContainer}>
          <StyledUnorderedList
            ref={this.setRefInnerContainer}
            id='containerInner'
            dangerouslySetInnerHTML={{ __html: he.decode(list) }}
            className={classes}
          />
        </div>
        {anchorEl}
      </StyledHighlights>
    );
  }
}

export default Highlights;
