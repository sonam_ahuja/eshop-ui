import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledUnorderedList = styled.div`
  ol,
  ul {
    padding-top: 0rem;
  }

  li {
    position: relative;
    margin-bottom: 0.25rem;
    /* display: flex; */
    display: block;
    padding-left: 20px;

    font-size: 0.9375rem;
    line-height: 1.25rem;
    color: ${colors.darkGray};
    font-weight: 500;

    /* checkIcon start */
    &:after {
      content: '';
      width: 15px;
      height: 8px;
      border: 3px solid ${colors.magenta};
      border-radius: 2px;
      border-top: 0;
      border-right: 0;
      transform: rotate(-45deg);
      position: absolute;
      top: 5px;
      left: 0;
    }
    /* checkIcon end */

    .icons {
      color: ${colors.magenta};
      font-size: 1rem;
      padding-right: 0.75rem;
      width: 1.75rem;
      margin-top: 2px;
    }
    &:last-child {
      margin-bottom: 0;
    }
  }

  /* managing tags */
  strong,
  b {
    font-weight: bold;
  }

  em,
  i {
    font-style: italic;
  }
`;

export const StyledHighlights = styled.div<{ showMore: boolean }>`
  #scrollableContainer {
    overflow: hidden;
    max-height: ${props => (props.showMore ? `auto` : '13rem')};
  }
  .anchorWrapper {
    /* safari unexpected behaviour */
    margin: 0.5rem 0 1rem;
  }
  .anchorWrapper a {
    font-size: 0.9375rem;
    display: inline-block;
  }

  @media (min-width: ${breakpoints.mobile}px) {
    .anchorWrapper {
      margin-bottom: 0;
    }
  }
`;
