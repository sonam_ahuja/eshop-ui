import styled from 'styled-components';

export const StyledHorizontalScrollRow = styled.div`
  /* HORIZONTAL SCROLL START */
  display: flex;
  flex-wrap: nowrap;
  /* Make this scrollable when needed */
  overflow-x: auto;
  /* We don't want vertical scrolling */
  overflow-y: hidden;
  /* Make an auto-hiding scroller for the 3 people using a IE */
  -ms-overflow-style: -ms-autohiding-scrollbar;
  -webkit-overflow-scrolling: auto;
  scroll-behavior: smooth;
  /* For WebKit implementations, provide inertia scrolling */
  /* -webkit-overflow-scrolling: touch; */
  /* We don't want internal inline elements to wrap */
  /* white-space: nowrap; */
  /* Remove the default scrollbar for WebKit implementations */

  &::-webkit-scrollbar {
    display: none;
    width: 0 !important ;
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
  &,
  * {
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
  /* HORIZONTAL SCROLL END */

  .horizontalScrollRowInner {
    display: flex;
  }

  .horizontalScrollableItem:last-child {
    position: relative;
    &:last-child:after {
      content: '';
      width: 20px;
      height: 1px;
      position: absolute;
      right: -20px;
    }
  }
`;
