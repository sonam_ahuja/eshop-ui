import apiEndpoints from '@common/constants/apiEndpoints';
describe('constant ', () => {
  test('apiEndpoints get basket Object with url params', () => {
    expect(apiEndpoints.BASKET.GET_BASKET_DATA.url).toBeDefined();
  });
  test('apiEndpoints get basket Object with no url params', () => {
    expect(apiEndpoints.BASKET.GET_BASKET_DATA.url).toBeDefined();
  });

  test('apiEndpoints get checkout number porting no url params', () => {
    expect(
      apiEndpoints.CHECKOUT.NUMBER_PORTING.TERM_CONDITION.url('lifeCycleStatus')
    ).toBeDefined();
  });
});
