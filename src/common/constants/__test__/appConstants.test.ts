import appConstants from '@common/constants/appConstants';

describe('constant ', () => {
  test('value should render in correct format', () => {
    expect(appConstants.TRANSLATION_BASE_URL).toBeDefined();
    expect(appConstants.CONFIGURATION_BASE_URL).toBeDefined();
    expect(appConstants.ESHOP_BASE_URL).toBeDefined();
    expect(appConstants.LANGUAGE_CODE).toBeDefined();
    expect(appConstants.COUNTRY_CODE).toBeDefined();
  });
});
