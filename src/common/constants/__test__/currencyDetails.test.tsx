import { currencyDetails } from '@common/constants/currencyDetails';

describe('currency constant ', () => {
  test('value should render in correct format', () => {
    expect(currencyDetails).toBeDefined();
  });
});
