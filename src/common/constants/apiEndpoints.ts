// tslint:disable

export default {
  CONFIG: {
    GET_CMS_CONFIGURATION: {
      url: '/latest/config.json',
      method: 'GET',
      params: []
    },
    GET_CMS_TRANSLATION: {
      url: '/web-strings.json',
      method: 'GET',
      params: []
    },
    GET_MEGA_MENU: {
      url: (cmsConfigKey: string, language: string) =>
        `megamenu/config/all/${cmsConfigKey}?language=${language}`,
      method: 'GET',
      params: []
    },
    GET_PROLONGATION_MARKETING: {
      url: (cmsConfigKey: string, language: string) =>
        `/hns/config/all/${cmsConfigKey}?language=${language}`,
      method: 'GET',
      params: []
    }
  },
  LANDING_PAGE: {
    GET_STATIC_CONTENT: {
      url: '/landing/v1/content',
      method: 'GET',
      params: []
    },
    GET_DYNAMIC_CONTENT: {
      url: '/productOfferings/listing',
      method: 'POST'
    }
  },
  GOOGLE_MAP: {
    GEOCODE: {
      url: 'https://maps.googleapis.com/maps/api/place/autocomplete/',
      method: 'GET',
      params: []
    }
  },
  BASKET: {
    VALIDATE_CART_PRICE: {
      url: 'checkout/v1/cartValidation',
      method: 'PATCH',
      params: []
    },
    GET_BASKET_DATA: {
      url: (virtualBasketId?: string | null) =>
        virtualBasketId
          ? `shoppingCart?virtualBasketId=${virtualBasketId}`
          : 'shoppingCart',
      method: 'GET',
      params: []
    },
    DELETE_BASKET: {
      url: 'shoppingCart',
      method: 'DELETE',
      params: ['cartId']
    },
    DELETE_BASKET_ITEM: {
      url: 'shoppingCart',
      method: 'PATCH',
      params: []
    },
    UPDATE_BASKET_ITEM: {
      url: 'shoppingCart',
      method: 'PATCH',
      params: []
    }
  },
  PRODUCTLIST: {
    PROLONGATION: {
      GET_PRODUCTS_LIST: {
        url: (profileId: string) =>
          `/productOfferings/prolongation/listing?profileId=${profileId}`,
        method: 'POST'
      },
      CHECKOUT: {
        url: (profileId: string) =>
          `/productOfferings/prolongation/orderDetails?profileId=${profileId}`,
        method: 'POST'
      },
      PLACE_ORDER: {
        url: (profileId: string) =>
          `/productOfferings/prolongation/lead?profileId=${profileId}`,
        method: 'POST'
      },
      GET_LEAD: {
        url: (authToken: string) =>
          `/profiles/?sub=${authToken}&magentaEnabled=false`,
        method: 'GET'
      },
      GET_RSA_KEY: {
        url: `/prolongation/lead/key`,
        method: 'GET'
      },
      POST_LEAD: {
        url: (uuid: string) => `/lead/api/v1/cp/generate/?uuid=${uuid}`,
        method: 'POST'
      },
      GENERATE_TOKEN: {
        url: '/cdn/v1/generate/token',
        method: 'GET'
      },
      REFRESH_TOKEN: {
        url: '/cdn/v1/refresh/token',
        method: 'GET'
      },
      MOENGAGE_PUSH_EVENTS: {
        url: '/dps/push/v1/events',
        method: 'POST'
      }
    },
    GET_PRODUCTS_LIST: {
      url: '/productOfferings/listing',
      method: 'POST'
    },
    SEND_STOCK_NOTIFICATION: {
      url: '/productOfferings/notification',
      method: 'POST'
    }
  },
  AUTHENTICATION: {
    USERNAME_PASSWORD_LOGIN: {
      url: 'login',
      method: 'POST',
      params: []
    },
    EMAIL_OTP_REQUEST: {
      url: 'login/pin',
      method: 'POST',
      params: []
    },
    EMAIL_OTP_LOGIN: {
      url: 'login/pin',
      method: 'PUT',
      params: []
    },
    MSISDN_OTP_REQUEST: {
      url: 'login/pin',
      method: 'POST',
      params: []
    },
    MSISDN_OTP_LOGIN: {
      url: 'login/pin',
      method: 'PUT',
      params: []
    },
    RECOVERY: {
      url: 'recovery',
      method: 'POST'
    },
    PROFILE_CHECK: {
      url: 'profile-check',
      method: 'POST'
    },
    SOCIAL_REQUEST: {
      url: (callbackUrl: string) =>
        `login/social/facebook?redirectUrl=${callbackUrl}`,
      method: 'GET'
    },
    SOCIAL_LOGIN: {
      url: (callbackUrl: string, code: string) =>
        `login/social/facebook?redirectUrl=${callbackUrl}&facebookCode=${code}`,
      method: 'POST'
    },
    EMAIL_VERIFICATION: {
      url: (email: string) => `/login/email/checkUnique?email=${email}`,
      method: 'GET'
    },
    USER_REGISTERATION: {
      url: `/login/email/registration`,
      method: 'POST'
    },
    GOOGLE_LOGIN: {
      url: (callbackUrl: string, code: string) =>
        `login/social/google?redirectUrl=${callbackUrl}&googleCode=${code}`,
      method: 'POST'
    },
    HEADER_ENRICHMENT: {
      url: 'login/header-enrichment',
      method: 'POST'
    },
    PROFILE: {
      url: 'profile',
      method: 'GET'
    },
    logout: {
      url: 'login',
      method: 'DELETE'
    }
  },
  CHECKOUT: {
    GET_ORDER_DETAILS: {
      url: `checkout/order/`,
      method: 'GET',
      params: []
    },
    GET_PERSONA_INFO_DATA: {
      url: `checkout/contacts`,
      method: 'GET',
      params: []
    },
    GET_PAYMENT_INFO_DATA: {
      url: `checkout/paymentMethods`,
      method: 'GET',
      params: []
    },
    PATCH_PAYMENT_UPFRONT_INFO: {
      url: `checkout/paymentMethods`,
      method: 'PATCH',
      params: []
    },
    POST_PAYMENT_UPFRONT_INFO: {
      url: `checkout/paymentMethods`,
      method: 'POST',
      params: []
    },
    GET_CREDIT_QUALIFY: {
      url: `checkout/creditQualify`,
      method: 'GET',
      params: []
    },
    GET_CREDIT_SCORE: {
      url: `checkout/creditScore`,
      method: 'GET',
      params: []
    },
    GET_TERMS_AND_CONDITION: {
      url: `consents/tnc`,
      method: 'GET',
      params: []
    },
    POST_SUPPORT_CALL: {
      url: `checkout/quote`,
      method: 'POST'
    },
    POST_PAY_FULL_PRICE: {
      url: `checkout/payFullPrice`,
      method: 'POST',
      params: []
    },
    POST_MARKETING_DISCOUNT_CONSENT: {
      url: `v1/consents/getSummary`,
      method: 'POST',
      params: []
    },
    GET_ADDRESSES: {
      url: (searchTerm: string, searchSize: number) =>
        `checkout/addresses?page=0&searchTerm=${searchTerm}&size=${String(
          searchSize
        )}`,
      method: 'GET',
      params: []
    },
    SET_PERSONAL_INFO_DATA: {
      url: `checkout/personal`,
      method: 'POST',
      params: []
    },
    UPDATE_PERSONAL_INFO_DATA: {
      url: `checkout/personal`,
      method: 'PATCH',
      params: []
    },
    BILLING: {
      ADD_BILLING_ADDRESS: {
        url: 'checkout/billing',
        method: 'POST',
        params: []
      },
      UPDATE_BILLING_ADDRESS: {
        url: 'checkout/billing',
        method: 'PATCH',
        params: []
      },
      E_BILL: {
        url: 'checkout/billing/fee/eBill',
        method: 'GET',
        params: []
      },
      PAPER_BILL: {
        url: 'checkout/billing/fee/paperBill',
        method: 'GET',
        params: []
      }
    },
    SHIPPING: {
      GET_DELIVERY_DATE: {
        url: (specId: string): string =>
          `checkout/delivery/applicableDates?productSpecificationId=${specId}`,
        method: 'GET',
        params: []
      },
      SET_PERSONAL_SHIPPING_ADDRESS: {
        url: `checkout/shipping`,
        method: 'POST',
        params: []
      },
      UPDATE_PERSONAL_SHIPPING_ADDRESS: {
        url: `checkout/shipping`,
        method: 'PATCH',
        params: []
      },
      GET_SHIPPING_FEE: {
        url: `checkout/shipping/fee`,
        method: 'POST',
        params: []
      },
      GET_PICK_UP_POINTS: {
        url: 'checkout/shippingPoints',
        method: 'POST',
        params: []
      }
    },
    NUMBER_PORTING: {
      VALIDATE_PHONE_NUMBER: {
        url: 'v1/otp/send/validate',
        method: 'POST',
        params: []
      },
      GET_RESOURCE_POOL: {
        url: (resourceType: string): string =>
          `v1/resources?resourceType=${resourceType}`,
        method: 'GET',
        params: []
      },
      UPDATE_CART: {
        url: 'v1/resource/cart',
        method: 'PATCH',
        params: []
      },
      TERM_CONDITION: {
        url: (
          lifeCycleStatus: string = 'ACTIVE',
          privacyGroup: string = 'TERMS_OF_SERVICE',
          profileType: string = 'MNP'
        ): string =>
          `v1/partyPrivacyProfileType?lifeCycleStatus=${lifeCycleStatus}&privacyGroup=${privacyGroup}&profileType=${profileType}`,
        method: 'GET',
        params: []
      }
    },
    PLACE_ORDER: {
      url: `checkout/order`,
      method: 'POST',
      params: []
    },
    GET_AGREEMENTS: {
      url: `consents/order`,
      method: 'GET'
    },
    GET_PDF: {
      url: (url: string): string =>
        `/documentManagement/pdf?relativeUrl=${url}`,
      method: 'GET',
      params: []
    }
  },
  CATEGORY: {
    GET_CATEGORIES: {
      url: (id: string): string => `/productOfferings/category?id=${id}`,
      method: 'GET',
      params: []
    }
  },
  TARIFF: {
    GET_TARIFF_PLAN: {
      url: '/productOfferings/tariffs',
      method: 'POST',
      params: []
    },
    SAVE_TARIFF_PLAN: {
      url: '/productOfferings/preferences',
      method: 'POST',
      params: []
    },
    GET_TARIFF_ADDON_PRICE: {
      url: (id: string) => `/productOfferings/tariffs/${id}`,
      method: 'POST',
      params: []
    }
  },
  PRODUCT_DETAILED: {
    GET_CATEGORY_ATTRIBUTES: {
      url: '',
      method: 'GET',
      params: []
    },
    GET_PRODUCT_DETAILED: {
      url: 'productOfferings/details',
      method: 'POST',
      params: []
    },
    GET_JSON_LD: {
      url: `productOfferings/jsonLD`,
      method: 'POST',
      params: []
    },
    NOTIFY_ME: {
      url: 'productOfferings/notification',
      method: 'POST',
      params: []
    },
    GET_PRODUCT_SPECIFICATIONS: {
      url: (id: string) => `productOfferings/specifications?id=${id}`,
      method: 'GET',
      params: []
    },
    GET_BINKIES: {
      url: (contentId: string) =>
        `https://embed.binki.es/contentavailable/kxRQOYWn/${contentId}`,
      method: 'GET'
    }
  },
  SEARCH: {
    GET_SEARCH: {
      url: (query: string) => `search/?query=${encodeURIComponent(query)}`,
      method: 'GET'
    },
    GET_CATEGORY_LIST: {
      url: (query: string) =>
        `search/categoryList?query=${encodeURIComponent(query)}`,
      method: 'GET'
    },
    GET_SEARCH_LIST: {
      url: `search/listing`,
      method: 'POST'
    },
    GET_SEARCH_SUGGEST: {
      url: (query: string) =>
        `search/suggest?query=${encodeURIComponent(query)}`,
      method: 'GET'
    }
  },
  IDENTITY_VERIFICATION: {
    GET_VERIFICATION_PROVIDERS: {
      url: 'idVerification/v1/providers',
      method: 'GET',
      params: []
    },
    VERIFY_USER_IDENTITY: {
      url: 'idVerification/v1/provider',
      method: 'POST',
      params: []
    },
    OTP_VERIFICATION: {
      url: '/v1/otp/send/validate',
      method: 'POST',
      params: []
    }
  }
  // tslint:disable-next-line:max-file-line-count
};
