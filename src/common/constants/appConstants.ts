export default {
  CONFIGURATION_BASE_URL: '',
  ESHOP_CDN_BASE_URL: '',
  CONFIGURATION_API_KEY: '',
  TRANSLATION_BASE_URL: '',
  ESHOP_BASE_URL: '',
  LANGUAGE_CODE: '',
  COUNTRY_CODE: '',
  CONTENT_LANGUAGE: '',
  LOADER_MIN_DURATION: 300,
  CHANNEL: '',
  RUM_SERVER_URL: '',
  RUM_SERVER_NAME: '',
  RUM_VERSION: '',
  // tslint:disable-next-line:max-line-length
  PUBLIC_KEY: `-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4kmzj3sOxZ0i9KfUECP9kMjY3ejZWRPDHfgrgLF8Ih261xm+d1cySEJ01qdO6/HnIYIqE1sDYEs/xzprSbcO/amdbHz4/iSSdtyaVbQfEmmdiAabYE3E7g8jW0MRTGo+9CmE0M4sUxDVyeW1lkD+Ojiboi7xrscr5GcA/4e8NSAQbOu1UXXNunzWNu1AWZ5nQGux8sfWN8UkARn03/xNY2hK8VlwrZo5pdc26JHfiDRvka00VIs5DuWJc0oi/fDcVN1A6eD1oszKlhFZV5olTz8c+00rlvbD2QCv+NtdfkXCRg831E66nrduqUZbRS1iUpl8AFXCCK9dQ82D7Pf40wIDAQAB-----END PUBLIC KEY-----`,
  OPTIONAL: 'optionalText',
  NUMBER_OF_OTP_INPUTS: 4,
  LOGIN: 'login',
  ENVIRONMENT: '',
  USERNAME: 'username',
  EMAIL: 'email',
  ESHOP_CDN_STATIC_URL: '',
  PHONE_NUMBER: 'phoneNumber',
  IS_USER_LOGGED_IN: 'IsLoggedIn',
  USER_ID: 'userId',
  DEVICE_ID: 'c17416c7-39bc-418d-acde-ea3e0fb5cd47',
  DEVICE_MODAL: 'Samsung 910N',
  PUSH_TOKEN: 'f16PCS40qN',
  OS: 'android',
  COUNTRY: 'header-country',
  LANGUAGE: 'language',
  FOOTER: 'footer',
  MNP: 'MNP',
  S3_IMAGE_URL:
    'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius',
  MNS: 'MNS',
  BINKIES_PAGE_STYLE: (baseUrl: string) => `${baseUrl}/css/binkies.css`,
  BINKIES_MODAL_STYLE: (baseUrl: string) => `${baseUrl}/css/modal-style.css`,
  BINKIES_URL:
    'https://binkiescontentnode.blob.core.windows.net/integrations/kxRQOYWn/4jkk0ski/integration.js',
  CMSONEAPP:
    'https://cmsqa-cdn.yo-digital.com/cdn/hns/config/all/uLQvjoR2abQcwvByJB3KyyNJSQbbCDIL?language=en',
  WEB_VIEW_CONFIGURATION_BASE_URL: '',
  WEB_VIEW_TRANSLATION_BASE_URL: '',
  WEB_VIEW_TRANSLATION_API_KEY: '',
  WEB_VIEW_CONFIGURATION_API_KEY: '',
  ONEAPP_BASE_URL: 'http://bfftest.yo-digital.com/bff',
  UNLIMITED_TARIFF: '-1',
  MOENGAGE_BASE_URL:
    'https://eshop-oneshop-nginx-dev.eshop.yo-digital.com/oamoengage',
  CLIENT_ID: '',
  SECRET_KEY: '',
  WEB_VIEW_MARKETING_URL: '',
  PROLONGATION_INSTANCE_KEY: '',
  MEGA_MENU_CMS_BASE_URL: '',
  MEGA_MENU_FOOTER_KEY: '',
  MEGA_MENU_HEADER_KEY: '',
  UNIQUE_IDENTIFIER: 'unique_identifier'
};

export const REGEX = {
  EXTRACT_BASE_URL: /(http(s)?:\/\/)|(\/.*){1}/g,
  VALIDATE_URL: /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g,
  EMAIL: /^\w+([.-]\w+)*@\w+([.-]\w+)*(\.\w{2,4})+$/,
  PHONE: /^(\(?\+?[0-9]*\)?)?[0-9\- ]*$/
};

export const VALIDATIONS = {
  email: REGEX.EMAIL,
  phone: REGEX.PHONE,
  url: REGEX.VALIDATE_URL
};

export const IMAGE_TYPES = {
  HDPI: 'hdpi',
  MDPI: 'mdpi',
  XHDPI: 'xhdpi',
  XXHDPI: 'xxhdpi',
  XXXHDPI: 'xxxhdpi',
  ORIGINAL: 'original'
};

export const HISTORY_STATES = {
  POP: 'POP',
  PUSH: 'PUSH'
};

export const ONEAPP_PARAMS = {
  CHANNEL: 'channel',
  UUID: 'uuid',
  SEGMENT_ID: 'segmentId',
  PROFILE_ID: 'profileId',
  LANGUAGE_CODE: 'language'
};

export const DATE_FORMATTING = 'MM/DD/YYYY';

export const TARIFF_CATEGORY = 'tariff-mobile-postpaid';
export const BIRTH_DATE_LIMIT = 'BirthDateLimit';

export const CMS_REPLACEABLE_KEY = '{0}';

export const SHOW_PLAN_NAME = 'showPlanName'; //
export const SHOW_BENEFITS = 'showBenefits';
export const SHOW_BUY_BUTTON = 'showBuyButton';
export const SHOW_ADDONS = 'showAddons';
export const SHOW_UNLIMITED_BENEFITS = 'showUnlimitedBenefits';
export const SHOW_MONTHLY_PRICE = 'showMonthlyPrice';
export const SHOW_ADD_PHONE_BUTTON = 'showAddPhoneButton';

export enum ERROR_CODE_BFF {
  LEAD_ALREADY_EXIST = 'leadAlreadyExist',
  DT_UNKNOWN_ERROR = 'dtUnknownError',
  MANDATORY_ATTRIBUTE_NULL_OR_EMPTY = 'mandatoryMissing',
  VARIANTS_NOT_FOUND = 'variantsNotFound',
  HANDLE_401 = 'handle401',
  CANCELED_MSG = 'canceledMsg',
  CANCELED_STATUS_CODE = 'canceledStatusCode',
  DT_PAYMENT_METHOD_NOT_FOUND = 'paymentMethodNotFound'
}

export enum KEY_CODE {
  ENTER = 13
}

export enum PAGE {
  SEARCH = 'search'
}
