/**
 * @param
 * all the harcoded keys for sales catalog
 */
export default {
  CATEGORY_LANDING_ONLINE_OFFER: 'categoryOnlineOffer',
  CATEGORY_SPECIAL_OFFER_SECTION_1: 'categorySpecialOffersSection1',
  CATEGORY_SPECIAL_OFFER_SECTION_2: 'categorySpecialOffersSection2',
  CATEGOERY_FANTASTIC_DEVICE_OFFERS: 'categoryFantasticDeviceOffers',
  CATEGORY_BEST_DEVICE_OFFERS: 'bestDeviceOffers',
  CATEGORY_PARENT_SLUG: 'parentSlug',
  CATEGORY_SLUG: 'slug',
  CATEGORY_IMAGE: 'image',
  CATEGORY_BACKGROUND_IMAGE: 'landingPageBackgroundImage',
  CATEGORY_HIGHLIGHTPRODUCT: 'highLightProduct',
  CATEGORY_THEME: 'theme',
  BEST_DEVICE_OFFER_CATEGORY_ID: 'bestDeviceOffersCategoryId',
  PRODUCT_DETAILED_HIGHIGHTS_SECTION: 'highlightProductDetailedSection',
  PRODUCT_DETAILED_MARKETING_CONTENT: 'marketingContent',
  PRODUCT_DETAILED_EXTRA_FEATURE: 'productExtraFeature',
  PRODUCT_DETAILED_VARIANT_TAGS: 'variantTags',
  PRODUCT_DETAILED_LAUNCH_DATE: 'launchDate',
  PRODUCT_DETAILED_BINKIES_ID: 'binkies3dID',
  CATEGORY_HIDE_BUY_DEVICE_ONLY: 'hideBuyDeviceOnly',
  PRODUCT_PROMOTION: 'promotion',
  META_TAG_TITLE: 'metaTagTitle',
  META_TAG_DESCRIPTION: 'metaTagDescription',
  PRODUCT_IMAGE_FRONT: 'front',
  PRODUCT_IMAGE_BACK: 'back',
  PRODUCT_IMAGE_RIGHT: 'right',
  PRODUCT_IMAGE_LEFT: 'left',
  PRODUCT_IMAGE_BOTTOM: 'bottom',
  PRODUCT_IMAGE_TOP: 'top',
  PRODUCT_IMAGE_FRONT_AND_BACK: 'frontAndBack',
  THUMBNAIL_IMAGE: 'thumbnail',
  PRODUCT_LIST_BACKGROUND_IMAGE: 'listingPageBackgroundImage',
  PRODUCT_LIST_THEME: 'theme',
  SORTING_LIST: 'sortBy',
  DEFAULT_SORT: 'defaultSortBy',
  FILTER_CHIPS: 'Button',
  LABEL_WITH_SUPER: 'Label',
  VARIANT_SWITCHER: 'ColorPicker',
  BADGE: 'Range',
  BEST_DEVICE_OFFER: 'bestDeviceOffers',
  BFF_AVAILABILITY_TRANSLATION_ID: 'availability',
  TARIFF_HYBRID_PLAN_BANNER: 'hybridPlansBanner',
  TARIFF_DATA_ONLY_PLAN_BANNER: 'dataOnlyPlansBanner',
  TARIFF_FAMILY_BENIFITS: 'familyBenifits',
  TARIFF_MAGENTA_DISCOUNTS: 'magentaDiscounts',
  TARIFF_BACKGROUND_IMAGE: 'listingPageBackgroundImage',
  TARIFF_TERM_AND_CONDITION: 'termAndCondition',
  TARIFF_THEME: 'theme',
  TARIFF_FAQ: 'tariffFaq',
  TARIFF_BEST_DEVICE_OFFERS: 'bestDeviceOffers',
  BFF_PRICE_RANGE_ID: 'priceRange',
  POPULAR_INDEX: 'popularityIndex',
  BEST_DEVICE_OFFERS_CATEGORY_SLUG: 'slug',
  PRODUCT_LISTING_SELECTED_TARIFF: 'selectedTariff',
  PRODUCT_LISTING_SELECTED_AGREEMENT: 'selectedProductOfferingTerm',
  PRODUCT_LISTING_SELECTED_DISCOUNT: 'discount',
  TARIFF_CATEGORY_HIDE_LOYALTY_DROPDOWN: 'showLoyaltyDropdown',
  TARIFF_CATEGORY_HIDE_DISCOUNT_DROPDOWN: 'showDiscountDropdown',
  TARIFF_HD_ENABLED: 'hdEnabled'
};

export const TARIFF_BENEFIT_TEMPLATES = {
  IMAGE: 'IMAGE',
  BUTTON: 'BUTTON',
  DROPDOWN: 'DROPDOWN',
  SIMPLE: 'SIMPLE'
};

export const TARIFF_BENEFIT_TEMPLATE_TYPE = {
  SINGLE_SELECT: 'SINGLE_SELECT',
  MULTI_SELECT: 'MULTI_SELECT'
};
