/** ALSO USED ON PROLONGATION */
export default {
  SEARCH: {
    MOBILE_SEARCH: 'mobile-search'
  },
  HEADER: {
    MENU: 'menu',
    SUBMENU: 'submenu'
  },
  TARIFF: {
    YOUNG_AGE: 'YOUNG_AGE'
  },
  DEVICE_DETAILED: {
    YOUNG_TARIFF: 'YOUNG_TARIFF'
  },
  COMMON: {
    SUMMARY: 'summary'
  },
  BASKET: {
    REMOVE: 'remove',
    DELETE: 'delete'
  },
  MOBILE_APP: {
    CHECKOUT: {
      ORDER_PANEL: 'order-panel',
      TERMS_AND_CONDITION: 'terms-and-condition'
    }
  }
};
