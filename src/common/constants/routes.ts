export default {
  HOME: '/',
  BASKET: '/basket',
  CHECKOUT: {
    BASE: '/checkout',
    PERSONAL_INFO: '/checkout/personal-info',
    SHIPPING: '/checkout/shipping',
    BILLING: '/checkout/billing',
    PAYMENT: '/checkout/payment',
    ORDER_REVIEW: '/checkout/order-review'
  },
  CATEGORY: '/category',
  ORDER_CONFIRMATION: '/order-confirmation',
  AUTHENTICATION: '/authentication',
  PRODUCT_LIST: '/product-list',
  PRODUCT_DETAIL: '/productDetailed',
  TARIFF: '/tariff',
  ERROR: '/error',
  NOT_FOUND: '/not-found',
  SEARCH: '/search'
};
