import APP_CONSTANTS, { NOT_APPLICABLE } from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';
import { IProductDetailedData } from '@src/common/routes/productDetailed/store/types';
import { getProductDetailedState } from '@store/common/index';
import { getDevicePrice, getTariffPrice } from '@events/common/index';
import { TARIFF_CATEGORY } from '@src/common/constants/appConstants';
import { IPrices } from '@src/common/routes/tariff/store/types';
import { PRICES } from '@productList/store/enum';

export const sendProductDetailedViewEvent = (
  productDetailsData: IProductDetailedData
) => {
  if (
    productDetailsData &&
    productDetailsData.variants &&
    productDetailsData.variants.length
  ) {
    const productDetailedState = getProductDetailedState();
    const installmentEnabled = productDetailedState.instalmentId !== '0';
    const category = productDetailedState.categoryId;
    const price = installmentEnabled
      ? getDevicePrice(productDetailsData.variants[0].prices)
      : getDeviceBasePrice(productDetailsData.variants[0].prices);
    const data = {
      event: APP_CONSTANTS.DEVICE_DETAIL.EVENTS.PRODUCT_DETAIL_VIEW,
      ecommerce: {
        detail: {
          actionField: { list: '' },
          products: [
            {
              name: productDetailsData.productName,
              id: productDetailsData.variants[0].id,
              price: price ? price.discountedValue : null,
              brand: productDetailsData.variants[0].brand,
              category,
              variant: productDetailsData.variants[0].name
            }
          ]
        }
      }
    };
    if (productDetailsData.tariffs) {
      const selectedTariff = productDetailsData.tariffs.find(
        tariff => tariff.isSelected
      );
      if (selectedTariff) {
        const tariffPrice = selectedTariff.totalPrices
          ? getTariffPrice(selectedTariff.totalPrices)
          : null;
        data.ecommerce.detail.products.push({
          name: selectedTariff.name,
          id: selectedTariff.id,
          price: tariffPrice ? tariffPrice.discountedValue : null,
          brand: NOT_APPLICABLE,
          category: TARIFF_CATEGORY,
          variant: selectedTariff.name
        });
      }
    }
    sendEventGTM(data);
  }
};

export const getDeviceBasePrice = (variantPrices: IPrices[]) => {
  return variantPrices.find(
    (prices: IPrices) => prices.priceType === PRICES.BASE_PRICE
  );
};

export const sendOnDeviceDetailToolTipEvent = (
  productName: string,
  price: number
) => {
  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.Tooltip,
    Event_Category: APP_CONSTANTS.DEVICE_DETAIL.EVENTS.DEVICE_DEATIL,
    Event_Action: APP_CONSTANTS.DEVICE_DETAIL.ACTION.DEVICE_DEATIL_TOOLTIP,
    Event_Label: productName,
    Event_Value: price
  });
};
