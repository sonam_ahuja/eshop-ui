import APP_CONSTANTS from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';
import { IProductListItem, IVariant } from '@productList/store/types';
import * as productListApi from '@common/types/api/productList';
import { getDevicePrice } from '@events/common/index';
import { getCommonState, getConfiguration } from '@common/store/common/index';
import { currencyDetails } from '@src/common/constants/currencyDetails';
import { IProductImpression } from '@events/types';

export const sendValidateCredentialEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.VALIDATE_CREDENTIAL
  });
};

export const sendApplyFilterEvent = (
  filterName: string,
  filterCounts: number
) => {
  sendEventGTM({
    event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.FILTER_APPLIED,
    Filter_Name: filterName,
    Filter_Count: filterCounts
  });
};

export const sendMostPopularEvent = (sortName: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.MOST_POPULAR_CLICKS,
    SortBy_Name: sortName
  });
};

export const sendClickedProductListEvent = (
  productList: IProductListItem[],
  productId: string,
  categoryId: string,
  list: string
) => {
  const index = productList.findIndex(
    (product: IProductListItem) => product.variant.productId === productId
  );

  const price = getDevicePrice(productList[index].variant.prices);
  const configuration = getConfiguration();
  const currencyLocale = configuration.cms_configuration.global.currency.locale;

  let data = {};
  if (index !== -1) {
    data = {
      event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.PRODUCT_CLICKS,
      ecommerce: {
        currencyCode:
          currencyLocale && currencyDetails[currencyLocale]
            ? currencyDetails[currencyLocale].symbol
            : null,
        click: [
          {
            actionField: { list },
            name: productList[index].variant.productName,
            id: productList[index].variant.id,
            price: price ? price.discountedValue : null,
            brand: productList[index].variant.brand,
            category: categoryId,
            variant: productList[index].variant.name,
            position: index + 1
          }
        ]
      }
    };
    sendEventGTM(data);
  }
};

export const sendClickedProductEvent = (
  productList: IVariant[],
  productId: string,
  categoryId: string,
  list: string
) => {
  const index = productList.findIndex(
    (product: IVariant) => product.productId === productId
  );
  const price = getDevicePrice(productList[index].prices);
  const configuration = getConfiguration();
  const currencyLocale = configuration.cms_configuration.global.currency.locale;

  let data = {};
  if (index !== -1) {
    data = {
      event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.PRODUCT_CLICKS,
      ecommerce: {
        currencyCode:
          currencyLocale && currencyDetails[currencyLocale]
            ? currencyDetails[currencyLocale].symbol
            : null,
        click: [
          {
            actionField: { list },
            name: productList[index].productName,
            id: productList[index].id,
            price: price ? price.discountedValue : null,
            brand: productList[index].brand,
            category: categoryId,
            variant: productList[index].name,
            position: index + 1
          }
        ]
      }
    };
    sendEventGTM(data);
  }
};

export const sendProductImpressionEvent = (
  responseData: productListApi.POST.IResponse,
  categoryId: string
) => {
  const data = {
    event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.PRODUCT_IMPRESSION,
    ecommerce: {
      currencyCode: 'currencyCode',
      impressions: [] as IProductImpression[]
    }
  };

  const list = getCommonState().deviceList;

  responseData.data.forEach((product: IProductListItem, index: number) => {
    const price = getDevicePrice(product.variant.prices);
    const configuration = getConfiguration();
    const currencyLocale =
      configuration.cms_configuration.global.currency.locale;

    data.ecommerce.currencyCode =
      currencyLocale && currencyDetails[currencyLocale]
        ? currencyDetails[
            configuration.cms_configuration.global.currency.locale
          ].symbol
        : null;

    data.ecommerce.impressions.push({
      name: product.variant.productName,
      id: product.variant.id,
      price: price ? price.discountedValue : null,
      brand: product.variant.brand,
      category: categoryId,
      variant: product.variant.name,
      list,
      position: index + 1
    });
  });
  if (data.ecommerce.impressions && data.ecommerce.impressions.length) {
    sendEventGTM(data);
  }
};
