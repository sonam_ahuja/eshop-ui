import {
  sendApplyFilterEvent,
  sendClickedProductEvent,
  sendClickedProductListEvent,
  sendMostPopularEvent,
  sendProductImpressionEvent,
  sendValidateCredentialEvent
} from '@events/DeviceList/index';
import { ProductListResponse2 } from '@common/mock-api/productList/productList.mock';

describe('Device list event test', () => {
  it('Test send apply filter event', () => {
    expect(sendApplyFilterEvent('', 1)).toBeUndefined();
  });
  it('Test send click product event', () => {
    const variants = [
      {
        id: '',
        productId: '',
        productName: '',
        name: '',
        description: '',
        group: '',
        bundle: true,
        prices: [],
        attachments: {},
        characteristics: [],
        eligibilityUnavailabilityReason: [],
        unavailabilityReasonCodes: []
      }
    ];
    expect(sendClickedProductEvent(variants, '', '', '')).toBeUndefined();
  });
  it('Test send product detail view event', () => {
    const productList = [
      {
        category: '',
        groups: {},
        variant: {
          id: '',
          productId: '',
          productName: '',
          name: '',
          description: '',
          group: '',
          bundle: true,
          prices: [],
          attachments: {},
          characteristics: [],
          eligibilityUnavailabilityReason: [],
          unavailabilityReasonCodes: []
        }
      }
    ];
    expect(
      sendClickedProductListEvent(productList, '', '', '')
    ).toBeUndefined();
  });
  it('Test send most popular event', () => {
    expect(sendMostPopularEvent('')).toBeUndefined();
  });
  it('Test send product detail view event', () => {
    expect(
      sendProductImpressionEvent(ProductListResponse2, '')
    ).toBeUndefined();
  });
  it('Test send validate credential', () => {
    expect(sendValidateCredentialEvent()).toBeUndefined();
  });
});
