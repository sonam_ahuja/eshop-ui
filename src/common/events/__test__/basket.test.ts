import {
  getAddonValue,
  getAttributes,
  getAttributesObject,
  getBasketAmount,
  getColor,
  getProductFromId,
  getProducts,
  getTariffAttributes,
  getTotalBasketItem,
  getTotalPirce,
  sendAddToBasketEvent,
  sendBasketDeleteItemEvent,
  sendClearBasketEvent,
  sendClickOnRemoveButtonEvent,
  sendContinueShoppingEvent,
  sendOnBasketLoadEvent,
  sendonBasketLoadTrigger,
  sendOnBasketRemoveParticulars,
  sendOnBasketSummaryToolTipEvent,
  sendOnBasketToolTipEvent,
  sendOnBasketTooltipParticulars,
  sendOutOfStockBasketEvent,
  sendPopupCloseEvent,
  sendTariffAddToBasketEvent
} from '@events/basket/index';
import { productDetailsResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { getBasketState } from '@store/common/index';
import { PRICE_TYPE, RECURRING_TYPE } from '@tariff/store/enum';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

import { ITEM_STATUS_EVENT_NAME } from '../constants/eventName';

// tslint:disable-next-line:no-big-function
describe('Basket event test', () => {
  it('Test add to basket', () => {
    expect(
      sendAddToBasketEvent(productDetailsResponse, '12', false)
    ).toBeUndefined();
  });
  it('Test delete basket', () => {
    expect(sendBasketDeleteItemEvent('12')).toBeUndefined();
  });
  it('Test clear basket', () => {
    expect(sendClearBasketEvent()).toBeUndefined();
  });
  it('Test add to basket', () => {
    const price = [
      {
        priceAlterations: [],
        price: 20,
        totalPrice: 20,
        currency: 'INR',
        priceType: 'basePrice'
      }
    ];

    expect(sendClickOnRemoveButtonEvent('2', price, '')).toBeUndefined();
  });
  it('Test load basket event', () => {
    expect(sendOnBasketLoadEvent()).toBeUndefined();
  });
  it('Test basket load trigger', () => {
    expect(sendonBasketLoadTrigger()).toBeUndefined();
  });
  it('Test basket tooltip', () => {
    const price = [
      {
        priceAlterations: [],
        price: 20,
        totalPrice: 20,
        currency: 'INR',
        priceType: 'basePrice'
      }
    ];

    expect(sendOnBasketToolTipEvent('', price, '')).toBeUndefined();
  });
  it('Test out of basket', () => {
    expect(sendOutOfStockBasketEvent(getBasketState())).toBeUndefined();
  });
  it('Test tariff and add to basket', () => {
    const plans = [
      {
        id: 'bests',
        name: 'BestS',
        description: '',
        group: '',
        isSelected: true,
        allBenefits: [],
        totalPrices: [],
        prices: [
          {
            priceType: PRICE_TYPE.RECURRING,
            actualValue: 200,
            currency: 'EUR',
            recurringChargePeriod: RECURRING_TYPE.MONTH,
            discounts: [],
            discountedValue: 200,
            recurringChargeOccurrence: 1,
            dutyFreeValue: 0,
            taxRate: 1,
            taxPercentage: 20
          }
        ],
        characteristics: [],
        benefits: [],
        commonBenefits: []
      }
    ];
    expect(sendTariffAddToBasketEvent('12', plans)).toBeUndefined();
  });
  it('Test attributes', () => {
    const characteristics = [
      {
        name: 'Brand',
        values: [
          {
            value: 'Nokia'
          }
        ]
      },
      {
        name: 'storage',
        values: [
          {
            value: '32GB'
          }
        ]
      },
      {
        name: 'colour',
        values: [
          {
            value: 'black'
          }
        ]
      },
      {
        name: 'numberOfInstalments',
        values: [
          {
            value: 'black'
          }
        ]
      },
      {
        name: 'Data',
        values: [
          {
            value: 'black'
          }
        ]
      },
      {
        name: 'minutes',
        values: [
          {
            value: 'black'
          }
        ]
      },
      {
        name: 'variantTags',
        values: [
          {
            value: 't1,t2'
          }
        ]
      },
      {
        name: 'metaTagDescription',
        values: [
          {
            value: ''
          }
        ]
      },
      {
        name: 'metaTagTitle',
        values: [
          {
            value: ''
          }
        ]
      }
    ];
    expect(
      getAttributes(characteristics, '', ITEM_STATUS_EVENT_NAME.ACTIVE)
    ).toBeDefined();
    expect(
      getAttributes(characteristics, 'tariff', ITEM_STATUS_EVENT_NAME.ACTIVE)
    ).toBeDefined();
    expect(
      // tslint:disable-next-line:no-any
      getAttributes(null as any, 'tariff', ITEM_STATUS_EVENT_NAME.ACTIVE)
    ).toBeDefined();
    const characteristics1 = [
      {
        name: 'Brand',
        values: [
          {
            label: '1',
            value: 'Nokia'
          }
        ]
      },
      {
        name: 'storage',
        values: [
          {
            label: '1',
            value: '32GB'
          }
        ]
      },
      {
        name: 'colour',
        values: [
          {
            label: '1',
            value: 'black'
          }
        ]
      },
      {
        name: 'numberOfInstalments',
        values: [
          {
            label: '1',
            value: 'black'
          }
        ]
      },
      {
        name: 'Data',
        values: [
          {
            label: '1',
            value: 'black'
          }
        ]
      },
      {
        name: 'minutes',
        values: [
          {
            label: '1',
            value: 'black'
          }
        ]
      },
      {
        name: 'variantTags',
        values: [
          {
            label: '1',
            value: 't1,t2'
          }
        ]
      },
      {
        name: 'metaTagDescription',
        values: [
          {
            label: '1',
            value: ''
          }
        ]
      },
      {
        name: 'metaTagTitle',
        values: [
          {
            label: '1',
            value: ''
          }
        ]
      }
    ];
    expect(
      getAttributes(
        characteristics1,
        'tariff',
        ITEM_STATUS_EVENT_NAME.OUT_OF_STOCK
      )
    ).toBeDefined();
  });
  it('Test tariff attributes', () => {
    const benefits = [
      { isSelectable: false, label: '', name: '', values: [], selected: true },
      {
        isSelectable: false,
        label: '',
        name: 'Data',
        values: [],
        selected: true
      },
      {
        isSelectable: false,
        label: '',
        name: 'minutes',
        values: [],
        selected: true
      }
    ];
    expect(getTariffAttributes(benefits, '')).toBeDefined();
    expect(getAddonValue(benefits, 'Data')).toBeDefined();
  });
  it('Test basket attributes', () => {
    expect(getAttributesObject()).toBeDefined();
  });
  it('Test basket amount', () => {
    const price = [
      {
        priceAlterations: [],
        price: 1,
        totalPrice: 1,
        currency: 'INR',
        priceType: ''
      }
    ];
    expect(getBasketAmount(price, '')).toBe(0);
  });
  it('Test basket attributes1', () => {
    expect(
      getProductFromId('123457', BASKET_ALL_ITEM.cartItems)
    ).toBeUndefined();
  });
  it('Test basket count', () => {
    expect(getTotalBasketItem(BASKET_ALL_ITEM.cartItems)).toBeDefined();
  });
  it('Test basket attributes2', () => {
    expect(getTotalPirce(BASKET_ALL_ITEM.cartSummary)).toBeDefined();
  });
  it('Test basket attributes3', () => {
    const char = [{ name: '', value: '' }];
    expect(getColor(char)).toBeDefined();
    const char1 = [{ name: 'aa', value: '' }];
    expect(getColor(char1)).toBeDefined();
  });
  it('Test basket attributes4', () => {
    expect(getProducts(BASKET_ALL_ITEM.cartItems, 'Active')).toEqual([
      {
        brand: undefined,
        // tslint:disable-next-line:no-duplicate-string
        category: 'tariff-mobile-postpaid',
        dimension10: '1',
        dimension11: '1',
        dimension12: '1',
        dimension14: 'NA',
        dimension15: 'NA',
        dimension16: 'NA',
        dimension17: 'Active',
        dimension18: 'in stock',
        dimension21: 'NA',
        dimension22: 'NA',
        dimension8: '1',
        dimension9: '1',
        id: '123458',
        // tslint:disable-next-line:no-duplicate-string
        name: 'Mobile M Plan',
        price: undefined,
        quantity: 13,
        variant: undefined
      },
      {
        brand: undefined,
        category: undefined,
        dimension10: '1',
        dimension11: '1',
        dimension12: '1',
        dimension14: 'NA',
        dimension15: 'NA',
        dimension16: 'NA',
        dimension17: 'Active',
        dimension18: 'in stock',
        dimension21: 'NA',
        dimension22: 'NA',
        dimension8: 'NA',
        dimension9: 'NA',
        id: '123458',
        name: 'Mobile M Plan',
        price: undefined,
        quantity: 13,
        variant: undefined
      }
    ]);
  });
  // tslint:disable-next-line:no-identical-functions
  it('Test basket attributes5', () => {
    expect(getProducts(BASKET_ALL_ITEM.cartItems, 'OutOfStock')).toEqual([
      {
        brand: undefined,
        category: 'tariff-mobile-postpaid',
        dimension10: '1',
        dimension11: '1',
        dimension12: '1',
        dimension14: 'NA',
        dimension15: 'NA',
        dimension16: 'NA',
        dimension17: 'OutOfStock',
        dimension18: 'in stock',
        dimension21: 'NA',
        dimension22: 'NA',
        dimension8: '1',
        dimension9: '1',
        id: '123458',
        name: 'Mobile M Plan',
        price: undefined,
        quantity: 13,
        variant: undefined
      },
      {
        brand: undefined,
        category: undefined,
        dimension10: '1',
        dimension11: '1',
        dimension12: '1',
        dimension14: 'NA',
        dimension15: 'NA',
        dimension16: 'NA',
        dimension17: 'OutOfStock',
        dimension18: 'in stock',
        dimension21: 'NA',
        dimension22: 'NA',
        dimension8: 'NA',
        dimension9: 'NA',
        id: '123458',
        name: 'Mobile M Plan',
        price: undefined,
        quantity: 13,
        variant: undefined
      }
    ]);
  });
  // tslint:disable-next-line:no-identical-functions
  it('Test basket attributes61', () => {
    expect(getProducts(BASKET_ALL_ITEM.cartItems, '')).toEqual([
      {
        brand: undefined,
        category: 'tariff-mobile-postpaid',
        dimension10: '1',
        dimension11: '1',
        dimension12: '1',
        dimension14: 'NA',
        dimension15: 'NA',
        dimension16: 'NA',
        dimension17: 'NA',
        dimension18: 'in stock',
        dimension21: 'NA',
        dimension22: 'NA',
        dimension8: '1',
        dimension9: '1',
        id: '123458',
        name: 'Mobile M Plan',
        price: undefined,
        quantity: 13,
        variant: undefined
      },
      {
        brand: undefined,
        category: undefined,
        dimension10: '1',
        dimension11: '1',
        dimension12: '1',
        dimension14: 'NA',
        dimension15: 'NA',
        dimension16: 'NA',
        dimension17: 'NA',
        dimension18: 'in stock',
        dimension21: 'NA',
        dimension22: 'NA',
        dimension8: 'NA',
        dimension9: 'NA',
        id: '123458',
        name: 'Mobile M Plan',
        price: undefined,
        quantity: 13,
        variant: undefined
      }
    ]);
  });

  it('Test basket attributes6', () => {
    expect(
      sendOnBasketTooltipParticulars(
        '',
        BASKET_ALL_ITEM.cartItems[0].itemPrice,
        'tariff'
      )
    ).toBeUndefined();
  });
  // tslint:disable-next-line:no-identical-functions
  it('Test basket attributes62', () => {
    expect(
      sendOnBasketTooltipParticulars(
        '',
        BASKET_ALL_ITEM.cartItems[0].itemPrice,
        'device'
      )
    ).toBeUndefined();
  });

  it('Test basket attributes7', () => {
    expect(
      sendOnBasketRemoveParticulars(
        '',
        BASKET_ALL_ITEM.cartItems[0].itemPrice,
        'tariff'
      )
    ).toBeUndefined();
  });
  // tslint:disable-next-line:no-identical-functions
  it('Test basket attributes8', () => {
    expect(
      sendOnBasketRemoveParticulars(
        '',
        BASKET_ALL_ITEM.cartItems[0].itemPrice,
        'device'
      )
    ).toBeUndefined();
  });
  it('Test basket attributes9', () => {
    expect(sendContinueShoppingEvent()).toBeUndefined();
  });

  it('Test sendPopupCloseEvent function ', () => {
    expect(sendPopupCloseEvent()).toBeUndefined();
  });

  it('Test sendOnBasketSummaryToolTipEvent funtion ', () => {
    expect(sendOnBasketSummaryToolTipEvent('name ', 0)).toBeUndefined();
  });
  // tslint:disable-next-line:max-file-line-count
});
