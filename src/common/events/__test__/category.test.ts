import {
  sendPromotionClickEvent,
  sendPromotionViewEvent
} from '@events/category/index';

describe('Checkout event test', () => {
  it('check promotion click event working', () => {
    expect(
      sendPromotionClickEvent('productDetailsResponse', '12')
    ).toBeUndefined();
  });

  // tslint:disable-next-line:no-identical-functions
  it('check second slot working', () => {
    expect(
      sendPromotionClickEvent('productDetailsResponse', 'slot1')
    ).toBeUndefined();
  });

  it('should promotion 2 event trigger', () => {
    expect(sendPromotionViewEvent('', '12')).toBeUndefined();
  });

  it('should promotion 1 event trigger', () => {
    expect(sendPromotionViewEvent('promotion', null)).toBeUndefined();
  });

  it('should promotion both event trigger', () => {
    expect(sendPromotionViewEvent('promotion', 'promotion')).toBeUndefined();
  });
});
