import {
  cancelPortSuccess,
  getShippingType,
  sendAddToCalEvent,
  sendChooseOtherMethodEvent,
  sendContentFilledOutEvent,
  sendEditDetailsEvent,
  sendModifyPaymentOptionEvent,
  sendOrderPlacedEvent,
  sendOtherSelectStoreEvent,
  sendPayDeviceFullEvent,
  sendPortMyNumberEvent,
  sendProceedEvent,
  sendRenewInfoEvent,
  sendRequestSupportEvent,
  sendRescheduleOrderEvent,
  sendSelectDiffDeviceEvent,
  sendSelectMachineEvent,
  sendSelectPointEvent,
  sendSelectStoreEvent,
  sendSelectThisStoreEvent,
  sendTrackOrderEvent,
  sendTransactionFailedEvent
} from '@events/checkout/index';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { orderResponse } from '@mocks/checkout/order';
import appState from '@store/states/app';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';
import * as funcBModule from '@store/common/index';

// tslint:disable-next-line:no-big-function
describe('Checkout event test', () => {
  it('Test port success', () => {
    expect(cancelPortSuccess()).toBeUndefined();
  });
  it('Test add to cal', () => {
    expect(sendAddToCalEvent()).toBeUndefined();
  });
  it('Test chose other method', () => {
    expect(sendChooseOtherMethodEvent()).toBeUndefined();
  });
  it('Test other store', () => {
    expect(sendOtherSelectStoreEvent()).toBeUndefined();
  });
  it('Test pay device', () => {
    expect(sendPayDeviceFullEvent()).toBeUndefined();
  });
  it('Test edit details', () => {
    expect(sendEditDetailsEvent('')).toBeUndefined();
  });
  it('Test port my number', () => {
    expect(sendPortMyNumberEvent('', '')).toBeUndefined();
  });
  it('Test proceed event', () => {
    expect(sendProceedEvent(2, '')).toBeUndefined();
  });
  it('Test proceed event with step 3', () => {
    expect(sendProceedEvent(3, '')).toBeUndefined();
  });
  it('Test proceed event with step 4', () => {
    expect(sendProceedEvent(4, '')).toBeUndefined();
  });
  it('Test proceed event', () => {
    const basketResponse = {
      priceNotificationStrip: {
        show: false,
        notification: ''
      },
      basketItems: [...BASKET_ALL_ITEM.cartItems],
      cartId: null,
      loading: false,
      error: null,
      pagination: {
        total: 0,
        current: null
      },
      isCheckoutEnable: true,
      cartSummary: [],
      notification: [],
      undoItems: [],
      oldCartId: null,
      removeBasketItem: {
        modalOpen: false,
        error: null,
        loading: false,
        selectedItem: null
      },
      clearBasket: {
        modalOpen: false,
        error: null,
        loading: false
      },
      undoAbleItems: [],
      showRemoveItemNotification: false,
      showAppShell: true,
      stickySummary: true,
      showBasketSideBar: false,
      showCartRestrictionMessage: false,
      proceedToCheckoutLoading: false
    };

    const spy = jest.spyOn(funcBModule, 'getBasketState');
    spy.mockReturnValue(basketResponse);

    expect(sendProceedEvent(2, '')).toBeUndefined();
  });
  it('Test proceed event for basket', () => {
    const basketResponse = {
      basketItems: [...BASKET_ALL_ITEM.cartItems],
      cartId: null,
      loading: false,
      error: null,
      pagination: {
        total: 0,
        current: null
      },
      isCheckoutEnable: true,
      cartSummary: [],
      notification: [],
      undoItems: [],
      oldCartId: null,
      removeBasketItem: {
        modalOpen: false,
        error: null,
        loading: false,
        selectedItem: null
      },
      clearBasket: {
        modalOpen: false,
        error: null,
        loading: false
      },
      undoAbleItems: [],
      showRemoveItemNotification: false,
      showAppShell: true,
      stickySummary: true,
      showBasketSideBar: false,
      proceedToCheckoutLoading: false
    };

    // tslint:disable-next-line:no-duplicate-string
    jest.mock('@store/common/index', () => ({
      getBasketState: jest.fn().mockReturnValue(basketResponse)
    }));
    expect(sendProceedEvent(2, '')).toBeUndefined();
  });
  it('Test renew info', () => {
    expect(sendRenewInfoEvent()).toBeUndefined();
  });
  it('Test request support event', () => {
    expect(sendRequestSupportEvent()).toBeUndefined();
  });
  it('Test reschedule order', () => {
    expect(sendRescheduleOrderEvent()).toBeUndefined();
  });
  it('Test select diff device', () => {
    expect(sendSelectDiffDeviceEvent()).toBeUndefined();
  });
  it('Test select machine', () => {
    expect(sendSelectMachineEvent()).toBeUndefined();
  });
  it('Test send point', () => {
    expect(sendSelectPointEvent()).toBeUndefined();
  });
  it('Test select store', () => {
    expect(sendSelectStoreEvent('')).toBeUndefined();
  });
  it('Test select this store', () => {
    expect(sendSelectThisStoreEvent('')).toBeUndefined();
  });
  it('Test send promotion', () => {
    expect(sendTrackOrderEvent()).toBeUndefined();
  });
  it('Test getShippingType', () => {
    expect(getShippingType('withinTime')).toBeDefined();
  });
  it('Test getShippingType2', () => {
    expect(getShippingType('standard')).toBeDefined();
  });
  it('Test getShippingType1', () => {
    expect(getShippingType('pickUpStore')).toBeDefined();
  });
  it('Test getShippingType1', () => {
    expect(getShippingType('')).toBeDefined();
  });
  it('test sendTransactionFailedEvent', () => {
    expect(sendTransactionFailedEvent('', 0, 0, 0)).toBeUndefined();
  });
  it('test sendContentFilledOutEvent', () => {
    expect(sendContentFilledOutEvent('', ''));
  });
  it('test sendModifyPaymentOptionEvent', () => {
    expect(sendModifyPaymentOptionEvent(''));
  });
  it('test order placed', () => {
    const basketResponse = {
      basketItems: [...BASKET_ALL_ITEM.cartItems],
      cartId: null,
      loading: false,
      error: null,
      pagination: {
        total: 0,
        current: null
      },
      isCheckoutEnable: true,
      cartSummary: [],
      notification: [],
      undoItems: [],
      oldCartId: null,
      removeBasketItem: {
        modalOpen: false,
        error: null,
        loading: false,
        selectedItem: null
      },
      clearBasket: {
        modalOpen: false,
        error: null,
        loading: false
      },
      undoAbleItems: [],
      showRemoveItemNotification: false,
      showAppShell: true,
      stickySummary: true,
      showBasketSideBar: false,
      proceedToCheckoutLoading: false
    };

    jest.mock('@store/common/index', () => ({
      getBasketState: jest.fn().mockReturnValue(basketResponse)
    }));
    jest.mock('@store/common/index', () => ({
      getCheckoutState: jest.fn().mockReturnValue(appState().checkout)
    }));
    expect(sendOrderPlacedEvent(orderResponse)).toBeUndefined();
  });
  it('test order placed', () => {
    const basketResponse = {
      priceNotificationStrip: {
        show: false,
        notification: ''
      },
      basketItems: [],
      cartId: null,
      loading: false,
      error: null,
      pagination: {
        total: 0,
        current: null
      },
      isCheckoutEnable: true,
      cartSummary: [],
      notification: [],
      undoItems: [],
      oldCartId: null,
      removeBasketItem: {
        modalOpen: false,
        error: null,
        loading: false,
        selectedItem: null
      },
      clearBasket: {
        modalOpen: false,
        error: null,
        loading: false
      },
      undoAbleItems: [],
      showRemoveItemNotification: false,
      showAppShell: true,
      stickySummary: true,
      showBasketSideBar: false,
      showCartRestrictionMessage: false,
      proceedToCheckoutLoading: false
    };

    const spy = jest.spyOn(funcBModule, 'getBasketState');
    spy.mockReturnValue(basketResponse);

    const checkoutState = funcBModule.getCheckoutState();
    checkoutState.payment.upFront.paymentTypeSelected =
      PAYMENT_TYPE.PAY_BY_LINK;
    const spy1 = jest.spyOn(funcBModule, 'getCheckoutState');
    spy1.mockReturnValue(checkoutState);

    expect(sendOrderPlacedEvent(orderResponse)).toBeUndefined();
    spy.mockRestore();
  });
  it('test order placed2', () => {
    const basketResponse = {
      priceNotificationStrip: {
        show: false,
        notification: ''
      },
      basketItems: [],
      cartId: null,
      loading: false,
      error: null,
      pagination: {
        total: 0,
        current: null
      },
      isCheckoutEnable: true,
      cartSummary: [],
      notification: [],
      undoItems: [],
      oldCartId: null,
      removeBasketItem: {
        modalOpen: false,
        error: null,
        loading: false,
        selectedItem: null
      },
      clearBasket: {
        modalOpen: false,
        error: null,
        loading: false
      },
      undoAbleItems: [],
      showRemoveItemNotification: false,
      showAppShell: true,
      stickySummary: true,
      showBasketSideBar: false,
      showCartRestrictionMessage: false,
      proceedToCheckoutLoading: false
    };

    const spy = jest.spyOn(funcBModule, 'getBasketState');
    spy.mockReturnValue(basketResponse);

    const checkoutState = funcBModule.getCheckoutState();
    checkoutState.payment.upFront.paymentTypeSelected =
      PAYMENT_TYPE.PAY_ON_DELIVERY;
    const spy1 = jest.spyOn(funcBModule, 'getCheckoutState');
    spy1.mockReturnValue(checkoutState);

    expect(sendOrderPlacedEvent(orderResponse)).toBeUndefined();
    spy.mockRestore();
  });
  it('test order placed6', () => {
    const basketResponse = {
      priceNotificationStrip: {
        show: false,
        notification: ''
      },
      basketItems: [...BASKET_ALL_ITEM.cartItems],
      cartId: null,
      loading: false,
      error: null,
      pagination: {
        total: 0,
        current: null
      },
      isCheckoutEnable: true,
      cartSummary: [],
      notification: [],
      undoItems: [],
      oldCartId: null,
      removeBasketItem: {
        modalOpen: false,
        error: null,
        loading: false,
        selectedItem: null
      },
      clearBasket: {
        modalOpen: false,
        error: null,
        loading: false
      },
      undoAbleItems: [],
      showRemoveItemNotification: false,
      showAppShell: true,
      stickySummary: true,
      showBasketSideBar: false,
      showCartRestrictionMessage: false,
      proceedToCheckoutLoading: false
    };
    basketResponse.basketItems[0].group = 'tariff';

    const spy = jest.spyOn(funcBModule, 'getBasketState');
    spy.mockReturnValue(basketResponse);

    orderResponse.cartItems[0].group = 'tariff';

    expect(sendOrderPlacedEvent(orderResponse)).toBeUndefined();
  });
  // tslint:disable-next-line:max-file-line-count
});
