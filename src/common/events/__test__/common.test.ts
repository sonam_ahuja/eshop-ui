import {
  getDevicePrice,
  getLoginType,
  getTariffPrice,
  sendDropdownClickEvent,
  sendErrorEvent,
  sendFooterClickEvent,
  sendHeaderClickEvent,
  sendSearchClickEvent,
  setLoginType
} from '@events/common/index';
import {
  pageViewEvent,
  sendCTAClicks,
  sendEventGTM,
  sendMessageToQueue
} from '@events/index';

describe('Common event test', () => {
  it('Test footer click', () => {
    expect(sendFooterClickEvent('', '')).toBeUndefined();
  });
  it('Test footer click1', () => {
    expect(getLoginType()).toBeDefined();
  });
  it('Test footer click2', () => {
    expect(setLoginType('')).toBeUndefined();
  });
  it('Test footer click3', () => {
    expect(sendDropdownClickEvent('')).toBeUndefined();
  });
  it('Test header click', () => {
    expect(sendHeaderClickEvent('')).toBeUndefined();
  });
  it('Test search click', () => {
    expect(sendSearchClickEvent('', '')).toBeUndefined();
  });
  it('Test page view click', () => {
    expect(pageViewEvent('')).toBeUndefined();
  });
  it('Test footer click', () => {
    expect(sendFooterClickEvent('', '')).toBeUndefined();
  });
  it('Test CTA click', () => {
    expect(sendCTAClicks('', '')).toBeUndefined();
  });
  it('Test send event to GTM', () => {
    expect(sendEventGTM({})).toBeUndefined();
  });
  it('Test message to queue', () => {
    expect(sendMessageToQueue({})).toBeUndefined();
  });
  it('Test message to queue', () => {
    expect(sendMessageToQueue({ test: '1' })).toBeUndefined();
  });
  it('Test error 1', () => {
    expect(sendErrorEvent(502, '502')).toBeUndefined();
  });
  it('Test error 2', () => {
    expect(sendErrorEvent(500, '500')).toBeUndefined();
  });
  it('Test error 2', () => {
    expect(sendErrorEvent(400, '400')).toBeUndefined();
  });

  it('Test send tariff price to GTM', () => {
    const price = [
      {
        priceType: '',
        actualValue: 1,
        currency: '',
        recurringChargePeriod: '',
        discounts: [],
        discountedValue: 1,
        recurringChargeOccurrence: 1,
        dutyFreeValue: 1,
        taxRate: 1,
        taxPercentage: 1
      }
    ];
    expect(getTariffPrice(price)).toBeUndefined();
  });
  it('Test device price to queue', () => {
    const price = [
      {
        priceType: '',
        actualValue: 1,
        currency: '',
        recurringChargePeriod: '',
        discounts: [],
        discountedValue: 1,
        recurringChargeOccurrence: 1,
        dutyFreeValue: 1,
        taxRate: 1,
        taxPercentage: 1
      }
    ];
    expect(getDevicePrice(price)).toBeUndefined();
  });
});
