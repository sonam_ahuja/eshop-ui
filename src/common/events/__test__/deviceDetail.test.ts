import {
  getDeviceBasePrice,
  sendOnDeviceDetailToolTipEvent,
  sendProductDetailedViewEvent
} from '@events/DeviceDetail/index';
import { productDetailsResponse } from '@common/mock-api/productDetailed/details.mock';

describe('Device detail event test', () => {
  it('Test send product detail view event', () => {
    expect(
      sendProductDetailedViewEvent(productDetailsResponse)
    ).toBeUndefined();
  });
  it('Test get deveice Base price function ', () => {
    expect(
      getDeviceBasePrice(productDetailsResponse.variants[0].prices)
    ).toBeDefined();
  });
  it('Test send product detail view event 1', () => {
    productDetailsResponse.variants[0].prices = [];
    expect(
      sendProductDetailedViewEvent(productDetailsResponse)
    ).toBeUndefined();
  });
  it('Test sendOnDeviceDetailToolTipEvent detail view ', () => {
    expect(sendOnDeviceDetailToolTipEvent('abc ', 11)).toBeUndefined();
  });
});
