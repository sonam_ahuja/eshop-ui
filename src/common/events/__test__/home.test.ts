import {
  sendLandingPageProductImpressionEvent,
  sendLandingPagePromotionViewEvent
} from '@events/home/index';
import {
  ProductListResponse,
  ProductListResponse2
} from '@common/mock-api/productList/productList.mock.ts';

describe('Home page events /', () => {
  test('check sendLandingPagePromotionViewEvent function ', () => {
    expect(sendLandingPagePromotionViewEvent()).toBeUndefined();
  });

  test('test sendLandingPageProductImpressionEvent function ', () => {
    expect(
      sendLandingPageProductImpressionEvent(ProductListResponse, 'id')
    ).toBeUndefined();
    expect(
      sendLandingPageProductImpressionEvent(ProductListResponse2, 'id')
    ).toBeUndefined();
  });
});
