import {
  callEvent,
  findClickHandlerElement,
  logEvent,
  trackClickEvent
} from '@events/listener';
import EVENT_NAME from '@events/constants/eventName';

interface IEvent extends Event {
  path?: HTMLElement[];
}

describe('Listener event test', () => {
  const el = document.createElement('div');

  it('Test send tariff discount event', () => {
    expect(logEvent('')).toBeUndefined();
  });
  it('Test click handler event', () => {
    const a = new Event('');
    const event = {
      path: [
        {
          dataset: {
            eventId: '12'
          },
          ...el
        }
      ],
      ...a
    };
    expect(findClickHandlerElement(event)).toMatchObject({
      dataset: { eventId: '12' }
    });
    const event1 = {
      path: undefined
    };
    expect(findClickHandlerElement(event1 as IEvent)).toBeNull();

    const event2 = null;
    // tslint:disable-next-line:no-any
    expect(findClickHandlerElement(event2 as any)).toBeNull();
  });
  it('Test send  add listener event', () => {
    expect(trackClickEvent()).toBeUndefined();
  });
  it('Test send  add listener event with empty event id', () => {
    // event.dataset.eventId && event.dataset.eventMessage
    const event = {
      ...el,
      dataset: {
        eventId: '',
        eventMessage: ''
      }
    };
    expect(callEvent(event)).toBeUndefined();
    const event1 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS,
        eventMessage: 'y',
        eventPath: 'abc.bcd.cde'
      }
    };
    expect(callEvent(event1)).toBeUndefined();
  });
  it('Test send  add listener event', () => {
    // event.dataset.eventId && event.dataset.eventMessage
    const event = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event)).toBeUndefined();
    const event1 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event1)).toBeUndefined();
    const event2 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.CHECKOUT.EVENTS.REQUEST_SUPPORT_CALL,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event2)).toBeUndefined();
    const event3 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.CHECKOUT.EVENTS.SELECT_DIFF_DEVICE,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event3)).toBeUndefined();
    const event4 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.CHECKOUT.EVENTS.PAY_FULL_PRICE,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event4)).toBeUndefined();
    const event5 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_OTHER,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event5)).toBeUndefined();
    const event6 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.CHECKOUT.EVENTS.CANCEL_PORT,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event6)).toBeUndefined();
    const event7 = {
      ...el,
      dataset: {
        eventId: EVENT_NAME.DEVICE_CATALOG.EVENTS.VALIDATE_CREDENTIAL,
        eventMessage: 'y'
      }
    };
    expect(callEvent(event7)).toBeUndefined();
  });
  const event7 = {
    ...el,
    dataset: {
      eventId: EVENT_NAME.TARIFF.EVENTS.YOUNG_TARIFF_VALIDATE,
      eventMessage: 'y'
    }
  };
  expect(callEvent(event7)).toBeUndefined();
  const event8 = {
    ...el,
    dataset: {
      eventId: EVENT_NAME.TARIFF.EVENTS.ADD_TO_PHONE,
      eventMessage: 'y'
    }
  };
  expect(callEvent(event8)).toBeUndefined();
  const event9 = {
    ...el,
    dataset: {
      eventId: EVENT_NAME.CHECKOUT.EVENTS.RENEW_INFO,
      eventMessage: 'y'
    }
  };
  expect(callEvent(event9)).toBeUndefined();
});
