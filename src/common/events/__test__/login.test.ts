import {
  sendAgainEvent,
  sendCheckoutGuestEvent,
  sendForgotCredentialsEvent,
  sendForgotPasswordEvent,
  sendLoginFailedEvent,
  sendLoginSuccessEvent,
  sendLogOutEvent,
  sendOTPAgainEvent,
  sendPasswordNextEvent,
  sendRecoverPasswordSuccess,
  sendUserNameEvent,
  sendValidateOTPEvent,
  sendValidatePasswordEvent
} from '@events/login/index';

describe('login event test', () => {
  it('Test send checkout guest event', () => {
    expect(sendCheckoutGuestEvent()).toBeUndefined();
  });
  it('Test send forgot credential', () => {
    expect(sendForgotCredentialsEvent()).toBeUndefined();
  });
  it('Test send forgot password event', () => {
    expect(sendForgotPasswordEvent()).toBeUndefined();
  });
  it('Test send login failed event', () => {
    expect(sendLoginFailedEvent('')).toBeUndefined();
  });
  it('Test send login success ', () => {
    expect(sendLoginSuccessEvent('')).toBeUndefined();
  });
  it('Test send log out event', () => {
    expect(sendLogOutEvent()).toBeUndefined();
  });
  it('Test send otp again event', () => {
    expect(sendOTPAgainEvent()).toBeUndefined();
  });
  it('Test send otp again event', () => {
    expect(sendAgainEvent()).toBeUndefined();
  });
  it('Test send password next event', () => {
    expect(sendPasswordNextEvent()).toBeUndefined();
  });
  it('Test send recovery password success', () => {
    expect(sendRecoverPasswordSuccess('')).toBeUndefined();
  });
  it('Test send user name event', () => {
    expect(sendUserNameEvent()).toBeUndefined();
  });
  it('Test send validate otp event', () => {
    expect(sendValidateOTPEvent()).toBeUndefined();
  });
  it('Test send validate password event', () => {
    expect(sendValidatePasswordEvent()).toBeUndefined();
  });
});
