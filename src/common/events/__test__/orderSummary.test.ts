import {
  sendServiceInfoToggleEvent,
  sendTermsToggleEvent,
  sendThirdPartyToggleEvent
} from '@events/orderSummary/index';

describe('Order Summary events ', () => {
  test('test function sendServiceInfoToggleEvent ', () => {
    expect(sendServiceInfoToggleEvent(true)).toBeUndefined();
    expect(sendServiceInfoToggleEvent(false)).toBeUndefined();
  });

  test('test function sendTermsToggleEvent ', () => {
    expect(sendTermsToggleEvent(true)).toBeUndefined();
    expect(sendTermsToggleEvent(false)).toBeUndefined();
  });

  test('test function sendThirdPartyToggleEvent ', () => {
    expect(sendThirdPartyToggleEvent(true)).toBeUndefined();
    expect(sendThirdPartyToggleEvent(false)).toBeUndefined();
  });
});
