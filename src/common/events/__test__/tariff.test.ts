import {
  getPlanPrice,
  sendTariffDiscountDropDownEvent,
  sendTariffDropDownEvent,
  sendTariffDurationDropDownEvent,
  sendTariffImpressionEvent,
  sendTariffTabEvent
} from '@events/tariff/index';
import { tariffResponse } from '@common/mock-api/tariff/tariff.mock';

describe('Tariff event test', () => {
  it('Test send tariff discount event', () => {
    expect(sendTariffDiscountDropDownEvent('')).toBeUndefined();
  });
  it('Test send tariff duration event', () => {
    expect(sendTariffDurationDropDownEvent('')).toBeUndefined();
  });
  it('Test send tariff tab event', () => {
    expect(sendTariffTabEvent('')).toBeUndefined();
  });
  it('Test send tariff tab event', () => {
    expect(sendTariffDropDownEvent('')).toBeUndefined();
  });
  it('Test send tariff impression event', () => {
    expect(sendTariffImpressionEvent()).toBeUndefined();
  });
  it('Test send tariff price event', () => {
    expect(getPlanPrice(tariffResponse.tariffs[0])).toBeDefined();
  });
  it('Test send tariff price event with null value ', () => {
    // tslint:disable-next-line:no-any
    expect(getPlanPrice(null as any)).toBeDefined();
  });
});
