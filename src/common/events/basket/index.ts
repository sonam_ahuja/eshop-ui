import {
  ICharacteristic as IDetailCharacteristic,
  IProductDetailedData
} from '@productDetailed/store/types';
import { ITEM_STATUS } from '@src/common/store/enums';
import { TARIFF_BENEFIT } from '@tariff/store/enum';
import { sendEventGTM } from '@events/index';
import { IProducts } from '@src/common/events/types';
import { DROPDOWN_TYPE, PRODUCT_ATTRIBUTES } from '@productDetailed/store/enum';
import APP_CONSTANTS, {
  BASKET_STATE,
  ITEM_STATUS_EVENT_NAME,
  itemStatusEventNameType,
  NOT_APPLICABLE,
  PAGE_VIEW
} from '@events/constants/eventName';
import {
  IBasketItem,
  IBasketState,
  ICartSummary,
  ICharacteristic,
  IItemPrice
} from '@basket/store/types';
import {
  BASKET_GROUP,
  ITEM_AVAILABILITY_STATUS,
  ITEM_PRICE_TYPE
} from '@basket/store/enums';
import { IBenefits, IPlans } from '@tariff/store/types';
import { TARIFF_CATEGORY } from '@src/common/constants/appConstants';
import {
  getBasketState,
  getConfiguration,
  getProductDetailedState,
  getTariffState
} from '@store/common/index';
import { getDevicePrice, getTariffPrice } from '@events/common/index';
import { findOnKey } from '@utils/arrayHelper';
import { currencyDetails } from '@src/common/constants/currencyDetails';
import { EXTRA_DATA, FREE_APP } from '@tariff/constants';

export interface IProductDimension {
  dimension8: string;
  dimension9: string;
  dimension10: string;
  dimension11: string;
  dimension12: string;
  dimension14: string;
  dimension15: string;
  dimension16: string;
  dimension17: string;
  dimension18: string;
  dimension21: string;
  dimension22: string;
}

export const getProductFromId = (itemId: string, basketItems: IBasketItem[]) =>
  basketItems.find(
    (basketItem: IBasketItem) => basketItem.product.id === itemId
  );

export const getTotalPirce = (cartSummary: ICartSummary[]) => {
  let sum = 0;
  cartSummary.forEach((cart: ICartSummary) => {
    sum += cart.totalPrice;
  });

  return Math.round(sum);
};

export const getColor = (char: ICharacteristic[]) => {
  const emptyChar = char.find(el => el.name === '');

  return emptyChar ? emptyChar : '';
};

export const getTotalBasketItem = (basketItems: IBasketItem[]) => {
  let totalCount = 0;
  basketItems.forEach((item: IBasketItem) => {
    if (item.quantity !== null && item.quantity !== undefined) {
      totalCount += item.quantity;
    }
  });

  return totalCount;
};

export const getProducts = (basketItems: IBasketItem[], source?: string) => {
  const productItems: IProducts[] = [];
  basketItems.forEach((item: IBasketItem) => {
    const attributes: IProductDimension = getAttributesObject();
    if (
      item.group === BASKET_GROUP.DEVICE ||
      item.group === BASKET_GROUP.TARIFF
    ) {
      if (item.product.characteristic) {
        item.product.characteristic.forEach(
          (characteristic: ICharacteristic) => {
            // tslint:disable-next-line:switch-default
            switch (characteristic.name) {
              case PRODUCT_ATTRIBUTES.COLOUR:
                attributes.dimension8 = characteristic.value;
                break;
              case PRODUCT_ATTRIBUTES.STORAGE:
                attributes.dimension9 = characteristic.value;
                break;
              case DROPDOWN_TYPE.INSTALLMENTS:
                attributes.dimension10 = characteristic.value;
                break;
              case TARIFF_BENEFIT.DATA:
                attributes.dimension11 = characteristic.value;
                break;
              case TARIFF_BENEFIT.MINUTES:
                attributes.dimension12 = characteristic.value;
                break;
            }
          }
        );
      }

      if (source) {
        attributes.dimension17 = source;
      }

      attributes.dimension18 =
        item.status !== ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK
          ? ITEM_STATUS_EVENT_NAME.ACTIVE
          : ITEM_STATUS_EVENT_NAME.OUT_OF_STOCK;

      const upFrontPrice = item.itemPrice.find(
        (price: IItemPrice) =>
          price.priceType ===
          (item.group === BASKET_GROUP.DEVICE
            ? ITEM_PRICE_TYPE.UPFRONT
            : ITEM_PRICE_TYPE.RECURRING_FEE)
      );

      productItems.push({
        name: item.product.name,
        id: item.product.id,
        price: upFrontPrice ? upFrontPrice.totalPrice : undefined,
        brand: item.product.brandName,
        category:
          item.group === BASKET_GROUP.TARIFF
            ? TARIFF_CATEGORY
            : (item.product.categoryId as string),
        variant: item.product.variantName,
        quantity: item.quantity,
        ...attributes
      });
    }
  });

  return productItems;
};

export const sendOnBasketLoadEvent = () => {
  const basketState: IBasketState = getBasketState();
  const isItemPresent =
    basketState.basketItems && basketState.basketItems.length !== 0;
  const data = {
    Basket_State: isItemPresent
      ? BASKET_STATE.PRODUCTS_ADDED
      : BASKET_STATE.EMPTY,
    Product_Counts: isItemPresent
      ? getTotalBasketItem(basketState.basketItems)
      : 0,
    Basket_Amount:
      basketState.cartSummary && basketState.cartSummary.length
        ? getTotalPirce(basketState.cartSummary)
        : 0,
    event: APP_CONSTANTS.BASKET.EVENTS.LOAD_BASKET,
    ecommerce: {
      checkout: {
        actionField: { step: 1, option: APP_CONSTANTS.BASKET_VIEW },
        products: isItemPresent ? getProducts(basketState.basketItems) : []
      }
    }
  };
  sendEventGTM(data);
};

export const sendonBasketLoadTrigger = () => {
  const basketState: IBasketState = getBasketState();
  const isItemPresent =
    basketState.basketItems && basketState.basketItems.length !== 0;

  let data = {};

  isItemPresent
    ? (data = {
        Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
        Event_Action: APP_CONSTANTS.BASKET.ACTION.BASKET,
        Event_Label: getTotalBasketItem(basketState.basketItems),
        Event_Value:
          basketState.cartSummary && basketState.cartSummary.length
            ? getTotalPirce(basketState.cartSummary)
            : 0
      })
    : (data = {
        Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
        Event_Action: APP_CONSTANTS.BASKET.ACTION.BASKET,
        Event_Label: APP_CONSTANTS.BASKET.EVENTS.EMPTY
      });

  sendOutOfStockBasketEvent(basketState);

  sendEventGTM(data);
};

export const sendOutOfStockBasketEvent = (basketState: IBasketState) => {
  if (basketState.basketItems && basketState.basketItems.length) {
    basketState.basketItems.forEach((item: IBasketItem) => {
      if (item.status === ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK) {
        const data = {
          Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
          Event_Action: APP_CONSTANTS.BASKET.ACTION.OUT_OF_STOCK,
          Event_Label: item.product.name,
          Event_Value: getBasketAmount(item.itemPrice, item.group)
        };
        sendEventGTM(data);
      }
    });
  }
};

export const sendOnBasketToolTipEvent = (
  productName: string,
  price: IItemPrice[],
  group: string
) => {
  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.Tooltip,
    Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
    Event_Action: APP_CONSTANTS.BASKET.ACTION.TOOLTIP,
    Event_Label: productName,
    Event_Value: getBasketAmount(price, group)
  });
};

export const sendOnBasketSummaryToolTipEvent = (
  productName: string,
  price: number
) => {
  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.Tooltip,
    Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
    Event_Action: APP_CONSTANTS.BASKET.ACTION.SUMMARY_TOOLTIP,
    Event_Label: productName,
    Event_Value: price
  });
};

export const sendOnBasketTooltipParticulars = (
  productName: string,
  price: IItemPrice[],
  group: string
) => {
  sendEventGTM({
    Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
    Event_Action: APP_CONSTANTS.BASKET.ACTION.Basket_Product_Tooltip,
    Event_Label: productName,
    Event_Value: getBasketAmount(price, group)
  });
};

export const sendOnBasketRemoveParticulars = (
  productName: string,
  price: IItemPrice[],
  group: string
) => {
  sendEventGTM({
    Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
    Event_Action:
      APP_CONSTANTS.BASKET.ACTION.Product_Selected_For_Removal_From_Basket,
    Event_Label: productName,
    Event_Value: getBasketAmount(price, group)
  });
};

export const sendContinueShoppingEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.CONTINUE_SHOPPING
  });
};

export const sendClickOnRemoveButtonEvent = (
  productName: string,
  price: IItemPrice[],
  group: string
) => {
  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.REMOVE,
    Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
    Event_Action: APP_CONSTANTS.BASKET.ACTION.REMOVE_BUTTON_CLICK,
    Event_Label: productName,
    Event_Value: getBasketAmount(price, group)
  });
};

export const sendBasketDeleteItemEvent = (itemId: string) => {
  const basketState: IBasketState = getBasketState();

  const basketItem: IBasketItem | undefined = getProductFromId(
    itemId,
    basketState.basketItems
  );

  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.REMOVE_CART,
    ecommerce: {
      remove: {
        products: getProducts(basketItem ? [basketItem] : [])
      }
    }
  });
};

export const sendClearBasketEvent = () => {
  const basketState: IBasketState = getBasketState();
  sendEventGTM({
    event: APP_CONSTANTS.BASKET.EVENTS.CLEAR,
    Event_Category: APP_CONSTANTS.BASKET.CATEGORY.BASKET,
    Event_Action: APP_CONSTANTS.BASKET.ACTION.CLEAR_BUTTON_CLICK,
    Event_Label: basketState.basketItems
      ? getTotalBasketItem(basketState.basketItems)
      : 0,
    Event_Value:
      basketState.cartSummary && basketState.cartSummary.length
        ? getTotalPirce(basketState.cartSummary)
        : 0
  });
};

export const sendAddToBasketEvent = (
  product: IProductDetailedData,
  selectedTariffId: string | undefined,
  considerStockForPreOrder: boolean
) => {
  if (product.variants && product.variants[0]) {
    const productDetailedState = getProductDetailedState();
    const category = productDetailedState.categoryId;
    const price = getDevicePrice(product.variants[0].prices);
    let status = ITEM_STATUS_EVENT_NAME.ACTIVE;

    if (
      product.variants[0].unavailabilityReasonCodes &&
      product.variants[0].unavailabilityReasonCodes.length
    ) {
      if (
        product.variants[0].unavailabilityReasonCodes.includes(
          ITEM_STATUS.PRE_ORDER
        )
      ) {
        status =
          product.variants[0].unavailabilityReasonCodes.includes(
            ITEM_STATUS.OUT_OF_STOCK
          ) && considerStockForPreOrder
            ? ITEM_STATUS_EVENT_NAME.OUT_OF_STOCK
            : ITEM_STATUS_EVENT_NAME.PRE_ORDER;
      } else if (
        product.variants[0].unavailabilityReasonCodes.includes(
          ITEM_STATUS.OUT_OF_STOCK
        )
      ) {
        status = ITEM_STATUS_EVENT_NAME.OUT_OF_STOCK;
      }
    }
    const deviceAttributes = getAttributes(
      product.variants[0].characteristics
        ? product.variants[0].characteristics
        : [],
      PAGE_VIEW.DEVICE_DETAILED,
      status
    );
    const configuration = getConfiguration();
    const currencyLocale =
      configuration.cms_configuration.global.currency.locale;

    const data = {
      event: APP_CONSTANTS.BASKET.EVENTS.ADD_TO_CART,
      ecommerce: {
        currencyCode:
          currencyLocale && currencyDetails[currencyLocale]
            ? currencyDetails[currencyLocale].symbol
            : null,
        add: {
          products: [
            {
              name: product.productName,
              id: product.variants[0].id,
              price: price ? price.discountedValue : null,
              brand: product.variants[0].brand,
              category,
              variant: product.variants[0].name,
              quantity: 1,
              ...deviceAttributes
            }
          ]
        }
      }
    };
    if (selectedTariffId) {
      const selectedTariff = product.tariffs.find(tariff => tariff.isSelected);
      if (selectedTariff) {
        const selectedBennefits = getBenefits(selectedTariff.id);
        const extraData =
          selectedBennefits && getAddonValue(selectedBennefits, EXTRA_DATA);
        const freeApp =
          selectedBennefits && getAddonValue(selectedBennefits, FREE_APP);

        const tariffAttributes = getAttributes(
          selectedTariff.characteristics ? selectedTariff.characteristics : [],
          PAGE_VIEW.DEVICE_DETAILED,
          ITEM_STATUS_EVENT_NAME.ACTIVE
        );
        if (extraData) {
          tariffAttributes.dimension21 = extraData;
        }
        if (freeApp) {
          tariffAttributes.dimension22 = freeApp;
        }
        const tariffPrice = selectedTariff.totalPrices
          ? getTariffPrice(selectedTariff.totalPrices)
          : null;

        data.ecommerce.add.products.push({
          name: selectedTariff.name,
          id: selectedTariff.id,
          price: tariffPrice ? tariffPrice.discountedValue : null,
          brand: NOT_APPLICABLE,
          category: TARIFF_CATEGORY,
          variant: selectedTariff.name,
          quantity: 1,
          ...tariffAttributes
        });
      }
    }

    sendEventGTM(data);
  }
};

export const sendTariffAddToBasketEvent = (
  tariffId: string,
  plans: IPlans[]
) => {
  const tariff = plans.find((plan: IPlans) => {
    return plan.id === tariffId;
  });

  if (tariff) {
    const price = tariff.prices && tariff.prices[0];
    const tariffAttributes = getTariffAttributes(
      tariff.benefits,
      PAGE_VIEW.TARIFF
    );

    const configuration = getConfiguration();
    const currencyLocale =
      configuration.cms_configuration.global.currency.locale;

    const data = {
      event: APP_CONSTANTS.BASKET.EVENTS.ADD_TO_CART,
      ecommerce: {
        currencyCode:
          currencyLocale && currencyDetails[currencyLocale]
            ? currencyDetails[currencyLocale].symbol
            : null,
        add: {
          products: [
            {
              name: tariff.name,
              id: tariff.id,
              price: price.discountedValue,
              brand: NOT_APPLICABLE,
              category: TARIFF_CATEGORY,
              variant: tariff.name,
              quantity: 1,
              ...tariffAttributes
            }
          ]
        }
      }
    };
    sendEventGTM(data);
  }
};

export const sendPopupCloseEvent = () => {
  sendEventGTM({ event: APP_CONSTANTS.BASKET.EVENTS.POPUP_CLOSE });
};

export const getAttributes = (
  characteristics: IDetailCharacteristic[],
  source: string,
  status: itemStatusEventNameType
) => {
  const productAttributes: IProductDimension = getAttributesObject();
  if (characteristics) {
    characteristics.forEach((characteristic: IDetailCharacteristic) => {
      // tslint:disable-next-line:switch-default
      switch (characteristic.name) {
        case PRODUCT_ATTRIBUTES.COLOUR:
          productAttributes.dimension8 =
            characteristic.values &&
            characteristic.values.length &&
            characteristic.values[0].label
              ? characteristic.values[0].label
              : NOT_APPLICABLE;
          break;
        case PRODUCT_ATTRIBUTES.STORAGE:
          productAttributes.dimension9 =
            characteristic.values &&
            characteristic.values.length &&
            characteristic.values[0].label
              ? characteristic.values[0].label
              : NOT_APPLICABLE;
          break;
        case DROPDOWN_TYPE.INSTALLMENTS:
          productAttributes.dimension10 =
            characteristic.values &&
            characteristic.values.length &&
            characteristic.values[0].label
              ? characteristic.values[0].label
              : NOT_APPLICABLE;
          break;
        case TARIFF_BENEFIT.DATA:
          productAttributes.dimension11 =
            characteristic.values &&
            characteristic.values.length &&
            characteristic.values[0].label
              ? characteristic.values[0].label
              : NOT_APPLICABLE;
          break;
        case TARIFF_BENEFIT.MINUTES:
          productAttributes.dimension12 =
            characteristic.values &&
            characteristic.values.length &&
            characteristic.values[0].label
              ? characteristic.values[0].label
              : NOT_APPLICABLE;
          break;
      }
    });
  }

  if (source) {
    productAttributes.dimension17 = source;
  }
  productAttributes.dimension18 = status;

  return productAttributes;
};

export const getTariffAttributes = (benefits: IBenefits[], source: string) => {
  const tariffAttributes: IProductDimension = getAttributesObject();
  benefits.forEach((benefit: IBenefits) => {
    if (!benefit.isSelectable) {
      if (
        benefit.name === TARIFF_BENEFIT.DATA &&
        benefit.values &&
        benefit.values[0] &&
        benefit.values[0].label
      ) {
        tariffAttributes.dimension11 = benefit.values[0].label;
      } else if (
        benefit.name === TARIFF_BENEFIT.MINUTES &&
        benefit.values &&
        benefit.values[0] &&
        benefit.values[0].label
      ) {
        tariffAttributes.dimension12 = benefit.values[0].label;
      }
    }
  });

  tariffAttributes.dimension17 = source;
  tariffAttributes.dimension18 = ITEM_STATUS_EVENT_NAME.ACTIVE;

  return tariffAttributes;
};

export const getBenefits = (tariifId: string) => {
  const tariffState = getTariffState();
  const tariff = tariffState.plans.filter(({ id }) => id === tariifId)[0];

  return tariff && tariff.benefits;
};

export const getAddonValue = (benefits: IBenefits[], addonName: string) => {
  const addOn = benefits.filter(benefit => benefit.name === addonName)[0];
  const addOnValue = addOn && addOn.values.filter(value => value.isSelected)[0];
  if (addOnValue) {
    return addOnValue.label;
  }

  return null;
};
export const getAttributesObject = () => {
  return {
    dimension8: NOT_APPLICABLE,
    dimension9: NOT_APPLICABLE,
    dimension10: NOT_APPLICABLE,
    dimension11: NOT_APPLICABLE,
    dimension12: NOT_APPLICABLE,
    dimension14: NOT_APPLICABLE, // product size
    dimension15: NOT_APPLICABLE,
    dimension16: NOT_APPLICABLE,
    dimension17: NOT_APPLICABLE,
    dimension18: NOT_APPLICABLE,
    dimension21: NOT_APPLICABLE,
    dimension22: NOT_APPLICABLE
  };
};

export const getBasketAmount = (price: IItemPrice[], group: string) => {
  const monthlyPrice = findOnKey(
    price,
    'priceType',
    ITEM_PRICE_TYPE.RECURRING_FEE
  ) as IItemPrice;

  const upfrontPrice = findOnKey(
    price,
    'priceType',
    ITEM_PRICE_TYPE.UPFRONT
  ) as IItemPrice;

  const basePrice = findOnKey(
    price,
    'priceType',
    ITEM_PRICE_TYPE.BASEPRICE
  ) as IItemPrice;

  return group === BASKET_GROUP.TARIFF
    ? monthlyPrice
      ? // tslint:disable-next-line:radix
        Math.round(monthlyPrice.totalPrice)
      : 0
    : upfrontPrice
    ? Math.round(upfrontPrice.totalPrice)
    : basePrice
    ? Math.round(basePrice.totalPrice)
    : 0;
  // tslint:disable-next-line:max-file-line-count
};
