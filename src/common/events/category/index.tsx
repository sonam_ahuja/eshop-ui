import APP_CONSTANTS from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';

export const sendPromotionViewEvent = (
  promotionName1: string | null,
  promotionName2: string | null
) => {
  const promotions = [];
  if (promotionName1) {
    promotions.push({
      name: promotionName1,
      creative: 'banner1',
      position: 'slot1'
    });
  }

  if (promotionName2) {
    promotions.push({
      name: promotionName2,
      creative: 'skyscraper1',
      position: promotionName1 ? 'slot2' : 'slot1'
    });
  }

  const data = {
    event: APP_CONSTANTS.CATEGORY.EVENTS.PROMOTION_VIEW,
    ecommerce: {
      promoView: {
        promotions: [...promotions]
      }
    }
  };

  if (promotions.length > 0) {
    sendEventGTM(data);
  }
};

export const sendPromotionClickEvent = (
  promotionName: string,
  slot: string
) => {
  const data = {
    event: APP_CONSTANTS.CATEGORY.EVENTS.PROMOTION_CLICK,
    ecommerce: {
      promoClick: {
        promotions: [
          {
            name: promotionName,
            creative: slot === 'slot1' ? 'banner1' : 'skyscraper1',
            position: slot
          }
        ]
      }
    }
  };
  sendEventGTM(data);
};
