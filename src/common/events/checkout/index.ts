import APP_CONSTANTS, {
  CHECKOUT_VIEW,
  DELIVERY_METHOD,
  NOT_APPLICABLE,
  OFF,
  ON,
  TOGGLE_NAME
} from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';
import { getBasketState, getCheckoutState } from '@store/common/index';
import { getProducts, getTotalPirce } from '@events/basket/index';
import { IOrderTrackingResponse } from '@checkout/store/orderReview/types';
import {
  PAYMENT_TYPE,
  SHIPPING_TYPE
} from '@src/common/routes/checkout/store/enums';
import {
  BASKET_GROUP,
  ITEM_PRICE_TYPE
} from '@src/common/routes/basket/store/enums';
import { getEnglishText } from '@events/listener';

export const sendRequestSupportEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.SUPPORT_CALL
  });
};

export const sendRenewInfoEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.RENEW_INFO
  });
};

export const sendSelectDiffDeviceEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.SELECT_DIFF_DEVICE
  });
};

export const sendPayDeviceFullEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.PAY_DEVICE_FULL
  });
};

export const sendSelectThisStoreEvent = (storeName: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.SELECT_THIS_STORE,
    Event_Category: APP_CONSTANTS.CHECKOUT.CATEGORY.STORE,
    Event_Action: storeName
  });
};

export const sendOtherSelectStoreEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.CHOOSE_OTHER_STORE
  });
};

export const sendAddToCalEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.ADD_TO_CALENDAR
  });
};

export const sendTrackOrderEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.TRACK_ORDER
  });
};

export const sendPortMyNumberEvent = (
  portType: string,
  connectionType: string
) => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.PORT_MY_NUMBER,
    Port_Type: portType,
    Connection_Type: connectionType
  });
};

export const cancelPortSuccess = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.CANCEL_PORT
  });
};

export const sendSelectStoreEvent = (storeName: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.SELECT_STORE,
    Event_Category: APP_CONSTANTS.CHECKOUT.CATEGORY.STORE,
    Event_Action: storeName
  });
};

export const sendSelectMachineEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.SELECT_MACHINE
  });
};

export const sendSelectPointEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.SELECT_POINT
  });
};

export const sendChooseOtherMethodEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.CHOOSE_OTHER_METHOD
  });
};

export const sendEditDetailsEvent = (detailTypePath: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.EDIT_DETAILS,
    Detail_Type: getEnglishText(detailTypePath)
  });
};

export const sendContentFilledOutEvent = (formName: string, label: string) => {
  const data = {
    event: APP_CONSTANTS.CHECKOUT.EVENTS.CONTENT_FILL_OUT,
    Event_Category: APP_CONSTANTS.CHECKOUT.CATEGORY.CONTENT_FILLED,
    Event_Actions: formName,
    Event_Label: label
  };

  sendEventGTM(data);
};

export const sendModifyPaymentOptionEvent = (paymentOptions: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.MODIFY_PAYMENT_OPTION,
    Payment_Mode: paymentOptions
  });
};

export const sendRescheduleOrderEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.CHECKOUT.EVENTS.RESCHEDULE_ORDER
  });
};

const getToggleNameAndSwitch = (stepNumber: number) => {
  const checkoutState = getCheckoutState();
  if (stepNumber === CHECKOUT_VIEW.PERSONAL_INFO.STEP) {
    return {
      Toggle_Switch: checkoutState.personalInfo.isSmsEnabled ? ON : OFF,
      Toggle_Name: TOGGLE_NAME.TOGGLE_SMS
    };
  } else if (stepNumber === CHECKOUT_VIEW.SHIPPING.STEP) {
    return {
      Toggle_Switch: checkoutState.shippingInfo.sameAsPersonalInfo ? ON : OFF,
      Toggle_Name: TOGGLE_NAME.TOGGLE_ADDRESS
    };
  } else {
    return {
      Toggle_Switch: NOT_APPLICABLE,
      Toggle_Name: NOT_APPLICABLE
    };
  }
};
export const sendProceedEvent = (stepNumber: number, viewName: string) => {
  const basketState = getBasketState();
  const isItemPresent =
    basketState.basketItems && basketState.basketItems.length !== 0;

  const dataToSend = {
    ...getToggleNameAndSwitch(stepNumber),
    event: APP_CONSTANTS.CHECKOUT.EVENTS.CHECKOUT,
    ecommerce: {
      checkout: {
        actionField: { step: stepNumber, option: viewName },
        products: isItemPresent ? getProducts(basketState.basketItems) : []
      }
    }
  };

  sendEventGTM(dataToSend);
};

export const getShippingType = (deliveryType: string): string => {
  if (deliveryType === SHIPPING_TYPE.SAME_DAY) {
    return 'Same Day Delivery ';
  } else if (deliveryType === SHIPPING_TYPE.STANDARD) {
    return 'Standard Delivery';
  } else if (deliveryType === SHIPPING_TYPE.PICK_UP_STORE) {
    return 'Pick up at store';
  } else {
    return deliveryType;
  }
};

export const getBuyingOption = (items: IOrderTrackingResponse['cartItems']) => {
  let tariffPresent = false;
  let devicePresent = false;
  if (items && Array.isArray(items)) {
    items.forEach((item: IOrderTrackingResponse['cartItems'][0]) => {
      if (item.group === BASKET_GROUP.TARIFF) {
        tariffPresent = true;
      } else if (item.group === BASKET_GROUP.DEVICE) {
        devicePresent = true;
      }
    });
  }
  if (tariffPresent && devicePresent) {
    return 'bundle';
  } else if (tariffPresent) {
    return 'plan only';
  } else if (devicePresent) {
    return 'device only';
  }

  return '';
};

// tslint:disable-next-line:no-any
export const sendOrderPlacedEvent = (response: any) => {
  if (
    response &&
    response.shippingDetails &&
    response.shippingDetails.deliveryType &&
    response.cartItems &&
    Array.isArray(response.cartItems)
  ) {
    const deliveryOption = getShippingType(
      response.shippingDetails.deliveryType
    );
    const items: IOrderTrackingResponse['cartItems'] = response.cartItems.filter(
      // tslint:disable-next-line:no-any
      (item: any) => item.group !== DELIVERY_METHOD.DELIVERY_METHOD
    );
    const buyingOption = getBuyingOption(items);
    const checkoutState = getCheckoutState();
    const basketState = getBasketState();
    let paymentType = '';
    const isItemPresent =
      basketState.basketItems && basketState.basketItems.length !== 0;
    const products = isItemPresent ? getProducts(basketState.basketItems) : [];
    let shippingFee = 0;
    const shippingItem = response.cartItems.filter(
      // tslint:disable-next-line:no-any
      (item: any) => item.group === DELIVERY_METHOD.DELIVERY_METHOD
    );

    if (shippingItem && Array.isArray(shippingItem) && shippingItem.length) {
      const shippingPrice = shippingItem[0].totalPrice.filter(
        // tslint:disable-next-line:no-any
        (price: any) => price.priceType === ITEM_PRICE_TYPE.UPFRONT
      );

      shippingFee = shippingPrice[0].totalPrice;
    }

    if (
      checkoutState &&
      checkoutState.payment &&
      checkoutState.payment.upFront &&
      checkoutState.payment.upFront.paymentTypeSelected
    ) {
      paymentType =
        checkoutState.payment.upFront.paymentTypeSelected ===
        PAYMENT_TYPE.CREDIT_DEBIT_CARD
          ? 'card'
          : checkoutState.payment.upFront.paymentTypeSelected ===
            PAYMENT_TYPE.PAY_BY_LINK
          ? 'payByLink'
          : 'payOnDelivery';
    }

    const dataToSend = {
      Buying_Option: buyingOption,
      Payment_Mode: paymentType,
      Delivery_Option: deliveryOption,
      ecommerce: {
        purchase: {
          actionField: {
            id: response.orderId, // Transaction ID. Required for purchases and refunds.
            affiliation: 'Online Store',
            revenue:
              (basketState && Array.isArray(basketState.cartSummary)
                ? getTotalPirce(basketState.cartSummary)
                : 0) + shippingFee, // Total transaction value (incl. tax and shipping)
            tax: NOT_APPLICABLE,
            shipping: shippingFee,
            coupon: NOT_APPLICABLE
          },
          products: [...products]
        }
      }
    };
    sendEventGTM(dataToSend);
  }
};

export const sendTransactionFailedEvent = (
  cardType: string,
  transactionId: number,
  transactionAmount: number,
  productQuantity: number
) => {
  const dataToSend = {
    event: 'transactionFailed',
    Payment_Mode: cardType,
    Transaction_Id: transactionId,
    Transaction_Amount: transactionAmount,
    Product_Quantity: productQuantity
  };

  sendEventGTM(dataToSend);
  // tslint:disable-next-line:max-file-line-count
};
