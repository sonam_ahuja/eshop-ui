import APP_CONSTANTS, { ERROR_REASON } from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';
import { IPrices } from '@src/common/routes/tariff/store/types';
import { PRICES } from '@productList/store/enum';

const loginType = {
  type: ''
};
export const getLoginType = () => loginType.type;

export const setLoginType = (type: string) => {
  loginType.type = type;
};

export const sendHeaderClickEvent = (headingName: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.HEADER.EVENTS.HEADER_CLICKs,
    Event_Category: APP_CONSTANTS.HEADER.CATEGORY.HEADER_CLICKs,
    Event_Action: headingName
  });
};

export const sendSearchClickEvent = (
  searchTerm: string,
  searchMode: string
) => {
  sendEventGTM({
    event: APP_CONSTANTS.COMMON.EVENTS.SEARCH_CLICK,
    Search_Term: searchTerm,
    Search_Mode: searchMode
  });
};

export const sendFooterClickEvent = (footerCTA: string, pageName: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.COMMON.EVENTS.FOOTER_CLICK,
    Event_Category: APP_CONSTANTS.COMMON.CATEGORY.FOOTER_CLICK,
    Event_Action: footerCTA,
    Event_Label: pageName
  });
};

export const sendDropdownClickEvent = (selectedName: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.COMMON.EVENTS.DROP_DOWN,
    Event_Category: APP_CONSTANTS.COMMON.CATEGORY.DROP_DOWN_CLICK,
    Event_Action: selectedName
  });
};

export const getDevicePrice = (variantPrices: IPrices[]) => {
  let price = variantPrices.find(
    (prices: IPrices) => prices.priceType === PRICES.UPFRONT
  );

  if (!price) {
    price = variantPrices.find(
      (prices: IPrices) => prices.priceType === PRICES.BASE_PRICE
    );
  }

  return price;
};

export const getTariffPrice = (variantPrices: IPrices[]) => {
  return variantPrices.find(
    (prices: IPrices) => prices.priceType === PRICES.MONTHLY
  );
};

export const sendErrorEvent = (
  errorCode: number | string | undefined,
  errorDescription: string
) => {
  sendEventGTM({
    event: APP_CONSTANTS.COMMON.EVENTS.ERROR_EVENT,
    Error_Type: 'server error',
    Error_Description:
      errorCode === 408 || errorCode === 502
        ? ERROR_REASON.REQUEST_TIME_OUT
        : errorDescription || ERROR_REASON.INTERNAL_SERVER_ERROR
  });
};

export const sendPopupCloseEvent = () => {
  sendEventGTM({ event: APP_CONSTANTS.COMMON.EVENTS.POPUP_CLOSE });
};
