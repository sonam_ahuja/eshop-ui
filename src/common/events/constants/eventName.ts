export default {
  BASKET_VIEW: 'basket view',
  BASKET: {
    EVENTS: {
      ADD_TO_CART: 'addToCart',
      LOAD_BASKET: 'checkout',
      Tooltip: 'toolTipButton',
      CONTINUE_SHOPPING: 'continueShopping',
      REMOVE: 'removeIconClick',
      REMOVE_CART: 'removeFromCart',
      CLEAR: 'clearBasketClick',
      EMPTY: 'Empty Basket',
      PLACE_ORDER: 'place order',
      PROCEED_TO_CHECKOUT: 'proceed to checkout',
      REMOVE_ITEM: 'remove item',
      ADD_ITEM: 'add item',
      STEPPER: 'basket stepper',
      CHANGE_VARIANT: 'change variant',
      SELECT_DIFF_DEVICE: 'select diff device',
      VIEW_BASKET: 'view cart',
      POPUP_CLOSE: 'popupClose'
    },
    CATEGORY: {
      BASKET: 'Checkout'
    },
    ACTION: {
      TOOLTIP: 'Basket Product Tool Tip',
      REMOVE_BUTTON_CLICK: 'product selected for removal from cart',
      CLEAR_BUTTON_CLICK: 'product selected for removal from basket all',
      BASKET: 'Basket',
      OUT_OF_STOCK: 'OUT_OF_STOCK',
      Basket_Product_Tooltip: 'Basket_Product_Tooltip',
      Product_Selected_For_Removal_From_Basket:
        'Product_Selected_For_Removal_From_Basket',
      SUMMARY_TOOLTIP: 'Basket Summary Tool Tip'
    }
  },
  AUTHENTICATE: {
    CATEGORY: {
      LOGIN: 'Login',
      LOG_OUT: 'Logout',
      FORGOT_CRED: 'Forgot Credentials',
      REGISTRATION: 'Registration'
    },
    EVENTS: {
      LOGIN_USER_NAME_ENTER: 'userNameNext',
      LOGIN_PASSWORD_VALIDATE: 'validatePassword',
      PASSWORD_NEXT: 'passwordNext',
      VALIDATE_OTP: 'validateOtp',
      FORGOT_CREDENTIALS: 'forgotCredentials',
      FORGOT_PASSWORD: 'forgotPassword',
      RECOVER_SUCCESS: 'recoverSuccessful',
      CHECKOUT_GUEST: 'checkoutAsGuest',
      SEND_OTP_AGAIN: 'resendOtp',
      SEND_AGAIN: 'sendAgain',
      LOGIN_SUCCESS: 'loginSuccess',
      LOGIN_FAILED: 'loginFailed',
      REGISTRATION_SUCCESS: 'registrationSuccess',
      REGISTRATION_FAILED: 'registrationFailed',
      LOG_OUT: 'logOut',
      NEXT: 'next',
      VERIFICATION_NEXT: 'verfication next',
      VALIDATE_PASSWORD: 'validate password',
      CONTIUNE: 'continue',
      SIGN_OUT: 'sign out',
      HOME_PAGE: 'home page',
      SIGN_AGAIN: 'sign again',
      RECOVER_CREDS_NEXT: 'recover creds next',
      RECOVER: 'RECOVER'
    },
    ACTION: {
      SUCCESS: 'Success',
      FAILED: 'Failed',
      RECOVER_SUCCESS: 'Recovery Success'
    }
  },
  TARIFF: {
    EVENTS: {
      TAB_CLICK: 'tabClicks',
      DROP_DOWN_CLICK: 'dropdownClicks',
      YOUNG_TARIFF_VALIDATE: 'yound tariff validate',
      ADD_TO_PHONE: 'add to phone',
      BUY_PLAN: 'buy plan',
      CONFIRM_PLAN: 'confirm plan',
      VIEW_ACTION_BUTTON: 'view action button',
      TERMS_CONDITION: 'terms condition',
      ADD_ON_CLICK: 'addOn click',
      ABOUT_DISCOUNT: 'about discount'
    },
    CATEGORY: {
      TAB_CATEGEGORY: 'Tab Clicks',
      DROP_DOWN_CLICK: 'Dropdown Clicks'
    }
  },
  CHECKOUT: {
    EVENTS: {
      SUPPORT_CALL: 'requestSupportCall',
      CHECKOUT: 'checkout',
      RENEW_INFO: 'renewYourInformation',
      SELECT_DIFF_DEVICE: 'selectDifferentDevice',
      PAY_DEVICE_FULL: 'payDeviceInFull',
      SELECT_THIS_STORE: 'selectThisStore',
      CHOOSE_OTHER_STORE: 'chooseOtherStore',
      ADD_TO_CALENDAR: 'addToCalendar',
      TRACK_ORDER: 'trackYourOrder',
      PORT_MY_NUMBER: 'portMyNumber',
      CANCEL_PORT: 'cancelPortSuccess',
      SELECT_MACHINE: 'selectMachine',
      SELECT_POINT: 'selectPoint',
      CHOOSE_OTHER_METHOD: 'chooseOtherMethod',
      EDIT_DETAILS: 'editDetails',
      SELECT_STORE: 'selectStore',
      RESCHEDULE_ORDER: 'rescheduleOrder',
      REVIEW_ORDER: 'review order',
      CREDIT_SUCCESS: 'credit success',
      PAY_FULL_PRICE: 'pay full price',
      REQUEST_SUPPORT_CALL: 'request support call',
      MNP_VALIDATE_NUMBER: 'mnp validate number',
      PORT_NUMBER: 'port number',
      CONFIRM_NUMBER: 'confirm number',
      PROCEED: 'proceed',
      PAYMENT_METHOD: 'payment method',
      PROCEED_TO_SHIPPING: 'proceed to shipping',
      PROCEED_TO_CREDIT: 'proceed to credit',
      PROCEED_TO_PAYMENT: 'procced to payment',
      CHOOSE_OTHER: 'choose other',
      CHOOSE_SHIPPING: 'choose shipping',
      TRY_AGAIN: 'try again',
      TERMS_CONDITION: 'terms and condition',
      NUMBER_PORTING: 'number porting',
      MNP_PORT_SEND_AGAIN: 'mnp port again',
      MNS_OTHER_NUMBER: 'MNS_OTHER_NUMBER',
      PORT_ON: 's',
      Port_Type: '',
      Connection_Type: '',
      CONTENT_FILL_OUT: 'contentFillOut',
      SMS_NOTIFICATION: 'smsNotification',
      ADDRESS_TOGGLE: 'addressToggle',
      MODIFY_PAYMENT_OPTION: 'modifyPaymentOptions',
      AGREEMENT: 'agreement',
      E_BILL: 'ebill',
      PAPER_BILL: 'paperBill'
    },
    CATEGORY: {
      STORE: 'Store',
      CONTENT_FILLED: 'Content Filled'
    }
  },
  ERROR: {
    EVENTS: {
      ERROR_EVENTS: 'errorEvent'
    },
    CATEGORY: {}
  },
  COMMON: {
    EVENTS: {
      CTA_CLICKS: 'ctaClicks',
      HEADER_CLICk: 'headerClicks',
      SEARCH_CLICK: 'searchClick',
      FOOTER_CLICK: 'footerClicks',
      ERROR_EVENT: 'errorEvent',
      DROP_DOWN: 'dropdownClicks',
      POPUP_CLOSE: 'popupClose',
      BREAD_CRUMB_CLICK: 'breadCrumbClicks'
    },
    CATEGORY: {
      CTA_CLICKS: 'ctaClicks',
      HEADER_CLICK: 'Header Clicks',
      FOOTER_CLICK: 'Footer Clicks',
      DROP_DOWN_CLICK: 'Dropdown Clicks'
    }
  },
  DEVICE_CATALOG: {
    EVENTS: {
      VALIDATE_CREDENTIAL: 'validateCredentials',
      FILTER_APPLIED: 'filterApplied',
      MOST_POPULAR_CLICKS: 'mostPopularClicks',
      PRODUCT_CLICKS: 'productClick',
      PRODUCT_IMPRESSION: 'productImpression',
      OUT_OF_STOCK: 'out of stock',
      REDIRECT_TO_DETAIL: 'redired to detail',
      VIEW_RESULT: 'view result',
      OUT_OF_STOCK_ACTION: 'OUT_OF_STOCK_ACTION',
      CLEAR_ALL: 'CLEAR_ALL'
    },
    CATGORY: {}
  },
  DEVICE_DETAIL: {
    EVENTS: {
      PRODUCT_DETAIL_VIEW: 'productDetailView',
      NOTIFY_ME: 'notify me',
      ADD_TO_BASKET: 'add to basket',
      COLOR_SELECTION: 'colorSelection',
      STORAGE_SELECTION: 'storageSelection',
      DEVICE_DEATIL: 'DEVICE DETAIL'
    },
    ACTION: {
      LIST: 'list',
      DETAIL: 'detail',
      DEVICE_DEATIL_TOOLTIP: 'Device detail Tooltip'
    }
  },
  CATEGORY: {
    EVENTS: {
      PROMOTION_VIEW: 'promotionView',
      PROMOTION_CLICK: 'promotionClick'
    }
  },
  FOOTER: {
    CATEGORY: {},
    EVENTS: {
      FOOTER_CLICKS: 'Footer Clicks'
    }
  },
  HEADER: {
    CATEGORY: {
      HEADER_CLICKs: 'Header Clicks'
    },
    EVENTS: {
      HEADER_CLICKs: 'headerClicks',
      LOGIN_SIGIN: 'login sign'
    }
  },
  ORDER_SUMMARY: {
    EVENTS: {
      SERVICE_INFO_TOGGLE: 'serviceInfoToggle',
      THIRD_PARTY_TOGGLE: 'thirdPartyToggle',
      TERMS_TOGGLE: 'termsToggle'
    }
  }
};

export const ON = 'on';
export const OFF = 'off';

export enum BASKET_STATE {
  EMPTY = 'empty',
  PRODUCTS_ADDED = 'product is added it'
}

export enum PAGE_VIEW {
  BASKET = 'BASKET',
  LOGIN = 'LOGIN',
  TARIFF = 'Tariff',
  DEVICE_LIST = 'deviceListing',
  HOME = 'HOME',
  DEVICE_DETAILED = 'deviceDetailed',
  NOT_FOUND = 'NOT FOUND',
  CHECKOUT_PERSONAL_INFO = 'checkout/PersonalInfo',
  CHECKOUT_SHIPPING_INFO = 'checkout/ShippingInfo',
  CHECKOUT_PAYMENT_INFO = 'checkout/PaymentInfo',
  CHECKOUT_BILLING_INFO = 'checkout/BillingInfo',
  CHECKOUT_ORDER_REVIEW_INFO = 'checkout/OrderReviewInfo',
  CHECKOUT_ORDER_CONFIRMATION = 'checkout/OrderConfirmation',
  CHECKOUT_MNP = 'checkout/mnp',
  CATEGORY_LANDING_PAGE = 'categoryLandingPage',
  SEARCH = 'search',
  ENTER_LOGIN_CREDENTIALS = 'Signin/EnterLoginCredentials',
  ENTER_OTP = 'Signin/EnterOtp',
  EMAIL_CONFIRMATION = 'Signin/EmailConfirmation',
  CONFIRM_YOUR_IDENTITY = 'Signin/ConfirmYourIdentity',
  LOG_OUT = 'Logout',
  ENTER_PASSWORD = 'Signin/EnterPassword',
  ENTER_RECOVERY_EMAIL = 'Signin/EnterRecoveryEmail',
  RECOVER_PASSWORD = 'Signin/RecoverPassword',
  CHECKOUT_GUEST = 'Signin/CheckoutAsGuest'
}

export const CHECKOUT_VIEW = {
  PERSONAL_INFO: {
    STEP: 2,
    VIEW: 'personal information'
  },
  SHIPPING: {
    STEP: 3,
    VIEW: 'shipping'
  },
  PAYMENT: {
    STEP: 4,
    VIEW: ' payment'
  },
  BILLING: {
    STEP: 5,
    VIEW: 'billing'
  }
};

export const FORM_NAME = {
  PERSONAL_INFO: 'Personal Info',
  SHIPPING_INFO: 'Shipping Info',
  PAYMENT_INFO: 'Payment Info',
  BILLING_INFO: 'Billing Info'
};
export const TOGGLE_NAME = {
  TOGGLE_ADDRESS: 'address toggle',
  TOGGLE_SMS: 'sms toggle'
};

export enum SEARCH_MODE {
  TYPED_MODE = 'type search',
  VOICE_MODE = 'voice search'
}

export enum ITEM_STATUS_EVENT_NAME {
  OUT_OF_STOCK = 'out of stock',
  ACTIVE = 'in stock',
  PRE_ORDER = 'preorder'
}

export type itemStatusEventNameType =
  | ITEM_STATUS_EVENT_NAME.ACTIVE
  | ITEM_STATUS_EVENT_NAME.OUT_OF_STOCK
  | ITEM_STATUS_EVENT_NAME.PRE_ORDER;

export enum ERROR_REASON {
  BAD_GATEWAY = 'bad gateways',
  REQUEST_TIME_OUT = 'request time out',
  PAGE_TIME_OUT = 'page time out',
  INTERNAL_SERVER_ERROR = 'internal server error'
}

export const NOT_APPLICABLE = 'NA';

export enum BUYING_OPTION {
  DEVICE_ONLY = 'device only',
  TARIFF = 'plan only',
  BUNDLE = 'bundle'
}

export enum PAYMENT_METHOD {
  DEBIT_CARD = 'Debit card',
  CREDIT_CARD = 'credit card',
  WALLET = 'wallet'
}

export enum DELIVERY_METHOD {
  STANDARD_DELIVERy = 'Standard Delivery',
  SAME_DELIVERy = 'Same Day Delivery',
  PICK_UP = 'Pick up at store'
}

export enum DEVICE_LIST {
  BEST_DEVICE = 'best device section',
  POPULAR_DEVICE = 'popular device section',
  DEVICE_LIST_SECTION = 'device list section',
  SEARCH_RESULT = 'search results'
}

export enum DELIVERY_METHOD {
  DELIVERY_METHOD = 'deliveryMethod'
  // tslint:disable-next-line:max-file-line-count
}
