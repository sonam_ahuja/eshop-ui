// add path to this files
export default {
  BASKET_VIEW: 'basket view',
  BASKET: {
    EVENTS: {
      ADD_TO_CART: 'addToCart',
      LOAD_BASKET: 'checkout',
      Tooltip: 'toolTipButton',
      CONTINUE_SHOPPING: 'continueShopping',
      REMOVE: 'removeIconClick',
      REMOVE_CART: 'removeFromCart',
      CLEAR: 'cart.basket.popUps.removeButton',
      EMPTY: 'Empty Basket',
      PLACE_ORDER: 'cart.checkout.orderReview.placeOrder',
      PROCEED_TO_CHECKOUT: 'cart.basket.proceedToCheckout',
      REMOVE_ITEM: 'cart.basket.popUps.removeButton',
      ADD_ITEM: 'add item',
      STEPPER: 'basket stepper',
      CHANGE_VARIANT: 'cart.basket.changeVariant',
      SELECT_DIFF_DEVICE: 'select diff device',
      VIEW_BASKET: 'cart.basket.viewBasket',
      POPUP_CLOSE: 'popupClose'
    }
  },
  AUTHENTICATE: {
    EVENTS: {
      LOGIN_USER_NAME_ENTER: 'userNameNext',
      LOGIN_PASSWORD_VALIDATE: 'validatePassword',
      PASSWORD_NEXT: 'passwordNext',
      VALIDATE_OTP: 'cart.login.loginEnterOtp.validateOtp',
      FORGOT_CREDENTIALS: 'forgotCredentials',
      FORGOT_PASSWORD: 'forgotPassword',
      RECOVER_SUCCESS: 'recoverSuccessful',
      CHECKOUT_GUEST: 'checkoutAsGuest',
      SEND_OTP_AGAIN: 'resendOtp',
      SEND_AGAIN: 'sendAgain',
      LOGIN_SUCCESS: 'loginSuccess',
      LOGIN_FAILED: 'loginFailed',
      REGISTRATION_SUCCESS: 'registrationSuccess',
      REGISTRATION_FAILED: 'registrationFailed',
      LOG_OUT: 'logOut',
      NEXT: 'cart.login.common.next',
      VERIFICATION_NEXT: 'cart.login.identityVerification.next',
      VALIDATE_PASSWORD: 'cart.login.loginEnterPassword.validatePassword',
      CONTIUNE: 'cart.login.loginWithProceedAsGuest.continue',
      SIGN_OUT: 'sign out',
      HOME_PAGE: 'cart.login.logout.goToHomePage',
      SIGN_AGAIN: 'cart.login.logout.singInAgain',
      RECOVER_CREDS_NEXT: 'cart.login.recoveryCredentials.next',
      RECOVER: 'RECOVER'
    }
  },
  TARIFF: {
    EVENTS: {
      TAB_CLICK: 'tabClicks',
      DROP_DOWN_CLICK: 'dropdownClicks',
      YOUNG_TARIFF_VALIDATE: 'cart.tariff.youngTariff.validate',
      ADD_TO_PHONE: 'cart.tariff.addAPhone',
      BUY_PLAN: 'cart.tariff.buyPlanOnly',
      CONFIRM_PLAN: 'cart.tariff.confirmPlan',
      VIEW_ACTION_BUTTON: 'view action button',
      TERMS_CONDITION: 'cart.tariff.termsAndConditions',
      ADD_ON_CLICK: 'addOn click',
      ABOUT_DISCOUNT: 'cart.tariff.aboutDiscounts'
    }
  },
  CHECKOUT: {
    EVENTS: {
      SUPPORT_CALL: 'requestSupportCall',
      CHECKOUT: 'checkout',
      RENEW_INFO: 'cart.checkout.creditCheck.idNotMatchButtonText',
      SELECT_DIFF_DEVICE: 'cart.checkout.creditCheck.selectDifferentDeviceText',
      PAY_DEVICE_FULL: 'cart.checkout.creditCheck.payFullButtonText',
      SELECT_THIS_STORE: 'selectThisStore',
      CHOOSE_OTHER_STORE: 'chooseOtherStore',
      ADD_TO_CALENDAR: 'addToCalendar',
      TRACK_ORDER: 'cart.checkout.orderConfirmation.trackYourOrder',
      PORT_MY_NUMBER: 'cart.checkout.mnp.portMyNumberText',
      CANCEL_PORT: 'cart.checkout.mnp.cancelPortText',
      SELECT_MACHINE: 'selectMachine',
      SELECT_POINT: 'selectPoint',
      CHOOSE_OTHER_METHOD:
        'cart.checkout.orderReview.placeOrderModals.chooseOtherMethod',
      EDIT_DETAILS: 'editDetails',
      SELECT_STORE: 'selectStore',
      RESCHEDULE_ORDER: 'rescheduleOrder',
      REVIEW_ORDER: 'cart.checkout.billingInfo.reviewOrder',
      CREDIT_SUCCESS: 'cart.checkout.creditCheck.okGotIt',
      PAY_FULL_PRICE: 'cart.checkout.creditCheck.payFullButtonText',
      REQUEST_SUPPORT_CALL:
        'cart.checkout.creditCheck.requestSupportCallButtonText',
      MNP_VALIDATE_NUMBER: 'cart.checkout.mnp.validateNumberText',
      PORT_NUMBER: 'port number',
      CONFIRM_NUMBER: 'cart.checkout.mns.confirmNumber',
      PROCEED: 'proceed',
      PAYMENT_METHOD: 'payment method',
      PROCEED_TO_SHIPPING: 'cart.checkout.personalInfo.proceedToShipping',
      PROCEED_TO_CREDIT: 'cart.checkout.personalInfo.proceedToCreditButton',
      PROCEED_TO_PAYMENT: 'cart.checkout.shippingInfo.proceedToPayment',
      CHOOSE_OTHER: 'cart.checkout.shippingInfo.chooseOther',
      CHOOSE_SHIPPING: 'cart.checkout.shippingInfo.chooseShipping',
      TRY_AGAIN: 'cart.checkout.orderReview.placeOrderModals.tryAgain',
      TERMS_CONDITION: 'cart.global.termsAndConditions',
      NUMBER_PORTING: 'number porting',
      MNP_PORT_SEND_AGAIN: 'cart.checkout.mnp.sendAgainText',
      MNS_OTHER_NUMBER: 'cart.checkout.mns.otherNumbers',
      PORT_ON: 's',
      Port_Type: '',
      Connection_Type: '',
      CONTENT_FILL_OUT: 'contentFillOut',
      SMS_NOTIFICATION: 'smsNotification',
      ADDRESS_TOGGLE: 'addressToggle',
      MODIFY_PAYMENT_OPTION: 'modifyPaymentOptions',
      AGREEMENT: 'agreement',
      E_BILL: 'ebill',
      PAPER_BILL: 'paperBill'
    }
  },
  ERROR: {
    EVENTS: {
      ERROR_EVENTS: 'errorEvent'
    },
    CATEGORY: {}
  },
  COMMON: {
    EVENTS: {
      CTA_CLICKS: 'ctaClicks',
      HEADER_CLICk: 'headerClicks',
      SEARCH_CLICK: 'searchClick',
      FOOTER_CLICK: 'footerClicks',
      ERROR_EVENT: 'errorEvent',
      DROP_DOWN: 'dropdownClicks',
      POPUP_CLOSE: 'popupClose',
      BREAD_CRUMB_CLICK: 'breadCrumbClicks'
    }
  },
  DEVICE_CATALOG: {
    EVENTS: {
      VALIDATE_CREDENTIAL: 'cart.productList.validateButton',
      FILTER_APPLIED: 'filterApplied',
      MOST_POPULAR_CLICKS: 'mostPopularClicks',
      PRODUCT_CLICKS: 'productClick',
      PRODUCT_IMPRESSION: 'productImpression',
      OUT_OF_STOCK: 'cart.productList.notificationAction',
      REDIRECT_TO_DETAIL: 'redired to detail',
      VIEW_RESULT: 'cart.productList.viewResults',
      OUT_OF_STOCK_ACTION: 'OUT_OF_STOCK_ACTION',
      CLEAR_ALL: 'cart.productList.clearAll'
    },
    CATGORY: {}
  },
  DEVICE_DETAIL: {
    EVENTS: {
      PRODUCT_DETAIL_VIEW: 'productDetailView',
      NOTIFY_ME: 'cart.productDetailed.notifyMe',
      ADD_TO_BASKET: 'cart.productDetailed.addToBasket',
      COLOR_SELECTION: 'colorSelection',
      STORAGE_SELECTION: 'storageSelection',
      DEVICE_DEATIL: 'DEVICE DETAIL'
    }
  },
  CATEGORY: {
    EVENTS: {
      PROMOTION_VIEW: 'promotionView',
      PROMOTION_CLICK: 'promotionClick'
    }
  },
  FOOTER: {
    CATEGORY: {},
    EVENTS: {
      FOOTER_CLICKS: 'Footer Clicks'
    }
  },
  HEADER: {
    CATEGORY: {
      HEADER_CLICKs: 'Header Clicks'
    },
    EVENTS: {
      HEADER_CLICKs: 'headerClicks',
      LOGIN_SIGIN: 'login sign'
    }
  },
  ORDER_SUMMARY: {
    EVENTS: {
      SERVICE_INFO_TOGGLE: 'serviceInfoToggle',
      THIRD_PARTY_TOGGLE: 'thirdPartyToggle',
      TERMS_TOGGLE: 'termsToggle'
    }
  }
};
