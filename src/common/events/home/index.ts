import APP_CONSTANTS, { PAGE_VIEW } from '@events/constants/eventName';
import { BANNER } from '@events/constants';
import { sendEventGTM } from '@events/index';
import * as productListApi from '@common/types/api/productList';
import { IProductImpression, IPromotion } from '@events/types';
import { IProductListItem } from '@productList/store/types';
import { getDevicePrice } from '@events/common/index';
import { currencyDetails } from '@src/common/constants/currencyDetails';
import {
  getCommonState,
  getConfiguration,
  getLandingPageState
} from '@common/store/common/index';
import { IDataSection, IMetaData } from '@routes/home/store/types';

export const sendLandingPagePromotionViewEvent = () => {
  const promotions = getPromotionData();
  const data = {
    event: APP_CONSTANTS.CATEGORY.EVENTS.PROMOTION_VIEW,
    ecommerce: {
      promoView: {
        promotions: [...promotions]
      }
    }
  };
  if (promotions.length > 0) {
    sendEventGTM(data);
  }
};

export const sendLandingPagePromotionClickEvent = (metaData: IMetaData) => {
  const promotions = getPromotionClickData(metaData);
  if (promotions && promotions.length > 0) {
    const data = {
      event: APP_CONSTANTS.CATEGORY.EVENTS.PROMOTION_CLICK,
      ecommerce: {
        promoClick: {
          promotions: [...promotions]
        }
      }
    };
   sendEventGTM(data);
  }
};

export const getPromotionData = () => {
  const landingState = getLandingPageState();
  let promotions: IPromotion[] = [];
  if (
    landingState.staticContent &&
    landingState.staticContent.data &&
    landingState.staticContent.data.length > 0
  ) {
    const bannerContent = landingState.staticContent.data.filter(
      content => content.type === BANNER
    );

    // tslint:disable-next-line: no-ignored-return
    bannerContent.map((staticData: IDataSection) => {
      const promotionView =
        staticData.metaData &&
        staticData.metaData.map((meta: IMetaData, index: number) => {
          return {
            id: meta.id,
            name: meta.title,
            creative: `banner${index + 1}`,
            position: `slot${index + 1}`,
          };
        });
      promotions = promotions.concat(promotionView);
    });
  }

  return promotions;
};

export const getPromotionClickData = (data: IMetaData) => {
  const promotions = getPromotionData();

  const promotion = promotions.filter(({name, id}) => {
    return data.title === name || (id && data.id === id);
  });

   if (promotion.length > 0) {
     return [promotion[0]];
   }

   return null;
};

export const sendLandingPageProductImpressionEvent = (
  responseData: productListApi.POST.IResponse,
  categoryId: string
) => {
  const data = {
    event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.PRODUCT_IMPRESSION,
    ecommerce: {
      currencyCode: 'currencyCode',
      impressions: [] as IProductImpression[]
    }
  };

  responseData.data.forEach((product: IProductListItem, index: number) => {
    const price = getDevicePrice(product.variant.prices);
    const configuration = getConfiguration();
    const list = getCommonState().landingPageDeviceList || PAGE_VIEW.HOME;
    const currencyLocale =
      configuration.cms_configuration.global.currency.locale;
    data.ecommerce.currencyCode =
      currencyLocale && currencyDetails[currencyLocale]
        ? currencyDetails[
            configuration.cms_configuration.global.currency.locale
          ].symbol
        : null;

    data.ecommerce.impressions.push({
      name: product.variant.productName,
      id: product.variant.id,
      price: price ? price.discountedValue : null,
      brand: product.variant.brand,
      category: categoryId,
      variant: product.variant.name,
      list,
      position: index + 1
    });
  });
  if (data.ecommerce.impressions && data.ecommerce.impressions.length) {
    sendEventGTM(data);
  }
};
