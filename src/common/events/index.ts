import { IWindow } from '@src/client/index';
import APP_CONSTANTS from '@events/constants/eventName';
import { sendMessage } from '@common/mqt';
import { logError } from '@utils/index';
import appConstants from '@common/constants/appConstants';
import { getUniqueIdentifier } from '@utils/uniqueIdentifier';

export const sendEventGTM = (data: object) => {
  try {
    const userType = getUserId() ? 'loggedin' : 'guest';
    const guestId = getUniqueIdentifier();
    const updatedData: object = {
      ...data,
      User_Type: userType,
      Guest_Id: guestId
    };
    (window as IWindow).dataLayer.push(updatedData);
    sendMessageToQueue(updatedData);
  } catch (error) {
    logError(error);
  }
};

export const sendCTAClicks = (textName: string, URL: string) => {
  const data = {
    event: APP_CONSTANTS.COMMON.EVENTS.CTA_CLICKS,
    Event_Category: APP_CONSTANTS.COMMON.CATEGORY.CTA_CLICKS,
    Event_Action: textName,
    Event_Label: URL
  };
  sendEventGTM(data);
};

export const pageViewEvent = (pageName: string) => {
  const data = {
    event: 'PageView',
    Page_Name: pageName
  };

  sendEventGTM(data);
};

export const sendSearchEvent = (keyword: string, searchMode: string) => {
  const data = {
    event: APP_CONSTANTS.COMMON.EVENTS.SEARCH_CLICK,
    Search_Term: keyword,
    Search_Mode: searchMode
  };
  sendEventGTM(data);
};

export const getUserId = (): boolean | null | string => {
  return localStorage ? localStorage.getItem('IsLoggedIn') : false;
};

export const sendMessageToQueue = (data: object): void => {
  try {
    const dataToLog = {
      countryCode: appConstants.COUNTRY_CODE,
      environment: appConstants.ENVIRONMENT,
      userIdentifier: getUniqueIdentifier(),
      isUserLoggedIn: getUserId(),
      data
    };
    const stringfyData = JSON.stringify(dataToLog);
    sendMessage(stringfyData);
  } catch (error) {
    logError(error);
  }
};
