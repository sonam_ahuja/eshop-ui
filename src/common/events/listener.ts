import { isBrowser } from '@src/common/utils';
import { sendCTAClicks } from '@events/index';
import {
  sendFooterClickEvent,
  sendHeaderClickEvent
} from '@events/common/index';
import { sendValidateCredentialEvent } from '@events/DeviceList/index';
import {
  cancelPortSuccess,
  sendOtherSelectStoreEvent,
  sendPayDeviceFullEvent,
  sendRenewInfoEvent,
  sendRequestSupportEvent,
  sendSelectDiffDeviceEvent
} from '@events/checkout/index';
import EVENT_NAME from '@events/constants/eventName';
import EVENT_PATHS from '@events/constants/eventTranslationPath';
import { getComposedPath } from '@client/DOMUtils';
import { getEnglishTranslation } from '@common/store/common';

interface IEvent extends Event {
  path?: HTMLElement[];
}
export const trackClickEvent = () => {
  if (isBrowser) {
    document.addEventListener(
      'click',
      (event: IEvent): void => {
        const el = event.target as HTMLElement;
        if (el && el.dataset && el.dataset.eventId && el.dataset.eventMessage) {
          callEvent(el);
        } else {
          const element = findClickHandlerElement(event);
          if (element) {
            callEvent(element);
          }
        }
      },
      false
    );
  }
};

export const callEvent = (event: HTMLElement) => {
  if (event.dataset && event.dataset.eventId && event.dataset.eventMessage) {
    let message: string;
    if (event.dataset.eventPath) {
      message =
        getEnglishText(event.dataset.eventPath) || event.dataset.eventMessage;
    } else {
      const key = getEventKey(EVENT_NAME, event.dataset.eventId);
      const path = getEventPath(EVENT_PATHS, key);
      message = getEnglishText(path) || event.dataset.eventMessage;
    }

    switch (event.dataset.eventId) {
      case EVENT_NAME.HEADER.EVENTS.HEADER_CLICKs:
        sendHeaderClickEvent(message);
        break;
      case EVENT_NAME.FOOTER.EVENTS.FOOTER_CLICKS:
        sendFooterClickEvent(event.dataset.eventMessage, event.dataset
          .eventPageName as string);
        break;
      case EVENT_NAME.CHECKOUT.EVENTS.REQUEST_SUPPORT_CALL:
        sendRequestSupportEvent();
        logEvent(message);
        break;
      case EVENT_NAME.CHECKOUT.EVENTS.SELECT_DIFF_DEVICE:
        sendSelectDiffDeviceEvent();
        logEvent(message);
        break;
      case EVENT_NAME.CHECKOUT.EVENTS.PAY_FULL_PRICE:
        sendPayDeviceFullEvent();
        logEvent(message);
        break;
      case EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_OTHER:
        sendOtherSelectStoreEvent();
        logEvent(message);
        break;
      case EVENT_NAME.CHECKOUT.EVENTS.CANCEL_PORT:
        cancelPortSuccess();
        logEvent(message);
        break;
      case EVENT_NAME.CHECKOUT.EVENTS.RENEW_INFO:
        sendRenewInfoEvent();
        logEvent(message);
        break;
      case EVENT_NAME.DEVICE_CATALOG.EVENTS.VALIDATE_CREDENTIAL:
        sendValidateCredentialEvent();
        logEvent(message);
        break;
      case EVENT_NAME.TARIFF.EVENTS.YOUNG_TARIFF_VALIDATE:
        {
          sendValidateCredentialEvent();
          logEvent(message);
        }
        break;
      default:
        logEvent(message);
    }
  }
};

export const findClickHandlerElement = (event: IEvent) => {
  if (event) {
    const path = getComposedPath(event);
    if (path && path.length) {
      const domHandler = path.find(item => {
        return !!(
          item &&
          item.dataset &&
          item.dataset.eventId &&
          item.dataset.eventId !== undefined
        );
      });

      return domHandler ? domHandler : null;
    }
  }

  return null;
};

export const logEvent = (eventName: string) => {
  const URL = window.location.href;
  sendCTAClicks(eventName, URL);
};

export const getEnglishText = (pathName: string) => {
  if (!pathName) {
    return pathName;
  }

  const englishTranslation = getEnglishTranslation();
  const path = pathName.split('.');
  let value = englishTranslation;

  // tslint:disable-next-line:prefer-for-of
  for (let index = 0; index < path.length; index++) {
    if (value[path[index]]) {
      value = value[path[index]];
    }

    if (typeof value === 'string') {
      return value;
    }
  }

  return pathName;
};

// tslint:disable:no-any
const getEventKey = (obj: any, value: string): string => {
  for (const key in obj) {
    if (obj[key] === value) {
      return key;
    } else if (typeof obj[key] === 'object') {
      const foundKey = getEventKey(obj[key], value);
      if (foundKey) {
        return foundKey;
      }
    }
  }

  return '';
};

const getEventPath = (obj: any, value: string): string => {
  for (const key in obj) {
    if (key === value) {
      return obj[key];
    } else if (typeof obj[key] === 'object') {
      const foundKey = getEventPath(obj[key], value);
      if (foundKey) {
        return foundKey;
      }
    }
  }

  return '';
};
