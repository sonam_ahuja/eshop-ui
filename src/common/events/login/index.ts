import APP_CONSTANTS from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';
import { getLoginType } from '@events/common';

export const sendUserNameEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.LOGIN_USER_NAME_ENTER
  });
};

export const sendValidatePasswordEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.LOGIN_PASSWORD_VALIDATE
  });
};

export const sendPasswordNextEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.PASSWORD_NEXT
  });
};

export const sendValidateOTPEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.VALIDATE_OTP
  });
};

export const sendForgotCredentialsEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.FORGOT_CREDENTIALS
  });
};

export const sendForgotPasswordEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.FORGOT_PASSWORD
  });
};

export const sendRecoverPasswordSuccess = (type: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.RECOVER_SUCCESS,
    Event_Category: APP_CONSTANTS.AUTHENTICATE.CATEGORY.FORGOT_CRED,
    Event_Action: APP_CONSTANTS.AUTHENTICATE.ACTION.RECOVER_SUCCESS,
    EVENT_LABEL: type
  });
};

export const sendCheckoutGuestEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.CHECKOUT_GUEST
  });
};

export const sendOTPAgainEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.SEND_OTP_AGAIN
  });
};

export const sendAgainEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.SEND_AGAIN
  });
};

export const sendLoginSuccessEvent = (userID: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.LOGIN_SUCCESS,
    Event_Category: APP_CONSTANTS.AUTHENTICATE.CATEGORY.LOGIN,
    Event_Action: APP_CONSTANTS.AUTHENTICATE.ACTION.SUCCESS,
    Event_Label: getLoginType(),
    User_ID: userID
  });
};

export const sendLoginFailedEvent = (loginMethod: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.LOGIN_FAILED,
    Event_Category: APP_CONSTANTS.AUTHENTICATE.CATEGORY.LOGIN,
    Event_Action: APP_CONSTANTS.AUTHENTICATE.ACTION.FAILED,
    Event_Label: loginMethod
  });
};

export const sendLogOutEvent = () => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.LOG_OUT,
    Event_Category: APP_CONSTANTS.AUTHENTICATE.CATEGORY.LOG_OUT,
    Event_Action: APP_CONSTANTS.AUTHENTICATE.ACTION.SUCCESS
  });
};

export const sendRegistrationFailedEvent = (loginMethod: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.REGISTRATION_FAILED,
    Event_Category: APP_CONSTANTS.AUTHENTICATE.CATEGORY.REGISTRATION,
    Event_Action: APP_CONSTANTS.AUTHENTICATE.ACTION.FAILED,
    Event_Label: loginMethod
  });
};

export const sendRegistraionSuccessEvent = (loginMethod: string) => {
  sendEventGTM({
    event: APP_CONSTANTS.AUTHENTICATE.EVENTS.REGISTRATION_SUCCESS,
    Event_Category: APP_CONSTANTS.AUTHENTICATE.CATEGORY.REGISTRATION,
    Event_Action: APP_CONSTANTS.AUTHENTICATE.ACTION.SUCCESS,
    Event_Label: loginMethod
  });
};
