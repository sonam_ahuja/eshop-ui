import APP_CONSTANTS, { OFF, ON } from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';

export const sendServiceInfoToggleEvent = (status: boolean) => {
  const data = {
    event: APP_CONSTANTS.ORDER_SUMMARY.EVENTS.SERVICE_INFO_TOGGLE,
    Toggle_Switch: status ? ON : OFF
  };

  sendEventGTM(data);
};

export const sendThirdPartyToggleEvent = (status: boolean) => {
  const data = {
    event: APP_CONSTANTS.ORDER_SUMMARY.EVENTS.THIRD_PARTY_TOGGLE,
    Toggle_Switch: status ? ON : OFF
  };
  sendEventGTM(data);
};

export const sendTermsToggleEvent = (status: boolean) => {
  const data = {
    event: APP_CONSTANTS.ORDER_SUMMARY.EVENTS.TERMS_TOGGLE,
    Toggle_Switch: status ? ON : OFF
  };
  sendEventGTM(data);
};
