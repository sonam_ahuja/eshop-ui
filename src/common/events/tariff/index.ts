import APP_CONSTANTS, { NOT_APPLICABLE } from '@events/constants/eventName';
import { sendEventGTM } from '@events/index';
import { getConfiguration, getTariffState } from '@common/store/common';
import { IPlans } from '@src/common/routes/tariff/store/types';
import { PRICES } from '@src/common/routes/productList/store/enum';
import { currencyDetails } from '@src/common/constants/currencyDetails';
import { IProductImpression } from '@events/types';

export const sendTariffTabEvent = (tabName: string) => {
  const data = {
    event: APP_CONSTANTS.TARIFF.EVENTS.TAB_CLICK,
    Event_Category: APP_CONSTANTS.TARIFF.CATEGORY.TAB_CATEGEGORY,
    Event_Action: tabName
  };
  sendEventGTM(data);
};

export const sendTariffDiscountDropDownEvent = (discountName: string) => {
  const data = {
    event: APP_CONSTANTS.TARIFF.EVENTS.DROP_DOWN_CLICK,
    Event_Category: APP_CONSTANTS.TARIFF.CATEGORY.DROP_DOWN_CLICK,
    Event_Action: discountName
  };
  sendEventGTM(data);
};

export const sendTariffDurationDropDownEvent = (duration: string) => {
  const data = {
    event: APP_CONSTANTS.TARIFF.EVENTS.DROP_DOWN_CLICK,
    Event_Category: APP_CONSTANTS.TARIFF.CATEGORY.DROP_DOWN_CLICK,
    Event_Action: duration
  };
  sendEventGTM(data);
};

export const sendTariffDropDownEvent = (appName: string) => {
  const data = {
    event: APP_CONSTANTS.TARIFF.EVENTS.DROP_DOWN_CLICK,
    Event_Category: APP_CONSTANTS.TARIFF.CATEGORY.DROP_DOWN_CLICK,
    Event_Action: appName
  };
  sendEventGTM(data);
};

export const sendTariffImpressionEvent = () => {
  const tariffState = getTariffState();
  const data = {
    event: APP_CONSTANTS.DEVICE_CATALOG.EVENTS.PRODUCT_IMPRESSION,
    ecommerce: {
      currencyCode: 'currencyCode',
      impressions: [] as IProductImpression[]
    }
  };
  const configuration = getConfiguration();
  const currencyLocale = configuration.cms_configuration.global.currency.locale;

  if (tariffState && tariffState.plans && tariffState.plans.length) {
    tariffState.plans.forEach((plan: IPlans, index: number) => {
      const tariffPrice = getPlanPrice(plan);
      // @
      data.ecommerce.currencyCode =
        currencyLocale && currencyDetails[currencyLocale]
          ? currencyDetails[currencyLocale].symbol
          : null;
      data.ecommerce.impressions.push({
        name: plan.name,
        id: plan.id,
        price:
          tariffPrice && tariffPrice[0] ? tariffPrice[0].discountedValue : null,
        brand: NOT_APPLICABLE,
        category: tariffState.categoryId,
        variant: plan.name,
        list: '',
        position: index + 1
      });
    });
  }

  if (data.ecommerce.impressions && data.ecommerce.impressions.length) {
    sendEventGTM(data);
  }
};

export const getPlanPrice = (plan: IPlans) => {
  if (plan && plan.totalPrices && plan.totalPrices.length) {
    return plan.totalPrices.filter(price => price.priceType === PRICES.MONTHLY);
  }

  return null;
};
