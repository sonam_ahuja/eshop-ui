export interface IAddToBasketEvent {
  event: string;
  ecommerce: IEcommerce;
}

export interface IEcommerce {
  currencyCode: string;
  add: IAddProduct;
}

export interface IAddProduct {
  products: IProducts[];
}

export interface IProducts {
  name?: string;
  id?: string;
  price?: number;
  brand?: string;
  category: string;
  variant?: string;
  quantity?: number | null;
  dimension8?: string;
  dimension9?: string;
  dimension10?: string;
  dimension11?: string;
  dimension12?: string;
  dimension13?: string;
  dimension14?: string;
  dimension15?: string;
  dimension16?: string;
  dimension17?: string;
}

export interface IBasketLoadEvent {
  event: string;
  Basket_State: string;
  Product_Counts: string;
  Basket_Amount: number;
  ecommerce: IBasketLoadEcommerceEvent;
}

export interface IBasketLoadEcommerceEvent {
  checkout: {
    actionField: object;
    products: IProducts[];
  };
}

export interface IEvent {
  event: string;
  Event_Category?: string;
  Event_Action?: string;
  Event_Label?: string;
  Event_Value?: string;
}

export interface IBasketContinueShoppingEvent {
  event: string;
}
export interface IPromotion {
  id?: string;
  name?: string;
  creative?: string;
}

export interface IBasketRemoveCartEvent {
  event: string;
  ecommerce: {
    remove: {
      products: IProducts[];
    };
  };
}

export interface IProductImpression {
  name: string;
  id: string;
  price: number | null;
  brand?: string;
  category: string;
  variant: string;
  list: string;
  position: number;
}
