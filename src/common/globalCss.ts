export default `
*[tabindex]{
  outline: none;
}

::-moz-selection { /* Code for Firefox */
  background: transparent;
}

html{
  -webkit-overflow-scrolling: touch;
  overscroll-behavior: contain;
}
body{
  /* overflow-x: hidden prevents sticky from working but its fine for pppm,
  this code css overwrites in common for eshop */
  overflow-x: hidden;
  width: 100%;
}

/* ::selection {
  background: transparent;
} */

/* .styles__SummaryContentCompactWrapper-jctqrb-5{
  transition: 0.2s linear 0s !important;
} */
/* .hideDrawer{
  .isSticky{
  transform: translateY(0%) !important;
  position: relative !important;
  }
} */

/* LandscapeModePlaceholder */

/*** RESET CSS ***/
html {
  font-family: 'TeleGrotesk Next' !important;
}

@media (min-width: 1366px) {
  html {
    font-size: 20px !important;
  }
}

@media (max-width: 1365px) {
  html {
    font-size: 16px !important;
  }
}

button:focus {
  outline: none;
}

/*********** Product Detailed Plan Modal Body Scroll False CSS 03-06-2019 ***********/
body{
  &.overflowHide{
    overflow:hidden;
    position:fixed;
    width:100%;
  }
}
/*********** Product Detailed Plan Modal Body Scroll False CSS 03-06-2019 ***********/



*,
*::after,
*::before {
  box-sizing: border-box;
  font-family: 'TeleGrotesk Next';
  -webkit-tap-highlight-color: transparent;
}

html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
b,
u,
i,
center,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
canvas,
details,
embed,
figure,
figcaption,
footer,
header,
hgroup,
menu,
nav,
output,
ruby,
section,
summary,
time,
mark,
audio,
video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
menu,
nav,
section {
  display: block;
}

body {
  line-height: 1;
  text-rendering: optimizeLegibility;
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
}

ol,
ul {
  list-style: none;
}

a {
  color: inherit;
  text-decoration: none;
}

blockquote,
q {
  quotes: none;
}

blockquote::before,
blockquote::after,
q::before,
q::after {
  content: '';
  content: none;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}

/*** slick slider ***/
/* Slider */
.slick-slider
{
    position: relative;

    display: block;
    box-sizing: border-box;

    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;

    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
        touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
}

.slick-list
{
    position: relative;

    display: block;
    overflow: hidden;

    margin: 0;
    padding: 0;
}
.slick-list:focus
{
    outline: none;
}
.slick-list.dragging
{
    cursor: pointer;
    cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
       -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
         -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
}

.slick-track
{
    position: relative;
    top: 0;
    left: 0;

    display: block;
}
.slick-track:before,
.slick-track:after
{
    display: table;

    content: '';
}
.slick-track:after
{
    clear: both;
}
.slick-loading .slick-track
{
    visibility: hidden;
}

.slick-slide
{
    display: none;
    float: left;

    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.slick-slide img
{
    display: block;
}
.slick-slide.slick-loading img
{
    display: none;
}
.slick-slide.dragging img
{
    pointer-events: none;
}
.slick-initialized .slick-slide
{
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;

    height: auto;

    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}
#binkies-on-page,
#binkies-in-modal
{
	transition: opacity 0.5s, visibility 0.5s;
}
body.binkies-show #binkies-on-page,
body.binkies-show #binkies-in-modal{
	opacity: 1;
	visibility: visible;
}
body.binkies-hide #binkies-on-page,
body.binkies-hide #binkies-in-modal
{
	opacity: 0;
	visibility: hidden;
}
.binkies-bar{
  visibility: hidden;
}



/* hide scroll bar */
body *::-webkit-scrollbar {
    display: none;
    width: 0 !important ;
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
 body  *{
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  /* } */

  /** Binkies Page CSS */

  #binkies-on-page.outOfStock{
  opacity: 0.6;
  }
  @media only screen and (max-width: 767px){
    #binkies-on-page.outOfStock{
      opacity: 0.4;
    }
  }
}



#binkies-on-page{transition:0.5s linear 0s; opacity:0;}
.styledImageViewer{transition:0.5s linear 0s; opacity:1;}

body.binkies-show{
  #binkies-on-page{
    opacity:1;
  }
  .styledImageViewer{
    /* opacity:0; */
  }
}
body.binkies-hide{
  #binkies-on-page{
    opacity:0;
  }
  .styledImageViewer{
    opacity:1;
  }
}

img {
  color: transparent;
}


/* managing tags */
strong,
  b {
    font-weight: bold;
  }

  em,
  i {
    font-style: italic;
  }

  img:-moz-loading {
  visibility: hidden;
  }

  img {
  &:not([src]) {
    visibility: hidden !important;
    display: none;
  }
}


/*********** Footer Styles for Shadow DOM ************/
.footerHeadTitles{
  margin-bottom: 0.75rem;
}
.footerHeadTitles .link-txt{
    color:#d0d0d0;
    font-size: 0.75rem;
    line-height: 1rem;
    font-weight: bold;
    transition: all 1s ease 0s;
    text-transform:uppercase;
}
.footerListTitles .link-txt{
    margin-bottom: 0.5rem;
    opacity: 1;
    display: inline-block;
    transition: all 1s ease 0s;
    color: #a3a3a3;
    font-size: 1rem;
    line-height: 1.5rem;
}
.footerStaticLists .link-txt{
  color: #d0d0d0;
  font-size: 0.75rem;
  line-height: 1rem;
  margin-bottom: 0.75rem;
  font-weight: bold;
}
.termsConditionTitle .link-txt{
  font-size: 0.75rem;
  line-height: 1rem;
  color: #6C6C6C;
}
.footerHeadTitles .link-txt:hover,
.footerListTitles .link-txt:hover{
  opacity:0.6;
}
.termsConditionTitle .link-txt:hover{
  text-decoration:underline;
}

.default-list{
  color: rgb(237, 237, 237);
    font-size: 1.125rem;
    font-weight: bold;
    border-bottom: 1px solid rgb(75, 75, 75);
    padding: 1.875rem 0rem;
    border-left: 1.25rem solid transparent;
    border-right: 1.25rem solid transparent;
}
.default-list-wrap {
  background: #ffffff;
  text-align: left;
  border-bottom: 1px solid #e6e6e6;
  padding: 1.75rem 1.875rem 1.75rem 0rem;
  font-size: 1.25rem;
  line-height: 1.5rem;
  color: #383838;
  font-weight: normal;
  margin-left: 3rem;
}

`;
// tslint:disable-next-line: max-file-line-count
