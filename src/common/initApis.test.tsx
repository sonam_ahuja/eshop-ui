import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import appState from '@store/states/app';
import configureStore from 'redux-mock-store';
import { RootState } from '@common/store/reducers';
import { commonAction } from '@store/actions';

import InitApis, { mapDispatchToProps } from './initApis';

const mockStore = configureStore();
const initStateValue: RootState = appState();
const store = mockStore(initStateValue);

describe('<InitApis />', () => {
  const props = {
    getUserProfile: jest.fn()
  };

  test('should render properly', () => {
    window.localStorage.__proto__.getItem = jest.fn();
    jest
      .spyOn(window.localStorage.__proto__, 'getItem')
      .mockReturnValue('true');
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <InitApis {...props} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).getUserProfile();
    expect(dispatch.mock.calls[0][0]).toEqual(commonAction.fetchUserDetails());
  });
});
