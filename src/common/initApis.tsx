import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Component, ReactNode } from 'react';
import { commonAction } from '@store/actions';
import APP_CONSTANTS from '@common/constants/appConstants';

export interface IProps {
  getUserProfile(): void;
}

export class InitApis extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  componentDidMount(): void {
    const isUserLoggedIn = localStorage.getItem(
      APP_CONSTANTS.IS_USER_LOGGED_IN
    );
    if (isUserLoggedIn === 'true') {
      this.props.getUserProfile();
    }
  }

  render(): ReactNode {
    return null;
  }
}

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  getUserProfile(): void {
    dispatch(commonAction.fetchUserDetails());
  }
});

export default connect(
  undefined,
  mapDispatchToProps
)(InitApis);
