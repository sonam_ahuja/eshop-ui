export const ADD_BASKET_ITEM_SCENARIO = {
  ADD_SINGLE_PRODUCT_IN_BASKET: {
    // Single Product:  (Adding a single product in the basket.)
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        }
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_BUNDLE_PRODUCT_IN_BASKET: {
    // Bundle Product: (Adding a Bundle product in the basket.)
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Sim'
            }
          }
        ]
      }
    ],

    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_BUNDLE_WITH_ADD_ONS_PRODUCT_IN_BASKET: {
    // Bundle With add on:  (Adding a Bundle product with Add on in the basket.)
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Sim'
            }
          },
          {
            action: 'add',
            product: {
              id: 'Earphone'
            }
          }
        ]
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_MULTI_SINGLE_PRODUCT_IN_BASKET: {
    // Multi Single Product: (Adding more then 1 Single product by single json)
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        }
      },
      {
        action: 'add',
        product: {
          id: 'Computer'
        }
      },
      {
        action: 'add',
        product: {
          id: 'Sim'
        }
      },
      {
        action: 'add',
        product: {
          id: 'Laptop'
        }
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_MULTI_BUNDLE_PRODUCT_IN_BASKET: {
    //  Multi Bundle: (Adding more then 1 Bundle product by single json)
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'Computer'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Laptop'
            }
          }
        ]
      },
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Sim'
            }
          }
        ]
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_OUT_OF_STOCK_PRODUCT_IN_BASKET: {
    //  Out of Stock: (Adding Out of stock in the basket but we need to make sure that status of the item that we are going to add it should be Out of Stock. )
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'Charger'
        }
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_SINGLE_PRODUCT_WITH_ADD_ON_IN_BASKET: {
    // Single with add on: (Adding single product with Add - on)
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Earphone'
            }
          }
        ]
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_BUNDLE_PRODUCT_WITH_ADD_ON_OUT_OF_STOCK_IN_BASKET: {
    // Bundle With add on (Out of Stock): (Adding single product with Add - on but we need to make sure that status of the item that we are going to add it should be Out of Stock. )
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Sim'
            }
          },
          {
            action: 'add',
            product: {
              id: 'Charger'
            }
          }
        ]
      }
    ],
    id: '2c96bf396855b2fd016855d5ad91000b'
  },
  ADD_MUITI_BUNDLE_PRODUCT_WITH_ADD_ON_IN_BASKET: {
    // Multi Bundle with addon:
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'Computer'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Laptop'
            }
          },
          {
            action: 'add',
            product: {
              id: 'PenDrive'
            }
          }
        ]
      },
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Sim'
            }
          },
          {
            action: 'add',
            product: {
              id: 'Charger'
            }
          }
        ]
      }
    ],
    id: '2c96bf34685aabb501685b48a53a0089'
  },
  ADD_MULTIPLE_SINGLE_PRODUCT_WITH_ADD_ON_IN_BASKET: {
    // Multi- Single with Add-on for 1 product:
    cartItems: [
      {
        action: 'add',
        product: {
          id: 'iphone'
        },
        cartItems: [
          {
            action: 'add',
            product: {
              id: 'Earphone'
            }
          }
        ]
      },
      {
        action: 'add',
        product: {
          id: 'Computer'
        }
      },
      {
        action: 'add',
        product: {
          id: 'Sim'
        }
      },
      {
        action: 'add',
        product: {
          id: 'Laptop'
        }
      }
    ],
    id: '2c96bf34685b71fd01685bb6ff47006f'
  }
};
