import {
  IResponse,
  IBasketItem,
  ICartRecurringBreakUp
} from '@basket/store/types';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';

export const BASKET_ALL_ITEM: IResponse = {
  cartItems: [
    {
      id: '123457',
      isModifiable: true,
      group: 'tariff',
      quantity: 13,
      status: 'Active',
      totalPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 32413
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            }
          ],
          priceType: 'upfront',
          price: 213,
          totalPrice: 232323
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23424
            }
          ],
          priceType: 'monthly',
          price: 34,
          totalPrice: 232323
        }
      ],
      product: {
        specId: '123',
        categorySlug: '123',
        imageUrl: 'https://i.ibb.co/Y7MfRgd/sim.png',
        name: 'Mobile M Plan',
        shortDescription: 'hi',
        productGroupId: '1',
        description: '3GB Data / Unlimated Call && SMS',
        characteristic: [
          {
            name: 'SMS',
            value: 'Standard SIM card'
          },
          {
            name: 'Unlimated',
            value: 'Unlimited Applications'
          },
          {
            name: 'Brand',
            value: '1'
          },
          {
            name: 'storage',
            value: '1'
          },
          {
            name: 'colour',
            value: '1'
          },
          {
            name: 'numberOfInstalments',
            value: '1'
          },
          {
            name: 'Data',
            value: '1'
          },
          {
            name: 'minutes',
            value: '1'
          }
        ],
        id: '123458'
      },
      cartTerms: [
        {
          name: 'loyality',
          duration: {
            timePeriod: '24',
            type: 'months'
          }
        }
      ],
      itemPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'upfront',
          totalPrice: 123213,
          price: 134314
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'monthly',
          price: 134,
          totalPrice: 31244
        }
      ],
      cartItems: [
        {
          id: '123453',
          isModifiable: false,
          quantity: 11,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'base',
              totalPrice: 123213,

              price: 1223
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 123
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            }
          ],
          group: 'bundleItem',
          product: {
            specId: '123',
            categorySlug: '123',
            productGroupId: '1',
            shortDescription: 'hi',

            imageUrl: 'https://i.ibb.co/Kx9pn9Z/phone.png',
            name: 'samsung',
            description: '(RED) / 64GB',
            characteristic: [
              {
                name: 'warranty',
                value: '2 years full warranty'
              },
              {
                name: 'condition',
                value: 'Can only use with a physical SIM'
              }
            ],
            id: '123454'
          },
          cartTerms: [
            {
              name: '',
              duration: {
                timePeriod: '24',
                type: 'months'
              }
            }
          ],
          itemPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 12
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 34
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'base',
              totalPrice: 34,
              price: 445
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 445
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'upfront',
              price: 56,
              totalPrice: 45
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 56
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 67
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'monthly',
              price: 56,
              totalPrice: 45
            }
          ],
          cartItems: []
        },
        {
          id: '1234566',
          isModifiable: false,
          quantity: 11,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'base',
              totalPrice: 123213,

              price: 1223
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 12
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            }
          ],
          group: 'bundledProduct',
          product: {
            specId: '123',
            categorySlug: '123',
            productGroupId: '2',
            shortDescription: 'hi',

            imageUrl: 'https://i.ibb.co/Kx9pn9Z/phone.png',
            name: 'samsung',
            description: '(RED) / 64GB',
            characteristic: [
              {
                name: 'warranty',
                value: '2 years full warranty'
              },
              {
                name: 'condition',
                value: 'Can only use with a physical SIM'
              }
            ],
            id: '123454'
          },
          cartTerms: [
            {
              name: '',
              duration: {
                timePeriod: '24',
                type: 'months'
              }
            }
          ],
          itemPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 34
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'base',
              totalPrice: 1234,
              price: 45
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 56
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'upfront',
              price: 45,

              totalPrice: 455
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 567
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 456
                }
              ],
              priceType: 'monthly',
              price: 45,

              totalPrice: 345
            }
          ],
          cartItems: []
        }
      ]
    },
    {
      id: '123457',
      isModifiable: true,
      quantity: 13,
      group: 'device',
      status: 'Active',
      totalPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 32413
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            }
          ],
          priceType: 'upfront',
          price: 213,

          totalPrice: 232323
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23424
            }
          ],
          priceType: 'monthly',
          price: 34,

          totalPrice: 232323
        }
      ],
      product: {
        specId: '123',
        categorySlug: '123',
        imageUrl: 'https://i.ibb.co/Y7MfRgd/sim.png',
        name: 'Mobile M Plan',
        productGroupId: '1',
        shortDescription: 'hi',

        description: '3GB Data / Unlimated Call && SMS',
        characteristic: [
          {
            name: 'SMS',
            value: 'Standard SIM card'
          },
          {
            name: 'Unlimated',
            value: 'Unlimited Applications'
          },
          {
            name: 'Brand',
            value: '1'
          },
          {
            name: 'Storage',
            value: '1'
          },
          {
            name: 'Colour',
            value: '1'
          },
          {
            name: 'numberOfInstalments',
            value: '1'
          },
          {
            name: 'Data',
            value: '1'
          },
          {
            name: 'minutes',
            value: '1'
          }
        ],
        id: '123458'
      },
      cartTerms: [
        {
          name: 'loyality',
          duration: {
            timePeriod: '24',
            type: 'months'
          }
        }
      ],
      itemPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'upfront',
          totalPrice: 123213,

          price: 134314
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'monthly',

          price: 134,
          totalPrice: 31244
        }
      ],
      cartItems: [
        {
          id: '123453',
          isModifiable: false,
          quantity: 11,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'base',
              totalPrice: 123213,

              price: 1223
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'upfront',

              price: 213,
              totalPrice: 123123
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 123
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            }
          ],
          group: 'bundleItem',
          product: {
            specId: '123',
            categorySlug: '123',
            productGroupId: '1',
            shortDescription: 'hi',

            imageUrl: 'https://i.ibb.co/Kx9pn9Z/phone.png',
            name: 'samsung',
            description: '(RED) / 64GB',
            characteristic: [
              {
                name: 'warranty',
                value: '2 years full warranty'
              },
              {
                name: 'condition',
                value: 'Can only use with a physical SIM'
              }
            ],
            id: '123454'
          },
          cartTerms: [
            {
              name: '',
              duration: {
                timePeriod: '24',
                type: 'months'
              }
            }
          ],
          itemPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 12
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 34
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'base',

              totalPrice: 34,
              price: 445
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 445
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'upfront',
              price: 56,

              totalPrice: 45
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 56
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 67
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'monthly',

              price: 56,
              totalPrice: 45
            }
          ],
          cartItems: []
        },
        {
          id: '12345668',
          isModifiable: false,
          quantity: 11,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'base',
              totalPrice: 123213,

              price: 1223
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 12
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            }
          ],
          group: 'bundledProduct',
          product: {
            specId: '123',
            categorySlug: '123',
            productGroupId: '2',
            shortDescription: 'hi',

            imageUrl: 'https://i.ibb.co/Kx9pn9Z/phone.png',
            name: 'samsung',
            description: '(RED) / 64GB',
            characteristic: [
              {
                name: 'warranty',
                value: '2 years full warranty'
              },
              {
                name: 'condition',
                value: 'Can only use with a physical SIM'
              }
            ],
            id: '123454'
          },
          cartTerms: [
            {
              name: '',
              duration: {
                timePeriod: '24',
                type: 'months'
              }
            }
          ],
          itemPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 34
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'base',

              totalPrice: 1234,
              price: 45
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 56
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'upfront',

              price: 45,
              totalPrice: 455
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 567
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 456
                }
              ],
              priceType: 'monthly',

              price: 45,
              totalPrice: 345
            }
          ],
          cartItems: []
        }
      ]
    }
  ],
  cartSummary: [
    {
      priceAlterations: [
        {
          priceType: 'discount',
          duration: {
            timePeriod: '3',
            type: 'monthly'
          },
          price: 234,
          details: [
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            }
          ]
        },
        {
          priceType: 'discount',
          duration: {
            timePeriod: '23',
            type: 'monthly'
          },
          price: 12312,
          details: [
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            }
          ]
        }
      ],
      priceType: ITEM_PRICE_TYPE.RECURRING_FEE,
      price: 1323,
      totalPrice: 123213
    },
    {
      priceAlterations: [
        {
          priceType: 'discount',
          duration: {
            timePeriod: '23',
            type: 'monthly'
          },
          price: 123,
          details: [
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            }
          ]
        },
        {
          priceType: 'discount',
          duration: {
            timePeriod: '23',
            type: 'motnhly'
          },
          price: 41341,
          details: [
            {
              name: 'christmas',
              value: '1324'
            },
            {
              name: 'hello',
              value: '134'
            },
            {
              name: 'someting',
              value: '134'
            }
          ]
        }
      ],
      priceType: ITEM_PRICE_TYPE.UPFRONT,
      totalPrice: 12312,
      price: 123
    }
  ],
  totalItems: 8
};

export const BASKET_RESPONSE_WITH_OUTSTOCK_PRODUCT: IResponse = {
  cartItems: [
    {
      id: '123457',
      isModifiable: true,
      quantity: 13,
      status: 'OutOfStock',
      totalPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 32413
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            }
          ],
          priceType: 'upfront',
          price: 213,

          totalPrice: 232323
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23424
            }
          ],
          priceType: 'monthly',
          price: 34,

          totalPrice: 232323
        }
      ],
      group: 'SIM',
      product: {
        specId: '123',
        categorySlug: '123',
        productGroupId: '1',
        imageUrl: 'https://i.ibb.co/Y7MfRgd/sim.png',
        name: 'Mobile M Plan',
        shortDescription: 'hi',

        description: '3GB Data / Unlimated Call && SMS',
        characteristic: [
          {
            name: 'SMS',
            value: 'Standard SIM card'
          },
          {
            name: 'Unlimated',
            value: 'Unlimited Applications'
          }
        ],
        id: '123458'
      },
      cartTerms: [
        {
          name: 'loyality',
          duration: {
            timePeriod: '24',
            type: 'months'
          }
        }
      ],
      itemPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'upfront',
          totalPrice: 123213,

          price: 134314
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'monthly',
          price: 134,

          totalPrice: 31244
        }
      ],
      cartItems: [
        {
          id: '123453',
          isModifiable: false,
          quantity: 11,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'base',
              totalPrice: 123213,

              price: 1223
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 123
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            }
          ],
          group: 'bundleItem',
          product: {
            specId: '123',
            categorySlug: '123',
            productGroupId: '2',
            imageUrl: 'https://i.ibb.co/Kx9pn9Z/phone.png',
            name: 'samsung',
            shortDescription: 'hi',

            description: '(RED) / 64GB',
            characteristic: [
              {
                name: 'warranty',
                value: '2 years full warranty'
              },
              {
                name: 'condition',
                value: 'Can only use with a physical SIM'
              }
            ],
            id: '123454'
          },
          cartTerms: [
            {
              name: '',
              duration: {
                timePeriod: '24',
                type: 'months'
              }
            }
          ],
          itemPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 12
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 34
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'base',
              totalPrice: 34,
              price: 445
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 445
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'upfront',
              price: 56,
              totalPrice: 45
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 56
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 67
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'monthly',
              price: 56,
              totalPrice: 45
            }
          ],
          cartItems: []
        },
        {
          id: '1234566',
          isModifiable: false,
          quantity: 11,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'base',

              totalPrice: 123213,
              price: 1223
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '24',
                    type: 'months'
                  },
                  price: 32413
                },
                {
                  priceType: 'shipping',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                },
                {
                  priceType: 'something',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 23
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 12
                }
              ],
              priceType: 'upfront',
              price: 213,

              totalPrice: 123123
            }
          ],
          group: 'bundledProduct',
          product: {
            specId: '123',
            categorySlug: '123',
            productGroupId: '1',
            imageUrl: 'https://i.ibb.co/Kx9pn9Z/phone.png',
            name: 'samsung',
            shortDescription: 'hi',

            description: '(RED) / 64GB',
            characteristic: [
              {
                name: 'warranty',
                value: '2 years full warranty'
              },
              {
                name: 'condition',
                value: 'Can only use with a physical SIM'
              }
            ],
            id: '123454'
          },
          cartTerms: [
            {
              name: '',
              duration: {
                timePeriod: '24',
                type: 'months'
              }
            }
          ],
          itemPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 34
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'base',
              totalPrice: 1234,
              price: 45
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 56
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                }
              ],
              priceType: 'upfront',
              price: 45,
              totalPrice: 455
            },
            {
              priceAlterations: [
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 45
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 567
                },
                {
                  priceType: 'discount',
                  duration: {
                    timePeriod: '',
                    type: ''
                  },
                  price: 456
                }
              ],
              priceType: 'monthly',
              price: 45,
              totalPrice: 345
            }
          ],
          cartItems: []
        }
      ]
    }
  ],
  cartSummary: [
    {
      priceAlterations: [
        {
          priceType: 'discount',
          duration: {
            timePeriod: '3',
            type: 'monthly'
          },
          price: 234,
          details: [
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            }
          ]
        },
        {
          priceType: 'discount',
          duration: {
            timePeriod: '23',
            type: 'monthly'
          },
          price: 12312,
          details: [
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            }
          ]
        }
      ],
      priceType: 'upfront',
      price: 1323,
      totalPrice: 123213
    },
    {
      priceAlterations: [
        {
          priceType: 'discount',
          duration: {
            timePeriod: '23',
            type: 'monthly'
          },
          price: 123,
          details: [
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            },
            {
              name: '',
              value: ''
            }
          ]
        },
        {
          priceType: 'discount',
          duration: {
            timePeriod: '23',
            type: 'motnhly'
          },
          price: 41341,
          details: [
            {
              name: 'christmas',
              value: '1324'
            },
            {
              name: 'hello',
              value: '134'
            },
            {
              name: 'someting',
              value: '134'
            }
          ]
        }
      ],
      priceType: 'monthly',
      totalPrice: 12312,
      price: 123
    }
  ],
  totalItems: 8
};

export const BASKET_ITEM_NOT_HAVE_SUB_ITEMS: { cartItems: IBasketItem[] } = {
  cartItems: [
    {
      id: '123457',
      isModifiable: true,
      quantity: 13,
      status: 'OutOfStock',
      totalPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 32413
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23
            }
          ],
          priceType: 'upfront',
          price: 213,

          totalPrice: 232323
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 23424
            }
          ],
          priceType: 'monthly',
          price: 34,

          totalPrice: 232323
        }
      ],
      group: 'SIM',
      product: {
        specId: '123',
        categorySlug: '123',
        productGroupId: '1',
        shortDescription: 'hi',

        imageUrl: 'https://i.ibb.co/Y7MfRgd/sim.png',
        name: 'Mobile M Plan',
        description: '3GB Data / Unlimated Call && SMS',
        characteristic: [
          {
            name: 'SMS',
            value: 'Standard SIM card'
          },
          {
            name: 'Unlimated',
            value: 'Unlimited Applications'
          }
        ],
        id: '123458'
      },
      cartTerms: [
        {
          name: 'loyality',
          duration: {
            timePeriod: '24',
            type: 'months'
          }
        }
      ],
      itemPrice: [
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'upfront',
          totalPrice: 123213,
          price: 134314
        },
        {
          priceAlterations: [
            {
              priceType: 'discount',
              duration: {
                timePeriod: '24',
                type: 'months'
              },
              price: 1234
            },
            {
              priceType: 'shipping',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            },
            {
              priceType: 'something',
              duration: {
                timePeriod: '',
                type: ''
              },
              price: 123
            }
          ],
          priceType: 'monthly',
          price: 134,
          totalPrice: 31244
        }
      ],
      cartItems: []
    }
  ]
};

export const cartRecurringBreakUp: ICartRecurringBreakUp[] = [
  {
    afterDuration: {
      timePeriod: 200,
      type: 'sec'
    },
    afterDurationPrice: 123
  }
];
