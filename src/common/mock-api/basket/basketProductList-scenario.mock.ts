export const BASKET_LIST_SCENARIO = {
  BASKET_LIST_WITH_ALL_PRODUCT: {
    id: '2c96bf3368502b990168601ffbdd0066',
    totalItems: 6,
    cartItems: [
      {
        id: '2c96bf3368502b9901686020eacf0067',
        isModifiable: true,
        quantity: 1,
        product: {
          id: 'BESTXL24',
          name: 'Virtual Storage Medium',
          imageUrl:
            'http://localhost:8080/catalogManagement/productOffering/42',
          description: 'Virtual Storage Medium'
        },
        status: 'Active',
        totalPrice: [
          {
            totalPrice: 12,
            price: 12,
            priceType: 'upfront',
            duration: {
              timePeriod: 6,
              type: 'MONTHLY'
            }
          }
        ],
        itemPrice: [
          {
            totalPrice: 12,
            price: 12,
            priceType: 'upfront',
            duration: {
              timePeriod: 6,
              type: 'MONTHLY'
            }
          }
        ]
      },
      {
        id: '2c96bf3368502b990168602120510068',
        isModifiable: true,
        quantity: 1,
        product: {
          id: 'BESTXL25',
          name: 'BestXL-25-Virtual Storage Medium 25-name',
          imageUrl:
            'http://localhost:8080/catalogManagement/productOffering/42',
          description: 'BestXL-25-Virtual Storage Medium-desc'
        },
        status: 'Active',
        totalPrice: [
          {
            totalPrice: 0,
            price: 0,
            priceType: 'upfront'
          }
        ],
        itemPrice: [
          {
            totalPrice: 0,
            price: 0,
            priceType: 'upfront'
          }
        ]
      },
      {
        id: '2c96bf3368502b990168602133e80069',
        isModifiable: true,
        quantity: 1,
        product: {
          id: 'BESTXL26',
          name: 'BestXL-26-Virtual Storage Medium',
          imageUrl:
            'http://localhost:8080/catalogManagement/productOffering/42',
          description: 'BestXL-26-Virtual Storage Medium'
        },
        status: 'OutOfStock',
        totalPrice: [
          {
            totalPrice: 12,
            price: 12,
            priceType: 'recurringFee',
            duration: {
              timePeriod: 6,
              type: 'MONTHLY'
            }
          }
        ],
        itemPrice: [
          {
            totalPrice: 12,
            price: 12,
            priceType: 'recurringFee',
            duration: {
              timePeriod: 6,
              type: 'MONTHLY'
            }
          }
        ]
      },
      {
        id: '2c96bf3368502b99016860214654006a',
        isModifiable: true,
        quantity: 1,
        product: {
          id: 'BESTXL27',
          name: 'BestXL-27-Virtual Storage Medium',
          imageUrl:
            'http://localhost:8080/catalogManagement/productOffering/42',
          description: 'BestXL-27-Virtual Storage Medium'
        },
        status: 'Active',
        totalPrice: [
          {
            totalPrice: 0,
            price: 0,
            priceType: 'upfront',
            duration: {
              timePeriod: 6,
              type: 'MONTHLY'
            }
          }
        ],
        itemPrice: [
          {
            totalPrice: 0,
            price: 0,
            priceType: 'upfront',
            duration: {
              timePeriod: 6,
              type: 'MONTHLY'
            }
          }
        ]
      },
      {
        id: '2c96bf3368502b99016860215acf006b',
        isModifiable: true,
        quantity: 1,
        product: {
          id: 'BESTXL28',
          name: 'BestXL-28-Virtual Storage Medium-name',
          imageUrl:
            'http://localhost:8080/catalogManagement/productOffering/42',
          description: 'BestXL-28-Virtual Storage Medium -desc'
        },
        status: 'OutOfStock',
        totalPrice: [
          {
            totalPrice: 555,
            price: 555,
            priceType: 'upfront'
          }
        ],
        itemPrice: [
          {
            totalPrice: 555,
            price: 555,
            priceType: 'upfront'
          }
        ]
      }
    ],
    cartSummary: [
      {
        totalPrice: 1122,
        price: 1122,
        priceType: 'upfront',
        duration: {
          timePeriod: 6,
          type: 'MONTHLY'
        }
      },
      {
        totalPrice: 12,
        price: 12,
        priceType: 'recurringFee',
        duration: {
          timePeriod: 6,
          type: 'MONTHLY'
        }
      }
    ],
    status: 'Active'
  }
};
