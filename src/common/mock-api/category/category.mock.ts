import { IBestDeviceOffers, ICategory } from '@category/store/types';
import { ITEM_STATUS } from '@src/common/store/enums';

export const BestDeviceOffers: IBestDeviceOffers[] = [
  {
    id: '1',
    productId: '1',
    name: 'name',
    productName: '',
    description: 'description',
    group: 'group',
    bundle: true,
    characteristics: [
      {
        key: 'key',
        value: 'key'
      }
    ],
    prices: [
      {
        priceType: 'basePrice',
        actualValue: 300.0,
        discountedValue: 300.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'upfrontPayment',
        actualValue: 150.0,
        discountedValue: 160.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'recurringFee',
        actualValue: 200.0,
        discountedValue: 180.0,
        recurringChargePeriod: 'min',
        discounts: []
      }
    ],
    attachments: {
      key: [
        {
          name: 'key',
          url: 'value'
        }
      ]
    },
    unavailabilityReasonCodes: [ITEM_STATUS.OUT_OF_STOCK]
  }
];

export const tariffCategoryMock: ICategory = {
  slug: '',
  image: '',
  parentSlug: null,
  // TODO highLightProduct & priority are missing right now, will be added in future
  highLightProduct: {
    name: '',
    value: ''
  },
  id: 'tariffMobilePostpaid',
  name: 'Tariff postpaid',
  description:
    'All plans include Best network with LTE Max, Hotspot flat rate, Unlimited SMS, EU roaming.',
  subCategories: [],
  characteristics: [
    {
      name: 'selectedProductOfferingTerm',
      values: [
        {
          value: 'agreement12'
        }
      ]
    },
    {
      name: 'numberOfInstalments',
      values: [
        {
          value: ''
        }
      ]
    },
    {
      name: 'showLoyaltyDropdown',
      values: [
        {
          value: 'false'
        }
      ]
    },
    {
      name: 'showDiscountDropdown',
      values: [
        {
          value: 'true'
        }
      ]
    },
    {
      name: 'selectedTariff',
      values: [
        {
          value: 'best_t'
        }
      ]
    },
    {
      name: 'slug',
      values: [
        {
          value: 'tariff-mobile-postpaid'
        }
      ]
    },
    {
      name: 'parentSlug',
      values: [
        {
          value: 'tariff'
        }
      ]
    },
    {
      name: 'boxType',
      values: [
        {
          value: 'small'
        }
      ]
    },
    {
      name: 'template',
      values: [
        {
          value: 'categoryMobileDevice'
        }
      ]
    },
    {
      name: 'theme',
      values: [
        {
          value: 'secondary'
        }
      ]
    },
    {
      name: 'defaultSortBy',
      values: [
        {
          value: 'lowestPrice'
        }
      ]
    },
    {
      name: 'sortBy',
      values: [
        {
          value: ''
        }
      ]
    }
  ]
};
