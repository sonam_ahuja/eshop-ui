import { IBillingInfoResponse } from '@checkout/store/types';
import { IBillingFormField } from '@src/common/routes/checkout/store/billing/types';
import { BILLING_TYPE } from '@checkout/store/enums';
import { IFields, validationType } from '@common/store/types/configuration';

export const billingInfoResponse: IBillingInfoResponse = {
  billingAddress: [
    {
      streetNumber: 'streetNumber',
      city: 'city',
      postCode: 'postCode',
      flatNumber: 'flatNumber',
      address: 'address',
      deliveryNote: 'deliveryNote',
      unit: 'unit'
    }
  ],
  sameAsPersonalInfo: true,
  sameAsShippingInfo: true,
  typeOfBill: BILLING_TYPE.PAPER_BILL
};

export const billingForm: IBillingFormField = {
  streetAddress: {
    value: '',
    isValid: false,
    validationMessage: '',
    colDesktop: 1,
    colMobile: 1
  },
  city: {
    value: '',
    isValid: false,
    validationMessage: '',
    colDesktop: 1,
    colMobile: 1
  },
  postCode: {
    value: '',
    isValid: false,
    validationMessage: '',
    colDesktop: 1,
    colMobile: 1
  },
  streetNumber: {
    value: '',
    isValid: false,
    validationMessage: '',
    colDesktop: 1,
    colMobile: 1
  },
  unit: {
    value: '',
    isValid: false,
    validationMessage: '',
    colDesktop: 1,
    colMobile: 1
  }
};

export const formFields: IFields = {
  dummy: {
    order: 1,
    labelKey: '',
    inputType: '',
    show: false,
    readOnly: false,
    mandatory: false,
    validation: {
      type: 'regex' as validationType,
      value: '',
      message: ''
    }
  }
};
