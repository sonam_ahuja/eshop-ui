export const orderResponse = {
  orderStatus: {
    status: 'ORDER_CONFIRMED',
    subStatus: 'CONFIRMED'
  },
  orderId: '5cbd2e71-6b9c-45b9-a805-01c2f9c7f099',
  cartItems: [
    {
      id: '2c96814f6c26e732016c2886496702b4',
      group: 'device',
      quantity: 1,
      product: {
        id: 'iphone_xs_V_3',
        name: 'Apple iPhone Xs',
        variantName: 'Apple iPhone Xs 256 GB Gold',
        imageUrl:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_b3_83db3346daef3517e0fe821ecc68.jpeg',
        productGroupId: '2c9681676c26de1d016c288731b4009d',
        categoryId: 'Mobile',
        categoryName: 'Mobile',
        characteristic: [
          {
            name: 'numberOfInstalments',
            value: '10'
          },
          {
            name: 'storage',
            value: '256 GB'
          },
          {
            name: 'colour',
            value: 'Gold'
          },
          {
            name: 'bestDeviceOffers',
            value: 'bestDeviceOffers'
          },
          {
            name: 'promotion',
            value: 'promotion'
          },
          {
            name: 'metaTagDescription'
          },
          {
            name: 'metaTagTitle'
          }
        ]
      },
      totalPrice: [
        {
          totalPrice: 350,
          price: 400,
          priceType: 'upfrontPrice',
          priceAlterations: [
            {
              priceType: 'discount',
              price: -50
            }
          ]
        },
        {
          totalPrice: 1100,
          price: 1200,
          priceType: 'recurringFee',
          priceAlterations: [
            {
              priceType: 'discount',
              price: -100
            }
          ]
        }
      ],
      itemPrice: [
        {
          totalPrice: 350,
          price: 400,
          priceType: 'upfrontPrice',
          priceAlterations: [
            {
              priceType: 'discount',
              name: 'TariffPlan discount',
              price: -50
            }
          ]
        },
        {
          totalPrice: 0,
          price: 600,
          priceType: 'basePrice',
          priceAlterations: [
            {
              priceType: 'discount',
              name: 'Tariff Plan discount',
              price: -120
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Disocunt',
              price: -100
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -110
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -140
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan discount',
              price: -120
            },
            {
              priceType: 'discount',
              name: 'Tarrif Plan Discount',
              price: -140
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -130
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -160
            }
          ]
        }
      ]
    },
    {
      id: '2c96814f6c26e732016c2886496702b4',
      group: 'device',
      quantity: 1,
      product: {
        id: 'iphone_xs_V_3',
        name: 'Apple iPhone Xs',
        variantName: 'Apple iPhone Xs 256 GB Gold',
        imageUrl:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_b3_83db3346daef3517e0fe821ecc68.jpeg',
        productGroupId: '2c9681676c26de1d016c288731b4009d',
        categoryId: 'Mobile',
        categoryName: 'Mobile',
        characteristic: [
          {
            name: 'numberOfInstalments',
            value: '10'
          },
          {
            name: 'storage',
            value: '256 GB'
          },
          {
            name: 'colour',
            value: 'Gold'
          },
          {
            name: 'bestDeviceOffers',
            value: 'bestDeviceOffers'
          },
          {
            name: 'promotion',
            value: 'promotion'
          },
          {
            name: 'metaTagDescription'
          },
          {
            name: 'metaTagTitle'
          }
        ]
      },
      totalPrice: [
        {
          totalPrice: 350,
          price: 400,
          priceType: 'upfrontPrice',
          priceAlterations: [
            {
              priceType: 'discount',
              price: -50
            }
          ]
        },
        {
          totalPrice: 1100,
          price: 1200,
          priceType: 'recurringFee',
          priceAlterations: [
            {
              priceType: 'discount',
              price: -100
            }
          ]
        }
      ],
      itemPrice: [
        {
          totalPrice: 350,
          price: 400,
          priceType: 'upfrontPrice',
          priceAlterations: [
            {
              priceType: 'discount',
              name: 'TariffPlan discount',
              price: -50
            }
          ]
        },
        {
          totalPrice: 0,
          price: 600,
          priceType: 'basePrice',
          priceAlterations: [
            {
              priceType: 'discount',
              name: 'Tariff Plan discount',
              price: -120
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Disocunt',
              price: -100
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -110
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -140
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan discount',
              price: -120
            },
            {
              priceType: 'discount',
              name: 'Tarrif Plan Discount',
              price: -140
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -130
            },
            {
              priceType: 'discount',
              name: 'Tariff Plan Discount',
              price: -160
            }
          ]
        }
      ]
    },
    {
      id: '2c96814f6c26e732016c2886c14102c6',
      group: 'deliveryMethod',
      quantity: 1,
      product: {
        id: 'eve_dlv_1',
        name: 'eve_dlv_spec_1',
        variantName: 'eve_dlv_1',
        productGroupId: '2c9681676c26de1d016c288731b700b2',
        categoryId: 'deliveryMethod',
        categoryName: 'deliveryMethod',
        characteristic: [
          {
            name: 'deliveryDate',
            value: '2019-07-29T09:46:49.000Z'
          },
          {
            name: 'metaTagDescription'
          },
          {
            name: 'metaTagTitle'
          }
        ]
      },
      totalPrice: [
        {
          totalPrice: 40,
          price: 40,
          priceType: 'upfrontPrice',
          priceAlterations: [
            {
              priceType: 'discount',
              price: 0
            }
          ]
        },
        {
          totalPrice: 0,
          price: 0,
          priceType: 'recurringFee',
          priceAlterations: [
            {
              priceType: 'discount',
              price: 0
            }
          ]
        }
      ],
      itemPrice: [
        {
          totalPrice: 40,
          price: 40,
          priceType: 'upfrontPrice'
        },
        {
          totalPrice: 0,
          price: 0
        }
      ]
    }
  ],
  personalInfo: {
    name: 'Jitendra',
    email: 'jsmith@natco.com',
    contact: '9876543210'
  },
  shippingDetails: {
    id: '2c96814f6c26e732016c2886c14102c6',
    estimatedDelivery: 'eve_dlv_1',
    deliveryType: 'withinTime',
    shippingTo: '21 Warsaw Warsaw 00255 '
  }
};
