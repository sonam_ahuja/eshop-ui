import {
  IPaymentInfoResponse,
  ICardDetailResponse,
  ICardDetails,
  IAddNewCard
} from '@checkout/store/payment/types';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/types';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

export const addNewCard: IAddNewCard = {
  cardId: '1',
  cardNumber: {
    value: '',
    isValid: true,
    validationMessage: ''
  },
  isDefault: true,
  cardType: 'monthly',
  expiryDate: {
    value: '',
    isValid: true,
    validationMessage: ''
  },
  securityCode: {
    value: '',
    isValid: true,
    validationMessage: ''
  },
  nameOnCard: {
    value: '',
    isValid: true,
    validationMessage: ''
  },
  isCardSaved: true,
  lastFourDigits: '1234',
  brand: 'visa',
  nonce: ''
};

export const cardDetails: ICardDetails = {
  cardNumber: '88888888',
  isDefault: true,
  lastFourDigits: '33333',
  cardType: 'upfront',
  expiryDate: '13/20',
  securityCode: '2033',
  nameOnCard: 'nameOnCard',
  id: '1',
  isCardSaved: true,
  brand: 'visa'
};
const card: ICardDetailResponse = {
  cardNumber: '88888888',
  isDefault: true,
  lastFourDigits: '33333',
  cardType: 'upfront',
  expiryDate: '13/20',
  securityCode: '2033',
  nameOnCard: 'nameOnCard',
  id: '1',
  isCardSaved: true,
  brand: 'visa'
};

export const cardDetailsState: ICardState = {
  cardNumber: {
    value: '',
    isValid: true,
    validationMessage: '',
    isDirty: true
  },
  nameOnCard: {
    value: '',
    isValid: true,
    validationMessage: '',
    isDirty: true
  },
  expiryDate: {
    value: '',
    isValid: true,
    validationMessage: '',
    isDirty: true
  },
  securityCode: {
    value: '',
    isValid: true,
    validationMessage: '',
    isDirty: true
  },
  cardType: 'dummy',
  brand: 'dummy',
  showForm: true
};

export const PaymentResponseData: IPaymentInfoResponse = {
  upfront: {
    tokenizedCard: { cardDetails: [card], id: '' },
    bankAccounts: [],
    payByLink: {},
    payOnDelivery: {},
    paymentTypeSelected: PAYMENT_TYPE.CREDIT_DEBIT_CARD
  },
  monthly: {
    sameAsUpfront: true,
    tokenizedCard: { cardDetails: [card], id: '' },
    bankAccounts: [],
    payByLink: {},
    payOnDelivery: {},
    paymentTypeSelected: PAYMENT_TYPE.CREDIT_DEBIT_CARD
  }
};

export const MonthlyData = {
  cardDetails: [card],
  saveNewCard: false,
  showEditIcon: false,
  useSameCard: false,
  proceedDisabled: true,
  addingNewCard: false
};
