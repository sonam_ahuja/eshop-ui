import { ILocalFormState } from '@checkout/CheckoutSteps/PersonalInfo/formGrid';
import { IPersonalInfoConverterResponse } from '@checkout/store/personalInfo/types';

export interface ILocalFormStateWithStatus extends ILocalFormState {
  status: boolean;
}

export const localFormStateWithStatus: ILocalFormStateWithStatus = {
  status: true,
  address: {
    value: 'address',
    isValid: true,
    validationMessage: '',
    colMobile: 12,
    colDesktop: 12,
    order: 1
  },
  streetNumber: {
    value: 'streetNumber',
    isValid: true,
    validationMessage: '',
    colMobile: 12,
    colDesktop: 12,
    order: 1
  },
  flatNumber: {
    value: 'flatNumber',
    isValid: true,
    validationMessage: '',
    colMobile: 12,
    colDesktop: 12,
    order: 1
  },
  city: {
    value: 'city',
    isValid: true,
    validationMessage: '',
    colMobile: 12,
    colDesktop: 12,
    order: 1
  },
  postCode: {
    value: 'postCode',
    isValid: true,
    validationMessage: '',
    colMobile: 12,
    colDesktop: 12,
    order: 1
  }
};

export const personalInfoConverterResponse: IPersonalInfoConverterResponse = {
  id: '',
  formFields: {
    firstName: '',
    lastName: '',
    dob: '',
    address: '',
    addressId: '',
    city: '',
    flatNumber: '',
    pesel:'',
    idNumber: '',
    oibNumber: '',
    company: '',
    postCode: '',
    streetNumber: '',
    emailId: '',
    email: '',
    phoneNumberId: '',
    phoneNumber: ''
  },
  isDefault: false,
  isSmsEnabled: false,
  smsNotificationName: '',
  smsNotificationDesc: ''
};
