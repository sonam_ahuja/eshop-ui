import {
  IShippingFormField,
  IShippingAddressResponse,
  IShippingForm
} from '@checkout/store/shipping/types';
import { SHIPPING_TYPE } from '@checkout/store/enums';

export const ShippingFormFieldData: IShippingFormField = {};

export const ShippingAddressResponse: IShippingAddressResponse = {
  sameAsPersonalInfo: true,
  shippingAddress: [
    {
      id: '8108ed35-8c71-4cfa-bb63-d342f8cd7e4f',
      streetNumber: '1',
      address: 'Friedrich-Ebert-Allee',
      flatNumber: '2',
      postCode: '53113',
      city: 'Bonn',
      country: 'Deutschland',
      deliveryNote: 'string',
      unit: 'string',
      state: 'string',
      name: '',
      geoX: '',
      geoY: ''
    }
  ],
  deliveryMethod: {
    id: '',
    fee: 0,
    shippingType: SHIPPING_TYPE.STANDARD,
    characteristics: [
      {
        name: 'deliveryDate',
        value: '2019-01-02T10:00:00Z'
      }
    ]
  }
};

export const ShippingForm: IShippingForm = {
  id: undefined,
  streetAddress: {
    value: '',
    isValid: false,
    validationMessage: '',
    id: undefined
  },
  city: {
    value: '',
    isValid: false,
    validationMessage: '',
    id: undefined
  },
  notes: {
    value: '',
    isValid: false,
    validationMessage: '',
    id: undefined
  },
  postCode: {
    value: '',
    isValid: false,
    validationMessage: '',
    id: undefined
  },
  streetNumber: {
    value: '',
    isValid: false,
    validationMessage: '',
    id: undefined
  },
  unit: {
    value: '',
    isValid: false,
    validationMessage: '',
    id: undefined
  },
  isDefault: false,
  enableProceedButton: false,
  isFormChanged: false,
  isPersonalInfoPrefilled: false
};
