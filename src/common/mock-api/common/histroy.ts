import { RouteComponentProps } from 'react-router-dom';
export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export type IComponentProps = RouteComponentProps<IRouterParams>;

export const histroyParams: IComponentProps = {
  match: {
    path: '/basket',
    url: '/basket',
    isExact: true,
    params: {
      match: '',
      history: '',
      location: ''
    }
  },
  location: {
    pathname: '/basket',
    search: '',
    hash: '',
    key: 'hmi41z',
    state: ''
  },
  history: {
    length: 2,
    action: 'POP',
    location: {
      pathname: '/basket',
      search: '',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    push: jest.fn(),
    replace: jest.fn(),
    go: jest.fn(),
    goBack: jest.fn(),
    goForward: jest.fn(),
    block: jest.fn(),
    listen: jest.fn(),
    createHref: jest.fn()
  }
};
