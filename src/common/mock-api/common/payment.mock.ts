import {
  IPaymentMethods,
  IPaymentMethodResponseTypes,
  IAavailablePaymentMode
} from '@common/store/types/configuration';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

export const cardValue: IPaymentMethodResponseTypes = {
  show: true,
  link: 'http://localhost:4000',
  labelKey: 'card'
};

export const availablePaymentMode: IAavailablePaymentMode = {
  maestro: {
    show: true
  },

  applePay: {
    show: true
  },

  googlePay: {
    show: true
  },

  visa: {
    show: true
  },

  payPal: {
    show: true
  },
  worldPay: {
    show: true
  }
};

export const paymentMethod: IPaymentMethods = {
  upfront: {
    tokenizedCard: { ...cardValue },
    payOnDelivery: { ...cardValue },
    payByLink: { ...cardValue },
    bankAccount: { ...cardValue },
    manualPayments: { ...cardValue },
    defaultPaymentMethod: PAYMENT_TYPE.CREDIT_DEBIT_CARD
  },
  monthly: {
    tokenizedCard: { ...cardValue },
    payOnDelivery: { ...cardValue },
    payByLink: { ...cardValue },
    bankAccount: { ...cardValue },
    manualPayments: { ...cardValue },
    defaultPaymentMethod: PAYMENT_TYPE.CREDIT_DEBIT_CARD
  },
  availablePaymentMode: { ...availablePaymentMode }
};
