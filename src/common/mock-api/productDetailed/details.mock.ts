import {
  POST as PRODUCT_DETAILS,
  GET_PRODUCT_SPECIFICATION
} from '@common/types/api/productDetailed';
import { ITEM_STATUS } from '@common/store/enums';
import {
  IInstallmentData,
  INotificationData,
  IProductDetailResponse
} from '@productDetailed/store/types';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';

export const productDetailsResponse: PRODUCT_DETAILS.IResponse = {
  categoryName: 'Mobile',
  productName: '',
  variants: [
    {
      id: 'nokia_14',
      name: 'Nokia 17 -32GB White',
      group: '',
      unavailabilityReasonCodes: [ITEM_STATUS.OUT_OF_STOCK],
      characteristics: [
        {
          name: 'Brand',
          values: [
            {
              value: 'Nokia'
            }
          ]
        },
        {
          name: 'Storage',
          values: [
            {
              value: '32GB'
            }
          ]
        },
        {
          name: 'Colour',
          values: [
            {
              value: 'black'
            }
          ]
        },
        {
          name: 'variantTags',
          values: [
            {
              value: 't1,t2'
            }
          ]
        },
        {
          name: 'metaTagDescription',
          values: [
            {
              value: ''
            }
          ]
        },
        {
          name: 'metaTagTitle',
          values: [
            {
              value: ''
            }
          ]
        }
      ],
      attachments: {
        thumbnail: [
          {
            url:
              'https://tshop.r10s.com/f0b/3a4/3357/93e1/7055/01a6/1a15/1172e9aa01a81e84d03674.jpg',
            type: 'front',
            name: 'front'
          },
          {
            url:
              'https://tshop.r10s.com/00b/32b/3436/cb1f/f004/015f/1aeb/1189e9aa01a81e84d03674.jpg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://tshop.r10s.com/03b/38d/3452/2ffa/b06a/0144/1a64/11dae9aa01a81e84d03674.jpg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://tshop.r10s.com/6ab/3d6/35f7/41ac/203d/01fe/1a31/11dde9aa01a81e84d03674.jpg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://s3.eu-central-1.amazonaws.com/eshop-sales-catalog-sylius/new/d3/55/906685f7a71068f43b74d8a839b0.jpeg',
            type: 'picture',
            name: 'front'
          }
        ]
      },
      prices: [
        {
          priceType: 'basePrice',
          actualValue: 300.0,
          discountedValue: 300.0,
          recurringChargePeriod: 'min',
          discounts: []
        },
        {
          priceType: 'upfrontPayment',
          actualValue: 150.0,
          discountedValue: 160.0,
          recurringChargePeriod: 'min',
          discounts: []
        },
        {
          priceType: 'recurringFee',
          actualValue: 200.0,
          discountedValue: 180.0,
          recurringChargePeriod: 'min',
          discounts: []
        }
      ],
      quantity: 10
    }
  ],
  groups: {
    Storage: [
      {
        label: '32 GB',
        value: '32 GB',
        enabled: true,
        name: 'dummy'
      }
    ],
    Colour: [
      {
        label: 'White',
        value: 'White',
        enabled: true,
        name: 'dummy'
      }
    ]
  },
  tariffs: [
    {
      id: 'best_l_1',
      name: 'Best L',
      group: '',
      description: 'Descriptions',
      isSelected: true,
      characteristics: [
        {
          name: 'selectedProductOfferingTerm',
          values: [
            {
              value: 'agreement12',
              label: '12 Months Loyality'
            },
            {
              value: 'agreement24',
              label: '24 Months Loyality'
            },
            {
              value: 'agreement36',
              label: '36 Months Loyality'
            }
          ]
        },
        {
          name: 'discount',
          values: [
            {
              value: 'magentaDiscount',
              label: 'Magenta Discount'
            },
            {
              value: 'familyDiscount',
              label: 'Family Discount'
            }
          ]
        },
        {
          name: 'minutes',
          values: [
            {
              value: 'minutes',
              label: '500'
            }
          ]
        },
        {
          name: 'call_sms',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'maxtv',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'Data',
          values: [
            {
              value: 'Data',
              label: '1 GB'
            }
          ]
        },
        {
          name: 'metaTagDescription',
          values: [
            {
              value: 'des'
            }
          ]
        },
        {
          name: 'metaTagTitle',
          values: [
            {
              value: ''
            }
          ]
        }
      ],
      prices: [
        {
          priceType: 'recurringFee',
          actualValue: 100,
          discountedValue: 88,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      totalPrices: [
        {
          priceType: 'recurringFee',
          actualValue: 100,
          discountedValue: 88,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      groups: {
        productOfferingBenefit: [
          {
            name: 'discount',
            label: 'Magenta Discount',
            value: 'magentaDiscount',
            isSelected: false
          },
          {
            name: 'discount',
            label: 'Family Discount',
            value: 'familyDiscount',
            isSelected: false
          }
        ],
        productOfferingTerm: [
          {
            name: 'selectedProductOfferingTerm',
            label: '12 Months Loyality',
            value: 'agreement12',
            isSelected: true
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '24 Months Loyality',
            value: 'agreement24',
            isSelected: false
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '36 Months Loyality',
            value: 'agreement36',
            isSelected: false
          }
        ]
      },
      addons: [
        {
          id: 'facebook',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        },
        {
          id: 'extra_data_20_gb',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        }
      ]
    },
    {
      id: 'best_s',
      name: 'Best S',
      group: '',
      description: '',
      isSelected: false,
      characteristics: [
        {
          name: 'selectedProductOfferingTerm',
          values: [
            {
              value: 'agreement12',
              label: '12 Months Loyality'
            },
            {
              value: 'agreement24',
              label: '24 Months Loyality'
            },
            {
              value: 'agreement36',
              label: '36 Months Loyality'
            }
          ]
        },
        {
          name: 'discount',
          values: [
            {
              value: 'magentaDiscount',
              label: 'Magenta Discount'
            },
            {
              value: 'familyDiscount',
              label: 'Family Discount'
            }
          ]
        },
        {
          name: 'minutes',
          values: [
            {
              value: 'minutes',
              label: '1000'
            }
          ]
        },
        {
          name: 'call_sms',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'maxtv',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'BirthDateLimit',
          values: [
            {
              value: 'kuchBhi',
              label: 'BirthDatLimit',
              valueFrom: '1990-01-01',
              valueTo: '2019-06-14'
            }
          ]
        },
        {
          name: 'Data',
          values: [
            {
              value: 'Data',
              label: '20 GB'
            }
          ]
        },
        {
          name: 'metaTagDescription',
          values: [
            {
              value: ''
            }
          ]
        },
        {
          name: 'metaTagTitle',
          values: [
            {
              value: ''
            }
          ]
        }
      ],
      prices: [
        {
          priceType: 'recurringFee',
          actualValue: 100,
          discountedValue: 95,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      totalPrices: [
        {
          priceType: 'recurringFee',
          actualValue: 100,
          discountedValue: 88,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      groups: {
        productOfferingBenefit: [
          {
            name: 'discount',
            label: 'Magenta Discount',
            value: 'magentaDiscount',
            isSelected: false
          },
          {
            name: 'discount',
            label: 'Family Discount',
            value: 'familyDiscount',
            isSelected: false
          }
        ],
        productOfferingTerm: [
          {
            name: 'selectedProductOfferingTerm',
            label: '12 Months Loyality',
            value: 'agreement12',
            isSelected: true
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '24 Months Loyality',
            value: 'agreement24',
            isSelected: false
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '36 Months Loyality',
            value: 'agreement36',
            isSelected: false
          }
        ]
      },
      addons: [
        {
          id: 'facebook',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        },
        {
          id: 'extra_data_20_gb',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        }
      ]
    },
    {
      id: 'best_m',
      name: 'Best M',
      group: '',
      description: '',
      isSelected: false,
      characteristics: [
        {
          name: 'selectedProductOfferingTerm',
          values: [
            {
              value: 'agreement12',
              label: '12 Months Loyality'
            },
            {
              value: 'agreement24',
              label: '24 Months Loyality'
            },
            {
              value: 'agreement36',
              label: '36 Months Loyality'
            }
          ]
        },
        {
          name: 'discount',
          values: [
            {
              value: 'magentaDiscount',
              label: 'Magenta Discount'
            },
            {
              value: 'familyDiscount',
              label: 'Family Discount'
            }
          ]
        },
        {
          name: 'minutes',
          values: [
            {
              value: 'minutes',
              label: '1000'
            }
          ]
        },
        {
          name: 'Data',
          values: [
            {
              value: 'data',
              label: '15 GB'
            }
          ]
        },
        {
          name: 'call_sms',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'maxtv',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'metaTagDescription',
          values: [
            {
              value: 'Best m'
            }
          ]
        },
        {
          name: 'metaTagTitle',
          values: [
            {
              value: ''
            }
          ]
        }
      ],
      prices: [
        {
          priceType: 'recurringFee',
          actualValue: 150,
          discountedValue: 150,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      totalPrices: [
        {
          priceType: 'recurringFee',
          actualValue: 100,
          discountedValue: 88,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      groups: {
        productOfferingBenefit: [
          {
            name: 'discount',
            label: 'Magenta Discount',
            value: 'magentaDiscount',
            isSelected: false
          },
          {
            name: 'discount',
            label: 'Family Discount',
            value: 'familyDiscount',
            isSelected: false
          }
        ],
        productOfferingTerm: [
          {
            name: 'selectedProductOfferingTerm',
            label: '12 Months Loyality',
            value: 'agreement12',
            isSelected: true
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '24 Months Loyality',
            value: 'agreement24',
            isSelected: false
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '36 Months Loyality',
            value: 'agreement36',
            isSelected: false
          }
        ]
      },
      addons: [
        {
          id: 'facebook',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        },
        {
          id: 'extra_data_20_gb',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        }
      ]
    },
    {
      id: 'best_xl',
      name: 'Best XL',
      group: '',
      description: '',
      isSelected: false,
      characteristics: [
        {
          name: 'selectedProductOfferingTerm',
          values: [
            {
              value: 'agreement12',
              label: '12 Months Loyality'
            },
            {
              value: 'agreement24',
              label: '24 Months Loyality'
            },
            {
              value: 'agreement36',
              label: '36 Months Loyality'
            }
          ]
        },
        {
          name: 'discount',
          values: [
            {
              value: 'magentaDiscount',
              label: 'Magenta Discount'
            },
            {
              value: 'familyDiscount',
              label: 'Family Discount'
            }
          ]
        },
        {
          name: 'minutes',
          values: [
            {
              value: 'minutes',
              label: 'Unlimited'
            }
          ]
        },
        {
          name: 'call_sms',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'maxtv',
          values: [
            {
              value: 'true',
              label: 'true'
            }
          ]
        },
        {
          name: 'Data',
          values: [
            {
              value: 'Data',
              label: '10 GB'
            }
          ]
        },
        {
          name: 'metaTagDescription',
          values: [
            {
              value: ''
            }
          ]
        },
        {
          name: 'metaTagTitle',
          values: [
            {
              value: ''
            }
          ]
        }
      ],
      prices: [
        {
          priceType: 'recurringFee',
          actualValue: 300,
          discountedValue: 250,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      totalPrices: [
        {
          priceType: 'recurringFee',
          actualValue: 100,
          discountedValue: 88,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: -1
        }
      ],
      groups: {
        productOfferingBenefit: [
          {
            name: 'discount',
            label: 'Magenta Discount',
            value: 'magentaDiscount',
            isSelected: false
          },
          {
            name: 'discount',
            label: 'Family Discount',
            value: 'familyDiscount',
            isSelected: false
          }
        ],
        productOfferingTerm: [
          {
            name: 'selectedProductOfferingTerm',
            label: '12 Months Loyality',
            value: 'agreement12',
            isSelected: true
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '24 Months Loyality',
            value: 'agreement24',
            isSelected: false
          },
          {
            name: 'selectedProductOfferingTerm',
            label: '36 Months Loyality',
            value: 'agreement36',
            isSelected: false
          }
        ]
      },
      addons: [
        {
          id: 'facebook',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        },
        {
          id: 'extra_data_20_gb',
          categoryId: '',
          prices: [
            {
              priceType: 'recurringFee',
              actualValue: 100,
              discountedValue: 88,
              discounts: [],
              recurringChargePeriod: 'month',
              recurringChargeOccurrence: -1
            }
          ]
        }
      ]
    }
  ],
  totalPrices: [],
  linkedCategory: ''
};

export const productSpecificationResponse: GET_PRODUCT_SPECIFICATION.IResponse = [
  {
    name: '',
    values: [{ value: '', unit: '' }],
    isCustomerVisible: true
  },
  {
    name: 'highlightProductDetailedSection',
    values: [{ value: '', unit: '' }],
    isCustomerVisible: true
  },
  {
    name: 'productExtraFeature',
    values: [{ value: '', unit: '' }],
    isCustomerVisible: true
  },
  {
    name: 'marketingContent',
    values: [{ value: '', unit: '' }],
    isCustomerVisible: true
  },
  {
    name: 'metaTagTitle',
    values: [{ value: '', unit: '' }],
    isCustomerVisible: true
  },
  {
    name: 'metaTagDescription',
    values: [{ value: '', unit: '' }],
    isCustomerVisible: true
  }
];

export const notificationData: INotificationData = {
  variantId: '12',
  deviceName: 'PHONE',
  mediumValue: 'phone value',
  reason: 'some reson',
  type: NOTIFICATION_MEDIUM.PHONE
};

export const productDetailsSuccessResponse: IProductDetailResponse = {
  productDetailsData: productDetailsResponse,
  selectedAttributes: [
    {
      name: 'name',
      value: 'value'
    }
  ],
  installmentGroup: [
    {
      id: 1,
      title: 'title',
      label: 'label',
      amount: null,
      active: false,
      amountType: 'amount',
      monthlySymbol: null,
      upfrontAmount: '12',
      enabled: false
    }
  ],
  tariffGroup: [],
  instalmentId: null,
  tariffId: null,
  agreementId: null,
  buyDeviceOnly: false,
  buyWithoutInstallments: false
};

export const installmentData: IInstallmentData[] = [
  {
    id: 1,
    title: 'title',
    label: 'label',
    amount: null,
    active: false,
    amountType: 'amount',
    monthlySymbol: null,
    upfrontAmount: '12',
    enabled: false
  }
];
