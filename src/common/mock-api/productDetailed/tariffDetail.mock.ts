import { ITariffDetail } from '@productDetailed/store/types';


export const tariffDetail: ITariffDetail[] = [
  {
    id: '12',
    title: 'title',
    value: 'value',
    label: 'value',
    description: 'description',
    agreements: [{
      id: '2',
      title: 'title',
      active: false
    }],
    amount: '12',
    active: false,
    monthlySymbol: '$',
    characteristics: [
      {
        name: 'change',
        values: [{
          value: 'value',
          label: 'label',
          valueFrom: 'valueFrom',
          valueTo: 'valueTo'
        }]
      }
    ]
  }
];
