import {
  IFetchProlongationProductListPayload,
  IProlongationPlaceOrderRequest,
  IProlongationCheckoutRequestPayload,
  IProlongationProductListResponse,
  IGetLead
} from '@productList/store/types';

export const FetchProlongationProductList: IFetchProlongationProductListPayload = {
  uuid: 'uuid',
  channel: 'us',
  category: 'mobile',
  segment: '',
  productId: 'black',
  profileId: '',
  variantId: 'mobile',
  checkHistoricalLead: false,
  changedAttributes: [
    {
      name: 'string',
      value: 'string'
    }
  ],
  selectedAttributes: [
    {
      name: 'string',
      value: 'string'
    }
  ],
  requestedAttributes: ['string', 'string']
};

export const ProlongationPlaceOrderRequest: IProlongationPlaceOrderRequest = {
  data: '',
  enc_key: '',
  rsa_key_label: '',
  uuid: '',
  profileId: '',
  category_id: '',
  segment_id: '',
  natco_code: ''
};

export const ProlongationCheckoutRequestPayload: IProlongationCheckoutRequestPayload = {
  uuid: 'uuid',
  channel: 'us',
  category: 'mobile',
  variantId: 'mobile',
  requestedAttributes: ['string', 'string'],
  marketingDiscount: true,
  profileId: 'f'
};

export const ProlongationProductListResponse: IProlongationProductListResponse = {
  category: 'Devices-Mobile',
  data: [
    {
      variantId: 'test_11_7',
      productId: 'test_11',
      name: 'Huawei P20 32GB Black',
      shortDescription: 'with 10GB Data & Unlimited Calling',
      description:
        '5.8 inches display/n\r\n4 GB RAM, 32 GB Memory/n\r\nOcta-core processor/n\r\nDual-lens camera/n\r\n10 GB Mobile internet data/n\r\nUnlimited calling',
      characteristics: [
        {
          name: 'storage',
          values: [{ value: '32_GB', label: '32 GB' }]
        },
        {
          name: 'colour',
          values: [{ value: 'Black', label: 'Black' }]
        },
        {
          name: 'plan',
          values: [
            {
              value: 'bestxl',
              label: '10 Gb Data Unlimited Calling (12 Months)'
            }
          ]
        },
        {
          name: 'segment',
          values: [
            { value: 'segment1', label: 'segment1' },
            { value: 'segment2', label: 'segment2' }
          ]
        },
        {
          name: 'popularityIndex',
          values: [{ value: 'po', label: '0' }]
        },
        { name: 'availabilityStatus', values: [{ value: 'IN_STOCK' }] },
        { name: 'metaTagDescription', values: [{ value: 'sa' }] },
        { name: 'metaTagTitle', values: [{ value: '' }] }
      ],
      attachments: {
        thumbnail: [
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=53_f1_61bf92b315f9098c3cea2ef078aa.png',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=1e_5c_bae9e534fed25c0978151cca3223.jpeg',
            type: 'picture',
            name: 'back'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=5d_3d_dc502fff455a2592bee072db4a71.jpeg',
            type: 'picture',
            name: 'bottom'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=04_f0_f9620e692a0ba945c8c373cf8d40.jpeg',
            type: 'picture',
            name: 'top'
          }
        ]
      },
      prices: [
        {
          priceType: 'basePrice',
          actualValue: 40.0,
          discountedValue: 40.0,
          discounts: []
        },
        {
          priceType: 'upfrontPrice',
          actualValue: 40.0,
          discountedValue: 40.0,
          discounts: []
        },
        {
          priceType: 'recurringFee',
          actualValue: 30.0,
          discountedValue: 30.0,
          discounts: []
        }
      ],
      groups: {
        colour: [
          { name: 'colour', label: 'Black', value: 'Black', enabled: true },
          { name: 'colour', label: 'Blue', value: 'Blue', enabled: true }
        ],
        storage: [
          { name: 'storage', label: '32 GB', value: '32_GB', enabled: true },
          { name: 'storage', label: '128 GB', value: '128_GB', enabled: true },
          { name: 'storage', label: '16 GB', value: '16_GB', enabled: true }
        ],
        plan: [
          {
            name: 'plan',
            label: '10 Gb Data Unlimited Calling (12 Months)',
            value: 'bestxl',
            enabled: true
          },
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Calling (24 Months)',
            value: 'bests',
            enabled: true
          },
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Calling (36 Months)',
            value: 'bestl',
            enabled: true
          },
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Calling (12 Months)',
            value: 'bestm',
            enabled: true
          }
        ]
      },
      optionValues: [
        '32 GB',
        'Black',
        '10 Gb Data Unlimited Calling (12 Months)'
      ]
    },
    {
      variantId: 'samsung22_v2',
      productId: 'samsung_22',
      name: 'Samsung A50',
      shortDescription: 'with 5GB Data & Unlimited Calling',
      description:
        '64 GB storage, 4 GB RAM/n\r\nWhite, triple camera/n\r\nOcta-core processor/n\r\n5GB Data, 500 SMS/n\r\nUnlimited local and roaming calling/n\r\n12 months loyalty',
      characteristics: [
        {
          name: 'storage',
          values: [{ value: '32_GB', label: '32 GB' }]
        },
        {
          name: 'colour',
          values: [{ value: 'White', label: 'White' }]
        },
        {
          name: 'plan',
          values: [
            { value: 'bestm', label: '5 Gb Data Unlimited Talk (12 Months)' }
          ]
        },
        {
          name: 'segment',
          values: [
            { value: 'segment1', label: 'segment1' },
            { value: 'segment2', label: 'segment2' },
            { value: 'segment3', label: 'segment3' }
          ]
        },
        {
          name: 'popularityIndex',
          values: [{ value: 'ppp', label: '1' }]
        },
        { name: 'availabilityStatus', values: [{ value: 'OUT_OF_STOCK' }] },
        { name: 'metaTagDescription', values: [{ value: '' }] },
        { name: 'metaTagTitle', values: [{ value: '' }] }
      ],
      attachments: {
        thumbnail: [
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=6c_7c_6db621f54d31ee176873842ddb0c.png',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=2f_26_50cd42b7c018aaee7521a49b49dc.jpeg',
            type: 'picture',
            name: 'back'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=1d_8b_d89d42c8e8719d5904000b34e8cd.jpeg',
            type: 'picture',
            name: 'left'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b2_06_f802dcdd2cb312159202576c490a.jpeg',
            type: 'picture',
            name: 'top'
          }
        ]
      },
      prices: [
        {
          priceType: 'basePrice',
          actualValue: 3500.0,
          discountedValue: 3500.0,
          discounts: [],
          recurringChargeOccurrence: 0
        },
        {
          priceType: 'upfrontPrice',
          actualValue: 123456.0,
          discountedValue: 123456.0,

          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: 0
        },
        {
          priceType: 'recurringFee',
          actualValue: 25.454,
          discountedValue: 25.454,
          discounts: [],
          recurringChargePeriod: 'month',
          recurringChargeOccurrence: 0
        }
      ],
      groups: {
        colour: [
          { name: 'colour', label: 'White', value: 'White', enabled: true },
          { name: 'colour', label: 'Black', value: 'Black', enabled: false }
        ],
        storage: [
          { name: 'storage', label: '32 GB', value: '32_GB', enabled: true },
          { name: 'storage', label: '128 GB', value: '128_GB', enabled: true }
        ],
        plan: [
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Talk (12 Months)',
            value: 'bestm',
            enabled: true
          },
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Talk (24 Months)',
            value: 'bests',
            enabled: false
          }
        ]
      },
      optionValues: ['32 GB', 'White', '5 Gb Data Unlimited Talk (12 Months)']
    },
    {
      variantId: 'google_pixel3_v1',
      productId: 'google_pixel_3',
      name: 'Google Pixel',
      shortDescription: '',
      description: '',
      characteristics: [
        {
          name: 'storage',
          values: [{ value: '64_GB', label: '64 GB' }]
        },
        {
          name: 'colour',
          values: [{ value: 'Black', label: 'Black' }]
        },
        {
          name: 'plan',
          values: [
            { value: 'bestm', label: '5 Gb Data Unlimited Calling (12 Months)' }
          ]
        },
        {
          name: 'segment',
          values: [
            { value: 'segment1', label: 'segment1' },
            { value: 'segment2', label: 'segment2' },
            { value: 'segment3', label: 'segment3' }
          ]
        },
        { name: 'availabilityStatus', values: [{ value: 'IN_STOCK' }] },
        { name: 'metaTagDescription', values: [{ value: '' }] },
        { name: 'metaTagTitle', values: [{ value: '' }] }
      ],
      attachments: {},
      prices: [
        {
          priceType: 'basePrice',
          actualValue: 3700.0,
          discountedValue: 3700.0,
          discounts: [],
          recurringChargeOccurrence: 0
        },
        {
          priceType: 'upfrontPrice',
          actualValue: 1900.0,
          discountedValue: 1900.0,
          discounts: [],
          recurringChargeOccurrence: 0
        },
        {
          priceType: 'recurringFee',
          actualValue: 150.0,
          discountedValue: 150.0,
          discounts: [],
          recurringChargeOccurrence: 0
        }
      ],
      groups: {
        colour: [
          { name: 'colour', label: 'Black', value: 'Black', enabled: true }
        ],
        storage: [
          { name: 'storage', label: '64 GB', value: '64_GB', enabled: true }
        ],
        plan: [
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Calling (12 Months)',
            value: 'bestm',
            enabled: true
          },
          {
            name: 'plan',
            label: '5 Gb Data Unlimited Calling (24 Months)',
            value: 'bests',
            enabled: true
          },
          {
            name: 'plan',
            label: '10 Gb Data Unlimited Calling (12 Months)',
            value: 'bestxl',
            enabled: true
          },
          {
            name: 'plan',
            label: '10 Gb Data Unlimited Calling (24 Months)',
            value: '10_gb_unlimited',
            enabled: true
          }
        ]
      },
      optionValues: [
        '64 GB',
        'Black',
        '5 Gb Data Unlimited Calling (12 Months)'
      ]
    }
  ]
};

export const GetLeadResponse: IGetLead = {
  magenta: {
    type: 'A2A',
    bundles: ['c1e1d300-c2b9-46f5-8853-8bae9aa63b2e'],
    partyId: ['5fba0646-f499-46b9-862e-6a09f2e39b41']
  },
  digitalBanking: {
    id: '183c3f62-1d80-44f5-a329-9c9aa067f0fe',
    label: 'Your Digital Bank',
    name: 'Digital bank'
  },
  userProfileRelatedPartyId: '5fba0646-f499-46b9-862e-6a09f2e39b41',
  id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
  status: 'validated',
  detailedStatus: 'validated',
  individual: {
    gender: 'male',
    birthDate: '1980-12-22',
    givenName: 'John',
    familyName: 'Smith'
  },
  characteristics: [
    {
      name: 'telekom_username',
      value: 'jsmith'
    },
    {
      name: 'engagementCardName',
      value: 'John Smith'
    },
    {
      name: 'engagementCardId',
      value: '1111 2222 3333 4444'
    }
  ],
  relatedParties: [
    {
      id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      role: 'customer',
      name: 'John Smith'
    }
  ],
  contactMediums: [
    {
      type: 'email',
      role: {
        name: 'contact'
      },
      medium: {
        emailAddress: 'jsmith@natco.com',
        number: '+3851444444'
      }
    },
    {
      type: 'phone',
      role: {
        name: 'contact'
      },
      medium: {
        number: '+3851444444',
        emailAddress: 'jsmith@natco.com'
      }
    },
    {
      type: 'mobile',
      role: {
        name: 'contact'
      },
      medium: {
        number: '+385981234567',
        emailAddress: 'jsmith@natco.com'
      }
    }
  ],
  manageableAssets: [],
  bundles: [
    {
      id: 'c1e1d300-c2b9-46f5-8853-8bae9aa63b2e',
      privilege: 'user',
      name: 'Magenta 1',
      label: 'Magenta 1',
      description: 'A2A Magenta Bundle Testing',
      type: 'magenta1',
      enabled: true
    },
    {
      id: '17e0b3fb-1cc9-4c77-98e7-68a97608e440',
      privilege: 'user',
      name: 'Family package,',
      label: 'Family package',
      type: 'normal',
      description: 'A2A Magenta Bundle Testing',
      enabled: true
    }
  ]
};

export const ProlongationCheckoutProduct = {
  name: '',
  optionValues: ['32 GB', 'White', '5 Gb Data Unlimited Talk (12 Months)'],

  attachments: {
    thumbnail: [
      {
        url:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=53_f1_61bf92b315f9098c3cea2ef078aa.png',
        type: 'picture',
        name: 'front'
      },
      {
        url:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=1e_5c_bae9e534fed25c0978151cca3223.jpeg',
        type: 'picture',
        name: 'back'
      },
      {
        url:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=5d_3d_dc502fff455a2592bee072db4a71.jpeg',
        type: 'picture',
        name: 'bottom'
      },
      {
        url:
          'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=04_f0_f9620e692a0ba945c8c373cf8d40.jpeg',
        type: 'picture',
        name: 'top'
      }
    ]
  },
  characteristics: [],
  prices: [
    {
      priceType: 'basePrice',
      actualValue: 40.0,
      discountedValue: 40.0,
      discounts: [],
      currency: 'PLN'
    },
    {
      priceType: 'upfrontPrice',
      actualValue: 40.0,
      discountedValue: 40.0,
      discounts: [],
      currency: 'PLN'
    },
    {
      priceType: 'recurringFee',
      actualValue: 30.0,
      discountedValue: 30.0,
      discounts: [],
      currency: 'PLN'
    }
  ]
};
