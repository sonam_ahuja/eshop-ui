import {
  IFilter,
  ISelectedFilters,
  ISortBy,
  IFilterCharacteristic,
  ICharacteristics,
  INotificationData,
  IGetNotificationRequestTemplatePayload,
  IFetchProductListParams,
  IError,
  IProductListItem,
  IProductListResponse,
  IVariant
} from '@productList/store/types';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';
import APP_KEYS from '@common/constants/appkeys';
import { ITEM_STATUS } from '@src/common/store/enums';

export const PopularDevices: IVariant[] = [
  {
    id: '1',
    productId: '123',
    name: 'name',
    productName: 'productName',
    description: 'description',
    group: 'group',
    bundle: false,
    characteristics: [
      {
        key: 'key',
        value: 'key'
      }
    ],
    prices: [
      {
        priceType: 'basePrice',
        actualValue: 300.0,
        discountedValue: 300.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'upfrontPayment',
        actualValue: 150.0,
        discountedValue: 160.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'recurringFee',
        actualValue: 200.0,
        discountedValue: 180.0,
        recurringChargePeriod: 'min',
        discounts: []
      }
    ],
    attachments: {
      attachments: [
        {
          name: 'key',
          url: 'value'
        }
      ]
    },
    unavailabilityReasonCodes: [ITEM_STATUS.OUT_OF_STOCK]
  }
];
export const Filter: IFilter[] = [
  {
    name: 'name1',
    default: false,
    value: 'value1',
    quantity: 1
  },
  {
    name: 'name2',
    default: true,
    value: 'value2',
    quantity: 1
  },
  {
    name: 'name3',
    default: true,
    value: 'value3'
  }
];

export const SelectedFilters: ISelectedFilters['key'] = [
  {
    name: 'name1',
    default: false,
    value: 'value1',
    quantity: 1
  },
  {
    name: 'name2',
    default: true,
    value: 'value2',
    quantity: 1
  },
  {
    name: 'name3',
    default: true,
    value: 'value3'
  }
];

export const SelectedFilter: ISelectedFilters = {
  default: [
    {
      name: 'name1',
      default: false,
      value: 'value1',
      quantity: 1
    },
    {
      name: 'name2',
      default: true,
      value: 'value2',
      quantity: 1
    },
    {
      name: 'name3',
      default: true,
      value: 'value3'
    }
  ]
};

export const SortedBy: ISortBy = {
  id: '1',
  title: 'title'
};

export const SortByList: ISortBy[] = [
  {
    id: '1',
    title: 'title1'
  },
  {
    id: '2',
    title: 'title2'
  }
];

export const FilterCharacteristics: IFilterCharacteristic[] = [
  {
    id: '1',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '2',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '3',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '4',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '5',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '6',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '6',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '7',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '8',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  },
  {
    id: '9',
    name: 'name',
    type: APP_KEYS.BADGE,
    data: Filter
  }
];

export const Characteristics: ICharacteristics[] = [
  {
    name: 'name',
    values: [
      {
        value: 'value'
      }
    ]
  },
  {
    name: 'background',
    values: [
      {
        value: 'https://i.ibb.co/mHxM497/Untitled-1.jpg'
      }
    ]
  },
  {
    name: 'theme',
    values: [
      {
        value: 'primary'
      }
    ]
  }
];

export const NotificationData: INotificationData = {
  variantId: '123',
  deviceName: 'Samsung',
  mediumValue: 'phone',
  type: NOTIFICATION_MEDIUM.PHONE
};

export const SendStockPayload: IGetNotificationRequestTemplatePayload = {
  type: NOTIFICATION_MEDIUM.EMAIL,
  variantId: '123',
  mediumValue: '12'
};

export const CustomErrorData: IError = {
  code: 200,
  message: 'message'
};

export const ErrorData: Error = {
  name: 'error',
  stack: 'stack',
  message: 'message'
};

export const FetchProductListParams: IFetchProductListParams = {
  currentPage: 2,
  categoryId: '123'
};

export const ProductListItem: IProductListItem = {
  category: 'phone',
  variant: {
    id: '123',
    productName: '',
    productId: '1234',
    name: 'iphone',
    description: 'phone',
    group: 'phone',
    bundle: true,
    characteristics: [
      {
        key: 'key',
        value: 'value'
      }
    ],
    prices: [
      {
        priceType: 'basePrice',
        actualValue: 300.0,
        discountedValue: 300.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'upfrontPayment',
        actualValue: 150.0,
        discountedValue: 160.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'recurringFee',
        actualValue: 200.0,
        discountedValue: 180.0,
        recurringChargePeriod: 'min',
        discounts: []
      }
    ],
    attachments: {
      attachments: [
        {
          name: 'key',
          url: 'value'
        }
      ]
    },
    unavailabilityReasonCodes: [ITEM_STATUS.OUT_OF_STOCK]
  },
  groups: {
    attachments: {
      key: 'key',
      value: 'value'
    }
  }
};

export const ProductListItemNoOutOfStockReason: IProductListItem = {
  category: 'phone',
  variant: {
    id: '123',
    productName: '',
    productId: '1234',
    name: 'iphone',
    description: 'phone',
    group: 'phone',
    bundle: true,
    characteristics: [
      {
        key: 'key',
        value: 'value'
      }
    ],
    prices: [
      {
        priceType: 'basePrice',
        actualValue: 300.0,
        discountedValue: 300.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'upfrontPayment',
        actualValue: 150.0,
        discountedValue: 160.0,
        recurringChargePeriod: 'min',
        discounts: []
      },
      {
        priceType: 'recurringFee',
        actualValue: 200.0,
        discountedValue: 180.0,
        recurringChargePeriod: 'min',
        discounts: []
      }
    ],
    attachments: {
      attachments: [
        {
          name: 'key',
          url: 'value'
        }
      ]
    },
    unavailabilityReasonCodes: []
  },
  groups: {
    attachments: {
      key: 'key',
      value: 'value'
    }
  }
};

export const ProductListResponse: IProductListResponse = {
  filterCharacteristics: FilterCharacteristics,
  characteristics: Characteristics,
  pageNumber: 3,
  itemsPerPage: 12,
  categoryId: '',
  categoryName: '',
  resultCount: 3,
  data: [],
  plan: {
    id: '',
    productId: '',
    productName: '',
    name: '',
    description: '',
    group: '',
    bundle: false,
    prices: [],
    attachments: {},
    characteristics: [],
    unavailabilityReasonCodes: []
  },
  installmentEnabled: false,
  loyality: '12',
  installment: '24'
};

export const ProductListResponse2: IProductListResponse = {
  filterCharacteristics: FilterCharacteristics,
  characteristics: Characteristics,
  pageNumber: 3,
  itemsPerPage: 12,
  categoryId: '',
  categoryName: '',
  resultCount: 3,
  data: [
    {
      variant: {
        id: 'nokia_v_4',
        name: 'Nokia 5.1 Plus 16 Blue',
        description: '',
        group: '',
        productId: 'nokia',
        productName: 'Nokia 5.1 Plus',
        unavailabilityReasonCodes: [ITEM_STATUS.OUT_OF_STOCK],
        characteristics: [],
        attachments: {
          thumbnail: [
            {
              url:
                'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=26_c2_bec9f5d9c2ed9c732e5ff9c65d45.jpeg',
              name: 'front'
            },
            {
              url:
                'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=a3_22_13cef7989371ebb4f52fcaa14504.jpeg',
              name: 'frontAndBack'
            }
          ]
        },
        prices: [
          {
            priceType: 'basePrice',
            actualValue: 400,
            discountedValue: 280,
            discounts: []
          },
          {
            priceType: 'upfrontPrice',
            actualValue: 300,
            discountedValue: 120,
            discounts: []
          },
          {
            priceType: 'recurringFee',
            actualValue: 1400,
            discountedValue: 1230,
            discounts: [],
            recurringChargeOccurrence: 10
          }
        ],
        bundle: false
      },
      category: '',
      groups: {}
    }
  ],
  plan: {
    id: '',
    productId: '',
    productName: '',
    name: '',
    description: '',
    group: '',
    bundle: false,
    prices: [],
    attachments: {},
    characteristics: [],
    unavailabilityReasonCodes: []
  },
  installmentEnabled: false,
  loyality: '12',
  installment: '24'
};
