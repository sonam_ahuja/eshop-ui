import { ISuggestionConverter } from '@search/store/transformer';

export const combineSuggestionResult: ISuggestionConverter[] = [
  {
    type: 'LOCAL',
    index: 1,
    filterType: null,
    activeText: null,
    fullText: 'samsung',
    inActiveText: 'samsung',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 2,
    filterType: null,
    activeText: null,
    fullText: 'mobiles',
    inActiveText: 'mobiles',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 3,
    filterType: null,
    activeText: null,
    fullText: 'laptops',
    inActiveText: 'laptops',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 4,
    filterType: null,
    activeText: null,
    fullText: 'televisions',
    inActiveText: 'televisions',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 5,
    filterType: null,
    activeText: null,
    fullText: 'sim',
    inActiveText: 'sim',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 6,
    filterType: null,
    activeText: null,
    fullText: 'accessories',
    inActiveText: 'accessories',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 7,
    filterType: null,
    activeText: null,
    fullText: 'tablets',
    inActiveText: 'tablets',
    activeIndex: false
  },
  {
    type: 'DEFAULT',
    index: 8,
    filterType: null,
    activeText: null,
    fullText: 'fixed services',
    inActiveText: 'fixed services',
    activeIndex: false
  },
  {
    type: 'LOCAL',
    index: 1,
    filterType: 'MATCH',
    activeText: 'samsung',
    fullText: 'samsung',
    inActiveText: 'samsung',
    activeIndex: false
  },
  {
    type: 'RESPONSE',
    index: 2,
    filterType: 'MODIFIED',
    activeText: 'samsung',
    fullText: 'Samsung 11- 32 GB Gold',
    inActiveText: ' 11- 32 GB Gold',
    activeIndex: false
  },
  {
    type: 'RESPONSE',
    index: 3,
    filterType: 'MODIFIED',
    activeText: 'samsung',
    fullText: 'Samsung 11- 32GB Black',
    inActiveText: ' 11- 32GB Black',
    activeIndex: false
  },
  {
    type: 'RESPONSE',
    index: 4,
    filterType: 'MODIFIED',
    activeText: 'samsung',
    fullText: 'Samsung 11- 32GB Black11',
    inActiveText: ' 11- 32GB Black11',
    activeIndex: false
  },
  {
    type: 'RESPONSE',
    index: 5,
    filterType: 'MODIFIED',
    activeText: 'samsung',
    fullText: 'Samsung 12 - 128 GB Black',
    inActiveText: ' 12 - 128 GB Black',
    activeIndex: false
  },
  {
    type: 'RESPONSE',
    index: 6,
    filterType: 'MODIFIED',
    activeText: 'samsung',
    fullText: 'Samsung 12 - Black  32GB',
    inActiveText: ' 12 - Black  32GB',
    activeIndex: false
  }
];
