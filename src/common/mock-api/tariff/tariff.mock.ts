import { PRICE_TYPE, RECURRING_TYPE } from '@tariff/store/enum';
import * as tariffApi from '@common/types/api/tariff';
import { ITariffDetail, ITariff } from '@productDetailed/store/types';
import { IPrices, IPlans, ITariffState } from '@tariff/store/types';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { IProps } from '@src/common/routes/tariff/index';

export const tariffResponse: tariffApi.GET.IResponse = {
  tariffs: [
    {
      id: 'bests',
      name: 'BestS',
      description: '',
      allBenefits: [],
      totalPrices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 200,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      group: '',
      isSelected: true,
      prices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 200,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      benefits: [
        {
          label: 'Free App',
          name: 'freeApp',
          isSelectable: true,

          values: [
            {
              unit: null,
              value: 'Netflix',
              label: 'Netflix',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              isSelected: true,
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Data',
          name: 'addon id not translatable',
          isSelectable: true,
          values: [
            {
              unit: 'MiB',
              value: '750',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              isSelected: true,
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,

          values: [
            {
              unit: 'GiB',
              value: '10',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        }
      ],
      commonBenefits: [
        {
          label: 'Unlimited Calls & sms',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              value: '-1',
              attachments: {}
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              value: '10',
              attachments: {}
            }
          ]
        }
      ],
      characteristics: [
        {
          name: 'bestDeviceOffers',
          values: [
            {
              value: 'true'
            }
          ]
        },
        {
          name: 'birthDate',
          values: [
            {
              valueTo: 'date',
              valueFrom: 'date'
            }
          ]
        }
      ]
    },
    {
      name: 'BestM',
      description: '',
      allBenefits: [],
      totalPrices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 200,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      id: 'bests',
      group: '',
      isSelected: true,
      prices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 300,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      benefits: [
        {
          label: 'Free App',
          name: 'freeApp',
          isSelectable: true,

          values: [
            {
              unit: null,
              value: 'Netflix',
              label: 'Netflix',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              isSelected: true,
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Data',
          name: 'addon id not translatable',
          isSelectable: true,

          values: [
            {
              unit: 'MiB',
              value: '750',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              isSelected: true,
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              value: '10',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        }
      ],
      commonBenefits: [
        {
          label: 'Unlimited Calls & sms',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              value: '-1',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              value: '10',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        }
      ],
      characteristics: [
        {
          name: 'birthDate',
          values: [
            {
              valueTo: 'date',
              valueFrom: 'date'
            }
          ]
        }
      ]
    },
    {
      name: 'BestL',
      description: '',
      group: '',
      isSelected: true,
      allBenefits: [],
      totalPrices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 200,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      id: 'bests',
      prices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 400,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      benefits: [
        {
          label: 'Free App',
          name: 'freeApp',
          isSelectable: true,

          values: [
            {
              unit: null,
              value: 'Netflix',
              label: 'Netflix',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              isSelected: true,
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Data',
          name: 'addon id not translatable',
          isSelectable: true,

          values: [
            {
              unit: 'MiB',
              value: '750',
              isSelected: true,
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,

          values: [
            {
              unit: 'GiB',
              value: '10',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        }
      ],
      commonBenefits: [
        {
          label: 'Unlimited Calls & sms',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              value: '-1',
              attachments: {}
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              value: '10',
              attachments: {}
            }
          ]
        }
      ],
      characteristics: [
        {
          name: 'bestDeviceOffers',
          values: [
            {
              value: 'true'
            }
          ]
        },
        {
          name: 'birthDate',
          values: [
            {
              valueTo: 'date',
              valueFrom: 'date'
            }
          ]
        }
      ]
    },
    {
      name: 'BestXL',
      description: '',
      group: '',
      allBenefits: [],
      totalPrices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 200,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      isSelected: true,
      id: 'bests',
      prices: [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 200,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      benefits: [
        {
          label: 'Free App',
          name: 'freeApp',
          isSelectable: true,

          values: [
            {
              unit: null,
              value: 'Netflix',
              label: 'Netflix',
              isSelected: true,
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Data',
          name: 'addon id not translatable',

          isSelectable: true,
          values: [
            {
              unit: 'MiB',
              value: '750',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              isSelected: true,
              attachments: {
                thumbnail: [
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                    type: 'picture',
                    name: 'front'
                  },
                  {
                    url:
                      'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                    type: 'picture',
                    name: 'frontAndBack'
                  }
                ]
              }
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              value: '10',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        }
      ],
      commonBenefits: [
        {
          label: 'Unlimited Calls & sms',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              value: '-1',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              attachments: {}
            }
          ]
        },
        {
          label: 'Christmas Extra Data',
          name: 'christmasExtraData',
          isSelectable: false,
          values: [
            {
              unit: 'GiB',
              totalPrices: [],
              prices: [
                {
                  priceType: PRICE_TYPE.RECURRING,
                  actualValue: 200,
                  recurringChargePeriod: RECURRING_TYPE.MONTH,
                  discounts: [],
                  discountedValue: 200,
                  recurringChargeOccurrence: 1,
                  dutyFreeValue: 0,
                  taxRate: 1,
                  taxPercentage: 20
                }
              ],
              value: '10',
              attachments: {}
            }
          ]
        }
      ],
      characteristics: [
        {
          name: 'bestDeviceOffers',
          values: [
            {
              value: 'true'
            }
          ]
        },
        {
          name: 'birthDate',
          values: [
            {
              valueTo: 'date',
              valueFrom: 'date'
            }
          ]
        }
      ]
    }
  ],
  groups: {
    productOfferingTerm: [
      {
        label: '24 months loyalty',
        value: '24',
        isSelected: true
      },
      {
        label: '12 months loyalty',
        value: '12',
        isSelected: false
      },
      {
        label: 'no loyalty',
        value: '0',
        isSelected: false
      }
    ],
    productOfferingBenefit: [
      {
        label: 'Magenta Premium',
        value: 'magentaPremium',
        isSelected: true
      },
      {
        label: 'unfolded plans',
        value: 'unfolded plans',
        isSelected: false
      },
      {
        label: 'Magenta Discount',
        value: 'magenta_discount',
        isSelected: false
      },
      {
        label: 'Faimly Discount',
        value: 'faimly discount',
        isSelected: false
      }
    ]
  },
  category: '',
  linkedCategory: ''
};

export const tariffDetail: ITariffDetail[] = [
  {
    id: '12',
    title: 'title',
    value: 'value',
    label: 'value',
    description: 'description',
    agreements: [
      {
        id: '2',
        title: 'title',
        active: false
      }
    ],
    amount: '12',
    active: false,
    monthlySymbol: '$',
    characteristics: [
      {
        name: 'change',
        values: [
          {
            value: 'value',
            label: 'label',
            valueFrom: 'valueFrom',
            valueTo: 'valueTo'
          }
        ]
      }
    ]
  }
];

export const tariff: ITariff[] = [
  {
    id: '1',
    name: 'name',
    group: '',
    addons: [
      {
        id: 'addons',
        categoryId: '',
        prices: [
          {
            priceType: PRICE_TYPE.RECURRING,
            actualValue: 200,
            recurringChargePeriod: RECURRING_TYPE.MONTH,
            discounts: [],
            discountedValue: 200,
            recurringChargeOccurrence: 1,
            dutyFreeValue: 0,
            taxRate: 1,
            taxPercentage: 20
          }
        ]
      },
      {
        id: 'addons',
        categoryId: '',
        prices: [
          {
            priceType: PRICE_TYPE.RECURRING,
            actualValue: 200,
            recurringChargePeriod: RECURRING_TYPE.MONTH,
            discounts: [],
            discountedValue: 200,
            recurringChargeOccurrence: 1,
            dutyFreeValue: 0,
            taxRate: 1,
            taxPercentage: 20
          }
        ]
      }
    ],
    prices: [
      {
        priceType: PRICE_TYPE.RECURRING,
        actualValue: 200,
        recurringChargePeriod: RECURRING_TYPE.MONTH,
        discounts: [],
        discountedValue: 200,
        recurringChargeOccurrence: 1,
        dutyFreeValue: 0,
        taxRate: 1,
        taxPercentage: 20
      }
    ],
    totalPrices: [
      {
        priceType: PRICE_TYPE.RECURRING,
        actualValue: 200,
        recurringChargePeriod: RECURRING_TYPE.MONTH,
        discounts: [],
        discountedValue: 200,
        recurringChargeOccurrence: 1,
        dutyFreeValue: 0,
        taxRate: 1,
        taxPercentage: 20
      }
    ],
    groups: {
      productOfferingTerm: [
        {
          name: '24 months loyalty',
          label: '24 months loyalty',
          value: '24',
          isSelected: true
        },
        {
          name: '12 months loyalty',
          label: '12 months loyalty',
          value: '12',
          isSelected: false
        },
        {
          name: 'no loyalty',
          label: 'no loyalty',
          value: '0',
          isSelected: false
        }
      ],
      productOfferingBenefit: [
        {
          name: 'Magenta Premium',
          label: 'Magenta Premium',
          value: 'magentaPremium',
          isSelected: true
        },
        {
          name: 'unfolded plans',
          label: 'unfolded plans',
          value: 'unfolded plans',
          isSelected: false
        },
        {
          name: 'Magenta Discount',
          label: 'Magenta Discount',
          value: 'magenta_discount',
          isSelected: false
        },
        {
          name: 'Faimly Discount',
          label: 'Faimly Discount',
          value: 'faimly discount',
          isSelected: false
        }
      ]
    },
    characteristics: [
      {
        name: 'change',
        values: [
          {
            value: 'value',
            label: 'label',
            valueFrom: 'valueFrom',
            valueTo: 'valueTo'
          }
        ]
      }
    ],
    isSelected: false,
    description: 'description'
  }
];

export const totalPrices: IPrices[] = [
  {
    priceType: 'priceType',
    actualValue: 23,
    recurringChargePeriod: '12',
    discounts: [
      {
        discount: 23,
        name: 'name',
        label: '',
        dutyFreeValue: 34,
        taxRate: 45,
        taxPercentage: 3
      }
    ],
    discountedValue: 3,
    recurringChargeOccurrence: 34,
    dutyFreeValue: 34,
    taxRate: 4,
    taxPercentage: 3
  }
];

export const plan: IPlans = {
  name: 'name',
  id: 'id',
  group: '',
  totalPrices: [
    {
      priceType: PRICE_TYPE.RECURRING,
      actualValue: 200,
      recurringChargePeriod: RECURRING_TYPE.MONTH,
      discounts: [],
      discountedValue: 200,
      recurringChargeOccurrence: 1,
      dutyFreeValue: 0,
      taxRate: 1,
      taxPercentage: 20
    }
  ],
  prices: [
    {
      priceType: PRICE_TYPE.RECURRING,
      actualValue: 200,
      recurringChargePeriod: RECURRING_TYPE.MONTH,
      discounts: [],
      discountedValue: 200,
      recurringChargeOccurrence: 1,
      dutyFreeValue: 0,
      taxRate: 1,
      taxPercentage: 20
    }
  ],
  benefits: [
    {
      label: 'Free App',
      name: 'freeApp',
      isSelectable: true,

      values: [
        {
          unit: null,
          value: 'Netflix',
          label: 'Netflix',
          totalPrices: [],
          prices: [
            {
              priceType: PRICE_TYPE.RECURRING,
              actualValue: 200,
              recurringChargePeriod: RECURRING_TYPE.MONTH,
              discounts: [],
              discountedValue: 200,
              recurringChargeOccurrence: 1,
              dutyFreeValue: 0,
              taxRate: 1,
              taxPercentage: 20
            }
          ],
          isSelected: true,
          attachments: {
            thumbnail: [
              {
                url:
                  'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                type: 'picture',
                name: 'front'
              },
              {
                url:
                  'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                type: 'picture',
                name: 'frontAndBack'
              }
            ]
          }
        }
      ]
    },
    {
      label: 'Data',
      name: 'addon id not translatable',
      isSelectable: true,
      values: [
        {
          unit: 'MiB',
          value: '750',
          isSelected: true,
          totalPrices: [],
          prices: [
            {
              priceType: PRICE_TYPE.RECURRING,
              actualValue: 200,
              recurringChargePeriod: RECURRING_TYPE.MONTH,
              discounts: [],
              discountedValue: 200,
              recurringChargeOccurrence: 1,
              dutyFreeValue: 0,
              taxRate: 1,
              taxPercentage: 20
            }
          ],
          attachments: {
            thumbnail: [
              {
                url:
                  'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                type: 'picture',
                name: 'front'
              },
              {
                url:
                  'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                type: 'picture',
                name: 'frontAndBack'
              }
            ]
          }
        }
      ]
    },
    {
      label: 'Christmas Extra Data',
      name: 'christmasExtraData',
      isSelectable: false,
      values: [
        {
          unit: 'GiB',
          value: '10',
          totalPrices: [],
          prices: [
            {
              priceType: PRICE_TYPE.RECURRING,
              actualValue: 200,
              recurringChargePeriod: RECURRING_TYPE.MONTH,
              discounts: [],
              discountedValue: 200,
              recurringChargeOccurrence: 1,
              dutyFreeValue: 0,
              taxRate: 1,
              taxPercentage: 20
            }
          ],
          attachments: {}
        }
      ]
    }
  ],
  commonBenefits: [
    {
      label: 'Unlimited Calls & sms',
      name: 'christmasExtraData',
      isSelectable: false,

      values: [
        {
          unit: 'GiB',
          totalPrices: [],
          prices: [
            {
              priceType: PRICE_TYPE.RECURRING,
              actualValue: 200,
              recurringChargePeriod: RECURRING_TYPE.MONTH,
              discounts: [],
              discountedValue: 200,
              recurringChargeOccurrence: 1,
              dutyFreeValue: 0,
              taxRate: 1,
              taxPercentage: 20
            }
          ],
          value: '-1',
          attachments: {}
        }
      ]
    },
    {
      label: 'Christmas Extra Data',
      name: 'christmasExtraData',
      isSelectable: false,
      values: [
        {
          unit: 'GiB',
          value: '10',
          totalPrices: [],
          prices: [
            {
              priceType: PRICE_TYPE.RECURRING,
              actualValue: 200,
              recurringChargePeriod: RECURRING_TYPE.MONTH,
              discounts: [],
              discountedValue: 200,
              recurringChargeOccurrence: 1,
              dutyFreeValue: 0,
              taxRate: 1,
              taxPercentage: 20
            }
          ],
          attachments: {}
        }
      ]
    }
  ],
  characteristics: [
    {
      name: 'bestDeviceOffers',
      values: [
        {
          value: 'true'
        }
      ]
    },
    {
      name: 'birthDate',
      values: [
        {
          valueTo: 'date',
          valueFrom: 'date'
        }
      ]
    }
  ],
  allBenefits: [],
  isSelected: true
};

const returnNewTariff = (tariffState: ITariffState): ITariffState => {
  tariffState.plans = [plan];
  return tariffState;
};

export const plansProps: IPlansProps = {
  ...histroyParams,
  currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
  commonError: {
    code: '',
    retryable: false
  },
  confirmPlanBtnLoading: true,
  setMultiplePlansWrapRef: jest.fn(),
  tariff: returnNewTariff(appState().tariff),
  planSelectionOnly: false,
  tariffTranslation: appState().translation.cart.tariff,
  tariffConfiguration: appState().configuration.cms_configuration.modules
    .tariff,
  selectedProductOfferingTerm: {
    label: 'label',
    value: 'value',
    isSelected: true
  },
  globalTranslation: appState().translation.cart.global,
  categories: appState().categories.categories,
  selectedProductOfferingBenefit: {
    label: 'label',
    value: 'value',
    isSelected: true
  },
  fetchTariff: jest.fn(),
  fetchCategories: jest.fn(),
  setDiscount: jest.fn(),
  setLoyalty: jest.fn(),
  showMagentaDiscount: jest.fn(),
  showAppShell: jest.fn(),
  showTermAndConditions: jest.fn(),
  setTariffId: jest.fn(),
  buyPlanOnly: jest.fn(),
  saveTariff: jest.fn(),
  toggleSelectedBenefit: jest.fn(),
  setCategoryId: jest.fn(),
  resetPlansData: jest.fn(),
  redirectToListingPage: jest.fn(),
  addPlanToBasket: jest.fn(),
  validateAgeLimit: jest.fn(),
  youngTariffCheck: jest.fn(),
  clickOnAddToBasket: jest.fn(),
  getTariffAddonPrice: jest.fn(),
  clickOnBuyPlanOnly: jest.fn(),
  ageLimitSuccess: jest.fn(),
  setMobileAccordionPanelRef: jest.fn()
};

export const tarrifProps: IProps = {
  currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
  ...histroyParams,
  commonError: {
    code: '',
    retryable: false
  },
  confirmPlanBtnLoading: true,
  tariff: appState().tariff,
  tariffTranslation: appState().translation.cart.tariff,
  tariffConfiguration: appState().configuration.cms_configuration.modules
    .tariff,
  selectedProductOfferingTerm: {
    label: 'label',
    value: 'value',
    isSelected: true
  },
  globalTranslation: appState().translation.cart.global,
  categories: appState().categories.categories,
  selectedProductOfferingBenefit: {
    label: 'label',
    value: 'value',
    isSelected: true
  },
  fetchTariff: jest.fn(),
  fetchCategories: jest.fn(),
  setDiscount: jest.fn(),
  setLoyalty: jest.fn(),
  showMagentaDiscount: jest.fn(),
  showAppShell: jest.fn(),
  showTermAndConditions: jest.fn(),
  setTariffId: jest.fn(),
  buyPlanOnly: jest.fn(),
  saveTariff: jest.fn(),
  toggleSelectedBenefit: jest.fn(),
  setCategoryId: jest.fn(),
  resetPlansData: jest.fn(),
  getTariffAddonPrice: jest.fn(),
  validateAgeLimit: jest.fn(),
  youngTariffCheck: jest.fn(),
  clickOnAddToBasket: jest.fn(),
  clickOnBuyPlanOnly: jest.fn(),
  ageLimitSuccess: jest.fn()
};

export const planMock: IPlans[] = [
  {
    name: 'name',
    id: 'id',
    group: '',
    totalPrices: [],
    allBenefits: [],
    prices: [
      {
        priceType: PRICE_TYPE.RECURRING,
        actualValue: 200,
        recurringChargePeriod: RECURRING_TYPE.MONTH,
        discounts: [],
        discountedValue: 200,
        recurringChargeOccurrence: 1,
        dutyFreeValue: 0,
        taxRate: 1,
        taxPercentage: 20
      }
    ],
    benefits: [
      {
        label: 'Free App',
        name: 'freeApp',
        isSelectable: true,

        values: [
          {
            unit: null,
            value: 'Netflix',
            label: 'Netflix',
            isSelected: true,
            totalPrices: [],
            prices: [
              {
                priceType: PRICE_TYPE.RECURRING,
                actualValue: 200,
                recurringChargePeriod: RECURRING_TYPE.MONTH,
                discounts: [],
                discountedValue: 200,
                recurringChargeOccurrence: 1,
                dutyFreeValue: 0,
                taxRate: 1,
                taxPercentage: 20
              }
            ],
            attachments: {
              thumbnail: [
                {
                  url:
                    'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                  type: 'picture',
                  name: 'front'
                },
                {
                  url:
                    'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                  type: 'picture',
                  name: 'frontAndBack'
                }
              ]
            }
          }
        ]
      },
      {
        label: 'Data',
        name: 'addon id not translatable',
        isSelectable: true,

        values: [
          {
            unit: 'MiB',
            value: '750',
            totalPrices: [],
            prices: [
              {
                priceType: PRICE_TYPE.RECURRING,
                actualValue: 200,
                recurringChargePeriod: RECURRING_TYPE.MONTH,
                discounts: [],
                discountedValue: 200,
                recurringChargeOccurrence: 1,
                dutyFreeValue: 0,
                taxRate: 1,
                taxPercentage: 20
              }
            ],
            isSelected: true,
            attachments: {
              thumbnail: [
                {
                  url:
                    'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
                  type: 'picture',
                  name: 'front'
                },
                {
                  url:
                    'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
                  type: 'picture',
                  name: 'frontAndBack'
                }
              ]
            }
          }
        ]
      },
      {
        label: 'Christmas Extra Data',
        name: 'christmasExtraData',
        isSelectable: false,
        values: [
          {
            unit: 'GiB',
            value: '10',
            totalPrices: [],
            prices: [
              {
                priceType: PRICE_TYPE.RECURRING,
                actualValue: 200,
                recurringChargePeriod: RECURRING_TYPE.MONTH,
                discounts: [],
                discountedValue: 200,
                recurringChargeOccurrence: 1,
                dutyFreeValue: 0,
                taxRate: 1,
                taxPercentage: 20
              }
            ],
            attachments: {}
          }
        ]
      }
    ],
    commonBenefits: [
      {
        label: 'Unlimited Calls & sms',
        name: 'christmasExtraData',
        isSelectable: false,

        values: [
          {
            unit: 'GiB',
            totalPrices: [],
            prices: [
              {
                priceType: PRICE_TYPE.RECURRING,
                actualValue: 200,
                recurringChargePeriod: RECURRING_TYPE.MONTH,
                discounts: [],
                discountedValue: 200,
                recurringChargeOccurrence: 1,
                dutyFreeValue: 0,
                taxRate: 1,
                taxPercentage: 20
              }
            ],
            value: '-1',
            attachments: {}
          }
        ]
      },
      {
        label: 'Christmas Extra Data',
        name: 'christmasExtraData',
        isSelectable: false,
        values: [
          {
            unit: 'GiB',
            totalPrices: [],
            prices: [
              {
                priceType: PRICE_TYPE.RECURRING,
                actualValue: 200,
                recurringChargePeriod: RECURRING_TYPE.MONTH,
                discounts: [],
                discountedValue: 200,
                recurringChargeOccurrence: 1,
                dutyFreeValue: 0,
                taxRate: 1,
                taxPercentage: 20
              }
            ],
            value: '10',
            attachments: {}
          }
        ]
      }
    ],
    characteristics: [
      {
        name: 'bestDeviceOffers',
        values: [
          {
            value: 'true'
          }
        ]
      },
      {
        name: 'birthDate',
        values: [
          {
            valueTo: 'date',
            valueFrom: 'date'
          }
        ]
      }
    ],
    isSelected: true
  }
];
