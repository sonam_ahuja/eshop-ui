import { logSuccess } from '@utils/index';
import { getConfigurationState } from '@store/services/index';
// tslint:disable-next-line:no-any
declare const Paho: any;

export interface IOptions {
  userName: string;
  password: string;
  onSuccess(): void;
  onFailure(responseObject: object): void;
}

export interface IClient {
  onConnectionLost?(responseObject: object): void;
  onMessageDelivered?(): void;
  connect?(options: IOptions): void;
  send?(message: string): void;
  isConnected?(): boolean;
}

export interface IConfig {
  eventTopicName: string | null;
  retryHappened: number;
  mqttConnected: boolean;
  clientId: string;
}

let client: IClient | null = null;
// tslint:disable-next-line:no-any
let messageQueue: any[] = [];
const config: IConfig = {
  eventTopicName: null,
  retryHappened: 0,
  mqttConnected: false,
  clientId: ''
};
export const initiateConnection = () => {
  const configurationState = getConfigurationState();
  const wsbroker = configurationState.cms_configuration.global.mqtHost;
  const wsport = configurationState.cms_configuration.global.mqtPort;
  config.eventTopicName =
    configurationState.cms_configuration.global.eventTopicName;
  config.clientId = `js-utility-${makeid()}`;
  client = new Paho.Client(wsbroker, Number(wsport), '/ws', config.clientId);

  // tslint:disable-next-line:no-empty
  if (client) {
    client.onConnectionLost = (_responseObject: object): void => {
      logSuccess(_responseObject);
    };

    client.onMessageDelivered = (): void => {
      // tslint:disable-next-line:no-commented-code
      // logSuccess('message delivered');
    };
    MQTConnect();
  }
};

export const MQTConnect = (): void => {
  const configurationState = getConfigurationState();
  const wsbroker = configurationState.cms_configuration.global.mqtHost;
  const wsport = configurationState.cms_configuration.global.mqtPort;
  const vhost = configurationState.cms_configuration.global.mqtVhost;
  const options = {
    invocationContext: {
      host: wsbroker,
      port: wsport,
      path: '/ws',
      clientId: config.clientId
    },
    reconnect: true,
    useSSL: true,
    cleanSession: true,
    keepAliveInterval: 30,
    timeout: 3,
    onSuccess(): void {
      config.mqttConnected = true;
      logSuccess('Connected to Queue');
      if (messageQueue && Array.isArray(messageQueue) && messageQueue.length) {
        for (const iterator of messageQueue) {
          sendMessage(iterator);
        }
        messageQueue = [];
      }
    },
    userName: `${vhost}:${
      configurationState.cms_configuration.global.mqtUserName
    }`,
    password: configurationState.cms_configuration.global.mqtPassword,
    onFailure(_responseObject: object): void {
      config.mqttConnected = false;
    }
  };
  if (client && client.connect) {
    client.connect(options);
  }
};

export const sendMessage = (data: string): void => {
  if (client && client.isConnected && client.isConnected()) {
    const message = new Paho.Message(data);
    message.destinationName = config.eventTopicName;
    if (client.send) {
      client.send(message);
    }
  } else {
    if (messageQueue && Array.isArray(messageQueue)) {
      messageQueue.push(data);
    }
  }
};

function makeid(): string {
  let text = '';
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (let i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}
