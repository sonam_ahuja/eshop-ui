import React, { FunctionComponent } from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';
import SwitchRenderRoute from '@src/common/oneAppSwitchRenderRoute';

const loading: FunctionComponent<LoadingComponentProps> = () => null;

const ProductList = Loadable({
  loading,
  loader: () => import('./oneAppRoutes/productList')
});

const Checkout = Loadable({
  loading,
  loader: () => import('./oneAppRoutes/checkout')
});

const OrderConfirmation = Loadable({
  loading,
  loader: () => import('./oneAppRoutes/orderConfirmation')
});

const ProductDetailed = Loadable({
  loading,
  loader: () => import('./oneAppRoutes/productDetailed')
});

const NotFound = Loadable({
  loading,
  loader: () => import('./routes/notFound')
});

const routes: IRoutes[] = [
  {
    path: '/checkout',
    exact: true,
    component: Checkout
  },
  {
    path: '/product-list',
    basePath: '/product-list',
    exact: true,
    component: ProductList
  },
  {
    path: '/productDetailed',
    basePath: '/productDetailed',
    exact: true,
    component: ProductDetailed
  },
  {
    path: '/order-confirmation',
    basePath: '/order-confirmation',
    exact: true,
    component: OrderConfirmation
  },
  {
    path: '/not-found',
    basePath: '/not-found',
    exact: true,
    component: NotFound
  },
  {
    path: '',
    exact: true,
    component: SwitchRenderRoute
  }
];

export default routes;

export interface IRoutes {
  childRoutes?: IRoutes[];
  // headerFooterIncluded?: boolean;
  // mainHeaderFooterIncluded?: boolean;
  basePath?: string;
  path?: string;
  exact?: boolean;
  // tslint:disable-next-line:no-any
  component?: React.ComponentType<any>;
  loadData?(): Generator;
}
