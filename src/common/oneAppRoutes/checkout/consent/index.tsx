import React from 'react';
import { StyledAccordionWithCheckbox } from '@src/common/components/Accordion/AccordionWithCheckbox/styles';
import { Accordion, Checkbox, Title } from 'dt-components';
import { IProlongation } from '@src/common/store/types/translation';
import {
  IProlongationCheckoutDetails,
  ITransformedConsentProlongation,
  ITransformedSubConsent
} from '@src/common/routes/productList/store/types';

import { StyledConsent, StyledTnc } from './styles';

export interface IProps {
  className?: string;
  isConsentNeeded: boolean;
  translation: IProlongation;
  checkoutDetails: IProlongationCheckoutDetails;
  termsAndConditionClick(): void;
  toggleTnC(): void;
  innerAgreementToggle(outerIndex: number, innerIndex: number): void;
  mainAgreementToggle(data: number): void;
}

const Consent = (props: IProps) => {
  const {
    className,
    translation,
    termsAndConditionClick,
    toggleTnC,
    checkoutDetails,
    innerAgreementToggle,
    mainAgreementToggle,
    isConsentNeeded
  } = props;

  const {
    agreeOnTermsAndCondition,
    marketingConsentAgreement,
    isFormSubmitted
  } = checkoutDetails;
  const { agreementText, termsAndConditionText } = translation.checkout;

  const handleTnCToggle = () => {
    return () => {
      toggleTnC();
    };
  };

  const handleInnerToggle = (consentIndex: number, subConsentIndex: number) => {
    return () => {
      innerAgreementToggle(consentIndex, subConsentIndex);
    };
  };

  const handleOuterToggle = (consentIndex: number) => {
    return () => {
      mainAgreementToggle(consentIndex);
    };
  };

  return (
    <StyledConsent className={className}>
      {isConsentNeeded &&
        marketingConsentAgreement.map(
          (mainAgreement: ITransformedConsentProlongation, index: number) => {
            return (
              <StyledAccordionWithCheckbox key={index}>
                <Accordion
                  className={
                    mainAgreement.subConsent && mainAgreement.subConsent.length
                      ? ''
                      : 'empty'
                  }
                  isOpen={false}
                  headerTitle={
                    <>
                      <Checkbox
                        checked={mainAgreement.isSelected}
                        disabled={false}
                        name='checkbox'
                        focused={false}
                        onClick={handleOuterToggle(index)}
                        size='large'
                      />
                      <div
                        className='text'
                        dangerouslySetInnerHTML={{
                          __html: mainAgreement.description.trim()
                        }}
                      />
                    </>
                  }
                >
                  <div className='accordionBody'>
                    {mainAgreement.subConsent.map(
                      (
                        subConsent: ITransformedSubConsent,
                        subIndex: number
                      ) => {
                        return (
                          <div className='textWrapper' key={subIndex}>
                            <Checkbox
                              checked={subConsent.isSelected}
                              disabled={false}
                              name='checkbox'
                              focused={false}
                              onClick={handleInnerToggle(index, subIndex)}
                              size='large'
                            />
                            <div
                              className='text'
                              dangerouslySetInnerHTML={{
                                __html: subConsent.description.trim()
                              }}
                            />
                          </div>
                        );
                      }
                    )}
                  </div>
                </Accordion>
              </StyledAccordionWithCheckbox>
            );
          }
        )}

      <StyledTnc>
        <Checkbox
          checked={agreeOnTermsAndCondition}
          disabled={false}
          name='checkbox'
          focused={false}
          onClick={handleTnCToggle()}
          size='large'
          className={
            isFormSubmitted && !agreeOnTermsAndCondition ? 'error' : ''
          }
        />
        <Title size='xsmall' weight='bold'>
          {agreementText}{' '}
          <span className='link-text' onClick={termsAndConditionClick}>
            {termsAndConditionText}
          </span>
        </Title>
      </StyledTnc>
    </StyledConsent>
  );
};

export default Consent;
