import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledConsent = styled.div`
  padding: 1.5rem 1.25rem 1rem;

  .dt_accordion .accordionBody .textWrapper .dt_checkbox + .text {
    line-height: 1.2rem;
  }

  .dt_accordion .accordionHeader .accordionTitle {
    font-size: 1.125rem;

    .dt_checkbox label div {
      width: 24px;
      height: 24px;
      border-radius: 4px;
      i {
        font-size: 16px;
      }
    }
  }
  /* checkbox with content inside accordion body start */
  .dt_accordion .accordionBody .textWrapper .dt_checkbox label div {
    width: 20px !important;
    height: 20px !important;
    border-radius: 4px !important;
    i {
      font-size: 12px !important;
    }
  }
  /* checkbox with content inside accordion body end */

  .dt_accordion .accordionHeader .accordionTitle + .dt_icon {
    top: 1.35rem;
  }
  /* remove icon if accordion body in empty start */
  .dt_accordion.empty .accordionHeader .accordionTitle + .dt_icon {
    display: none;
  }
  /* remove icon if accordion body in empty end */
  .dt_accordion.empty .accordionHeader + div {
    display: none;
  }

  /* full with border start*/
  .dt_accordion {
    .accordionHeader:after {
      width: 100%;
    }
  }
  .dt_accordion.isOpen {
    .accordionHeader {
      padding-bottom: 1.25rem;
    }
  }
  /* full with border end*/
`;

export const StyledTnc = styled.div`
  display: flex;
  align-items: flex-start;
  padding-top: 1.25rem;
  margin-top: -1px;
  border-top: 1px solid ${colors.lightGray};

  .dt_title {
    font-size: 1.125rem;
    align-self: center;
    font-size: 1.125rem;
    color: ${colors.mediumGray};
    a {
      display: block;
      font-size: inherit;
    }
  }
  .dt_checkbox {
    margin-right: 1.25rem;

    label div {
      width: 24px;
      height: 24px;
      border-radius: 4px !important;
      i {
        font-size: 16px;
      }
    }
  }
  .link-text {
    color: ${colors.magenta};
    text-decoration: underline;
    font-weight: 600;
    cursor: pointer;
    &:hover {
      text-decoration: none;
    }
  }

  .error label div {
    border-color: #ff9a1e;
  }
`;
