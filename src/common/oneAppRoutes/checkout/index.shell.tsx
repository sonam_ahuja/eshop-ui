import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';

export const StyledCheckoutShell = styled(StyledShell)`
  padding: 2rem 1.25rem 0 1.25rem;
  height: 100vh;
    position: relative;

  .header{
    display: flex;
    align-items: center;
    margin-bottom: 3rem;
    .circle{
      height:2.5rem;
      width:2.5rem;
      border-radius: 50%;
      margin-right: 1rem;
    }
    .text{
      height:1.25rem;
      width: 8.75rem;
    }
  }

  .form{
    ul{
      display: block;

      li{
        display: block;
        height:1.5rem;
        width: 100%;
        margin-bottom: 2rem;
      }
    }
  }

  footer{
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;

    .tnc{
      height: 1rem;
      margin: 2rem;
    }

    .cta{
      display: flex;
      justify-content: space-between;
      li{
        width: 50%;
        height: 4rem;
      }
    }
  }
`;

const CheckoutShell = () => {
  return (
    <StyledCheckoutShell className='primary'>
      <h2 className='header'>
        <div className='circle shine' />
        <div className='text shine' />
      </h2>
      <form className='form'>
        <ul>
          <li className='shine' />
          <li className='shine' />
          <li className='shine' />
          <li className='shine' />
          <li className='shine' />
        </ul>
      </form>
      <footer>
        <p className='tnc shine' />
        <ul className='cta'>
          <li className='shine' />
          <li className='shine' />
        </ul>
      </footer>
    </StyledCheckoutShell>
  );
};

export default CheckoutShell;
