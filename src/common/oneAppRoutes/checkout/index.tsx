import { withRouter } from 'react-router-dom';
import React, { Component, ReactNode } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import AuxiliaryRoute from '@src/common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';
import { isBrowser } from '@src/common/utils';
import { IWindow } from '@src/client';

import { canPlaceOrderAsPerConsents } from '../utils/transformMarketingAgreement';

import {
  StyledCheckoutAppWrapper,
  StyledDialogBox,
  StyledOrderStickyWrap,
  StyledPersonalInformation
} from './styles';
import ProductInformationHeader from './personal-information/PersonalInformationHeader/index';
import ProductInformationForm from './personal-information/PersonalInformationForm/index';
import CheckoutShell from './index.shell';
import OrderPanelWrap from './order-panel';
import Consent from './consent';
import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IProps, IState } from './types';
import TncAppshell from './tncAppshell';

export class CheckoutComponent extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isIframeLoaded: false,
      isPersonalFormInEditForm: false,
      isTermsAndConditionOpened:
        this.props.location.hash ===
        `#${auxillaryRouteConfig.MOBILE_APP.CHECKOUT.TERMS_AND_CONDITION}`
    };
    this.onEditButtonClickHandler = this.onEditButtonClickHandler.bind(this);
    this.openTermsModal = this.openTermsModal.bind(this);
    this.closeTermsModal = this.closeTermsModal.bind(this);
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const { marketingConsentAgreement } = this.props.checkoutDetails;
    const {
      marketingConsentAgreement: nextPropsConsent
    } = nextProps.checkoutDetails;
    if (
      marketingConsentAgreement &&
      marketingConsentAgreement[0] &&
      nextPropsConsent &&
      nextPropsConsent[0] &&
      marketingConsentAgreement[0].isSelected !== nextPropsConsent[0].isSelected
    ) {
      this.props.callFetchProlongationCheckoutDetails();
    }

    if (
      this.props.location.hash !== nextProps.location.hash &&
      nextProps.location.hash ===
      `#${auxillaryRouteConfig.MOBILE_APP.CHECKOUT.TERMS_AND_CONDITION}` &&
      nextProps.history.action === 'POP'
    ) {
      this.setState({
        ...this.state,
        isTermsAndConditionOpened: true
      });
    }
  }

  componentDidMount(): void {
    if (
      isBrowser &&
      document &&
      document.body &&
      typeof document.body.scrollTo === 'function'
    ) {
      document.body.scrollTo(0, 0);
    }
    const vh = document.documentElement.clientHeight;
    this.setState({ viewportHeight: vh });

    const checkLocalStorageLead = localStorage.getItem('leadExist');
    if (
      isBrowser &&
      (window as IWindow).OneApp &&
      checkLocalStorageLead &&
      checkLocalStorageLead === 'yes'
    ) {
      (window as IWindow).OneApp.closeOneAppWebview();
    }
    this.props.fetchProlongationCheckoutDetails();
    this.props.getLead();
    this.props.getRSALeadKey();
  }

  onEditButtonClickHandler(): void {
    this.setState({
      isPersonalFormInEditForm: !this.state.isPersonalFormInEditForm
    });
  }

  closeTermsModal(): void {
    this.setState({
      isTermsAndConditionOpened: false,
      isIframeLoaded: false
    });
  }

  openTermsModal(): void {
    this.setState({
      isTermsAndConditionOpened: true
    });
  }

  render(): ReactNode {
    const {
      translation,
      checkoutDetails,
      innerAgreementToggle,
      mainAgreementToggle,
      checkProlongationFormValidation,
      formSubmitAction
    } = this.props;
    const {
      updateFormData,
      placeOrder,
      loading,
      orderSummaryEvent,
      toggleTnC
    } = this.props;
    const { showAppShell, configuration } = this.props;
    const pageTitle =
      configuration.cms_configuration.modules.prolongation
        .personalDetailsPageTitle;
    const {
      termsAndConditionsUrl
    } = configuration.cms_configuration.modules.prolongation;

    const {
      applyConsentDiscount
    } = configuration.cms_configuration.global.prolongation;

    const { isTermsAndConditionOpened } = this.state;

    return (
      <>
        <AuxiliaryRoute
          isChildrenRender={isTermsAndConditionOpened}
          hashPath={
            auxillaryRouteConfig.MOBILE_APP.CHECKOUT.TERMS_AND_CONDITION
          }
          isModal={true}
          onClose={this.closeTermsModal}
        >
          <StyledDialogBox
            isOpen={isTermsAndConditionOpened}
            closeOnBackdropClick={true}
            closeOnEscape={true}
            onEscape={this.closeTermsModal}
            onBackdropClick={this.closeTermsModal}
            backgroundScroll={false}
            type='fullHeight'
            onClose={this.closeTermsModal}
          >
            <div className='innerDiv'>
              {this.state.isIframeLoaded ? null : <TncAppshell />}
              <iframe
                src={termsAndConditionsUrl}
                onLoad={() => this.setState({ isIframeLoaded: true })} />
            </div>
          </StyledDialogBox>
        </AuxiliaryRoute>
        {showAppShell ? (
          <CheckoutShell />
        ) : (
            <StyledCheckoutAppWrapper viewportHeight={this.state.viewportHeight}>
              <Helmet title={pageTitle} meta={[]} />
              <StyledPersonalInformation>
                <ProductInformationHeader translation={translation.checkout} />
                <ProductInformationForm
                  userInfo={checkoutDetails.userInfo}
                  formDataValidation={checkoutDetails.formValidation}
                  translation={translation.checkout}
                  updateFormData={updateFormData}
                  prolongationConfig={
                    configuration.cms_configuration.modules.prolongation
                  }
                />
              </StyledPersonalInformation>
              <Consent
                translation={translation}
                termsAndConditionClick={this.openTermsModal}
                toggleTnC={toggleTnC}
                checkoutDetails={checkoutDetails}
                innerAgreementToggle={innerAgreementToggle}
                mainAgreementToggle={mainAgreementToggle}
                isConsentNeeded={
                  configuration.cms_configuration.global.prolongation
                    .applyConsentDiscount
                }
              />

              {/* <TermsCondition
              translation={translation.checkout}
              termsAndConditionClick={this.openTermsModal}
            /> */}
              <StyledOrderStickyWrap className='hideOnInputFocus'>
                <OrderPanelWrap
                  translation={translation.checkout}
                  configuration={configuration}
                  placeOrder={placeOrder}
                  loadingState={loading}
                  product={checkoutDetails.data}
                  termsAndConditionClick={this.openTermsModal}
                  orderSummaryEvent={orderSummaryEvent}
                  agreeOnTermsAndCondition={
                    checkoutDetails.agreeOnTermsAndCondition
                  }
                  discountApplied={
                    checkoutDetails.marketingConsentAgreement &&
                    checkoutDetails.marketingConsentAgreement[0] &&
                    checkoutDetails.marketingConsentAgreement[0].isSelected
                  }
                  globalTranslation={translation.global}
                  canPlaceOrderAsPerConsents={canPlaceOrderAsPerConsents(
                    checkoutDetails.marketingConsentAgreement,
                    applyConsentDiscount
                  )}
                  isConsentNeeded={applyConsentDiscount}
                  checkoutUserInfo={checkoutDetails.userInfo}
                  pppFormValidation={checkoutDetails.formValidation}
                  checkProlongationFormValidation={checkProlongationFormValidation}
                  formSubmitAction={formSubmitAction}
                />
              </StyledOrderStickyWrap>
            </StyledCheckoutAppWrapper>
          )}
      </>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CheckoutComponent)
);
