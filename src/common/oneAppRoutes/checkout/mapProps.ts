import { RootState } from '@common/store/reducers';
import actions from '@productList/store/actions';
import { Dispatch } from 'redux';
import { IFetchProlongationProductListPayload, IProlongationFormValidation } from '@productList/store/types';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  checkoutDetails: state.productList.prolongationCheckoutDetails,
  loading: state.productList.loading,
  showAppShell: state.productList.showAppShell,
  error: state.productList.error,
  translation: state.translation.prolongation,
  configuration: state.configuration

  // prolongationConfiguration:
  //   state.configuration.cms_configuration.modules.prolongation
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  fetchProductList(payload: IFetchProlongationProductListPayload): void {
    dispatch(actions.fetchProlongationProductList(payload));
  },
  fetchProlongationCheckoutDetails(): void {
    dispatch(actions.fetchProlongationCheckoutDetails(true));
  },
  updateFormData(field: string, value: string): void {
    dispatch(actions.updateFormField({ field, value }));
  },
  placeOrder(): void {
    dispatch(actions.placeOrder());
  },
  getLead(): void {
    dispatch(actions.getLead());
  },
  getRSALeadKey(): void {
    dispatch(actions.getRSALeadKey());
  },
  orderSummaryEvent(data: boolean): void {
    dispatch(actions.orderSummaryEvent(data));
  },
  toggleTnC(): void {
    dispatch(actions.toggleTermsAndCondition());
  },
  innerAgreementToggle(outerIndex: number, innerIndex: number): void {
    dispatch(actions.innerAgreementToggle({ outerIndex, innerIndex }));
  },
  mainAgreementToggle(data: number): void {
    dispatch(actions.mainAgreementToggle(data));
  },
  callFetchProlongationCheckoutDetails(): void {
    dispatch(actions.callProlongationCheckoutAction());
  },
  checkProlongationFormValidation(data: IProlongationFormValidation): void {
    dispatch(actions.checkProlongationFormValidation(data));
  },
  formSubmitAction(data: boolean): void {
    dispatch(actions.formSubmitAction(data));
  }
});
