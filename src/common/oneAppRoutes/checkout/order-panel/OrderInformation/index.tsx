import React, { Component, ReactNode } from 'react';
import { Anchor, Button, Checkbox, Title } from 'dt-components';
import { IProlongationCheckout } from '@common/store/types/translation';
import { IConfigurationState } from '@src/common/store/types/configuration';
import EVENT_NAME from '@events/constants/eventName';
import appConstants from '@src/common/constants/appConstants';

import { StyledOrderInformation, StyledTermsWrap } from './styles';

interface IProps {
  translation: IProlongationCheckout;
  configuration: IConfigurationState;
  deliveryAddress: boolean;
  isEmailValid: boolean;
  placeOrder(): void;
}

interface IState {
  isButtonDisable: boolean;
}
export class OrderInformation extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isButtonDisable: false
    };
    this.toggleCheckBox = this.toggleCheckBox.bind(this);
  }

  toggleCheckBox(): void {
    this.setState({
      isButtonDisable: !this.state.isButtonDisable
    });
  }

  render(): ReactNode {
    const { isButtonDisable } = this.state;
    const {
      translation,
      configuration,
      deliveryAddress,
      isEmailValid,
      placeOrder
    } = this.props;
    const termsAndConditionUrl =
      configuration.cms_configuration.global.termsAndConditionsUrl;

    return (
      <StyledOrderInformation>
        <StyledTermsWrap>
          <Checkbox
            checked={!isButtonDisable}
            name='checkbox'
            focused={false}
            size='large'
            className=''
            onClick={this.toggleCheckBox}
          />
          <Title size='xsmall' weight='bold'>
            {translation.agreementText}
            <Anchor
              size='medium'
              hreflang={appConstants.LANGUAGE_CODE}
              title={translation.termsAndConditionText}
              href={termsAndConditionUrl}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.TERMS_CONDITION}
              data-event-path={'prolongation.checkout.termsAndConditionText'}
              data-event-message={translation.termsAndConditionText}
            >
              <span>{translation.termsAndConditionText}</span>
            </Anchor>
          </Title>
        </StyledTermsWrap>
        <Button
          disabled={isButtonDisable || !deliveryAddress || !isEmailValid}
          onClickHandler={placeOrder}
        >
          {translation.placeOrder}
        </Button>
      </StyledOrderInformation>
    );
  }
}

export default OrderInformation;
