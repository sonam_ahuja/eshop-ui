import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledOrderInformation = styled.div`
  padding: 1.75rem 1.25rem 2rem;
  position: sticky;
  bottom: 0;
  background: ${colors.white};
  z-index: 1;
  .dt_button {
    width: 100%;
    .buttonText {
      /* text-transform: none; */
    }
  }
`;

export const StyledTermsWrap = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 1.75rem;
  .dt_title {
    padding-left: 1.25rem;
    a {
      color: ${colors.mediumGray};
      display: block;
      font-size: 1.125rem;
      line-height: 1.25rem;
      font-weight: bold;
      span {
        display: block;
        color: ${colors.magenta};
      }
    }
  }
`;
