import React from 'react';
import { IProlongationCheckout } from '@src/common/store/types/translation';

import { StyledTermsConditionWrap } from './styles';

interface IProps {
  translation: IProlongationCheckout;
  termsAndConditionClick(): void;
}

const TermsCondition = (props: IProps) => {
  const { translation, termsAndConditionClick } = props;

  return (
    <StyledTermsConditionWrap>
      <p className='text-wrap'>
        {translation.agreementText}{' '}
        <span className='link-text' onClick={termsAndConditionClick}>
          {' '}
          {translation.termsAndConditionText}{' '}
        </span>
      </p>
    </StyledTermsConditionWrap>
  );
};

export default TermsCondition;
