import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledTermsConditionWrap = styled.div`
  padding: 1.25rem 1.25rem 0;
  width: 100%;
  margin-bottom: 1.25rem;
  .text-wrap {
    font-size: 0.9375rem;
    line-height: 1.4;
    color: ${colors.charcoalGray};
    font-weight: 500;
  }
  .inline-wrap {
    display: inline-block;
  }
  .link-text {
    color: ${colors.magenta};
    text-decoration: underline;
    font-weight: 600;
    cursor: pointer;
    &:hover {
      text-decoration: none;
    }
  }
`;
