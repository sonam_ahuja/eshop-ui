import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';
import { getPriceAndCurrency } from '@src/common/routes/tariff/store/utils';
import { Button, Icon } from 'dt-components';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  IProlongationCheckout,
  IProlongationGlobalTranslation
} from '@src/common/store/types/translation';
import {
  IConfigurationState,
  ProlongationDiscountEnum
} from '@src/common/store/types/configuration';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import React, { Component, ReactNode } from 'react';
import { Column, Row } from '@src/common/components/Grid/styles';
import {
  IProlongationCheckoutProduct,
  IProlongationFormValidation,
  IUserInfo
} from '@src/common/routes/productList/store/types';
import { getImage } from '@src/common/routes/productList/utils/getProductDetail';
import htmlKeys from '@common/constants/appkeys';
import { getFormatedCurrencyValue } from '@src/common/utils/currency';
import { isFormValidFunction } from '@src/common/oneAppRoutes/utils/personalDetailForm';
import { getCurrencyCode } from '@store/common/index';

import { OrderDialogBoxApp } from '../../commonComponents/OrderDialogBoxApp/styles';
import ProductCardWithImageMonthlyNowPrice from '../product-card/ProductCardWithImageMonthlyNowPrice/index';
import {
  getMonthlyPrice,
  getUpfrontPrice
} from '../../productList/product-card';

import {
  StyledDiscountText,
  StyledOrderPanelWrap,
  StyledOrderPlaceWrap,
  StyledOrderSummaryWrap,
  StyledTitle
} from './styles';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

interface IProps extends RouteComponentProps<IRouterParams> {
  product: IProlongationCheckoutProduct[];
  translation: IProlongationCheckout;
  globalTranslation: IProlongationGlobalTranslation;
  configuration: IConfigurationState;
  loadingState: boolean;
  agreeOnTermsAndCondition: boolean;
  discountApplied: boolean;
  pppFormValidation: IProlongationFormValidation;
  canPlaceOrderAsPerConsents: boolean;
  isConsentNeeded: boolean;
  checkoutUserInfo: IUserInfo;
  placeOrder(): void;
  formSubmitAction(data: boolean): void;
  termsAndConditionClick(): void;
  orderSummaryEvent(data: boolean): void;
  checkProlongationFormValidation(data: IProlongationFormValidation): void;
}
export interface IState {
  isOpenSummaryOpened: boolean;
}

export class OrderPanelWrap extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isOpenSummaryOpened:
        this.props.location.hash ===
        `#${auxillaryRouteConfig.MOBILE_APP.CHECKOUT.ORDER_PANEL}`
    };
    this.closeSummaryPanel = this.closeSummaryPanel.bind(this);
    this.openSummaryPanel = this.openSummaryPanel.bind(this);
    this.getProductCard = this.getProductCard.bind(this);
    this.toogleSummaryPanel = this.toogleSummaryPanel.bind(this);
    this.sendEventOrderSummary = this.sendEventOrderSummary.bind(this);
    this.placeOrderCheck = this.placeOrderCheck.bind(this);
  }

  componentWillReceiveProps(nextProps: IProps): void {
    if (
      this.props.location.hash !== nextProps.location.hash &&
      nextProps.location.hash ===
      `#${auxillaryRouteConfig.MOBILE_APP.CHECKOUT.ORDER_PANEL}` &&
      nextProps.history.action === 'POP'
    ) {
      this.setState({
        ...this.state,
        isOpenSummaryOpened: true
      });
    }
  }

  closeSummaryPanel(): void {
    this.setState({ isOpenSummaryOpened: false });
  }

  openSummaryPanel(): void {
    this.setState({ isOpenSummaryOpened: true });
    this.props.orderSummaryEvent(true);
  }

  toogleSummaryPanel(): void {
    if (this.state.isOpenSummaryOpened) {
      this.closeSummaryPanel();
    } else {
      this.openSummaryPanel();
    }
  }

  sendEventOrderSummary(): void {
    this.props.orderSummaryEvent(false);
  }

  getProductCard(): ReactNode {
    const selectedProduct = this.props.product;
    if (Array.isArray(selectedProduct) && selectedProduct.length) {
      return selectedProduct.map((product, index) => {
        const actualUpfrontPrice = getUpfrontPrice(product.prices);
        const upfrontPrice = getFormatedCurrencyValue(actualUpfrontPrice);
        const monthlyPrice = getFormatedCurrencyValue(
          getMonthlyPrice(product.prices)
        );
        const currencyCode = getPriceAndCurrency(
          actualUpfrontPrice,
          getCurrencyCode()
        ).discountCurrency;
        const description = product.optionValues
          .filter(value => value)
          .toString()
          .replace(new RegExp(',', 'g'), ', ');

        return (
          <ProductCardWithImageMonthlyNowPrice
            key={index}
            title={product.name}
            deviceDescription={description}
            primaryMonthlyTitle={this.props.translation.totalMonthly}
            secondaryNowTitle={this.props.translation.totalNow}
            primaryMonthlyAmount={monthlyPrice}
            secondaryNowAmount={upfrontPrice}
            currencySymbol={currencyCode}
            imageUrl={getImage(
              product.attachments[htmlKeys.THUMBNAIL_IMAGE],
              htmlKeys.PRODUCT_IMAGE_FRONT
            )}
          />
        );
      });
    }

    return null;
  }

  placeOrderCheck(): void {
    const {
      configuration,
      discountApplied,
      placeOrder,
      isConsentNeeded,
      checkoutUserInfo,
      pppFormValidation,
      checkProlongationFormValidation,
      formSubmitAction,
      agreeOnTermsAndCondition,
      canPlaceOrderAsPerConsents
    } = this.props;
    const {
      consentDiscount
    } = configuration.cms_configuration.global.prolongation;
    const { form: prolongationFormConfig } = configuration.cms_configuration.modules.prolongation;

    const updatedFormValidation = isFormValidFunction(checkoutUserInfo, prolongationFormConfig, pppFormValidation);

    formSubmitAction(true);

    let isFormValid = true;
    Object.keys(updatedFormValidation).forEach(keys => {

      if (!updatedFormValidation[keys]) {
        isFormValid = false;
      }
    });

    if (!isFormValid || !agreeOnTermsAndCondition || !canPlaceOrderAsPerConsents) {

      checkProlongationFormValidation(updatedFormValidation);

      return;
    }

    if (
      isConsentNeeded &&
      consentDiscount !== ProlongationDiscountEnum.IGNORE &&
      !discountApplied
    ) {
      if (this.state.isOpenSummaryOpened) {
        placeOrder();
      } else {
        this.openSummaryPanel();
      }
    } else {
      placeOrder();
    }
  }

  render(): ReactNode {
    const {
      translation,
      discountApplied,
      globalTranslation,
      isConsentNeeded,
      loadingState,
      configuration
    } = this.props;

    const {
      consentDiscount
    } = configuration.cms_configuration.global.prolongation;
    const { givenDiscountMessage, losingDiscountMessage } = globalTranslation;

    const { isOpenSummaryOpened } = this.state;

    return (
      <StyledOrderPanelWrap>
        <AuxiliaryRoute
          isChildrenRender={isOpenSummaryOpened}
          hashPath={auxillaryRouteConfig.MOBILE_APP.CHECKOUT.ORDER_PANEL}
          isModal={true}
          onClose={() => {
            this.closeSummaryPanel();
          }}
          auxUnmount={this.sendEventOrderSummary}
        >
          <OrderDialogBoxApp
            isOpen={isOpenSummaryOpened}
            type='flexibleHeight'
            onClose={() => {
              if (isOpenSummaryOpened) {
                this.closeSummaryPanel();
              }
            }}
          >
            <StyledTitle>{translation.orderSummary}</StyledTitle>
            {this.getProductCard()}
            {isConsentNeeded &&
              consentDiscount !== ProlongationDiscountEnum.IGNORE ? (
                <StyledDiscountText>
                  {discountApplied ? givenDiscountMessage : losingDiscountMessage}
                </StyledDiscountText>
              ) : null}
          </OrderDialogBoxApp>
        </AuxiliaryRoute>
        <Row className='row'>
          <Column
            className='column'
            colDesktop={6}
            colMobile={6}
            orderDesktop={6}
          >
            <StyledOrderSummaryWrap>
              <div className='wrap-text' onClick={this.toogleSummaryPanel}>
                <StyledTitle>{translation.orderSummary}</StyledTitle>
                <span className='icon-wrapper'>
                  <Icon
                    color='currentColor'
                    size='inherit'
                    name='ec-collapse'
                  />
                </span>
              </div>
            </StyledOrderSummaryWrap>
          </Column>
          <Column
            className='column'
            colDesktop={6}
            colMobile={6}
            orderDesktop={6}
          >
            <StyledOrderPlaceWrap>
              <Button
                className='btn-bundle'
                type='primary'
                loading={loadingState}
                onClickHandler={this.placeOrderCheck}
              >
                {translation.placeOrder}
              </Button>
            </StyledOrderPlaceWrap>
          </Column>
        </Row>
      </StyledOrderPanelWrap>
    );
  }
}

// tslint:disable-next-line:max-file-line-count
export default withRouter(OrderPanelWrap);
