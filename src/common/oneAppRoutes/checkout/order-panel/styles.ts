import styled from 'styled-components';
import { Title } from 'dt-components';
import { colors } from '@src/common/variables';

export const StyledTitle = styled(Title).attrs({
  weight: 'bold',
  size: 'xsmall'
  // transform: 'capitalize'
})`
  color: ${colors.darkGray};
  font-size: 1.25rem;
  font-weight: bold;
  line-height: 1.5rem;
  word-break: break-word;
`;

export const StyledOrderSummaryWrap = styled.div`
  background: ${colors.white};
  height: 100%;
  min-height: 4rem;
  display: flex;
  align-items: center;
  .row {
    margin: 0;
    .column {
      padding: 0;
    }
  }
  .icon-wrapper {
  }
  /* div {
    padding: 0.875rem 1.25rem 0.75rem;
  } */
`;

export const StyledOrderPlaceWrap = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${colors.magenta};
  .btn-bundle {
    color: ${colors.white};
    font-size: 1.25rem;
    font-weight: bold;
    line-height: 1.5rem;
    width: 100%;
    height: 100%;
    white-space: normal;
    word-break: break-word;
    .buttonText {
      /* text-transform: none; */
    }
  }
`;

export const StyledOrderPanelWrap = styled.div`
  /* margin-top: 1rem; */
  width: 100%;
  cursor: pointer;
  z-index: 1001;
  .row {
    margin: 0;
    .column {
      padding: 0;
      /* button can break words in multiple lines start */
      width: 50%;
      flex-grow: 0;
      /* button can break words in multiple lines end */
    }
  }
  .wrap-text {
    width: 100%;
    height: 100%;
    padding: 0 0.5rem 0 1.25rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    .icon-wrapper {
    }
  }
`;

export const StyledDiscountText = styled.div`
  font-size: 0.9375rem;
  line-height: 1.4;
  color: ${colors.magenta};
  font-weight: 500;
  margin-top: 1rem;
`;
