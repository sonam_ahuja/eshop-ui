import React, { Component, ReactNode } from 'react';
import { Column, Row } from '@src/common/components/Grid/styles';
import { FloatLabelInput } from 'dt-components';
import {
  IProlongationFormValidation,
  IUserInfo
} from '@productList/store/types';
import { IProlongationCheckout } from '@common/store/types/translation';
import { IProlongationConfig } from '@src/common/store/types/configuration';
import { getLabelValueForPPPFormFields } from '@src/common/oneAppRoutes/utils/personalDetailForm';
import { isMobile } from '@src/common/utils';

import { StyledPersonalForm } from './styles';

interface IProps {
  userInfo: IUserInfo;
  translation: IProlongationCheckout;
  prolongationConfig: IProlongationConfig;
  formDataValidation: IProlongationFormValidation;
  updateFormData(field: string, value: string): void;
}

class ProductInformationForm extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.updateChange = this.updateChange.bind(this);
    this.validationCheck = this.validationCheck.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.checkIsKeyBoardOpen = this.checkIsKeyBoardOpen.bind(this);
  }

  componentDidMount(): void {
    window.scrollTo(0, 0);
    this.checkIsKeyBoardOpen();
  }

  componentWillUnmount(): void {
    // tslint:disable-next-line:no-empty
    window.removeEventListener('resize', () => {});
  }

  handleChange(
    field: string,
    event: React.ChangeEvent<HTMLInputElement>,
    fieldValue: string
  ): void {
    const inputValue = (event.target as HTMLInputElement).value;
    const maxLen = this.props.prolongationConfig.form[fieldValue].maxLength;
    if (inputValue.length <= maxLen) {
      this.updateChange(field, inputValue);
    }
  }

  checkIsKeyBoardOpen(): void {
    const originalSize = window.innerHeight + window.innerWidth;

    window.addEventListener('resize', () => {
      const resizeValue = window.innerHeight + window.innerWidth;
      if (resizeValue !== originalSize) {
        this.handleFocus();
      } else {
        this.handleBlur();
      }
    });
  }

  handleFocus(): void {
    if (isMobile.phone) {
      document.body.classList.add('hideStickyFooter');
    }
  }

  handleBlur(): void {
    if (isMobile.phone) {
      document.body.classList.remove('hideStickyFooter');
    }
  }

  updateChange(field: string, value: string): void {
    this.props.updateFormData(field, value);
  }

  validationCheck(value: string, field: string): boolean {
    if (value && value !== '') {
      try {
        const { prolongationConfig } = this.props;
        const validationRegex = prolongationConfig.form[field].validation;
        const newRegex = new RegExp(validationRegex);

        return newRegex.test(value);
      } catch (e) {
        return true;
      }
    }

    return true;
  }

  render(): ReactNode {
    const {
      userInfo,
      translation,
      prolongationConfig,
      formDataValidation
    } = this.props;
    const { form: formConfig } = prolongationConfig;
    const { form: formTranslation, optionalText } = translation;

    return (
      <StyledPersonalForm>
        {
          <div className='editPanel'>
            <Row className='row'>
              <Column
                className='column-wrap'
                colDesktop={6}
                colMobile={6}
                orderDesktop={6}
              >
                <FloatLabelInput
                  tabIndex={1}
                  className='input-field'
                  type={formConfig.firstName.inputType}
                  label={getLabelValueForPPPFormFields(
                    formConfig.firstName.mandatory,
                    formTranslation.firstName.label,
                    optionalText
                  )}
                  autoComplete={'off'}
                  value={userInfo.firstName}
                  disabled={formConfig.firstName.readOnly}
                  maxLength={formConfig.firstName.maxLength}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange('firstName', event, 'firstName');
                  }}
                  error={!formDataValidation.firstNameValid}
                  errorMessage={formConfig.firstName.validationMessage}
                />
              </Column>
              <Column
                className='column-wrap pr-none'
                colDesktop={6}
                colMobile={6}
                orderDesktop={6}
              >
                <FloatLabelInput
                  tabIndex={2}
                  type={formConfig.lastName.inputType}
                  className='input-field'
                  label={getLabelValueForPPPFormFields(
                    formConfig.lastName.mandatory,
                    formTranslation.lastName.label,
                    optionalText
                  )}
                  autoComplete={'off'}
                  disabled={formConfig.lastName.readOnly}
                  value={userInfo.lastName}
                  maxLength={formConfig.lastName.maxLength}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange('lastName', event, 'lastName');
                  }}
                  error={!formDataValidation.lastNameValid}
                  errorMessage={formConfig.lastName.validationMessage}
                />
              </Column>
              <Column
                className='column-wrap pr-none'
                colDesktop={12}
                colMobile={12}
                orderDesktop={12}
              >
                <FloatLabelInput
                  tabIndex={3}
                  disableDot={true}
                  type={formConfig.phoneNumber.inputType}
                  className='input-field'
                  disabled={formConfig.phoneNumber.readOnly}
                  label={getLabelValueForPPPFormFields(
                    formConfig.phoneNumber.mandatory,
                    formTranslation.phoneNumber.label,
                    optionalText
                  )}
                  autoComplete={'off'}
                  value={userInfo.msisdn}
                  maxLength={formConfig.phoneNumber.maxLength}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange('msisdn', event, 'phoneNumber');
                  }}
                  error={!formDataValidation.msisdnValid}
                  errorMessage={formConfig.phoneNumber.validationMessage}
                />
              </Column>
              <Column
                className='column-wrap pr-none'
                colDesktop={12}
                colMobile={12}
                orderDesktop={12}
              >
                <FloatLabelInput
                  tabIndex={4}
                  className='input-field'
                  type={formConfig.email.inputType}
                  label={getLabelValueForPPPFormFields(
                    formConfig.email.mandatory,
                    formTranslation.email.label,
                    optionalText
                  )}
                  disabled={formConfig.email.readOnly}
                  autoComplete={'off'}
                  value={userInfo.email}
                  maxLength={formConfig.email.maxLength}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange('email', event, 'email');
                  }}
                  error={!formDataValidation.emailValid}
                  errorMessage={formConfig.email.validationMessage}
                />
              </Column>
              <Column
                className='column-wrap pr-none'
                colDesktop={12}
                colMobile={12}
                orderDesktop={12}
              >
                <FloatLabelInput
                  tabIndex={5}
                  disableDot={true}
                  type={formConfig.deliveryContact.inputType}
                  className='input-field'
                  label={getLabelValueForPPPFormFields(
                    formConfig.deliveryContact.mandatory,
                    formTranslation.deliveryContact.label,
                    optionalText
                  )}
                  disabled={formConfig.deliveryContact.readOnly}
                  autoComplete={'off'}
                  value={userInfo.deliveryContact}
                  maxLength={formConfig.deliveryContact.maxLength}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange(
                      'deliveryContact',
                      event,
                      'deliveryContact'
                    );
                  }}
                  error={!formDataValidation.deliveryContactValid}
                  errorMessage={formConfig.deliveryContact.validationMessage}
                />
              </Column>
              <Column
                className='column-wrap pr-none'
                colDesktop={12}
                colMobile={12}
                orderDesktop={12}
              >
                <FloatLabelInput
                  cols={0}
                  tabIndex={6}
                  type={formConfig.deliveryAddress.inputType}
                  className='input-field'
                  disabled={formConfig.deliveryAddress.readOnly}
                  label={getLabelValueForPPPFormFields(
                    formConfig.deliveryAddress.mandatory,
                    formTranslation.deliveryAddress.label,
                    optionalText
                  )}
                  autoComplete={'off'}
                  value={userInfo.billingAddress}
                  maxLength={formConfig.deliveryAddress.maxLength}
                  // tslint:disable-next-line:no-identical-functions
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange(
                      'billingAddress',
                      event,
                      'deliveryAddress'
                    );
                  }}
                  error={!formDataValidation.billingAddressValid}
                  errorMessage={formConfig.deliveryAddress.validationMessage}
                />
              </Column>
            </Row>
          </div>
        }
      </StyledPersonalForm>
    );
  }
}

export default ProductInformationForm;
