import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledPersonalForm = styled.form`
  /* margin: 0rem 0 2.8125rem; */
  padding-top: 1.78125rem;
  .row {
    margin: 0;
  }
  .column-wrap {
    word-break: break-word;
    padding: 0;
    padding-right: 1rem;
    &.pr-none {
      padding-right: 0;
    }
  }

  .input-field input,
  .input-field textarea,
  .input[type='textarea'] {
    width: 100%;
    color: ${colors.darkGray};
    font-size: 1.25rem;
    line-height: 1.25rem;
    letter-spacing: 0.2px;
    &:disabled {
      /* border-bottom-color: transparent; */
      opacity: 0.6;
    }
  }
`;
