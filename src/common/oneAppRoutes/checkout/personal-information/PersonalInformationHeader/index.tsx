import React from 'react';
import { Icon, Paragraph } from 'dt-components';
import { IProlongationCheckout } from '@common/store/types/translation';

import { StyledPersonalHeader } from './styles';

interface IProps {
  translation: IProlongationCheckout;
}

const ProductInformationHeader = (props: IProps) => {
  const { translation } = props;

  return (
    <StyledPersonalHeader>
      <div className='circleDiv'>
        <Icon color='currentColor' size='inherit' name='ec-user-account' />
      </div>
      <div className='text-edit-wrap'>
        <Paragraph size='large' weight='bold'>
          {translation.personalInfo.heading}
        </Paragraph>
      </div>
    </StyledPersonalHeader>
  );
};

export default ProductInformationHeader;
