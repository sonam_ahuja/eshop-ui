import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledPersonalHeader = styled.div`
  display: flex;
  .circleDiv {
    width: 40px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    background: ${colors.magenta};
    margin-right: 0.75rem;
    .dt_icon {
      display: flex;
      align-items: center;
      justify-content: center;
      color: ${colors.white};
      font-size: 1.25rem;
    }
  }
  .text-edit-wrap {
    width: calc(100% - 52px);
    display: flex;
    align-items: center;
    justify-content: space-between;
    .dt_paragraph {
      color: ${colors.ironGray};
      /* text-transform: capitalize; */
      font-size: 1.5rem;
      font-weight: 900;
    }
    .actionDiv {
      color: ${colors.magenta};
      cursor: pointer;
      span {
        font-size: 0.625rem;
        line-height: 0.75rem;
        margin-right: 0.5rem;
        /* text-transform: capitalize; */
      }
      .dt_icon {
        font-size: 0.75rem;
      }
    }
  }
`;
