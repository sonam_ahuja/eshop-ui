import styled from 'styled-components';
import { Title } from 'dt-components';
import { colors } from '@src/common/variables';

export const StyledProductTitle = styled(Title).attrs({
  size: 'xsmall',
  weight: 'medium'
})`
  color: ${colors.cloudGray};
`;
export const StyledProductDescription = styled(Title).attrs({
  size: 'xsmall',
  weight: 'medium'
})`
  color: ${colors.cloudGray};
`;

export const StyledPriceAmount = styled.div.attrs({
  size: 'small',
  weight: 'ultra'
})`
  font-size: 1.5rem;
  line-height: 1.75rem;
`;

export const StyledPriceList = styled.div`
  margin: 0.75rem 0 0;
  ul {
    li {
      display: inline-block;
      margin-right: 0.75rem;
      opacity: 0.6;
      .amount {
        font-weight: 900;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        color: ${colors.black};
      }
      .unit {
        font-weight: 500;
        font-size: 0.625rem;
        line-height: 0.625rem;
        margin-left: 0.25rem;
        span {
          display: block;
        }
      }
      &:last-child {
        margin-right: 0rem;
        .amount {
          font-weight: 500;
        }
      }
    }
  }
`;

export const StyledProductCard = styled.div`
  padding: 1.25rem;
  .dt_title {
    max-width: 15rem;
  }
  .productTitle {
    opacity: 1;
  }
  .productDescription {
    opacity: 0.7;
  }
  &.pictonBlueCard {
    background: ${colors.pictonBlue};
  }
  &.toryBlueCard {
    background: ${colors.toryBlue};
  }
  &.blumineCard {
    background: ${colors.blumine};
    ul {
      li {
        &:last-child {
          .amount {
            display: none;
          }
        }
      }
    }
  }
`;
