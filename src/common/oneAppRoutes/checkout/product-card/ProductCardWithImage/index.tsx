import React from 'react';
import cx from 'classnames';
import { IProlongationListing } from '@common/store/types/translation';

import {
  StyledLeftPanel,
  StyledPriceAmount,
  StyledPriceList,
  StyledProductCardWithImage,
  StyledProductDescription,
  StyledProductTitle,
  StyledRightPanel
} from './styles';

export interface IProps {
  className?: string;
  bgColor?: string;
  title: string;
  tarriffDescription: string;
  deviceDescription: string;
  primaryAmount: number;
  secondaryAmount: number;
  currencySymbol: string;
  thumbnailImage: string;
  translation: IProlongationListing;
}

const ProductCardWithImage = (props: IProps) => {
  const {
    className,
    title,
    tarriffDescription,
    deviceDescription,
    primaryAmount,
    secondaryAmount,
    bgColor,
    thumbnailImage,
    currencySymbol,
    translation
  } = props;

  const classes = cx(className);

  return (
    <StyledProductCardWithImage
      className={classes}
      style={{ background: bgColor }}
    >
      <div className='panel clearfix'>
        <StyledLeftPanel>
          <StyledProductTitle className='productTitle'>
            {title}
          </StyledProductTitle>
          <StyledProductDescription className='productDescription'>
            {deviceDescription}
          </StyledProductDescription>
          <StyledProductDescription className='productDescription'>
            {tarriffDescription}
          </StyledProductDescription>
          <StyledPriceList>
            <ul>
              <li>
                <StyledPriceAmount>
                  <span className='amount'>
                    <span className='value'>{primaryAmount}</span>
                    <span className='unit'>
                      {currencySymbol}
                      <span>/{translation.monthlyShort}</span>
                    </span>
                  </span>
                </StyledPriceAmount>
              </li>
              <li>
                <StyledPriceAmount>
                  <span className='amount'>
                    <span className='value'>{secondaryAmount}</span>
                    <span className='unit'>
                      {currencySymbol}
                      <span>/{translation.nowText}</span>
                    </span>
                  </span>
                </StyledPriceAmount>
              </li>
            </ul>
          </StyledPriceList>
        </StyledLeftPanel>
        <StyledRightPanel>
          <div className='img-wrap'>
            <img src={thumbnailImage} />
          </div>
        </StyledRightPanel>
      </div>
    </StyledProductCardWithImage>
  );
};

export default ProductCardWithImage;
