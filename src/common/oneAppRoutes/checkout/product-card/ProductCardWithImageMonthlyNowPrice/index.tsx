import React from 'react';
import cx from 'classnames';
import PreloadImage from '@src/common/components/Preload';
import { IMAGE_TYPE } from '@src/common/store/enums';

import {
  StyledLeftPanel,
  StyledPriceAmount,
  StyledPriceList,
  StyledProductCardWithImageMonthlyNowPrice,
  StyledProductDescription,
  StyledProductTitle,
  StyledRightPanel,
  StyledText
} from './styles';

export interface IProps {
  className?: string;
  bgColor?: string;
  title: string;
  deviceDescription: string;
  primaryMonthlyTitle: string;
  secondaryNowTitle: string;
  currencySymbol: string;
  primaryMonthlyAmount: string;
  secondaryNowAmount: string;
  imageUrl: string;
}

const ProductCardWithImageMonthlyNowPrice = (props: IProps) => {
  const {
    className,
    title,
    deviceDescription,
    bgColor,
    primaryMonthlyTitle,
    secondaryNowTitle,
    currencySymbol,
    primaryMonthlyAmount,
    secondaryNowAmount,
    imageUrl
  } = props;

  const classes = cx(className);

  return (
    <StyledProductCardWithImageMonthlyNowPrice
      className={classes}
      style={{ background: bgColor }}
    >
      <div className='panel clearfix'>
        <StyledLeftPanel>
          <div className='img-wrap'>
            <PreloadImage
              isObserveOnScroll={true}
              imageHeight={190}
              mobileWidth={70}
              width={70}
              type={IMAGE_TYPE.WEBP}
              imageWidth={70}
              imageUrl={imageUrl}
              loadDefaultImage={true}
              intersectionObserverOption={{
                root: null,
                rootMargin: '60px',
                threshold: 0
              }}
            />
          </div>
        </StyledLeftPanel>
        <StyledRightPanel>
          <StyledProductTitle className='productTitle'>
            {title}
          </StyledProductTitle>
          <StyledProductDescription className='productDescription'>
            {deviceDescription}
          </StyledProductDescription>
          <StyledPriceList>
            <ul>
              <li className='totalMonthlyWrap'>
                <StyledText>{primaryMonthlyTitle}</StyledText>
                <StyledPriceAmount>
                  <span className='amount'>
                    <span className='value'>
                      {primaryMonthlyAmount} <sup>{currencySymbol}</sup>
                    </span>
                  </span>
                </StyledPriceAmount>
              </li>
              <li className='totalNowWrap'>
                <StyledText>{secondaryNowTitle}</StyledText>
                <StyledPriceAmount>
                  <span className='amount'>
                    <span className='value'>
                      {secondaryNowAmount} <sup>{currencySymbol}</sup>
                    </span>
                  </span>
                </StyledPriceAmount>
              </li>
            </ul>
          </StyledPriceList>
        </StyledRightPanel>
      </div>
    </StyledProductCardWithImageMonthlyNowPrice>
  );
};

export default ProductCardWithImageMonthlyNowPrice;
