import styled from 'styled-components';
import { Paragraph } from 'dt-components';
import { colors } from '@src/common/variables';

export const StyledProductTitle = styled(Paragraph).attrs({
  size: 'small',
  weight: 'medium'
})`
  color: ${colors.darkGray};
  font-weight: 600;
  font-size: 0.9375rem;
`;
export const StyledProductDescription = styled(Paragraph).attrs({
  size: 'small',
  weight: 'medium'
})`
  color: ${colors.mediumGray};
  font-size: 0.9375rem;
`;

export const StyledPriceAmount = styled.div.attrs({
  size: 'small',
  weight: 'ultra'
})`
  font-size: 1.25rem;
  line-height: 1.5rem;
  sup {
    vertical-align: super;
    font-size: 0.625rem;
    line-height: 0.625rem;
    font-weight: 600;
  }
`;

export const StyledText = styled.div`
  font-size: 0.85rem;
  line-height: 1.2;
  /* text-transform: capitalize; */
`;

export const StyledPriceList = styled.div`
  margin: 0.75rem 0 0;
  ul {
    li {
      display: flex;
      justify-content: space-between;
      margin-bottom: 0.5rem;
      align-items: center;
      font-weight: 600;
      .amount {
        font-weight: 900;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        color: ${colors.black};
      }
      .unit {
        font-weight: 500;
        font-size: 0.625rem;
        line-height: 0.625rem;
        margin-left: 0.25rem;
        span {
          display: block;
        }
      }
      &:last-child {
        margin-bottom: 0rem;
        .amount {
          font-weight: 500;
        }
      }
      &.totalMonthlyWrap {
        ${StyledText} {
          color: ${colors.magenta};
        }
        ${StyledPriceAmount} {
          .amount {
            color: ${colors.magenta};
            font-weight: 900;
            .value {
              color: ${colors.magenta};
            }
          }
        }
      }
      &.totalNowWrap {
        ${StyledText} {
          color: ${colors.shadowGray};
        }
        ${StyledPriceAmount} {
          .amount {
            color: ${colors.magenta};
            .value {
              color: ${colors.darkGray};
            }
          }
        }
      }
    }
  }
`;

export const StyledLeftPanel = styled.div`
  width: 3.5rem;
  height: 8.5rem;
  .img-wrap {
    > div {
      width: 3.5rem;
      height: 8.5rem;
      padding: 0;
    }
    img {
      max-width: 100%;
      max-height: 100%;
    }
  }
`;

export const StyledRightPanel = styled.div`
  width: calc(100% - 3.5rem);
  padding-left: 1rem;
  margin-top: 0.5rem;
`;

export const StyledProductCardWithImageMonthlyNowPrice = styled.div`
  .clearfix:after {
    content: ' ';
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  margin: 0 0rem 1.25rem;
  .panel {
    display: flex;
    flex-shrink: 0;
  }
`;
