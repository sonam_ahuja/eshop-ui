import { colors } from '@src/common/variables';
import styled from 'styled-components';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';

export const StyledDialogBox = styled(DialogBoxApp)`
  iframe {
    width: 100%;
    height: 100%;
  }
  .innerDiv {
    height: 100%;
    width: 100%;
    overflow: scroll;
    -webkit-overflow-scrolling: touch;
  }
  .closeIcon {
    z-index: 100;
  }
`;

export const StyledProductCheckout = styled.div`
  background: ${colors.coldGray};
`;

export const StyledPersonalInformation = styled.div`
  padding: 2.25rem 1.25rem 0rem;
  background: ${colors.coldGray};
  flex: 1;
`;

export const StyledOrderStickyWrap = styled.div`
  position: sticky;
  -webkit-overflow-scrolling: auto;
  width: 100%;
  bottom: 0;
  background: ${colors.coldGray};
`;

export const StyledCheckoutAppWrapper = styled.div<{ viewportHeight?: number }>`
  /* height: calc(100vh - 4rem); */
  /* min-height: 100vh; */
  min-height: ${props => props.viewportHeight}px;
  /* padding-bottom: 4rem; */
  display: flex;
  flex-direction: column;
  /* overflow-y: auto;
  -webkit-overflow-scrolling: touch; */
  background: ${colors.coldGray};
`;
