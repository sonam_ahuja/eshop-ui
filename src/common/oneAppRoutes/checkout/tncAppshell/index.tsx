import React from 'react';

import { StyledTncAppshell } from './styles';

const TncAppshell = () => {

  return <StyledTncAppshell className={'primary'}>
    <h2 className='shine' />
    <h3>
      <span className='shine' />
      <span className='shine' />
      <span className='shine' />
    </h3>
    <ul>
      <li className='shine' />
      <li className='shine' />
      <li className='shine' />
      <li className='shine' />
      <li className='shine' />
      <li className='shine' />
      <li className='shine' />
      <li className='shine' />
    </ul>
  </StyledTncAppshell>;
};

export default TncAppshell;
