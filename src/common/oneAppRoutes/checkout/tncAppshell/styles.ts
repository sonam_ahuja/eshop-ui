import styled from 'styled-components';
import StyledShell from '@src/common/components/ShellWrap';

export const StyledTncAppshell = styled(StyledShell)`
  min-height: 100vh;
  h2 {
    width: 70%;
    height: 2.5rem;
  }
  h3 {
    margin-top: 1.5rem;
    span {
      width: 100%;
      height: 1rem;
      margin-bottom: 0.5rem;
      display: block;
    }
    span:nth-child(2) {
      width: 80%;
    }
    span:nth-child(3) {
      width: 70%;
    }
  }
  ul {
    margin-top: 2rem;
    li {
      height: 2rem;
      width: 100%;
      margin-bottom: 1rem;
    }
  }
`;
