import {
  IError,
  IFetchProlongationProductListPayload,
  IProlongationCheckoutDetails,
  IProlongationFormValidation
} from '@productList/store/types';
import { IProlongation } from '@common/store/types/translation';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { RouteComponentProps } from 'react-router-dom';

export interface IMapDispatchToProps {
  fetchProductList(payload: IFetchProlongationProductListPayload): void;
  fetchProlongationCheckoutDetails(): void;
  updateFormData(field: string, value: string): void;
  placeOrder(): void;
  getLead(): void;
  getRSALeadKey(): void;
  orderSummaryEvent(data: boolean): void;
  toggleTnC(): void;
  innerAgreementToggle(outerIndex: number, innerIndex: number): void;
  mainAgreementToggle(data: number): void;
  callFetchProlongationCheckoutDetails(): void;
  checkProlongationFormValidation(data: IProlongationFormValidation): void;
  formSubmitAction(data: boolean): void;
}

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export interface IMapStateToProps {
  checkoutDetails: IProlongationCheckoutDetails;
  loading: boolean;
  showAppShell: boolean;
  error: IError | null;
  translation: IProlongation;
  configuration: IConfigurationState;
}

export interface IState {
  isPersonalFormInEditForm: boolean;
  isTermsAndConditionOpened: boolean;
  viewportHeight?: number;
  isIframeLoaded: boolean;
}
