import styled from 'styled-components';
import { DialogBox } from 'dt-components';
import { colors } from '@src/common/variables';

import {
  StyledOrderSummaryWrap,
  StyledTitle
} from '../../checkout/order-panel/styles';
import { StyledTermsConditionWrap } from '../../checkout/order-panel/TermsCondition/styles';

export const OrderDialogBoxApp = styled(DialogBox)`
  /* common dialogBox styling START */
  bottom: 4rem;
  &.open {
    & ~ .row {
      ${StyledOrderSummaryWrap} {
        .icon-wrapper {
          .dt_icon {
            transform: rotate(180deg);
            transition: all 1s ease;
          }
        }
      }
    }
  }
  .dialogBoxContentWrap {
    padding: 1.25rem;
    .dialogBoxHeader {
      padding-bottom: 2rem;
      .imageWrap {
        img {
          bottom: auto;
        }
      }
    }
    .dialogBoxBody {
      padding-bottom: 2.5rem;
      .dt_title {
        max-width: 400px;
        word-break: break-word;
      }
    }

    .dialogBoxFooter {
      .dt_button {
        width: 100%;
        /* text-transform: capitalize; */
      }

      .dt_button + .dt_button {
        margin-top: 0.75rem;
      }
    }
  }
  .closeIcon {
    z-index: 1;
    top: inherit;
    right: 1.25rem;
    margin-top: -4rem;
    @keyframes fadeIn {
      from {
        opacity: 0;
      }
      to {
        opacity: 1;
      }
    }
    animation-name: fadeIn;
    animation-duration: 100ms;
    animation-delay: 400ms;
    animation-fill-mode: both;
  }
  ${StyledTitle} {
    font-size: 1.5rem;
    color: ${colors.ironGray};
    margin-bottom: 0.875rem;
    font-weight: 900;
  }
  ${StyledTermsConditionWrap} {
    padding: 0;
    margin: 0;
  }

  /* common dialogBox styling END */

  /* flexibleHeight dialogBox styling START */
  &.dt_dialogBox.flexibleHeight {
    .dt_overlay {
      align-items: flex-end;
      margin-top: 0;
      overflow: auto;
      bottom: 4rem;
      border-bottom: 1px solid ${colors.silverGray};
    }

    .dt_outsideClick {
      /* margin: 0; */
      margin-bottom: 0;
      width: 100%;
    }

    .dialogBoxContentWrap {
      border-radius: 0;
      background: ${colors.white};
      cursor: default;
    }
  }
  /* flexibleHeight dialogBox styling END */
`;
