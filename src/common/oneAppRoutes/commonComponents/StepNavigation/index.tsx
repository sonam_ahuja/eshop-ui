import React from 'react';

import { StyledStepNavigation } from './styles';

const StepNavigation = () => {
  return (
    <StyledStepNavigation
      steps={[
        {
          iconName: 'ec-confirm',
          activeIconName: 'ec-confirm',
          onClickHandler: ''
        },
        {
          iconName: 'ec-transporter-right',
          activeIconName: 'ec-transporter-right',
          onClickHandler: ''
        },
        {
          iconName: 'ec-home',
          activeIconName: 'ec-home',
          onClickHandler: ''
        }
      ]}
    />
  );
};

export default StepNavigation;
