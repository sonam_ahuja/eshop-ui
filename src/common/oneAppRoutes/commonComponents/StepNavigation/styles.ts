import styled from 'styled-components';
import { ProgressStepper } from 'dt-components';
import { IProps } from 'dt-components/lib/es/components/molecules/progress-stepper';
import { ComponentType } from 'enzyme';
import { colors } from '@src/common/variables';

export const StyledStepNavigation = styled(ProgressStepper)`
  &.dt_progressStepper {
    background: ${colors.coldGray};
    padding: 0;
    .dt_title {
      padding: 0 15px 0 8px;
    }
  }
  .dt_icon.lockIcon {
    display: none;
    margin-left: -1.3rem;
    font-size: 15px;
    margin-top: -2px;
  }
  .circleDiv.active {
    background: ${colors.magenta};
    .dt_icon {
      font-size: 1.25rem;
    }
  }
  .circleDiv.disabled {
    width: 40px;
    height: 40px;
    background: ${colors.silverGray};
    box-shadow: 0 0 0 2px transparent inset;
    .dt_icon {
      font-size: 1.25rem;
      svg {
        path {
          fill: ${colors.steelGray};
        }
      }
    }
  }
  .dt_title {
    display: none;
  }
  .dt_divider {
    color: ${colors.silverGray};
  }
` as ComponentType<IProps>;
