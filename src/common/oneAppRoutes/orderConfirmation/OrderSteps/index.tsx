import React from 'react';
import { Paragraph } from 'dt-components';
import StepNavigation from '@src/common/oneAppRoutes/commonComponents/StepNavigation/index';
import { IProlongationOrderConfirmation } from '@common/store/types/translation';

interface IProps {
  translation: IProlongationOrderConfirmation;
}

import {
  StyledOrderDescription,
  StyledOrderDetails,
  StyleOrderSteps
} from './styles';

const OrderSteps = (props: IProps) => {
  return (
    <StyleOrderSteps>
      <StyledOrderDetails>
        <Paragraph size='small' weight='bold'>
          {props.translation.subHeading}
        </Paragraph>
        <StyledOrderDescription>
          {props.translation.instructionMessage}
        </StyledOrderDescription>
      </StyledOrderDetails>
      <StepNavigation />
    </StyleOrderSteps>
  );
};

export default OrderSteps;
