import styled from 'styled-components';
import { Paragraph } from 'dt-components';
import { colors } from '@src/common/variables';

export const StyledOrderDescription = styled(Paragraph).attrs({
  size: 'large',
  weight: 'normal'
})`
  color: ${colors.ironGray};
  font-size: 0.75rem;
  line-height: 1rem;
  letter-spacing: 0.24px;
`;

export const StyleOrderSteps = styled.div`
  padding-left: 1.625rem;
  width: 100%;
  padding-right: 0.25rem;
`;

export const StyledOrderDetails = styled.div`
  padding-right: 3.75rem;
  .dt_paragraph {
    color: ${colors.darkGray};
  }
`;
