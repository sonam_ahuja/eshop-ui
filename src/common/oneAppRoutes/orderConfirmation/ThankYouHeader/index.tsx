import React from 'react';
import { Title } from 'dt-components';
import { IProlongationOrderConfirmation } from '@common/store/types/translation';

import { StyledThankYouHeader } from './styles';

interface IProps {
  translation: IProlongationOrderConfirmation;
}

const ThankYouHeader = (props: IProps) => {
  return (
    <StyledThankYouHeader>
      <Title size='large' weight='ultra'>
        {props.translation.heading}
      </Title>
    </StyledThankYouHeader>
  );
};

export default ThankYouHeader;
