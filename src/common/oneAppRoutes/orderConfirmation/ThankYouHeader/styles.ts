import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledThankYouHeader = styled.div`
  padding: 0rem 2rem 0rem 0rem;
  margin-bottom: 5.5rem;
  .dt_title {
    color: ${colors.darkGray};
  }
`;
