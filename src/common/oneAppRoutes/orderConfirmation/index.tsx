import { IPrologationProducts } from '@productList/store/types';
import React, { Component, ReactNode } from 'react';
import { RootState } from '@common/store/reducers';
import { IProlongationConfig } from '@common/store/types/configuration';
import { IProlongationOrderConfirmation } from '@common/store/types/translation';
import { sendOnOrderConfirmEvent } from '@common/oneAppRoutes/utils/googleTracking';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Button } from 'dt-components';
import { IWindow } from '@src/client';
import { Helmet } from 'react-helmet';
import { encodeRFC5987ValueChars } from '@src/common/utils/encodeURL';

import { StyledThankYouWrap } from './styles';
import ThankYouHeader from './ThankYouHeader/index';
import OrderSteps from './OrderSteps/index';

interface IProps extends RouteComponentProps {
  translation: IProlongationOrderConfirmation;
  prolongationData: IPrologationProducts[];
  prolongationSelectedVariant: string;
  gaCode: string;
  prolongationConfiguration: IProlongationConfig;
}

export interface IState {
  viewportHeight?: number;
}
class ThankYouOrder extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      viewportHeight: 10
    };
  }

  componentDidMount(): void {
    const vh = document.documentElement.clientHeight;
    this.setState({ viewportHeight: vh });

    localStorage.setItem('leadExist', 'yes');
    sendOnOrderConfirmEvent({
      url: encodeRFC5987ValueChars(window.location.href),
      gaCode: this.props.gaCode
    });
  }

  render(): ReactNode {
    return (
      <StyledThankYouWrap viewportHeight={this.state.viewportHeight}>
        <Helmet
          title={this.props.prolongationConfiguration.orderPageTitle}
          meta={[]}
        />
        <div className='wrapper'>
          <ThankYouHeader translation={this.props.translation} />
          <OrderSteps translation={this.props.translation} />
          <Button
            type='complementary'
            onClickHandler={() =>
              (window as IWindow).OneApp.closeOneAppWebview()
            }
          >
            {this.props.translation.backToHomeText}
          </Button>
        </div>
      </StyledThankYouWrap>
    );
  }
}

export const mapStateToProps = (state: RootState) => ({
  translation: state.translation.prolongation.orderConfirmation,
  prolongationData: state.productList.prolongationData,
  prolongationSelectedVariant: state.productList.prolongationSelectedVariant,
  gaCode: state.configuration.cms_configuration.modules.prolongation.gaCode,
  prolongationConfiguration:
    state.configuration.cms_configuration.modules.prolongation
});

export default withRouter(connect(mapStateToProps)(ThankYouOrder));
