import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledThankYouWrap = styled.div<{ viewportHeight?: number }>`
  background: ${colors.coldGray};
  padding: 2.5rem 1.25rem 2rem;
  min-height: ${props => props.viewportHeight}px;
  -webkit-overflow-scrolling: touch;
  display: flex;
  .wrapper {
    /* height: 100%; */
    display: flex;
    flex-direction: column;
    flex: 1;
  }
  button {
    width: 100%;
    position: sticky;
    bottom: 0;
    margin-top: auto;
    .buttonText {
      /* text-transform: none; */
    }
  }
`;
