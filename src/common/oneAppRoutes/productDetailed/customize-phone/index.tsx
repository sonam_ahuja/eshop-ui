import React, { Component, ReactNode } from 'react';
import { Column, Row } from '@src/common/components/Grid/styles';
import { getSelectedCharacterstic } from '@productDetailed/utils';
import { PRODUCT_ATTRIBUTE } from '@productList/store/enum';
import {
  ICharacteristics,
  IProlongationGroups
} from '@productList/store/types';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import { IProlongationProductDetails } from '@src/common/store/types/translation';
import { isBrowser } from '@src/common/utils';

import {
  StyledCustomizePhone,
  StyledCustomizePhoneTitle,
  StyledFieldsWrap
} from './styles';
import ColorComponent from './color/index';
import PlanComponent from './plan/index';
import StorageComponent from './storage/index';
import RamComponent from './ram/index';

export interface IProps {
  characteristics: ICharacteristics[];
  variantGroups: IProlongationGroups;
  translation: IProlongationProductDetails;
  onAttributeChange(atribute: IKeyValue): void;
}

export default class CustomizePhone extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.getVariantGroup = this.getVariantGroup.bind(this);
  }

  componentDidMount(): void {
    if (
      isBrowser &&
      document &&
      document.body &&
      typeof document.body.scrollTo === 'function'
    ) {
      document.body.scrollTo(0, 0);
    }
  }

  getVariantGroup(
    variantGroups: IProlongationGroups,
    groupName: string
  ): IVariantGroupsType[] {
    return (
      variantGroups[groupName] &&
      variantGroups[groupName].map(item => {
        return {
          value: item.value as string,
          label: item.label as string,
          enabled: item.enabled as boolean,
          name: item.name as string
        };
      })
    );
  }

  render(): ReactNode {
    const { characteristics, variantGroups } = this.props;
    const { onAttributeChange, translation } = this.props;
    const selectedColour = getSelectedCharacterstic(
      characteristics,
      this.getVariantGroup(variantGroups, PRODUCT_ATTRIBUTE.COLOUR),
      PRODUCT_ATTRIBUTE.COLOUR
    );

    const selectedStorage = getSelectedCharacterstic(
      characteristics,
      this.getVariantGroup(variantGroups, PRODUCT_ATTRIBUTE.STORAGE),
      PRODUCT_ATTRIBUTE.STORAGE
    );

    const selectedPlan = getSelectedCharacterstic(
      characteristics,
      this.getVariantGroup(variantGroups, PRODUCT_ATTRIBUTE.PLAN),
      PRODUCT_ATTRIBUTE.PLAN
    );

    const selectedRam = getSelectedCharacterstic(
      characteristics,
      this.getVariantGroup(variantGroups, PRODUCT_ATTRIBUTE.RAM),
      PRODUCT_ATTRIBUTE.RAM
    );

    return (
      <StyledCustomizePhone>
        <StyledCustomizePhoneTitle>
          {translation.customiseYourPhone}
        </StyledCustomizePhoneTitle>
        <StyledFieldsWrap>
          <Row className='row'>
            <Column
              className='column'
              colDesktop={6}
              colMobile={6}
              orderDesktop={6}
            >
              <ColorComponent
                variants={variantGroups[PRODUCT_ATTRIBUTE.COLOUR]}
                selected={selectedColour}
                storageTranslatedKey={PRODUCT_ATTRIBUTE.COLOUR}
                storageTranslatedValue={translation[PRODUCT_ATTRIBUTE.COLOUR]}
                onAttributeChange={onAttributeChange}
              />
            </Column>
            <Column
              className='column'
              colDesktop={6}
              colMobile={6}
              orderDesktop={6}
            >
              <StorageComponent
                variants={variantGroups[PRODUCT_ATTRIBUTE.STORAGE]}
                selected={selectedStorage}
                storageTranslatedKey={PRODUCT_ATTRIBUTE.STORAGE}
                storageTranslatedValue={translation[PRODUCT_ATTRIBUTE.STORAGE]}
                onAttributeChange={onAttributeChange}
              />
            </Column>
            <Column
              className='column'
              colDesktop={6}
              colMobile={6}
              orderDesktop={6}
            >
              <RamComponent
                variants={variantGroups[PRODUCT_ATTRIBUTE.RAM]}
                selected={selectedRam}
                ramTranslatedKey={PRODUCT_ATTRIBUTE.RAM}
                ramTranslatedValue={translation[PRODUCT_ATTRIBUTE.RAM]}
                onAttributeChange={onAttributeChange}
              />
            </Column>
            <Column
              className='column lastCol'
              colDesktop={12}
              colMobile={12}
              orderDesktop={12}
            >
              <PlanComponent
                variants={variantGroups[PRODUCT_ATTRIBUTE.PLAN]}
                selected={selectedPlan}
                storageTranslatedKey={PRODUCT_ATTRIBUTE.PLAN}
                storageTranslatedValue={translation[PRODUCT_ATTRIBUTE.PLAN]}
                onAttributeChange={onAttributeChange}
              />
            </Column>
          </Row>
        </StyledFieldsWrap>
      </StyledCustomizePhone>
    );
  }
}
