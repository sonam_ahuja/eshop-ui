import React from 'react';
import { Select } from 'dt-components';
import { IProlongationGroupValue } from '@productList/store/types';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import SimpleSelect from '@common/components/Select/SimpleSelect';

import { StyledSelectField, StyledTitle } from '../styles';

import { StyledPlanDropdown } from './styles';

export interface IProps {
  variants: IProlongationGroupValue[];
  selected?: IVariantGroupsType;
  storageTranslatedKey: string;
  storageTranslatedValue: string;
  onAttributeChange(atribute: IKeyValue): void;
}

const PlanComponent = (props: IProps) => {
  const {
    selected,
    variants,
    storageTranslatedKey,
    storageTranslatedValue
  } = props;
  const { onAttributeChange } = props;

  return variants && variants.length ? (
    <StyledPlanDropdown className=''>
      <StyledTitle>{storageTranslatedValue}</StyledTitle>
      <StyledSelectField
        className={variants.length === 1 ? 'hasSingleOption' : ''}
      >
        {variants.length === 1 ? (
          <p className='text'>{variants[0].label}</p>
        ) : (
          <Select
            useNativeDropdown={true}
            enableControlled={true}
            SelectedItemComponent={SimpleSelect}
            onItemSelect={(item: IListItem) => {
              if (selected && item.id === selected.value) {
                return;
              }
              onAttributeChange({
                value: item.id as string,
                name: storageTranslatedKey
              });
            }}
            selectedItem={
              selected
                ? {
                    id: selected.value,
                    title: selected.label
                  }
                : null
            }
            items={(variants || []).map(variant => {
              return {
                id: variant.value as string,
                title: variant.label as string,
                disabled: !variant.enabled
              };
            })}
          />
        )}
      </StyledSelectField>
    </StyledPlanDropdown>
  ) : null;
};

export default PlanComponent;
