import React from 'react';
import { Select } from 'dt-components';
import { IProlongationGroupValue } from '@productList/store/types';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import SimpleSelect from '@common/components/Select/SimpleSelect';

import { StyledSelectField, StyledTitle } from '../styles';

import { StyledRamDropdown } from './styles';

export interface IProps {
  variants: IProlongationGroupValue[];
  selected?: IVariantGroupsType;
  ramTranslatedKey: string;
  ramTranslatedValue: string;
  onAttributeChange(atribute: IKeyValue): void;
}

const RamComponent = (props: IProps) => {
  const { selected, variants, ramTranslatedKey, ramTranslatedValue } = props;
  const { onAttributeChange } = props;

  return variants && variants.length ? (
    <StyledRamDropdown className=''>
      <StyledTitle>{ramTranslatedValue}</StyledTitle>
      <StyledSelectField
        className={variants && variants.length === 1 ? 'hasSingleOption' : ''}
      >
        {variants && variants.length === 1 ? (
          <p className='text'>{variants[0].label}</p>
        ) : (
          <Select
            useNativeDropdown={true}
            enableControlled={true}
            SelectedItemComponent={SimpleSelect}
            onItemSelect={(item: IListItem) => {
              if (selected && item.id === selected.value) {
                return;
              }
              onAttributeChange({
                value: item.id as string,
                name: ramTranslatedKey
              });
            }}
            selectedItem={
              selected
                ? {
                    id: selected.value,
                    title: selected.label
                  }
                : null
            }
            items={(variants || []).map(variant => {
              return {
                id: variant.value as string,
                title: variant.label as string,
                disabled: !variant.enabled
              };
            })}
          />
        )}
      </StyledSelectField>
    </StyledRamDropdown>
  ) : null;
};

export default RamComponent;
