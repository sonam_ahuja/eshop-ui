import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Section, Title } from 'dt-components';

export const StyledCustomizePhoneTitle = styled(Title).attrs({
  size: 'small',
  weight: 'bold'
})`
  color: ${colors.ironGray};
  font-size: 1.25rem;
  line-height: 1rem;
  letter-spacing: 0.24px;
  margin-bottom: 1.25rem;
`;

export const StyledFieldsWrap = styled.div`
  .styledSelectWrap {
    border-bottom: 1px solid ${colors.silverGray};
    .styledSelect {
      font-size: 1rem;
      line-height: 1.5rem;
      font-weight: bold;
    }
  }
  .optionsList {
    margin-top: -1.3rem;
    border-radius: 0.4rem;
    left: -1px;
    width: calc(100% + 2px);
    div {
      color: ${colors.darkGray};
      padding: 0.25rem 0.75rem;
      font-size: 0.875rem;
      line-height: 1.25rem;
      font-weight: bold;
      height: auto;
      text-align: left;
      &:hover {
        background: ${colors.fogGray};
      }
      &.active {
        color: ${colors.magenta};
      }
    }
  }
  .row {
    margin: 0 -0.5rem 0;
    .column {
      padding: 0 0.5rem;
      &:last-child {
        .planDropdown {
          margin-bottom: 0.1875rem;
        }
      }
    }
  }
`;

export const StyledCustomizePhone = styled.div`
  padding: 1.25rem;
  /* padding-bottom: 5rem; */
  background: ${colors.coldGray};
  width: 100%;
  /* min-height: 14rem; */
  flex-shrink: 0;
  flex: 1;
`;

// DROPDOWN CSS START
export const StyledTitle = styled(Section).attrs({
  weight: 'medium',
  size: 'large'
  // text-transform: capitalize;
})`
  color: ${colors.mediumGray};
  margin-bottom: 0.5rem;
  font-size: 0.9375rem;
`;
export const StyledSelectField = styled.div`
  height: 1.75rem;
  border-bottom: 1px solid ${colors.silverGray};
  .styledSimpleSelect {
    display: flex;
    justify-content: space-between;
    .dt_paragraph {
      font-size: 1.25rem;
      font-weight: normal;
      line-height: 1.25;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }
  }

  &.hasSingleOption {
    border-bottom: 0px;
    .styledSimpleSelect {
      .dt_paragraph + .dt_icon {
        display: none;
      }
    }
    .text {
      font-weight: normal;
      font-size: 1.25rem;
      line-height: 1.25;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }
  }

  @media (min-width: ${breakpoints.mobile}px) {
    /* .styledSimpleSelect {
      .dt_paragraph {
        font-size: 1.25rem;
      }
    }
    &.hasSingleOption {
      .text {
        font-size: 1.25rem;
      }
    } */
  }
`;
