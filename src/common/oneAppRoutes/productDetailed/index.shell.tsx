import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { colors } from '@src/common/variables';
import { Button } from 'dt-components';

export const StyledProductDetailedShell = styled.div`
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  height: 100vh;
  flex-direction: column;
  background: ${colors.coldGray};
  .card {
    flex: 1;
    min-height: 8rem;
    width: 100%;
    padding: 0;
    padding-bottom: 0;
    display: flex;
    flex-direction: column;
    height: 100%;
    .primaryHeadTitle {
      height: 1rem;
      width: 60%;
      background: ${colors.cloudGray};
    }
    .secondaryHeadTitle {
      height: 1.5rem;
      width: 60%;
      background: ${colors.cloudGray};
      margin-bottom: 0.5rem;
    }
    .smallTitle {
      height: 0.7rem;
      width: 60%;
      background: ${colors.cloudGray};
      margin-bottom: 0.5rem;
    }
    .image {
      width: 100%;
      height: 100%;
      background: ${colors.cloudGray};
    }

    .headTitle {
      height: 2rem;
      width: 100%;
      background: ${colors.cloudGray};
      margin-bottom: 0.75rem;
    }
    .info {
      height: 1.5rem;
      width: 100%;
      background: ${colors.cloudGray};
      margin-bottom: 0.5rem;
    }

    .list-inline {
      margin-left: 0rem;
      margin-top: auto;
      li {
        display: inline-block;
        width: calc(50% - 0rem);
        padding-right: 0rem;
        &:last-child {
          margin-bottom: 0rem;
        }
      }
    }
    .panel {
      display: flex;
      .leftPanel {
        width: 5.5rem;
        height: 11.5rem;
        margin: 0 2.75rem 0 1.75rem;
      }
      .rightPanel {
        height: 100%;
        flex: 1;
        .info {
          margin-bottom: 0.25rem;
        }
      }
    }
  }
  .row {
    margin: 0;
    display: flex;
    flex-wrap: wrap;
    height: 100%;
    .col {
      padding: 2rem 1.25rem 0;
      width: 100%;
      background: ${colors.white};
      height: 100vh;
    }
  }
  .mainShellTitleWrap {
    background: ${colors.coldGray};
    padding: 2rem 1.25rem;
    height: 8rem;
    border-bottom: 1px solid ${colors.lightishGray};
  }
  .sectionCardShells {
    height: calc(100% - 0rem);
  }
  .shine {
    height: 100%;
    width: 100%;
    margin: 0;
    &:after {
      background: ${colors.silverGray};
    }
  }
  .header {
  }
  .footer {
    background: ${colors.coldGray};
    padding: 1.75rem 1.25rem 0;
    margin-top: 2.375rem;
    width: calc(100% + 2.5rem);
    margin: 2.375rem -1.25rem 0;
    .list-inline {
      margin-top: 1.25rem;
      li {
        &:first-child {
          padding-right: 1rem;
        }
        &:last-child {
          width: 100%;
        }
      }
    }
    .purchaseItems {
      margin: 0 -1.25rem;
      display: flex;
      margin-top: 1rem;
      .left {
        background: ${colors.white};
        flex: 1;
        padding: 0 0.25rem 0 1.25rem;
        .list-inline {
          li {
            width: 50%;
            padding-right: 1rem;
          }
        }
      }
      .right {
        flex: 1;
        .buttonWrap {
          height: 3.75rem;
          .dt_button {
            width: 100%;
            height: 100%;
          }
        }
      }
    }
  }
  .mainBody {
    margin-top: 1rem;
    height: calc(100vh - 5rem);
    .list-inline {
      margin-left: 0rem;
      margin-top: auto;
      li {
        display: inline-block;
        width: 100%;
        &:last-child {
          margin-bottom: 0rem;
        }
      }
    }
  }
`;

const ProductDetailedShell = () => {
  return (
    <StyledShell>
      <StyledProductDetailedShell>
        <section className='sectionCardShells'>
          <div className='row'>
            <div className='col'>
              <div className='card'>
                <div className='header'>
                  <div className='secondaryHeadTitle shine' />
                  <div className='info shine' />
                </div>
                <div className='mainBody'>
                  <div className='panel'>
                    <div className='leftPanel'>
                      <div className='shine image' />
                    </div>
                    <div className='rightPanel'>
                      <div className='smallTitle shine' />
                      <ul className='list-inline'>
                        <li>
                          <div className='shine info' />
                        </li>
                        <li>
                          <div className='shine info' />
                        </li>
                        <li>
                          <div className='shine info' />
                        </li>
                        <li>
                          <div className='shine info' />
                        </li>
                        <li>
                          <div className='shine info' />
                        </li>
                        <li>
                          <div className='shine info' />
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className='footer'>
                  <div className='customizeWrap'>
                    <div className='info shine' />
                    <ul className='list-inline'>
                      <li>
                        <div className='shine info' />
                      </li>
                      <li>
                        <div className='shine info' />
                      </li>
                      <li>
                        <div className='shine info' />
                      </li>
                    </ul>
                  </div>
                  <div className='purchaseItems'>
                    <div className='left'>
                      <ul className='list-inline'>
                        <li>
                          <div className='shine info' />
                        </li>
                        <li>
                          <div className='shine info' />
                        </li>
                      </ul>
                    </div>
                    <div className='right'>
                      <div className='buttonWrap'>
                        <Button type='primary' className='loading' />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </StyledProductDetailedShell>
    </StyledShell>
  );
};

export default ProductDetailedShell;
