import { getSelectedFiltersFromParams } from '@src/common/routes/productDetailed/utils';
import { sendForProductDetailPage } from '@src/common/oneAppRoutes/utils/googleTracking';
import actions from '@productList/store/actions';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { RootState } from '@src/common/store/reducers';
import {
  IProlongationProductDetailedPayload,
  IProlongationProductListResponse
} from '@productList/store/types';
import { IKeyValue } from '@productDetailed/store/types';
import { RouteComponentProps, withRouter } from 'react-router';
import React, { Component, ReactNode } from 'react';
import SimpleLoader from '@src/common/components/Loader';
import { PRICE_TYPE } from '@src/common/routes/productDetailed/store/enum';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import htmlKeys from '@common/constants/appkeys';
import { getCharacterstic } from '@category/utils/helper';
import { Helmet } from 'react-helmet';
import { encodeRFC5987ValueChars } from '@src/common/utils/encodeURL';
import { getPriceAndCurrency } from '@src/common/routes/tariff/store/utils';
import { getUpfrontPrice } from '@src/common/oneAppRoutes/productList/product-card';
import { IWindow } from '@src/client';
import { isBrowser } from '@src/common/utils';
import { getCurrencyCode } from '@store/common/index';

import { StyledMobileAppProductDetailed } from './styles';
import CustomizePhone from './customize-phone/index';
import PurchaseItems from './purchase-items/index';
import ProductView from './productView';
import { IMapDispatchToProps, IMapStateToProps } from './types';

type IProps = IMapDispatchToProps & IMapStateToProps & RouteComponentProps;

export class ProductDetailed extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.getProductVariantIDFromURL = this.getProductVariantIDFromURL.bind(
      this
    );
    this.onAttributeChange = this.onAttributeChange.bind(this);
  }

  getProductVariantIDFromURL(
    pathname: string
  ): { categorySlug: string; productId: string; variantId: string } {
    const arrPath = pathname.split('/');
    if (arrPath && arrPath[1] && (arrPath[3] || arrPath[4])) {
      return {
        categorySlug: arrPath[1],
        productId: arrPath[3],
        variantId: arrPath[4]
      };
    }

    return { categorySlug: '', productId: '', variantId: '' };
  }

  // tslint:disable-next-line:cognitive-complexity
  componentDidMount(): void {
    if (
      isBrowser &&
      document &&
      document.body &&
      typeof document.body.scrollTo === 'function'
    ) {
      setTimeout(() => {
        document.body.scrollIntoView();
        const appEle = document.getElementById('app');
        if (appEle) {
          appEle.scrollIntoView();
        }
      }, 100);
    }

    const checkLocalStorageLead = localStorage.getItem('leadExist');
    if (
      isBrowser &&
      (window as IWindow).OneApp &&
      checkLocalStorageLead &&
      checkLocalStorageLead === 'yes'
    ) {
      (window as IWindow).OneApp.closeOneAppWebview();

      return;
    }

    const { productList, setProlongationProductList } = this.props;
    const { location, category } = this.props;
    const {
      categorySlug,
      productId,
      variantId
    } = this.getProductVariantIDFromURL(location.pathname);
    if (Array.isArray(productList) && productList.length) {
      const productData = productList.filter(
        product => product.variantId === variantId
      );
      if (Array.isArray(productData) && !productData.length) {
        const { changedAttribute } = getSelectedFiltersFromParams(
          this.props.location.search
        );
        this.onAttributeChange(changedAttribute);
      } else {
        setProlongationProductList({
          category,
          data: productData
        });
      }

      /*** GA */
      if (productData[0] && Array.isArray(productData[0].prices)) {
        const upfrontAmt = productData[0].prices.find(price => {
          return price.priceType === PRICE_TYPE.UPFRONT_PRICE;
        });
        const monthlyAmt = productData[0].prices.find(price => {
          return price.priceType === PRICE_TYPE.RECURRING_FEE;
        });
        const productOptionValues = productData[0].optionValues;

        const actualUpfrontPrice = getUpfrontPrice(productData[0].prices);
        const currencySymbol = getPriceAndCurrency(
          actualUpfrontPrice,
          getCurrencyCode()
        ).discountCurrency;
        const currencyCode = getCurrencyCode();

        sendForProductDetailPage({
          label: `${variantId}_${productData[0].name}_${
            productOptionValues[1]
          }_${productOptionValues[0]}_${productOptionValues[2]}`,
          deviceId: variantId,
          deviceName: productData[0].name,
          deviceUpfrontAmt: upfrontAmt
            ? String(upfrontAmt.discountedValue)
            : '',
          deviceMonthlyAmt: monthlyAmt
            ? String(monthlyAmt.discountedValue)
            : '',
          openedFrom: this.props.prolongationOpenedFrom,
          pageUrl: encodeRFC5987ValueChars(window.location.href),
          gaCode: this.props.prolongationConfiguration.gaCode,
          currencySymbol,
          currencyCode
        });
      }
      /*** GA */
    } else {
      const { changedAttribute } = getSelectedFiltersFromParams(
        this.props.location.search
      );
      const parsedQuery = parseQueryString(this.props.location.search);

      this.props.fetchProlongationProductDetails({
        category: categorySlug,
        productId,
        variantId,
        segment: parsedQuery.segmentId,
        changedAttribute: changedAttribute
          ? changedAttribute
          : { name: '', value: '' }
      });
    }
  }

  onAttributeChange(changedAttribute: IKeyValue | null): void {
    const { categorySlug, productId } = this.getProductVariantIDFromURL(
      this.props.location.pathname
    );
    const parsedQuery = parseQueryString(this.props.location.search);

    this.props.fetchProlongationProductDetails({
      category: categorySlug,
      segment: parsedQuery.segmentId,
      productId,
      changedAttribute: changedAttribute
        ? changedAttribute
        : { name: '', value: '' }
    });
  }

  render(): ReactNode {
    const { showAppShell, productList, translation } = this.props;
    const product = productList[0];
    let metaTagTitle = '';
    if (product) {
      metaTagTitle = getCharacterstic(
        product.characteristics,
        htmlKeys.META_TAG_TITLE
      );
    }

    return (
      <>
        {product && product.groups ? (
          <StyledMobileAppProductDetailed>
            <Helmet title={metaTagTitle} meta={[]} />
            <ProductView productList={productList} translation={translation} />
            <CustomizePhone
              variantGroups={product.groups}
              characteristics={product.characteristics}
              onAttributeChange={this.onAttributeChange}
              translation={translation}
            />
            <PurchaseItems
              productList={productList}
              translation={translation}
            />
            <SimpleLoader open={showAppShell} className='simple-loader' />
          </StyledMobileAppProductDetailed>
        ) : null}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  productList: state.productList.prolongationData,
  category: state.productList.categoryName,
  loading: state.productList.loading,
  showAppShell: state.productList.showAppShell,
  error: state.productList.error,
  translation: state.translation.prolongation.productDetails,
  prolongationConfiguration:
    state.configuration.cms_configuration.modules.prolongation,
  prolongationOpenedFrom: state.productList.prolongationOpenedFrom
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  fetchProlongationProductDetails(
    payload: IProlongationProductDetailedPayload
  ): void {
    dispatch(actions.fetchProlongationProductDetails(payload));
  },
  setProlongationProductList(payload: IProlongationProductListResponse): void {
    dispatch(actions.setProlongationProductList(payload));
  }
});

export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(ProductDetailed)
);
