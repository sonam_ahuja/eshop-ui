import React from 'react';

export interface IProps {
  name: string;
  description: string;
}

import {
  StyledProductDescription,
  StyledProductHeader,
  StyledProductTitle
} from './styles';

const ProductHeader = (props: IProps) => {
  return (
    <StyledProductHeader>
      <StyledProductTitle>{props.name}</StyledProductTitle>
      <StyledProductDescription>
        {props.description}
      </StyledProductDescription>
    </StyledProductHeader>
  );
};

export default ProductHeader;
