import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledProductTitle = styled.div`
  color: ${colors.darkGray};
  line-height: 1.25rem;
  font-size: 1.5rem;
  margin-bottom: 0.35rem;
`;

export const StyledProductDescription = styled.div`
  color: ${colors.mediumGray};
  line-height: 1.25rem;
  font-size: 1.25rem;
`;

export const StyledProductHeader = styled.div`
  font-weight: 900;
`;
