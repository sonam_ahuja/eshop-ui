import React from 'react';
import UnorderedList from '@src/common/components/UnorderedList/index';
import { IPrologationProducts } from '@productList/store/types';
import { IMAGE_TYPE } from '@common/store/enums';
import PreloadImage from '@src/common/components/Preload';
import { getImage } from '@src/common/routes/productList/utils/getProductDetail';
import htmlKeys from '@common/constants/appkeys';
import { IProlongationProductDetails } from '@src/common/store/types/translation';

import ProductHeader from '../productHeader';

import {
  StyledImgPanel,
  StyledProductView,
  StyledProductViewList
} from './styles';

export interface IProps {
  productList: IPrologationProducts[];
  translation: IProlongationProductDetails;
}

const ProductView = (props: IProps) => {
  const product = props.productList[0];
  const { translation } = props;
  const getImageUrl = getImage(
    product.attachments[htmlKeys.THUMBNAIL_IMAGE],
    htmlKeys.PRODUCT_IMAGE_FRONT
  );

  if (!product) {
    return null;
  }

  return (
    <StyledProductView>
      <ProductHeader
        name={product.name}
        description={product.shortDescription}
      />
      <StyledProductViewList>
        <div className='product-list-wrapper'>
          <div className='row'>
            {getImageUrl !== '' ? (
              <div className='column'>
                <StyledImgPanel>
                  <PreloadImage
                    isObserveOnScroll={true}
                    imageHeight={175}
                    mobileWidth={85}
                    width={85}
                    type={IMAGE_TYPE.WEBP}
                    imageWidth={85}
                    loadDefaultImage={true}
                    imageUrl={getImageUrl}
                    intersectionObserverOption={{
                      root: null,
                      rootMargin: '60px',
                      threshold: 0
                    }}
                  />
                </StyledImgPanel>
              </div>
            ) : null}
            <div className='column'>
              <h2 className='listHeading'>{translation.highlightText}</h2>
              <UnorderedList translation={translation} list={product.description} variantId={product.variantId} />
            </div>
          </div>
        </div>
      </StyledProductViewList>
    </StyledProductView>
  );
};

export default ProductView;
