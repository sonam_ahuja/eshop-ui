import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledProductView = styled.div`
  background: ${colors.white};
  padding: 1.25rem 1.25rem 0.5rem;
  /* flex: 1;
  flex-shrink: 0; */
`;

export const StyledImgPanel = styled.div`
  /* padding: 0 1.75rem; */
  margin: auto;
  width: 5.5rem;
  height: 11.5rem;
  .img-fluid {
    max-width: 100%;
    max-height: 100%;
    margin: auto;
  }
`;

export const StyledList = styled.div`
  .clearfix:after {
    content: ' ';
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  ul {
    li {
      margin-bottom: 0.25rem;
    }
  }
  .icons {
    color: ${colors.magenta};
    font-size: 1rem;
    padding-right: 0.75rem;
    float: left;
    width: 1.75rem;
  }
  .text {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.darkGray};
    font-weight: 500;
    float: left;
    width: calc(100% - 1.75rem);
  }
`;

export const StyledProductViewList = styled.div`
  margin: 2rem 0 0.5rem;
  width: 100%;

  .row {
    display: flex;
    .column {
      flex-basis: 40%;
      flex-grow: 1;
      flex-shrink: 0;
    }
    .column:last-child {
      max-width: 60%;
      flex-basis: 60%;
      min-width: 60%;
      padding-left: 0.5rem;
    }
    .column:first-child {
      padding-left: 0;
    }

    .listHeading {
      color: ${colors.mediumGray};
      margin-bottom: 0.5rem;
      /* text-transform: capitalize; */
      font-size: 0.9375rem;
    }
  }
`;
