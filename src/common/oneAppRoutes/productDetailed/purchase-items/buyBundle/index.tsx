import React from 'react';
import { Button } from 'dt-components';
import { IProlongationProductDetails } from '@src/common/store/types/translation';
import history from '@client/history';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';

import { StyledBuyBundle } from './styles';

interface IProps {
  translation: IProlongationProductDetails;
}

const BuyBundleComponent = (props: IProps) => {
  const { translation } = props;

  return (
    <StyledBuyBundle>
      <Button
        onClickHandler={() => {
          if (history) {
            const parsedQuery = parseQueryString(history.location.search);
            /** TODO PREVIOUS CODE: was pathname: `${history.location.pathname}/checkout/order-details`, */
            history.push({
              pathname: `${history.location.pathname}/checkout/order-details`,
              search: `categoryId=${parsedQuery.categoryId}`
            });
          }
        }}
        className='btn-bundle'
        type='primary'
        iconName='ec-continue'
        isIconRight={true}
      >
        {translation.buyBundle}
      </Button>
    </StyledBuyBundle>
  );
};

export default BuyBundleComponent;
