import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledBuyBundle = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${colors.magenta};
  min-height: 4rem;
  .btn-bundle {
    color: ${colors.white};
    font-size: 1.25rem;
    font-weight: bold;
    line-height: 1.5rem;
    width: 100%;
    height: 100%;
    word-break: break-word;
    .buttonText {
      /* text-transform: none; */
    }
  }
  .dt_button {
    white-space: normal;
    .buttonText {
      /* text-transform: none; */
    }
  }
`;
