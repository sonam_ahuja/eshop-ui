import React from 'react';
import { Column, Row } from '@src/common/components/Grid/styles';
import { IPrologationProducts } from '@productList/store/types';
import { IProlongationProductDetails } from '@src/common/store/types/translation';

import { StyledPurchaseItems } from './styles';
import PriceListComponent from './price-list';
import BuyBundleComponent from './buyBundle';

export interface IProps {
  productList: IPrologationProducts[];
  translation: IProlongationProductDetails;
}

const PurchaseItems = (props: IProps) => {
  const product = props.productList[0];
  const { translation } = props;
  document.body.style.overflow = '';

  return (
    <StyledPurchaseItems>
      <Row className='row'>
        <Column
          className='column'
          colDesktop={6}
          colMobile={6}
          orderDesktop={6}
        >
          <PriceListComponent
            prices={product.prices}
            translation={translation}
          />
        </Column>
        <Column
          className='column'
          colDesktop={6}
          colMobile={6}
          orderDesktop={6}
        >
          <BuyBundleComponent translation={translation} />
        </Column>
      </Row>
    </StyledPurchaseItems>
  );
};

export default PurchaseItems;
