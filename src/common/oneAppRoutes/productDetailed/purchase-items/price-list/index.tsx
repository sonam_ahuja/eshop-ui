import React from 'react';
import { Column, Row } from '@src/common/components/Grid/styles';
import { IPrices } from '@tariff/store/types';
import {
  getMonthlyPrice,
  getUpfrontPrice
} from '@src/common/oneAppRoutes/productList/product-card';
import { getPriceAndCurrency } from '@src/common/routes/tariff/store/utils';
import { IProlongationProductDetails } from '@src/common/store/types/translation';
import { getFormatedCurrencyValue } from '@src/common/utils/currency';
import { getCurrencyCode } from '@store/common/index';

import { StyledPriceList } from './styles';
import TotalMonthlyComponent from './totalMonthly/index';
import TotalNewComponent from './totalNew/index';

export interface IProps {
  prices: IPrices[];
  translation: IProlongationProductDetails;
}

const PriceListComponent = (props: IProps) => {
  const actualUpfrontPrice = getUpfrontPrice(props.prices);
  const upfrontPrice = getFormatedCurrencyValue(actualUpfrontPrice);
  const monthlyPrice = getFormatedCurrencyValue(getMonthlyPrice(props.prices));
  let currencyCode = '';
  if (props.prices[0]) {
    currencyCode = getPriceAndCurrency(actualUpfrontPrice, getCurrencyCode())
      .discountCurrency;
  }
  const { translation } = props;

  return (
    <StyledPriceList>
      <Row className='row'>
        <Column
          className='column'
          colDesktop={12}
          colMobile={12}
          orderDesktop={12}
        >
          <TotalMonthlyComponent
            currency={monthlyPrice}
            currencySymbol={currencyCode}
            translation={translation}
          />
        </Column>
        <Column
          className='column'
          colDesktop={12}
          colMobile={12}
          orderDesktop={12}
        >
          <TotalNewComponent
            currency={upfrontPrice}
            currencySymbol={currencyCode}
            translation={translation}
          />
        </Column>
      </Row>
    </StyledPriceList>
  );
};

export default PriceListComponent;
