import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { Title } from 'dt-components';

export const StyledCustomizePhoneTitle = styled(Title).attrs({
  size: 'small',
  weight: 'normal'
})`
  color: ${colors.ironGray};
  font-size: 0.75rem;
  line-height: 1rem;
  letter-spacing: 0.24px;
  margin-bottom: 1.25rem;
`;

export const StyledFieldsWrap = styled.div`
  .styledSelectWrap {
    border-bottom: 1px solid ${colors.silverGray};
    .styledSelect {
      font-size: 1rem;
      line-height: 1.5rem;
      font-weight: bold;
    }
  }
  .optionsList {
    margin-top: -1.3rem;
    border-radius: 0.4rem;
    left: -1px;
    width: calc(100% + 2px);
    div {
      color: ${colors.darkGray};
      padding: 0.25rem 0.75rem;
      font-size: 0.875rem;
      line-height: 1.25rem;
      font-weight: bold;
      height: auto;
      text-align: left;
      &:hover {
        background: ${colors.fogGray};
      }
      &.active {
        color: ${colors.magenta};
      }
    }
  }
`;

export const StyledPriceList = styled.div`
  padding: 0.5rem 0.75rem 0.5rem 1.2rem;
  background: ${colors.white};
  min-height: 4rem;
  display: flex;
  flex: 1;
  align-items: center;
  .row {
    width: 100%;
    margin: 0;
    .column {
      padding: 0;
    }
  }
`;
