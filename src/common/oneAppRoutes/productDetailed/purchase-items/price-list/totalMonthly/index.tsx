import React from 'react';
import { IProlongationProductDetails } from '@src/common/store/types/translation';

import { StyledTitle, StyledTotalMonthly, StyledValue } from './styles';

export interface IProps {
  currency: string;
  currencySymbol: string;
  translation: IProlongationProductDetails;
}

const TotalMonthlyComponent = (props: IProps) => {
  return (
    <StyledTotalMonthly>
      <StyledTitle>{props.translation.totalMonthly}</StyledTitle>
      <StyledValue>
        {props.currency}{' '}{props.currencySymbol}
      </StyledValue>
    </StyledTotalMonthly>
  );
};

export default TotalMonthlyComponent;
