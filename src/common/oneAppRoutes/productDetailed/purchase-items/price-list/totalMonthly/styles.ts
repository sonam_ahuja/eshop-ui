import styled from 'styled-components';
import { Section } from 'dt-components';
import { colors } from '@src/common/variables';

export const StyledTitle = styled(Section).attrs({
  weight: 'medium',
  size: 'large'
  // text-transform: capitalize;
})`
  color: ${colors.magenta};
`;

export const StyledValue = styled.div`
  font-size: 1.25rem;
  line-height: 1.5rem;
  color: ${colors.magenta};
  font-weight: 900;
  /* sup {
    vertical-align: super;
    font-size: 0.625rem;
    line-height: 0.625rem;
    font-weight: 500;
  } */
`;

export const StyledTotalMonthly = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  .dt_section {
    word-break: break-word;
    font-size: 0.9375rem;
  }
  ${StyledValue} {
    min-width: 40%;
    word-break: break-word;
    text-align: right;
  }
`;
