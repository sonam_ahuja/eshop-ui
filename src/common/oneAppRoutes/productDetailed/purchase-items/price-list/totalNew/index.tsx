import React from 'react';
import { IProlongationProductDetails } from '@src/common/store/types/translation';

import { StyledTitle, StyledTotalNew, StyledValue } from './styles';

export interface IProps {
  currency: string;
  currencySymbol: string;
  translation: IProlongationProductDetails;
}

const TotalNewComponent = (props: IProps) => {
  return (
    <StyledTotalNew>
      <StyledTitle>{props.translation.totalUpfront}</StyledTitle>
      <StyledValue>
        {props.currency}{' '}{props.currencySymbol}
      </StyledValue>
    </StyledTotalNew>
  );
};

export default TotalNewComponent;
