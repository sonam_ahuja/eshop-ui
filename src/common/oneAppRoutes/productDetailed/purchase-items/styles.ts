import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPurchaseItems = styled.div`
  /* position: fixed; */
  bottom: 0;
  width: 100%;
  position: sticky; /*********** due to dynamic height fixes ***********/
  z-index: 100;
  .row {
    margin: 0;
    .column {
      padding: 0;
    }
  }

  @media (max-width: ${breakpoints.mobile - 1}px) {
    .row {
      .column {
        flex-basis: 50%;
        max-width: 50%;
        .row {
          .column {
            flex-basis: 100%;
            max-width: 100%;
          }
        }
      }
    }
  }
`;
