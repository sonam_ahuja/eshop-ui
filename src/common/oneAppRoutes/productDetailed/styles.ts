import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledMobileAppProductDetailed = styled.div`
  min-height: 100vh;
  display: flex;
  background: ${colors.coldGray};
  flex-wrap: wrap;
  flex-direction: column;
`;
