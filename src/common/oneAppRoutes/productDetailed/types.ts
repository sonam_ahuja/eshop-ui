import {
  IError,
  IPrologationProducts,
  IProlongationProductDetailedPayload,
  IProlongationProductListResponse
} from '@productList/store/types';
import { IProlongationProductDetails } from '@src/common/store/types/translation';
import { IProlongationConfig } from '@src/common/store/types/configuration';

export interface IMapStateToProps {
  productList: IPrologationProducts[];
  loading: boolean;
  showAppShell: boolean;
  error: IError | null;
  category: string;
  prolongationOpenedFrom: string;
  translation: IProlongationProductDetails;
  prolongationConfiguration: IProlongationConfig;
}

export interface IMapDispatchToProps {
  fetchProlongationProductDetails(
    payload: IProlongationProductDetailedPayload
  ): void;
  setProlongationProductList(payload: IProlongationProductListResponse): void;
}
