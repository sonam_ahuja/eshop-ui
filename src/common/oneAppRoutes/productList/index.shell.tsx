import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const StyledProductShell = styled.div`
  color: #dadada;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  height: 100vh;
  flex-direction: column;
  .mainHeading {
    padding: 2.5rem 0 3rem;
    max-width: 20rem;
    div {
      height: 0.75rem;
      background: currentColor;
      margin-bottom: 0.5rem;
    }
    div:first-child {
      width: 80%;
      margin-bottom: 1rem;
    }
    div:last-child {
      width: 60%;
    }
  }

  .card {
    flex: 1;
    min-height: 8rem;
    width: 100%;
    padding: 0;
    padding-bottom: 0;
    display: flex;
    justify-content: space-evenly;
    flex-direction: column;
    height: 100%;
    /* background: ${colors.cloudGray}; */

    .heading {
      height: 0.5rem;
      width: 90%;
      background: currentColor;
      max-width: 15rem;
    }

    .image {
      width: 100%;
      height: 100%;
      background: currentColor;
    }

    .bottom {
      border-top: 1px solid currentColor;
      padding: 2.25rem;
      margin: 0 -1.25rem;

      ul li {
        height: 0.5rem;
        width: 50%;
        background: currentColor;
        margin-top: 0.25rem;

        &:last-child {
          width: 75%;
        }
      }
      ul + ul {
        margin-top: 1rem;
      }
    }

    .headTitle {
      height: 2rem;
      width: 100%;
      background: ${colors.cloudGray};
      margin-bottom: 0.75rem;
    }
    .info {
      height: 1.5rem;
      width: 100%;
      background: ${colors.cloudGray};
    }

    .list-inline{
      margin-left:-0.75rem;
      margin-top: auto;
      li{
        display:inline-block;
        padding:0 0.75rem;
        width:50%;
        &:last-child{
          padding-right:0;
        }
      }
    }
    .panel{
      display:flex;
      .leftPanel{
      width: calc(100% - 4rem);
      padding-right: 1rem;
    }
    .rightPanel{
      width: 4rem;
      height:100%;
    }
    }

  }

  .row {
    margin: 0;
    display: flex;
    flex-wrap: wrap;
    height:100%;
    .col {
      padding: 1.25rem;
      width: 100%;
      background:${colors.coldGray};
      &.productCardWithImage {
        background: ${hexToRgbA(colors.coldGray, 1)};
        border-radius: 8px;
        margin: -5px 0;
        position: relative;
        border-top:1px solid ${colors.lightishGray};
        border-bottom:1px solid ${colors.lightishGray};
      }
    }
  }

  .mainShellTitleWrap {
    background: ${colors.coldGray};
    padding: 2rem 1.25rem;
    height:8rem;
    border-bottom: 1px solid ${colors.lightishGray};
    .primaryHeadTitle {
      height: 4rem;
      width: 70%;
      background: ${colors.cloudGray};
    }
  }

  .sectionCardShells{
    height:calc(100% - 8rem);
  }
  .shine {
    height: 100%;
    width: 100%;
    margin: 0;
    &:after {
      background: ${colors.silverGray};
    }
  }
`;

const ProductShell = () => {
  return (
    <StyledShell>
      <StyledProductShell>
        <div className='mainShellTitleWrap'>
          <div className='shine primaryHeadTitle' />
        </div>
        <section className='sectionCardShells'>
          <div className='row'>
            <div className='col productCard'>
              <div className='card'>
                <div className='shine headTitle' />
                <div className='shine headTitle' />
                <ul className='list-inline'>
                  <li>
                    <div className='shine info' />
                  </li>
                  <li>
                    <div className='shine info' />
                  </li>
                </ul>
              </div>
            </div>
            <div className='col productCardWithImage'>
              <div className='card'>
                <div className='panel'>
                  <div className='leftPanel'>
                    <div className='shine headTitle' />
                    <div className='shine headTitle' />
                    <ul className='list-inline'>
                      <li>
                        <div className='shine info' />
                      </li>
                      <li>
                        <div className='shine info' />
                      </li>
                    </ul>
                  </div>
                  <div className='rightPanel'>
                    <div className='shine image' />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </StyledProductShell>
    </StyledShell>
  );
};

export default ProductShell;
