import React, { Component, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { RootState } from '@common/store/reducers';
import { connect } from 'react-redux';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { Title } from 'dt-components';
import history from '@client/history';
import { isBrowser, logError } from '@common/utils';
import { PRODUCT_ATTRIBUTE } from '@src/common/routes/productList/store/enum';
import { Helmet } from 'react-helmet';

import { StyledMobileAppProductList, StyledProductHeader } from './styles';
import ProductCard from './product-card/index';
import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IMapDispatchToProps, IMapStateToProps } from './types';
import ProductShell from './index.shell';

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export class ProductList extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.onClickProductCard = this.onClickProductCard.bind(this);
  }

  componentDidMount(): void {
    if (
      isBrowser &&
      document &&
      document.body &&
      typeof document.body.scrollTo === 'function'
    ) {
      document.body.scrollTo(0, 0);
    }
    const parsedQuery = parseQueryString(this.props.location.search);
    this.props.setUuid(parsedQuery.uuid);
    this.props.setProfileId(parsedQuery.profileId);
    try {
      this.props.fetchProductList({
        uuid: parsedQuery.uuid,
        profileId: parsedQuery.profileId,
        channel: parsedQuery.channel,
        variantId: null,
        checkHistoricalLead: true,
        category: parsedQuery.categoryId,
        segment: parsedQuery.segmentId,
        requestedAttributes: [
          PRODUCT_ATTRIBUTE.STORAGE,
          PRODUCT_ATTRIBUTE.COLOUR,
          PRODUCT_ATTRIBUTE.PLAN,
          PRODUCT_ATTRIBUTE.RAM
        ]
      });
    } catch {
      logError('channel or uuid is not passed');
    }
  }

  onClickProductCard(
    productId: string,
    variantId: string,
    variantName: string
  ): void {
    if (history) {
      const parsedQuery = parseQueryString(history.location.search);
      history.push({
        pathname: `/${
          this.props.categoryName
          }/${variantName}/${productId}/${variantId}`,
        search: `categoryId=${
          parsedQuery && parsedQuery.categoryId !== undefined
            ? parsedQuery.categoryId
            : ''
          }`
      });
    }
  }

  render(): ReactNode {
    const { translation, productList, prolongationConfiguration } = this.props;
    const { showAppShell } = this.props;

    return (
      <>
        {showAppShell ? (
          <ProductShell />
        ) : (
            <StyledMobileAppProductList>
              <Helmet
                title={prolongationConfiguration.landingPageTitle}
                meta={[]}
              />
              <StyledProductHeader>
                <Title size='large' weight='ultra'>
                  {translation.heading}
                </Title>
              </StyledProductHeader>
              {Array.isArray(productList) && productList.length ? (
                <ProductCard
                  productData={productList}
                  translation={translation}
                  onClickProductCard={this.onClickProductCard}
                />
              ) : null}
            </StyledMobileAppProductList>
          )}
      </>
    );
  }
}

export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(ProductList)
);
