import actions from '@productList/store/actions';
import { Dispatch } from 'redux';
import { IFetchProlongationProductListPayload } from '@productList/store/types';
import { RootState } from '@common/store/reducers';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  productList: state.productList.prolongationData,
  loading: state.productList.loading,
  showAppShell: state.productList.showAppShell,
  categoryName: state.productList.categoryName,
  error: state.productList.error,
  globalTranslation: state.translation.cart.global,
  translation: state.translation.prolongation.landing,
  currencyConfiguration: state.configuration.cms_configuration.global.currency,
  prolongationConfiguration:
    state.configuration.cms_configuration.modules.prolongation
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  fetchProductList(payload: IFetchProlongationProductListPayload): void {
    dispatch(actions.fetchProlongationProductList(payload));
  },
  setUuid(uuid: string): void {
    dispatch(actions.setUuid(uuid));
  },
  setProfileId(profileId: string): void {
    dispatch(actions.setProfileId(profileId));
  }
});
