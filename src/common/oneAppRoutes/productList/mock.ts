// tslint:disable
export const mock = {
  category: 'Devices-Mobile',
  channel: 'OneApp',
  categoryDescription: 'New phone and Internet From Mock',
  data: [
    {
      productId: 'iphone_x0',
      variantId: 'iphoe_x_b_128',
      name: 'Apple IPhone X0',
      shortDescription: '128GB, Black',
      description: 'desc1 | desc2 | desc3',
      optionValues: ['128GB', 'Silver', '5GB with Unlimited Talk'],
      characteristics: [
        {
          name: 'storage',
          values: [
            {
              value: '32_GB',
              label: '32 GB'
            }
          ]
        },
        {
          name: 'plan',
          values: [
            {
              value: 'bestm',
              label: '5GB Data and121 Unlimited Talk'
            }
          ]
        },
        {
          name: 'colour',
          values: [
            {
              value: 'Purple',
              label: 'Purple'
            }
          ]
        }
      ],
      prices: [
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          recurringChargePeriod: 'month',
          priceType: 'recurringFee',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        },
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          priceType: 'basePrice',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        },
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          priceType: 'upfrontPrice',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        }
      ],
      attachments: {
        thumbnail: [
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
            type: 'picture',
            name: 'frontAndBack'
          }
        ]
      },
      groups: {
        colour: [
          {
            label: 'Purple',
            value: 'Purple',
            enabled: true,
            isSelected: true
          },
          {
            label: 'White',
            value: 'White',
            enabled: true,
            isSelected: false
          },
          {
            label: 'Red',
            value: 'Red',
            enabled: true,
            isSelected: false
          }
        ],
        storage: [
          {
            label: '32 GB',
            value: '32_GB',
            enabled: true,
            isSelected: true
          },
          {
            label: '128 GB',
            value: '128_GB',
            enabled: false,
            isSelected: false
          }
        ],
        plan: [
          {
            label: '5GB Data and Unlimited Talk',
            value: 'bestm',
            enabled: true,
            isSelected: true
          },
          {
            label: '2GB Data 12and Unlimited Talk',
            value: 'bests',
            enabled: false,
            isSelected: false
          }
        ]
      }
    },
    {
      productId: 'iphone_x1',
      variantId: 'iphoe_x_b_127',
      name: 'Apple IPhone X 127GB Yellow',
      shortDescription: '127GB, Yellow',
      description: 'desc1 | desc2 | desc3',
      optionValues: ['128GB', 'Silver', '5GB with Unlimited Talk'],
      characteristics: [
        {
          name: 'storage',
          values: [
            {
              value: '64_GB',
              label: '64 GB'
            }
          ]
        },
        {
          name: 'plan',
          values: [
            {
              value: 'bests',
              label: '2GB Data and Unlimited Talk'
            }
          ]
        },
        {
          name: 'colour',
          values: [
            {
              value: 'Yellow',
              label: 'yellow'
            }
          ]
        }
      ],
      prices: [
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          recurringChargePeriod: 'month',
          priceType: 'recurringFee',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        },
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          priceType: 'basePrice',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        },
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          priceType: 'upfrontPrice',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        }
      ],
      attachments: {
        thumbnail: [
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
            type: 'picture',
            name: 'frontAndBack'
          }
        ]
      },
      groups: {
        colour: [
          {
            label: 'Yellow',
            value: 'Yellow',
            enabled: true,
            isSelected: true
          },
          {
            label: 'White',
            value: 'White',
            enabled: true,
            isSelected: false
          },
          {
            label: 'Red',
            value: 'Red',
            enabled: true,
            isSelected: false
          }
        ],
        storage: [
          {
            label: '64 GB',
            value: '64_GB',
            enabled: true,
            isSelected: true
          },
          {
            label: '128 GB',
            value: '128_GB',
            enabled: false,
            isSelected: false
          }
        ],
        plan: [
          {
            label: '2GB Data and Unlimited Talk',
            value: 'bests',
            enabled: true,
            isSelected: true
          },
          {
            label: '5GB Data and Unlimited Talk',
            value: 'bestm',
            enabled: false,
            isSelected: false
          }
        ]
      }
    },
    {
      productId: 'iphone_x0',
      variantId: 'iphoe_x_b_128',
      name: 'Apple IPhone X 128GB Black',
      shortDescription: '128GB, Black',
      description: 'desc1 | desc2 | desc3',
      optionValues: ['128GB', 'Silver', '5GB with Unlimited Talk'],
      characteristics: [
        {
          name: 'storage',
          values: [
            {
              value: '32_GB',
              label: '32 GB'
            }
          ]
        },
        {
          name: 'plan',
          values: [
            {
              value: 'bestm',
              label: '5GB Data and121 Unlimited Talk'
            }
          ]
        },
        {
          name: 'colour',
          values: [
            {
              value: 'Purple',
              label: 'Purple'
            }
          ]
        }
      ],
      prices: [
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          recurringChargePeriod: 'month',
          priceType: 'recurringFee',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        },
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          priceType: 'basePrice',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        },
        {
          actualValue: 200,
          currency: 'EUR',
          discountedValue: 200,
          priceType: 'upfrontPrice',
          discounts: [],
          dutyFreeValue: 0,
          taxRate: 1,
          totalDiscount: 100
        }
      ],
      attachments: {
        thumbnail: [
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=89_e0_db734cfe2edbe20b5c1ac94f2e7b.jpeg',
            type: 'picture',
            name: 'front'
          },
          {
            url:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=b9_df_2145c11a9f9abde8d05e17ffb9c6.jpeg',
            type: 'picture',
            name: 'frontAndBack'
          }
        ]
      },
      groups: {
        colour: [
          {
            label: 'Purple',
            value: 'Purple',
            enabled: true,
            isSelected: true
          },
          {
            label: 'White',
            value: 'White',
            enabled: true,
            isSelected: false
          },
          {
            label: 'Red',
            value: 'Red',
            enabled: true,
            isSelected: false
          }
        ],
        storage: [
          {
            label: '32 GB',
            value: '32_GB',
            enabled: true,
            isSelected: true
          },
          {
            label: '128 GB',
            value: '128_GB',
            enabled: false,
            isSelected: false
          }
        ],
        plan: [
          {
            label: '5GB Data and Unlimited Talk',
            value: 'bestm',
            enabled: true,
            isSelected: true
          },
          {
            label: '2GB Data 12and Unlimited Talk',
            value: 'bests',
            enabled: false,
            isSelected: false
          }
        ]
      }
    }
  ]
};
