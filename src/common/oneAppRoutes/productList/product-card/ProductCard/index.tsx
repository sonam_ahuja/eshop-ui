import React from 'react';
import cx from 'classnames';
import { IProlongationListing } from '@common/store/types/translation';

import {
  StyledPriceAmount,
  StyledPriceList,
  StyledProductCard,
  StyledProductDescription,
  StyledProductTitle
} from './styles';

export interface IProps {
  className?: string;
  bgColor?: string;
  title: string;
  tarriffDescription: string;
  deviceDescription: string;
  primaryAmount: string;
  secondaryAmount: string;
  currencySymbol: string;
  translation: IProlongationListing;
  onClickProductCard(): void;
}

const ProductCard = (props: IProps) => {
  const {
    className,
    title,
    tarriffDescription,
    deviceDescription,
    primaryAmount,
    secondaryAmount,
    bgColor,
    currencySymbol,
    translation,
    onClickProductCard
  } = props;

  const classes = cx(className);

  return (
    <StyledProductCard
      className={classes}
      style={{ background: bgColor }}
      onClick={onClickProductCard}
    >
      <StyledProductTitle className='productTitle'>{title}</StyledProductTitle>
      <StyledProductDescription className='productDescription'>
        {deviceDescription}
      </StyledProductDescription>
      <StyledProductDescription className='productDescription'>
        {tarriffDescription}
      </StyledProductDescription>
      <StyledPriceList>
        <ul>
          <li>
            <StyledPriceAmount>
              <span className='amount'>
                <span className='value'>{primaryAmount}</span>
                <span className='unit'>
                  {currencySymbol}
                  <span>/{translation.monthlyShort}</span>
                </span>
              </span>
            </StyledPriceAmount>
          </li>
          <li>
            <StyledPriceAmount>
              <span className='amount'>
                <span className='value'>{secondaryAmount}</span>
                <span className='unit'>
                  {currencySymbol}
                  <span>/{translation.nowText}</span>
                </span>
              </span>
            </StyledPriceAmount>
          </li>
        </ul>
      </StyledPriceList>
    </StyledProductCard>
  );
};

export default ProductCard;
