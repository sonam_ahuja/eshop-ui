import React from 'react';
import cx from 'classnames';
import { IProlongationListing } from '@common/store/types/translation';
import { IMAGE_TYPE } from '@common/store/enums';
import PreloadImage from '@src/common/components/Preload';

import {
  StyledLeftPanel,
  StyledPriceAmount,
  StyledPriceList,
  StyledProductCardWithImage,
  StyledProductDescription,
  StyledProductTitle,
  StyledRightPanel
} from './styles';

export interface IProps {
  className?: string;
  bgColor?: string;
  title: string;
  tarriffDescription: string;
  deviceDescription: string;
  primaryAmount: string;
  secondaryAmount: string;
  thumbnailImage: string;
  currencySymbol: string;
  translation: IProlongationListing;
  onClickProductCard(): void;
}

const ProductCardWithImage = (props: IProps) => {
  const {
    className,
    title,
    tarriffDescription,
    deviceDescription,
    primaryAmount,
    secondaryAmount,
    bgColor,
    thumbnailImage,
    currencySymbol,
    translation,
    onClickProductCard
  } = props;

  const classes = cx(className);

  return (
    <StyledProductCardWithImage
      className={classes}
      style={{ background: bgColor }}
      onClick={onClickProductCard}
    >
      <div className='panel clearfix'>
        <StyledLeftPanel>
          <StyledProductTitle className='productTitle'>
            {title}
          </StyledProductTitle>
          <StyledProductDescription className='productDescription'>
            {deviceDescription}
          </StyledProductDescription>
          <StyledProductDescription className='productDescription'>
            {tarriffDescription}
          </StyledProductDescription>
          <StyledPriceList>
            <ul>
              <li>
                <StyledPriceAmount>
                  <span className='amount'>
                    <span className='value'>{primaryAmount}</span>
                    <span className='unit'>
                      {currencySymbol}
                      <span>/{translation.monthlyShort}</span>
                    </span>
                  </span>
                </StyledPriceAmount>
              </li>
              <li>
                <StyledPriceAmount>
                  <span className='amount'>
                    <span className='value'>{secondaryAmount}</span>
                    <span className='unit'>
                      {currencySymbol}
                      <span>/{translation.nowText}</span>
                    </span>
                  </span>
                </StyledPriceAmount>
              </li>
            </ul>
          </StyledPriceList>
        </StyledLeftPanel>
        <StyledRightPanel>
          <div className='img-wrap'>
            <PreloadImage
              isObserveOnScroll={true}
              imageHeight={130}
              width={63}
              mobileWidth={63}
              type={IMAGE_TYPE.WEBP}
              imageWidth={63}
              imageUrl={thumbnailImage}
              loadDefaultImage={true}
              intersectionObserverOption={{
                root: null,
                rootMargin: '60px',
                threshold: 0
              }}
            />
          </div>
        </StyledRightPanel>
      </div>
    </StyledProductCardWithImage>
  );
};

export default ProductCardWithImage;
