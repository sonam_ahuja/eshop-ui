import styled from 'styled-components';
import { Title } from 'dt-components';
import { colors } from '@src/common/variables';

const hexToRgbA = (hex: string, alpha: string | number) => {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);

  return alpha ? `rgba(${r}, ${g}, ${b}, ${alpha})` : `rgba(${r}, ${g}, ${b})`;
};

export const StyledProductTitle = styled(Title).attrs({
  size: 'small',
  weight: 'ultra'
})`
  color: ${colors.white};
`;
export const StyledProductDescription = styled(Title).attrs({
  size: 'xsmall',
  weight: 'medium'
})`
  color: ${colors.cloudGray};
`;

export const StyledPriceAmount = styled.div.attrs({
  size: 'small',
  weight: 'ultra'
})`
  font-size: 1.5rem;
  line-height: 1.75rem;
`;

export const StyledPriceList = styled.div`
  margin: 0.75rem 0 0;
  ul {
    li {
      display: inline-block;
      margin-right: 0.75rem;
      .amount {
        font-weight: 900;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        color: ${colors.black};
      }
      .unit {
        font-weight: 500;
        font-size: 0.625rem;
        line-height: 0.625rem;
        margin-left: 0.25rem;
        span {
          display: block;
        }
      }
      &:last-child {
        margin-right: 0rem;
        .amount {
          font-weight: 500;
        }
      }
    }
  }
`;

export const StyledLeftPanel = styled.div`
  flex: 1;
  padding-right: 1rem;
`;

export const StyledRightPanel = styled.div`
  width: 4rem;
  height: 8.125rem;
  align-self: flex-end;

  .img-wrap {
    height: 100%;
    width: 100%;
    > div {
      height: 100%;
      width: 100%;
      padding: 0%;
    }
    img {
      max-width: 100%;
      max-height: 100%;
    }
  }
`;

export const StyledProductCardWithImage = styled.div`
  .clearfix:after {
    content: ' ';
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  padding: 1.25rem;
  .dt_title {
    max-width: 15rem;
    opacity: 0.7;
  }
  .productTitle {
    opacity: 1;
  }
  .panel {
    display: flex;
    flex-shrink: 0;
  }
  &.darkMagentaCard {
    background: ${hexToRgbA(colors.darkMagenta, 1)};
    border-radius: 8px;
    box-shadow: 0 4px 20px 0 rgba(192, 0, 99, 0.4);
    margin: -5px 0;
    position: relative;
    ul {
      padding-top: 2.875rem;
      .amount {
        color: ${colors.white};
      }
    }
  }
  &.whiteCard {
    background: ${colors.white};
    border-radius: 8px;
    position: relative;
    .productTitle {
      color: ${colors.ironGray};
      font-size: 1.125rem;
      font-weight: 500;
    }
    .productDescription {
      color: ${colors.mediumGray};
      font-size: 1.125rem;
      font-weight: 500;
    }
    .img-wrap {
      margin: 0;
    }
    .amount {
      color: ${colors.shadowGray};
    }
  }
`;
