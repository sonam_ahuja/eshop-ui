import React from 'react';
import { IPrologationProducts } from '@productList/store/types';
import { IPrices as IProlongationPrices } from '@tariff/store/types';
import { PRICE_TYPE } from '@src/common/routes/productDetailed/store/enum';
import { getImage } from '@src/common/routes/productList/utils/getProductDetail';
import htmlKeys from '@common/constants/appkeys';
import { getPriceAndCurrency } from '@src/common/routes/tariff/store/utils';
import { IProlongationListing } from '@common/store/types/translation';
import { getFormatedCurrencyValue } from '@src/common/utils/currency';
import { getCurrencyCode } from '@store/common/index';

import { StyledProductCard } from './styles';
import ProductCardWithImage from './ProductCardWithImage/index';
import ProductCard from './ProductCard/index';

interface IProps {
  productData: IPrologationProducts[];
  translation: IProlongationListing;
  onClickProductCard(
    ProductId: string,
    variantId: string,
    variantName: string
  ): void;
}

const colorCoding = {
  0: 'pictonBlueCard',
  1: 'darkMagentaCard',
  2: 'toryBlueCard'
};

/** USED ON PROLONGATION */
export const getUpfrontPrice = (prices: IProlongationPrices[]): number => {
  let upfrontPrice = 0;
  (prices || []).forEach((price: IProlongationPrices) => {
    if (price && price.priceType === PRICE_TYPE.UPFRONT_PRICE) {
      upfrontPrice = price.discountedValue;
    }
  });

  return upfrontPrice;
};

/** USED ON PROLONGATION */
export const getMonthlyPrice = (prices: IProlongationPrices[]): number => {
  let monthlyPrice = 0;
  (prices || []).forEach((price: IProlongationPrices) => {
    if (price && price.priceType === PRICE_TYPE.RECURRING_FEE) {
      monthlyPrice = price.discountedValue;
    }
  });

  return monthlyPrice;
};

const ProductCardWrap = (props: IProps) => {
  if (Array.isArray(props.productData) && props.productData.length) {
    return (
      <StyledProductCard>
        {props.productData.map((product, index) => {
          if (
            !product ||
            !Array.isArray(product.prices) ||
            product.prices.length === 0
          ) {
            return null;
          }
          const actualUpfrontPrice = getUpfrontPrice(product.prices);
          const upfrontPrice = getFormatedCurrencyValue(actualUpfrontPrice);
          const monthlyPrice = getFormatedCurrencyValue(
            getMonthlyPrice(product.prices)
          );
          const currencyCode = getPriceAndCurrency(
            actualUpfrontPrice,
            getCurrencyCode()
          ).discountCurrency;
          const deviceDescription = product.optionValues
            .filter(value => value)
            .slice()
            .splice(0, 2)
            .toString()
            .replace(',', ', ');
          const tarriffDescription = product.optionValues
            .filter(value => value)
            .slice()
            .splice(2)
            .toString();
          if (index !== 1) {
            return (
              <ProductCard
                key={index}
                title={product.name}
                deviceDescription={deviceDescription}
                tarriffDescription={tarriffDescription}
                primaryAmount={monthlyPrice}
                secondaryAmount={upfrontPrice}
                className={colorCoding[index]}
                currencySymbol={currencyCode}
                translation={props.translation}
                onClickProductCard={() =>
                  props.onClickProductCard(
                    product.productId,
                    product.variantId,
                    product.name
                  )
                }
              />
            );
          } else {
            return (
              <ProductCardWithImage
                key={index}
                title={product.name}
                deviceDescription={deviceDescription}
                tarriffDescription={tarriffDescription}
                primaryAmount={monthlyPrice}
                secondaryAmount={upfrontPrice}
                className={colorCoding[index]}
                thumbnailImage={getImage(
                  product.attachments[htmlKeys.THUMBNAIL_IMAGE],
                  htmlKeys.PRODUCT_IMAGE_FRONT
                )}
                currencySymbol={currencyCode}
                translation={props.translation}
                onClickProductCard={() =>
                  props.onClickProductCard(
                    product.productId,
                    product.variantId,
                    product.name
                  )
                }
              />
            );
          }
        })}
      </StyledProductCard>
    );
  }

  return null;
};

export default ProductCardWrap;
