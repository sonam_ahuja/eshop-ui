import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledMobileAppProductList = styled.div``;

export const StyledProductHeader = styled.div`
  background: ${colors.coldGray};
  padding: 2rem 1.25rem;
  .dt_title {
    color: ${colors.darkGray};
  }
`;
