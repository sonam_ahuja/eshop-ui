import {
  IError,
  IFetchProlongationProductListPayload,
  IPrologationProducts
} from '@productList/store/types';
import {
  ICurrencyConfiguration,
  IProlongationConfig
} from '@common/store/types/configuration';
import {
  IGlobalTranslation,
  IProlongationListing
} from '@common/store/types/translation';

export interface IMapStateToProps {
  productList: IPrologationProducts[];
  loading: boolean;
  showAppShell: boolean;
  categoryName: string;
  error: IError | null;
  translation: IProlongationListing;
  globalTranslation: IGlobalTranslation;
  currencyConfiguration: ICurrencyConfiguration;
  prolongationConfiguration: IProlongationConfig;
}

export interface IMapDispatchToProps {
  fetchProductList(payload: IFetchProlongationProductListPayload): void;
  setUuid(uuid: string): void;
  setProfileId(profileId: string): void;
}
