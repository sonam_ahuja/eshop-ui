export enum PROLONGATION_PAGE {
  THANK_YOU_PAGE = 'Prolongation_OrderPlaced',
  PERSONAL_DETAIL_PAGE = 'Prolongation_PersonalDetails',
  PRODUCT_DETAIL_PAGE = 'Prolongation_ProductDetails',
  LANDING_PAGE = 'Prolongation_Landing'
}

export enum PROLONGATION_ACTION {
  THANK_YOU_PAGE = 'Page_OrderPlaced',
  PERSONAL_DETAIL_PAGE = 'Page_PersonalDetails',
  PRODUCT_DETAIL_PAGE = 'Page_ProductDetails',
  LANDING_PAGE = 'Page_Landing',
  DEVICE_BOUGHT = 'Device_Bought',
  PLAN_BOUGHT = 'Plan_Bought',
  OPTION_CHANGE = 'Option_Change',
  PRODUCT_SELECTED = 'Product_Selected',
  ORDER_SUMMARY_OPENED = 'Order_Summary_Opened',
  ORDER_SUMMARY_CLOSED = 'Order_Summary_Closed',
  ERROR = 'Error'
}

export enum PROLONGATION_EVENT_NAME {
  PAGE_OPEN = 'Bridge_PageOpen',
  USER_ACTION = 'Bridge_UserAction',
  ERROR = 'Error'
}

export enum PROLONGATION_EVENT_VALUE {
  ORDER_CONFIRM = 0
}

export enum PROLONGATION_CONSENT_TAG {
  MANDATORY = 'mobilePostpaid',
  PRESELECTED = 'fixedVoice'
}
