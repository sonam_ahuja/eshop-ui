import { IProlongationEventDetail } from '@src/common/routes/productList/store/types';
import ReactGA from 'react-ga';
import productListActions from '@productList/store/actions';
import store from '@src/common/store';
import { IWindow } from '@src/client';

import {
  PROLONGATION_ACTION,
  PROLONGATION_EVENT_NAME,
  PROLONGATION_EVENT_VALUE,
  PROLONGATION_PAGE
} from './enum';

export const sendForLandingPage = (payload: IProlongationEventDetail) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
    eventAction: PROLONGATION_ACTION.LANDING_PAGE,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.LANDING_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.LANDING_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));

  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForProductDetailPage = (payload: IProlongationEventDetail) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
    eventAction: PROLONGATION_ACTION.PRODUCT_DETAIL_PAGE,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.PRODUCT_DETAIL_PAGE,
    url: payload.pageUrl,
    opened_from: payload.openedFrom,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', event);
  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.PRODUCT_DETAIL_PAGE
  });
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForProductDetailOptionChange = (
  payload: IProlongationEventDetail
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.USER_ACTION,
    eventAction: PROLONGATION_ACTION.OPTION_CHANGE,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.PRODUCT_DETAIL_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.PRODUCT_DETAIL_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForPersonalDetailPage = (
  // landing on personal details page success callback for api
  payload: IProlongationEventDetail
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
    eventAction: PROLONGATION_ACTION.PERSONAL_DETAIL_PAGE,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE,
    url: payload.pageUrl,
    opened_from: payload.openedFrom,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForPersonalDetailProductSelected = (
  // place order click saga call
  payload: IProlongationEventDetail
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.USER_ACTION,
    eventAction: PROLONGATION_ACTION.PRODUCT_SELECTED,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForPersonalDetailSuccess = (
  // place order success
  payload: IProlongationEventDetail
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.USER_ACTION,
    eventAction: PROLONGATION_ACTION.DEVICE_BOUGHT,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.THANK_YOU_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.THANK_YOU_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }

  sendForPersonalDetailPlanSuccess(payload);
};

export const sendForPersonalDetailPlanSuccess = (
  // place order success
  payload: IProlongationEventDetail
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.USER_ACTION,
    eventAction: PROLONGATION_ACTION.PLAN_BOUGHT,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceMonthlyAmt)),
    page: PROLONGATION_PAGE.THANK_YOU_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceMonthlyAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.THANK_YOU_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

/** USED ON PROLONGATION */
export const sendOnOrderConfirmEvent = (payload: {
  url: string;
  gaCode: string;
}) => {
  // Thank you success (didmount)
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
    eventAction: PROLONGATION_ACTION.THANK_YOU_PAGE,
    eventValue: PROLONGATION_EVENT_VALUE.ORDER_CONFIRM,
    page: PROLONGATION_PAGE.THANK_YOU_PAGE,
    url: payload.url
  };
  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.THANK_YOU_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));
  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForOrderSummaryOpen = (payload: IProlongationEventDetail) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.USER_ACTION,
    eventAction: PROLONGATION_ACTION.ORDER_SUMMARY_OPENED,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));

  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForOrderSummaryClosed = (
  payload: IProlongationEventDetail
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.USER_ACTION,
    eventAction: PROLONGATION_ACTION.ORDER_SUMMARY_CLOSED,
    eventLabel: payload.label,
    eventValue: Math.round(Number(payload.deviceUpfrontAmt)),
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE,
    url: payload.pageUrl,
    device_id: payload.deviceId,
    device_name: payload.deviceName,
    device_amt_upfront: payload.deviceUpfrontAmt,
    device_amt_monthly: payload.deviceMonthlyAmt,
    amt: payload.deviceUpfrontAmt,
    currencySymbol: payload.currencySymbol,
    currencyCode: payload.currencyCode
  };

  ReactGA.ga('send', {
    hitType: 'pageview',
    page: PROLONGATION_PAGE.PERSONAL_DETAIL_PAGE
  });
  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));

  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }
};

export const sendForApiError = (
  errorStatus: string,
  errorValue: number,
  errorUrl: string
) => {
  const event = {
    hitType: 'event',
    eventCategory: PROLONGATION_EVENT_NAME.ERROR,
    eventAction: PROLONGATION_ACTION.ERROR,
    eventLabel: errorStatus,
    eventValue: errorValue,
    url: errorUrl
  };

  ReactGA.ga('send', event);
  store.dispatch(productListActions.sendMoengageEvent(event));

  if ((window as IWindow).OneApp) {
    (window as IWindow).OneApp.logEventInOneApp(
      event.eventAction as string,
      JSON.stringify(event)
    );
  }

  // tslint:disable-next-line:max-file-line-count
};
