import {
  IProlongationFormValidation,
  IUserInfo
} from '@src/common/routes/productList/store/types';
import { IProlongationForm } from '@src/common/store/types/configuration';

export function getLabelValueForPPPFormFields(
  isFieldMandatory: boolean,
  fieldLabelValue: string,
  optionalText: string
): string {
  if (!isFieldMandatory && optionalText) {
    fieldLabelValue += ` ${optionalText}`;
  }

  return fieldLabelValue;
}

export function isFormValidFunction(
  userInfo: IUserInfo,
  cmsFormConfiguration: IProlongationForm,
  formValidation: IProlongationFormValidation
): IProlongationFormValidation {
  const updatedForm: IProlongationFormValidation = {
    firstNameValid: formValidation.firstNameValid,
    lastNameValid: formValidation.lastNameValid,
    billingAddressValid: formValidation.billingAddressValid,
    msisdnValid: formValidation.msisdnValid,
    deliveryContactValid: formValidation.deliveryContactValid,
    emailValid: formValidation.emailValid
  };

  Object.keys(cmsFormConfiguration).forEach(field => {
    let formField = field;
    if (field === 'phoneNumber') {
      formField = 'msisdn';
    } else if (field === 'deliveryAddress') {
      formField = 'billingAddress';
    }

    updatedForm[`${formField}Valid`] = isFormFieldValidAsPerType(
      cmsFormConfiguration[field].validation,
      userInfo[formField],
      cmsFormConfiguration[field].mandatory
    );
  });

  return updatedForm;
}

export function isFormFieldValidAsPerType(
  regexValue: string,
  inputValue: string,
  isMandatory: boolean
): boolean {
  if (inputValue !== '') {
    try {
      const newRegex = new RegExp(regexValue);

      return newRegex.test(inputValue);
    } catch (e) {
      return true;
    }
  } else if (isMandatory) {
    return false;
  } else {
    return true;
  }
}
