import { IMegaMenu, IMenusCategory } from '@src/common/store/types/common';
import {
  ITransformedConsentProlongation,
  ITransformedSubConsent
} from '@src/common/routes/productList/store/types';

import { PROLONGATION_CONSENT_TAG } from './enum';

export const transformAgreement = (agreementObj: IMegaMenu) => {
  const transformedConsentArr: ITransformedConsentProlongation[] = [];
  let transformedConsentObj: ITransformedConsentProlongation;
  let transformedSubConsent: ITransformedSubConsent;

  if (agreementObj.categories && agreementObj.categories.length) {
    agreementObj.categories.forEach((mainConsent: IMenusCategory) => {
      transformedConsentObj = {
        name: '',
        description: '',
        isSelected: false,
        isMandatory: false,
        subConsent: []
      };
      if (
        mainConsent.currentNodeMetaData &&
        mainConsent.currentNodeMetaData.data
      ) {
        const cmsData = mainConsent.currentNodeMetaData.data;
        const tagsData = mainConsent.currentNodeMetaData.tags;
        transformedConsentObj.name = cmsData.title;
        transformedConsentObj.description = cmsData.description;
        if (tagsData && tagsData.length) {
          tagsData.forEach((item: string) => {
            switch (item) {
              case PROLONGATION_CONSENT_TAG.MANDATORY:
                transformedConsentObj.isMandatory = true;
                break;

              case PROLONGATION_CONSENT_TAG.PRESELECTED:
                transformedConsentObj.isSelected = true;
                break;

              default:
                break;
            }
          });
        }

        if (mainConsent.childNodes && mainConsent.childNodes.length) {
          let isAllChildSelected = true;
          mainConsent.childNodes.forEach((subConsent: IMenusCategory) => {
            transformedSubConsent = {
              name: '',
              description: '',
              isSelected: false,
              isMandatory: false
            };
            transformedSubConsent.name =
              subConsent.currentNodeMetaData.data.title;
            transformedSubConsent.description =
              subConsent.currentNodeMetaData.data.description;
            const subConsentTag = subConsent.currentNodeMetaData.tags;

            if (subConsentTag && subConsentTag.length) {
              subConsentTag.forEach((item: string) => {
                switch (item) {
                  case PROLONGATION_CONSENT_TAG.MANDATORY:
                    transformedSubConsent.isMandatory = true;
                    break;

                  case PROLONGATION_CONSENT_TAG.PRESELECTED:
                    transformedSubConsent.isSelected = true;
                    break;

                  default:
                    break;
                }
              });
            }

            if (transformedConsentObj.isSelected) {
              transformedSubConsent.isSelected = true;
            }

            if (!transformedSubConsent.isSelected) {
              isAllChildSelected = false;
            }

            transformedConsentObj.subConsent.push(transformedSubConsent);
          });
          if (!isAllChildSelected) {
            transformedConsentObj.isSelected = false;
          }
        }

        transformedConsentArr.push(transformedConsentObj);
      }
    });

    return transformedConsentArr;
  }

  return transformedConsentArr;
};

export const canPlaceOrderAsPerConsents = (
  consentArr: ITransformedConsentProlongation[],
  applyConsentDiscount: boolean
): boolean => {
  let mandatoryFlag = true;

  if (applyConsentDiscount && consentArr && consentArr.length) {
    consentArr.forEach((itemConsent: ITransformedConsentProlongation) => {
      if (itemConsent.isMandatory && !itemConsent.isSelected) {
        mandatoryFlag = false;
      }

      if (itemConsent.subConsent && itemConsent.subConsent.length) {
        itemConsent.subConsent.forEach(
          (subConsentItem: ITransformedSubConsent) => {
            if (subConsentItem.isMandatory && !subConsentItem.isSelected) {
              mandatoryFlag = false;
            }
          }
        );
      }
    });
  }

  return mandatoryFlag;
};
