import React, { FunctionComponent, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ProductList from '@common/oneAppRoutes/productList';
import ProductDetailed from '@src/common/oneAppRoutes/productDetailed';
import Checkout from '@src/common/oneAppRoutes/checkout';
import Loadable, { LoadingComponentProps } from 'react-loadable';

export interface IProps extends RouteComponentProps<IRouterParams> {}
const loading: FunctionComponent<LoadingComponentProps> = () => null;

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

interface IState {
  isProductListRoutePresent: boolean;
  isProductDetailedRoutePresent: boolean;
  isProductCheckoutPresent: boolean;
}

const NotFound = Loadable({
  loading,
  loader: () => import('./routes/notFound')
});

export class SwitchRenderRoute extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isProductListRoutePresent: false,
      isProductDetailedRoutePresent: false,
      isProductCheckoutPresent: false
    };
    this.routeCheck = this.routeCheck.bind(this);
  }

  componentWillMount(): void {
    this.routeCheck();
  }

  routeCheck(): void {
    const { history } = this.props;
    if (history.location.pathname) {
      const arrPaths = history.location.pathname.split('/');
      if (arrPaths.length === 2) {
        // We are adding this check because for the listing page,
        // we always have routes like www.eshop.com/device/mobile-device or www.eshop.com/tvs
        this.setState({
          isProductListRoutePresent: true,
          isProductDetailedRoutePresent: false,
          isProductCheckoutPresent: false
        });
      } else if (arrPaths.length === 5) {
        // We are adding this check because for the product detailed page,
        // we always have routes like www.eshop.com/variant-name/product-id/variant-id
        this.setState({
          isProductListRoutePresent: false,
          isProductDetailedRoutePresent: true,
          isProductCheckoutPresent: false
        });
      } else if (arrPaths.length === 7) {
        // We are adding this check because for the product checkout page of prolongation,
        // we always have routes like www.eshop.com/categoryslug/variant-name/product-id/variant-id/checkout/roderDetails
        this.setState({
          isProductListRoutePresent: false,
          isProductDetailedRoutePresent: false,
          isProductCheckoutPresent: true
        });
      }
    }
  }

  componentDidUpdate(prevProps: IProps): void {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.routeCheck();
    }
  }

  render(): ReactNode {
    const {
      isProductListRoutePresent,
      isProductDetailedRoutePresent,
      isProductCheckoutPresent
    } = this.state;

    return (
      <>
        {isProductListRoutePresent && <ProductList />}
        {isProductDetailedRoutePresent && <ProductDetailed />}
        {isProductCheckoutPresent && <Checkout />}
        {!isProductListRoutePresent &&
          !isProductDetailedRoutePresent &&
          !isProductCheckoutPresent && <NotFound />}
      </>
    );
  }
}

export default withRouter(SwitchRenderRoute);
