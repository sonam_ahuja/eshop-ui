import { createGlobalStyle } from 'styled-components';
import isMobilejs from 'ismobilejs';

import { breakpoints, colors } from './variables';
import { StyledOrderStickyWrap } from './oneAppRoutes/checkout/styles';
import { StyledOrderPanelWrap } from './oneAppRoutes/checkout/order-panel/styles';
// tslint:disable
export default createGlobalStyle`


*[tabindex]{
  outline: none;
}

::-moz-selection { /* Code for Firefox */
  background: transparent;
}

html{
  -webkit-overflow-scrolling: touch;
  overscroll-behavior: contain;
}
body{
  /* overflow-x: hidden prevents sticky from working but its fine for pppm,
  this code css overwrites in common for eshop */
  overflow-x: hidden;
  width: 100%;
}

/* for ppp */
.hideStickyFooter{
  .hideOnInputFocus{
  ${StyledOrderStickyWrap},
  ${StyledOrderPanelWrap}{
      position: static;
    }
  }
}

/* ::selection {
  background: transparent;
} */

/* .styles__SummaryContentCompactWrapper-jctqrb-5{
  transition: 0.2s linear 0s !important;
} */
/* .hideDrawer{
  .isSticky{
  transform: translateY(0%) !important;
  position: relative !important;
  }
} */
.checkoutMobileFooterDrawer {
    width: 100%;
    display: flex;
    flex: 1;
    align-items: center;
    padding: 2.5rem;

    height: 56px;
    background-color: #000;

    font-size: 12px;
    line-height: 1.33;

    .copy {
      color: ${colors.mediumGray};
      margin-right: 1rem;
    }
    .tnc {
      color: ${colors.ironGray};
    }

    @media (min-width: ${breakpoints.desktop}px) {
      display: none;
    }
  }


/* LandscapeModePlaceholder */



  ${
    isMobilejs.phone
      ? `
   @media (min-width: 480px) and (orientation: landscape) {
    body{
      overflow: hidden;
    }
    div.landscapeModePlaceholder{
      display: flex;
    }
    div.styledAppInner{
      display: none;
    }
  }
  `
      : null
  }


/*** RESET CSS ***/
html {
  font-family: 'TeleGrotesk Next',  sans-serif, Arial !important;
}

@media (min-width: 1366px) {
  html {
    font-size: 20px !important;
  }
}

@media (max-width: 1365px) {
  html {
    font-size: 16px !important;
  }
}

button:focus {
  outline: none;
}

/*********** Product Detailed Plan Modal Body Scroll False CSS 03-06-2019 ***********/
body{
  &.overflowHide{
    overflow:hidden;
    position:fixed;
    width:100%;
  }
}
/*********** Product Detailed Plan Modal Body Scroll False CSS 03-06-2019 ***********/



*,
*::after,
*::before {
  box-sizing: border-box;
  font-family: 'TeleGrotesk Next',  sans-serif, Arial;
  -webkit-tap-highlight-color: transparent;
}

html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
b,
u,
i,
center,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
canvas,
details,
embed,
figure,
figcaption,
footer,
header,
hgroup,
menu,
nav,
output,
ruby,
section,
summary,
time,
mark,
audio,
video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
menu,
nav,
section {
  display: block;
}

body {
  line-height: 1;
  text-rendering: optimizeLegibility;
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
}

ol,
ul {
  list-style: none;
}

a {
  color: inherit;
  text-decoration: none;
}

blockquote,
q {
  quotes: none;
}

blockquote::before,
blockquote::after,
q::before,
q::after {
  content: '';
  content: none;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}

/*** slick slider ***/
/* Slider */
.slick-slider
{
    position: relative;

    display: block;
    box-sizing: border-box;

    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;

    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
        touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
}

.slick-list
{
    position: relative;

    display: block;
    overflow: hidden;

    margin: 0;
    padding: 0;
}
.slick-list:focus
{
    outline: none;
}
.slick-list.dragging
{
    cursor: pointer;
    cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
       -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
         -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
}

.slick-track
{
    position: relative;
    top: 0;
    left: 0;

    display: block;
}
.slick-track:before,
.slick-track:after
{
    display: table;

    content: '';
}
.slick-track:after
{
    clear: both;
}
.slick-loading .slick-track
{
    visibility: hidden;
}

.slick-slide
{
    display: none;
    float: left;

    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.slick-slide img
{
    display: block;
}
.slick-slide.slick-loading img
{
    display: none;
}
.slick-slide.dragging img
{
    pointer-events: none;
}
.slick-initialized .slick-slide
{
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;

    height: auto;

    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}
#binkies-on-page,
#binkies-in-modal
{
	transition: opacity 0.5s, visibility 0.5s;
}
body.binkies-show #binkies-on-page,
body.binkies-show #binkies-in-modal{
	opacity: 1;
	visibility: visible;
}
body.binkies-hide #binkies-on-page,
body.binkies-hide #binkies-in-modal
{
	opacity: 0;
	visibility: hidden;
}
.binkies-bar{
  visibility: hidden;
}

/* hide scroll bar */
body::-webkit-scrollbar{
  display: none;
  width: 0 !important ;
  scrollbar-width: none;
  overflow: -moz-scrollbars-none;
  -ms-overflow-style: none;
}

*{
  /** Binkies Page CSS */

  #binkies-on-page.outOfStock{
  opacity: 0.6;
  }
  @media only screen and (max-width: 767px){
    #binkies-on-page.outOfStock{
      opacity: 0.4;
    }
  }
}



#binkies-on-page{transition:0.5s linear 0s; opacity:0;}
.styledImageViewer{transition:0.5s linear 0s; opacity:1;}

body.binkies-show{
  #binkies-on-page{
    opacity:1;
  }
  .styledImageViewer{
    /* opacity:0; */
  }
}
body.binkies-hide{
  #binkies-on-page{
    opacity:0;
  }
  .styledImageViewer{
    opacity:1;
  }
}

img {
  color: transparent;
}


/* managing tags */
strong,
  b {
    font-weight: bold;
  }

  em,
  i {
    font-style: italic;
  }

  img:-moz-loading {
  visibility: hidden;
  }

  img {
  &:not([src]) {
    visibility: hidden !important;
    display: none;
  }
}
`;
