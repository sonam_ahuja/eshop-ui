import React, { FunctionComponent } from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';
import SwitchRenderRoute from '@common/switchRenderRoute';
import { isCategoryLandingPageRequired } from '@common/store/common/index';
import appRoutes from '@common/constants/routes';

const loading: FunctionComponent<LoadingComponentProps> = () => null;

const Home = Loadable({
  loading,
  loader: () => import('./routes/home')
});

const Category = Loadable({
  loading,
  loader: () => import('./routes/category')
});
// tslint:disable-next-line: no-commented-code
// const ProductList = Loadable({
//   loading,
//   loader: () =>
//     import('./routes/productList')
// });
// const ProductDetailed = Loadable({
//   loading,
//   loader: () =>
//     import('./routes/productDetailed')
// });

const Search = Loadable({
  loading,
  loader: () => import('./routes/search')
});

const Basket = Loadable({
  loading,
  loader: () => import('./routes/basket')
});

const Checkout = Loadable({
  loading,
  loader: () => import('./routes/checkout')
});

const PersonalInfo = Loadable({
  loading,
  loader: () => import('./routes/checkout/CheckoutSteps/PersonalInfo')
});

const Shipping = Loadable({
  loading,
  loader: () => import('./routes/checkout/CheckoutSteps/Shipping')
});

const Billing = Loadable({
  loading,
  loader: () => import('./routes/checkout/CheckoutSteps/Billing')
});

const Payment = Loadable({
  loading,
  loader: () => import('./routes/checkout/CheckoutSteps/Payment')
});

const OrderReview = Loadable({
  loading,
  loader: () => import('./routes/checkout/OrderReview')
});

const Authentication = Loadable({
  loading,
  loader: () => import('./routes/authentication')
});

const Tariff = Loadable({
  loading,
  loader: () => import('./routes/tariff')
});

const Error = Loadable({
  loading,
  loader: () =>
    // tslint:disable-next-line:no-any
    import('./routes/error') as Promise<any>
});

const NotFound = Loadable({
  loading,
  loader: () => import('./routes/notFound')
});

const OrderConfirmation = Loadable({
  loading,
  loader: () => import('./routes/orderConfirmation')
});

const categoryLandingPage = isCategoryLandingPageRequired()
  ? [
      {
        path: appRoutes.CATEGORY,
        basePath: appRoutes.CATEGORY,
        component: Category
      }
    ]
  : [];

const routes: IRoutes[] = [
  {
    path: '/',
    basePath: '/',
    exact: true,
    component: Home
  },
  {
    path: appRoutes.BASKET,
    basePath: appRoutes.BASKET,
    exact: true,
    component: Basket
  },
  {
    path: appRoutes.CHECKOUT.BASE,
    basePath: appRoutes.CHECKOUT.BASE,
    component: Checkout,
    childRoutes: [
      {
        path: appRoutes.CHECKOUT.PERSONAL_INFO,
        component: PersonalInfo,
        exact: true
      },
      {
        path: appRoutes.CHECKOUT.SHIPPING,
        component: Shipping,
        exact: true
      },
      {
        path: appRoutes.CHECKOUT.BILLING,
        component: Billing,
        exact: true
      },
      {
        path: appRoutes.CHECKOUT.PAYMENT,
        component: Payment,
        exact: true
      },
      {
        path: appRoutes.CHECKOUT.ORDER_REVIEW,
        component: OrderReview,
        exact: true
      }
    ]
  },
  ...categoryLandingPage,
  {
    path: appRoutes.ORDER_CONFIRMATION,
    basePath: appRoutes.ORDER_CONFIRMATION,
    exact: true,
    component: OrderConfirmation
  },
  {
    path: appRoutes.AUTHENTICATION,
    basePath: appRoutes.AUTHENTICATION,
    exact: true,
    component: Authentication
  },
  // {
  //   path: appRoutes.PRODUCT_LIST,
  //   basePath: appRoutes.PRODUCT_LIST,
  //   exact: true,
  //   component: ProductList
  // },
  // {
  //   path: appRoutes.PRODUCT_DETAIL,
  //   basePath: appRoutes.PRODUCT_DETAIL,
  //   exact: true,
  //   component: ProductDetailed
  // },
  {
    path: appRoutes.SEARCH,
    basePath: appRoutes.SEARCH,
    exact: true,
    component: Search
  },
  {
    path: appRoutes.TARIFF,
    basePath: appRoutes.TARIFF,
    exact: true,
    component: Tariff
  },
  {
    path: appRoutes.ERROR,
    basePath: appRoutes.ERROR,
    exact: true,
    component: Error
  },
  {
    path: appRoutes.NOT_FOUND,
    basePath: appRoutes.NOT_FOUND,
    exact: true,
    component: NotFound
  },
  {
    path: '',
    exact: true,
    component: SwitchRenderRoute
  }
];

export default routes;

export interface IRoutes {
  childRoutes?: IRoutes[];
  // headerFooterIncluded?: boolean;
  // mainHeaderFooterIncluded?: boolean;
  basePath?: string;
  path?: string;
  exact?: boolean;
  // tslint:disable-next-line:no-any
  component?: React.ComponentType<any>;
  loadData?(): Generator;
}
