import {
  IThirdPartyVerificaation,
  IVerifyUsingProvidersRequest
} from '@src/common/store/types/common';
import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';

import { IState as IConfirmYourIdentityIdentity } from './types';

export const createPayloadForTelekom = (
  state: IConfirmYourIdentityIdentity,
  setPhoneNumber: (phoneNumber: string) => void,
  userIdentityVerificationDetails: IThirdPartyVerificaation
): IVerifyUsingProvidersRequest => {
  const identificationProviders =
    userIdentityVerificationDetails.identificationProviders;
  const selectedProvider = state.selectedProviderMethod;
  const identificationProvidersDetails =
    identificationProviders && identificationProviders[selectedProvider]
      ? identificationProviders[selectedProvider]
      : null;

  setPhoneNumber(state.serviceIdInputValue);

  return {
    providerId: identificationProvidersDetails
      ? identificationProvidersDetails.providerSublist[0].providerId
      : '',
    providerType: IDENTITY_VERIFICATION_PROVIDERS.telekom,
    bankId: null,
    serviceId: state.serviceIdInputValue,
    personalId: state.peselId
  };
};

export const createPayloadForBank = (
  state: IConfirmYourIdentityIdentity,
  setBankName: (bankName: string) => void
): IVerifyUsingProvidersRequest => {
  setBankName(IDENTITY_VERIFICATION_PROVIDERS.bank);

  return {
    providerId: state.selectedBank.providerId,
    providerType: IDENTITY_VERIFICATION_PROVIDERS.bank,
    bankId: state.selectedBank.bankId ? state.selectedBank.bankId : null,
    serviceId: null
  };
};

export const createPayloadForCourier = (
  state: IConfirmYourIdentityIdentity,
  userIdentityVerificationDetails: IThirdPartyVerificaation
): IVerifyUsingProvidersRequest => {
  const identificationProviders =
    userIdentityVerificationDetails.identificationProviders;
  const selectedProvider = state.selectedProviderMethod;
  const identificationProvidersDetails =
    identificationProviders && identificationProviders[selectedProvider]
      ? identificationProviders[selectedProvider]
      : null;

  return {
    providerId: identificationProvidersDetails
      ? identificationProvidersDetails.providerSublist[0].providerId
      : '',
    providerType: selectedProvider
  };
};
