import { RootState } from '@common/store/reducers';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { Provider } from 'react-redux';
import { ConfirmYourIdentity } from '@authentication/LoginWithUser/ConfirmYourIdentity';
import { IComponentProps as IConfirmYourIdentityProps } from '@authentication/LoginWithUser/ConfirmYourIdentity/types';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/ConfirmYourIdentity/mapsProps';
import { IVerifyUsingProvidersRequest } from '@src/common/store/types/common';
import { histroyParams } from '@mocks/common/histroy';
import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';

// tslint:disable-next-line:no-big-function
describe('<EnterLoginCredentials />', () => {
  const identificationProviders = {
    COURIER: {
      providerType: '',
      providerSublist: [
        {
          providerId: '',
          bankId: '',
          displayValue: ''
        }
      ]
    },
    BANK: {
      providerType: '',
      providerSublist: [
        {
          providerId: '',
          bankId: '',
          displayValue: ''
        }
      ]
    },
    TELEKOM: {
      providerType: '',
      providerSublist: [
        {
          providerId: '',
          bankId: '',
          displayValue: ''
        }
      ]
    }
  };
  const mockStore = configureStore();
  const componentProps: IConfirmYourIdentityProps = {
    ...histroyParams,
    className: 'someClasss',
    userIdentityVerificationDetails: appState().common
      .userIdentityVerificationDetails,
    identityVerificationTranslation: appState().translation.cart.login
      .identityVerification,
    configuration: appState().configuration.cms_configuration.global
      .loginFormRules,
    getIdentityVerificationDetails: jest.fn(),
    verifyUser: jest.fn(),
    setPhoneNumber: jest.fn(),
    setIfUserBankAutheticated: jest.fn(),
    setLoadingText: jest.fn(),
    goToNextRoute: jest.fn(),
    setBankName: jest.fn(),
    bankName: 'back',
    userName: 'user',
    isPeselFieldNeeded: false
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IConfirmYourIdentityProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ConfirmYourIdentity {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(componentProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when verification provider change', () => {
    const newProps: IConfirmYourIdentityProps = {
      ...componentProps
    };
    const component = shallow(<ConfirmYourIdentity {...newProps} />);
    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.courier
    });
    component.update();
    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.telekom
    });
    component.update();
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when pesel form is needed', () => {
    const newProps: IConfirmYourIdentityProps = {
      ...componentProps,
      isPeselFieldNeeded: true
    };
    const component = shallow(<ConfirmYourIdentity {...newProps} />);
    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.telekom
    });
    component.update();
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when verification provider change', () => {
    const newProps: IConfirmYourIdentityProps = {
      ...componentProps
    };
    newProps.userIdentityVerificationDetails = {
      verificationRequired: true,
      identificationProviders
    };
    const component = shallow(<ConfirmYourIdentity {...newProps} />);
    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.courier
    });
    component.update();
    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.telekom
    });
    component.update();
    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const payload: IVerifyUsingProvidersRequest = {
      providerId: '12',
      bankId: '12',
      providerType: 'providerType',
      serviceId: 'serviceId'
    };
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).getIdentityVerificationDetails();
    mapDispatchToProps(dispatch).verifyUser(payload);
    mapDispatchToProps(dispatch).setPhoneNumber('9998075168');
    mapDispatchToProps(dispatch).setIfUserBankAutheticated(true);
    mapDispatchToProps(dispatch).setLoadingText('text');
    mapDispatchToProps(dispatch).goToNextRoute();
    mapDispatchToProps(dispatch).setBankName('ICICI');
  });

  test('action trigger', () => {
    const onFormEvent = {
      currentTarget: { value: '' },
      preventDefault: jest.fn()
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ConfirmYourIdentity {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).componentWillReceiveProps(
      componentProps
    );
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).handleKeydown(
      (onFormEvent as unknown) as React.KeyboardEvent<HTMLInputElement>
    );
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).checkButtonDisability();
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).verifyUser();
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).createPayload();
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).getSelectedItem({
      id: 1,
      title: 'string',
      disabled: true,
      rest: 'string',
      stringKey: 'string'
    });
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).getSelectedBank({
      id: 1,
      title: 'string',
      disabled: true,
      rest: 'string',
      stringKey: 'string'
    });
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).getModalTitle();
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).getModalTitle();
  });

  test('action trigger for component did mount', () => {
    const newProps = { ...componentProps };

    newProps.userIdentityVerificationDetails = {
      verificationRequired: true,
      identificationProviders
    };
    newProps.location.search =
      '?productOfferingBenefit=0&productOfferingTerm=agreement12&loading=true';

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ConfirmYourIdentity {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    identificationProviders.BANK.providerSublist[0].bankId = '123456';
    identificationProviders.BANK.providerSublist[0].displayValue = 'Bank 1';

    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.telekom
    });
    component.update();

    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).componentWillReceiveProps(newProps);
  });
  test('action trigger for render component with bank value ', () => {
    const newProps = { ...componentProps };

    newProps.userIdentityVerificationDetails = {
      verificationRequired: true,
      identificationProviders
    };
    newProps.location.search =
      '?productOfferingBenefit=0&productOfferingTerm=agreement12&loading=true';

    identificationProviders.BANK.providerSublist[0].bankId = '123456';
    identificationProviders.BANK.providerSublist[0].displayValue = 'Bank 1';

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ConfirmYourIdentity {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    identificationProviders.BANK.providerSublist[0].bankId = '';
    identificationProviders.BANK.providerSublist[0].displayValue = '';

    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).componentWillReceiveProps(newProps);

    const event = {
      currentTarget: { value: '' },
      keyCode: 10,
      preventDefault: jest.fn()
    };
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).onInputChange(
      // tslint:disable-next-line:no-any
      event as any,
      'serviceIdInputValue'
    );
    event.currentTarget.value = 'value';

    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).onInputChange(
      // tslint:disable-next-line:no-any
      event as any,
      'serviceIdInputValue'
    );
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).createPayload();

    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).verifyUser();
    component.setState({
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.bank
    });
    component.update();
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).createPayload();
    event.keyCode = 13;
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).handleKeydown(
      (event as unknown) as React.KeyboardEvent<HTMLInputElement>
    );
    event.keyCode = 32;
    event.currentTarget.value = '  ';
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).handleKeydown(
      (event as unknown) as React.KeyboardEvent<HTMLInputElement>
    );
    event.keyCode = 48;
    event.currentTarget.value = '12';
    (component
      .find('ConfirmYourIdentity')
      .instance() as ConfirmYourIdentity).handleKeydown(
      (event as unknown) as React.KeyboardEvent<HTMLInputElement>
    );
  });
  // tslint:disable-next-line:max-file-line-count
});
