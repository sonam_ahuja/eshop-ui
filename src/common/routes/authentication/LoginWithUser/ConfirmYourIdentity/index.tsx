import { connect } from 'react-redux';
import React, { Component, FormEvent, ReactNode } from 'react';
import { RootState } from '@src/common/store/reducers';
import { Button, Section, Title } from 'dt-components';
import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';
import { IVerifyUsingProvidersRequest } from '@src/common/store/types/common';
import { withRouter } from 'react-router';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import {
  charEPresent,
  ignoreSpaces
} from '@checkout/CheckoutSteps/PersonalInfo/utils';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/ConfirmYourIdentity/mapsProps';
import LoginActions from '@authentication/common/LoginActions';
import {
  StyledHeaderTextWrap,
  StyledInputWrap
} from '@authentication/common/styles';
import EVENT_NAME, { PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';
import { logError } from '@src/common/utils';

// tslint:disable:max-file-line-count
import { StyledConfirmYourIdentity } from './styles';
import {
  IComponentDispatchToProps,
  IComponentProps,
  IComponentStateToProps,
  IState
} from './types';
import {
  createPayloadForBank,
  createPayloadForCourier,
  createPayloadForTelekom
} from './createPayload';
import { getNextElement, getProvidersDropdownElement } from './utils';

type msisdnVerificationType = 'serviceIdInputValue' | 'peselId';

export class ConfirmYourIdentity extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      isVerificationNeeded: false,
      selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS.bank,
      selectedProviderId: '',
      peselId: '',
      serviceIdInputValue: '',
      selectedBank: {
        bankId: '',
        providerId: '',
        title: ''
      }
    };
    this.getSelectedItem = this.getSelectedItem.bind(this);
    this.verifyUser = this.verifyUser.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.handleKeydown = this.handleKeydown.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.getSelectedBank = this.getSelectedBank.bind(this);
  }

  componentWillReceiveProps(nextProps: IComponentProps): void {
    const userIdentityVerificationDetails =
      nextProps.userIdentityVerificationDetails;
    this.setIfVerificationNeed(
      userIdentityVerificationDetails.verificationRequired
    );
    try {
      const identificationProviders = userIdentityVerificationDetails.identificationProviders
        ? userIdentityVerificationDetails.identificationProviders
        : undefined;
      if (identificationProviders) {
        const providerMethod = Object.keys(
          identificationProviders as IDENTITY_VERIFICATION_PROVIDERS
        )[0];

        if (providerMethod === this.state.selectedProviderMethod) {
          this.setState({
            selectedProviderMethod: providerMethod
          });
        }
      }
    } catch (error) {
      logError(`there is no provider method present ${error}`);
    }
  }

  setIfVerificationNeed(isVerificationNeeded: boolean): void {
    this.setState({
      isVerificationNeeded
    });
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.CONFIRM_YOUR_IDENTITY);
    const params = new URLSearchParams(this.props.location.search);
    if (params.get('loading')) {
      this.props.setIfUserBankAutheticated(true);
      this.props.setLoadingText(this.getModalTitle());
      this.props.goToNextRoute();
    }
    const userIdentityVerificationDetails = this.props
      .userIdentityVerificationDetails;
    this.setIfVerificationNeed(
      userIdentityVerificationDetails.verificationRequired
    );
    try {
      const identificationProviders = userIdentityVerificationDetails.identificationProviders
        ? userIdentityVerificationDetails.identificationProviders
        : undefined;
      const validators =
        identificationProviders &&
        Object.keys(identificationProviders) &&
        Object.keys(identificationProviders).length
          ? identificationProviders
          : undefined;
      const isBankValidator = validators
        ? validators[IDENTITY_VERIFICATION_PROVIDERS.bank]
        : undefined;
      const firstBank =
        isBankValidator &&
        Array.isArray(isBankValidator.providerSublist) &&
        isBankValidator.providerSublist.length
          ? isBankValidator.providerSublist[0]
          : undefined;
      if (identificationProviders) {
        this.setState({
          selectedProviderMethod: Object.keys(
            identificationProviders
          )[0] as IDENTITY_VERIFICATION_PROVIDERS
        });
      }
      if (firstBank) {
        this.setState({
          selectedBank: {
            bankId: firstBank.bankId as string,
            providerId: firstBank.providerId,
            title: firstBank.displayValue || ''
          }
        });
      }
    } catch (error) {
      logError(`there is error in prviders wwith error : ${error}`);
    }
  }

  getModalTitle(): ReactNode {
    const { identityVerificationTranslation } = this.props;

    return (
      <>
        <div>
          {identityVerificationTranslation.hi} {this.props.userName},
        </div>
        <div>
          {identityVerificationTranslation.modalMessage.replace(
            '{0}',
            this.props.bankName
          )}
        </div>
      </>
    );
  }

  onInputChange(
    event: React.ChangeEvent<HTMLInputElement>,
    fieldName: msisdnVerificationType
  ): void {
    const inputFieldValue = event.currentTarget.value;
    const inputFieldValueLength = inputFieldValue.trim().length;

    switch (fieldName) {
      case 'serviceIdInputValue':
        this.setState({
          serviceIdInputValue:
            inputFieldValue && inputFieldValueLength ? inputFieldValue : ''
        });
        break;
      case 'peselId':
        this.setState({
          peselId:
            inputFieldValue && inputFieldValueLength ? inputFieldValue : ''
        });
        break;
      default:
    }
  }

  getSelectedBank(selectedBank: IListItem): void {
    this.setState({
      selectedBank: {
        bankId: selectedBank.id ? (selectedBank.id as string) : '',
        providerId: selectedBank.providerId as string,
        title: selectedBank.title
      }
    });
  }

  getSelectedItem(item: IListItem): void {
    this.setState({
      selectedProviderMethod: item.title as IDENTITY_VERIFICATION_PROVIDERS,
      selectedProviderId: item.id
    });
  }

  createPayload(): IVerifyUsingProvidersRequest | null {
    const { userIdentityVerificationDetails, setPhoneNumber } = this.props;
    const identificationProviders =
      userIdentityVerificationDetails.identificationProviders;
    const selectedProvider = this.state.selectedProviderMethod;
    if (identificationProviders) {
      switch (selectedProvider) {
        case IDENTITY_VERIFICATION_PROVIDERS.courier:
          return createPayloadForCourier(
            this.state,
            userIdentityVerificationDetails
          );
        case IDENTITY_VERIFICATION_PROVIDERS.bank:
          return createPayloadForBank(this.state, this.props.setBankName);
        case IDENTITY_VERIFICATION_PROVIDERS.telekom:
          return createPayloadForTelekom(
            this.state,
            setPhoneNumber,
            userIdentityVerificationDetails
          );
        default:
          return null;
      }
    }

    return null;
  }

  verifyUser(): void {
    const payload = this.createPayload();
    if (payload) {
      this.props.verifyUser(payload);
    }
  }

  checkButtonDisability(): boolean {
    const serviceIdInputValue = this.state.serviceIdInputValue.trim();
    const peselId = this.state.peselId.trim();
    if (
      this.state.selectedProviderMethod ===
      IDENTITY_VERIFICATION_PROVIDERS.telekom
    ) {
      return this.props.isPeselFieldNeeded
        ? !(serviceIdInputValue.length && peselId.length)
        : !serviceIdInputValue.length;
    }

    return false;
  }

  handleKeydown(event: React.KeyboardEvent<HTMLInputElement>): void {
    if (ignoreSpaces(event) || charEPresent(event)) {
      event.preventDefault();

      return;
    }
  }

  onSubmit(event: FormEvent<HTMLFormElement>): void {
    event.stopPropagation();
    this.verifyUser();
  }

  render(): ReactNode {
    const { identityVerificationTranslation } = this.props;
    const { userIdentityVerificationDetails } = this.props;

    return (
      <>
        {this.state.isVerificationNeeded ? (
          <LoginActions
            showLogo={false}
            showBackLink={false}
            showCloseButton={true}
          >
            <StyledConfirmYourIdentity>
              <StyledHeaderTextWrap>
                <div className='sectionTop'>
                  <Title size='small' weight='ultra'>
                    <div className='primaryText'>
                      {identityVerificationTranslation.confirm}
                    </div>
                    <div className='secondaryText'>
                      {identityVerificationTranslation.yourIndetity}
                    </div>
                  </Title>
                  <Section className='description' size='large' weight='normal'>
                    {identityVerificationTranslation.selectVerificationMessage}
                  </Section>
                </div>
              </StyledHeaderTextWrap>
              <StyledInputWrap>
                {getProvidersDropdownElement(
                  this.state,
                  identityVerificationTranslation,
                  userIdentityVerificationDetails,
                  this.getSelectedItem
                )}
                {getNextElement(
                  this.props.isPeselFieldNeeded,
                  this.state,
                  identityVerificationTranslation,
                  userIdentityVerificationDetails,
                  this.onInputChange,
                  this.handleKeydown,
                  this.getSelectedBank
                )}
                <div className='buttonWrap'>
                  <Button
                    size='medium'
                    onClickHandler={evt => {
                      evt.persist();
                      this.verifyUser();
                    }}
                    data-event-id={
                      EVENT_NAME.AUTHENTICATE.EVENTS.VERIFICATION_NEXT
                    }
                    data-event-message={identityVerificationTranslation.next}
                    disabled={this.checkButtonDisability()}
                  >
                    {identityVerificationTranslation.next}
                  </Button>
                </div>
              </StyledInputWrap>
            </StyledConfirmYourIdentity>
          </LoginActions>
        ) : null}
      </>
    );
  }
}

export default withRouter(
  connect<IComponentStateToProps, IComponentDispatchToProps, void, RootState>(
    mapStateToProps,
    mapDispatchToProps
  )(ConfirmYourIdentity)
);
