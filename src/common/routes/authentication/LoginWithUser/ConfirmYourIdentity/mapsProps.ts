import { RootState } from '@src/common/store/reducers';
import { Dispatch } from 'redux';
import { commonAction } from '@store/actions';
import actions from '@authentication/store/actions';
import { ReactNode } from 'react';
import { IVerifyUsingProvidersRequest } from '@src/common/store/types/common';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  userIdentityVerificationDetails: state.common.userIdentityVerificationDetails,
  identityVerificationTranslation:
    state.translation.cart.login.identityVerification,
  userName: state.common.userDetails.relatedParties[0].name,
  bankName: state.authentication.bankName,
  configuration: state.configuration.cms_configuration.global.loginFormRules,
  isPeselFieldNeeded:
    state.configuration.cms_configuration.modules.checkout.identityVerification
      .isPeselfieldNeeded
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  getIdentityVerificationDetails(): void {
    dispatch(commonAction.fetchIdentityVerificationDetails());
  },
  verifyUser(payload: IVerifyUsingProvidersRequest): void {
    dispatch(commonAction.verifyUserIdentity(payload));
  },
  setPhoneNumber(payload: string): void {
    dispatch(actions.setPhoneNumber(payload));
  },
  setIfUserBankAutheticated(payload: boolean): void {
    dispatch(actions.setIfUserBankAutheticated(payload));
  },
  setLoadingText(loaderText: ReactNode): void {
    dispatch(actions.setLoadingTextForProgressModal(loaderText));
  },
  goToNextRoute(): void {
    dispatch(commonAction.goToNextRoute());
  },
  setBankName(bankName: string): void {
    dispatch(actions.setBankName(bankName));
  }
});
