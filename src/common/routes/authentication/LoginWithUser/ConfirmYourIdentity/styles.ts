import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledConfirmYourIdentity = styled.div`
  /* sectionTop START */
  .sectionTop {
    .dt_title {
      .primaryText {
        color: ${colors.darkGray};
      }
      .secondaryText {
        color: ${colors.mediumGray};
      }
    }

    .description {
      margin-top: 0.25rem;
      color: ${colors.ironGray};
    }
  }
  /* sectionTop END */

  .buttonWrap {
    padding-top: 1.5rem;
  }

  .inputWrap {
    margin-bottom: 1rem;
    &.description {
      color: ${colors.ironGray};
    }
  }

  /* SELECTWITH LABEL START */
  .selectWithLabel {
    margin-bottom: 1.35rem;

    .label {
      color: ${colors.mediumGray};
      margin-bottom: 0.1875rem;
    }
    .dt_select {
      border-bottom: 1px solid ${colors.silverGray};
      padding-bottom: 0.35rem;
      .dt_outsideClick {
        display: flex;
        /* styles__StyledSimpleSelect */
        .styledSimpleSelect {
          display: flex;
          flex: 1;
          justify-content: space-between;

          .dt_paragraph {
            font-size: 1.25rem;
            font-weight: bold;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.2;
            letter-spacing: normal;
          }
        }
      }
    }
  }
  /* SELECTWITH LABEL END */

  @media (min-width: ${breakpoints.desktop}px) {
    .styledHeaderTextWrap {
      min-height: 10rem;
    }
    /* sectionTop START */
    .sectionTop {
      .description {
        margin-top: 0.5rem;
      }
    }
    /* sectionTop END */

    /* SELECTWITH LABEL START */
    .selectWithLabel {
      margin-bottom: 1rem;
      .dt_select {
        .dt_outsideClick {
          .styledSimpleSelect {
            .dt_paragraph {
              font-size: 1rem;
            }

            & + .optionsList {
              width: 100%;
            }
          }
        }
      }
    }
    /* SELECTWITH LABEL END */
  }
`;
