import {
  IThirdPartyVerificaation,
  IVerifyUsingProvidersRequest
} from '@src/common/store/types/common';
import { ILoginFormRules } from '@src/common/store/types/configuration';
import { IIdentityVerification } from '@src/common/store/types/translation';
import { ReactNode } from 'react';
import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';
import { RouteComponentProps } from 'react-router';

export type msisdnVerificationType = 'serviceIdInputValue' | 'peselId';

export interface IComponentStateToProps {
  userIdentityVerificationDetails: IThirdPartyVerificaation;
  identityVerificationTranslation: IIdentityVerification;
  userName: string;
  bankName: string;
  configuration: ILoginFormRules;
  isPeselFieldNeeded: boolean;
}

export interface IComponentDispatchToProps {
  getIdentityVerificationDetails(): void;
  verifyUser(payload: IVerifyUsingProvidersRequest): void;
  setPhoneNumber(payload: string): void;
  setIfUserBankAutheticated(payload: boolean): void;
  setLoadingText(loaderText: ReactNode): void;
  goToNextRoute(): void;
  setBankName(bankName: string): void;
}

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IState {
  isVerificationNeeded: boolean;
  selectedProviderMethod: IDENTITY_VERIFICATION_PROVIDERS;
  selectedProviderId: string | number;
  serviceIdInputValue: string;
  peselId: string;
  selectedBank: {
    bankId: string;
    providerId: string;
    title?: string;
  };
}

export interface IProps {
  className?: string;
}

export type IComponentProps = IComponentDispatchToProps &
  IComponentStateToProps &
  IProps &
  RouteComponentProps<IRouterParams>;
