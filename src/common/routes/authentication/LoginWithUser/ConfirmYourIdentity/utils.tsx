import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';
import React, { ReactNode } from 'react';
import { FloatLabelInput, Section, Select } from 'dt-components';
import SimpleSelect from '@src/common/components/Select/SimpleSelect';
import { isMobile } from '@src/common/utils';
import { IIdentityVerification } from '@src/common/store/types/translation';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import {
  IProviderSublist,
  IThirdPartyVerificaation
} from '@src/common/store/types/common';
import { sendDropdownClickEvent } from '@src/common/events/common';

import {
  IState as IConfirmYourIdentityIdentity,
  msisdnVerificationType
} from './types';

export const getNextElement = (
  isPeselFieldNeeded: boolean,
  state: IConfirmYourIdentityIdentity,
  identityVerificationTranslation: IIdentityVerification,
  userIdentityVerificationDetails: IThirdPartyVerificaation,
  onInputChange: (
    event: React.ChangeEvent<HTMLInputElement>,
    fieldName: msisdnVerificationType
  ) => void,
  handleKeydown: (event: React.KeyboardEvent<HTMLInputElement>) => void,
  getSelectedBank: (selectedBank: IListItem) => void
): ReactNode => {
  const { selectedBank } = state;
  const providerType = state.selectedProviderMethod;
  switch (providerType) {
    case IDENTITY_VERIFICATION_PROVIDERS.bank:
      return (
        <div className='selectWithLabel inputWrap'>
          <Section
            firstLetterTransform='uppercase'
            className='label'
            size='small'
            weight='normal'
          >
            {identityVerificationTranslation.bank}
          </Section>
          <Select
            useNativeDropdown={isMobile.phone}
            items={getBankList(userIdentityVerificationDetails)}
            selectedItem={{
              id: selectedBank.providerId,
              title: selectedBank.title || ''
            }}
            onItemSelect={item => {
              sendDropdownClickEvent(item.title);
              getSelectedBank(item);
            }}
            SelectedItemComponent={selectedItemProps => (
              <>
                <SimpleSelect
                  {...selectedItemProps}
                  isOpen
                  selectedItem={{
                    id:
                      selectedItemProps && selectedItemProps.selectedItem
                        ? selectedItemProps.selectedItem.id
                        : '',
                    title:
                      selectedItemProps && selectedItemProps.selectedItem
                        ? selectedItemProps.selectedItem.title
                        : ''
                  }}
                />
              </>
            )}
          />
        </div>
      );
    case IDENTITY_VERIFICATION_PROVIDERS.courier:
      return (
        <Section size='large' weight='normal' className='inputWrap description'>
          {identityVerificationTranslation.courierDeliveryMessage}
        </Section>
      );
    case IDENTITY_VERIFICATION_PROVIDERS.telekom:
      return (
        <>
          <FloatLabelInput
            label={identityVerificationTranslation.phoneNumberLabel}
            type='number'
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              onInputChange(event, 'serviceIdInputValue')
            }
            value={state.serviceIdInputValue}
            onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
              handleKeydown(event);
            }}
          />
          {isPeselFieldNeeded ? (
            <FloatLabelInput
              label={identityVerificationTranslation.peselLabel}
              type='text'
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                onInputChange(event, 'peselId')
              }
              value={state.peselId}
            />
          ) : null}
        </>
      );
    default:
      return null;
  }
};

export const getBankList = (
  userIdentityVerificationDetails: IThirdPartyVerificaation
): IListItem[] => {
  const bankList: IListItem[] = [];
  const identificationProviders =
    userIdentityVerificationDetails.identificationProviders;

  const validators =
    identificationProviders &&
    Object.keys(identificationProviders) &&
    Object.keys(identificationProviders).length
      ? identificationProviders
      : undefined;

  const isBankValidator = validators
    ? validators[IDENTITY_VERIFICATION_PROVIDERS.bank]
    : undefined;

  const banks =
    isBankValidator &&
    Array.isArray(isBankValidator.providerSublist) &&
    isBankValidator.providerSublist.length
      ? isBankValidator.providerSublist
      : undefined;

  if (banks) {
    banks.forEach((bank: IProviderSublist) => {
      const providerItem: IListItem = {
        id: bank.bankId ? bank.bankId : '',
        title: bank.displayValue ? bank.displayValue : '',
        providerId: bank.providerId
      };
      bankList.push(providerItem);
    });
  }

  return bankList;
};

export const getIdentityVerificationProviders = (
  userIdentityVerificationDetails: IThirdPartyVerificaation
): IListItem[] => {
  const verificationProviders: IListItem[] = [];
  const identificationProviders =
    userIdentityVerificationDetails.identificationProviders;
  if (
    identificationProviders &&
    Object.keys(identificationProviders) &&
    Object.keys(identificationProviders).length
  ) {
    Object.keys(identificationProviders).forEach((provider, index) => {
      const providerItem: IListItem = {
        id: index,
        title: provider
      };
      verificationProviders.push(providerItem);
    });
  }

  return verificationProviders;
};

export const getProvidersDropdownElement = (
  state: IConfirmYourIdentityIdentity,
  identityVerificationTranslation: IIdentityVerification,
  userIdentityVerificationDetails: IThirdPartyVerificaation,
  getSelectedItem: (item: IListItem) => void
): ReactNode => {
  return (
    <div className='selectWithLabel inputWrap'>
      <Section
        firstLetterTransform='uppercase'
        className='label'
        size='small'
        weight='normal'
      >
        {identityVerificationTranslation.method}
      </Section>
      <Select
        useNativeDropdown={isMobile.phone}
        items={getIdentityVerificationProviders(
          userIdentityVerificationDetails
        )}
        onItemSelect={item => getSelectedItem(item)}
        selectedItem={{
          id: state.selectedProviderId,
          title: state.selectedProviderMethod
        }}
        SelectedItemComponent={selectedItemProps => (
          <>
            <SimpleSelect
              {...selectedItemProps}
              isOpen
              selectedItem={{
                id:
                  selectedItemProps && selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.id
                    : '',
                title:
                  selectedItemProps && selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.title
                    : ''
              }}
            />
          </>
        )}
      />
    </div>
  );
};
