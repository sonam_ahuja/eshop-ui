import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { RootState } from '@common/store/reducers';
import { Provider } from 'react-redux';
import appState from '@store/states/app';
import {
  EmailConfirmation,
  IComponentProps as IEmailConfirmationProps
} from '@authentication/LoginWithUser/EmailConfirmation';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/EmailConfirmation/mapProps';
import actions from '@authentication/store/actions';
import checkoutActions from '@checkout/store/personalInfo/actions';
import '@babel/polyfill';

describe('<EmailConfirmation />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  const mockStore = configureStore();

  const componentProps: IEmailConfirmationProps = {
    translation: appState().translation.cart.login,
    resetEmail: jest.fn(),
    proceedAsGuest: false,
    configuration: appState().configuration.cms_configuration.global
      .loginFormRules,
    email: 'email',
    identityVerification: true,
    buttonLoading: false,
    commonError: appState().common.error,
    setPersonalInfoEmail: jest.fn(),
    loginViaRegistration: jest.fn(),
    closeLoginModal: jest.fn(),
    setButtonLoading: jest.fn(),
    removeGenricError: jest.fn()
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EmailConfirmation {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when valid loginStep query is preset and user is not logged in ', () => {
    const newProps: IEmailConfirmationProps = { ...componentProps, email: '' };
    const component = shallow(<EmailConfirmation {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('all function test', () => {
    const copyComponentProps: IEmailConfirmationProps = { ...componentProps };
    copyComponentProps.proceedAsGuest = true;
    const inputEvent = { currentTarget: { value: 'avinash' } };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EmailConfirmation {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('EmailConfirmation')
      .instance() as EmailConfirmation).onInputChange(
      'email',
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('EmailConfirmation')
      .instance() as EmailConfirmation).onInputChange(
      'number',
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('EmailConfirmation')
      .instance() as EmailConfirmation).inputValidation();
    (component
      .find('EmailConfirmation')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as EmailConfirmation).onSubmit({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      // tslint:disable-next-line: no-empty
      stopPropagation: () => {},
      // tslint:disable-next-line: no-empty
      persist: () => {}
    } as React.FormEvent<HTMLFormElement>);
    // @
    // (component
    //   .find('EmailConfirmation')
    //   .instance() as EmailConfirmation).onNavigate();

    (component
      .find('EmailConfirmation')
      .instance() as EmailConfirmation).componentWillUnmount();

    const form = component.find('form').at(0);
    const children = form
      .render()
      .children()
      .children();
    form.simulate('submit', { target: { children } });
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).setPersonalInfoEmail('XXXXXX@gmail.com');
    mapDispatchToProps(dispatch).closeLoginModal();
    mapDispatchToProps(dispatch).loginViaRegistration();
    mapDispatchToProps(dispatch).resetEmail();
    mapDispatchToProps(dispatch).setButtonLoading(false);
    expect(dispatch.mock.calls[0][0]).toEqual(
      checkoutActions.setSelectedEmail('XXXXXX@gmail.com')
    );
    expect(dispatch.mock.calls[1][0]).toEqual(actions.closeLoginModal());
    expect(dispatch.mock.calls[2][0]).toEqual(actions.loginViaRegistration());
    expect(dispatch.mock.calls[3][0]).toEqual(actions.setEmail(''));
    expect(dispatch.mock.calls[4][0]).toEqual(actions.setButtonLoading(false));
  });
});
