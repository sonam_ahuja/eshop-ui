import * as userRegistrationUsingEmailApi from '@common/types/api/authentication/userRegistrationUsingEmail';
import React, { Component, FormEvent, ReactNode } from 'react';
import { connect } from 'react-redux';
import SocialMediaLogins from '@authentication/common/SocialMediaLogins';
import { StyledInputWrap } from '@authentication/common/styles';
import { RootState } from '@src/common/store/reducers';
import LoginActions from '@authentication/common/LoginActions';
import getErrorMessage from '@authentication/utils/getErrorMessage';
import { emailConfirmationValidation } from '@src/common/routes/authentication/utils/emailConfirmationValidation';
import { StyledEmailConfirmation } from '@authentication/LoginWithUser/EmailConfirmation/styles.ts';
import { userRegistrationService } from '@authentication/store/services';
import { Button, FloatLabelInput, Title } from 'dt-components';
import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IProps,
  IState
} from '@authentication/LoginWithUser/EmailConfirmation/types';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/EmailConfirmation/mapProps';
import { logError } from '@src/common/utils';
import { PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';
import {
  sendRegistraionSuccessEvent,
  sendRegistrationFailedEvent
} from '@events/login/index';
import { LOGIN_TYPES } from '@authentication/store/enum';
import history from '@src/client/history';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';
import { loginQueryParam, loginSteps } from '@authentication/constants';

export type IComponentProps = IComponentDispatchToProps &
  IComponentStateToProps &
  IProps;

export class EmailConfirmation extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    const { email } = this.props;
    this.state = {
      emailValue: email,
      emailMessage: '',
      isEmailError: false,
      confirmEmailValue: '',
      confirmEmailMessage: '',
      isConfirmEmailError: false,
      isNextButtonDisable: true
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.inputValidation = this.inputValidation.bind(this);
    this.onNavigate = this.onNavigate.bind(this);
  }

  componentWillMount(): void {
    const username = this.props.email;
    if (history && !username) {
      history.replace({
        pathname: history.location.pathname,
        search: updateQueryStringParameter(
          history.location.search,
          loginQueryParam,
          loginSteps.ENTER_LOGIN_CREDENTIAL
        )
      });
    }
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.EMAIL_CONFIRMATION);
  }

  componentWillUnmount(): void {
    this.props.resetEmail();
    this.props.removeGenricError();
  }

  onSubmit(event: FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    event.stopPropagation();
    event.persist();
    this.onNavigate();
  }

  inputValidation(): void {
    const { emailValue, confirmEmailValue } = this.state;
    const { configuration, translation } = this.props;
    const returnValue: IState = emailConfirmationValidation(
      emailValue,
      confirmEmailValue,
      translation,
      configuration
    );
    this.setState({
      ...returnValue
    });
  }

  onInputChange(
    type: string,
    event: React.ChangeEvent<HTMLInputElement>
  ): void {
    const inputFieldValue = event.currentTarget.value;
    if (type === 'email') {
      this.setState(
        {
          emailValue: inputFieldValue
        },
        () => this.inputValidation()
      );
    } else {
      this.setState(
        {
          confirmEmailValue: inputFieldValue
        },
        () => this.inputValidation()
      );
    }
  }

  onNavigate(): void {
    const { setPersonalInfoEmail, loginViaRegistration } = this.props;
    const { setButtonLoading } = this.props;
    const { emailValue } = this.state;
    setButtonLoading(true);
    userRegistrationService({ email: emailValue }, true)
      .then((success: userRegistrationUsingEmailApi.POST.IResponse | Error) => {
        if (success !== undefined) {
          sendRegistraionSuccessEvent(LOGIN_TYPES.TELEKOM_LOGIN);
          setPersonalInfoEmail(emailValue);
          loginViaRegistration();
        }
      })
      .catch(error => {
        sendRegistrationFailedEvent(LOGIN_TYPES.TELEKOM_LOGIN);
        logError(`error in user registraition ${error}`);
      })
      .finally(() => {
        setButtonLoading(false);
      });
  }

  render(): ReactNode {
    const { proceedAsGuest, translation, buttonLoading } = this.props;
    const {
      creatingNew,
      account,
      speedUpCheckout,
      placeHolderForEmailInput,
      placeHolderForConfirmInput
    } = this.props.translation.loginEmailConfirmation;
    const {
      emailValue,
      emailMessage,
      isEmailError,
      confirmEmailValue,
      confirmEmailMessage,
      isConfirmEmailError,
      isNextButtonDisable
    } = this.state;
    const errorMessage = getErrorMessage({}, this.props.commonError);
    const nextButton = (
      <Button
        className='button'
        disabled={isNextButtonDisable}
        size='medium'
        loading={buttonLoading}
      >
        {translation.common.next}
      </Button>
    );
    const inputFormElement = (
      <StyledInputWrap>
        <form onSubmit={this.onSubmit}>
          <FloatLabelInput
            type={'text'}
            label={placeHolderForEmailInput}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              this.onInputChange('email', event);
            }}
            error={isEmailError}
            errorMessage={emailMessage}
            autoComplete={'off'}
            isFocused={false}
            value={emailValue}
            disabled={true}
          />
          <FloatLabelInput
            type={'text'}
            label={placeHolderForConfirmInput}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              this.onInputChange('confirmEmail', event);
            }}
            onPasteCapture={(event: React.ClipboardEvent<HTMLInputElement>) => {
              event.preventDefault();
            }}
            error={isConfirmEmailError || !!errorMessage}
            errorMessage={confirmEmailMessage || errorMessage}
            autoComplete={'off'}
            isFocused={false}
            value={confirmEmailValue}
          />
          {nextButton}
        </form>
      </StyledInputWrap>
    );

    return (
      <LoginActions
        showLogo={proceedAsGuest}
        showBackLink
        showCloseButton={!proceedAsGuest}
      >
        <StyledEmailConfirmation className='styledEmailConfirmation'>
          <Title className='heading' size='small' weight='ultra'>
            {creatingNew} <span className={'active'}>{account}</span>
            <div className='subHeading'>{speedUpCheckout}</div>
          </Title>
          {inputFormElement}
        </StyledEmailConfirmation>
        <SocialMediaLogins />
      </LoginActions>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(EmailConfirmation);
