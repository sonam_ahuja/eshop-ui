import { Dispatch } from 'redux';
import { RootState } from '@src/common/store/reducers';
import actions from '@authentication/store/actions';
import checkoutActions from '@checkout/store/personalInfo/actions';
import commonAction from '@common/store/actions/common';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  configuration: state.configuration.cms_configuration.global.loginFormRules,
  translation: state.translation.cart.login,
  buttonLoading: state.authentication.buttonLoading,
  email: state.authentication.email.value,
  identityVerification:
    state.configuration.cms_configuration.modules.checkout.identityVerification
      .enabled,
  commonError: state.common.error
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  setPersonalInfoEmail(email: string): void {
    dispatch(checkoutActions.setSelectedEmail(email));
  },
  closeLoginModal(): void {
    dispatch(actions.closeLoginModal());
  },
  loginViaRegistration(): void {
    dispatch(actions.loginViaRegistration());
  },
  setButtonLoading(buttonLoading: boolean): void {
    dispatch(actions.setButtonLoading(buttonLoading));
  },
  resetEmail(): void {
    dispatch(actions.setEmail(''));
  },
  removeGenricError(): void {
    dispatch(commonAction.removeGenricError());
  }
});
