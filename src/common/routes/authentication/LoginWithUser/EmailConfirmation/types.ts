import { ILoginTranslation } from '@src/common/store/types/translation';
import { ILoginFormRules } from '@src/common/store/types/configuration';
import { IError as IGenricError } from '@src/common/store/types/common';

export interface IProps {
  proceedAsGuest?: boolean;
}

export interface IComponentStateToProps {
  configuration: ILoginFormRules;
  translation: ILoginTranslation;
  email: string;
  identityVerification: boolean;
  buttonLoading: boolean;
  commonError: IGenricError;
}

export interface IComponentDispatchToProps {
  setPersonalInfoEmail(email: string): void;
  loginViaRegistration(): void;
  closeLoginModal(): void;
  setButtonLoading(buttonLoading: boolean): void;
  resetEmail(): void;
  removeGenricError(): void;
}

export interface IState {
  emailValue: string;
  emailMessage: string;
  isEmailError: boolean;
  confirmEmailValue: string;
  confirmEmailMessage: string;
  isConfirmEmailError: boolean;
  isNextButtonDisable: boolean;
}
