import { RootState } from '@common/store/reducers';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { Provider } from 'react-redux';
import { mapStateToProps } from '@authentication/LoginWithUser/EnterLoginCredentials/mapProps';
import {
  EnterLoginCredentials,
  IComponentProps as IEnterLoginCredentialsProps
} from '@authentication/LoginWithUser/EnterLoginCredentials';

describe('<EnterLoginCredentials />', () => {
  const mockStore = configureStore();
  const componentProps: IEnterLoginCredentialsProps = {
    translation: appState().translation.cart.login
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IEnterLoginCredentialsProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterLoginCredentials {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(componentProps);
    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...componentProps };
    const inputEvent = { currentTarget: { value: 'avinash' } };
    const component = componentWrapper(copyComponentProps);
    const input = component.find('input');
    input.simulate('change', inputEvent);
  });
});
