import { connect } from 'react-redux';
import { RootState } from '@src/common/store/reducers';
import { Section, Title } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import { mapStateToProps } from '@authentication/LoginWithUser/EnterLoginCredentials/mapProps';
import { IComponentStateToProps } from '@authentication/LoginWithUser/EnterLoginCredentials/types';
import LoginInputForm from '@authentication/common/LoginInputForm';
import { pageViewEvent } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';

export type IComponentProps = IComponentStateToProps;

export class EnterLoginCredentials extends Component<IComponentProps> {
  constructor(props: IComponentProps) {
    super(props);
    this.getTitle = this.getTitle.bind(this);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.ENTER_LOGIN_CREDENTIALS);
  }

  getTitle(): ReactNode {
    const { translation } = this.props;

    return (
      <>
        <Title tag='h1' className='heading' size='small' weight='ultra'>
          {translation.loginUserNamePassword.speedUp}{' '}
          {translation.loginUserNamePassword.yourCheckout}
          <div className='subHeading'>
            {translation.loginUserNamePassword.signIn}
            <span> {translation.loginUserNamePassword.or} </span>
            {translation.loginUserNamePassword.register}
          </div>
        </Title>

        <Section tag='h2' size='large'>
          <div> {translation.loginUserNamePassword.generalInfo1} </div>
          <div>{translation.loginUserNamePassword.generalInfo2}</div>
        </Section>
      </>
    );
  }

  render(): ReactNode {
    return <LoginInputForm title={this.getTitle()} isBackRequired={false} />;
  }
}
export default connect<IComponentStateToProps, void, void, RootState>(
  mapStateToProps,
  undefined
)(EnterLoginCredentials);
