import { RootState } from '@src/common/store/reducers';

import { IComponentStateToProps } from './types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  translation: state.translation.cart.login
});
