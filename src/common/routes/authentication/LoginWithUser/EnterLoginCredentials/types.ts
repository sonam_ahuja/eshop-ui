import { ILoginTranslation } from '@src/common/store/types/translation';

export interface IComponentStateToProps {
  translation: ILoginTranslation;
}
