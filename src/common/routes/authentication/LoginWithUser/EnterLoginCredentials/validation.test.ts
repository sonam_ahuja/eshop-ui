import {
  checkPhoneNumberValidation,
  checkUsernameValidation
} from '@authentication/LoginWithUser/EnterLoginCredentials/validation';
import appState from '@store/states/app';

describe('<Validation />', () => {
  const loginFormRules = appState().configuration.cms_configuration.global
    .loginFormRules;
  test('checkPhoneNumberValidation test', () => {
    expect(
      checkPhoneNumberValidation(
        loginFormRules,
        '9876543221',
        appState().translation.cart.login,
        jest.fn()
      )
    ).toBeDefined();

    loginFormRules.phoneNumber.regex = '/asa';
    expect(
      checkPhoneNumberValidation(
        loginFormRules,
        '987654',
        appState().translation.cart.login,
        jest.fn()
      )
    ).toBeDefined();
  });

  test('checkUsernameValidation test', () => {
    expect(
      checkUsernameValidation(
        appState().configuration.cms_configuration.global.loginFormRules,
        'enter',
        appState().translation.cart.login,
        jest.fn()
      )
    ).toBeDefined();
    loginFormRules.userName.regex = '/asa';
    expect(
      checkUsernameValidation(
        loginFormRules,
        'enter',
        appState().translation.cart.login,
        jest.fn()
      )
    ).toBeDefined();
  });
});
