import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { isFormFieldValidAsPerType } from '@src/common/routes/checkout/CheckoutSteps/PersonalInfo/utils';
import { ILoginFormRules } from '@src/common/store/types/configuration';
import { ILoginTranslation } from '@src/common/store/types/translation';
import { IError } from '@authentication/store/types';

export const checkPhoneNumberValidation = (
  configuration: ILoginFormRules,
  enteredValue: string,
  translation: ILoginTranslation,
  setErrorMessage: (error: IError) => void
): boolean => {
  let regexValidation = true;
  let phoneValidation = true;
  if (configuration.phoneNumber.regex) {
    regexValidation = isFormFieldValidAsPerType(
      configuration.phoneNumber.regex,
      enteredValue,
      VALIDATION_TYPE.REGEX
    );
  }
  phoneValidation =
    regexValidation &&
    isFormFieldValidAsPerType(
      configuration.phoneNumber.minLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MIN
    ) &&
    isFormFieldValidAsPerType(
      configuration.phoneNumber.maxLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MAX
    );

  if (!phoneValidation) {
    setErrorMessage({
      message: translation.loginUserNamePassword.mobileNumberInvalid
    });

    return false;
  } else {
    return true;
  }
};

export const checkUsernameValidation = (
  configuration: ILoginFormRules,
  enteredValue: string,
  translation: ILoginTranslation,
  setErrorMessage: (error: IError) => void
): boolean => {
  let regexValidation = true;
  let usernameValidation = true;
  if (configuration.userName.regex) {
    regexValidation = isFormFieldValidAsPerType(
      configuration.userName.regex,
      enteredValue,
      VALIDATION_TYPE.REGEX
    );
  }

  usernameValidation =
    regexValidation &&
    isFormFieldValidAsPerType(
      configuration.userName.minLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MIN
    ) &&
    isFormFieldValidAsPerType(
      configuration.userName.maxLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MAX
    );

  if (!usernameValidation) {
    setErrorMessage({
      message: translation.loginUserNamePassword.userNameInvalid
    });

    return false;
  } else {
    return true;
  }
};
