import React from 'react';
import { RootState } from '@common/store/reducers';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';
import appState from '@store/states/app';

import { EnterOtp, IComponentProps as IEnterOtpProps } from '.';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

describe('<EnterOtp />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  const mockStore = configureStore();

  const componentProps: IEnterOtpProps = {
    translation: appState().translation.cart.login,
    loginWithEmailOtp: jest.fn(),
    isUserLoggedIn: true,
    validateUserUsingOtp: jest.fn(),
    loginWithServiceIdOtp: jest.fn(),
    activeStep: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
    loginType: LOGIN_TYPES.MSISDN_OTP,
    waitingPeriod: 300,
    resendOtpRequest: jest.fn(),
    verifyUser: jest.fn(),
    setEmail: jest.fn(),
    setMsisdn: jest.fn(),
    commonError: appState().common.error,
    removeGenricError: jest.fn(),
    userIdentityVerificationDetails: { verificationRequired: false },
    globalAuthenticationConfiguration: {
      encryption: { publicKey: '' },
      numberofOtp: 1,
      apiLatency: 3000,
      masking: {
        email: '',
        phone: ''
      }
    }
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterOtp {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('with Login Type EMAIL_OTP & globalAuthenticationConfiguration', () => {
    const props = {
      ...componentProps,
      globalAuthenticationConfiguration: {
        encryption: { publicKey: '' },
        numberofOtp: 1,
        apiLatency: 3000,
        masking: {
          email: 'testemail@gmail.com',
          phone: '9898989899'
        }
      }
    };
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterOtp {...props} loginType={LOGIN_TYPES.EMAIL_OTP} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const payload = {
      PIN: '12344',
      context: 'context',
      device: { id: '1233', browser: 'string' },
      nonce: 'nonce',
      serviceId: '12323',
      serviceType: 'emo'
    };
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).loginWithEmailOtp(payload);
    mapDispatchToProps(dispatch).loginWithServiceIdOtp(payload);
    mapDispatchToProps(dispatch).resendOtpRequest(LOGIN_TYPES.EMAIL_OTP);
    mapDispatchToProps(dispatch).validateUserUsingOtp({
      action: 'string',
      mobileNumber: 'string',
      nonce: 'string',
      otp: 'string',
      otpContext: 'string'
    });
    mapDispatchToProps(dispatch).verifyUser({
      providerId: 'string',
      bankId: 'string',
      providerType: 'string',
      serviceId: 'string'
    });
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...componentProps };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterOtp {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('EnterOtp').instance() as EnterOtp).getTitleText('hello');
    (component.find('EnterOtp').instance() as EnterOtp).showSendAgain();
    (component.find('EnterOtp').instance() as EnterOtp).getOtpValue('12345');
    (component.find('EnterOtp').instance() as EnterOtp).getTitleText(
      'someTitle'
    );
    (component.find('EnterOtp').instance() as EnterOtp).reSendCall();
    (component.find('EnterOtp').instance() as EnterOtp).makeLoginRequest();
    (component.find('EnterOtp').instance() as EnterOtp).componentWillUnmount();
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component.find('EnterOtp').instance() as EnterOtp).onSubmit({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      // tslint:disable-next-line: no-empty
      stopPropagation: () => {},
      // tslint:disable-next-line: no-empty
      persist: () => {}
    } as React.FormEvent<HTMLFormElement>);
  });
});
