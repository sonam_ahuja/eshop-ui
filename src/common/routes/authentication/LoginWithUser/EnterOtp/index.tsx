import CONSTANTS from '@common/constants/appConstants';
import { RootState } from '@src/common/store/reducers';
import SocialMediaLogins from '@authentication/common/SocialMediaLogins';
import {
  StyledHeaderTextWrap,
  StyledInputWrap
} from '@authentication/common/styles';
import SendAgain from '@authentication/common/SendAgain';
import React, { Component, FormEvent, Fragment, ReactNode } from 'react';
import { connect } from 'react-redux';
import { Button, OtpInput, Title } from 'dt-components';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as verifyUserUsingOtpApi from '@common/types/api/identityVerification/validateUserUsingOtp';
import LoginActions from '@authentication/common/LoginActions';
import clearGenricError from '@authentication/utils/clearGenricError';
import getErrorMessage from '@authentication/utils/getErrorMessage';
import { LOGIN_SERVICE_TYPE, LOGIN_TYPES } from '@authentication/store/enum';
import { encrypt } from '@authentication/utils/encryption';
import { IVerifyUsingProvidersRequest } from '@src/common/store/types/common';
import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';
import authenticationConstants, {
  loginQueryParam,
  loginSteps
} from '@authentication/constants';
import { sendOTPAgainEvent, sendValidateOTPEvent } from '@events/login';
import EVENT_NAME, { PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';
import history from '@src/client/history';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';

import { StyledEnterOtp } from './styles';
import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IState
} from './types';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

export type IComponentProps = IComponentDispatchToProps &
  IComponentStateToProps;

export class EnterOtp extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      inputFieldValue: '',
      numInputs: props.globalAuthenticationConfiguration.numberofOtp,
      showSendAgain: false
    };
    this.getOtpValue = this.getOtpValue.bind(this);
    this.makeLoginRequest = this.makeLoginRequest.bind(this);
    this.showSendAgain = this.showSendAgain.bind(this);
    this.getTitleText = this.getTitleText.bind(this);
    this.reSendCall = this.reSendCall.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount(): void {
    const email = this.props.email;
    const msisdn = this.props.msisdn;
    if (history && !(email || msisdn)) {
      history.replace({
        pathname: history.location.pathname,
        search: updateQueryStringParameter(
          history.location.search,
          loginQueryParam,
          loginSteps.ENTER_LOGIN_CREDENTIAL
        )
      });
    }
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.ENTER_OTP);
    this.showSendAgain();
  }

  componentWillUnmount(): void {
    const { commonError, removeGenricError } = this.props;
    this.props.setEmail();
    this.props.setMsisdn();
    clearGenricError(commonError, removeGenricError);
  }

  showSendAgain(): void {
    setTimeout(() => {
      this.setState({
        showSendAgain: true
      });
    }, this.props.waitingPeriod);
  }

  getOtpValue(otp: string): void {
    const { commonError, removeGenricError } = this.props;
    this.setState({ inputFieldValue: otp });
    clearGenricError(commonError, removeGenricError);
  }

  createPayloadForLoginWithEmailOtp(): Promise<emailOtpLoginApi.POST.IRequest> {
    return encrypt(this.state.inputFieldValue).then((encryptedPin: string) => {
      return {
        context: CONSTANTS.LOGIN,
        serviceType: LOGIN_SERVICE_TYPE.EMAIL,
        serviceId: this.props.email as string,
        nonce: this.props.emailNonce as string,
        PIN: encryptedPin,
        device: {
          browser: window.navigator.userAgent,
          id: CONSTANTS.DEVICE_ID,
          model: CONSTANTS.DEVICE_MODAL,
          pushToken: CONSTANTS.PUSH_TOKEN,
          os: CONSTANTS.OS
        }
      };
    });
  }

  loginWithEmailOtp(): void {
    this.createPayloadForLoginWithEmailOtp().then(
      payloadForLoginWithEmailOtp => {
        this.props.loginWithEmailOtp(payloadForLoginWithEmailOtp);
      }
    );
  }

  createPayloadForLoginWithMsisdnOtp(): Promise<
    msisdnOtpLoginApi.POST.IRequest
  > {
    return encrypt(this.state.inputFieldValue).then((encryptedPin: string) => {
      return {
        context: CONSTANTS.LOGIN,
        serviceType: LOGIN_SERVICE_TYPE.PHONE_NUMBER,
        serviceId: this.props.msisdn as string,
        PIN: encryptedPin,
        nonce: this.props.msisdnNonce as string,
        device: {
          browser: window.navigator.userAgent,
          id: CONSTANTS.DEVICE_ID,
          model: CONSTANTS.DEVICE_MODAL,
          pushToken: CONSTANTS.PUSH_TOKEN,
          os: CONSTANTS.OS
        }
      };
    });
  }

  loginWithServiceIdOtp(): void {
    this.createPayloadForLoginWithMsisdnOtp().then(
      payloadForLoginWithMsisdnOtp =>
        this.props.loginWithServiceIdOtp(payloadForLoginWithMsisdnOtp)
    );
  }

  createPayloadToValidateUser(): verifyUserUsingOtpApi.POST.IRequest {
    return {
      action: authenticationConstants.VALIDATE,
      mobileNumber: this.props.msisdn as string,
      nonce: this.props.msisdnNonce as string,
      otp: this.state.inputFieldValue,
      otpContext: authenticationConstants.ID_VARIFICATION
    };
  }

  makeLoginRequest(): void {
    if (this.props.isUserLoggedIn) {
      this.props.validateUserUsingOtp(this.createPayloadToValidateUser());
    } else {
      sendValidateOTPEvent();
      if (this.props.loginType === LOGIN_TYPES.EMAIL_OTP) {
        this.loginWithEmailOtp();
      } else {
        this.loginWithServiceIdOtp();
      }
    }
  }

  createPayloadForTelekomVerification(): IVerifyUsingProvidersRequest {
    const identificationProviders = this.props.userIdentityVerificationDetails
      .identificationProviders;
    const selectedProvider = IDENTITY_VERIFICATION_PROVIDERS.telekom;
    const identificationProvidersDetails =
      identificationProviders && identificationProviders[selectedProvider]
        ? identificationProviders[selectedProvider]
        : null;

    return {
      providerId: identificationProvidersDetails
        ? identificationProvidersDetails.providerSublist[0].providerId
        : '',
      providerType: IDENTITY_VERIFICATION_PROVIDERS.telekom,
      bankId: null,
      serviceId: this.props.msisdn
    };
  }

  reSendCall(): void {
    if (this.props.isUserLoggedIn) {
      this.props.verifyUser(this.createPayloadForTelekomVerification());
    } else {
      sendOTPAgainEvent();
      this.props.resendOtpRequest(this.props.loginType);
    }
  }

  getTitleText(otpSentMessage: string): ReactNode {
    const { loginType, globalAuthenticationConfiguration } = this.props;
    const maskInputValue =
      loginType === LOGIN_TYPES.EMAIL_OTP
        ? this.props.email
        : this.props.msisdn;
    const maskingpattern =
      loginType === LOGIN_TYPES.EMAIL_OTP
        ? globalAuthenticationConfiguration.masking.email &&
          globalAuthenticationConfiguration.masking.email.length
          ? new RegExp(globalAuthenticationConfiguration.masking.email)
          : null
        : globalAuthenticationConfiguration.masking.phone &&
          globalAuthenticationConfiguration.masking.phone.length
        ? new RegExp(globalAuthenticationConfiguration.masking.phone)
        : null;
    let maskedValue = maskInputValue;
    if (maskingpattern) {
      maskedValue = (maskInputValue || '').replace(
        maskingpattern,
        (_, a, b, c) => a + b.replace(/./g, '*') + c
      );
    }

    return (
      <>
        {otpSentMessage.replace('{0}', '')}{' '}
        <span className='userInputText'>{maskedValue}</span>
      </>
    );
  }

  onSubmit(event: FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    event.stopPropagation();
    event.persist();
    const { inputFieldValue, numInputs } = this.state;
    if (inputFieldValue.length < numInputs) {
      this.makeLoginRequest();
    }
  }

  render(): ReactNode {
    const { translation, commonError } = this.props;
    const { inputFieldValue, numInputs, showSendAgain } = this.state;
    const errorMessage = getErrorMessage({}, commonError);
    const headingTextElement = (
      <Fragment>
        <Title size='small' weight='ultra'>
          {this.getTitleText(translation.loginEnterOtp.otpSentMessage)}
        </Title>
        {showSendAgain && (
          <SendAgain
            loading={false}
            sendAgainText={translation.common.sendAgain}
            iconRight
            onClick={this.reSendCall}
          />
        )}
      </Fragment>
    );
    const inputElement = (
      <StyledInputWrap>
        <form onSubmit={this.onSubmit}>
          <OtpInput
            getOtpValue={this.getOtpValue}
            numInputs={numInputs}
            hasErrored={!!errorMessage}
            errorMessage={errorMessage}
            shouldAutoFocus={true}
          />
          <Button
            className='button'
            disabled={inputFieldValue.length < numInputs}
            size='medium'
            onClickHandler={this.makeLoginRequest}
            data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.VALIDATE_OTP}
            data-event-message={translation.loginEnterOtp.validateOtp}
          >
            {translation.loginEnterOtp.validateOtp}
          </Button>
        </form>
      </StyledInputWrap>
    );

    return (
      <LoginActions showLogo={false} showBackLink showCloseButton={true}>
        <StyledEnterOtp className='styledEnterOtp'>
          <StyledHeaderTextWrap>{headingTextElement}</StyledHeaderTextWrap>
          {inputElement}
        </StyledEnterOtp>
        <SocialMediaLogins />
      </LoginActions>
    );
  }
}
// tslint:disable:max-file-line-count
export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(EnterOtp);
