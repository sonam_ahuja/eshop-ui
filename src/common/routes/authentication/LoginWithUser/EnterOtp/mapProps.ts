import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import { commonAction } from '@store/actions';
import { RootState } from '@src/common/store/reducers';
import { LOGIN_TYPES } from '@authentication/store/enum';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as verifyUserUsingOtpApi from '@common/types/api/identityVerification/validateUserUsingOtp';
import { IVerifyUsingProvidersRequest } from '@src/common/store/types/common';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  userIdentityVerificationDetails: state.common.userIdentityVerificationDetails,
  translation: state.translation.cart.login,
  waitingPeriod:
    state.configuration.cms_configuration.global.resendOTPWaitingPeriod.mail,
  activeStep: state.authentication.activeStep,
  loginType: state.authentication.loginType,
  email: state.authentication.email.value,
  msisdn: state.authentication.phone.value,
  emailNonce: state.authentication.email.nonce,
  msisdnNonce: state.authentication.phone.nonce,
  globalAuthenticationConfiguration:
    state.configuration.cms_configuration.global.authentication,
  isUserLoggedIn: state.common.isLoggedIn,
  commonError: state.common.error
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  loginWithEmailOtp(payload: emailOtpLoginApi.POST.IRequest): void {
    dispatch(actions.loginWithEmailOtp(payload));
  },
  loginWithServiceIdOtp(payload: msisdnOtpLoginApi.POST.IRequest): void {
    dispatch(actions.loginWithServiceIdOtp(payload));
  },
  resendOtpRequest(payload: LOGIN_TYPES): void {
    dispatch(actions.setLoginType(payload));
  },
  validateUserUsingOtp(payload: verifyUserUsingOtpApi.POST.IRequest): void {
    dispatch(commonAction.verifyUserUsingOtp(payload));
  },
  verifyUser(payload: IVerifyUsingProvidersRequest): void {
    dispatch(commonAction.verifyUserIdentity(payload));
  },
  setEmail(): void {
    dispatch(actions.setEmail(''));
  },
  setMsisdn(): void {
    dispatch(actions.setPhoneNumber(''));
  },
  removeGenricError(): void {
    dispatch(commonAction.removeGenricError());
  }
});
