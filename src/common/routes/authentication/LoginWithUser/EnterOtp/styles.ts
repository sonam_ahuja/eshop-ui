import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledEnterOtp = styled.div`
  color: ${colors.darkGray};

  .userInputText {
    color: ${colors.mediumGray};
  }

  .dt_otpInput {
    margin-bottom: 1.5rem;
    padding: 0.5rem 0 1rem;
  }

  .styledHeaderTextProceedAsGuestWrap {
    color: ${colors.darkGray};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .styledHeaderTextWrap {
      min-height: 7.95rem;
    }
    .dt_otpInput {
      padding: 1.6rem 0 2rem;
      input {
        font-size: 1.5rem;
        line-height: 1.75rem;
      }
    }

    .styledHeaderTextProceedAsGuestWrap {
      .dt_title {
        font-size: 1.875rem;
        font-weight: 900;
        line-height: 1.07;
      }
    }
  }
`;
