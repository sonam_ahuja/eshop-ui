import { IAuthentication } from '@src/common/store/types/configuration';
import { ILoginTranslation } from '@src/common/store/types/translation';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as verifyUserUsingOtpApi from '@common/types/api/identityVerification/validateUserUsingOtp';
import {
  IError as IGenericError,
  IThirdPartyVerificaation,
  IVerifyUsingProvidersRequest
} from '@src/common/store/types/common';

export interface IComponentStateToProps {
  userIdentityVerificationDetails: IThirdPartyVerificaation;
  translation: ILoginTranslation;
  activeStep: LOGIN_STEPS;
  loginType: LOGIN_TYPES;
  email?: string;
  msisdn?: string;
  emailNonce?: string;
  msisdnNonce?: string;
  waitingPeriod: number;
  globalAuthenticationConfiguration: IAuthentication;
  isUserLoggedIn: boolean;
  commonError: IGenericError;
}

export interface IComponentDispatchToProps {
  loginWithEmailOtp(payload: emailOtpLoginApi.POST.IRequest): void;
  loginWithServiceIdOtp(payload: msisdnOtpLoginApi.POST.IRequest): void;
  resendOtpRequest(payload: LOGIN_TYPES): void;
  validateUserUsingOtp(payload: verifyUserUsingOtpApi.POST.IRequest): void;
  verifyUser(payload: IVerifyUsingProvidersRequest): void;
  setEmail(): void;
  setMsisdn(): void;
  removeGenricError(): void;
}

export interface IState {
  inputFieldValue: string;
  numInputs: number;
  showSendAgain: boolean;
}
