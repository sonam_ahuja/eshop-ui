import { Provider } from 'react-redux';
import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';
import appState from '@store/states/app';

import { EnterPassword, IComponentProps } from '.';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

describe('<EnterPassword />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = false;
  const props: IComponentProps = {
    isButtonLoading: true,
    isForgotLinkLoading: true,
    configuration: appState().configuration.cms_configuration.global
      .loginFormRules,
    translation: appState().translation.cart.login,
    username: 'user1',
    isPasswordRecovered: true,
    activeStep: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
    loginType: LOGIN_TYPES.MSISDN_OTP,
    error: {
      code: 400,
      message: 'error'
    },
    loginWithUsernamePassword: jest.fn(),
    recoveryUsingEmailId: jest.fn(),
    setError: jest.fn(),
    setUserName: jest.fn(),
    resetPasswordRecoveryState: jest.fn(),
    commonError: appState().common.error,
    removeGenricError: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterPassword {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: 'asfdf'
      }
    };
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).setError({
      code: 400,
      message: 'error'
    });
    mapDispatchToProps(dispatch).loginWithUsernamePassword(loginPayload);
    mapDispatchToProps(dispatch).recoveryUsingEmailId({
      email: 'xxxxxx@gmail.xon',
      telekomId: '12345'
    });
  });

  test('all function test', () => {
    const copyComponentProps = { ...props };
    const inputEvent = { currentTarget: { value: 'avinash' } };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterPassword {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('EnterPassword').instance() as EnterPassword).onInputChange(
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
    inputEvent.currentTarget.value = '';
    (component.find('EnterPassword').instance() as EnterPassword).onInputChange(
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('EnterPassword')
      .instance() as EnterPassword).validatedPassword('dummy');
    // tslint:disable-next-line:no-hardcoded-credentials
    copyComponentProps.configuration.password.regex = '/asaass';
    (component
      .find('EnterPassword')
      .instance() as EnterPassword).validatedPassword('dummy');
    (component
      .find('EnterPassword')
      .instance() as EnterPassword).createPayloadForPasswordRecovery();
    (component
      .find('EnterPassword')
      .instance() as EnterPassword).recoverPassword();
    (component
      .find('EnterPassword')
      .instance() as EnterPassword).changeInputType();

    (component.find('EnterPassword').instance() as EnterPassword).setRef(null);
    (component
      .find('EnterPassword')
      .instance() as EnterPassword).componentWillUnmount();
  });
});
