import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import SocialMediaLogins from '@authentication/common/SocialMediaLogins';
import { connect } from 'react-redux';
import {
  StyledHeaderTextWrap,
  StyledInputWrap
} from '@authentication/common/styles';
import InputWithLink from '@common/components/InputWithLink';
import { Button, Title } from 'dt-components';
import React, { Component, FormEvent, Fragment, ReactNode } from 'react';
import LoginActions from '@authentication/common/LoginActions';
import clearGenricError from '@authentication/utils/clearGenricError';
import getErrorMessage from '@authentication/utils/getErrorMessage';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import { RootState } from '@src/common/store/reducers';
import { isFormFieldValidAsPerType } from '@src/common/routes/checkout/CheckoutSteps/PersonalInfo/utils';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { createPayloadForUsernamePasswordLogin } from '@authentication/store/selector';
import { INPUT_TYPE } from '@common/store/enums';
import {
  sendForgotPasswordEvent,
  sendValidatePasswordEvent
} from '@events/login';
import EVENT_NAME, { PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';
import history from '@src/client/history';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';

import { loginQueryParam, loginSteps } from '../../constants';

import { StyledEnterPassword } from './styles';
import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IState
} from './types';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

export type IComponentProps = IComponentStateToProps &
  IComponentDispatchToProps;

export class EnterPassword extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      inputFieldValue: '',
      isNextButtonDisable: true,
      inputType: INPUT_TYPE.PASSWORD
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.loginWithUsernamePassword = this.loginWithUsernamePassword.bind(this);
    this.createPayloadForPasswordRecovery = this.createPayloadForPasswordRecovery.bind(
      this
    );
    this.recoverPassword = this.recoverPassword.bind(this);
    this.validatedPassword = this.validatedPassword.bind(this);
    this.changeInputType = this.changeInputType.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.setRef = this.setRef.bind(this);
  }

  componentWillMount(): void {
    const username = this.props.username;
    if (history && !username) {
      history.replace({
        pathname: history.location.pathname,
        search: updateQueryStringParameter(
          history.location.search,
          loginQueryParam,
          loginSteps.ENTER_LOGIN_CREDENTIAL
        )
      });
    }
  }

  componentDidMount(): void {
    if (!this.props.isPasswordRecovered) {
      pageViewEvent(PAGE_VIEW.ENTER_PASSWORD);
    }
    this.props.resetPasswordRecoveryState();
  }

  componentWillUnmount(): void {
    const { commonError, removeGenricError } = this.props;
    this.props.setUserName();
    clearGenricError(commonError, removeGenricError);
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputFieldValue = event.currentTarget.value;
    const inputFieldValueLength = inputFieldValue.trim().length;
    const isButtonEnable = this.validatedPassword(inputFieldValue);
    const { commonError, removeGenricError } = this.props;
    clearGenricError(commonError, removeGenricError);
    if (inputFieldValue && inputFieldValueLength) {
      this.setState({
        inputFieldValue,
        isNextButtonDisable: !isButtonEnable
      });
    } else {
      this.setState({
        inputFieldValue: '',
        isNextButtonDisable: true
      });
    }

    if (isButtonEnable || inputFieldValueLength === 0) {
      this.props.setError({ message: '' });
    }
  }

  changeInputType(): void {
    this.setState(prevState => ({
      inputType:
        prevState.inputType === INPUT_TYPE.PASSWORD
          ? INPUT_TYPE.TEXT
          : INPUT_TYPE.PASSWORD
    }));
  }

  componentDidUpdate(prevProps: IComponentProps): void {
    if (
      prevProps.isPasswordRecovered !== this.props.isPasswordRecovered &&
      this.props.isPasswordRecovered
    ) {
      pageViewEvent(PAGE_VIEW.RECOVER_PASSWORD);
    }
  }

  validatedPassword(enteredValue: string): boolean {
    const configuration = this.props.configuration;
    let regexValidation = true;
    let passwordValidation = true;
    if (configuration.password.regex) {
      regexValidation = isFormFieldValidAsPerType(
        configuration.userName.regex as string,
        enteredValue,
        VALIDATION_TYPE.REGEX
      );
    }

    passwordValidation =
      regexValidation &&
      isFormFieldValidAsPerType(
        configuration.password.minLength.toString(),
        enteredValue,
        VALIDATION_TYPE.MIN
      ) &&
      isFormFieldValidAsPerType(
        configuration.password.minNumberOfDigits,
        enteredValue,
        VALIDATION_TYPE.MIN_DIGIT
      ) &&
      isFormFieldValidAsPerType(
        configuration.password.minLowerCaseLetters,
        enteredValue,
        VALIDATION_TYPE.MIN_LOWERCASE
      ) &&
      isFormFieldValidAsPerType(
        configuration.password.minUpperCaseLetters,
        enteredValue,
        VALIDATION_TYPE.MIN_UPPERCASE
      );

    if (!passwordValidation) {
      this.props.setError({
        message: this.props.translation.loginEnterPassword.passwordInvalid
      });
    }

    return passwordValidation;
  }

  loginWithUsernamePassword(): void {
    sendValidatePasswordEvent();

    createPayloadForUsernamePasswordLogin(this.state.inputFieldValue).then(
      (data: usernamePasswordLoginApi.POST.IRequest) => {
        this.props.loginWithUsernamePassword(data);
      }
    );
  }

  createPayloadForPasswordRecovery(): credentialRecoveryWithEmailApi.POST.IRequest {
    return {
      telekomId: this.props.username
    };
  }

  recoverPassword(): void {
    sendForgotPasswordEvent();
    this.props.recoveryUsingEmailId(this.createPayloadForPasswordRecovery());
  }

  onSubmit(event: FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    event.stopPropagation();
    event.persist();
    this.loginWithUsernamePassword();
  }

  setRef(el: HTMLElement | null): void {
    if (el) {
      el.focus();
    }
  }

  render(): ReactNode {
    const { isForgotLinkLoading, commonError } = this.props;
    const { isPasswordRecovered } = this.props;
    const { isButtonLoading, translation, error, username } = this.props;
    const { isNextButtonDisable, inputType } = this.state;
    const headingTextEl = (
      <Fragment>
        <Title size='small' weight='ultra'>
          {isPasswordRecovered ? (
            translation.recoveryCredentials.sentInstructions
          ) : (
            <>
              {translation.loginEnterPassword.enterPasswordMessage}{' '}
              <span className='userInputText'>{username}</span>
            </>
          )}
        </Title>
      </Fragment>
    );

    const inputEl = (
      <InputWithLink
        className='floatLabelInput'
        inputLabel={translation.loginEnterPassword.inputLabel}
        linkText={translation.loginEnterPassword.forgotPassword}
        changeInputType={this.changeInputType}
        type={inputType}
        showEye={!!this.state.inputFieldValue.length}
        onInputChange={this.onInputChange}
        goToRecoveryScreen={this.recoverPassword}
        errorMessage={getErrorMessage(error, commonError)}
        isError={!!getErrorMessage(error, commonError)}
        autoComplete={'off'}
        isFocused={true}
        getRef={this.setRef}
        isLoading={isForgotLinkLoading}
        value={this.state.inputFieldValue}
      />
    );

    const nextButton = (
      <Button
        className='button'
        disabled={isNextButtonDisable || isButtonLoading}
        loading={isButtonLoading}
        size='medium'
        data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.VALIDATE_PASSWORD}
        data-event-message={translation.loginEnterPassword.validatePassword}
        onClickHandler={this.loginWithUsernamePassword}
      >
        {translation.loginEnterPassword.validatePassword}
      </Button>
    );

    return (
      <LoginActions showLogo={false} showBackLink showCloseButton={true}>
        <StyledEnterPassword className='styledEnterPassword'>
          <StyledHeaderTextWrap>{headingTextEl}</StyledHeaderTextWrap>
          <StyledInputWrap>
            <form onSubmit={this.onSubmit}>
              {inputEl}
              {nextButton}
            </form>
          </StyledInputWrap>
        </StyledEnterPassword>
        <SocialMediaLogins />
      </LoginActions>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(EnterPassword);
