import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import commonAction from '@common/store/actions/common';
import { IError } from '@authentication/store/types';
import { RootState } from '@src/common/store/reducers';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  configuration: state.configuration.cms_configuration.global.loginFormRules,
  translation: state.translation.cart.login,
  activeStep: state.authentication.activeStep,
  loginType: state.authentication.loginType,
  username: state.authentication.username.value,
  isPasswordRecovered: state.authentication.isPasswordRecovered,
  error: state.authentication.error,
  isButtonLoading: state.authentication.buttonLoading,
  isForgotLinkLoading: state.authentication.linkLoading,
  commonError: state.common.error
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  loginWithUsernamePassword(
    payload: usernamePasswordLoginApi.POST.IRequest
  ): void {
    dispatch(actions.loginWithUsernamePassword(payload));
  },
  setUserName(): void {
    dispatch(actions.setUsername(''));
  },
  recoveryUsingEmailId(
    payload: credentialRecoveryWithEmailApi.POST.IRequest
  ): void {
    dispatch(actions.recoveryUsingEmailId(payload));
  },
  setError(error: IError): void {
    dispatch(actions.setError(error));
  },
  resetPasswordRecoveryState(): void {
    dispatch(actions.resetPasswordRecoveryState());
  },
  removeGenricError(): void {
    dispatch(commonAction.removeGenricError());
  }
});
