import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledEnterPassword = styled.div`
  color: ${colors.darkGray};
  .userInputText {
    color: ${colors.darkGray};
    display: block;
  }

  .styledHeaderTextProceedAsGuestWrap {
    color: ${colors.darkGray};
  }
`;
