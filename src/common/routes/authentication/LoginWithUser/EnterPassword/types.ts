import { ILoginFormRules } from '@src/common/store/types/configuration';
import { ILoginTranslation } from '@src/common/store/types/translation';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';
import { INPUT_TYPE } from '@common/store/enums';
import { IError } from '@authentication/store/types';
import { IError as IGenericError } from '@src/common/store/types/common';

export interface IComponentStateToProps {
  isButtonLoading: boolean;
  isForgotLinkLoading: boolean;
  configuration: ILoginFormRules;
  translation: ILoginTranslation;
  activeStep: LOGIN_STEPS;
  loginType: LOGIN_TYPES;
  username?: string;
  error: IError;
  isPasswordRecovered: boolean;
  commonError: IGenericError;
}

export interface IComponentDispatchToProps {
  loginWithUsernamePassword(
    payload: usernamePasswordLoginApi.POST.IRequest
  ): void;
  recoveryUsingEmailId(
    payload: credentialRecoveryWithEmailApi.POST.IRequest
  ): void;
  setError(error: IError): void;
  resetPasswordRecoveryState(): void;
  setUserName(): void;
  removeGenricError(): void;
}

export interface IState {
  inputFieldValue: string;
  isNextButtonDisable: boolean;
  inputType: INPUT_TYPE.PASSWORD | INPUT_TYPE.TEXT;
}
