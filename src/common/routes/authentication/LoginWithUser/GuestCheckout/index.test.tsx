import { Provider } from 'react-redux';
import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import {
  GuestCheckout,
  IComponentProps
} from '@authentication/LoginWithUser/GuestCheckout';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/GuestCheckout/mapProps';

describe('<GuestCheckout />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = false;
  const props: IComponentProps = {
    ...histroyParams,
    className: 'someClass',
    translation: appState().translation.cart.login,
    closeModal: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <GuestCheckout {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).closeModal();
  });

  test('all function test', () => {
    const copyComponentProps = { ...props };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <GuestCheckout {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('GuestCheckout')
      .instance() as GuestCheckout).goToCheckout();
  });
});
