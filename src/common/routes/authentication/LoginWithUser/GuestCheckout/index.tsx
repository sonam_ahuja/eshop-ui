import React, { Component, ReactNode } from 'react';
import { Button, Section, Title } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { connect } from 'react-redux';
import { ILoginTranslation } from '@src/common/store/types/translation';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import EVENT_NAME from '@events/constants/eventName';
import { sendCheckoutGuestEvent } from '@events/login/index';
import routeConstant from '@common/constants/routes';

import { StyledGuestCheckout } from './styles';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

export interface IProps {
  className?: string;
}

export interface IComponentStateToProps {
  translation: ILoginTranslation;
}

export interface IComponentDispatchToProps {
  closeModal(): void;
}

export type IComponentProps = IProps &
  IComponentStateToProps &
  RouteComponentProps &
  IComponentDispatchToProps;

export class GuestCheckout extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
    this.goToCheckout = this.goToCheckout.bind(this);
  }

  goToCheckout(): void {
    sendCheckoutGuestEvent();
    this.props.closeModal();
    this.props.history.push(routeConstant.CHECKOUT.PERSONAL_INFO);
  }

  render(): ReactNode {
    const { translation } = this.props;
    const { className } = this.props;

    return (
      <StyledGuestCheckout className={className}>
        <div className='headerText'>
          <Title className='title' size='small' weight='ultra'>
            {translation.loginWithProceedAsGuest.newCustomerAsk}
            <div className='secondaryText'>
              {translation.loginWithProceedAsGuest.guestCheckout}
            </div>
          </Title>
          <Section className='description' size='large' weight='normal'>
            {translation.loginWithProceedAsGuest.subTitle}
          </Section>
        </div>
        <div className='buttonWrap'>
          <Button
            type='secondary'
            size='medium'
            data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.CONTIUNE}
            data-event-message={translation.loginWithProceedAsGuest.continue}
            onClickHandler={this.goToCheckout}
          >
            {translation.loginWithProceedAsGuest.continue}
          </Button>
        </div>
      </StyledGuestCheckout>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(GuestCheckout));
