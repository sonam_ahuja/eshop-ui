import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import { RootState } from '@src/common/store/reducers';

import { IComponentDispatchToProps, IComponentStateToProps } from '.';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  translation: state.translation.cart.login
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  closeModal(): void {
    dispatch(actions.closeLoginModal());
  }
});
