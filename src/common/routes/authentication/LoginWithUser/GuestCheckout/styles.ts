import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledGuestCheckout = styled.div`
  background: ${colors.silverGray};
  padding: 2.5rem 1.25rem 2rem;
  flex: 1;

  .title {
    color: ${colors.darkGray};
    .secondaryText {
      color: ${colors.magenta};
    }
  }
  .description {
    margin-top: 0.25rem;
    color: ${colors.ironGray};
  }

  .buttonWrap {
    margin-top: 3rem;
    .dt_button {
      width: 100%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    position: absolute;
    width: 14rem;
    left: -14rem;
    top: 0;
    margin: 0;
    padding: 6rem 2rem 0;
    height: 100%;
    .headerText {
      height: 11.8rem;
    }
    .description {
      margin-top: 0.5rem;
    }
  }
`;
