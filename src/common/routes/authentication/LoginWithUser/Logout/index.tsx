import React, { Component, ReactNode } from 'react';
import { Button, Title } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { connect } from 'react-redux';
import { ILoginTranslation } from '@src/common/store/types/translation';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/Logout/mapProps';
import { StyledLogout } from '@authentication/LoginWithUser/Logout/styles';
import EVENT_NAME, { PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';
import history from '@src/client/history';
import { loginQueryParam, loginSteps } from '@authentication/constants';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';

export interface IComponentStateToProps {
  translation: ILoginTranslation;
}
export interface IComponentDispatchToProps {
  goToHome(): void;
  signInAgain(): void;
}

export type IComponentProps = IComponentStateToProps &
  IComponentDispatchToProps;

export class Logout extends Component<IComponentProps, {}> {
  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.LOG_OUT);
  }

  render(): ReactNode {
    const { goToHome, signInAgain } = this.props;
    const {
      translation: { logout }
    } = this.props;

    return (
      <StyledLogout
        showBackLink={false}
        showCloseButton={false}
        showLogo={true}
      >
        <Title size='large' weight='ultra'>
          <div className='heading'>{logout.signedOutMessage}</div>
          <div className='subHeading'>{logout.thankYouMessage}</div>
        </Title>
        <div className='buttonWrap'>
          <Button
            onClickHandler={goToHome}
            data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.HOME_PAGE}
            data-event-message={logout.goToHomePage}
          >
            {' '}
            {logout.goToHomePage}
          </Button>
          <Button
            onClickHandler={() => {
              signInAgain();
              if (history) {
                history.push({
                  pathname: history.location.pathname,
                  search: updateQueryStringParameter(
                    history.location.search,
                    loginQueryParam,
                    loginSteps.ENTER_LOGIN_CREDENTIAL
                  )
                });
              }
            }}
            type='secondary'
            data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.SIGN_AGAIN}
            data-event-message={logout.singInAgain}
          >
            {logout.singInAgain}
          </Button>
        </div>
      </StyledLogout>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(Logout);
