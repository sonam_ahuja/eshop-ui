import { RootState } from '@src/common/store/reducers';
import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import {
  IComponentDispatchToProps,
  IComponentStateToProps
} from '@authentication/LoginWithUser/Logout';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  translation: state.translation.cart.login
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  goToHome(): void {
    dispatch(actions.goToHomePage());
  },
  signInAgain(): void {
    dispatch(actions.resetAuthenticationState());
  }
});
