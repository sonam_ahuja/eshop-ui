import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import LoginActions from '../../common/LoginActions';

export const StyledLogout = styled(LoginActions)`
  padding-top: 10.25rem !important;
  justify-content: space-between;

  .logo {
    width: 3.5rem;
    position: absolute;
    left: 1.25rem;
    top: 2.875rem;
  }

  .heading {
    color: ${colors.darkGray};
  }
  .subHeading {
    color: ${colors.magenta};
  }

  .buttonWrap {
    .dt_button {
      width: 100%;
      margin-top: 0.75rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 6rem 6rem 2rem;
    .logo {
      left: 6rem;
      top: 2.2rem;
    }
    .heading {
      font-size: 1.5rem;
      line-height: 1.75rem;
    }
    .subHeading {
      font-size: 1.5rem;
      line-height: 1.75rem;
    }
  }
`;
