import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { RootState } from '@common/store/reducers';
import {
  EnterRecoveryEmail,
  IComponentProps as IProceedAsGuestProps
} from '@authentication/LoginWithUser/RecoverCredentials/EnterRecoveryEmail';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/RecoverCredentials/EnterRecoveryEmail/mapProps';
import actions from '@authentication/store/actions';
import appState from '@store/states/app';
import { IRecoveryWithEmailRequest } from '@authentication/store/types';
import { ErrorData } from '@mocks/common';

describe('<EnterRecoveryEmail />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  const mockStore = configureStore();

  const componentProps: IProceedAsGuestProps = {
    isButtonLoading: true,
    configuration: appState().configuration.cms_configuration.global
      .loginFormRules,
    error: {},
    recoveryUsingEmailId: jest.fn(),
    translation: appState().translation.cart.login,
    setError: jest.fn()
  };

  const initStateValue: RootState = appState();

  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterRecoveryEmail {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...componentProps };
    const spyOnhandleChange = jest.spyOn(
      EnterRecoveryEmail.prototype,
      'onInputChange'
    );
    const inputEvent = { currentTarget: { value: 'avinash' } };

    const component = mount(
      <ThemeProvider theme={{}}>
        <EnterRecoveryEmail {...copyComponentProps} />
      </ThemeProvider>
    );
    const input = component.find('input').first();
    input.simulate('change', inputEvent);
    expect(spyOnhandleChange).toHaveBeenCalled();
    (component
      .find('EnterRecoveryEmail')
      .instance() as EnterRecoveryEmail).recoverCredentialsUsingEmailId();
    (component
      .find('EnterRecoveryEmail')
      .instance() as EnterRecoveryEmail).onInputChange(
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('EnterRecoveryEmail')
      .instance() as EnterRecoveryEmail).setRef(null);
    (component
      .find('EnterRecoveryEmail')
      .instance() as EnterRecoveryEmail).checkEmailValidation(
      'avinash@gmail.com'
    );
    (component
      .find('EnterRecoveryEmail')
      .instance() as EnterRecoveryEmail).onSubmit(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        // tslint:disable-next-line: no-empty
        preventDefault: () => {},
        // tslint:disable-next-line: no-empty
        stopPropagation: () => {},
        // tslint:disable-next-line: no-empty
        persist: () => {}
      } as React.FormEvent<HTMLFormElement>
    );
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const recoveryUsingEmailIdPayload: IRecoveryWithEmailRequest = {
      email: 'xxxxx@gmail.com',
      telekomId: '12344'
    };
    mapDispatchToProps(dispatch).recoveryUsingEmailId(
      recoveryUsingEmailIdPayload
    );
    mapDispatchToProps(dispatch).setError(ErrorData);
    expect(dispatch.mock.calls[0][0]).toEqual(
      actions.recoveryUsingEmailId(recoveryUsingEmailIdPayload)
    );
    expect(dispatch.mock.calls[1][0]).toEqual(actions.setError(ErrorData));
  });
});
