import {
  StyledHeaderTextWrap,
  StyledInputWrap
} from '@authentication/common/styles';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import React, { Component, FormEvent, ReactNode } from 'react';
import { RootState } from '@src/common/store/reducers';
import { Button, FloatLabelInput, Section, Title } from 'dt-components';
import { connect } from 'react-redux';
import { checkEmailValidation } from '@authentication/common/LoginInputForm/validation';
import { VALIDATIONS } from '@src/common/constants/appConstants';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/RecoverCredentials/EnterRecoveryEmail/mapProps';
import EVENT_NAME, { PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';

import { StyledEnterRecoveryEmail } from './styles';
import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IState
} from './types';

export type IComponentProps = IComponentDispatchToProps &
  IComponentStateToProps;

export class EnterRecoveryEmail extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      inputFieldValue: '',
      isButtonDisabled: true
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.recoverCredentialsUsingEmailId = this.recoverCredentialsUsingEmailId.bind(
      this
    );
    this.createPayload = this.createPayload.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.setRef = this.setRef.bind(this);
  }

  setRef(el: HTMLElement | null): void {
    if (el) {
      el.focus();
    }
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputFieldValue = event.currentTarget.value;
    const inputFieldValueLength = inputFieldValue.trim().length;
    const isButtonEnable = this.checkEmailValidation(inputFieldValue);

    if (inputFieldValue && inputFieldValueLength) {
      this.setState({
        inputFieldValue,
        isButtonDisabled: !isButtonEnable
      });
    } else {
      this.setState({
        inputFieldValue: '',
        isButtonDisabled: true
      });
    }

    if (
      (isButtonEnable || !inputFieldValueLength) &&
      this.props.error.message &&
      this.props.error.message.length
    ) {
      this.props.setError({ message: '' });
    }
  }

  checkEmailValidation(inputFieldValue: string): boolean {
    const { configuration, translation, setError } = this.props;
    if (
      !VALIDATIONS.email.test(inputFieldValue) ||
      !checkEmailValidation(
        configuration.email.regex as string,
        inputFieldValue,
        VALIDATION_TYPE.REGEX
      )
    ) {
      setError({
        message: translation.loginUserNamePassword.emailInvalid
      });

      return false;
    }

    return true;
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.ENTER_RECOVERY_EMAIL);
  }

  createPayload(): credentialRecoveryWithEmailApi.POST.IRequest {
    return {
      email: this.state.inputFieldValue
    };
  }

  recoverCredentialsUsingEmailId(): void {
    this.props.recoveryUsingEmailId(this.createPayload());
  }

  onSubmit(event: FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    event.stopPropagation();
    event.persist();
    this.recoverCredentialsUsingEmailId();
  }

  render(): ReactNode {
    const { translation, error, isButtonLoading } = this.props;

    return (
      <StyledEnterRecoveryEmail>
        <StyledHeaderTextWrap>
          <Title
            className='heading'
            firstLetterTransform='uppercase'
            size='small'
            weight='ultra'
          >
            {translation.recoveryCredentials.title}
          </Title>
          <Section size='large'>
            {translation.recoveryCredentials.sentInstructions}
          </Section>
        </StyledHeaderTextWrap>

        <StyledInputWrap>
          <form onSubmit={this.onSubmit} noValidate>
            <FloatLabelInput
              className='floatLabelInput'
              type='email'
              label={translation.recoveryCredentials.inputLabel}
              onChange={this.onInputChange}
              error={!!(error.message && error.message.length)}
              errorMessage={error.message}
              getRef={this.setRef}
              autoComplete={'on'}
              value={this.state.inputFieldValue}
            />
            <Button
              className='button'
              size='medium'
              onClickHandler={this.recoverCredentialsUsingEmailId}
              loading={isButtonLoading}
              data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.RECOVER_CREDS_NEXT}
              data-event-message={translation.recoveryCredentials.next}
              disabled={this.state.isButtonDisabled || isButtonLoading}
            >
              {translation.recoveryCredentials.next}
            </Button>
          </form>
        </StyledInputWrap>
      </StyledEnterRecoveryEmail>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(EnterRecoveryEmail);
