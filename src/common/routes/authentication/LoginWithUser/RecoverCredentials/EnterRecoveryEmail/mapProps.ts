import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import { RootState } from '@src/common/store/reducers';
import { IError } from '@authentication/store/types';
import {
  IComponentDispatchToProps,
  IComponentStateToProps
} from '@authentication/LoginWithUser/RecoverCredentials/EnterRecoveryEmail/types';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  translation: state.translation.cart.login,
  configuration: state.configuration.cms_configuration.global.loginFormRules,
  error: state.authentication.error,
  isButtonLoading: state.authentication.linkLoading
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  recoveryUsingEmailId(
    payload: credentialRecoveryWithEmailApi.POST.IRequest
  ): void {
    dispatch(actions.recoveryUsingEmailId(payload));
  },
  setError(error: IError): void {
    dispatch(actions.setError(error));
  }
});
