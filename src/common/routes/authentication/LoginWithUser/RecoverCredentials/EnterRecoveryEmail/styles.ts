import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledEnterRecoveryEmail = styled.div`
  .styledHeaderTextWrap {
    min-height: 5.25rem;
  }
  .heading {
    font-size: 1.125rem;
    line-height: 1.25rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .styledHeaderTextWrap {
      /* min-height: 4.75rem; */
      min-height: 8.75rem;

      .heading {
        margin-bottom: 0.5rem;
      }
    }
  }
`;
