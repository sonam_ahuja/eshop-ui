import { ILoginFormRules } from '@src/common/store/types/configuration';
import { ILoginTranslation } from '@src/common/store/types/translation';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { IError } from '@authentication/store/types';

export interface IComponentStateToProps {
  isButtonLoading: boolean;
  translation: ILoginTranslation;
  configuration: ILoginFormRules;
  error: IError;
}

export interface IComponentDispatchToProps {
  recoveryUsingEmailId(
    payload: credentialRecoveryWithEmailApi.POST.IRequest
  ): void;
  setError(error: IError): void;
}

export interface IState {
  inputFieldValue: string;
  isButtonDisabled: boolean;
}
