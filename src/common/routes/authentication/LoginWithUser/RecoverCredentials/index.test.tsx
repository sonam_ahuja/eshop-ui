import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import appState from '@store/states/app';

import RecoverCredentials from '.';

describe('<RecoverCredentials />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  const mockStore = configureStore();

  const initStateValue: RootState = appState();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <RecoverCredentials />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });
});
