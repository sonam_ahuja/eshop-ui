import React from 'react';
import SocialMediaLogins from '@authentication/common/SocialMediaLogins';
import EnterRecoveryEmail from '@authentication/LoginWithUser/RecoverCredentials/EnterRecoveryEmail';
import {
  LoginWrapperRecoverCredentials,
  StyledRecoverCredentials
} from '@authentication/LoginWithUser/RecoverCredentials/styles';

const RecoverCredentials = () => {
  const childrenEl = (
    <>
      <StyledRecoverCredentials>
        <EnterRecoveryEmail />
      </StyledRecoverCredentials>
      <SocialMediaLogins />
    </>
  );

  return (
    <LoginWrapperRecoverCredentials showBackLink showCloseButton>
      {childrenEl}
    </LoginWrapperRecoverCredentials>
  );
};

export default RecoverCredentials;
