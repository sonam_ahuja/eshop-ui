import styled from 'styled-components';
import { colors } from '@src/common/variables';
import LoginActions from '@authentication/common/LoginActions';

export const LoginWrapperRecoverCredentials = styled(LoginActions)``;

export const StyledRecoverCredentials = styled.div`
  color: ${colors.darkGray};

  .styledHeaderTextWrap {
    .heading {
      margin-bottom: 0.5rem;
      font-weight: bold;
    }
  }

  .styledInputWrap {
    .dropdown {
      margin-bottom: 2.5rem;
    }
  }

  .tabsWrap {
    color: ${colors.darkGray};

    .navTabs {
      position: relative;
      z-index: 1;
      padding-bottom: 2rem;

      &:after {
        content: '';
        background: ${colors.cloudGray};
        position: absolute;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: -1;
        height: 1000%;
        width: 1000%;
        left: -100%;
      }
    }

    .navItem {
      font-size: 1.5rem;
      font-weight: bold;
    }
  }
`;
