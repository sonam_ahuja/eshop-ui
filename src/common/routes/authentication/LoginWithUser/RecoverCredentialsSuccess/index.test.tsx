import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { RootState } from '@common/store/reducers';
import {
  IComponentProps,
  RecoverCredentialsSuccess
} from '@authentication/LoginWithUser/RecoverCredentialsSuccess';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/RecoverCredentialsSuccess/mapProps';
import appState from '@store/states/app';
import { IRecoveryWithEmailRequest } from '@authentication/store/types';
import actions from '@authentication/store/actions';

describe('<RecoverCredentialsSuccess />', () => {
  const mockStore = configureStore();

  const componentProps: IComponentProps = {
    translation: appState().translation.cart.login,
    isUsernameRecovered: true,
    recoveryEmail: 'sss@gmail.com',
    emailMaskingPattern: '',
    waitingPeriod: 200,
    isForgetButtonLoading: true,
    recoveryUsingEmailId: jest.fn()
  };

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <RecoverCredentialsSuccess {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const wapper = componentWrapper(componentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, procced as guest', () => {
    const copyComponentProps = { ...componentProps, proceedAsGuest: false };
    const wapper = componentWrapper(copyComponentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...componentProps };
    const inputEvent = { currentTarget: { value: 'avinash' } };
    const component = componentWrapper(copyComponentProps);
    const input = component.find('input');
    input.simulate('change', inputEvent);
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const recoveryUsingEmailIdPayload: IRecoveryWithEmailRequest = {
      email: 'xxxxx@gmail.com',
      telekomId: '12344'
    };
    mapDispatchToProps(dispatch).recoveryUsingEmailId(
      recoveryUsingEmailIdPayload
    );
    expect(dispatch.mock.calls[0][0]).toEqual(
      actions.recoveryUsingEmailId(recoveryUsingEmailIdPayload)
    );
  });

  test('handle trigger', () => {
    const component = mount<RecoverCredentialsSuccess>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <RecoverCredentialsSuccess {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component
      .find('RecoverCredentialsSuccess')
      .instance() as RecoverCredentialsSuccess).recoverCredentialsbyEmail();
  });
});
