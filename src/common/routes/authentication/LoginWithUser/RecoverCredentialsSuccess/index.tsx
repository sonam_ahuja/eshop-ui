import { connect } from 'react-redux';
import { RootState } from '@src/common/store/reducers';
import { Title } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/LoginWithUser/RecoverCredentialsSuccess/mapProps';
import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IState
} from '@authentication/LoginWithUser/RecoverCredentialsSuccess/types';
import { createPayloadForCredentialRecoveryWithEmail } from '@authentication/store/selector';
import { getMaskedValue } from '@authentication/utils/masking';
import { replacedString } from '@authentication/utils';
import SendAgain from '@authentication/common/SendAgain';
import LoginInputForm from '@authentication/common/LoginInputForm';
import { StyledHeaderTextWrap } from '@authentication/common/styles';
import { sendAgainEvent } from '@src/common/events/login';

export type IComponentProps = IComponentStateToProps &
  IComponentDispatchToProps;

export class RecoverCredentialsSuccess extends Component<
  IComponentProps,
  IState
> {

  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      showSendAgain: false
    };
    this.getRecoveryMessage = this.getRecoveryMessage.bind(this);
    this.recoverCredentialsbyEmail = this.recoverCredentialsbyEmail.bind(this);
  }

  componentDidMount(): void {
    this.showSendAgain();
  }

  showSendAgain(): void {
    setTimeout(() => {
      this.setState({
        showSendAgain: true
      });
    }, this.props.waitingPeriod);
  }

  recoverCredentialsbyEmail(): void {
    this.props.recoveryUsingEmailId(
      createPayloadForCredentialRecoveryWithEmail(this.props.recoveryEmail)
    );
  }

  getRecoveryMessage(): ReactNode {
    return (
      <>
        <Title size='small' weight='ultra'>
          {replacedString(
            this.props.translation.recoveryCredentials
              .sentInstructionForUsernameRecovery,
            '{0}',
            getMaskedValue(
              this.props.emailMaskingPattern,
              this.props.recoveryEmail
            )
          )}
        </Title>
        {this.showSendAgain && (
          <SendAgain
            loading={this.props.isForgetButtonLoading}
            sendAgainText={this.props.translation.common.sendAgain}
            onClick={() => {
              sendAgainEvent();
              this.recoverCredentialsbyEmail();
            }}
          />
        )}
      </>
    );
  }

  render(): ReactNode {
    const { isUsernameRecovered } = this.props;

    return (
      <LoginInputForm
        title={
          <StyledHeaderTextWrap>
            {this.getRecoveryMessage()}
          </StyledHeaderTextWrap>
        }
        isBackRequired={!!isUsernameRecovered}
      />
    );
  }
}
export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(RecoverCredentialsSuccess);
