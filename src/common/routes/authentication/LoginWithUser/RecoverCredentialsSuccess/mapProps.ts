import { Dispatch } from 'redux';
import { RootState } from '@src/common/store/reducers';
import actions from '@authentication/store/actions';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => {
  return {
    recoveryUsingEmailId(
      payload: credentialRecoveryWithEmailApi.POST.IRequest
    ): void {
      dispatch(actions.recoveryUsingEmailId(payload));
    }
  };
};

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  translation: state.translation.cart.login,
  emailMaskingPattern:
    state.configuration.cms_configuration.global.authentication.masking.email,
  isUsernameRecovered: state.authentication.isUsernameRecovered,
  recoveryEmail: state.authentication.recoveryEmail,
  waitingPeriod:
    state.configuration.cms_configuration.global.resendOTPWaitingPeriod.mail,
  isForgetButtonLoading: state.authentication.linkLoading
});
