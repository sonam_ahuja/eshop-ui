import { ILoginTranslation } from '@src/common/store/types/translation';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';

export interface IComponentStateToProps {
  translation: ILoginTranslation;
  isUsernameRecovered: boolean;
  recoveryEmail: string;
  emailMaskingPattern: string;
  waitingPeriod: number;
  isForgetButtonLoading: boolean;
}

export interface IState {
  showSendAgain: boolean;
}

export interface IComponentDispatchToProps {
  recoveryUsingEmailId(
    payload: credentialRecoveryWithEmailApi.POST.IRequest
  ): void;
}
