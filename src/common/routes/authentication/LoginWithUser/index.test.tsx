import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import appState from '@store/states/app';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { histroyParams } from '@common/mock-api/common/histroy';
import actions from '@authentication/store/actions';

import { IComponentProps, LoginWithUser } from '.';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

describe('<LoginWithUser />', () => {
  const componentProps: IComponentProps = {
    isLoggedIn: true,
    ...histroyParams,
    loading: false,
    isLoginButtonClicked: false,
    loaderText: '',
    isIdentityVerificatonAllowed: false,
    closeLoginModal: jest.fn(),
    resetLoginState: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <LoginWithUser {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const wapper = componentWrapper(componentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when enter as guest and login button is clicked', () => {
    const newComponentProps: IComponentProps = {
      ...componentProps,
      loading: true,
      isLoginButtonClicked: true
    };
    const wapper = componentWrapper(newComponentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when enter as guest and login button is not clicked', () => {
    const newComponentProps: IComponentProps = {
      ...componentProps,
      loading: true,
      isLoginButtonClicked: false
    };
    const wapper = componentWrapper(newComponentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when enter as guest and isIdentityVerificatonAllowed button clicked', () => {
    const newComponentProps: IComponentProps = {
      ...componentProps,
      isIdentityVerificatonAllowed: true,
      loading: false,
      isLoginButtonClicked: false
    };
    const wapper = componentWrapper(newComponentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when enter as guest and login button clicked', () => {
    const newComponentProps: IComponentProps = {
      ...componentProps,
      isIdentityVerificatonAllowed: false,
      loading: false,
      isLoginButtonClicked: false
    };
    const wapper = componentWrapper(newComponentProps);
    expect(wapper).toMatchSnapshot();
  });

  test('renderLoginStep test for enter otp screen', () => {
    const newComponentProps: IComponentProps = {
      ...componentProps
    };
    const component = componentWrapper(newComponentProps);
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).componentWillUnmount();
    expect(component).toMatchSnapshot();
  });

  test('renderLoginStep test for enter otp screen', () => {
    const newComponentProps: IComponentProps = {
      ...componentProps
    };
    const component = componentWrapper(newComponentProps);
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();
  });

  test('renderLoginStep test for enter password screen', () => {
    const newComponentProps = {
      ...componentProps
    };
    const component = componentWrapper(newComponentProps);
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();
  });
  test('renderLoginStep test for recover screen', () => {
    const newComponentProps = {
      ...componentProps
    };
    newComponentProps.location.search = '?loginStep=enterCredentials';
    const component = componentWrapper(newComponentProps);
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=recovery';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=recoverySuccess';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=enterOtp';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=enterPassword';

    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=identityVerification';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=sdf';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=emailconfirmation';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();

    newComponentProps.location.search = '?loginStep=logout';
    (component
      .find('LoginWithUser')
      .instance() as LoginWithUser).renderLoginStep();
    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).closeLoginModal();
    mapDispatchToProps(dispatch).resetLoginState();
    expect(dispatch.mock.calls[0][0]).toEqual(actions.closeLoginModal());
    expect(dispatch.mock.calls[1][0]).toEqual(
      actions.resetAuthenticationState()
    );
  });
});
