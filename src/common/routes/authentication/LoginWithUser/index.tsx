import { LoginWithUserFlowPanel } from '@authentication/LoginWithUser/styles';
import { Icon, Title } from 'dt-components';
import { connect } from 'react-redux';
import { RootState } from '@common/store/reducers';
import React, { Component, FunctionComponent, ReactNode } from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';
import LoginActions from '@authentication/common/LoginActions';
import { RouteComponentProps, withRouter } from 'react-router';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { loginSteps } from '@authentication/constants';

import Logout from './Logout';
import ConfirmYourIdentity from './ConfirmYourIdentity';
import GuestCheckout from './GuestCheckout';
import { StyledProceedAsGuestNew } from './styles';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

const Loading: FunctionComponent<LoadingComponentProps> = () => null;

export const EnterLoginCredentials = Loadable<{}, {}>({
  loading: Loading,
  loader: () => import('./EnterLoginCredentials'),
  render(loaded: typeof import('./EnterLoginCredentials')): ReactNode {
    const EnterLoginCredentialsLoaded = loaded.default;

    return <EnterLoginCredentialsLoaded />;
  }
});

export const EmailConfirmation = Loadable<{}, {}>({
  loading: Loading,
  loader: () => import('./EmailConfirmation'),
  render(loaded: typeof import('./EmailConfirmation')): ReactNode {
    const EmailConfirmationLoaded = loaded.default;

    return <EmailConfirmationLoaded />;
  }
});

const EnterOtp = Loadable<{}, {}>({
  loading: Loading,
  loader: () => import('./EnterOtp'),
  render(loaded: typeof import('./EnterOtp')): ReactNode {
    const EnterOtpLoaded = loaded.default;

    return <EnterOtpLoaded />;
  }
});

const EnterPassword = Loadable<{}, {}>({
  loading: Loading,
  loader: () => import('./EnterPassword'),
  render(loaded: typeof import('./EnterPassword')): ReactNode {
    const EnterPasswordLoaded = loaded.default;

    return <EnterPasswordLoaded />;
  }
});

const RecoverCredentials = Loadable<{}, {}>({
  loading: Loading,
  loader: () => import('./RecoverCredentials'),
  render(loaded: typeof import('./RecoverCredentials')): ReactNode {
    const RecoverCredentialsLoaded = loaded.default;

    return <RecoverCredentialsLoaded />;
  }
});

const RecoverCrendetialsSuccess = Loadable<{}, {}>({
  loading: Loading,
  loader: () => import('./RecoverCredentialsSuccess'),
  render(loaded: typeof import('./RecoverCredentialsSuccess')): ReactNode {
    const RecoverCredentialsSuccessLoaded = loaded.default;

    return <RecoverCredentialsSuccessLoaded />;
  }
});

export interface IStateToProps {
  loading: boolean;
  isLoggedIn: boolean;
  isLoginButtonClicked: boolean;
  loaderText: ReactNode;
  isIdentityVerificatonAllowed: boolean;
}
export interface IComponentDispatchToProps {
  closeLoginModal(): void;
  resetLoginState(): void;
}

export type IComponentProps = IStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps;

export class LoginWithUser extends Component<IComponentProps> {
  componentDidMount(): void {
    RecoverCredentials.preload();
    EnterPassword.preload();
    EnterOtp.preload();
  }
  renderLoginStep = () => {
    const parsedQuery = parseQueryString(
      decodeURIComponent(this.props.location.search)
    );
    switch (parsedQuery.loginStep) {
      case loginSteps.ENTER_LOGIN_CREDENTIAL:
        return <EnterLoginCredentials />;
      case loginSteps.EMAIL_CONFIRMATION:
        return <EmailConfirmation />;
      case loginSteps.ENTER_OTP:
        return <EnterOtp />;
      case loginSteps.ENTER_PASSWORD:
        return <EnterPassword />;
      case loginSteps.RECOVER:
        return <RecoverCredentials />;
      case loginSteps.RECOVER_SUCCESS:
        return <RecoverCrendetialsSuccess />;
      case loginSteps.LOGOUT:
        return <Logout />;
      case loginSteps.IDENTITY_VERIFICATION:
        return <ConfirmYourIdentity />;
      default:
        return null;
    }
  }

  componentWillUnmount(): void {
    this.props.resetLoginState();
  }

  render(): ReactNode {
    const { loading, isLoginButtonClicked, loaderText } = this.props;
    const { isIdentityVerificatonAllowed } = this.props;

    const loadModalClasses = [
      loading && isLoginButtonClicked ? 'loadingModal' : ''
    ].join(' ');

    return (
      <LoginWithUserFlowPanel
        className={loadModalClasses}
        showCloseButton={false}
        isOpen
        onBackdropClick={this.props.closeLoginModal}
      >
        {loading ? (
          <LoginActions className='loadingText'>
            <div className='textWrap'>
              <Icon name='ec-solid-loading' size='xlarge' />
              <Title size='large' weight='ultra'>
                {loaderText}
              </Title>
            </div>
          </LoginActions>
        ) : (
          <StyledProceedAsGuestNew>
            {!isIdentityVerificatonAllowed && !isLoginButtonClicked ? (
              <GuestCheckout className='guestCheckout' />
            ) : null}
            {this.renderLoginStep()}
          </StyledProceedAsGuestNew>
        )}
      </LoginWithUserFlowPanel>
    );
  }
}

export default withRouter(
  connect<IStateToProps, IComponentDispatchToProps, void, RootState>(
    mapStateToProps,
    mapDispatchToProps
  )(LoginWithUser)
);
