import { RootState } from '@common/store/reducers';
import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';

import { IComponentDispatchToProps, IStateToProps } from '.';

export const mapStateToProps = (state: RootState): IStateToProps => ({
  loading: state.authentication.loading,
  isLoggedIn: state.authentication.isLoggedIn,
  loaderText: state.authentication.loadingText,
  isLoginButtonClicked: state.common.isLoginButtonClicked,
  isIdentityVerificatonAllowed:
    state.configuration.cms_configuration.modules.checkout.identityVerification
      .enabled
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  closeLoginModal(): void {
    dispatch(actions.closeLoginModal());
  },
  resetLoginState(): void {
    dispatch(actions.resetAuthenticationState());
  }
});
