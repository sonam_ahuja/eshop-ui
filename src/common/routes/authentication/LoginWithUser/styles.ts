import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledLoginActions } from '@authentication/common/LoginActions/style';
import { FlowPanel } from 'dt-components';

export const StyledProceedAsGuestNew = styled.div`
  display: flex;
  min-height: 100vh;
  min-height: fill-available;
  flex-direction: column-reverse;

  .styledSocialMediaLogins {
    margin-top: 6.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    height: 100%;
    .styledSocialMediaLogins {
      margin-top: auto;
    }
  }
`;

export const LoginWithUserFlowPanel = styled(FlowPanel)`
  /* Flex dialogbox hack start */
  &.loadingModal {
    .dt_overlay {
      align-items: flex-end;
      .dt_outsideClick {
        margin: 0;
        height: auto;

        .flowPanelInner {
          border-radius: 0.5rem 0.5rem 0 0;
          overflow: hidden;
        }

        .loadingText {
          padding-top: 3.25rem;
        }
      }
    }
    .loadingText {
      padding: 1.25rem 1.25rem 2rem;
      justify-content: flex-end;
      color: ${colors.darkGray};

      .dt_icon {
        margin-bottom: 2rem;
      }
    }
  }
  /* Flex dialogbox hack end */

  .floatLabelInput {
    margin-bottom: 1.5rem;
  }

  .dt_outsideClick,
  .flowPanelInner,
  ${StyledLoginActions} {
    height: 100%;
    width: 100%;
  }
  .dt_overlay .dt_outsideClick .flowPanelInner {
    padding: 0;
    background: ${colors.coldGray};
  }

  ${StyledLoginActions} {
    padding: 4.5rem 1.25rem 1.75rem 1.25rem;
    .closeIconWrap {
      right: 1.25rem;
      top: 1.25rem;
    }

    .backArrow {
      position: absolute;
      z-index: 10;
      left: 1.25rem;
      top: 1.5rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .floatLabelInput {
      margin-bottom: 2.5rem;
    }

    .dt_overlay {
      align-items: flex-start;
      justify-content: flex-end;

      .dt_outsideClick {
        margin: 0 !important;
        width: auto;

        .flowPanelInner {
          padding: 0;
        }
      }
    }

    ${StyledLoginActions} {
      .closeIconWrap {
        right: 1rem;
        top: 1rem;
      }
      flex: 0 0 auto;
      width: 28.8rem;
      padding: 6rem 6rem 2rem;
    }

    /* Flex dialogbox hack start */
    &.loadingModal {
      .dt_overlay {
        align-items: flex-end;
        .dt_outsideClick {
          margin-left: auto;
          height: 100%;

          .flowPanelInner {
            border-radius: 0;
          }

          .loadingText {
            padding-top: 3.25rem;
          }
        }
      }
      .loadingText {
        padding: 6rem;
        justify-content: flex-end;
        color: ${colors.darkGray};

        .dt_icon {
          margin-bottom: 2rem;
        }
      }
    }
    /* Flex dialogbox hack end */
  }
`;
