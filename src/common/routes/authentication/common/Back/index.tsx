import React from 'react';
import { Icon } from 'dt-components';

import { StyledBackIconWrap } from './style';

export interface IProps {
  onBackClick?(): void;
}

const Back = (props: IProps) => {
  return (
    <StyledBackIconWrap
      className='backArrow'
      onClickHandler={props.onBackClick}
    >
      <Icon size='inherit' name='ec-arrow-left' />
    </StyledBackIconWrap>
  );
};

export default Back;
