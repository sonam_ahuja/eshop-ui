import styled from 'styled-components';
import { Anchor } from 'dt-components';

export const StyledBackIconWrap = styled(Anchor)`
  transition: opacity 0.2s linear 0s;

  &:hover {
    opacity: 0.8;
  }
`;
