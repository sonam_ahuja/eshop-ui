import React from 'react';
import { IError } from '@common/store/types/common';
import { Icon } from 'dt-components';

import { StyledErrorBanner } from './style';

export const ErrorBanner = (props: IError) => {
  const { httpStatusCode, code, message = {} } = props;

  if (httpStatusCode) {
    return (
      <StyledErrorBanner>
        <Icon name='dt-sad-face' />
        {message.userMessage || code}
      </StyledErrorBanner>
    );
  }

  return null;
};
