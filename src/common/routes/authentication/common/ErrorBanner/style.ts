import styled from 'styled-components';

export const StyledErrorBanner = styled.div`
  padding: 0.5rem;
  display: flex;
  margin-top: -1rem;
  margin-bottom: 1rem;
  background: rgba(0, 0, 0, 0.04);
  align-items: center;

  .dt_icon {
    margin-right: 0.5rem;
    font-size: 1.5rem;
  }
`;
