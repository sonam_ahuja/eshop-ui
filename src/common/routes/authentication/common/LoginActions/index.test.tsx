import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';

import LoginWrapper from '.';
import { mapDispatchToProps } from './mapProps';

describe('<LoginWrapper />', () => {
  const props = {
    children: null,
    onBackClick: jest.fn(),
    closeLoginModal: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <LoginWrapper {...props} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).closeLoginModal();
    mapDispatchToProps(dispatch).onBackClick();
  });
});
