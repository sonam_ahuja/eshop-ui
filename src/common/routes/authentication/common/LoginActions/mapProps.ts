import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';

import { IComponentDispatchToProps } from '.';

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  closeLoginModal(): void {
    dispatch(actions.closeLoginModal());
  },
  onBackClick(): void {
    dispatch(actions.goToPreviousScreen());
  }
});
