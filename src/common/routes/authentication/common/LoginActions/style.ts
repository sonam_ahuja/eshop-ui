import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledLoginActions = styled.div`
  background: ${colors.coldGray};
  position: relative;
  width: 100%;
  /* overflow: hidden; */
  display: flex;
  flex-direction: column;
  flex: 1;

  .closeIconWrap {
    position: absolute;
    z-index: 10;
  }

  .backArrow {
    position: relative;
    svg {
      width: 30px;
      height: 30px;
    }
    z-index: 10;
  }

  .logo {
    position: relative;
    z-index: 10;

    width: 7rem;
    height: auto;
    margin-bottom: 3rem;
    svg {
      width: 100%;
      height: auto;

      path {
        fill: ${colors.magenta};
      }
    }
  }
`;
