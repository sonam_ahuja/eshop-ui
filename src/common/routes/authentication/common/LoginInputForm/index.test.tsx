import { RootState } from '@common/store/reducers';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import configurationState from '@store/states/configuration';
import appState from '@store/states/app';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { Provider } from 'react-redux';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/common/LoginInputForm/mapProps';
import {
  checkPhoneNumberValidation,
  checkUsernameValidation
} from '@authentication/common/LoginInputForm/validation';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';

import { IComponentProps as ILoginInputFormProps, LoginInputForm } from '.';

// tslint:disable-next-line:no-big-function
describe('<LoginInputForm />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  const mockStore = configureStore();

  const componentProps: ILoginInputFormProps = {
    title: '',
    isBackRequired: false,
    configuration: appState().configuration.cms_configuration.global
      .loginFormRules,
    translation: appState().translation.cart.login,
    loginTypeConfiguration: configurationState().cms_configuration.global
      .loginRegistrationMethods,
    loginType: 'login',
    error: {
      code: 400,
      message: 'error'
    },
    emailMaskingPattern: '',
    isloginWithEmailConfirmation: false,
    buttonLoading: false,
    activeStep: LOGIN_STEPS.ENTER_PASSWORD,
    proceedToNextScreen: jest.fn(),
    setLoginType: jest.fn(),
    requestOtpForEmailLogin: jest.fn(),
    requestOtpForMsisdnLogin: jest.fn(),
    goToRecoveryScreen: jest.fn(),
    setUsername: jest.fn(),
    setEmail: jest.fn(),
    setPhone: jest.fn(),
    setError: jest.fn(),
    setLoaderText: jest.fn(),
    isUsernameRecovered: false,
    recoveryEmail: '',
    recoveryUsingEmailId: jest.fn(),
    username: 'username',
    phone: 'phone',
    email: 'dess@gmail.com',
    commonError: appState().common.error,
    removeGenricError: jest.fn()
  };
  const { loginFormRules } = appState().configuration.cms_configuration.global;

  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <LoginInputForm {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const payload = {
      PIN: '12344',
      context: 'context',
      device: { id: '1233', browser: '', pushToken: '', os: '', model: '' },
      nonce: 'nonce',
      serviceId: '12323',
      serviceType: 'emo'
    };
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).goToRecoveryScreen();
    mapDispatchToProps(dispatch).proceedToNextScreen();
    mapDispatchToProps(dispatch).requestOtpForEmailLogin(payload);
    mapDispatchToProps(dispatch).requestOtpForMsisdnLogin(payload);
    mapDispatchToProps(dispatch).setEmail('xxxx@gmail.con');
    mapDispatchToProps(dispatch).setPhone('+9187566556');
    mapDispatchToProps(dispatch).setError({
      code: 400,
      message: 'error'
    });
    mapDispatchToProps(dispatch).setLoginType(LOGIN_TYPES.MSISDN_OTP);
    mapDispatchToProps(dispatch).setUsername('XXXXXXXX');
    mapDispatchToProps(dispatch).goToRecoveryScreen();
    mapDispatchToProps(dispatch).setLoaderText('hello');
    mapDispatchToProps(dispatch).recoveryUsingEmailId({
      email: 'gmail@gmai.com'
    });
  });

  test('handleChange test for input field with empty input', () => {
    const copyComponentProps = { ...componentProps };
    const spyOnhandleChange = jest.spyOn(
      LoginInputForm.prototype,
      'onInputChange'
    );
    const inputEvent = { currentTarget: { value: '' } };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <LoginInputForm {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    const input = component.find('input');
    input.simulate('change', inputEvent);
    expect(spyOnhandleChange).toHaveBeenCalled();
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).onInputChange(
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...componentProps };
    const spyOnhandleChange = jest.spyOn(
      LoginInputForm.prototype,
      'onInputChange'
    );
    const inputEvent = { currentTarget: { value: 'avinash' } };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <LoginInputForm {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    const input = component.find('input');
    input.simulate('change', inputEvent);
    expect(spyOnhandleChange).toHaveBeenCalled();
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).onInputChange(
      inputEvent as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).recoverCredentialsbyEmail();
    (component.find('LoginInputForm').instance() as LoginInputForm).setRef(
      null
    );
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).setMsisdnOTPAsLogin('');
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).setEmailOTPAsLogin('abc@gmail.com');
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).getModalTitle(LOGIN_TYPES.EMAIL_OTP);
    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).getModalTitle(LOGIN_TYPES.MSISDN_OTP);

    (component
      .find('LoginInputForm')
      .instance() as LoginInputForm).setLoginType();

    // tslint:disable-next-line: no-object-literal-type-assertion
    (component.find('LoginInputForm').instance() as LoginInputForm).submit({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      // tslint:disable-next-line: no-empty
      stopPropagation: () => {},
      // tslint:disable-next-line: no-empty
      persist: () => {}
    } as React.FormEvent<HTMLFormElement>);
  });

  test('checkPhoneNumberValidation function test cases ', () => {
    expect(
      checkPhoneNumberValidation(loginFormRules, '9876543221')
    ).toBeDefined();

    loginFormRules.phoneNumber.regex = '/asa';
    expect(checkPhoneNumberValidation(loginFormRules, '987654')).toBeDefined();
  });
  test('checkUsernameValidation test', () => {
    expect(
      checkUsernameValidation(
        appState().configuration.cms_configuration.global.loginFormRules,
        'enter'
      )
    ).toBeDefined();
    loginFormRules.userName.regex = '/asa';
    expect(checkUsernameValidation(loginFormRules, 'enter')).toBeDefined();
  });
});
