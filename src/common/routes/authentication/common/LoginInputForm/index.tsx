import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IProps,
  IState
} from '@authentication/common/LoginInputForm/types';
import { RootState } from '@src/common/store/reducers';
import { Button } from 'dt-components';
import {
  StyledHeaderTextWrap,
  StyledInputWrap
} from '@authentication/common/styles';
import { LOGIN_TYPES } from '@authentication/store/enum';
import SocialMediaLogins from '@authentication/common/SocialMediaLogins';
import React, { Component, FormEvent, ReactNode } from 'react';
import { connect } from 'react-redux';
import { VALIDATIONS } from '@common/constants/appConstants';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@authentication/common/LoginInputForm/mapProps';
import { StyledEnterLoginCredentials } from '@authentication/common/LoginInputForm/styles';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import {
  checkEmailValidation,
  checkPhoneNumberValidation,
  checkUsernameValidation
} from '@authentication/common/LoginInputForm/validation';
import { createPayloadForCredentialRecoveryWithEmail } from '@authentication/store/selector';
import LoginActions from '@authentication/common/LoginActions';
import getErrorMessage from '@authentication/utils/getErrorMessage';
import { isMobile } from '@src/common/utils';
import InputWithLink from '@src/common/components/InputWithLink';
import { sendForgotCredentialsEvent, sendUserNameEvent } from '@events/login';
import EVENT_NAME from '@events/constants/eventName';
import history from '@src/client/history';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';

import { loginQueryParam, loginSteps } from '../../constants';

export type IComponentProps = IProps &
  IComponentStateToProps &
  IComponentDispatchToProps;

export class LoginInputForm extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      isNextButtonDisable: true,
      inputValue: '',
      loginType: LOGIN_TYPES.USERNAME_PASSWORD
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.setLoginType = this.setLoginType.bind(this);
    this.setEmailOTPAsLogin = this.setEmailOTPAsLogin.bind(this);
    this.setMsisdnOTPAsLogin = this.setMsisdnOTPAsLogin.bind(this);
    this.setUsernamePasswordAsLogin = this.setUsernamePasswordAsLogin.bind(
      this
    );
    this.getModalTitle = this.getModalTitle.bind(this);
    this.recoverCredentialsbyEmail = this.recoverCredentialsbyEmail.bind(this);
    this.submit = this.submit.bind(this);
    this.setRef = this.setRef.bind(this);
  }

  componentWillUnmount(): void {
    this.props.removeGenricError();
  }

  setRef(el: HTMLElement | null): void {
    if (el && !isMobile.phone) {
      el.focus();
      setTimeout(() => {
        el.blur();
        el.focus();
      }, 450); // 400 = flowpanel sideInLeft animation time
      this.setState({ inputValue: '' });
    }
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputFieldValue = event.currentTarget.value;
    const inputFieldValueLength = inputFieldValue.trim().length;
    const { commonError } = this.props;
    const isButtonEnable =
      this.setEmailOTPAsLogin(inputFieldValue) ||
      this.setMsisdnOTPAsLogin(inputFieldValue) ||
      this.setUsernamePasswordAsLogin(inputFieldValue);
    if (inputFieldValue && inputFieldValueLength) {
      this.setState({
        isNextButtonDisable: !isButtonEnable,
        inputValue: inputFieldValue
      });
    } else {
      this.setState({
        isNextButtonDisable: true,
        inputValue: ''
      });
    }

    if (isButtonEnable || !inputFieldValueLength) {
      this.props.setError({ message: '' });
    }

    if (commonError.httpStatusCode) {
      this.props.removeGenricError();
    }
  }

  setMsisdnOTPAsLogin(enteredValue: string): boolean {
    const enteredValueIsPhoneNumber = VALIDATIONS.phone.test(enteredValue);
    const { configuration, translation, setError } = this.props;
    const { loginTypeConfiguration } = this.props;

    if (!loginTypeConfiguration.MSISDNOTP || !enteredValueIsPhoneNumber) {
      return false;
    }

    if (!checkPhoneNumberValidation(configuration, enteredValue)) {
      setError({
        message: translation.loginUserNamePassword.mobileNumberInvalid
      });

      return false;
    }

    this.setState({ loginType: LOGIN_TYPES.MSISDN_OTP });

    return true;
  }

  setUsernamePasswordAsLogin(enteredValue: string): boolean {
    const { configuration, translation, setError } = this.props;
    const { loginTypeConfiguration } = this.props;
    if (
      loginTypeConfiguration.usernamePassword &&
      checkUsernameValidation(configuration, enteredValue)
    ) {
      this.setState({ loginType: LOGIN_TYPES.USERNAME_PASSWORD });

      return true;
    }

    setError({
      message: translation.loginUserNamePassword.userNameInvalid
    });

    return false;
  }

  // tslint:disable-next-line: cognitive-complexity
  setEmailOTPAsLogin(inputValue: string): boolean {
    const { configuration, translation, setError } = this.props;
    const enteredValueIsEmail = VALIDATIONS.email.test(inputValue);

    if (!enteredValueIsEmail) {
      return false;
    }

    if (
      !checkEmailValidation(
        configuration.email.regex as string,
        inputValue,
        VALIDATION_TYPE.REGEX
      )
    ) {
      setError({
        message: translation.loginUserNamePassword.emailInvalid
      });

      return false;
    }

    this.setState({ loginType: LOGIN_TYPES.EMAIL_OTP });

    return true;
  }

  setLoginType(): void {
    const { setUsername, setPhone, setEmail } = this.props;
    const { inputValue, loginType } = this.state;
    sendUserNameEvent();
    switch (loginType) {
      case LOGIN_TYPES.EMAIL_OTP:
        setEmail(inputValue);
        break;
      case LOGIN_TYPES.EMAIL_CONFIRMATION:
        setEmail(inputValue);
        break;
      case LOGIN_TYPES.MSISDN_OTP:
        setPhone(inputValue);
        this.props.setLoaderText(this.getModalTitle(LOGIN_TYPES.MSISDN_OTP));
        break;
      case LOGIN_TYPES.USERNAME_PASSWORD:
        setUsername(inputValue);
        break;
      default:
        break;
    }
    this.props.setLoaderText(this.getModalTitle(loginType));
    this.props.setLoginType(loginType);
  }

  getModalTitle(loginType: LOGIN_TYPES): ReactNode {
    const { translation } = this.props;

    return (
      <>
        <div>{translation.loginProgressOtpSent.hello}</div>
        <div>
          {loginType === LOGIN_TYPES.EMAIL_OTP
            ? translation.loginProgressOtpSent.messageForEmailLogin
            : translation.loginProgressOtpSent.messageForMsisdnLogin}
        </div>
      </>
    );
  }

  recoverCredentialsbyEmail(): void {
    sendForgotCredentialsEvent();
    createPayloadForCredentialRecoveryWithEmail(this.props.recoveryEmail);
  }

  submit(event: FormEvent<HTMLFormElement>): void {
    event.preventDefault();
    event.stopPropagation();
    event.persist();
    this.setLoginType();
  }

  render(): ReactNode {
    const { isNextButtonDisable } = this.state;
    const { buttonLoading, commonError } = this.props;
    const { translation, error, isBackRequired } = this.props;
    const errorMessage = getErrorMessage(error, commonError);
    const headingTextEl = (
      <StyledHeaderTextWrap>{this.props.title}</StyledHeaderTextWrap>
    );
    const inputElement = (
      <InputWithLink
        className='floatLabelInput'
        inputLabel={translation.loginUserNamePassword.placeHolderForInput}
        linkText={translation.loginUserNamePassword.linkTextKey}
        onInputChange={this.onInputChange}
        goToRecoveryScreen={() => {
          sendForgotCredentialsEvent();
          if (history) {
            history.push({
              pathname: history.location.pathname,
              search: updateQueryStringParameter(
                history.location.search,
                loginQueryParam,
                loginSteps.RECOVER
              )
            });
          }
        }}
        errorMessage={errorMessage}
        isError={!!errorMessage}
        getRef={this.setRef}
        value={this.state.inputValue}
      />
    );
    const nextButton = (
      <Button
        className='button'
        data-event-id={EVENT_NAME.AUTHENTICATE.EVENTS.NEXT}
        data-event-message={translation.common.next}
        size='medium'
        disabled={isNextButtonDisable && !buttonLoading}
        loading={buttonLoading}
      >
        {translation.common.next}
      </Button>
    );

    return (
      <LoginActions
        showLogo={false}
        showCloseButton={true}
        showBackLink={isBackRequired}
      >
        <StyledEnterLoginCredentials className='styledEnterLoginCredentials'>
          {headingTextEl}
          <StyledInputWrap>
            <form onSubmit={this.submit}>
              {inputElement}
              {nextButton}
            </form>
          </StyledInputWrap>
        </StyledEnterLoginCredentials>
        <SocialMediaLogins />
      </LoginActions>
    );
  }
}
export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
  // tslint:disable-next-line:max-file-line-count
)(LoginInputForm);
