import { Dispatch } from 'redux';
import { RootState } from '@src/common/store/reducers';
import actions from '@authentication/store/actions';
import commonActions from '@common/store/actions/common';
import { LOGIN_TYPES } from '@authentication/store/enum';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { IError } from '@authentication/store/types';
import { ReactNode } from 'react';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => {
  return {
    proceedToNextScreen(): void {
      dispatch(actions.proceedToNextScreen());
    },
    setLoginType(loginType: LOGIN_TYPES): void {
      dispatch(actions.setLoginType(loginType));
    },
    requestOtpForEmailLogin(payload: emailOtpLoginApi.POST.IRequest): void {
      dispatch(actions.requestOtpForEmailLogin(payload));
    },
    requestOtpForMsisdnLogin(payload: msisdnOtpLoginApi.POST.IRequest): void {
      dispatch(actions.requestOtpForMsisdnLogin(payload));
    },
    goToRecoveryScreen(): void {
      dispatch(actions.goToRecoveryScreen());
    },
    setUsername(username: string): void {
      dispatch(actions.setUsername(username));
    },
    setEmail(email: string): void {
      dispatch(actions.setEmail(email));
    },
    setPhone(phone: string): void {
      dispatch(actions.setPhoneNumber(phone));
    },
    setError(error: IError): void {
      dispatch(actions.setError(error));
    },
    setLoaderText(loaderText: ReactNode): void {
      dispatch(actions.setLoadingTextForProgressModal(loaderText));
    },
    recoveryUsingEmailId(
      payload: credentialRecoveryWithEmailApi.POST.IRequest
    ): void {
      dispatch(actions.recoveryUsingEmailId(payload));
    },
    removeGenricError(): void {
      dispatch(commonActions.removeGenricError());
    }
  };
};

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  configuration: state.configuration.cms_configuration.global.loginFormRules,
  loginTypeConfiguration:
    state.configuration.cms_configuration.global.loginRegistrationMethods,
  isloginWithEmailConfirmation:
    state.configuration.cms_configuration.global.loginWithEmailConfirmation,
  emailMaskingPattern:
    state.configuration.cms_configuration.global.authentication.masking.email,
  translation: state.translation.cart.login,
  activeStep: state.authentication.activeStep,
  loginType: state.authentication.loginType,
  error: state.authentication.error,
  username: state.authentication.username.value,
  phone: state.authentication.phone.value,
  email: state.authentication.email.value,
  isUsernameRecovered: state.authentication.isUsernameRecovered,
  recoveryEmail: state.authentication.recoveryEmail,
  buttonLoading: state.authentication.buttonLoading,
  commonError: state.common.error
});
