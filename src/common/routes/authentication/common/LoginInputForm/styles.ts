import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledEnterLoginCredentials = styled.div`
  color: ${colors.ironGray};

  .styledHeaderTextWrap {
    .heading {
      color: ${colors.darkGray};
    }
    .subHeading {
      margin-bottom: 0.25rem;
      color: ${colors.magenta};
      span {
        color: ${colors.darkGray};
      }
    }

    @media (min-width: ${breakpoints.desktop}px) {
      .subHeading {
        margin-bottom: 0.5rem;
      }
    }
  }

  .styledHeaderTextProceedAsGuestWrap {
    .heading {
      color: ${colors.darkGray};
    }
    .subHeading {
      color: ${colors.magenta};
    }

    @media (min-width: ${breakpoints.desktop}px) {
      .heading,
      .subHeading {
        font-size: 2.5rem;
        line-height: 1.1;
        letter-spacing: -0.3px;
      }
    }
  }
`;
