import { ILoginTranslation } from '@src/common/store/types/translation';
import {
  ILoginFormRules,
  ILoginRegistrationMethods
} from '@src/common/store/types/configuration';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import { IError } from '@authentication/store/types';
import { ReactNode } from 'react';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { IError as IGenricError } from '@src/common/store/types/common';

export interface IProps {
  title: React.ReactNode;
  isBackRequired: boolean;
}

export interface IComponentStateToProps {
  configuration: ILoginFormRules;
  translation: ILoginTranslation;
  activeStep: LOGIN_STEPS;
  loginType: string;
  error: IError;
  loginTypeConfiguration: ILoginRegistrationMethods;
  isUsernameRecovered: boolean;
  isloginWithEmailConfirmation: boolean;
  recoveryEmail: string;
  emailMaskingPattern: string;
  username: string;
  phone: string;
  email: string;
  buttonLoading: boolean;
  commonError: IGenricError;
}

export interface IState {
  isNextButtonDisable: boolean;
  inputValue: string;
  loginType: LOGIN_TYPES;
}

export interface IComponentDispatchToProps {
  setLoaderText(loaderText: ReactNode): void;
  proceedToNextScreen(): void;
  setLoginType(loginType: LOGIN_TYPES): void;
  requestOtpForEmailLogin(payload: emailOtpLoginApi.POST.IRequest): void;
  requestOtpForMsisdnLogin(payload: msisdnOtpLoginApi.POST.IRequest): void;
  goToRecoveryScreen(): void;
  setUsername(username: string): void;
  setEmail(email: string): void;
  setPhone(phone: string): void;
  setError(error: IError): void;
  recoveryUsingEmailId(
    payload: credentialRecoveryWithEmailApi.POST.IRequest
  ): void;
  removeGenricError(): void;
}
