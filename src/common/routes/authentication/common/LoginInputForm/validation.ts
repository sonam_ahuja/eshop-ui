import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { isFormFieldValidAsPerType } from '@src/common/routes/checkout/CheckoutSteps/PersonalInfo/utils';
import { ILoginFormRules } from '@src/common/store/types/configuration';

export const checkPhoneNumberValidation = (
  configuration: ILoginFormRules,
  enteredValue: string
): boolean => {
  let regexValidation = true;
  let phoneValidation = true;
  if (configuration.phoneNumber.regex) {
    regexValidation = isFormFieldValidAsPerType(
      configuration.phoneNumber.regex,
      enteredValue,
      VALIDATION_TYPE.REGEX
    );
  }

  phoneValidation =
    regexValidation &&
    isFormFieldValidAsPerType(
      configuration.phoneNumber.minLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MIN
    ) &&
    isFormFieldValidAsPerType(
      configuration.phoneNumber.maxLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MAX
    );

  return phoneValidation;
};

export const checkUsernameValidation = (
  configuration: ILoginFormRules,
  enteredValue: string
): boolean => {
  let regexValidation = true;
  let usernameValidation = true;
  if (configuration.userName.regex) {
    regexValidation = isFormFieldValidAsPerType(
      configuration.userName.regex,
      enteredValue,
      VALIDATION_TYPE.REGEX
    );
  }

  usernameValidation =
    regexValidation &&
    isFormFieldValidAsPerType(
      configuration.userName.minLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MIN
    ) &&
    isFormFieldValidAsPerType(
      configuration.userName.maxLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MAX
    );

  return usernameValidation;
};

export const checkEmailValidation = (
  regex: string,
  inputValue: string,
  validationType: VALIDATION_TYPE
): boolean => {
  let emailValidation = true;

  emailValidation = isFormFieldValidAsPerType(
    regex,
    inputValue,
    validationType
  );

  return emailValidation;
};
