import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import SendAgain, { IProps as ISendAgainProps } from '.';

describe('<LoginActions />', () => {
  const props: ISendAgainProps = {
    loading: true,
    iconRight: true,
    sendAgainText: 'Text',
    onClick: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SendAgain {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when loading false', () => {
    const newProps = { ...props, loading: false };
    const component = mount(
      <ThemeProvider theme={{}}>
        <SendAgain {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
