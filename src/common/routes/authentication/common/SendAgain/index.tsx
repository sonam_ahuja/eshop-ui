import React from 'react';
import { Anchor, Icon } from 'dt-components';
import styled from 'styled-components';
import appConstants from '@src/common/constants/appConstants';

const StyledSendAgain = styled.div`
  margin-top: 0.5rem;
  a {
    font-size: 0.875rem;
    line-height: 1.25rem;
    i {
      margin-left: 5px;
    }
  }
`;

export interface IProps {
  iconRight?: boolean;
  sendAgainText: string;
  loading: boolean;
  onClick(): void;
}
const SendAgain = (props: IProps) => {
  const { iconRight, sendAgainText, onClick, loading } = props;

  const sendAgainWithConditionalIcon = iconRight ? (
    <Anchor
      size='medium'
      onClickHandler={onClick}
      hreflang={appConstants.LANGUAGE_CODE}
      title={sendAgainText}
      isLoading={loading}
    >
      {sendAgainText} <Icon name='ec-reload' />
    </Anchor>
  ) : (
    <Anchor
      onClickHandler={onClick}
      hreflang={appConstants.LANGUAGE_CODE}
      title={sendAgainText}
      isLoading={loading}
    >
      {sendAgainText}
    </Anchor>
  );

  return (
    <StyledSendAgain className='sendAgain'>
      {sendAgainWithConditionalIcon}
    </StyledSendAgain>
  );
};

export default SendAgain;
