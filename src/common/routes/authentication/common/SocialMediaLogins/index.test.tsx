import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import appState from '@store/states/app';
import { IWindow } from '@src/client/index';

import { IComponentProps, SocialMediaLogins } from '.';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

describe('<SocialMediaLogins />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  // tslint:disable: no-any
  (window as IWindow).gapi = {} as any;
  (window as IWindow).gapi.auth2 = {
    getAuthInstance(): object {
      return {
        grantOfflineAccess: jest.fn(),
        isSignedIn: jest.fn()
      };
    }
  } as any;

  const componentProps: IComponentProps = {
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    loginWithGoogle: jest.fn(),
    socialConfiguration: appState().configuration.cms_configuration.modules
      .login.social,
    location: {
      pathname: '/basket',
      search: '',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    },
    translation: translationState().cart.login,
    loginTypeConfiguration: configurationState().cms_configuration.global
      .loginRegistrationMethods,
    loading: false,
    loginWithSocialMedia: jest.fn(),
    setLoadingText: jest.fn()
  };

  const initStateValue: RootState = appState();

  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SocialMediaLogins {...componentProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).loginWithSocialMedia('/login');
    mapDispatchToProps(dispatch).setLoadingText(null);
    mapDispatchToProps(dispatch).loginWithGoogle({
      redirectUrl: 'string',
      googleCode: 'string'
    });
  });

  test('handleChange test for input field', async () => {
    const copyComponentProps = { ...componentProps };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SocialMediaLogins {...copyComponentProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('SocialMediaLogins')
      .instance() as SocialMediaLogins).loginWithSocialMedia();
    await (component
      .find('SocialMediaLogins')
      .instance() as SocialMediaLogins).loginWithGoogle();
  });
});
