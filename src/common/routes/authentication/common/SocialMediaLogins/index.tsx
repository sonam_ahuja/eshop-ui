import React, { Component, ReactNode } from 'react';
import { Anchor, Icon, Section } from 'dt-components';
import { connect } from 'react-redux';
import { RootState } from '@src/common/store/reducers';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { ILoginTranslation } from '@src/common/store/types/translation';
import { IWindow } from '@src/client/index';
import { IGoogleLoginParams } from '@authentication/store/types';
import appConstants from '@src/common/constants/appConstants';

import { StyledSocialMediaLogins } from './style';
import { IComponentDispatchToProps, IComponentStateToProps } from './types';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

export type IComponentProps = IComponentStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps;

export class SocialMediaLogins extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
    this.loginWithSocialMedia = this.loginWithSocialMedia.bind(this);
    this.loginWithGoogle = this.loginWithGoogle.bind(this);
  }

  async loginWithGoogle(): Promise<void> {
    const auth2 = (window as IWindow).gapi.auth2.getAuthInstance();
    const response:
      | { error: string }
      | { code: string } = await auth2.grantOfflineAccess();
    if (response && (response as { code: string }).code) {
      const { code } = response as { code: string };
      const signInParams: IGoogleLoginParams = {
        googleCode: code,
        redirectUrl: window.location.origin
      };
      this.props.loginWithGoogle(signInParams);
      this.props.setLoadingText(this.getModalTitle(this.props.translation));
    }
  }

  loginWithSocialMedia(): void {
    this.props.loginWithSocialMedia(window.location.href);
    this.props.setLoadingText(this.getModalTitle(this.props.translation));
  }

  getModalTitle(translation: ILoginTranslation): ReactNode {
    return (
      <>
        <div>{translation.loginProgressFacebookSignIn.hi}</div>
        <div>{translation.loginProgressFacebookSignIn.message}</div>
      </>
    );
  }

  render(): ReactNode {
    const { translation, loginTypeConfiguration } = this.props;

    return (
      <>
        {(loginTypeConfiguration.socialAccount ||
          loginTypeConfiguration.googleAccount) && (
          <>
            <StyledSocialMediaLogins className='styledSocialMediaLogins'>
              <Section size='small' weight='bold'>
                {translation.common.otherOptionsHeading}
              </Section>
              <div className='iconsWrap'>
                {loginTypeConfiguration.socialAccount && (
                  <Anchor
                    hreflang={appConstants.LANGUAGE_CODE}
                    onClickHandler={this.loginWithSocialMedia}
                  >
                    <Icon className='icon facebook' name='ec-facebook' />
                  </Anchor>
                )}
                {loginTypeConfiguration.googleAccount && (
                  <Anchor
                    hreflang={appConstants.LANGUAGE_CODE}
                    onClickHandler={this.loginWithGoogle}
                  >
                    <Icon className='icon google' name='ec-google' />
                  </Anchor>
                )}
              </div>
            </StyledSocialMediaLogins>{' '}
          </>
        )}
      </>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SocialMediaLogins));
