import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import { RootState } from '@src/common/store/reducers';
import { ReactNode } from 'react';
import { IGoogleLoginParams } from '@authentication/store/types';

import { IComponentDispatchToProps, IComponentStateToProps } from './types';

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  translation: state.translation.cart.login,
  socialConfiguration:
    state.configuration.cms_configuration.modules.login.social,
  loading: state.authentication.loading,
  loginTypeConfiguration:
    state.configuration.cms_configuration.global.loginRegistrationMethods
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  loginWithSocialMedia(callbackUrl: string): void {
    dispatch(actions.loginWithSocialMedia(callbackUrl));
  },
  loginWithGoogle(params: IGoogleLoginParams): void {
    dispatch(actions.loginWithGoogle(params));
  },
  setLoadingText(loaderText: ReactNode): void {
    dispatch(actions.setLoadingTextForProgressModal(loaderText));
  }
});
