import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSocialMediaLogins = styled.div`
  text-align: center;
  margin-top: auto;
  color: ${colors.mediumGray};

  .iconsWrap {
    display: flex;
    justify-content: center;

    a {
      display: inline-block;
      padding: 5px;
      margin: 7px 10px 0;
      font-size: 1.75rem;
      .icon {
        path {
          fill: ${colors.gray};
        }
      }
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .iconsWrap {
      a {
        font-size: 1.25rem;
      }
    }
  }
`;
