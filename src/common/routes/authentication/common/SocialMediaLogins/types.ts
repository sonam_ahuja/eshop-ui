import {
  IConfigurationState,
  ILoginRegistrationMethods
} from '@src/common/store/types/configuration';
import { ILoginTranslation } from '@src/common/store/types/translation';
import { ReactNode } from 'react';
import { IGoogleLoginParams } from '@authentication/store/types';

export interface IComponentStateToProps {
  translation: ILoginTranslation;
  socialConfiguration: IConfigurationState['cms_configuration']['modules']['login']['social'];
  loading: boolean;
  loginTypeConfiguration: ILoginRegistrationMethods;
}

export interface IComponentDispatchToProps {
  loginWithSocialMedia(callbackUrl: string): void;
  loginWithGoogle(params: IGoogleLoginParams): void;
  setLoadingText(loaderText: ReactNode): void;
}
