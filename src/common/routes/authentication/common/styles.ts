import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledHeaderTextWrap = styled.div.attrs({
  className: 'styledHeaderTextWrap'
})`
  min-height: 7.75rem;
  overflow-wrap: break-word;
  word-wrap: break-word;

  @media (min-width: ${breakpoints.desktop}px) {
    min-height: 8.75rem;
  }
`;

export const StyledHeaderTextProceedAsGuestWrap = styled.div.attrs({
  className: 'styledHeaderTextProceedAsGuestWrap'
})`
  min-height: 9rem;
`;

export const StyledInputWrap = styled.div.attrs({
  className: 'styledInputWrap'
})`
  button {
    margin-top: 0;
    width: 100%;
  }
`;
