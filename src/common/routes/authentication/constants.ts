export default {
  VALIDATE: 'VALIDATE',
  ID_VARIFICATION: 'idVerification'
};

export const loginQueryParam = 'loginStep';

export const loginSteps = {
  ENTER_LOGIN_CREDENTIAL: 'enterCredentials',
  ENTER_OTP: 'enterOtp',
  ENTER_PASSWORD: 'enterPassword',
  EMAIL_CONFIRMATION: 'emailconfirmation',
  RECOVER_USING_EMAIL: 'recoverUsingEmail',
  RECOVER: 'recovery',
  RECOVER_SUCCESS: 'recoverySuccess',
  LOGOUT: 'logout',
  IDENTITY_VERIFICATION: 'identityVerification'
};

export const loginStepsWhichDoesntNeed = [loginSteps.LOGOUT];

export const nonOpenableLoginRoutes = [
  loginSteps.EMAIL_CONFIRMATION,
  loginSteps.RECOVER_SUCCESS,
  loginSteps.IDENTITY_VERIFICATION
];
