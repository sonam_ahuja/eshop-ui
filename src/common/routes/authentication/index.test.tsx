import { Provider } from 'react-redux';
import React from 'react';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import { histroyParams } from '@mocks/common/histroy';
import actions from '@common/store/actions/common';
import authActions from '@authentication/store/actions';

import Authentication, {
  Authentication as AuthenticationComponent,
  IComponentProps,
  mapDispatchToProps
} from '.';

// tslint:disable-next-line:no-big-function
describe('<Authentication />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = false;
  const props: IComponentProps = {
    ...histroyParams,
    isLoginButtonClicked: true,
    loginViaBasket: true,
    loaderText: '',
    isLoading: true,
    isIdentityVerificatonAllowed: true,
    isUserLoggedIn: true,
    msisdnValue: '',
    cancelationTokenHeaderEnrichmentApi: null,
    headerEnrichmentTranslation: {
      hiText: 'hi',
      loginMessage: 'login3',
      cancelationButtonText: 'cancel'
    },
    showHeaderEnrichmentModal: false,
    checkIfIsloggedInUser: jest.fn(),
    loginWithHeaderEnrichment: jest.fn(),
    openLoginFromViaSummaryButton: jest.fn(),
    openLogin: jest.fn(),
    common: appState().common
  };
  const mockStore = configureStore();

  test('should render properly', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Authentication {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when valid loginStep query is preset and user is not logged in ', () => {
    const newProps: IComponentProps = { ...props, isUserLoggedIn: false };
    newProps.location.search = '?loginStep=enterCredentials';
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when valid loginStep query is preset,user is not logged in and msisdn is also present', () => {
    const newProps: IComponentProps = { ...props, isUserLoggedIn: false };
    newProps.location.search = '?loginStep=sdfds';
    newProps.msisdnValue = '123123';
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when valid loginStep query is preset and user is logged in ', () => {
    const newProps: IComponentProps = { ...props, isUserLoggedIn: true };
    newProps.location.search = '?loginStep=enterOtp';
    jest
      .spyOn(window.localStorage.__proto__, 'getItem')
      .mockReturnValue('true');
    window.localStorage.__proto__.setItem = jest.fn();
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isUserLoggedIn is false', () => {
    const newProps: IComponentProps = { ...props, isUserLoggedIn: false };
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isLoginButtonClicked is false', () => {
    const newProps: IComponentProps = { ...props, isLoginButtonClicked: false };
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when loginViaBasket is false', () => {
    const newProps: IComponentProps = { ...props, loginViaBasket: false };
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isIdentityVerificatonAllowed is false', () => {
    const newProps: IComponentProps = {
      ...props,
      isIdentityVerificatonAllowed: false
    };
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isLoading is false', () => {
    const newProps: IComponentProps = {
      ...props,
      isLoading: false
    };
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when component update', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const newProps: IComponentProps = {
      ...props,
      isUserLoggedIn: true,
      isIdentityVerificatonAllowed: true
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <AuthenticationComponent {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when component update with isUserLogin false', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const newProps: IComponentProps = {
      ...props,
      isUserLoggedIn: false,
      isIdentityVerificatonAllowed: true
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <AuthenticationComponent {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly, when component update on user logged out', () => {
    const newProps: IComponentProps = {
      ...props,
      isUserLoggedIn: false,
      isIdentityVerificatonAllowed: true
    };
    const component = shallow(<AuthenticationComponent {...newProps} />);
    expect(component).toMatchSnapshot();
  });

  test('cancelHeaderEnrichmentLoginApiCall should be called', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const newProps: IComponentProps = {
      ...props,
      isUserLoggedIn: false,
      isIdentityVerificatonAllowed: true
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Authentication {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component
      .find('Authentication')
      .instance() as AuthenticationComponent).cancelHeaderEnrichmentLoginApiCall();

    newProps.common.userIdentityVerificationDetails.verificationRequired = true;
    newProps.isUserLoggedIn = true;

    (component
      .find('Authentication')
      .instance() as AuthenticationComponent).componentWillReceiveProps(
      newProps
    );
  });

  test('cancelHeaderEnrichmentLoginApiCall should be called with cancel token ', () => {
    const initStateValue: RootState = appState();
    // tslint:disable:no-any
    initStateValue.authentication.cancelationTokenForHeaderEnrichment = {
      cancel: jest.fn()
    } as any;
    initStateValue.authentication.showHeaderEnrichmentModal = true;
    const store = mockStore(initStateValue);
    const newProps: IComponentProps = {
      ...props
    };

    try {
      const component = mount(
        <StaticRouter context={{}}>
          <ThemeProvider theme={{}}>
            <Provider store={store}>
              <Authentication {...newProps} />
            </Provider>
          </ThemeProvider>
        </StaticRouter>
      );

      (component
        .find('Authentication')
        .instance() as AuthenticationComponent).cancelHeaderEnrichmentLoginApiCall();
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.log('error ', error);
    }
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).openLogin(true);
    mapDispatchToProps(dispatch).openLoginFromViaSummaryButton(true);
    mapDispatchToProps(dispatch).checkIfIsloggedInUser();
    mapDispatchToProps(dispatch).loginWithHeaderEnrichment('123');

    expect(dispatch.mock.calls[0][0]).toEqual(
      actions.setIfLoginButtonClicked(true)
    );
    expect(dispatch.mock.calls[1][0]).toEqual(
      actions.routeToLoginFromBasket(true)
    );
    expect(dispatch.mock.calls[2][0]).toEqual(
      authActions.checkIfIsloggedInUserOrnot()
    );
    expect(dispatch.mock.calls[3][0]).toEqual(
      authActions.loginWithHeaderEnrichment('123')
    );
  });
});
