import { CancelTokenSource } from 'axios';
import React, { Component, ReactNode } from 'react';
import { RootState } from '@src/common/store/reducers';
import { Dispatch } from 'redux';
import actions from '@authentication/store/actions';
import { ICommonState } from '@src/common/store/types/common';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import { Button } from 'dt-components';
import { ILoginWithHeaderEnrichment } from '@store/types/translation';
import { connect } from 'react-redux';
import appConstants from '@src/common/constants/appConstants';
import { RouteComponentProps, withRouter } from 'react-router';
import commonActions from '@common/store/actions/common';
import { parseQueryString } from '@common/utils/parseQuerySrting';
import LoginWithUser from '@authentication/LoginWithUser';
import {
  loginQueryParam,
  loginSteps,
  nonOpenableLoginRoutes
} from '@authentication/constants';
import { removeParamFromQueryString } from '@src/common/utils/removeQueryParam';
import history from '@src/client/history';

export interface IComponentStateToProps {
  isLoginButtonClicked: boolean;
  loginViaBasket: boolean;
  loaderText: ReactNode;
  isLoading: boolean;
  showHeaderEnrichmentModal: boolean;
  isIdentityVerificatonAllowed: boolean;
  isUserLoggedIn: boolean;
  common: ICommonState;
  msisdnValue: string;
  headerEnrichmentTranslation: ILoginWithHeaderEnrichment;
  cancelationTokenHeaderEnrichmentApi: CancelTokenSource | null;
}

export interface IComponentDispatchToProps {
  checkIfIsloggedInUser(): void;
  loginWithHeaderEnrichment(msisdn: string): void;
  openLogin(isLoginButtonClicked: boolean): void;
  openLoginFromViaSummaryButton(isCheckoutButtonClicked: boolean): void;
}

export type IComponentProps = IComponentStateToProps &
  IComponentDispatchToProps &
  RouteComponentProps;

export class Authentication extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
    this.cancelHeaderEnrichmentLoginApiCall = this.cancelHeaderEnrichmentLoginApiCall.bind(
      this
    );
  }

  componentDidMount(): void {
    const msisdnValue = this.props.msisdnValue;
    this.props.checkIfIsloggedInUser();
    const parsedQuery = parseQueryString(
      decodeURIComponent(this.props.location.search)
    );
    const isUserLoggedIn = localStorage.getItem(appConstants.IS_USER_LOGGED_IN);
    if (
      isUserLoggedIn === 'true' ||
      (parsedQuery.loginStep &&
        nonOpenableLoginRoutes.includes(parsedQuery.loginStep))
    ) {
      this.removeLoginQueryParam();
    } else {
      if (
        Object.values(loginSteps).includes(parsedQuery.loginStep) &&
        !!parsedQuery.loginStep
      ) {
        this.props.openLogin(!!parsedQuery.loginStep);
      } else {
        this.removeLoginQueryParam();
        if (msisdnValue && isUserLoggedIn !== 'true') {
          this.props.loginWithHeaderEnrichment(msisdnValue);
        }
      }
    }
  }

  removeLoginQueryParam(): void {
    if (history) {
      history.replace({
        pathname: history.location.pathname,
        search: encodeURIComponent(
          removeParamFromQueryString(
            loginQueryParam,
            decodeURIComponent(history.location.search)
          )
        )
      });
    }
  }

  // tslint:disable-next-line:cognitive-complexity
  componentWillReceiveProps(nextProps: IComponentProps): void {
    const {
      common: { isLoginButtonClicked, routeFromBasketToLogin }
    } = nextProps;
    const parsedQuery = parseQueryString(
      decodeURIComponent(nextProps.location.search)
    );
    if (
      nextProps.location.search !== this.props.location.search &&
      decodeURIComponent(nextProps.location.search).includes(loginQueryParam)
    ) {
      if (nextProps.isUserLoggedIn) {
        if (
          (history &&
            !!parsedQuery.loginStep &&
            parsedQuery.loginStep.includes(loginSteps.IDENTITY_VERIFICATION)) ||
          parsedQuery.loginStep.includes(loginSteps.ENTER_OTP)
        ) {
          this.props.openLoginFromViaSummaryButton(!!parsedQuery.loginStep);
        } else {
          this.removeLoginQueryParam();
        }
      } else {
        if (
          !nonOpenableLoginRoutes.includes(parsedQuery.loginStep) &&
          !(isLoginButtonClicked || routeFromBasketToLogin)
        ) {
          this.props.openLogin(!!parsedQuery.loginStep);
        }
      }
    }
  }

  cancelHeaderEnrichmentLoginApiCall(): void {
    const { cancelationTokenHeaderEnrichmentApi } = this.props;
    if (cancelationTokenHeaderEnrichmentApi) {
      cancelationTokenHeaderEnrichmentApi.cancel(
        'Header enrichment call cancelled'
      );
      this.props.openLogin(true);
    }
  }

  render(): ReactNode {
    const { isLoginButtonClicked, headerEnrichmentTranslation } = this.props;
    const { loginViaBasket, showHeaderEnrichmentModal } = this.props;
    const query = decodeURIComponent(this.props.location.search);

    return (
      <>
        {showHeaderEnrichmentModal ? (
          <DialogBoxApp
            isOpen={true}
            showCloseButton={false}
            closeOnBackdropClick={true}
            closeOnEscape={true}
            backgroundScroll={false}
            type='flexibleHeight'
          >
            <div className='dialogBoxHeader'>
              {headerEnrichmentTranslation.hiText}
            </div>
            <div className='dialogBoxBody'>
              {headerEnrichmentTranslation.loginMessage}
            </div>
            <div className='dialogBoxFooter'>
              <Button
                size='medium'
                type='secondary'
                onClickHandler={this.cancelHeaderEnrichmentLoginApiCall}
              >
                {headerEnrichmentTranslation.cancelationButtonText}
              </Button>
            </div>
          </DialogBoxApp>
        ) : null}
        <>
          {query.includes(loginQueryParam) &&
          (isLoginButtonClicked || loginViaBasket) ? (
            <LoginWithUser />
          ) : null}
        </>
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IComponentStateToProps => ({
  isLoginButtonClicked: state.common.isLoginButtonClicked,
  loginViaBasket: state.common.routeFromBasketToLogin,
  loaderText: state.authentication.loadingText,
  isLoading: state.authentication.loading,
  showHeaderEnrichmentModal: state.authentication.showHeaderEnrichmentModal,
  cancelationTokenHeaderEnrichmentApi:
    state.authentication.cancelationTokenForHeaderEnrichment,
  isUserLoggedIn: state.common.isLoggedIn,
  msisdnValue: state.common.msisdnValue,
  isIdentityVerificatonAllowed:
    state.configuration.cms_configuration.modules.checkout.identityVerification
      .enabled,
  common: state.common,
  headerEnrichmentTranslation:
    state.translation.cart.login.loginWithHeaderEnrichment
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IComponentDispatchToProps => ({
  checkIfIsloggedInUser(): void {
    dispatch(actions.checkIfIsloggedInUserOrnot());
  },
  loginWithHeaderEnrichment(msisdn: string): void {
    dispatch(actions.loginWithHeaderEnrichment(msisdn));
  },
  openLogin(isLoginButtonClicked: boolean): void {
    dispatch(commonActions.setIfLoginButtonClicked(isLoginButtonClicked));
  },
  openLoginFromViaSummaryButton(isCheckoutButtonClicked: boolean): void {
    dispatch(commonActions.routeToLoginFromBasket(isCheckoutButtonClicked));
  }
});

export default withRouter(
  connect<
    IComponentStateToProps,
    IComponentDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(Authentication)
);
