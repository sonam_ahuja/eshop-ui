import actions from '@authentication/store/actions';
import constants from '@authentication/store/constants';

describe('billing actions', () => {
  const payload = {
    PIN: '1234',
    context: 'context',
    device: {
      browser: 'chrome'
    },
    nonce: 'ssdsd',
    serviceId: '1234',
    serviceType: '1212121'
  };
  it('loginWithUsernamePassword action creator should return a object with expected value', () => {
    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    const expectedAction = {
      type: constants.LOGIN_WITH_USERNAME_PASSWORD,
      payload: loginPayload
    };
    expect(actions.loginWithUsernamePassword(loginPayload)).toEqual(
      expectedAction
    );
  });

  it('loginWithUsernamePasswordError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_USERNAME_PASSWORD_ERROR,
      payload: {}
    };
    expect(actions.loginWithUsernamePasswordError({})).toEqual(expectedAction);
  });
  it('loginWithUsernamePasswordSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_USERNAME_PASSWORD_SUCCESS,
      payload: undefined
    };
    expect(actions.loginWithUsernamePasswordSuccess()).toEqual(expectedAction);
  });

  it('requestOtpForEmailLogin action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.REQUEST_EMAIL_OTP,
      payload
    };
    expect(actions.requestOtpForEmailLogin(payload)).toEqual(expectedAction);
  });

  it('requestOtpForEmailLoginError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.REQUEST_EMAIL_OTP_ERROR,
      payload: {}
    };
    expect(actions.requestOtpForEmailLoginError({})).toEqual(expectedAction);
  });
  it('requestOtpForEmailLoginSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.REQUEST_EMAIL_OTP_SUCCESS,
      payload: undefined
    };
    expect(actions.requestOtpForEmailLoginSuccess()).toEqual(expectedAction);
  });
  it('loginWithEmailOtp action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_EMAIL_OTP,
      payload
    };
    expect(actions.loginWithEmailOtp(payload)).toEqual(expectedAction);
  });

  it('loginWithEmailOtpError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_EMAIL_OTP_ERROR,
      payload: {}
    };
    expect(actions.loginWithEmailOtpError({})).toEqual(expectedAction);
  });

  it('loginWithEmailOtpSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_EMAIL_OTP_SUCCESS,
      payload: undefined
    };
    expect(actions.loginWithEmailOtpSuccess()).toEqual(expectedAction);
  });

  it('requestOtpForMsisdnLogin action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.REQUEST_MSISDN_OTP,
      payload
    };
    expect(actions.requestOtpForMsisdnLogin(payload)).toEqual(expectedAction);
  });

  it('requestOtpForMsisdnLoginError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.REQUEST_MSISDN_OTP_ERROR,
      payload: {}
    };
    expect(actions.requestOtpForMsisdnLoginError({})).toEqual(expectedAction);
  });
  it('requestOtpForMsisdnLoginSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.REQUEST_MSISDN_OTP_SUCCESS,
      payload: undefined
    };
    expect(actions.requestOtpForMsisdnLoginSuccess()).toEqual(expectedAction);
  });
  it('loginWithServiceIdOtp action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_MSISDN_OTP,
      payload
    };
    expect(actions.loginWithServiceIdOtp(payload)).toEqual(expectedAction);
  });
  it('loginWithServiceIdOtpError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_MSISDN_OTP_ERROR,
      payload: {}
    };
    expect(actions.loginWithServiceIdOtpError({})).toEqual(expectedAction);
  });
  it('loginWithServiceIdOtpSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.LOGIN_WITH_MSISDN_OTP_SUCCESS,
      payload: undefined
    };
    expect(actions.loginWithServiceIdOtpSuccess()).toEqual(expectedAction);
  });
  it('recoveryUsingEmailId action creator should return a object with expected value', () => {
    const newPayload = {
      email: 'xxxxxx@gmail.com',
      telekomId: '12334'
    };
    const expectedAction = {
      type: constants.RECOVER_CREDENTIALS_WITH_EMAIL,
      payload: newPayload
    };
    expect(actions.recoveryUsingEmailId(newPayload)).toEqual(expectedAction);
  });

  it('recoveryUsingEmailIdError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.RECOVER_CREDENTIALS_WITH_EMAIL_ERROR,
      payload: {}
    };
    expect(actions.recoveryUsingEmailIdError({})).toEqual(expectedAction);
  });
  it('recoveryUsingEmailIdSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.RECOVER_CREDENTIALS_WITH_EMAIL_SUCCESS,
      payload: undefined
    };
    expect(actions.recoveryUsingEmailIdSuccess()).toEqual(expectedAction);
  });
});
