import authenticationState from '@authentication/store/state';
import authenticationReducer from '@authentication/store/reducer';

describe('authenticationReducer', () => {
  it('authenticationReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(authenticationReducer(authenticationState(), definedAction)).toEqual(
      authenticationState()
    );
  });
});
