import authentication from '@authentication/store/reducer';
import authenticationState from '@authentication/store/state';
import { IAuthenticationState } from '@authentication/store/types';
import actions from '@authentication/store/actions';

import { LOGIN_STEPS, LOGIN_TYPES } from '../enum';

describe('Authentication Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IAuthenticationState = authentication(
    undefined,
    definedAction
  );
  let expectedState: IAuthenticationState = authenticationState();
  const initializeValue = () => {
    initialStateValue = authentication(undefined, definedAction);
    expectedState = authenticationState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('PROCEED_TO_NEXT_SCREEN should mutate address field parameter in state for ENTER_OTP state', () => {
    const newOTPState: IAuthenticationState = {
      ...expectedState,
      activeStep: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      showEnterCredentialsScreen: true,
      showOtpScreen: false
    };
    // ENTER_OTP
    let otpAction = actions.proceedToNextScreen();
    let newMutatedState = authentication(initialStateValue, otpAction);
    expect(newMutatedState).toEqual(newOTPState);
    // When there is error
    const setError = actions.setError({ message: 'Error' });
    newMutatedState = authentication(initialStateValue, setError);
    otpAction = actions.proceedToNextScreen();
    newMutatedState = authentication(newMutatedState, otpAction);
    newOTPState.error.message = 'Error';
    newOTPState.activeStep = LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL;
    expect(newMutatedState).toEqual(newOTPState);
  });
  it('SET_LOGIN_TYPE should mutate address field parameter in state', () => {
    const passwordAction = actions.setLoginType(LOGIN_TYPES.USERNAME_PASSWORD);
    const passwordState = authentication(initialStateValue, passwordAction);
    expect(passwordState).toEqual(initialStateValue);
  });
  it('SET_NONCE_FOR_MSISDN should mutate NONCE of MSISDN in state', () => {
    const newStatePasswordState = { ...authenticationState() };
    newStatePasswordState.phone.nonce = LOGIN_TYPES.USERNAME_PASSWORD;
    const setNonce = actions.setNonceForMsisdn(LOGIN_TYPES.USERNAME_PASSWORD);
    const nonceState = authentication(initialStateValue, setNonce);
    expect(nonceState).toEqual(newStatePasswordState);
  });
  it('SET_NONCE_FOR_EMAIL should mutate NONCE of EMAIL in state', () => {
    const newStatePasswordState = { ...authenticationState() };
    newStatePasswordState.email.nonce = LOGIN_TYPES.USERNAME_PASSWORD;
    const setNonce = actions.setNonceForEmail(LOGIN_TYPES.USERNAME_PASSWORD);
    const nonceState = authentication(initialStateValue, setNonce);
    expect(nonceState).toEqual(newStatePasswordState);
  });
  it('SET_USERNAME should mutate SET_USERNAME in state', () => {
    const newState = { ...authenticationState() };
    newState.username.value = LOGIN_TYPES.USERNAME_PASSWORD;
    const setUsername = actions.setUsername(LOGIN_TYPES.USERNAME_PASSWORD);
    const newMutatedState = authentication(initialStateValue, setUsername);
    expect(newMutatedState).toEqual(newState);
  });
  it('SET_EMAIL should mutate SET_EMAIL in state', () => {
    const newState = { ...authenticationState() };
    newState.email.value = LOGIN_TYPES.USERNAME_PASSWORD;
    const setEmail = actions.setEmail(LOGIN_TYPES.USERNAME_PASSWORD);
    const newMutatedState = authentication(initialStateValue, setEmail);
    expect(newMutatedState).toEqual(newState);
  });
  it('SET_PHONE_NUMBER should mutate SET_PHONE_NUMBER in state', () => {
    const newState = { ...authenticationState() };
    newState.phone.value = LOGIN_TYPES.USERNAME_PASSWORD;
    const setPhone = actions.setPhoneNumber(LOGIN_TYPES.USERNAME_PASSWORD);
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });
  it('REQUEST_EMAIL_OTP_ERROR should mutate REQUEST_EMAIL_OTP_ERROR in state', () => {
    const setPhone = actions.requestOtpForEmailLoginError({});
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(initialStateValue);
  });

  it('REQUEST_EMAIL_OTP_SUCCESS should mutate REQUEST_EMAIL_OTP_SUCCESS in state', () => {
    const setPhone = actions.requestOtpForEmailLoginSuccess();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(initialStateValue);
  });

  it('REQUEST_MSISDN_OTP_ERROR should mutate MSISDN in state', () => {
    const setPhone = actions.requestOtpForMsisdnLoginError({});
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(initialStateValue);
  });

  it('REQUEST_MSISDN_OTP_SUCCESS should mutate REQUEST_MSISDN_OTP_SUCCESS in state', () => {
    const setPhone = actions.requestOtpForMsisdnLoginSuccess();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(initialStateValue);
  });

  it('RESET_STATE should mutate RESET_STATE in state', () => {
    const setPhone = actions.resetAuthenticationState();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(initialStateValue);
  });

  it('GO_TO_RECOVERY_SCREEN should mutate GO_TO_RECOVERY_SCREEN in state', () => {
    const newState = { ...authenticationState() };
    newState.activeStep = LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL;
    const setPhone = actions.goToRecoveryScreen();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('GO_TO_PREVIOUS_SCREEN should mutate GO_TO_PREVIOUS_SCREEN in state', () => {
    const newInitialStateValue = { ...initialStateValue };
    const newState = { ...authenticationState() };
    newInitialStateValue.activeStep = LOGIN_STEPS.ENTER_PASSWORD;
    newState.activeStep = LOGIN_STEPS.ENTER_PASSWORD;
    newState.error.code = 0;
    let setPhone = actions.goToPreviousScreen();
    let newMutatedState = authentication(newInitialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);

    newInitialStateValue.activeStep = LOGIN_STEPS.RECOVER;
    newState.activeStep = LOGIN_STEPS.RECOVER;
    setPhone = actions.goToPreviousScreen();
    newMutatedState = authentication(newInitialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('SET_ERROR should mutate SET_ERROR in state with error', () => {
    const newState = { ...authenticationState() };
    newState.error.message = 'Error';
    const setPhone = actions.setError({ message: 'Error' });
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  // tslint:disable
  // it('LOGIN_WITH_HEADER_ENRICHMENT should showHeaderEnrichmentModal true', () => {
  //   const newState = { ...authenticationState() };
  //   const setShowHeaderEnrichmentModalStateTrue = actions.loginWithHeaderEnrichment(
  //     'true'
  //   );
  //   const newMutatedState = authentication(
  //     initialStateValue,
  //     setShowHeaderEnrichmentModalStateTrue
  //   );
  //   expect(newMutatedState).toEqual(newState);
  // });

  it('LOGIN_WITH_HEADER_ENRICHMENT should showHeaderEnrichmentModal false', () => {
    const newState = { ...authenticationState() };
    let setShowHeaderEnrichmentModalStatefalse = actions.loginWithHeaderEnrichmentSuccess();
    let newMutatedState = authentication(
      initialStateValue,
      setShowHeaderEnrichmentModalStatefalse
    );
    expect(newMutatedState).toEqual(newState);

    setShowHeaderEnrichmentModalStatefalse = actions.loginWithHeaderEnrichmentError();
    newMutatedState = authentication(
      initialStateValue,
      setShowHeaderEnrichmentModalStatefalse
    );
    expect(newMutatedState).toEqual(newState);
  });

  it('SAVE_HEADER_ENRICHMENT_CANCELATION_TOKEN should showHeaderEnrichmentModal', () => {
    const newState = { ...authenticationState() };
    // tslint:disable-next-line: no-any
    const signal = actions.saveCancelationTokenForHeaderEnrichmentAPI(
      null as any
    );
    const newMutatedState = authentication(initialStateValue, signal);
    expect(newMutatedState).toEqual(newState);
  });
});
