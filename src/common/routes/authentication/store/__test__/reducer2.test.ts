import authentication from '@authentication/store/reducer';
import authenticationState from '@authentication/store/state';
import {
  IAuthenticationState,
  ILoginUsernameRequest
} from '@authentication/store/types';
import actions from '@authentication/store/actions';

describe('Authentication Reducer 2', () => {
  const payload = {
    PIN: '',
    context: '',
    device: {
      browser: 'string'
    },
    nonce: '',
    serviceId: '',
    serviceType: ''
  };
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IAuthenticationState = authentication(
    undefined,
    definedAction
  );

  const initializeValue = () => {
    initialStateValue = authentication(undefined, definedAction);
  };

  beforeEach(() => {
    initializeValue();
  });

  it('SET_ERROR should mutate SET_ERROR in state', () => {
    const newState = { ...authenticationState() };
    newState.error.code = 1;
    const setPhone = actions.setError({ code: 1 });
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });
  it('SET_ERROR should mutate SET_ERROR in state', () => {
    const setPhone = actions.setError({});
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(initialStateValue);
  });
  it('REQUEST_EMAIL_OTP should mutate SET_ERROR in state', () => {
    const newState = { ...authenticationState() };
    newState.loading = true;
    const setPhone = actions.requestOtpForEmailLogin(payload);
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('REQUEST_MSISDN_OTP should mutate SET_ERROR in state', () => {
    const newState = { ...authenticationState() };
    newState.loading = true;
    const setPhone = actions.requestOtpForMsisdnLogin(payload);
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('SEND_SOCIAL_MEDIA_CODE_TO_BACKEND should mutate SET_ERROR in state', () => {
    const newState = { ...authenticationState() };
    newState.loading = true;
    const setPhone = actions.sendSocialMediaCodeToBackend({
      callbackUrl: '/facebook',
      facebookCode: '12345'
    });
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('SEND_SOCIAL_MEDIA_CODE_TO_BACKEND_ERROR should mutate SET_ERROR in state', () => {
    const newState = { ...authenticationState() };
    newState.loading = false;
    const setPhone = actions.sendSocialMediaCodeToBackendError({});
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('SEND_SOCIAL_MEDIA_CODE_TO_BACKEND_SUCCESS should mutate SET_ERROR in state', () => {
    const newState = { ...authenticationState() };
    newState.loading = false;
    const setPhone = actions.sendSocialMediaCodeToBackendSuccess();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toEqual(newState);
  });

  it('RECOVER_CREDENTIALS_WITH_EMAIL_SUCCESS should mutate SET_ERROR in state', () => {
    const setPhone = actions.recoveryUsingEmailIdSuccess();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_PROGRESS_LOADING_TEXT should mutate SET_ERROR in state', () => {
    const setPhone = actions.setLoadingTextForProgressModal('');
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('RECOVER_CREDENTIALS_WITH_EMAIL should mutate SET_ERROR in state', () => {
    const setPhone = actions.recoveryUsingEmailId({
      email: 'string',
      telekomId: 'string'
    });
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('RESET_PASSWORD_RECOVERED_STATE should mutate SET_ERROR in state', () => {
    const setPhone = actions.resetPasswordRecoveryState();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('RECOVER_CREDENTIALS_WITH_EMAIL_ERROR should mutate SET_ERROR in state', () => {
    const setPhone = actions.recoveryUsingEmailIdError({});
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('LOGIN_WITH_USERNAME_PASSWORD should mutate SET_ERROR in state', () => {
    const params: ILoginUsernameRequest = {
      type: 'string',
      telekomLogin: {
        username: 'string',
        // tslint:disable-next-line: no-hardcoded-credentials
        password: 'string'
      },
      device: {
        browser: 'string'
      }
    };
    const setPhone = actions.loginWithUsernamePassword(params);
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('LOGIN_WITH_USERNAME_PASSWORD_ERROR should mutate SET_ERROR in state', () => {
    const setPhone = actions.loginWithUsernamePasswordError({});
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('LOGIN_WITH_USERNAME_PASSWORD_SUCCESS should mutate SET_ERROR in state', () => {
    const setPhone = actions.loginWithUsernamePasswordSuccess();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('IS_USER_BANK_AUTHENTICATED should mutate SET_ERROR in state', () => {
    const setPhone = actions.setIfUserBankAutheticated(true);
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_LOADING should mutate SET_ERROR in state', () => {
    const setPhone = actions.setLoading();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_LOADING_OFF should mutate SET_ERROR in state', () => {
    const setPhone = actions.setLoadingOff();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_BANK_NAME should mutate SET_ERROR in state', () => {
    const setPhone = actions.setBankName('name');
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('IS_EMAIL_UNIQUE should mutate SET_ERROR in state', () => {
    const setPhone = actions.isEmailUnique();
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_BUTTON_LOADING should mutate SET_ERROR in state', () => {
    const setPhone = actions.setButtonLoading(true);
    const newMutatedState = authentication(initialStateValue, setPhone);
    expect(newMutatedState).toBeDefined();
  });
});
