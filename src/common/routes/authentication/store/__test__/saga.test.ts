import authenticationSaga from '@authentication/store/sagas/';
import {
  closeLoginModal,
  credentailsRecoveryWithEmail,
  goToNextScreenWhenLoginSet,
  loginWithHeaderEnrichment
} from '@authentication/store/sagas/common';
import {
  loginWithSocialMedia,
  sendSocialMediaCodeToBackend
} from '@authentication/store/sagas/socialLoginSagas';
import { logout, onlogoutSuccess } from '@authentication/store/sagas/logout';

import { LOGIN_TYPES } from '../enum';
describe('Authentication Saga', () => {
  it('fetchConfiguration saga test', () => {
    authenticationSaga.forEach((authSaga: Generator) => {
      expect(authSaga).toBeDefined();
    });
  });
  it('logout saga test', () => {
    const logoutGenerator = logout();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('onlogoutSuccess saga test', () => {
    const logoutGenerator = onlogoutSuccess();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('closingModal saga test', () => {
    const closingModalGenerator = closeLoginModal();
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeUndefined();
  });

  it('credentailsRecoveryWithEmail saga test', () => {
    const credentailsRecoveryWithEmailGenerator = credentailsRecoveryWithEmail({
      type: '',
      payload: { email: 'hello' }
    });
    expect(credentailsRecoveryWithEmailGenerator.next().value).toBeUndefined();
  });

  it('loginWithSocialMedia saga test', () => {
    const loginWithSocialMediaGenerator = loginWithSocialMedia({
      type: '',
      payload: 'hello'
    });
    expect(loginWithSocialMediaGenerator.next().value).toBeDefined();
  });

  it('headerEnrichment saga test', () => {
    const headerEnrichmentGenerator = loginWithHeaderEnrichment({
      type: '',
      payload: 'hello'
    });
    expect(headerEnrichmentGenerator.next().value).toBeDefined();
  });

  it('sendSocialMediaCodeToBackend saga test', () => {
    const sendSocialMediaCodeToBackendGenerator = sendSocialMediaCodeToBackend({
      type: '',
      payload: {
        facebookCode: '',
        callbackUrl: ''
      }
    });
    expect(sendSocialMediaCodeToBackendGenerator.next().value).toBeDefined();
  });

  it('gotonextscreemn saga test', () => {
    let goToNextScreenWhenLoginSetGenerator = goToNextScreenWhenLoginSet({
      type: '',
      payload: LOGIN_TYPES.EMAIL_OTP
    });
    expect(goToNextScreenWhenLoginSetGenerator.next().value).toBeDefined();
    expect(goToNextScreenWhenLoginSetGenerator.return).toBeDefined();

    goToNextScreenWhenLoginSetGenerator = goToNextScreenWhenLoginSet({
      type: '',
      payload: LOGIN_TYPES.MSISDN_OTP
    });
    expect(goToNextScreenWhenLoginSetGenerator.next().value).toBeDefined();
    expect(goToNextScreenWhenLoginSetGenerator.return).toBeDefined();

    goToNextScreenWhenLoginSetGenerator = goToNextScreenWhenLoginSet({
      type: '',
      payload: LOGIN_TYPES.TELEKOM_LOGIN
    });
    expect(goToNextScreenWhenLoginSetGenerator.next().value).toBeDefined();
    expect(goToNextScreenWhenLoginSetGenerator.return).toBeDefined();
  });
});
