import commonSagas, {
  checkIfLoggedIn,
  closeLoginModal,
  credentailsRecoveryWithEmail,
  goToHomePage,
  goToNextScreenWhenLoginSet,
  loginWithHeaderEnrichment,
  routeAfterVerificationCheck
} from '@authentication/store/sagas/common';
import { IRecoveryWithEmailRequest } from '@authentication/store/types';
import constants from '@authentication/store/constants';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import appState from '@store/states/app';
import { ErrorData } from '@mocks/common';
import { LOGIN_TYPES } from '@authentication/store/enum';

jest.mock('@common/store', () => ({
  getState: () => ({ ...appState() })
}));

describe('Common sagas', () => {
  it('common saga test', () => {
    const sagaGenerator = commonSagas();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeUndefined();
  });

  it('credentails recovery with email saga test', () => {
    const payloadData: IRecoveryWithEmailRequest = {
      email: 'xxxxx@gmail.com',
      telekomId: '23'
    };
    const payload: {
      type: string;
      payload: credentialRecoveryWithEmailApi.POST.IRequest;
    } = { type: constants.LOGIN_WITH_EMAIL_OTP, payload: payloadData };
    const sagaGenerator = credentailsRecoveryWithEmail(payload);
    expect(sagaGenerator.next().value).toBeUndefined();
  });

  it('close login modal', async () => {
    const sagaGenerator = closeLoginModal();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeUndefined();
  });

  it('go to next screen when login set saga error', async () => {
    const payload: {
      type: string;
      payload: LOGIN_TYPES;
    } = { type: constants.REQUEST_EMAIL_OTP, payload: LOGIN_TYPES.EMAIL_OTP };

    const sagaGenerator = goToNextScreenWhenLoginSet(payload);
    await sagaGenerator.next(payload).value;
    expect(sagaGenerator.next().value).toBeUndefined();
  });

  it('loginWithHeaderEnrichment saga error', async () => {
    const payload: {
      type: string;
      payload: string;
    } = { type: constants.REQUEST_EMAIL_OTP, payload: '' };

    const sagaGenerator = loginWithHeaderEnrichment(payload);
    expect(sagaGenerator.next(ErrorData).value).toBeDefined();
    expect(sagaGenerator.next(ErrorData).value).toBeDefined();
    expect(sagaGenerator.next(ErrorData).value).toBeDefined();
    expect(sagaGenerator.next(ErrorData).value).toBeDefined();
    expect(sagaGenerator.next(ErrorData).value).toBeDefined();
    expect(sagaGenerator.next(ErrorData).value).toBeUndefined();
  });

  it('goToHomePage saga error', async () => {
    const generator = goToHomePage();
    expect(generator.next(ErrorData).value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next(ErrorData).value).toBeUndefined();
  });

  it('checkIfLoggedIn saga error', async () => {
    const generator = checkIfLoggedIn();
    expect(generator.next().value).toBeDefined();
  });

  it('routeAfterVerificationCheck saga error', async () => {
    const generator = routeAfterVerificationCheck();
    expect(generator.next(ErrorData).value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
    expect(generator.next(ErrorData).value).toBeUndefined();
  });
});
