import MockAdapter from 'axios-mock-adapter';
import emailOtpLoginSagas, {
  loginWithEmailOtp,
  requestOtpForEmailId
} from '@authentication/store/sagas/emailOtpLoginSagas';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import { ILoginOtpRequest } from '@authentication/store/types';
import axios from 'axios';
import constants from '@authentication/store/constants';
import { apiEndpoints } from '@common/constants';
import appState from '@store/states/app';
import { call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import actions from '@authentication/store/actions';
import { ErrorData } from '@mocks/common';

jest.mock('@common/store', () => ({
  getState: () => ({ ...appState() })
}));

describe('Email otp login sagas', () => {
  const mock = new MockAdapter(axios);
  it('emailOtpLoginSagas saga test', () => {
    const emailOtpLoginSagasGenerator = emailOtpLoginSagas();
    expect(emailOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(emailOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(emailOtpLoginSagasGenerator.next().value).toBeUndefined();
  });
  it('loginWithEmailOtp saga test', () => {
    const payloadData: ILoginOtpRequest = {
      PIN: 'pin',
      context: 'context',
      device: {
        browser: 'asfaf'
      },
      nonce: 'nonce',
      serviceId: '12',
      serviceType: 'serviceType'
    };
    const payload: {
      type: string;
      payload: emailOtpLoginApi.PUT.IRequest;
    } = { type: constants.LOGIN_WITH_EMAIL_OTP, payload: payloadData };

    const logoutGenerator = loginWithEmailOtp(payload);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('requestOtpForEmailId saga success', async () => {
    const responsePayload = { nonce: 'nonce' };
    const { url } = apiEndpoints.AUTHENTICATION.EMAIL_OTP_REQUEST;
    mock.onPost(url).reply(200, responsePayload);
    const payloadData: ILoginOtpRequest = {
      PIN: 'pin',
      context: 'context',
      device: {
        browser: 'string'
      },
      nonce: 'nonce',
      serviceId: '12',
      serviceType: 'serviceType'
    };
    const payload: {
      type: string;
      payload: emailOtpLoginApi.PUT.IRequest;
    } = { type: constants.REQUEST_EMAIL_OTP, payload: payloadData };

    const logoutGenerator = requestOtpForEmailId(payload);
    await axios.post(url);
    await logoutGenerator.next(payload).value;
    expect(logoutGenerator.next({ nonce: 'nonce' }).value).toEqual(
      put(actions.setNonceForEmail('nonce'))
    );
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toEqual(call(delay, 3000));
    expect(logoutGenerator.next().value).toEqual(
      put(actions.requestOtpForEmailLoginSuccess())
    );
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('requestOtpForEmailId saga error', async () => {
    const payloadData: ILoginOtpRequest = {
      PIN: 'pin',
      context: 'context',
      device: {
        browser: 'string'
      },
      nonce: 'nonce',
      serviceId: '12',
      serviceType: 'serviceType'
    };
    const payload: {
      type: string;
      payload: emailOtpLoginApi.PUT.IRequest;
    } = { type: constants.REQUEST_EMAIL_OTP, payload: payloadData };

    const logoutGenerator = requestOtpForEmailId(payload);
    await logoutGenerator.next(payload).value;
    expect(logoutGenerator.next().value).toEqual(call(delay, 3000));
    expect(logoutGenerator.next(ErrorData).value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next(ErrorData).value).toBeDefined();
  });
});
