import sagas from '@authentication/store/sagas/common';

describe('Sagas', () => {
  it('Saga test', () => {
    expect(sagas[0]).toBeUndefined();
    expect(sagas[1]).toBeUndefined();
    expect(sagas[2]).toBeUndefined();
    expect(sagas[3]).toBeUndefined();
    expect(sagas[4]).toBeUndefined();
  });
});
