import logoutSagas, {
  logout,
  onlogoutSuccess
} from '@authentication/store/sagas/logout';
import appState from '@store/states/app';
import { ErrorData } from '@mocks/common';

jest.mock('@common/store', () => ({
  getState: () => ({ ...appState() })
}));

describe('Common sagas', () => {
  it('common saga test', () => {
    const sagaGenerator = logoutSagas();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeDefined();
    expect(sagaGenerator.next().value).toBeUndefined();
  });

  it('logout saga error', async () => {
    const logoutGenerator = logout();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next(ErrorData).value).toBeUndefined();
  });

  it('goToHomePage saga error', async () => {
    const generator = onlogoutSuccess();
    expect(generator.next(ErrorData).value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next(ErrorData).value).toBeUndefined();
  });
});
