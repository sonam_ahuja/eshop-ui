import msisdnOtpLoginSagas, {
  loginWithMsisdnOtp,
  requestOtpForMsisdnId
} from '@authentication/store/sagas/msisdnOtpLoginSagas';
import constants from '@authentication/store/constants';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import { ILoginOtpRequest } from '@authentication/store/types';
import { apiEndpoints } from '@common/constants';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import actions from '@authentication/store/actions';
import { call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';

describe('msisdn otp login sagas', () => {
  const mock = new MockAdapter(axios);
  it('msisdnOtpLoginSagas saga test', () => {
    const msisdnOtpLoginSagasGenerator = msisdnOtpLoginSagas();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeUndefined();
  });

  it('loginWithMsisdnOtp saga test', () => {
    const payloadData: ILoginOtpRequest = {
      PIN: 'pin',
      context: 'context',
      device: {
        browser: 'string'
      },
      nonce: 'nonce',
      serviceId: '12',
      serviceType: 'serviceType'
    };
    const payload: {
      type: string;
      payload: emailOtpLoginApi.PUT.IRequest;
    } = { type: constants.LOGIN_WITH_MSISDN_OTP, payload: payloadData };

    const logoutGenerator = loginWithMsisdnOtp(payload);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('requestOtpForMsisdnId saga test', async () => {
    const responsePayload = { nonce: 'nonce' };
    const { url } = apiEndpoints.AUTHENTICATION.MSISDN_OTP_REQUEST;
    mock.onPost(url).reply(200, responsePayload);
    const payloadData: ILoginOtpRequest = {
      PIN: 'pin',
      context: 'context',
      device: {
        browser: 'chrome'
      },
      nonce: 'nonce',
      serviceId: '12',
      serviceType: 'serviceType'
    };
    const payload: {
      type: string;
      payload: emailOtpLoginApi.PUT.IRequest;
    } = { type: constants.REQUEST_MSISDN_OTP, payload: payloadData };

    const generator = requestOtpForMsisdnId(payload);
    await generator.next(payload).value;
    expect(generator.next({ nonce: 'nonce' }).value).toEqual(
      put(actions.setNonceForMsisdn('nonce'))
    );
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('requestOtpForMsisdnId error saga test', async () => {
    const payloadData: ILoginOtpRequest = {
      PIN: 'pin',
      context: 'context',
      device: {
        browser: 'chrome'
      },
      nonce: 'nonce',
      serviceId: '12',
      serviceType: 'serviceType'
    };
    const payload: {
      type: string;
      payload: emailOtpLoginApi.PUT.IRequest;
    } = { type: constants.REQUEST_MSISDN_OTP, payload: payloadData };

    const generator = requestOtpForMsisdnId(payload);
    await generator.next(payload).value;
    expect(generator.next().value).toEqual(call(delay, 3000));
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toEqual(call(delay, 3000));
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
