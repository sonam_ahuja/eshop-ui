import socialLoginSagas, {
  loginWithGoogle,
  loginWithSocialMedia,
  sendSocialMediaCodeToBackend
} from '@authentication/store/sagas/socialLoginSagas';
import constants from '@authentication/store/constants';
import { IParameterRequiredForSocialMedia } from '@authentication/store/types';
describe('social login sagas', () => {
  it('socialLoginSagas saga test', () => {
    const msisdnOtpLoginSagasGenerator = socialLoginSagas();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeUndefined();
  });

  it('loginWithSocialMedia saga test', () => {
    const payload: {
      type: string;
      payload: string;
    } = { type: constants.LOGIN_WITH_SOCIAL_MEDIA, payload: 'login' };

    const logoutGenerator = loginWithSocialMedia(payload);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('sendSocialMediaCodeToBackend saga test', () => {
    const payloadData: IParameterRequiredForSocialMedia = {
      callbackUrl: '/facebook',
      facebookCode: '12345'
    };
    const payload: {
      type: string;
      payload: IParameterRequiredForSocialMedia;
    } = {
      type: constants.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND,
      payload: payloadData
    };

    const logoutGenerator = sendSocialMediaCodeToBackend(payload);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('loginWithGoogle saga test', () => {
    const payloadData = {
      redirectUrl: '/facebook',
      googleCode: '12345'
    };
    const payload: {
      type: string;
      payload: {
        redirectUrl: string;
        googleCode: string;
      };
    } = {
      type: constants.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND,
      payload: payloadData
    };

    const logoutGenerator = loginWithGoogle(payload);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });
});
