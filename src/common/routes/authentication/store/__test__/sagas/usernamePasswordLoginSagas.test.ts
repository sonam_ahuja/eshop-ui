import usernamePasswordLoginSagas, {
  loginWithUsernamePassword
} from '@authentication/store/sagas/usernamePasswordLoginSagas';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import constants from '@authentication/store/constants';
import { ILoginUsernameRequest } from '@authentication/store/types';

describe('username password login sagas', () => {
  it('usernamePasswordLoginSagas saga test', () => {
    const msisdnOtpLoginSagasGenerator = usernamePasswordLoginSagas();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeDefined();
    expect(msisdnOtpLoginSagasGenerator.next().value).toBeUndefined();
  });

  it('loginWithUsernamePassword saga test', () => {
    const data: ILoginUsernameRequest = {
      type: 'login',
      telekomLogin: {
        username: 'username',
        password: ''
      },
      device: {
        browser: 'String'
      }
    };
    const payload: {
      type: string;
      payload: usernamePasswordLoginApi.POST.IRequest;
    } = { type: constants.LOGIN_WITH_USERNAME_PASSWORD, payload: { ...data } };

    const logoutGenerator = loginWithUsernamePassword(payload);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });
});
