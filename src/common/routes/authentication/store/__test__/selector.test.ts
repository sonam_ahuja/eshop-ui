import {
  createPayloadForHeaderEnrichmentLogin,
  createPayloadForRequestOtpForEmail,
  createPayloadForRequestOtpForMsisdnLogin
} from '@authentication/store/selector';

describe('selector', () => {
  it('createPayloadForHeaderEnrichmentLogin test', () => {
    const result = createPayloadForHeaderEnrichmentLogin('12345');
    expect(createPayloadForHeaderEnrichmentLogin('12345')).toMatchObject(
      result
    );
  });

  it('createPayloadForRequestOtpForEmail test', () => {
    const result = createPayloadForRequestOtpForEmail();
    expect(createPayloadForRequestOtpForEmail()).toMatchObject(result);
  });

  it('createPayloadForRequestOtpForMsisdnLogin test', () => {
    const result = createPayloadForRequestOtpForMsisdnLogin();
    expect(createPayloadForRequestOtpForMsisdnLogin()).toMatchObject(result);
  });
});
