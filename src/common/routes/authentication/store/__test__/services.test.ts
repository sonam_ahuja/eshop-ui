import {
  loginWithEmailOtpService,
  loginWithMsisdnOtpService,
  loginWithUsernamePasswordService,
  OtpForMsisdnService,
  OtpRequestForEmailService,
  recoveryWithEmailService
} from '@authentication/store/services';
import { apiEndpoints } from '@common/constants';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

describe('Authentication Service', () => {
  const mock = new MockAdapter(axios);
  const payload = {
    PIN: '1234',
    context: 'context',
    device: {
      id: '',
      model: 'samsumg',
      pushToken: '12345',
      os: 'chrome',
      browser: ''
    },
    nonce: 'ssdsd',
    serviceId: '1234',
    serviceType: '1212121'
  };
  it('loginWithUsernamePasswordService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.USERNAME_PASSWORD_LOGIN;
    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, loginPayload);
    axios.post(url).then(async () => {
      const result = await loginWithUsernamePasswordService(loginPayload);
      expect(result).toEqual(loginPayload);
    });
  });

  it('OtpRequestForEmailService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.EMAIL_OTP_REQUEST;
    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, loginPayload);
    axios.post(url).then(async () => {
      const result = await OtpRequestForEmailService(payload);
      expect(result).toEqual(loginPayload);
    });
  });

  it('loginWithEmailOtpService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.EMAIL_OTP_LOGIN;

    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, loginPayload);
    axios.post(url).then(async () => {
      const result = await loginWithEmailOtpService(payload);
      expect(result).toEqual(loginPayload);
    });
  });

  it('OtpForMsisdnService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.MSISDN_OTP_REQUEST;

    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, loginPayload);
    axios.post(url).then(async () => {
      const result = await OtpForMsisdnService(payload);
      expect(result).toEqual(loginPayload);
    });
  });

  // tslint:disable-next-line:no-identical-functions
  it('OtpForMsisdnService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.MSISDN_OTP_REQUEST;

    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, loginPayload);
    axios.post(url).then(async () => {
      const result = await OtpForMsisdnService(payload);
      expect(result).toEqual(loginPayload);
    });
  });

  it('loginWithMsisdnOtpService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.MSISDN_OTP_REQUEST;

    const loginPayload = {
      type: 'login',
      telekomLogin: {
        username: 'jone',
        // tslint:disable-next-line:no-hardcoded-credentials
        password: 'xxxxxxxxxxxx'
      },
      device: {
        id: '',
        model: 'crom',
        pushToken: 'xxxxxxxx',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, loginPayload);
    axios.post(url).then(async () => {
      const result = await loginWithMsisdnOtpService(payload);
      expect(result).toEqual(loginPayload);
    });
  });

  it('recoveryWithEmailService success test', async () => {
    const { url } = apiEndpoints.AUTHENTICATION.RECOVERY;
    const recoveryPayload = { email: 'xxxx@gmail.xon' };
    mock.onPost(url).replyOnce(200, recoveryPayload);
    axios.post(url).then(async () => {
      const result = await recoveryWithEmailService(recoveryPayload);
      expect(result).toEqual(recoveryPayload);
    });
  });
});
