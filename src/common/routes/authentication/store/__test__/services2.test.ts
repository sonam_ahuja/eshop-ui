import {
  fetchUserProfileService,
  loginWithHeaderEnrichmentService,
  logoutService,
  socialLoginRequestService,
  socialLoginService
} from '@authentication/store/services';
import { apiEndpoints } from '@common/constants';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { IUserDetails } from '@common/store/types/common';

describe('Authentication Service', () => {
  const mock = new MockAdapter(axios);
  it('socialLoginRequestService success test', async () => {
    const url = apiEndpoints.AUTHENTICATION.SOCIAL_REQUEST.url('/link');
    const recoveryPayload = { email: 'xxxx@gmail.xon' };
    mock.onPost(url).replyOnce(200, recoveryPayload);
    axios.post(url).then(async () => {
      const result = await socialLoginRequestService('/string');
      expect(result).toEqual(recoveryPayload);
    });
  });

  it('logoutService success test', async () => {
    const url = apiEndpoints.AUTHENTICATION.logout.url;
    mock.onDelete(url).replyOnce(200, {});
    axios.delete(url).then(async () => {
      const result = await logoutService();
      expect(result).toEqual({});
    });
  });

  it('socialLoginService success test', async () => {
    const url = apiEndpoints.AUTHENTICATION.SOCIAL_LOGIN.url('/url', '1234');
    const recoveryPayload = { email: 'xxxx@gmail.xon' };
    mock.onPost(url).replyOnce(200, recoveryPayload);
    axios.post(url).then(async () => {
      const result = await socialLoginService({
        callbackUrl: '/basket',
        facebookCode: '122334'
      });
      expect(result).toEqual(recoveryPayload);
    });
  });

  it('loginWithHeaderEnrichmentService success test', async () => {
    const url = apiEndpoints.AUTHENTICATION.SOCIAL_LOGIN.url('/url', '1234');
    const recoveryPayload = {
      serviceId: '12121',
      type: 'type',
      device: {
        id: '',
        model: 'samsumg',
        pushToken: '12345',
        os: 'chrome',
        browser: ''
      }
    };
    mock.onPost(url).replyOnce(200, recoveryPayload);
    axios.post(url).then(async () => {
      const result = await loginWithHeaderEnrichmentService(recoveryPayload);
      expect(result).toEqual(recoveryPayload);
    });
  });

  it('fetchUserProfileService success test', async () => {
    const url = apiEndpoints.AUTHENTICATION.PROFILE.url;
    const recoveryPayload = {
      serviceId: '12121',
      type: 'type',
      device: {
        id: '',
        model: 'samsumg',
        pushToken: '12345',
        os: 'chrome'
      }
    };
    const userDetails: IUserDetails = {
      id: 'string',
      status: 'string',
      relatedParties: [
        {
          id: 'string',
          role: 'string',
          name: 'string'
        }
      ],
      contactMediums: [
        {
          type: 'string',
          role: { name: 'admin' },
          medium: {
            emailAddress: 'string',
            number: 'string'
          },
          preferred: true
        }
      ]
    };
    mock.onGet(url).replyOnce(200, userDetails);
    axios.get(url).then(async () => {
      const result = await fetchUserProfileService();
      expect(result).toEqual(recoveryPayload);
    });
  });
});
