import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@authentication/store/constants';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as recoveryUsingEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { ReactNode } from 'react';
import { IGoogleLoginParams } from '@authentication/store/types';
import { CancelTokenSource } from 'axios';

import { LOGIN_TYPES } from './enum';
import { IError, IParameterRequiredForSocialMedia } from './types';

export default {
  loginWithUsernamePassword: actionCreator<
    usernamePasswordLoginApi.POST.IRequest
  >(CONSTANTS.LOGIN_WITH_USERNAME_PASSWORD),

  // tslint:disable-next-line:no-any
  loginWithUsernamePasswordError: actionCreator<any>(
    CONSTANTS.LOGIN_WITH_USERNAME_PASSWORD_ERROR
  ),
  loginWithUsernamePasswordSuccess: actionCreator<undefined>(
    CONSTANTS.LOGIN_WITH_USERNAME_PASSWORD_SUCCESS
  ),

  requestOtpForEmailLogin: actionCreator<emailOtpLoginApi.POST.IRequest>(
    CONSTANTS.REQUEST_EMAIL_OTP
  ),
  // tslint:disable-next-line:no-any
  requestOtpForEmailLoginError: actionCreator<any>(
    CONSTANTS.REQUEST_EMAIL_OTP_ERROR
  ),
  requestOtpForEmailLoginSuccess: actionCreator<undefined>(
    CONSTANTS.REQUEST_EMAIL_OTP_SUCCESS
  ),

  loginWithEmailOtp: actionCreator<emailOtpLoginApi.PUT.IRequest>(
    CONSTANTS.LOGIN_WITH_EMAIL_OTP
  ),
  // tslint:disable-next-line:no-any
  loginWithEmailOtpError: actionCreator<any>(
    CONSTANTS.LOGIN_WITH_EMAIL_OTP_ERROR
  ),
  loginWithEmailOtpSuccess: actionCreator<undefined>(
    CONSTANTS.LOGIN_WITH_EMAIL_OTP_SUCCESS
  ),

  requestOtpForMsisdnLogin: actionCreator<msisdnOtpLoginApi.POST.IRequest>(
    CONSTANTS.REQUEST_MSISDN_OTP
  ),
  // tslint:disable-next-line:no-any
  requestOtpForMsisdnLoginError: actionCreator<any>(
    CONSTANTS.REQUEST_MSISDN_OTP_ERROR
  ),
  requestOtpForMsisdnLoginSuccess: actionCreator<undefined>(
    CONSTANTS.REQUEST_MSISDN_OTP_SUCCESS
  ),

  loginWithServiceIdOtp: actionCreator<msisdnOtpLoginApi.PUT.IRequest>(
    CONSTANTS.LOGIN_WITH_MSISDN_OTP
  ),
  // tslint:disable-next-line:no-any
  loginWithServiceIdOtpError: actionCreator<any>(
    CONSTANTS.LOGIN_WITH_MSISDN_OTP_ERROR
  ),
  loginWithServiceIdOtpSuccess: actionCreator<undefined>(
    CONSTANTS.LOGIN_WITH_MSISDN_OTP_SUCCESS
  ),

  loginWithSocialMedia: actionCreator<string>(
    CONSTANTS.LOGIN_WITH_SOCIAL_MEDIA
  ),

  loginWithGoogle: actionCreator<IGoogleLoginParams>(
    CONSTANTS.LOGIN_WITH_GOOGLE
  ),

  sendSocialMediaCodeToBackend: actionCreator<IParameterRequiredForSocialMedia>(
    CONSTANTS.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND
  ),
  // tslint:disable-next-line:no-any
  sendSocialMediaCodeToBackendError: actionCreator<any>(
    CONSTANTS.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND_ERROR
  ),
  sendSocialMediaCodeToBackendSuccess: actionCreator<void>(
    CONSTANTS.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND_SUCCESS
  ),

  loginWithSocialMediaError: actionCreator<string>(
    CONSTANTS.LOGIN_WITH_SOCIAL_MEDIA_ERROR
  ),
  loginWithSocialMediaSuccess: actionCreator<string>(
    CONSTANTS.LOGIN_WITH_SOCIAL_MEDIA_SUCCESS
  ),

  recoveryUsingEmailId: actionCreator<recoveryUsingEmailApi.POST.IRequest>(
    CONSTANTS.RECOVER_CREDENTIALS_WITH_EMAIL
  ),
  // tslint:disable-next-line:no-any
  recoveryUsingEmailIdError: actionCreator<any>(
    CONSTANTS.RECOVER_CREDENTIALS_WITH_EMAIL_ERROR
  ),
  recoveryUsingEmailIdSuccess: actionCreator<void>(
    CONSTANTS.RECOVER_CREDENTIALS_WITH_EMAIL_SUCCESS
  ),

  logout: actionCreator<void>(CONSTANTS.LOGOUT),
  logoutError: actionCreator<void>(CONSTANTS.LOGOUT_ERROR),
  logoutSuccess: actionCreator<void>(CONSTANTS.LOGOUT_SUCCESS),

  proceedToNextScreen: actionCreator<void>(CONSTANTS.PROCEED_TO_NEXT_SCREEN),
  setLoginType: actionCreator<LOGIN_TYPES>(CONSTANTS.SET_LOGIN_TYPE),
  goToPreviousScreen: actionCreator<void>(CONSTANTS.GO_TO_PREVIOUS_SCREEN),
  closeLoginModal: actionCreator<void>(CONSTANTS.CLOSE_MODAL),
  setNonceForEmail: actionCreator<string>(CONSTANTS.SET_NONCE_FOR_EMAIL),
  setNonceForMsisdn: actionCreator<string>(CONSTANTS.SET_NONCE_FOR_MSISDN),
  setUsername: actionCreator<string>(CONSTANTS.SET_USERNAME),
  setEmail: actionCreator<string>(CONSTANTS.SET_EMAIL),
  setPhoneNumber: actionCreator<string>(CONSTANTS.SET_PHONE_NUMBER),
  goToRecoveryScreen: actionCreator<void>(CONSTANTS.GO_TO_RECOVERY_SCREEN),
  setError: actionCreator<IError>(CONSTANTS.SET_ERROR),
  loginWithHeaderEnrichment: actionCreator<string>(
    CONSTANTS.LOGIN_WITH_HEADER_ENRICHMENT
  ),
  loginWithHeaderEnrichmentSuccess: actionCreator(
    CONSTANTS.LOGIN_WITH_HEADER_ENRICHMENT_SUCCESS
  ),
  loginWithHeaderEnrichmentError: actionCreator(
    CONSTANTS.LOGIN_WITH_HEADER_ENRICHMENT_ERROR
  ),
  resetAuthenticationState: actionCreator<void>(CONSTANTS.RESET_STATE),
  checkIfIsloggedInUserOrnot: actionCreator<void>(
    CONSTANTS.CHECK_IF_LOCALSTORAGE_HAS_IS_LOGGED_IN
  ),
  setLoadingTextForProgressModal: actionCreator<ReactNode>(
    CONSTANTS.SET_PROGRESS_LOADING_TEXT
  ),
  resetPasswordRecoveryState: actionCreator<void>(
    CONSTANTS.RESET_PASSWORD_RECOVERED_STATE
  ),
  goToHomePage: actionCreator<void>(CONSTANTS.GO_TO_HOMEPAGE),
  setIfUserBankAutheticated: actionCreator<boolean>(
    CONSTANTS.IS_USER_BANK_AUTHENTICATED
  ),
  setLoadingOff: actionCreator(CONSTANTS.SET_LOADING_OFF),
  setLoading: actionCreator(CONSTANTS.SET_LOADING),
  setBankName: actionCreator<string>(CONSTANTS.SET_BANK_NAME),
  routeAfterVerificationCheck: actionCreator(
    CONSTANTS.ROUTE_AFTER_VERIFICATION_CHECK
  ),
  loginViaRegistration: actionCreator<void>(CONSTANTS.LOGIN_VIA_REGISTRATION),
  isEmailUnique: actionCreator<void>(CONSTANTS.IS_EMAIL_UNIQUE),
  setButtonLoading: actionCreator<boolean>(CONSTANTS.SET_BUTTON_LOADING),
  onLoginSuccess: actionCreator<void>(CONSTANTS.ON_LOGIN_SUCCESS),
  saveCancelationTokenForHeaderEnrichmentAPI: actionCreator<CancelTokenSource>(
    CONSTANTS.SAVE_HEADER_ENRICHMENT_CANCELATION_TOKEN
  )
};
