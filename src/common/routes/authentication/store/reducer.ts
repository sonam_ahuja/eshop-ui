import { LOGIN_TYPES } from '@authentication/store/enum';
import CONSTANTS from '@authentication/store/constants';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import { IAuthenticationState, IError } from '@authentication/store/types';
import initialState from '@authentication/store/state';
import history from '@src/client/history';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { ReactNode } from 'react';
import * as recoveryUsingEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { CancelTokenSource } from 'axios';
import { loginSteps } from '@authentication/constants';

const reducers = {
  [CONSTANTS.SET_LOGIN_TYPE]: (
    state: IAuthenticationState,
    payload: LOGIN_TYPES
  ) => {
    state.loginType = payload;
  },
  [CONSTANTS.SET_NONCE_FOR_MSISDN]: (
    state: IAuthenticationState,
    nonce: string
  ) => {
    state.phone.nonce = nonce;
  },
  [CONSTANTS.SET_NONCE_FOR_EMAIL]: (
    state: IAuthenticationState,
    nonce: string
  ) => {
    state.email.nonce = nonce;
  },
  [CONSTANTS.SET_USERNAME]: (state: IAuthenticationState, username: string) => {
    state.username.value = username;
  },
  [CONSTANTS.SET_EMAIL]: (state: IAuthenticationState, email: string) => {
    state.email.value = email;
  },
  [CONSTANTS.SET_PHONE_NUMBER]: (
    state: IAuthenticationState,
    phone: string
  ) => {
    state.phone.value = phone;
  },
  [CONSTANTS.REQUEST_EMAIL_OTP]: (state: IAuthenticationState) => {
    state.loading = true;
  },
  [CONSTANTS.REQUEST_EMAIL_OTP_ERROR]: (state: IAuthenticationState) => {
    state.loading = false;
  },
  [CONSTANTS.REQUEST_EMAIL_OTP_SUCCESS]: (state: IAuthenticationState) => {
    state.loading = false;
  },
  [CONSTANTS.REQUEST_MSISDN_OTP]: (state: IAuthenticationState) => {
    state.loading = true;
  },
  [CONSTANTS.REQUEST_MSISDN_OTP_ERROR]: (state: IAuthenticationState) => {
    state.loading = false;
  },
  [CONSTANTS.REQUEST_MSISDN_OTP_SUCCESS]: (state: IAuthenticationState) => {
    state.loading = false;
  },
  [CONSTANTS.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND]: (
    state: IAuthenticationState
  ) => {
    state.loading = true;
  },
  [CONSTANTS.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND_ERROR]: (
    state: IAuthenticationState
  ) => {
    state.loading = false;
  },
  [CONSTANTS.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND_SUCCESS]: (
    state: IAuthenticationState
  ) => {
    state.loading = false;
  },
  [CONSTANTS.SET_ERROR]: (state: IAuthenticationState, error: IError) => {
    state.error = {
      message: error.message ? error.message : '',
      code: error.code ? error.code : 0
    };
  },
  [CONSTANTS.SET_PROGRESS_LOADING_TEXT]: (
    state: IAuthenticationState,
    loaderText: ReactNode
  ) => {
    state.loadingText = loaderText;
  },
  [CONSTANTS.RESET_STATE]: (state: IAuthenticationState) => {
    Object.assign(state, initialState());
  },
  [CONSTANTS.RECOVER_CREDENTIALS_WITH_EMAIL]: (
    state: IAuthenticationState,
    payload: recoveryUsingEmailApi.POST.IRequest
  ) => {
    state.linkLoading = true;
    if (payload.email) {
      state.recoveryEmail = payload.email;
    }
  },
  [CONSTANTS.RECOVER_CREDENTIALS_WITH_EMAIL_SUCCESS]: (
    state: IAuthenticationState
  ) => {
    state.linkLoading = false;
    if (state.recoveryEmail) {
      state.isUsernameRecovered = true;
    }
    if (history) {
      const parsedQuery = parseQueryString(
        decodeURIComponent(history.location.search)
      );
      if (!parsedQuery.loginStep.includes(loginSteps.ENTER_PASSWORD)) {
        state.isPasswordRecovered = true;
      }
    }
  },
  [CONSTANTS.RESET_PASSWORD_RECOVERED_STATE]: (state: IAuthenticationState) => {
    state.isPasswordRecovered = false;
  },
  [CONSTANTS.RECOVER_CREDENTIALS_WITH_EMAIL_ERROR]: (
    state: IAuthenticationState
  ) => {
    state.linkLoading = false;
  },
  [CONSTANTS.LOGIN_WITH_USERNAME_PASSWORD]: (state: IAuthenticationState) => {
    state.buttonLoading = true;
  },
  [CONSTANTS.LOGIN_WITH_USERNAME_PASSWORD_ERROR]: (
    state: IAuthenticationState
  ) => {
    state.buttonLoading = false;
  },
  [CONSTANTS.LOGIN_WITH_USERNAME_PASSWORD_SUCCESS]: (
    state: IAuthenticationState
  ) => {
    state.buttonLoading = false;
  },
  [CONSTANTS.IS_EMAIL_UNIQUE]: (state: IAuthenticationState) => {
    state.buttonLoading = true;
  },
  [CONSTANTS.IS_USER_BANK_AUTHENTICATED]: (
    state: IAuthenticationState,
    isUserBankAuthenticated: boolean
  ) => {
    state.loading = isUserBankAuthenticated;
    state.isUserBankAuthenticated = isUserBankAuthenticated;
  },
  [CONSTANTS.SET_LOADING]: (state: IAuthenticationState) => {
    state.loading = true;
  },
  [CONSTANTS.SET_LOADING_OFF]: (state: IAuthenticationState) => {
    state.loading = false;
  },
  [CONSTANTS.SET_BANK_NAME]: (
    state: IAuthenticationState,
    bankName: string
  ) => {
    state.bankName = bankName;
  },
  [CONSTANTS.SET_BUTTON_LOADING]: (
    state: IAuthenticationState,
    payload: boolean
  ) => {
    state.buttonLoading = payload;
  },
  [CONSTANTS.LOGIN_WITH_HEADER_ENRICHMENT]: (state: IAuthenticationState) => {
    state.showHeaderEnrichmentModal = true;
  },
  [CONSTANTS.LOGIN_WITH_HEADER_ENRICHMENT_SUCCESS]: (
    state: IAuthenticationState
  ) => {
    state.showHeaderEnrichmentModal = false;
  },
  [CONSTANTS.LOGIN_WITH_HEADER_ENRICHMENT_ERROR]: (
    state: IAuthenticationState
  ) => {
    state.showHeaderEnrichmentModal = false;
  },
  [CONSTANTS.SAVE_HEADER_ENRICHMENT_CANCELATION_TOKEN]: (
    state: IAuthenticationState,
    signal: CancelTokenSource
  ) => {
    state.cancelationTokenForHeaderEnrichment = signal;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IAuthenticationState,
  AnyAction
>;
