import store from '@common/store';
import { logError } from '@src/common/utils';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import actions from '@authentication/store/actions';
import CONSTANTS from '@common/constants/appConstants';
import constants from '@authentication/store/constants';
import { LOGIN_TYPES } from '@authentication/store/enum';
import {
  emailVerificationService,
  loginWithHeaderEnrichmentService,
  recoveryWithEmailService
} from '@authentication/store/services';
import { SagaIterator } from '@authentication/store/types';
import {
  createPayloadForHeaderEnrichmentLogin,
  createPayloadForRequestOtpForEmail,
  createPayloadForRequestOtpForMsisdnLogin
} from '@authentication/store/selector';
import { commonAction } from '@store/actions';
import history from '@src/client/history';
import { fetchIdentityVerificationProvidersService } from '@src/common/store/services/identityVerification';
import routeConstants from '@common/constants/routes';
import { sendRecoverPasswordSuccess } from '@events/login';
import axios from 'axios';
import { loginFlow } from '@authentication/utils';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';
import { loginQueryParam, loginSteps } from '@authentication/constants';
import { getApptState, getCommonState } from '@src/common/store/common';
import basketActions from '@basket/store/actions';
import { ICommonState } from '@common/store/types/common';

// tslint:disable:max-file-line-count
export function* credentailsRecoveryWithEmail({
  payload
}: {
  type: string;
  payload: credentialRecoveryWithEmailApi.POST.IRequest;
}): SagaIterator {
  try {
    if (history) {
      const parsedQuery = parseQueryString(
        decodeURIComponent(history.location.search)
      );

      yield recoveryWithEmailService(payload);
      yield put(actions.recoveryUsingEmailIdSuccess());
      if (
        parsedQuery.loginStep.includes(loginSteps.RECOVER) &&
        payload.email &&
        payload.email.length
      ) {
        sendRecoverPasswordSuccess('user Id');
        yield put(actions.proceedToNextScreen());
      }
    }
  } catch (error) {
    yield put(actions.recoveryUsingEmailIdError(error));
  }
}

export function* loginWithHeaderEnrichment({
  payload
}: {
  type: string;
  payload: string;
}): SagaIterator {
  try {
    const signal = axios.CancelToken.source();
    yield put(actions.saveCancelationTokenForHeaderEnrichmentAPI(signal));
    yield loginWithHeaderEnrichmentService({
      ...createPayloadForHeaderEnrichmentLogin(payload),
      signal
    });
    yield put(commonAction.isUserLoggedIn(true));
    yield put(actions.loginWithHeaderEnrichmentSuccess());
    localStorage.setItem(CONSTANTS.IS_USER_LOGGED_IN, 'true');
    yield put(commonAction.fetchUserDetails());
    if (
      history &&
      history.location.pathname.indexOf(routeConstants.CHECKOUT.BASE) > -1
    ) {
      yield put(commonAction.routeToLoginFromBasket(true));
      yield put(actions.routeAfterVerificationCheck());
    }
  } catch (error) {
    yield put(actions.loginWithHeaderEnrichmentError());
    logError(error);
  }
}

export function* isEmailUnique(): SagaIterator {
  try {
    const state = store.getState();
    const isloginWithEmailConfirmation =
      state.configuration.cms_configuration.global.loginWithEmailConfirmation;
    const email = state.authentication.email.value;
    const isEmailOtpMethodEnabled =
      state.configuration.cms_configuration.global.loginRegistrationMethods
        .emailOTP;
    if (!isEmailOtpMethodEnabled && history) {
      if (isloginWithEmailConfirmation) {
        const response = yield call(
          emailVerificationService,
          {
            email
          },
          true
        );
        if (response.isUnique) {
          history.push({
            pathname: history.location.pathname,
            search: updateQueryStringParameter(
              history.location.search,
              loginQueryParam,
              loginSteps.EMAIL_CONFIRMATION
            )
          });
        } else {
          yield put(actions.setUsername(email));
          yield put(actions.setLoginType(LOGIN_TYPES.USERNAME_PASSWORD));
        }
      } else {
        yield put(actions.setUsername(email));
        yield put(actions.setLoginType(LOGIN_TYPES.USERNAME_PASSWORD));
      }
    } else {
      yield put(
        actions.requestOtpForEmailLogin(createPayloadForRequestOtpForEmail())
      );
    }
  } catch (error) {
    logError(error);
  } finally {
    yield put(actions.setButtonLoading(false));
  }
}

export function* goToNextScreenWhenLoginSet({
  payload
}: {
  type: string;
  payload: LOGIN_TYPES;
}): SagaIterator {
  try {
    switch (payload) {
      case LOGIN_TYPES.EMAIL_OTP:
        yield put(actions.isEmailUnique());
        break;
      case LOGIN_TYPES.MSISDN_OTP:
        yield put(
          actions.requestOtpForMsisdnLogin(
            createPayloadForRequestOtpForMsisdnLogin()
          )
        );
        break;
      default:
        yield put(actions.proceedToNextScreen());
        break;
    }
  } catch (error) {
    logError(error);
    yield put(actions.recoveryUsingEmailIdError(error));
  }
}

export function* closeLoginModal(): SagaIterator {
  try {
    yield put(commonAction.routeToLoginFromBasket(false));
    yield put(commonAction.setIfLoginButtonClicked(false));
    yield put(actions.resetAuthenticationState());
  } catch (error) {
    logError(error);
  }
}

export function* goToHomePage(): SagaIterator {
  try {
    yield put(commonAction.setIfLoginButtonClicked(false));
    yield put(actions.resetAuthenticationState());
    if (history) {
      if (history.location.pathname === routeConstants.HOME) {
        window.location.reload();
      } else {
        history.push(routeConstants.HOME);
      }
    }
  } catch (error) {
    logError(error);
  }
}

export function* checkIfLoggedIn(): SagaIterator {
  try {
    if (localStorage && localStorage.getItem(CONSTANTS.IS_USER_LOGGED_IN)) {
      yield put(commonAction.isUserLoggedIn(true));
    }
  } catch (error) {
    logError(error);
  }
}

export function* routeAfterVerificationCheck(): Generator {
  try {
    const state = getApptState();
    const isIdentityVerificationAllowed =
      state.configuration.cms_configuration.modules.checkout
        .identityVerification.enabled;
    let verificationRequired = false;
    if (history && state.common.routeFromBasketToLogin) {
      if (isIdentityVerificationAllowed) {
        try {
          const verificationProviders = yield fetchIdentityVerificationProvidersService();
          yield put(
            commonAction.setIdentityVerificationDetails(verificationProviders)
          );
          verificationRequired = verificationProviders.verificationRequired;
          if (verificationRequired) {
            history.replace({
              pathname: history.location.pathname,
              search: updateQueryStringParameter(
                history.location.search,
                loginQueryParam,
                loginSteps.IDENTITY_VERIFICATION
              )
            });
          } else {
            yield put(actions.closeLoginModal());
            history.push(routeConstants.CHECKOUT.PERSONAL_INFO);
          }
        } catch (error) {
          logError(error);
        }
      } else {
        yield put(actions.closeLoginModal());
        history.push(routeConstants.CHECKOUT.PERSONAL_INFO);
      }
    } else {
      yield put(actions.closeLoginModal());
    }
  } catch (error) {
    logError(error);
  }
}

export function* onLoginSuccess(): SagaIterator {
  try {
    const commonState: ICommonState = yield select(getCommonState);
    yield put(commonAction.isUserLoggedIn(true));
    yield put(commonAction.fetchUserDetails());
    if (!commonState.routeFromBasketToLogin) {
      yield put(basketActions.fetchBasket({}));
    }
    localStorage.setItem(CONSTANTS.IS_USER_LOGGED_IN, 'true');
    yield put(actions.routeAfterVerificationCheck());
  } catch (error) {
    logError(error);
  }
}

export function* onNextClick(): Generator {
  try {
    const state = store.getState();
    if (history) {
      const parsedQuery = parseQueryString(
        decodeURIComponent(history.location.search)
      );
      const loginQueryParamValue = `${loginFlow[state.authentication.loginType][
        parsedQuery.loginStep
      ].success()}`;
      history.push({
        pathname: history.location.pathname,
        search: updateQueryStringParameter(
          history.location.search,
          loginQueryParam,
          loginQueryParamValue
        )
      });
    }
  } catch (error) {
    logError(error);
  }
}

export function* goBackInHistory(): Generator {
  try {
    if (history) {
      history.goBack();
    }
  } catch (error) {
    logError(error);
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(
    constants.RECOVER_CREDENTIALS_WITH_EMAIL,
    credentailsRecoveryWithEmail
  );
  yield takeLatest(constants.SET_LOGIN_TYPE, goToNextScreenWhenLoginSet);
  yield takeLatest(
    constants.LOGIN_WITH_HEADER_ENRICHMENT,
    loginWithHeaderEnrichment
  );
  yield takeLatest(constants.CLOSE_MODAL, closeLoginModal);
  yield takeLatest(
    constants.CHECK_IF_LOCALSTORAGE_HAS_IS_LOGGED_IN,
    checkIfLoggedIn
  );
  yield takeLatest(
    constants.ROUTE_AFTER_VERIFICATION_CHECK,
    routeAfterVerificationCheck
  );
  yield takeLatest(constants.GO_TO_HOMEPAGE, goToHomePage);
  yield takeLatest(constants.IS_EMAIL_UNIQUE, isEmailUnique);
  yield takeLatest(constants.ON_LOGIN_SUCCESS, onLoginSuccess);
  yield takeLatest(constants.LOGIN_VIA_REGISTRATION, onLoginSuccess);
  yield takeLatest(constants.PROCEED_TO_NEXT_SCREEN, onNextClick);
  yield takeLatest(constants.GO_TO_PREVIOUS_SCREEN, goBackInHistory);
}
export default watcherSaga;
