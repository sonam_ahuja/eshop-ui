import constants from '@authentication/store/constants';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import { call, put, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import actions from '@authentication/store/actions';
import {
  loginWithEmailOtpService,
  OtpRequestForEmailService
} from '@authentication/store/services';
import CONSTANTS from '@common/constants/appConstants';
import store from '@common/store';
import { SagaIterator } from '@authentication/store/types';
import { sendLoginFailedEvent } from '@events/login';
import history from '@src/client/history';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { setLoginType } from '@events/common';

export function* requestOtpForEmailId({
  payload
}: {
  type: string;
  payload: emailOtpLoginApi.POST.IRequest;
}): SagaIterator {
  try {
    const { nonce } = yield call(OtpRequestForEmailService, payload, true);
    yield put(actions.setNonceForEmail(nonce));
    yield put(actions.setEmail(payload.serviceId));
    // next is is checked for resend
    if (history) {
      const parsedQuery = parseQueryString(
        decodeURIComponent(history.location.search)
      );
      if (!parsedQuery.loginStep.includes('enterOtp')) {
        yield put(actions.proceedToNextScreen());
      }
    }
  } catch (error) {
    const apiLatencyTime = store.getState().configuration.cms_configuration
      .global.authentication.apiLatency;
    yield call(delay, apiLatencyTime);
    yield put(actions.requestOtpForEmailLoginError(error));
  } finally {
    const apiLatencyTime = store.getState().configuration.cms_configuration
      .global.authentication.apiLatency;
    yield call(delay, apiLatencyTime);
    yield put(actions.requestOtpForEmailLoginSuccess());
  }
}

export function* loginWithEmailOtp({
  payload
}: {
  type: string;
  payload: emailOtpLoginApi.PUT.IRequest;
}): Generator {
  try {
    yield loginWithEmailOtpService(payload, true);
    localStorage.setItem(CONSTANTS.EMAIL, payload.serviceId);
    yield put(actions.onLoginSuccess());
    setLoginType(payload.serviceType);
  } catch (error) {
    sendLoginFailedEvent(payload.serviceType);
    yield put(actions.loginWithEmailOtpError(error));
  } finally {
    yield put(actions.loginWithEmailOtpSuccess());
  }
}
function* watcherSaga(): Generator {
  yield takeLatest(constants.REQUEST_EMAIL_OTP, requestOtpForEmailId);
  yield takeLatest(constants.LOGIN_WITH_EMAIL_OTP, loginWithEmailOtp);
}
export default watcherSaga;
