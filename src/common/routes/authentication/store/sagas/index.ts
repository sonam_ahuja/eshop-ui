import msisdnOtpLoginSagas from '@authentication/store/sagas/msisdnOtpLoginSagas';
import emailOtpLoginSagas from '@authentication/store/sagas/emailOtpLoginSagas';
import usernamePasswordLoginSagas from '@authentication/store/sagas/usernamePasswordLoginSagas';
import socialLoginSagas from '@authentication/store/sagas/socialLoginSagas';
import commonSagas from '@authentication/store/sagas/common';
import logoutSagas from '@authentication/store/sagas/logout';

const sagas = [
  usernamePasswordLoginSagas(),
  emailOtpLoginSagas(),
  msisdnOtpLoginSagas(),
  commonSagas(),
  socialLoginSagas(),
  logoutSagas()
];
export default sagas;
