import { logError } from '@src/common/utils';
import history from '@src/client/history';
import { put, takeLatest } from 'redux-saga/effects';
import actions from '@authentication/store/actions';
import CONSTANTS from '@common/constants/appConstants';
import constants from '@authentication/store/constants';
import { logoutService } from '@authentication/store/services';
import { commonAction } from '@store/actions';
import basketActions from '@basket/store/actions';
import { sendLogOutEvent } from '@events/login';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';
import { loginQueryParam, loginSteps } from '@authentication/constants';
import routeConstants from '@common/constants/routes';
import { setUniqueIdentifier } from '@src/common/utils/uniqueIdentifier';

export function* logout(): Generator {
  try {
    yield logoutService();
    yield put(actions.resetAuthenticationState());
    yield put(actions.logoutSuccess());
  } catch (error) {
    logError(error);
  }
}

export function* onlogoutSuccess(): Generator {
  try {
    yield put(commonAction.isUserLoggedIn(false));
    yield put(basketActions.fetchBasket({}));
    localStorage.removeItem(CONSTANTS.IS_USER_LOGGED_IN);
    localStorage.removeItem(CONSTANTS.EMAIL);
    localStorage.removeItem(CONSTANTS.USER_ID);
    localStorage.removeItem(CONSTANTS.PHONE_NUMBER);
    setUniqueIdentifier();
    sendLogOutEvent();
    if (history) {
      history.push({
        pathname: history.location.pathname.includes(
          routeConstants.CHECKOUT.BASE
        )
          ? routeConstants.BASKET
          : history.location.pathname,
        search: updateQueryStringParameter(
          history.location.search,
          loginQueryParam,
          loginSteps.LOGOUT
        )
      });
    }
  } catch (error) {
    logError(error);
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(constants.LOGOUT, logout);
  yield takeLatest(constants.LOGOUT_SUCCESS, onlogoutSuccess);
}
export default watcherSaga;
