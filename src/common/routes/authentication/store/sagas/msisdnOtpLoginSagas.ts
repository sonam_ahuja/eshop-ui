import CONSTANTS from '@common/constants/appConstants';
import {
  loginWithMsisdnOtpService,
  OtpForMsisdnService
} from '@authentication/store/services';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import { call, put, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import actions from '@authentication/store/actions';
import constants from '@authentication/store/constants';
import store from '@common/store';
import { SagaIterator } from '@authentication/store/types';
import { sendLoginFailedEvent } from '@events/login';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import history from '@src/client/history';
import { setLoginType } from '@src/common/events/common';

export function* requestOtpForMsisdnId({
  payload
}: {
  type: string;
  payload: msisdnOtpLoginApi.POST.IRequest;
}): SagaIterator {
  try {
    const { nonce } = yield call(OtpForMsisdnService, payload, true);
    yield put(actions.setNonceForMsisdn(nonce));
    yield put(actions.setPhoneNumber(payload.serviceId));
    // next is checked for resend
    if (history) {
      const parsedQuery = parseQueryString(
        decodeURIComponent(history.location.search)
      );
      if (!parsedQuery.loginStep.includes('enterOtp')) {
        yield put(actions.proceedToNextScreen());
      }
    }
  } catch (error) {
    const apiLatencyTime = store.getState().configuration.cms_configuration
      .global.authentication.apiLatency;
    yield call(delay, apiLatencyTime);
    yield put(actions.requestOtpForMsisdnLoginError(error));
  } finally {
    const apiLatencyTime = store.getState().configuration.cms_configuration
      .global.authentication.apiLatency;
    yield call(delay, apiLatencyTime);
    yield put(actions.requestOtpForMsisdnLoginSuccess());
  }
}

export function* loginWithMsisdnOtp({
  payload
}: {
  type: string;
  payload: msisdnOtpLoginApi.PUT.IRequest;
}): Generator {
  try {
    yield loginWithMsisdnOtpService(payload, true);
    localStorage.setItem(CONSTANTS.PHONE_NUMBER, payload.serviceId);
    setLoginType(payload.serviceType);
    yield put(actions.onLoginSuccess());
  } catch (error) {
    sendLoginFailedEvent(payload.serviceType);

    yield put(actions.loginWithServiceIdOtpError(error));
  } finally {
    yield put(actions.loginWithServiceIdOtpSuccess());
  }
}
function* watcherSaga(): Generator {
  yield takeLatest(constants.REQUEST_MSISDN_OTP, requestOtpForMsisdnId);
  yield takeLatest(constants.LOGIN_WITH_MSISDN_OTP, loginWithMsisdnOtp);
}
export default watcherSaga;
