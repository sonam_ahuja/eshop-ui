import {
  googleLoginService,
  socialLoginRequestService,
  socialLoginService
} from '@authentication/store/services';
import { put, takeLatest } from 'redux-saga/effects';
import actions from '@authentication/store/actions';
import constants from '@authentication/store/constants';
import * as googleLoginApi from '@common/types/api/authentication/googleLogin.ts';
import {
  IParameterRequiredForSocialMedia,
  SagaIterator
} from '@authentication/store/types';
import { sendLoginFailedEvent } from '@events/login';
import { setLoginType } from '@src/common/events/common';

import { LOGIN_TYPES } from '../enum';

export function* loginWithSocialMedia({
  payload
}: {
  type: string;
  payload: string;
}): SagaIterator {
  try {
    yield socialLoginRequestService(payload);
  } catch (error) {
    yield put(actions.recoveryUsingEmailIdError(error));
  } finally {
    yield put(actions.recoveryUsingEmailIdSuccess());
  }
}

export function* loginWithGoogle({
  payload
}: {
  type: string;
  payload: googleLoginApi.POST.IRequest;
}): SagaIterator {
  try {
    yield googleLoginService(payload);
    yield put(actions.onLoginSuccess());
  } catch (error) {
    yield put(actions.recoveryUsingEmailIdError(error));
  } finally {
    yield put(actions.recoveryUsingEmailIdSuccess());
  }
}

export function* sendSocialMediaCodeToBackend({
  payload
}: {
  type: string;
  payload: IParameterRequiredForSocialMedia;
}): Generator {
  try {
    yield socialLoginService(payload);
    setLoginType(LOGIN_TYPES.SOCIAL_LOGIN);
    yield put(actions.onLoginSuccess());
  } catch (error) {
    sendLoginFailedEvent(LOGIN_TYPES.SOCIAL_LOGIN);
    yield put(actions.sendSocialMediaCodeToBackendError(error));
  } finally {
    yield put(actions.sendSocialMediaCodeToBackendSuccess());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(constants.LOGIN_WITH_SOCIAL_MEDIA, loginWithSocialMedia);
  yield takeLatest(constants.LOGIN_WITH_GOOGLE, loginWithGoogle);
  yield takeLatest(
    constants.SEND_SOCIAL_MEDIA_CODE_TO_BACKEND,
    sendSocialMediaCodeToBackend
  );
}
export default watcherSaga;
