import CONSTANTS from '@common/constants/appConstants';
import { loginWithUsernamePasswordService } from '@authentication/store/services';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import { put, takeLatest } from 'redux-saga/effects';
import actions from '@authentication/store/actions';
import constants from '@authentication/store/constants';
import { sendLoginFailedEvent } from '@events/login';
import { setLoginType } from '@src/common/events/common';

export function* loginWithUsernamePassword({
  payload
}: {
  type: string;
  payload: usernamePasswordLoginApi.POST.IRequest;
}): Generator {
  try {
    yield loginWithUsernamePasswordService(payload, true); // second params used for skipGenericError
    localStorage.setItem(CONSTANTS.USERNAME, payload.telekomLogin.username);
    setLoginType(payload.type);
    yield put(actions.onLoginSuccess());
  } catch (error) {
    sendLoginFailedEvent(payload.type);
    yield put(actions.loginWithUsernamePasswordError(error));
  } finally {
    yield put(actions.loginWithUsernamePasswordSuccess());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(
    constants.LOGIN_WITH_USERNAME_PASSWORD,
    loginWithUsernamePassword
  );
}
export default watcherSaga;
