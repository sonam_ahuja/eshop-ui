import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import * as headerEnrichmentLoginApi from '@common/types/api/authentication/headerEnrichmentLogin';
import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import CONSTANTS from '@common/constants/appConstants';
import { LOGIN_SERVICE_TYPE, LOGIN_TYPES } from '@authentication/store/enum';
import { encrypt } from '@authentication/utils/encryption';
import store from '@common/store';
import { isBrowser } from '@src/common/utils';

export const device = {
  browser: isBrowser ? window.navigator.userAgent : ''
};

export const createPayloadForRequestOtpForEmail = (): emailOtpLoginApi.POST.IRequest => {
  return {
    context: CONSTANTS.LOGIN,
    serviceType: LOGIN_SERVICE_TYPE.EMAIL,
    serviceId: store.getState().authentication.email.value,
    nonce: '',
    PIN: '',
    device
  };
};

export const createPayloadForRequestOtpForMsisdnLogin = (): msisdnOtpLoginApi.POST.IRequest => {
  return {
    context: CONSTANTS.LOGIN,
    serviceType: LOGIN_SERVICE_TYPE.PHONE_NUMBER,
    serviceId: store.getState().authentication.phone.value,
    PIN: '',
    nonce: '',
    device
  };
};

export const createPayloadForUsernamePasswordLogin = (
  password: string
): Promise<usernamePasswordLoginApi.POST.IRequest> => {
  return encrypt(password).then((encryptedPassword: string) => {
    return {
      type: LOGIN_TYPES.TELEKOM_LOGIN,
      telekomLogin: {
        username: store.getState().authentication.username.value,
        password: encryptedPassword
      },
      device
    };
  });
};

export const createPayloadForHeaderEnrichmentLogin = (
  serviceId: string
): headerEnrichmentLoginApi.POST.IRequest => {
  return {
    type: LOGIN_TYPES.HEADER_ENRICHMENT,
    serviceId,
    device
  };
};

export const createPayloadForCredentialRecoveryWithEmail = (
  email: string
): credentialRecoveryWithEmailApi.POST.IRequest => {
  return {
    email
  };
};
