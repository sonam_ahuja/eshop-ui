import * as credentialRecoveryWithEmailApi from '@common/types/api/authentication/credentailsRecoveryUsingEmail';
import apiCaller from '@common/utils/apiCaller';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import { apiEndpoints } from '@common/constants';
import * as headerEnrichmentLoginApi from '@common/types/api/authentication/headerEnrichmentLogin';
import * as emailVerificationApi from '@common/types/api/authentication/emailVerification';
import * as userRegistrationUsingEmailApi from '@common/types/api/authentication/userRegistrationUsingEmail';
import * as googleLoginApi from '@common/types/api/authentication/googleLogin.ts';
import * as userDetailsApi from '@common/types/api/userDetails';
import { logError } from '@src/common/utils';

import { IParameterRequiredForSocialMedia } from './types';

export const fetchUserProfileService = async (): Promise<
  userDetailsApi.GET.IResponse | Error
> => {
  const { url } = apiEndpoints.AUTHENTICATION.PROFILE;
  try {
    return await apiCaller.get(url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const loginWithUsernamePasswordService = async (
  payload: usernamePasswordLoginApi.POST.IRequest,
  skipGenericError: boolean = false
): Promise<usernamePasswordLoginApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.USERNAME_PASSWORD_LOGIN;
  try {
    return await apiCaller.post(url, payload, { skipGenericError });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const OtpRequestForEmailService = async (
  payload: emailOtpLoginApi.POST.IRequest,
  skipGenericError: boolean = false
): Promise<void | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.EMAIL_OTP_REQUEST;
  try {
    return await apiCaller.post(url, payload, { skipGenericError });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const loginWithEmailOtpService = async (
  payload: emailOtpLoginApi.PUT.IRequest,
  skipGenericError: boolean = false
): Promise<emailOtpLoginApi.PUT.IResponse | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.EMAIL_OTP_LOGIN;
  try {
    return await apiCaller.put(url, payload, { skipGenericError });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const OtpForMsisdnService = async (
  payload: msisdnOtpLoginApi.POST.IRequest,
  skipGenericError: boolean = false
): Promise<void | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.MSISDN_OTP_REQUEST;
  try {
    return await apiCaller.post(url, payload, { skipGenericError });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const loginWithMsisdnOtpService = async (
  payload: msisdnOtpLoginApi.PUT.IRequest,
  skipGenericError: boolean = false
): Promise<msisdnOtpLoginApi.PUT.IResponse | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.MSISDN_OTP_REQUEST;
  try {
    return await apiCaller.put(url, payload, { skipGenericError });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const socialLoginRequestService = async (
  callbackUrl: string
): Promise<void | Error> => {
  const url = apiEndpoints.AUTHENTICATION.SOCIAL_REQUEST.url(callbackUrl);
  try {
    return await apiCaller
      .get(url)
      .then(response => logError(response))
      .catch(error => {
        if (error.status === 301 && error.data) {
          window.location.href = error.data;
        }
      });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const googleLoginService = async (
  payload: googleLoginApi.POST.IRequest
): Promise<void | Error> => {
  const url = apiEndpoints.AUTHENTICATION.GOOGLE_LOGIN.url(
    payload.redirectUrl,
    payload.googleCode
  );
  try {
    return await apiCaller.get(url);
  } catch (e) {
    logError(e);

    throw e;
  }
};

export const socialLoginService = async (
  payload: IParameterRequiredForSocialMedia
): Promise<void | Error> => {
  const url = apiEndpoints.AUTHENTICATION.SOCIAL_LOGIN.url(
    payload.callbackUrl,
    payload.facebookCode
  );
  try {
    return await apiCaller.post(url);
  } catch (e) {
    logError(e);

    throw e;
  }
};

export const loginWithHeaderEnrichmentService = async (
  payload: headerEnrichmentLoginApi.POST.IRequest
): Promise<headerEnrichmentLoginApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.HEADER_ENRICHMENT;
  try {
    return await apiCaller.post(url, payload, {
      cancelToken: payload.signal && payload.signal.token
    });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const logoutService = async (): Promise<void | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.logout;
  try {
    return await apiCaller.delete(url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const recoveryWithEmailService = async (
  payload: credentialRecoveryWithEmailApi.POST.IRequest
): Promise<credentialRecoveryWithEmailApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.RECOVERY;
  try {
    return await apiCaller.post(url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const emailVerificationService = async (
  payload: emailVerificationApi.GET.IRequest,
  skipGenericError: boolean = false
): Promise<emailVerificationApi.GET.IResponse | Error> => {
  const url = apiEndpoints.AUTHENTICATION.EMAIL_VERIFICATION.url(payload.email);
  try {
    return await apiCaller.get(url, { skipGenericError });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const userRegistrationService = async (
  payload: userRegistrationUsingEmailApi.POST.IRequest,
  skipGenericError: boolean = false
): Promise<userRegistrationUsingEmailApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.AUTHENTICATION.USER_REGISTERATION;
  try {
    return await apiCaller.post(url, payload, { skipGenericError });
  } catch (e) {
    logError(e);
  }
};
