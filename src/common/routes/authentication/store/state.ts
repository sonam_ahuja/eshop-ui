import { IAuthenticationState } from '@authentication/store/types';
import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';

export default (): IAuthenticationState => ({
  loginType: LOGIN_TYPES.USERNAME_PASSWORD,
  activeStep: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
  previousStep: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
  linkLoading: false,
  buttonLoading: false,
  loading: false,
  loadingText: '',
  showModal: true,
  showHeaderEnrichmentModal: false,
  cancelationTokenForHeaderEnrichment: null,
  showEnterCredentialsScreen: true,
  showOtpScreen: false,
  showPasswordScreen: false,
  showRecoveryScreen: false,
  isLoggedIn: false,
  isPasswordRecovered: false,
  recoveryEmail: '',
  isUsernameRecovered: false,
  isUserBankAuthenticated: false,
  bankName: '',
  error: {
    code: 0,
    message: ''
  },
  username: {
    value: '',
    isValid: true // always valid
  },
  email: {
    value: '',
    isValid: false, // this validity is whether email exist in system or not
    nonce: ''
  },
  phone: {
    value: '',
    isValid: false,
    nonce: ''
  },
  otp: {
    value: '',
    isValid: false
  },
  password: {
    value: '',
    isValid: false
  }
});
