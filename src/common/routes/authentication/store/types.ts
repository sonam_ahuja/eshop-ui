import { LOGIN_STEPS, LOGIN_TYPES } from '@authentication/store/enum';
import { Effect } from 'redux-saga';
import * as msisdnOtpLoginApi from '@common/types/api/authentication/msisdnOtpLogin';
import * as emailOtpLoginApi from '@common/types/api/authentication/emailOtpLogin';
import * as usernamePasswordLoginApi from '@common/types/api/authentication/usernamePasswordLogin';
import { ReactNode } from 'react';
import { CancelTokenSource } from 'axios';

// saga Iterator
export type SagaIterator = IterableIterator<
  | Effect
  | Effect[]
  | Promise<
      // tslint:disable-next-line:max-union-size
      | usernamePasswordLoginApi.POST.IResponse
      | emailOtpLoginApi.POST.IResponse
      | emailOtpLoginApi.PUT.IResponse
      | msisdnOtpLoginApi.POST.IResponse
      | msisdnOtpLoginApi.PUT.IResponse
      | Error
    >
>;

export interface IError {
  code?: number;
  message?: string;
}

export type IEmail = IPhone;

export interface IPhone extends IUsername {
  nonce?: string;
}

export interface IUsername {
  value: string;
  isValid?: boolean;
}

export interface IPassword {
  value?: string;
  isValid?: boolean;
}

export type IOtp = IPassword;

export interface IParameterRequiredForSocialMedia {
  callbackUrl: string;
  facebookCode: string;
}
export interface IAuthenticationState {
  linkLoading: boolean;
  buttonLoading: boolean;
  loading: boolean;
  loadingText: ReactNode;
  error: IError;
  username: IUsername;
  email: IEmail;
  phone: IPhone;
  password: IPassword;
  otp: IOtp;
  activeStep: LOGIN_STEPS;
  showModal: boolean;
  showHeaderEnrichmentModal: boolean;
  cancelationTokenForHeaderEnrichment: CancelTokenSource | null;
  previousStep: LOGIN_STEPS;
  showOtpScreen: boolean;
  showPasswordScreen: boolean;
  showRecoveryScreen: boolean;
  isLoggedIn: boolean;
  showEnterCredentialsScreen: boolean;
  loginType: LOGIN_TYPES;
  isPasswordRecovered: boolean;
  recoveryEmail: string;
  isUsernameRecovered: boolean;
  isUserBankAuthenticated: boolean;
  bankName: string;
}

// API types

interface IDevice {
  browser: string;
}

interface ITelekomLogin {
  username: string;
  password: string;
}

export interface ILoginResponse {
  accessToken: string;
  accessExpiresIn: number;
  idToken: string;
  refreshToken: string;
  refreshExpiresIn: number;
}

export interface ILoginUsernameRequest {
  type: string;
  telekomLogin: ITelekomLogin;
  device: IDevice;
}

export interface ILoginWithHeaderEnrichmentRequest {
  device: IDevice;
  serviceId: string;
  type: string;
  signal?: CancelTokenSource;
}
export interface ILoginOtpRequest {
  PIN: string;
  context: string;
  device: IDevice;
  nonce: string;
  serviceId: string;
  serviceType: string;
}

export interface IRecoveryWithEmailRequest {
  email?: string;
  telekomId?: string;
}
export interface IProfileCheckRequest {
  email: string;
}

export type IEmailVerificationRequest = IProfileCheckRequest;
export type IUserRegistrationRequest = IProfileCheckRequest;

export interface IProfileCheckResponse {
  isVerified: boolean;
}

export interface IEmailVerificationResponse {
  isUnique: boolean;
}

export interface IGoogleLoginParams {
  redirectUrl: string;
  googleCode: string;
}
