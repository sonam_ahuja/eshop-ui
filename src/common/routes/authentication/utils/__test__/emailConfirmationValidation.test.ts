import { emailConfirmationValidation } from '@authentication/utils/emailConfirmationValidation';
import store from '@common/store';

describe('EmailConfirmationValidation', () => {
  const configuration = store.getState().configuration.cms_configuration.global
    .loginFormRules;
  const translation = store.getState().translation.cart.login;
  const email = 'Avinash@gmail.com';
  it('emailConfirmationValidation with email ::', () => {
    const result = emailConfirmationValidation(
      email,
      '',
      store.getState().translation.cart.login,
      store.getState().configuration.cms_configuration.global.loginFormRules
    );
    expect(
      emailConfirmationValidation(email, '', translation, configuration)
    ).toMatchObject(result);
  });

  it('emailConfirmationValidation with invalid email ::', () => {
    const result = emailConfirmationValidation(
      'demo',
      '',
      store.getState().translation.cart.login,
      store.getState().configuration.cms_configuration.global.loginFormRules
    );
    expect(
      emailConfirmationValidation('demo', '', translation, configuration)
    ).toMatchObject(result);
  });

  it('emailConfirmationValidation with confirm email ::', () => {
    const result = emailConfirmationValidation(
      email,
      'email',
      store.getState().translation.cart.login,
      store.getState().configuration.cms_configuration.global.loginFormRules
    );
    expect(
      emailConfirmationValidation(
        email,
        'email',
        store.getState().translation.cart.login,
        store.getState().configuration.cms_configuration.global.loginFormRules
      )
    ).toMatchObject(result);
  });

  it('emailConfirmationValidation with confirm email ::', () => {
    const result = emailConfirmationValidation(
      email,
      email,
      store.getState().translation.cart.login,
      store.getState().configuration.cms_configuration.global.loginFormRules
    );
    expect(
      emailConfirmationValidation(
        email,
        email,
        store.getState().translation.cart.login,
        store.getState().configuration.cms_configuration.global.loginFormRules
      )
    ).toMatchObject(result);
  });
});
