import {
  encrypt,
  setPublickKeyForEncryption
} from '@authentication/utils/encryption';

describe('Encryption should work', () => {
  it('should call setPublickKeyForEncryption', () => {
    setPublickKeyForEncryption();
  });
  it('should call encrypt', () => {
    encrypt('encrypt');
  });
});
