import { loginFlow, replacedString } from '@authentication/utils';
import { LOGIN_TYPES } from '@authentication/store/enum';

import { loginSteps } from '../../constants';

describe('Utils', () => {
  it('LOGIN_TYPES.MSISDN_OTP navigation flow on ENTER_LOGIN_CREDENTIAL', () => {
    expect(
      loginFlow[LOGIN_TYPES.MSISDN_OTP][
        loginSteps.ENTER_LOGIN_CREDENTIAL
      ].success()
    ).toBeDefined();
  });

  it('LOGIN_TYPES.MSISDN_OTP navigation flow on ENTER_OTP', () => {
    expect(
      loginFlow[LOGIN_TYPES.MSISDN_OTP][loginSteps.ENTER_OTP].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.MSISDN_OTP navigation flow on RECOVER', () => {
    expect(
      loginFlow[LOGIN_TYPES.MSISDN_OTP][loginSteps.RECOVER].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.MSISDN_OTP navigation flow on RECOVER_SUCCESS', () => {
    expect(
      loginFlow[LOGIN_TYPES.MSISDN_OTP][loginSteps.RECOVER_SUCCESS].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_OTP navigation flow on ENTER_LOGIN_CREDENTIAL', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_OTP][
        loginSteps.ENTER_LOGIN_CREDENTIAL
      ].success(LOGIN_TYPES.EMAIL_OTP)
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_OTP navigation flow on ENTER_OTP', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_OTP][loginSteps.ENTER_OTP].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_OTP navigation flow on RECOVER', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_OTP][loginSteps.RECOVER].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_OTP navigation flow on RECOVER_SUCCESS', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_OTP][loginSteps.RECOVER_SUCCESS].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.USERNAME_PASSWORD navigation flow on ENTER_LOGIN_CREDENTIAL', () => {
    expect(
      loginFlow[LOGIN_TYPES.USERNAME_PASSWORD][
        loginSteps.ENTER_LOGIN_CREDENTIAL
      ].success(LOGIN_TYPES.EMAIL_OTP)
    ).toBeDefined();
  });
  it('LOGIN_TYPES.USERNAME_PASSWORD navigation flow on ENTER_PASSWORD', () => {
    expect(
      loginFlow[LOGIN_TYPES.USERNAME_PASSWORD][
        loginSteps.ENTER_PASSWORD
      ].success(LOGIN_TYPES.EMAIL_OTP)
    ).toBeDefined();
  });

  it('LOGIN_TYPES.USERNAME_PASSWORD navigation flow on RECOVER', () => {
    expect(
      loginFlow[LOGIN_TYPES.USERNAME_PASSWORD][loginSteps.RECOVER].success(
        LOGIN_TYPES.EMAIL_OTP
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.USERNAME_PASSWORD navigation flow on RECOVER_SUCCESS', () => {
    expect(
      loginFlow[LOGIN_TYPES.USERNAME_PASSWORD][
        loginSteps.RECOVER_SUCCESS
      ].success(LOGIN_TYPES.EMAIL_OTP)
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_CONFIRMATION navigation flow on ENTER_LOGIN_CREDENTIAL', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_CONFIRMATION][
        loginSteps.ENTER_LOGIN_CREDENTIAL
      ].success(LOGIN_TYPES.EMAIL_CONFIRMATION)
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_CONFIRMATION navigation flow on EMAIL_CONFIRMATION', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_CONFIRMATION][
        loginSteps.EMAIL_CONFIRMATION
      ].success(LOGIN_TYPES.EMAIL_CONFIRMATION)
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_CONFIRMATION navigation flow on RECOVER', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_CONFIRMATION][loginSteps.RECOVER].success(
        LOGIN_TYPES.EMAIL_CONFIRMATION
      )
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_CONFIRMATION navigation flow on RECOVER_SUCCESS', () => {
    expect(
      loginFlow[LOGIN_TYPES.EMAIL_CONFIRMATION][
        loginSteps.RECOVER_SUCCESS
      ].success(LOGIN_TYPES.EMAIL_CONFIRMATION)
    ).toBeDefined();
  });

  it('LOGIN_TYPES.EMAIL_CONFIRMATION navigation flow on IDENTITY_VERIFICATION', () => {
    expect(
      loginFlow[LOGIN_TYPES.USERNAME_PASSWORD][
        loginSteps.IDENTITY_VERIFICATION
      ].success()
    ).toBeDefined();
  });

  it('replacedString', () => {
    expect(replacedString('newString', 'nextString', 'config')).toBeDefined();
  });
});
