import { IError } from '@src/common/store/types/common';

export default function clearGenricError(
  error: IError,
  clearError: () => void
): void {
  if (error.httpStatusCode) {
    clearError();
  }
}
