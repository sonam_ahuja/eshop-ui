import { VALIDATIONS } from '@common/constants/appConstants';
import { checkEmailValidation } from '@authentication/common/LoginInputForm/validation';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { ILoginTranslation } from '@src/common/store/types/translation';
import { ILoginFormRules } from '@src/common/store/types/configuration';
import { IState as IEmailConfirmationValidation } from '@authentication/LoginWithUser/EmailConfirmation/types';

const getDefaultState = (): IEmailConfirmationValidation => ({
  isNextButtonDisable: false,
  emailValue: '',
  confirmEmailValue: '',
  emailMessage: '',
  confirmEmailMessage: '',
  isEmailError: false,
  isConfirmEmailError: false
});

export const emailConfirmationValidation = (
  email: string,
  confirmEamil: string,
  translation: ILoginTranslation,
  configuration: ILoginFormRules
): IEmailConfirmationValidation => {
  const inputFieldValueIsEmail = VALIDATIONS.email.test(email);
  const confirmInputFieldValueIsEmail = VALIDATIONS.email.test(confirmEamil);
  const {
    errorMessageForEmailInput,
    errorMessageForConfirmInput
  } = translation.loginEmailConfirmation;
  let newState = getDefaultState();

  if (
    !inputFieldValueIsEmail &&
    !checkEmailValidation(
      configuration.email.regex as string,
      email,
      VALIDATION_TYPE.REGEX
    )
  ) {
    newState = {
      ...newState,
      ...{
        emailValue: email,
        confirmEmailValue: confirmEamil,
        emailMessage: errorMessageForEmailInput,
        isEmailError: true,
        isNextButtonDisable: true
      }
    };

    return newState;
  }

  if (
    confirmEamil.length &&
    !confirmInputFieldValueIsEmail &&
    !checkEmailValidation(
      configuration.email.regex as string,
      confirmEamil,
      VALIDATION_TYPE.REGEX
    )
  ) {
    newState = {
      ...newState,
      ...{
        emailValue: email,
        confirmEmailValue: confirmEamil,
        confirmEmailMessage: errorMessageForConfirmInput,
        isNextButtonDisable: true,
        emailMessage: '',
        isEmailError: false,
        isConfirmEmailError: true
      }
    };

    return newState;
  }

  if (email === confirmEamil && (email.length && confirmEamil.length)) {
    newState = {
      ...newState,
      ...{
        emailValue: email,
        confirmEmailValue: confirmEamil,
        isNextButtonDisable: false,
        confirmEmailMessage: '',
        emailMessage: '',
        isEmailError: false,
        isConfirmEmailError: false
      }
    };

    return newState;
  } else {
    newState = {
      ...newState,
      ...{
        emailValue: email,
        confirmEmailValue: confirmEamil,
        isNextButtonDisable: true,
        confirmEmailMessage: '',
        emailMessage: '',
        isEmailError: false,
        isConfirmEmailError: false
      }
    };

    return newState;
  }
};
