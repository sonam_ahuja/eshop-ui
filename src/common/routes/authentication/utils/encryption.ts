import { isBrowser } from '@src/common/utils';
import CONSTANTS from '@common/constants/appConstants';

export const encrypt = (palinText: string) => {
  if (isBrowser) {
    return import('jsencrypt').then(module => {
      const jsencrypt = module.default;

      return jsencrypt.prototype.encrypt(palinText, true);
    });
  }

  return Promise.resolve();
};

export const setPublickKeyForEncryption = () => {
  if (isBrowser) {
    return import('jsencrypt').then(module => {
      const jsencrypt = module.default;

      return jsencrypt.prototype.setPublicKey(CONSTANTS.PUBLIC_KEY);
    });
  }

  return Promise.resolve();
};
