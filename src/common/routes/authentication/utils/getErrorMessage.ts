import { IError as IGericError } from '@common/store/types/common';
import { IError } from '@authentication/store/types';

export default function getErrorMessage(
  error: IError,
  commonError: IGericError
): string {
  if (error.message) {
    return error.message;
  } else if (commonError.httpStatusCode) {
    return (commonError.message && commonError.message.userMessage) || ' ';
  }

  return '';
}
