import { IWindow } from '@src/client/index';
import { logError } from '@src/common/utils';
import { getConfiguration } from '@src/common/store/common';

export const initGoogleConnect = (): void => {
  try {
    const configuration = getConfiguration();
    const googleConfiguration =
      configuration.cms_configuration.modules.login.social.google;
    if (
      configuration.cms_configuration.global.loginRegistrationMethods
        .googleAccount
    ) {
      const scriptElement = document.createElement('script');
      scriptElement.type = 'text/javascript';
      scriptElement.async = true;
      scriptElement.onload = () => {
        (window as IWindow).gapi.load('auth2', () => {
          if (!(window as IWindow).gapi.auth2.getAuthInstance()) {
            (window as IWindow).gapi.auth2.init({
              client_id: googleConfiguration.client.id,
              scope: googleConfiguration.client.scope,
              cookie_policy: googleConfiguration.client.cookiePolicy
            });
          }
        });
      };
      scriptElement.src = googleConfiguration.client.script;
      const t = document.getElementsByTagName('script')[0];
      if (t && t.parentNode) {
        t.parentNode.insertBefore(scriptElement, t);
      }
      logError(`+++++Success IN GOOGLE SCRIPT LOADING+++++`);
    }
  } catch (error) {
    logError(`+++++ERROR IN GOOGLE SCRIPT LOADING+++++ ${error}`);
  }
};
