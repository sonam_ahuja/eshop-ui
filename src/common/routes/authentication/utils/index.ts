import { LOGIN_TYPES, POINTERS } from '@authentication/store/enum';
import { loginSteps as LOGIN_STEPS } from '@authentication/constants';

const LANDING_PAGE = '';

export const loginFlow: object = {
  [LOGIN_TYPES.EMAIL_CONFIRMATION]: {
    [LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.EMAIL_CONFIRMATION,
      error: POINTERS.CURRENT,
      back: null
    },
    [LOGIN_STEPS.EMAIL_CONFIRMATION]: {
      success: (_loginType: LOGIN_TYPES) => LANDING_PAGE,
      error: POINTERS.CURRENT,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL
    },
    [LOGIN_STEPS.RECOVER]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.RECOVER_SUCCESS,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      error: POINTERS.CURRENT
    },
    [LOGIN_STEPS.RECOVER_SUCCESS]: {
      success: () => LOGIN_STEPS.EMAIL_CONFIRMATION,
      back: LOGIN_STEPS.RECOVER,
      error: POINTERS.CURRENT
    }
  },
  [LOGIN_TYPES.MSISDN_OTP]: {
    [LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.ENTER_OTP,
      error: POINTERS.CURRENT,
      back: null
    },
    [LOGIN_STEPS.ENTER_OTP]: {
      success: (_loginType: LOGIN_TYPES) => LANDING_PAGE,
      error: POINTERS.CURRENT,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL
    },
    [LOGIN_STEPS.RECOVER]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.RECOVER_SUCCESS,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      error: POINTERS.CURRENT
    },
    [LOGIN_STEPS.RECOVER_SUCCESS]: {
      success: () => LOGIN_STEPS.ENTER_OTP,
      back: LOGIN_STEPS.RECOVER,
      error: POINTERS.CURRENT
    }
  },
  [LOGIN_TYPES.EMAIL_OTP]: {
    [LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.ENTER_OTP,
      error: POINTERS.CURRENT,
      back: null
    },
    [LOGIN_STEPS.ENTER_OTP]: {
      success: (_loginType: LOGIN_TYPES) => LANDING_PAGE,
      error: POINTERS.CURRENT,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL
    },
    [LOGIN_STEPS.RECOVER]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.RECOVER_SUCCESS,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      error: POINTERS.CURRENT
    },
    [LOGIN_STEPS.RECOVER_SUCCESS]: {
      success: () => LOGIN_STEPS.ENTER_OTP,
      back: LOGIN_STEPS.RECOVER,
      error: POINTERS.CURRENT
    }
  },
  [LOGIN_TYPES.USERNAME_PASSWORD]: {
    [LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.ENTER_PASSWORD,
      error: POINTERS.CURRENT,
      back: null,
      forgotUsername: LOGIN_STEPS.RECOVER_USING_EMAIL
    },
    [LOGIN_STEPS.ENTER_PASSWORD]: {
      success: (_loginType: LOGIN_TYPES) => LANDING_PAGE,
      error: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      forgotPassword: LOGIN_STEPS.RECOVER_USING_EMAIL
    },
    [LOGIN_STEPS.RECOVER]: {
      success: (_loginType: LOGIN_TYPES) => LOGIN_STEPS.RECOVER_SUCCESS,
      back: LOGIN_STEPS.ENTER_LOGIN_CREDENTIAL,
      error: POINTERS.CURRENT
    },
    [LOGIN_STEPS.RECOVER_SUCCESS]: {
      success: () => LOGIN_STEPS.ENTER_PASSWORD,
      back: LOGIN_STEPS.RECOVER,
      error: POINTERS.CURRENT
    },
    [LOGIN_STEPS.IDENTITY_VERIFICATION]: {
      success: () => '',
      back: null,
      error: null
    }
  }
};

export const replacedString = (
  toBeReplacedSentence: string,
  toBeReplacedString: string,
  replaceWith: string
): string => {
  return toBeReplacedSentence.replace(toBeReplacedString, replaceWith);
};
