export const getMaskedValue = (
  maskingPattern: string,
  value: string
): string => {
  const maskingpatternRegex =
    maskingPattern && maskingPattern.length ? new RegExp(maskingPattern) : null;
  let maskedValue = value;
  if (maskingpatternRegex) {
    maskedValue = (maskedValue || '').replace(
      maskingpatternRegex,
      (_, a, b, c) => a + b.replace(/./g, '*') + c
    );
  }

  return maskedValue;
};
