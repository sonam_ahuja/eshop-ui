import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationStates from '@store/states/translation';
import { StaticRouter } from 'react-router';
import { KEY_CODE } from '@src/common/constants/appConstants';

import BasketHeader, { IProps as IBasketHeaderProps } from '.';

describe('<BasketHeader />', () => {
  const props: IBasketHeaderProps = {
    enableClear: true,
    clearBasket: jest.fn(),
    translation: translationStates().cart.basket,
    clearBasketModalState: {
      loading: false,
      error: null,
      modalOpen: false
    },
    setDeleteBasketModal: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketHeader {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when enableClear false', () => {
    const newProps: IBasketHeaderProps = { ...props, enableClear: false };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketHeader {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when clearBasketModalState modal is open and enableClear is true', () => {
    const newProps: IBasketHeaderProps = {
      enableClear: true,
      clearBasket: jest.fn(),
      translation: translationStates().cart.basket,
      clearBasketModalState: {
        loading: false,
        error: null,
        modalOpen: true
      },
      setDeleteBasketModal: jest.fn()
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketHeader {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    component
      .find('div[onClick]')
      .at(0)
      .simulate('click');
    component
      .find('button[onClick]')
      .at(0)
      .simulate('click');
    component
      .find('div[onKeyDown]')
      .at(0)
      .simulate('keypress');
  });

  test('should render properly when clearBasketModalState modal is open and enableClear is false', () => {
    const newProps: IBasketHeaderProps = {
      enableClear: false,
      clearBasket: jest.fn(),
      translation: translationStates().cart.basket,
      clearBasketModalState: {
        loading: false,
        error: null,
        modalOpen: true
      },
      setDeleteBasketModal: jest.fn()
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketHeader {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );

    component
      .find('div[onClick]')
      .at(0)
      .simulate('click');
  });

  test('should render properly, when onkeydown modal open function call ', () => {
    const newProps: IBasketHeaderProps = { ...props, enableClear: true };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketHeader {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    try {
      component
        .find('div[onKeyDown]')
        // tslint:disable-next-line: no-object-literal-type-assertion
        .simulate('keydown', { keyCode: KEY_CODE.ENTER } as React.KeyboardEvent<
          Element
        >);
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.log('error ', error);
    }
  });
});
