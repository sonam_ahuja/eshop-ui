import React, { FunctionComponent } from 'react';
import { Paragraph, Section, Title } from 'dt-components';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { IBasketState } from '@basket/store/types';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';
import { KEY_CODE } from '@src/common/constants/appConstants';
import { sendClearBasketEvent, sendPopupCloseEvent } from '@events/basket';
import EVENT_NAME from '@events/constants/eventName';

import ClearBasketModal from '../ClearBasketModal';

import { StyledBasketItemCount, StyledTitleWrapper } from './styles';

export interface IProps {
  showBasketItemCount?: boolean;
  basketItemCount?: number;
  style?: React.CSSProperties;
  enableClear: boolean;
  clearBasketModalState: IBasketState['clearBasket'];
  translation: IBasketTranslation;
  setDeleteBasketModal(visibility: boolean): void;
  clearBasket(): void;
}

export const BasketHeader: FunctionComponent<IProps> = (props: IProps) => {
  const { enableClear, translation, clearBasketModalState } = props;
  const { basketItemCount, showBasketItemCount } = props;
  const basketTranslation = props.translation;

  const onModalOpen = () => {
    props.setDeleteBasketModal(true);
  };

  const onKeyDownModalOpen = (event: React.KeyboardEvent<Element>) => {
    if (event.keyCode === KEY_CODE.ENTER) {
      onModalOpen();
    }
  };

  const close = () => {
    props.setDeleteBasketModal(false);
  };

  const handleClose = () => {
    sendPopupCloseEvent();
    props.setDeleteBasketModal(false);
  };

  const remove = () => {
    sendClearBasketEvent();
    props.clearBasket();
  };

  const itemCountEl = `${!!basketItemCount ? basketItemCount : 0} ${
    basketTranslation.items
  }`;

  return (
    <StyledTitleWrapper>
      <Title tag='h1' weight='ultra' size='main'>
        {basketTranslation.pageHeading}
      </Title>
      {!!showBasketItemCount ? (
        <StyledBasketItemCount>
          <span className='basketItemCount'>
            <Section size='large' weight='bold'>
              {itemCountEl}
            </Section>
          </span>
        </StyledBasketItemCount>
      ) : null}
      {enableClear ? (
        <Paragraph
          className='clearBasketAction'
          size='small'
          transform='capitalize'
        >
          <div
            onClick={onModalOpen}
            tabIndex={0}
            onKeyDown={onKeyDownModalOpen}
            data-event-id={EVENT_NAME.BASKET.EVENTS.CLEAR}
            data-event-message={basketTranslation.clearBasket}
          >
            {basketTranslation.clearBasket}
          </div>
        </Paragraph>
      ) : null}
      <AuxiliaryRoute
        isChildrenRender={clearBasketModalState.modalOpen}
        hashPath={auxillaryRouteConfig.BASKET.DELETE}
        isModal={true}
        onClose={close}
      >
        <ClearBasketModal
          clearBasketModalState={clearBasketModalState}
          translation={translation}
          open={clearBasketModalState.modalOpen}
          onClose={handleClose}
          onRemove={remove}
        />
      </AuxiliaryRoute>
    </StyledTitleWrapper>
  );
};

export default BasketHeader;
