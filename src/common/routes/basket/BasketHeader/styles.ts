import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledTitleWrapper = styled.div`
  position: relative;
  margin-bottom: 1.25rem;
  color: ${colors.darkGray};
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  .clearBasketAction {
    color: ${colors.mediumGray};
    align-self: flex-end;
    cursor: pointer;
    /* text-transform: capitalize; */
    padding-bottom: 0.22rem;
    padding-right: 0.45rem;
    & + {
      .dt_dialogBox {
        .dt_overlay {
          overflow: hidden !important;
        }
      }
    }
  }
  & + {
    .dt_dialogBox {
      .dt_overlay {
        overflow: hidden !important;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .clearBasketAction {
      padding-right: 0;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .clearBasketAction {
      padding-right: 0.2rem;
      padding-bottom: 0.4rem;
    }
  }
`;

export const StyledBasketItemCount = styled.div`
  color: ${colors.mediumGray};
  flex-shrink: 0;
  align-items: flex-end;
  display: flex;
  flex: 1;
  margin-left: 0.75rem;
  padding-bottom: 0.1rem;
`;
