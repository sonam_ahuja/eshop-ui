import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import {
  BASKET_ALL_ITEM,
  BASKET_RESPONSE_WITH_OUTSTOCK_PRODUCT
} from '@mocks/basket/basket.mock';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router';

import ItemCard, { IProps as IItemCardProps } from '.';

describe('<ItemCard />', () => {
  test('should render properly', () => {
    const props: IItemCardProps = {
      basketItem: BASKET_ALL_ITEM.cartItems[0],
      currency: 'INR',
      fullwidth: true,
      outOfStock: true,
      basketTranslation: appState().translation.cart.basket,
      isPreOrder: false
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <ItemCard {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when item is outofstock', () => {
    const props: IItemCardProps = {
      basketItem: BASKET_RESPONSE_WITH_OUTSTOCK_PRODUCT.cartItems[0],
      fullwidth: false,
      currency: 'INR',
      outOfStock: false,
      basketTranslation: appState().translation.cart.basket,
      isPreOrder: false
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <ItemCard {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
