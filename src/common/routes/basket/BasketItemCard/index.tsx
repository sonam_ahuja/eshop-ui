import React, { FunctionComponent } from 'react';
import { Paragraph, Section } from 'dt-components';
import { IBasketItem, ICharacteristic } from '@basket/store/types';
import { ITEM_AVAILABILITY_STATUS, ITEM_TYPE } from '@basket//store/enums';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { Link } from 'react-router-dom';
import PreloadImageWrapper from '@src/common/components/Preload';
import { IMAGE_TYPE } from '@common/store/enums';
import { checkSafariAndIEBrowser } from '@src/client/DOMUtils';
import { CMS_REPLACEABLE_KEY } from '@common/constants/appConstants';
import { getInstallmentsText } from '@basket/utils/installmentType';
import EVENT_NAME from '@events/constants/eventName';
import basketConstants from '@basket/constants';
import he from 'he';

import {
  Card,
  CardContent,
  OutOfStockText,
  ProductName,
  StyledHtmlList,
  StyledList,
  StyledPreorderText
} from './styles';

export interface IProps {
  basketItem: IBasketItem;
  fullwidth: boolean;
  outOfStock: boolean;
  isPreOrder: boolean;
  currency: string;
  basketTranslation: IBasketTranslation;
}

const Installments = (
  item: IBasketItem,
  basketTranslation: IBasketTranslation,
  currency: string
) => {
  return <div>{getInstallmentsText(item, basketTranslation, currency)}</div>;
};

const productLink = (item: IBasketItem) => {
  return item.group.toLowerCase() === ITEM_TYPE.TARIFF
    ? `tariff/${item.product.categorySlug}`
    : `${item.product.categorySlug}/${item.product.name}/${
        item.product.specId
      }/${item.product.id}`;
};

const ItemCard: FunctionComponent<IProps> = (props: IProps) => {
  const { basketItem, isPreOrder, currency } = props;
  const { basketTranslation } = props;
  const addOns = basketItem.addons;
  const isSAfariBrowser = checkSafariAndIEBrowser();
  const imageType = isSAfariBrowser ? IMAGE_TYPE.PNG : IMAGE_TYPE.WEBP;
  const basketDescription =
    basketItem.product &&
    basketItem.product.characteristic &&
    basketItem.product.characteristic.filter((itemChar: ICharacteristic) => {
      return itemChar.name === basketConstants.basketDescription;
    });

  const evaluteCardContent = () =>
    basketItem.status !== ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK ? (
      <>
        {isPreOrder ? (
          <StyledPreorderText>
            {basketTranslation.preOrderText}
          </StyledPreorderText>
        ) : null}

        <ProductName>
          <Paragraph
            tag='h2'
            size='large'
            weight='medium'
            transform='capitalize'
          >
            {basketItem.group &&
            basketItem.group.toLowerCase() === ITEM_TYPE.TARIFF
              ? basketItem.product.variantName
              : basketItem.product.name}
          </Paragraph>
        </ProductName>
        <Paragraph size='small' transform='capitalize'>
          {basketItem.product.shortDescription}
        </Paragraph>
        {Installments(basketItem, basketTranslation, currency)}
        <StyledList>
          <>
            {basketDescription && basketDescription.length ? (
              basketDescription.map(
                (itemChar: ICharacteristic, index: number) => {
                  return (
                    <>
                      <Section className='listItem' key={index} size='large'>
                        {itemChar.value}
                      </Section>
                    </>
                  );
                }
              )
            ) : (
              <StyledHtmlList
                dangerouslySetInnerHTML={{
                  __html:
                    basketItem.product && basketItem.product.description
                      ? he.decode(basketItem.product.description.trim())
                      : ''
                }}
              />
            )}

            {addOns &&
              Object.keys(addOns) &&
              Object.keys(addOns).length &&
              Object.keys(addOns).map((addon: string, index: number) => {
                const name = addOns[addon].categoryName;
                const cartItems = addOns[addon].cartItems;

                return (
                  <Section
                    className='listItem socialListItem'
                    key={index}
                    size='large'
                  >
                    <div className='list-inline'>
                      {Array.isArray(cartItems) &&
                        cartItems.length &&
                        cartItems.map((item: IBasketItem, idx: number) => {
                          return (
                            <div
                              className={
                                item.product.imageUrl &&
                                item.product.imageUrl.length
                                  ? 'socialLists'
                                  : ''
                              }
                              key={idx}
                            >
                              <span className='addOnsTitles'>{name}</span>
                              {item.product.imageUrl &&
                              item.product.imageUrl.length ? (
                                <div className='addOnsImageListsWrap'>
                                  <Link
                                    className='primaryLink'
                                    to={productLink(item)}
                                  >
                                    <PreloadImageWrapper
                                      loadDefaultImage={true}
                                      isObserveOnScroll={true}
                                      imageHeight={50}
                                      mobileWidth={50}
                                      width={50}
                                      imageWidth={50}
                                      alt={item.product.variantName}
                                      type={imageType}
                                      imageUrl={item.product.imageUrl}
                                      intersectionObserverOption={{
                                        root: null,
                                        rootMargin: '60px',
                                        threshold: 0
                                      }}
                                    />
                                  </Link>
                                </div>
                              ) : (
                                item.product.variantName
                              )}
                            </div>
                          );
                        })}
                    </div>
                  </Section>
                );
              })}
          </>
        </StyledList>
      </>
    ) : (
      <OutOfStockText>
        <Paragraph size='large' weight='medium'>
          {basketTranslation.sorryText.replace(
            CMS_REPLACEABLE_KEY,
            basketItem.product.name
          )}{' '}
          <strong>{basketTranslation.outOfStockText}</strong>
        </Paragraph>
        <Section size='large' className='selectDeviceCta'>
          <Link
            data-event-id={EVENT_NAME.BASKET.EVENTS.CHANGE_VARIANT}
            data-event-message={basketTranslation.changeVariant}
            className='link'
            to={
              basketItem.group.toLowerCase() === ITEM_TYPE.TARIFF
                ? `tariff/${basketItem.product.categorySlug}`
                : `${basketItem.product.categorySlug}/${
                    basketItem.product.name
                  }/${basketItem.product.specId}/${basketItem.product.id}`
            }
          >
            {basketTranslation.changeVariant}
          </Link>{' '}
          {basketItem.group.toLowerCase() === ITEM_TYPE.TARIFF ? null : (
            <>
              <span className='linkSeprator'>{basketTranslation.or} </span>
              <Link
                className='link'
                to={`${basketItem.product.categorySlug}`}
                data-event-id={EVENT_NAME.BASKET.EVENTS.SELECT_DIFF_DEVICE}
                data-event-message={basketTranslation.selectOtherDevice}
              >
                {basketTranslation.selectOtherDevice}
              </Link>
            </>
          )}
        </Section>
      </OutOfStockText>
    );

  return (
    <Card fullwidth={props.fullwidth} outOfStock={props.outOfStock}>
      {basketItem.product.imageUrl && basketItem.product.imageUrl.length ? (
        <div className='productImage'>
          <Link className='primaryLink' to={productLink(basketItem)}>
            <PreloadImageWrapper
              loadDefaultImage={true}
              isObserveOnScroll={true}
              imageUrl={basketItem.product.imageUrl}
              imageHeight={50}
              imageWidth={50}
              mobileWidth={50}
              width={50}
              type={imageType}
              alt={basketItem.product.variantName}
            />
          </Link>
        </div>
      ) : null}
      <CardContent>{evaluteCardContent()}</CardContent>
    </Card>
  );
};

export default ItemCard;
