import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';
import { Paragraph } from 'dt-components';

export const Card = styled.div<{ fullwidth?: boolean; outOfStock?: boolean }>`
  display: flex;
  padding: 1rem 0.75rem 1.25rem;
  width: 100%;
  position: relative;
  background: ${({ outOfStock }) => (outOfStock ? 'none' : colors.coldGray)};
  border-radius: 0.25rem;
  border: 1px dashed
    ${({ outOfStock }) => (outOfStock ? colors.silverGray : 'transparent')};

  & + & {
    margin-top: 0.25rem;
  }

  .productImage {
    min-width: 5rem;
    height: 5rem;
    width: 5rem;
    margin-right: 0.75rem;

    opacity: ${({ outOfStock }) => (outOfStock ? '0.3' : '1')};

    img {
      bottom: auto;
    }
  }

  .lineThrough {
    span {
      position: relative;
      color: ${colors.mediumGray};
      &:after {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        margin: auto;
        height: 1px;
        width: 100%;
        background: ${colors.magenta};
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .productImage {
      margin-right: 0.5rem;
    }
    & + & {
      margin: 0;
      margin-left: 0.5rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 1rem 0.75rem 0.88rem;
    .productImage {
      margin-right: 0.77rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    flex-wrap: wrap;
    padding: 1rem 0.75rem 1rem;

    & + & {
      margin-left: 0.25rem;
    }
  }
`;

export const OutOfStockText = styled.div`
  color: ${colors.mediumGray};
  padding-bottom: 3.75rem;

  strong {
    color: ${colors.darkGray};
  }

  .selectDeviceCta {
    position: absolute;
    bottom: 0;

    .link {
      color: ${colors.magenta};
      &::first-letter {
        display: inline-block;
        /* text-transform: uppercase; */
      }
    }

    .linkSeprator {
      color: ${colors.darkGray};
    }
  }
`;

export const StyledHtmlList = styled.div`
  ul li {
    position: relative;
    padding-left: 0.75rem;
    font-size: 0.75rem;
    line-height: 1rem;
    letter-spacing: 0.2px;
    color: ${colors.ironGray};

    &:after {
      content: '';
      position: absolute;
      left: 0;
      top: 0.45rem;
      height: 0.2rem;
      width: 0.2rem;
      background-color: currentColor;
    }
  }
  font-size: 0.75rem;
`;
export const CardContent = styled.div`
  position: relative;
  color: ${colors.ironGray};

  @media (min-width: ${breakpoints.desktop}px) {
    /* flex-basis: 15rem; */
    flex-basis: 80%;
  }
`;

export const ProductName = styled.div`
  color: ${colors.darkGray};
  margin-bottom: 0.5rem;
  word-break: break-word;
`;

export const StyledPreorderText = styled(Paragraph).attrs({
  tag: 'h1',
  size: 'large',
  weight: 'medium',
  transform: 'capitalize'
})`
  color: ${colors.magenta};
`;

export const StyledList = styled.div`
  padding-top: 0.5rem;

  .listItem {
    position: relative;
    padding-left: 0.75rem;

    &:after {
      content: '';
      position: absolute;
      left: 0;
      top: 0.45rem;
      height: 0.2rem;
      width: 0.2rem;
      background-color: currentColor;
    }
    &.socialListItem {
      display: flex;
      margin-bottom: 0.1rem;
      .socialLists {
        display: flex;
        flex-wrap: wrap;
        .addOnsTitles {
          flex-shrink: 0;
          word-break: break-word;
          max-width: 100%;
          margin-right: 0.25rem;
        }
        .addOnsImageListsWrap {
          display: flex;
          .es_preloadImage {
            width: 1rem;
            height: 1rem;
            margin-right: 0.25rem;
            padding: 0;
            img {
              margin: auto;
            }
          }
        }
      }
    }
  }
`;
