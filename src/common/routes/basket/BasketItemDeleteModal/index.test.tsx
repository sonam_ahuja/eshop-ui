import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import {
  BASKET_ALL_ITEM,
  BASKET_ITEM_NOT_HAVE_SUB_ITEMS
} from '@mocks/basket/basket.mock';
import translationState from '@store/states/translation';
import { IBasketItem } from '@basket/store/types';

import BasketItemDeleteModal, { IProps as BasketItemDeleteModalProps } from '.';

describe('<BasketItemDeleteModal />', () => {
  test('should render properly', () => {
    const props: BasketItemDeleteModalProps = {
      open: true,
      onClose: jest.fn(),
      onRemove: jest.fn(),
      basketTranslation: translationState().cart.basket,
      removeBasketItemModalState: {
        modalOpen: true,
        error: null,
        loading: false,
        selectedItem: BASKET_ALL_ITEM.cartItems[0]
      }
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <BasketItemDeleteModal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without sub key cartItems', () => {
    const newBasketItem: IBasketItem = BASKET_ALL_ITEM.cartItems[0];
    newBasketItem.cartItems = [];
    const props: BasketItemDeleteModalProps = {
      open: true,
      onClose: jest.fn(),
      onRemove: jest.fn(),
      basketTranslation: translationState().cart.basket,
      removeBasketItemModalState: {
        modalOpen: true,
        error: null,
        loading: false,
        selectedItem: null
      }
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <BasketItemDeleteModal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without cartItems key', () => {
    const props: BasketItemDeleteModalProps = {
      open: true,
      onClose: jest.fn(),
      onRemove: jest.fn(),
      basketTranslation: translationState().cart.basket,
      removeBasketItemModalState: {
        modalOpen: true,
        error: null,
        loading: false,
        selectedItem: BASKET_ITEM_NOT_HAVE_SUB_ITEMS.cartItems[0]
      }
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <BasketItemDeleteModal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-identical-functions
  test('should render properly  cartItems when group is not tariff', () => {
    const props: BasketItemDeleteModalProps = {
      open: true,
      onClose: jest.fn(),
      onRemove: jest.fn(),
      basketTranslation: translationState().cart.basket,
      removeBasketItemModalState: {
        modalOpen: true,
        error: null,
        loading: false,
        selectedItem: {
          id: '2c9683ae6c3133e1016c32e4eb490001',
          isModifiable: false,
          status: 'Active',
          totalPrice: [
            {
              priceAlterations: [
                {
                  priceType: 'upfrontPrice',
                  duration: {
                    timePeriod: '12',
                    type: 'monthly'
                  },
                  price: 420
                }
              ],
              priceType: 'upfrontPrice',
              totalPrice: 420,
              price: 400
            }
          ],
          quantity: 2,
          group: 'device',
          product: {
            specId: '123',
            categorySlug: '12333',
            id: 'iphone_7p_V_1',
            imageUrl:
              'https://cdn.eshop-image-resize.yo-digital.com/resize-image?f=bb_9d_17128c16c26a30c2f2b92dbffaa0.jpeg',
            name: 'abc',
            brandName: 'addidas',
            description: 'description',
            shortDescription: 'hi',
            productGroupId: 'groupidproduct'
          },
          cartTerms: [],
          itemPrice: [],
          cartItems: []
        }
      }
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <BasketItemDeleteModal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
