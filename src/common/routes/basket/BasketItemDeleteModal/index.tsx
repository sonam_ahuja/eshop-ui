import React, { FunctionComponent, ReactNode } from 'react';
import { Button, Title } from 'dt-components';
import { IBasketItem, IBasketState } from '@basket/store/types';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import EVENT_NAME from '@events/constants/eventName';
import { ITEM_TYPE } from '@basket/store/enums';
import PreloadImageWrapper from '@src/common/components/Preload';
import { sendPopupCloseEvent } from '@events/basket';

import {
  StyledDescription,
  StyledLabel,
  StyledProductInfo,
  StyledProductListWrap
} from './styles';
export interface IProps {
  open: boolean;
  basketTranslation: IBasketTranslation;
  removeBasketItemModalState: IBasketState['removeBasketItem'];
  onClose(): void;
  onRemove(): void;
}

const BasketItemDeleteModal: FunctionComponent<IProps> = (props: IProps) => {
  const { open, onClose } = props;
  const { onRemove, removeBasketItemModalState } = props;
  const { basketTranslation } = props;
  const removablepProductList: ReactNode[] = [];
  const { selectedItem: basketItem } = removeBasketItemModalState;

  const handleClose = () => {
    sendPopupCloseEvent();
    onClose();
  };
  const getProductName = (item: IBasketItem) => {
    return item.group && item.group.toLowerCase() === ITEM_TYPE.TARIFF
      ? item.product.variantName || item.product.name
      : item.product.name;
  };
  const productList = () => {
    if (basketItem && Object.keys(basketItem).length) {
      removablepProductList.push(
        <>
          <StyledProductInfo className='productInfo'>
            {basketItem.product.imageUrl &&
            basketItem.product.imageUrl.length ? (
              <div className='productImage'>
                <PreloadImageWrapper
                  key={basketItem.id}
                  // imageUrl={`${basketItem.product.imageUrl}&q=90&w=50&t=png`}
                  loadDefaultImage={true}
                  isObserveOnScroll={true}
                  imageUrl={basketItem.product.imageUrl}
                  imageHeight={50}
                  imageWidth={50}
                  mobileWidth={50}
                  width={50}
                  alt={basketItem.product.variantName}
                />
              </div>
            ) : null}
            <div className='productInfo'>
              <StyledLabel>{getProductName(basketItem)}</StyledLabel>
              <StyledDescription>
                {basketItem.product.shortDescription}
              </StyledDescription>
            </div>
          </StyledProductInfo>
        </>
      );
      if (
        basketItem.group.toLowerCase() !== ITEM_TYPE.TARIFF &&
        Array.isArray(basketItem.cartItems) &&
        basketItem.cartItems.length
      ) {
        basketItem.cartItems.forEach((subBasketItem: IBasketItem) =>
          removablepProductList.push(
            <>
              <StyledProductInfo>
                {subBasketItem.product.imageUrl &&
                subBasketItem.product.imageUrl.length ? (
                  <div className='productImage'>
                    <PreloadImageWrapper
                      key={subBasketItem.id}
                      loadDefaultImage={true}
                      isObserveOnScroll={true}
                      imageUrl={subBasketItem.product.imageUrl}
                      imageHeight={50}
                      imageWidth={50}
                      mobileWidth={50}
                      width={50}
                      alt={basketItem.product.variantName}
                    />
                  </div>
                ) : null}
                <div className='productInfo'>
                  <StyledLabel>{getProductName(subBasketItem)}</StyledLabel>
                  <StyledDescription>
                    {subBasketItem.product.shortDescription}
                  </StyledDescription>
                </div>
              </StyledProductInfo>
            </>
          )
        );
      }

      return removablepProductList;
    }

    return null;
  };

  return (
    <DialogBoxApp
      isOpen={open}
      closeOnBackdropClick={true}
      closeOnEscape={true}
      onEscape={handleClose}
      onBackdropClick={handleClose}
      backgroundScroll={false}
      type='flexibleHeight'
      onClose={handleClose}
    >
      <div className='dialogBoxHeader'>
        <StyledProductListWrap>{productList()}</StyledProductListWrap>
      </div>
      <div className='dialogBoxBody'>
        <Title className='infoMessage' size='large' weight='ultra'>
          {basketTranslation.popUps.removeOneItem}
        </Title>
      </div>
      <div className='dialogBoxFooter'>
        <Button
          size='medium'
          type='secondary'
          loading={removeBasketItemModalState.loading}
          onClickHandler={onRemove}
          data-event-id={EVENT_NAME.BASKET.EVENTS.REMOVE_ITEM}
          data-event-message={basketTranslation.popUps.removeButton}
        >
          {basketTranslation.popUps.removeButton}
        </Button>
      </div>
    </DialogBoxApp>
  );
};

export default BasketItemDeleteModal;
