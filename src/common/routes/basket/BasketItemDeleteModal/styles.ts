import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const StyledLabel = styled.div`
  margin-bottom: 0.25rem;
  font-size: 0.875rem;
  line-height: 1.25rem;
  letter-spacing: 0px;
  color: ${colors.darkGray};
`;

export const StyledDescription = styled.div`
  font-size: 0.875rem;
  line-height: 1.25rem;
  letter-spacing: 0px;
  color: ${colors.mediumGray};
`;

export const StyledProductInfo = styled.div`
  padding: 1rem 0.75rem 1rem 0.75rem;
  display: flex;
  align-items: flex-start;
  cursor: pointer;
  border-radius: 0.5rem;
  background: ${colors.coldGray};
  .productImage {
    max-width: 3.75rem;
    width: 100%;
    margin: 0px 0.75rem 0px 0px;
    flex-shrink: 0;
  }
  & + .productInfo {
    margin-top: 0.25rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    min-height: 5rem;
    .productImage {
      max-width: 3rem;
    }
  }
`;

export const StyledProductListWrap = styled.div`
  width: 100%;
  background: ${colors.white};
  padding: 0.5rem;
  border-radius: 0.5rem;
`;
