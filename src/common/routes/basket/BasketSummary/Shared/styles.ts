import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledColumn = styled.div`
  flex-basis: 50%;
`;

export const StyledRow = styled.div`
  display: table;
  width: 100%;
  padding-bottom: 0.5rem;

  ${StyledColumn} {
    text-align: left;
    display: table-cell;
    width: 50%;
  }

  ${StyledColumn}:last-child {
    text-align: right;
  }

  &.total {
    padding-bottom: 0;
    padding-top: 1rem;
  }

  .totalAmount {
    color: ${colors.cloudGray};
  }
`;
