import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { paymentMethod } from '@mocks/common/payment.mock';

import SummaryPaymentMethods, { IProps } from '.';

describe('<SummaryPaymentMethods />', () => {
  test('should render properly', () => {
    const props: IProps = {
      availablePaymentMethods: paymentMethod
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryPaymentMethods {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
