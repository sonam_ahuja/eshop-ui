import React, { FunctionComponent } from 'react';
import PreloadImage from '@src/common/components/Preload';
import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { IPaymentMethods } from '@src/common/store/types/configuration';

const StyledPaymentMethods = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  position: absolute;
  bottom: 3.8rem;
  width: auto;
  left: 0;
  right: 0;
  margin: 0 rem;

  .productImage {
    width: 30px;
    height: 30px;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    position: static;
    margin: 0;
    margin-bottom: 1.25rem;
    width: 100%;
  }
`;

export interface IProps {
  availablePaymentMethods: IPaymentMethods;
}

const SummaryPaymentMethods: FunctionComponent<IProps> = (props: IProps) => {
  const { availablePaymentMode } = props.availablePaymentMethods;

  return (
    <StyledPaymentMethods>
      {availablePaymentMode.maestro && (
        <div className='productImage'>
          <PreloadImage
            imageUrl='/maestro.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
      )}
      {availablePaymentMode.applePay && (
        <div className='productImage'>
          <PreloadImage
            imageUrl='/apple-pay.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
      )}
      {availablePaymentMode.visa && (
        <div className='productImage'>
          <PreloadImage imageUrl='/visa.png' imageHeight={10} imageWidth={10} />
        </div>
      )}
      {availablePaymentMode.googlePay && (
        <div className='productImage'>
          <PreloadImage
            imageUrl='/google-pay.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
      )}
    </StyledPaymentMethods>
  );
};

export default SummaryPaymentMethods;
