import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@store/states/translation';
import { IProps as ISummaryUpfrontProps } from '@basket/BasketSummary/SummaryUpfront';

import SummaryUpfront from '.';

describe('<SummaryUpfront />', () => {
  test('should render properly', () => {
    const props: ISummaryUpfrontProps = {
      cartSummary: BASKET_ALL_ITEM.cartSummary[0],
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      translation: translationState()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryUpfront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when price and total Price and priceAlteration  not present', () => {
    const summary = { ...BASKET_ALL_ITEM.cartSummary[0] };
    delete summary.price;
    delete summary.totalPrice;
    delete summary.priceAlterations;
    const props: ISummaryUpfrontProps = {
      cartSummary: summary,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      translation: translationState()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryUpfront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
