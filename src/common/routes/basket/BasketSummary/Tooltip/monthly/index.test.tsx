import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { cartRecurringBreakUp } from '@mocks/basket/basket.mock';
import MonthlyToolTip, {
  IProps as IMonthlyToolTip
} from '@basket/BasketSummary/Tooltip/monthly';
import appState from '@store/states/app';

describe('<MonthlyToolTip />', () => {
  test('should render properly', () => {
    const props: IMonthlyToolTip = {
      translation: appState().translation.cart.basket,
      title: 'Tooltip content',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      cartRecurringBreakUp
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <MonthlyToolTip {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();

    (component
      .find('MonthlyToolTip')
      .instance() as MonthlyToolTip).openTooltip();
    (component
      .find('MonthlyToolTip')
      .instance() as MonthlyToolTip).closeTooltip();
  });
});
