import React, { Component, ReactNode } from 'react';
import { Icon, Tooltip } from 'dt-components';
import { ICartRecurringBreakUp } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

import TooltipContent from './tooltipContent';

export interface IProps {
  title: string;
  currency: ICurrencyConfiguration;
  cartRecurringBreakUp: ICartRecurringBreakUp[];
  translation: IBasket;
}

interface IState {
  tooltipOpen: boolean;
}

class MonthlyToolTip extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tooltipOpen: false
    };
    this.openTooltip = this.openTooltip.bind(this);
    this.closeTooltip = this.closeTooltip.bind(this);
  }

  openTooltip(): void {
    this.setState({
      tooltipOpen: true
    });
  }

  closeTooltip(): void {
    this.setState({
      tooltipOpen: false
    });
  }

  render(): ReactNode {
    const { currency, cartRecurringBreakUp } = this.props;
    const { title, translation } = this.props;

    return (
      <>
        <Tooltip
          autoClose={true}
          position='top'
          isOpen={this.state.tooltipOpen}
          tooltipContent={
            <TooltipContent
              translation={translation}
              title={title}
              currency={currency}
              cartRecurringBreakUp={cartRecurringBreakUp}
              onClose={this.closeTooltip}
            />
          }
        >
          <span>
            <Icon onClick={this.openTooltip} name='ec-information' />
          </span>
        </Tooltip>
      </>
    );
  }
}

export default MonthlyToolTip;
