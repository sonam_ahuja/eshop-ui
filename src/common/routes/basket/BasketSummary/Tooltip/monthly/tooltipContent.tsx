import React, { ReactNode } from 'react';
import { Icon, Section } from 'dt-components';
import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { ICartRecurringBreakUp } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { formatCurrency } from '@common/utils/currency';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

interface IProps {
  title: string;
  currency: ICurrencyConfiguration;
  cartRecurringBreakUp: ICartRecurringBreakUp[];
  translation: IBasket;
  onClose(): void;
}

const StyledTooltipContent = styled.div`
  color: ${colors.cloudGray};
  min-width: 21rem;

  .title {
    position: relative;
    padding-bottom: 1rem;
  }
  .iconClose {
    position: absolute;
    top: -1rem;
    right: -1rem;
  }

  .mainText {
    > p {
      display: flex;
      justify-content: space-between;
    }
  }

  .footerText {
    > p {
      display: flex;
      justify-content: space-between;
    }
  }

  .divider {
    min-height: 1px;
    color: ${colors.stoneGray};
    margin: 1rem 0;
  }
`;

const renderMonthlySummaryBreakup = (
  currency: ICurrencyConfiguration,
  cartRecurringBreakUp: ICartRecurringBreakUp[],
  translation: IBasket
) => {
  const arry: ReactNode[] = [];

  cartRecurringBreakUp.forEach(
    (monthPeriod: ICartRecurringBreakUp, index: number) => {
      arry.push(
        <p key={index}>
          <span className='label'>
            {translation.after} {monthPeriod.afterDuration.timePeriod}{' '}
            {translation.months}
          </span>
          <span className='value'>
            {formatCurrency(monthPeriod.afterDurationPrice, currency.locale)}
          </span>
        </p>
      );
    }
  );

  return arry;
};

export default (props: IProps) => {
  const { cartRecurringBreakUp, currency } = props;

  const { title, onClose, translation } = props;

  return (
    <StyledTooltipContent>
      <Section
        className='title'
        size='large'
        weight='bold'
        transform={'capitalize'}
      >
        {title}
        <Icon
          className='iconClose'
          name='ec-cancel'
          color='currentColor'
          size='xsmall'
          onClick={onClose}
        />
      </Section>
      <Section className='mainText' size='large'>
        {renderMonthlySummaryBreakup(
          currency,
          cartRecurringBreakUp,
          translation
        )}
      </Section>
    </StyledTooltipContent>
  );
};
