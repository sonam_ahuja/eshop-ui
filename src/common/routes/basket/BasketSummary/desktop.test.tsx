import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@common/store/states/translation';
import configurationState from '@common/store/states/configuration';
import { StaticRouter } from 'react-router';
import { histroyParams } from '@mocks/common/histroy';

import { BasketSummary, IProps as IBasketSummaryProps } from './desktop';

describe('<BasketSummary Desktop/>', () => {
  const props: IBasketSummaryProps = {
    goToLogin: jest.fn(),
    ...histroyParams,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    cartSummary: BASKET_ALL_ITEM.cartSummary,
    translation: translationState(),
    isCheckoutEnable: true,
    configuration: configurationState(),
    isUserLoggedIn: true,
    isTermsConditionChecked: true
  };
  test('BasketSummary should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('BasketSummary should render properly without summary price', () => {
    const newSummary = { ...BASKET_ALL_ITEM.cartSummary };
    newSummary[0].totalPrice = 0;
    newSummary[1].totalPrice = 0;
    const newProps = { ...props };
    newProps.cartSummary = newSummary;
    newProps.hideCheckoutButton = true;
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('Proceed to checkout when isUserLoggedIn is true', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;

    const newProps = { ...props };
    const component = mount<BasketSummary>(
      <ThemeProvider theme={{}}>
        <BasketSummary {...newProps} />
      </ThemeProvider>
    );
    (component
      .find('BasketSummary')
      .instance() as BasketSummary).proceedToCheckout();
  });

  test('Proceed to checkout when isUserLoggedIn is true', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;

    const newProps = { ...props };
    newProps.isUserLoggedIn = false;
    const component = mount<BasketSummary>(
      <ThemeProvider theme={{}}>
        <BasketSummary {...newProps} />
      </ThemeProvider>
    );
    (component
      .find('BasketSummary')
      .instance() as BasketSummary).proceedToCheckout();
  });
});
