import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { ICartSummary } from '@basket/store/types';
import { ITranslationState } from '@store/types/translation';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@store/types/configuration';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  SlideArea,
  SummaryContent,
  SummaryIconTop,
  SummaryWrapper
} from '@basket/BasketSummary/styles';
import EVENT_NAME from '@events/constants/eventName';

import SummaryUpfront from './SummaryUpfront';
import SummaryMonthly from './SummaryMonthly';
import SummaryPaymentMethods from './SummaryPaymentMethods';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  translation: ITranslationState;
  cartSummary: ICartSummary[];
  currency: ICurrencyConfiguration;
  isCheckoutEnable: boolean;
  hideCheckoutButton?: boolean;
  isCheckoutSummary?: boolean;
  configuration: IConfigurationState;
  isUserLoggedIn: boolean;
  isTermsConditionChecked: boolean;
  placeOrderButton?(): void;
  goToLogin(isRoutableFromLogin: boolean): void;
}

export class BasketSummary extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.proceedToCheckout = this.proceedToCheckout.bind(this);
  }

  proceedToCheckout(): void {
    const { placeOrderButton, history } = this.props;
    const { isCheckoutSummary, isUserLoggedIn, goToLogin } = this.props;
    if (!isCheckoutSummary) {
      if (isUserLoggedIn) {
        history.push('/checkout/personal-info');
      } else {
        goToLogin(true);
      }
    } else {
      if (placeOrderButton) {
        placeOrderButton();
      }
    }
  }

  render(): ReactNode {
    const {
      cartSummary,
      translation,
      isCheckoutEnable,
      isTermsConditionChecked,
      currency
    } = this.props;
    const { hideCheckoutButton, isCheckoutSummary } = this.props;
    const { configuration } = this.props;
    const { basket: basketTranslation } = this.props.translation.cart;

    return (
      <>
        <SummaryWrapper isMobile={false}>
          <SlideArea>
            <SummaryContent>
              <SummaryIconTop>
                <Icon
                  name='ec-solid-expand'
                  size='medium'
                  color={'currentColor'}
                />
              </SummaryIconTop>
              <Title
                className='heading'
                size='small'
                weight='ultra'
                transform='capitalize'
              >
                {basketTranslation.summaryHeading}
              </Title>
              <SummaryUpfront
                translation={translation}
                currency={currency}
                cartSummary={cartSummary[0]}
                key={0}
              />
              <SummaryMonthly
                translation={translation}
                currency={currency}
                cartSummary={cartSummary[1]}
                key={1}
              />
            </SummaryContent>
            <SummaryPaymentMethods
              availablePaymentMethods={
                configuration.cms_configuration.global.paymentMethods
              }
            />
          </SlideArea>
          {!!hideCheckoutButton ? null : isCheckoutSummary ? (
            <Button
              className='button'
              size='medium'
              disabled={isTermsConditionChecked}
              loading={false}
              type='primary'
              onClickHandler={this.proceedToCheckout}
              data-event-id={EVENT_NAME.BASKET.EVENTS.PLACE_ORDER}
              data-event-message={
                translation.cart.checkout.orderReview.placeOrder
              }
            >
              {translation.cart.checkout.orderReview.placeOrder}
            </Button>
          ) : (
            <Button
              className='button'
              size='medium'
              disabled={isCheckoutEnable}
              loading={false}
              type='primary'
              onClickHandler={this.proceedToCheckout}
              data-event-id={EVENT_NAME.BASKET.EVENTS.PROCEED_TO_CHECKOUT}
              data-event-message={basketTranslation.proceedToCheckout}
            >
              {basketTranslation.proceedToCheckout}
            </Button>
          )}
        </SummaryWrapper>
      </>
    );
  }
}

export default withRouter(BasketSummary);
