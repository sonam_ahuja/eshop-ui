import React from 'react';
import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';
import StyledShell from '@common/components/ShellWrap';

import { SummaryContent, SummaryWrapper } from './styles';

const Body = styled.div`
  background: ${colors.charcoalGray};
  color: #3e3c3c;
  padding: 7rem 2rem;
  border-radius: 1rem;
  min-height: 20rem;
  position: relative;
  overflow: hidden;
  max-width: 100%;
  align-items: flex-end;
  display: flex;
  flex-direction: column;
  * {
    border-radius: 1rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: 36rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 45rem;
  }

  h2 {
    height: 1.5rem;
    width: 10rem;
    background: currentColor;
  }
  section {
    width: 100%;
    display: flex;
    align-items: flex-end;
    flex-direction: column;
    & + section {
      margin-top: 5rem;
    }
    h6 {
      margin-top: 5rem;
      height: 1.5rem;
      width: 5rem;
      background: currentColor;
    }
    strong {
      width: 100%;
      height: 0rem;
      border: 0.1rem dashed currentColor;
      margin: 1rem 0 1.5rem;
    }
    ul {
      width: 100%;
      li {
        display: flex;
        width: 100%;
        margin-bottom: 1rem;
        justify-content: space-between;
        span {
          height: 1rem;
          width: 5rem;
          background: currentColor;
        }
      }
    }
  }
`;
const SummaryShell = () => {
  const section = (
    <section>
      <h6 />
      <strong />
      <ul>
        <li>
          <span />
          <span />
        </li>
        <li>
          <span />
          <span />
        </li>
        <li>
          <span />
          <span />
        </li>
      </ul>
    </section>
  );

  return (
    <SummaryWrapper isMobile={false}>
      <SummaryContent>
        <StyledShell>
          <Body>
            <h2 />
            {section}
            {section}
          </Body>
        </StyledShell>
      </SummaryContent>
    </SummaryWrapper>
  );
};
export default SummaryShell;
