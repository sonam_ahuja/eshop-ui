import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@common/store/states/translation';
import configurationState from '@common/store/states/configuration';
import { StaticRouter } from 'react-router';

import BasketSummary, { IProps as IBasketSummaryProps } from '.';
import SummaryShell from './index.shell';

describe('<BasketSummary />', () => {
  test('BasketSummary should render properly', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = false;
    const props: IBasketSummaryProps = {
      goToLogin: jest.fn(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      translation: translationState(),
      isCheckoutEnable: true,
      configuration: configurationState(),
      isUserLoggedIn: true,
      isTermsConditionChecked: true
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('SummaryShell should render properly', () => {
    const props = {};
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SummaryShell {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-identical-functions
  test('BasketSummary should render properly in desktop mode', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = false;
    const props: IBasketSummaryProps = {
      goToLogin: jest.fn(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      translation: translationState(),
      isCheckoutEnable: true,
      configuration: configurationState(),
      isUserLoggedIn: true,
      isTermsConditionChecked: true
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();
  });

  test('BasketSummary should render properly in mobile mode', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;
    const props: IBasketSummaryProps = {
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      translation: translationState(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      isCheckoutEnable: true,
      configuration: configurationState(),
      goToLogin: jest.fn(),
      isUserLoggedIn: true,
      isTermsConditionChecked: true
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketSummary {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
