import React from 'react';
import { isMobile } from '@src/common/utils';
import { ITranslationState } from '@store/types/translation';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@store/types/configuration';
import { ICartSummary } from '@basket/store/types';
import Loadable from 'react-loadable';

const BasketSummaryMobile = Loadable({
  loading: () => null,
  loader: () => import('./mobile')
});

const BasketSummaryDesktop = Loadable({
  loading: () => null,
  loader: () => import('./desktop')
});

export interface IProps {
  translation: ITranslationState;
  cartSummary: ICartSummary[];
  currency: ICurrencyConfiguration;
  isCheckoutEnable: boolean;
  hideCheckoutButton?: boolean;
  configuration: IConfigurationState;
  isCheckoutSummary?: boolean;
  isUserLoggedIn: boolean;
  isTermsConditionChecked: boolean;
  goToLogin(isRoutableFromLogin: boolean): void;
  placeOrderButton?(): void;
}

const BasketSummary = (props: IProps) => {
  const navigatorKey = isMobile.phone ? 'mobile' : 'desktop';
  if (navigatorKey === 'mobile') {
    return <BasketSummaryMobile {...props} />;
  } else {
    return <BasketSummaryDesktop {...props} />;
  }
};

export default BasketSummary;
