import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@common/store/states/translation';
import configurationState from '@common/store/states/configuration';
import { histroyParams } from '@mocks/common/histroy';

import { BasketSummary, IProps as IBasketSummaryProps } from './mobile';
import SummaryShell from './index.shell';

const event = {
  preventDefault: jest.fn(),
  stopPropagation: jest.fn(),
  persist: jest.fn()
  // tslint:disable-next-line:no-any
} as any;

describe('<BasketSummary Mobile/>', () => {
  const props: IBasketSummaryProps = {
    goToLogin: jest.fn(),
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    ...histroyParams,
    cartSummary: BASKET_ALL_ITEM.cartSummary,
    translation: translationState(),
    isCheckoutEnable: true,
    configuration: configurationState(),
    isUserLoggedIn: true,
    isTermsConditionChecked: true
  };
  const componentWrapper = (newProps: IBasketSummaryProps) => {
    return mount(
      <ThemeProvider theme={{}}>
        <BasketSummary {...newProps} />
      </ThemeProvider>
    );
  };
  test('BasketSummary should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('BasketSummary should render properly without summary price', () => {
    const newSummary = { ...BASKET_ALL_ITEM.cartSummary };
    newSummary[0].totalPrice = 0;
    newSummary[1].totalPrice = 0;
    const newProps = { ...props };
    newProps.cartSummary = newSummary;
    newProps.hideCheckoutButton = true;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('SummaryShell should render properly', () => {
    // tslint:disable-next-line:no-any
    const newProps: any = {};
    const component = mount(
      <ThemeProvider theme={{}}>
        <SummaryShell {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('Proceed to checkout when user is online', () => {
    const newProps: IBasketSummaryProps = {
      ...props,
      isCheckoutSummary: false,
      isUserLoggedIn: true
    };
    const component = componentWrapper(newProps);
    component
      .find({
        size: 'medium'
      })
      .at(0)
      .simulate('click');
    (component
      .find('BasketSummary')
      .instance() as BasketSummary).proceedToCheckout(event);
  });

  test('Proceed to checkout when isUserLoggedIn is true', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;
    const newProps: IBasketSummaryProps = {
      ...props,
      hideCheckoutButton: false,
      isCheckoutSummary: true
    };
    const component = componentWrapper(newProps);
    (component
      .find('BasketSummary')
      .instance() as BasketSummary).proceedToCheckout(event);
    (component.find('BasketSummary').instance() as BasketSummary).onSwipe();
  });

  test('Proceed to checkout when isUserLoggedIn is false', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;
    const newProps: IBasketSummaryProps = { ...props, isUserLoggedIn: false };
    const component = componentWrapper(newProps);
    (component
      .find('BasketSummary')
      .instance() as BasketSummary).proceedToCheckout(event);
  });
});
