import {
  SlideArea,
  SummaryContent,
  SummaryContentCompact,
  SummaryContentCompactWrapper,
  SummaryIconTop,
  SummaryWrapper,
  WrapperDiv
} from '@basket/BasketSummary/styles';
import React, { Component, ReactNode, SyntheticEvent } from 'react';
import { ICartSummary } from '@basket/store/types';
import { StyledColumn, StyledRow } from '@basket/BasketSummary/Shared/styles';
import { ITranslationState } from '@store/types/translation';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@store/types/configuration';
import { Button, Icon, Section, Title, Utils } from 'dt-components';
import { isMobile } from '@src/common/utils';
import Drawer from 'react-drag-drawer';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import FooterWithTermsAndConditions from '@common/components/FooterWithTermsAndConditions';
import EVENT_NAME from '@events/constants/eventName';

import SummaryUpfront from './SummaryUpfront';
import SummaryMonthly from './SummaryMonthly';
import SummaryPaymentMethods from './SummaryPaymentMethods';
export interface IProps extends RouteComponentProps<IRouterParams> {
  translation: ITranslationState;
  currency: ICurrencyConfiguration;
  cartSummary: ICartSummary[];
  isCheckoutEnable: boolean;
  isCheckoutSummary?: boolean;
  hideCheckoutButton?: boolean;
  configuration: IConfigurationState;
  isUserLoggedIn: boolean;
  isTermsConditionChecked: boolean;
  goToLogin(isRoutableFromLogin: boolean): void;
  placeOrderButton?(): void;
}

export interface IState {
  drawerOpen: boolean;
}
export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export class BasketSummary extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.onSwipe = this.onSwipe.bind(this);
    this.setModalEl = this.setModalEl.bind(this);
    this.openDrawer = Utils.debounce(this.openDrawer, 200).bind(this);
    this.closeDrawer = Utils.debounce(this.closeDrawer, 200).bind(this);
    this.state = {
      drawerOpen: false
    };

    this.proceedToCheckout = this.proceedToCheckout.bind(this);
  }

  setModalEl(el: HTMLElement): void {
    el.style.width = '100%';
    el.style.bottom = '0%';
    el.style.position = 'fixed';
  }

  openDrawer(event: SyntheticEvent): void {
    event.persist();
    this.setState(
      {
        drawerOpen: true
      },
      () => {
        document.body.style.overflow = 'hidden';
      }
    );
  }

  closeDrawer(event: SyntheticEvent): void {
    event.persist();
    this.setState(
      {
        drawerOpen: false
      },
      () => {
        document.body.style.overflow = '';
      }
    );
  }

  // tslint:disable-next-line:no-identical-functions
  onSwipe(): void {
    this.setState(
      {
        drawerOpen: !this.state.drawerOpen
      },
      () => {
        document.body.style.overflow = this.state.drawerOpen ? 'hidden' : '';
      }
    );
  }

  proceedToCheckout(event: SyntheticEvent): void {
    event.preventDefault();
    event.stopPropagation();
    const { placeOrderButton, history } = this.props;
    const { isCheckoutSummary, isUserLoggedIn, goToLogin } = this.props;
    if (!isCheckoutSummary) {
      if (isUserLoggedIn) {
        history.push('/checkout/personal-info');
      } else {
        goToLogin(true);
      }
    } else {
      if (placeOrderButton) {
        placeOrderButton();
      }
    }
  }

  render(): ReactNode {
    const { drawerOpen } = this.state;
    const {
      isCheckoutEnable,
      hideCheckoutButton,
      isTermsConditionChecked,
      currency
    } = this.props;
    const { isCheckoutSummary } = this.props;
    const { cartSummary, translation, configuration } = this.props;
    const { basket: basketTranslation } = this.props.translation.cart;
    const navigatorKey = isMobile.phone ? 'mobile' : 'desktop';

    const summaryContentCompact = (
      <>
        <StyledRow>
          <StyledColumn>
            <Section size='large' transform='uppercase'>
              {basketTranslation.totalUpFront}
            </Section>
          </StyledColumn>
          <StyledColumn>
            <Section size='large'>
              {cartSummary[0].totalPrice ? cartSummary[0].totalPrice : 0} Ft
            </Section>
          </StyledColumn>
        </StyledRow>
        <StyledRow>
          <StyledColumn>
            <Section size='large' transform='uppercase'>
              {basketTranslation.totalMonthly}
            </Section>
          </StyledColumn>
          <StyledColumn>
            <Section size='large'>
              {cartSummary[1].totalPrice ? cartSummary[1].totalPrice : 0} Ft
            </Section>
          </StyledColumn>
        </StyledRow>
      </>
    );

    return (
      <WrapperDiv>
        <SummaryContentCompactWrapper
          openDrawer={drawerOpen}
          onClick={this.openDrawer}
        >
          <SummaryContentCompact className='summeryCompactInnerText'>
            {summaryContentCompact}
          </SummaryContentCompact>
          {hideCheckoutButton ? null : (
            <div className='buttonWrap'>
              <Button
                className='button'
                size='medium'
                disabled={isCheckoutEnable}
                loading={false}
                onClickHandler={this.proceedToCheckout}
                type='primary'
                data-event-id={
                  isCheckoutSummary
                    ? EVENT_NAME.BASKET.EVENTS.PLACE_ORDER
                    : EVENT_NAME.BASKET.EVENTS.PROCEED_TO_CHECKOUT
                }
                data-event-message={
                  isCheckoutSummary
                    ? translation.cart.checkout.orderReview.placeOrder
                    : basketTranslation.proceedToCheckout
                }
              >
                {isCheckoutSummary
                  ? translation.cart.checkout.orderReview.placeOrder
                  : basketTranslation.proceedToCheckout}
              </Button>
            </div>
          )}
        </SummaryContentCompactWrapper>

        <Drawer
          getModalRef={this.setModalEl}
          modalElementClass='drawer'
          open={drawerOpen}
          onRequestClose={this.onSwipe}
        >
          <SummaryWrapper
            isMobile={true}
            isSticky={
              configuration.cms_configuration.global.stickyOrderSummary[
                navigatorKey
              ]
            }
          >
            <SlideArea>
              <SummaryContent className={isMobile.phone ? 'summaryMobile' : ''}>
                <SummaryIconTop>
                  <Icon
                    name='ec-solid-expand'
                    size='medium'
                    color={'currentColor'}
                    onClick={this.closeDrawer}
                  />
                </SummaryIconTop>
                <Title className='heading' size='small' weight='ultra'>
                  {basketTranslation.summaryHeading}
                </Title>
                <SummaryUpfront
                  translation={translation}
                  cartSummary={cartSummary[0]}
                  key={0}
                  currency={currency}
                />
                <SummaryMonthly
                  translation={translation}
                  cartSummary={cartSummary[1]}
                  key={1}
                  currency={currency}
                />
              </SummaryContent>
              <SummaryPaymentMethods
                availablePaymentMethods={
                  configuration.cms_configuration.global.paymentMethods
                }
              />
            </SlideArea>
            {hideCheckoutButton ? null : isCheckoutSummary ? (
              <Button
                className='button'
                size='medium'
                disabled={isTermsConditionChecked}
                loading={false}
                onClickHandler={this.proceedToCheckout}
                data-event-id={EVENT_NAME.BASKET.EVENTS.PLACE_ORDER}
                data-event-message={
                  translation.cart.checkout.orderReview.placeOrder
                }
                type='primary'
              >
                {translation.cart.checkout.orderReview.placeOrder}
              </Button>
            ) : (
              <Button
                className='button'
                size='medium'
                disabled={isCheckoutEnable}
                loading={false}
                onClickHandler={this.proceedToCheckout}
                type='primary'
                data-event-id={EVENT_NAME.BASKET.EVENTS.PROCEED_TO_CHECKOUT}
                data-event-message={basketTranslation.proceedToCheckout}
              >
                {basketTranslation.proceedToCheckout}
              </Button>
            )}
          </SummaryWrapper>
          <FooterWithTermsAndConditions
            className='checkoutMobileFooterDrawer'
            termsAndConditionsUrl={
              this.props.configuration.cms_configuration.global
                .termsAndConditionsUrl
            }
            shouldTermsAndConditionsOpenInNewTab={
              this.props.configuration.cms_configuration.global
                .shouldTermsAndConditionsOpenInNewTab
            }
            globalTranslation={this.props.translation.cart.global}
          />
        </Drawer>
      </WrapperDiv>
    );
  }
}

export default withRouter(BasketSummary);
