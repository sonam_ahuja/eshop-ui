import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

import { StyledSummaryUpFront } from './SummaryUpfront';
import { StyledSummaryMonthly } from './SummaryMonthly';

export const SlideArea = styled.div``;
export const WrapperDiv = styled.div`
  .drawer {
    width: 100%;
  }
`;
export const SummaryIconTop = styled.div`
  width: 100%;
  text-align: center;
  padding-top: 0.1rem;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 3;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: none;
  }
`;

export const SummaryContentCompact = styled.div`
  width: 100%;
`;

export const SummaryContent = styled.div`
  text-align: right;
  background: #262626;
  width: 100%;
  overflow: auto;

  &.summaryMobile {
    .heading {
      text-align: left;
    }
  }

  div[orientation] {
    margin: 0.5rem 0 1rem 0;
    color: ${colors.coldGray};
    opacity: 0.8;

    &:before {
      transform: scale(0.7);
      width: 200%;
      left: -50%;
    }
  }

  .heading {
    padding-top: 0;
  }

  ${StyledSummaryMonthly} {
    padding-top: 3.5rem;
    overflow: hidden;
  }
  ${StyledSummaryUpFront} {
    padding-top: 1.75rem;
    overflow: hidden;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    height: 100%;
    overflow: auto;
  }
`;

export const SummaryContentCompactWrapper = styled.div<{ openDrawer: boolean }>`
  width: 100%;
  position: fixed;
  bottom: 0;
  left: 0;
  padding-bottom: 5rem;
  height: auto;
  border-radius: 0.5rem 0.5rem 0 0;
  padding: 1.5rem 2.25rem 0;
  background: ${colors.charcoalGray};
  color: ${colors.mediumGray};
  z-index: 3;

  transition: opacity 0.2s linear 0s;
  opacity: ${({ openDrawer }) => (openDrawer ? '0' : '1')};

  &:after {
    content: '';
    width: 3.125em;
    background: ${colors.steelGray};
    height: 0.1875rem;
    border-radius: 0.1rem;
    position: absolute;
    left: 0;
    right: 0;
    top: 0.5rem;
    margin: auto;
  }

  .summeryCompactInnerText {
    margin-bottom: 1rem;
  }

  .buttonWrap {
    margin: 0 -2.25rem;
    .button {
      width: 100%;
    }
  }
`;

export const SummaryWrapper = styled.div<{
  isMobile: boolean;
  isSticky?: boolean;
}>`
  position: relative;
  max-height: 100vh;

  display: flex;
  flex-direction: column;

  ${SlideArea} {
    height: 100%;
    background: ${colors.charcoalGray};
    color: ${colors.mediumGray};
    display: flex;
    position: relative;
    padding: 4rem 2.25rem 8rem;
    transition: 0.5s linear 0s;
    ${({ isMobile }) => (isMobile ? 'border-radius: 0.5rem 0.5rem 0 0' : '')};

    @media (min-width: ${breakpoints.tabletPortrait}px) {
      border-radius: 0;
    }
  }

  .button {
    width: 100%;
    position: absolute;
    bottom: 0;
    left: 0;
    margin: 2.95rem 0 0 0;
  }

  ${props =>
    props.isSticky
      ? `
      `
      : `margin: 0 -1.25rem`};

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    height: 100vh;
    width: 17.5rem;
    position: fixed;
    top: 0;
    right: 0;
    margin: 0;

    .button {
      position: absolute;
      bottom: 0;
      margin: 0;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: 18rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    ${SlideArea} {
      padding-bottom: 0;
      flex-direction: column;
    }
    .button {
      width: 100%;
      position: static;
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 22.5rem;
  }
`;
