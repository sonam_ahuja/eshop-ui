import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';
import {
  BASKET_ALL_ITEM,
  BASKET_RESPONSE_WITH_OUTSTOCK_PRODUCT
} from '@mocks/basket/basket.mock';

import Addon, { IProps as IAddonProps } from '.';

describe('<Addon />', () => {
  test('should render properly', () => {
    const props: IAddonProps = {
      basketItem: BASKET_ALL_ITEM.cartItems[0],
      currency: 'INR',
      translation: translationState().cart.basket,
      setDeleteBasketItemModal: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Addon {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('Should render properly when item is out of stock', () => {
    const onClickDelete = jest.spyOn(Addon.prototype, 'clickDelete');
    const props: IAddonProps = {
      basketItem: BASKET_RESPONSE_WITH_OUTSTOCK_PRODUCT.cartItems[0],
      currency: 'INR',
      translation: translationState().cart.basket,
      setDeleteBasketItemModal: jest.fn()
    };

    const component = mount<Addon>(
      <ThemeProvider theme={{}}>
        <Addon {...props} />
      </ThemeProvider>
    );

    (component.find('Addon').instance() as Addon).clickDelete(
      BASKET_RESPONSE_WITH_OUTSTOCK_PRODUCT.cartItems[0]
    );
    expect(onClickDelete).toHaveBeenCalled();
  });
});
