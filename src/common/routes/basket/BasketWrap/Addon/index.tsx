import React, { Component, ReactNode } from 'react';
import { Anchor, Paragraph } from 'dt-components';
import PreloadImage from '@src/common/components/Preload';
import {
  IBasketItem,
  ISetDeleteBasketItemModalPayload
} from '@basket/store/types';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { ITEM_AVAILABILITY_STATUS } from '@basket/store/enums';
import { getInstallmentsText } from '@basket/utils/installmentType';
import { CMS_REPLACEABLE_KEY } from '@common/constants/appConstants';
import appConstants from '@src/common/constants/appConstants';

import {
  OutOfStockText,
  StyleAddon,
  StyledAmount,
  StyledContent
} from './styles';

export interface IProps {
  basketItem: IBasketItem;
  translation: IBasketTranslation;
  currency: string;
  setDeleteBasketItemModal(payload: ISetDeleteBasketItemModalPayload): void;
}

export interface IState {
  isOpen: boolean;
  currentItem?: IBasketItem;
}

class Addon extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
  }

  clickDelete(selectedItem: IBasketItem): void {
    this.props.setDeleteBasketItemModal({
      showModal: true,
      selectedItem,
      resetModalLoading: false
    });
  }

  render(): ReactNode {
    const { basketItem, translation: basketTranslation, currency } = this.props;

    return (
      <>
        <StyleAddon>
          <div className='addonProductImage'>
            <PreloadImage
              imageUrl={basketItem.product.imageUrl as string}
              imageHeight={10}
              imageWidth={10}
            />
          </div>
          {basketItem.status !== ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK ? (
            <StyledContent>
              <Paragraph
                className='title'
                size='large'
                weight='medium'
                transform='capitalize'
              >
                {basketItem.product.name}
              </Paragraph>

              <StyledAmount>
                <Paragraph size='small'>
                  {basketItem.product.shortDescription}
                </Paragraph>
                <Paragraph size='small'>
                  {getInstallmentsText(basketItem, basketTranslation, currency)}
                </Paragraph>
              </StyledAmount>

              {basketItem.isModifiable && (
                <Anchor
                  size='medium'
                  className='action'
                  hreflang={appConstants.LANGUAGE_CODE}
                  title={basketTranslation.remove}
                  onClickHandler={this.clickDelete.bind(this, basketItem)}
                >
                  <Paragraph size='small'>{basketTranslation.remove}</Paragraph>
                </Anchor>
              )}
            </StyledContent>
          ) : (
            <OutOfStockText>
              <Paragraph size='large' weight='medium'>
                {basketTranslation.sorryText.replace(
                  CMS_REPLACEABLE_KEY,
                  basketItem.product.name
                )}{' '}
                <strong>{basketTranslation.outOfStockText}</strong>
              </Paragraph>

              {basketItem.isModifiable && (
                <Anchor
                  hreflang={appConstants.LANGUAGE_CODE}
                  title={basketTranslation.remove}
                  onClickHandler={this.clickDelete.bind(this, basketItem)}
                >
                  <Paragraph size='small'>{basketTranslation.remove}</Paragraph>
                </Anchor>
              )}
            </OutOfStockText>
          )}
        </StyleAddon>
      </>
    );
  }
}

export default Addon;
