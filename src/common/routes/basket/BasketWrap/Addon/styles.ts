import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const StyleAddon = styled.div`
  background: ${colors.coldGray};
  border-radius: 0.25rem;
  margin-top: 0.25rem;
  display: flex;
  width: 100%;
  padding: 1rem 1.25rem;
  color: ${colors.ironGray};

  .addonProductImage {
    max-width: 52px;
    min-width: 52px;
    max-height: 52px;

    margin-right: 1.3rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 1rem 0.7rem 1rem 1.5rem;

    .addonProductImage {
      max-width: 65px;
      min-width: 65px;
      max-height: 65px;

      margin-right: 1.75rem;
    }
  }
`;

export const StyledContent = styled.div`
  position: relative;

  .title {
    color: ${colors.darkGray};
    margin-bottom: 0.5rem;
  }

  .action {
    padding-top: 1.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    align-items: flex-start;
    width: 100%;

    .title {
      min-width: 8rem;
      max-width: 8rem;
      margin-right: 1.75rem;
    }

    .action {
      width: 100%;
      padding-top: 0.25rem;
      padding-right: 0.65rem;
      justify-content: flex-end;
    }
  }
`;

export const OutOfStockText = styled.div`
  color: ${colors.mediumGray};
  padding-bottom: 1rem;

  strong {
    color: ${colors.darkGray};
  }

  .selectDeviceCta {
    position: absolute;
    bottom: -0.25rem;
  }
`;

export const StyledAmount = styled.div`
  @media (min-width: ${breakpoints.desktop}px) {
    padding-top: 0.25rem;
    min-width: 9.5rem;
    max-width: 9.5rem;
  }
`;
