import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import MonthlyAmount, { IProps as IMonthlyAmountProps } from '.';

describe('<MonthlyAmount />', () => {
  test('should render properly', () => {
    const props: IMonthlyAmountProps = {
      currency: 'INR',
      amount: 230,
      isUppercase: false,
      translation: appState().translation.cart.basket
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <MonthlyAmount {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
