import React, { FunctionComponent } from 'react';
import { InstallmentAmount, Title } from 'dt-components';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import {
  getCurrencySymbol,
  getFormatedPriceValue
} from '@common/utils/currency';

import { StyledMonthly } from './styles';

export interface IProps {
  amount: number;
  isUppercase: boolean;
  translation: IBasketTranslation;
  currency: string;
  className?: string;
}

export const MonthlyAmount: FunctionComponent<IProps> = (props: IProps) => {
  const { amount, currency } = props;
  const { translation: basketTranslation } = props;

  return (
    <StyledMonthly className='monthly-lists'>
      <div className='monthlyLabel'>{basketTranslation.monthly}</div>
      <Title size='small' weight='bold'>
        <InstallmentAmount
          amount={getFormatedPriceValue(amount, currency)}
          size='small'
          currencyName={getCurrencySymbol(currency)}
        />
      </Title>
    </StyledMonthly>
  );
};

export default MonthlyAmount;
