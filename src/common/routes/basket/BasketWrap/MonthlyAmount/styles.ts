import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const StyledMonthly = styled.div`
  color: ${colors.magenta};
  min-width: 6.5rem;
  max-width: 6.5rem;
  margin-right: 1.25rem;

  .monthlyLabel {
    font-size: 0.625rem;
    line-height: 1.2;
    letter-spacing: 0.3px;
    margin-bottom: 0.25rem;
    text-transform: uppercase;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-bottom: 2rem;
  }
`;
