import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import UpfrontAmount, { IProps as IUpfrontAmountProps } from '.';

describe('<UpfrontAmount />', () => {
  test('should render properly', () => {
    const props: IUpfrontAmountProps = {
      currency: 'INR',
      amount: 230,
      isUppercase: false,
      translation: appState().translation.cart.basket
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <UpfrontAmount {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
