import React, { FunctionComponent } from 'react';
import { InstallmentAmount, Title } from 'dt-components';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import {
  getCurrencySymbol,
  getFormatedPriceValue
} from '@common/utils/currency';

import { StyledUpfront } from './styles';

export interface IProps {
  amount: number;
  isUppercase: boolean;
  translation: IBasketTranslation;
  currency: string;
  className?: string;
}

const UpfrontAmount: FunctionComponent<IProps> = (props: IProps) => {
  const { translation: basketTranslation } = props;

  return (
    <StyledUpfront className='upfront-lists'>
      <div className='upfrontLabel'>{basketTranslation.cardUpfrontHeading}</div>
      <Title size='xsmall'>
        <InstallmentAmount
          amount={getFormatedPriceValue(props.amount, props.currency)}
          size='small'
          currencyName={getCurrencySymbol(props.currency)}
        />
      </Title>
    </StyledUpfront>
  );
};

export default UpfrontAmount;
