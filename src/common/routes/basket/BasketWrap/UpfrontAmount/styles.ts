import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const StyledUpfront = styled.div`
  color: ${colors.ironGray};
  min-width: 6.5rem;
  max-width: 6.5rem;
  margin-right: 1.25rem;

  .upfrontLabel {
    font-size: 0.625rem;
    line-height: 1.2;
    letter-spacing: 0.3px;
    text-transform: uppercase;
    margin-bottom: 0.5rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-bottom: 2rem;

    .upfrontLabel {
      margin-bottom: 0.25rem;
    }
  }
`;
