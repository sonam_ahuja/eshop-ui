import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints, colors } from '@src/common/variables';

const StyledBreadcrumb = styled.div`
  .breadCrumb {
    height: 1rem;
    width: 8.2rem;
    margin-top: 1.25rem;
    margin-bottom: 1.25rem;
  }
`;

const StyledTitlePanel = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2rem;
  align-items: flex-end;
`;

const DataLeftPanel = styled.div`
  .title {
    height: 3.5rem;
    width: 9rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .title {
      width: 14rem;
    }
  }
`;

const DataRightPanel = styled.div`
  .link {
    height: 1rem;
    width: 4rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .link {
      width: 8rem;
    }
  }
`;

const StyledCards = styled.div`
  .shine {
    height: 13.1875rem;
    margin-bottom: 1rem;
  }
`;

const StyledBasketWrapShell = styled(StyledShell)`
  width: 100%;
  background: ${colors.fogGray};
`;
const BasketWrapShell = () => {
  return (
    <StyledBasketWrapShell className='primary'>
      <StyledBreadcrumb>
        <div className='shine breadCrumb' />
      </StyledBreadcrumb>
      <StyledTitlePanel>
        <DataLeftPanel>
          <div className='shine title' />
        </DataLeftPanel>
        <DataRightPanel>
          <div className='shine link' />
        </DataRightPanel>
      </StyledTitlePanel>
      <StyledCards>
        <div className='shine' />
      </StyledCards>
      <StyledCards>
        <div className='shine' />
      </StyledCards>
    </StyledBasketWrapShell>
  );
};
export default BasketWrapShell;
