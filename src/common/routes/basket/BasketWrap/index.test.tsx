import React from 'react';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@store/states/translation';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@basket/BasketWrap/mapProps';
import { IComponentProps as IBasketWrapProps } from '@basket/BasketWrap';
import constants from '@basket/store/constants';
import { IRemoveBasketItemParams } from '@basket/store/types';
import { StaticRouter } from 'react-router-dom';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import { BasketWrap } from '.';
import BasketWrapShell from './index.shell';

describe('<BasketWrap />', () => {
  const newbasket = { ...appState().basket };
  const props: IBasketWrapProps = {
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    basket: newbasket,
    isStepperDisable: true,
    showMoreFlag: true,
    configuration: appState().configuration,
    cartId: '1212',
    basketItems: BASKET_ALL_ITEM.cartItems,
    removeBasketItemModalState: {
      modalOpen: false,
      error: null,
      loading: true,
      selectedItem: BASKET_ALL_ITEM.cartItems[0]
    },
    removeBasketItem: jest.fn(),
    updateItemQuantity: jest.fn(),
    setDeleteBasketItemModal: jest.fn(),
    translation: translationState().cart.basket,
    undoAbleItems: BASKET_ALL_ITEM.cartItems,
    clearRemoveBasketItemError: jest.fn()
  };
  test('BasketWrap should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketWrap {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('BasketWrapShell should render properly', () => {
    const newpProps = {};
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketWrapShell {...newpProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('Modal hide and show simulate via close', () => {
    const onClose = jest.spyOn(BasketWrap.prototype, 'close');
    const onRemove = jest.spyOn(BasketWrap.prototype, 'remove');
    const clickDelete = jest.spyOn(BasketWrap.prototype, 'clickDelete');
    const basketItemArray = BASKET_ALL_ITEM.cartItems;
    basketItemArray[0].group = 'bundle';
    const newpProps = { ...props };
    newpProps.basketItems = basketItemArray;
    const component = mount<BasketWrap>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketWrap {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('BasketWrap').instance() as BasketWrap).renderStepper(
      BASKET_ALL_ITEM.cartItems[0]
    );
    (component.find('BasketWrap').instance() as BasketWrap).close();
    expect(onClose).toHaveBeenCalled();
    (component.find('BasketWrap').instance() as BasketWrap).remove(
      BASKET_ALL_ITEM.cartItems[0]
    );
    expect(onRemove).toHaveBeenCalled();
    (component.find('BasketWrap').instance() as BasketWrap).remove(null);
    expect(onRemove).toHaveBeenCalled();

    (component.find('BasketWrap').instance() as BasketWrap).clickDelete(
      BASKET_ALL_ITEM.cartItems[0]
    );
    expect(clickDelete).toHaveBeenCalled();

    const cartItemWithoutId = { ...BASKET_ALL_ITEM.cartItems[0] };
    delete cartItemWithoutId.id;

    (component.find('BasketWrap').instance() as BasketWrap).remove(
      cartItemWithoutId
    );
    expect(onRemove).toHaveBeenCalled();
    (component.find('BasketWrap').instance() as BasketWrap).onKeyDownDelete(
      // tslint:disable-next-line: no-object-literal-type-assertion
      { keyCode: 13 } as React.KeyboardEvent<Element>,
      BASKET_ALL_ITEM.cartItems[0]
    );
    (component.find('BasketWrap').instance() as BasketWrap).onKeyDownDelete(
      // tslint:disable-next-line: no-object-literal-type-assertion
      { keyCode: 12 } as React.KeyboardEvent<Element>,
      BASKET_ALL_ITEM.cartItems[0]
    );
    (component.find('BasketWrap').instance() as BasketWrap).updateItemQuantity(
      BASKET_ALL_ITEM.cartItems[0]
    )(2);
  });

  test('Modal hide and show ', () => {
    const basketItemArray = BASKET_ALL_ITEM.cartItems;
    basketItemArray[0].group = 'bundle';
    const newProps = { ...props, showMoreFlag: false };
    newProps.basketItems = basketItemArray;
    newProps.basket.isCheckoutEnable = false;
    const component = mount<BasketWrap>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BasketWrap {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    const initStateValue = appState();
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const removeBasketItemData: IRemoveBasketItemParams = {
      cartItems: []
    };
    const removeBasketItemExpected = {
      type: constants.REMOVE_BASKET_ITEM,
      payload: { cartItems: [] }
    };
    const updateItemQuantityExpected = {
      type: constants.UPDATE_BASKET_ITEM_QUANTITY,
      payload: {
        id: '3',
        cartItems: [{ id: '1234', quantity: 2 }]
      }
    };

    const clearRemoveBasketItemErrorExpected = {
      type: constants.CLEAR_REMOVE_BASKET_ITEM_ERROR
    };

    mapDispatchToProps(dispatch).removeBasketItem(removeBasketItemData);
    mapDispatchToProps(dispatch).updateItemQuantity(
      updateItemQuantityExpected.payload
    );
    mapDispatchToProps(dispatch).setDeleteBasketItemModal({
      showModal: true,
      selectedItem: null,
      resetModalLoading: false
    });
    mapDispatchToProps(dispatch).clearRemoveBasketItemError();
    expect(dispatch.mock.calls[0][0]).toEqual(removeBasketItemExpected);
    expect(dispatch.mock.calls[1][0]).toEqual(updateItemQuantityExpected);
    expect(dispatch.mock.calls[2][0]).toBeDefined();
    expect(dispatch.mock.calls[3][0]).toEqual(
      clearRemoveBasketItemErrorExpected
    );
  });
});
