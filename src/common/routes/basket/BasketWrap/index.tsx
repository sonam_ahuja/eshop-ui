import { ITEM_AVAILABILITY_STATUS, ITEM_TYPE } from '@basket/store/enums';
import { Icon, Stepper } from 'dt-components';
import { IBasketItem } from '@basket/store/types';
import { RootState } from '@src/common/store/reducers';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';
import {
  IComponentDispatchToProps,
  IComponentStateToProps,
  IProps,
  IState
} from '@basket/BasketWrap/types';
import { connect } from 'react-redux';
import React, { Component, ReactNode } from 'react';
import {
  ActionsWrap,
  Bundle,
  CardHolder,
  CardsWrap,
  DeleteIconWrap,
  ItemTotalPrice
} from '@basket/BasketWrap/styles';
// tslint:disable
import { BASKET_API_ACTION, ITEM_PRICE_TYPE } from '@basket/store/enums';
import { colors } from '@common/variables';
import BasketItemDeleteModal from '@basket/BasketItemDeleteModal';
import ItemCard from '@basket/BasketItemCard';
import Addon from '@basket/BasketWrap/Addon';
import MonthlyAmount from '@basket/BasketWrap/MonthlyAmount';
import UpfrontAmount from '@basket/BasketWrap/UpfrontAmount';
import InformationText from '@src/common/components/InformationText';
import { getPriceTypeIndex } from '@basket/utils/getPriceTypeIndex';
import {
  mapDispatchToProps,
  mapStateToProps
} from '@basket/BasketWrap/mapProps';
import { KEY_CODE } from '@src/common/constants/appConstants';
import {
  sendBasketDeleteItemEvent,
  sendClickOnRemoveButtonEvent,
  sendOnBasketRemoveParticulars
} from '@events/basket';
import { sendCTAClicks } from '@events/index';
import EVENT_NAME from '@events/constants/eventName';
import Loadable from 'react-loadable';

export type IComponentProps = IProps &
  IComponentStateToProps &
  IComponentDispatchToProps;

const EmptyBasketComponent = Loadable({
  loading: () => null,
  loader: () =>
    import('@basket/EmptyBasket')
});

export class BasketWrap extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);

    this.remove = this.remove.bind(this);
    this.close = this.close.bind(this);
    this.onKeyDownDelete = this.onKeyDownDelete.bind(this);
    this.isPreOrder = this.isPreOrder.bind(this);
  }

  componentDidMount(): void {
    EmptyBasketComponent.preload();
  }

  isPreOrder(status: string): boolean {
    return status === ITEM_AVAILABILITY_STATUS.PRE_ORDER;
  }

  renderItems(basketItem: IBasketItem): ReactNode {
    const getItemAvailablityStatus = (status: string) =>
      status === ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK;

    const { translation, currency } = this.props;
    const items: ReactNode[] = [];
    const addOns: ReactNode[] = [];

    items.push(
      <ItemCard
        key={basketItem.id}
        currency={currency.locale}
        basketItem={basketItem}
        fullwidth={true}
        outOfStock={getItemAvailablityStatus(basketItem.status)}
        basketTranslation={translation}
        isPreOrder={this.isPreOrder(basketItem.status)}
      />
    );

    if (basketItem.cartItems && basketItem.cartItems.length) {
      basketItem.cartItems.forEach((item: IBasketItem) => {
        const isStocked: boolean = getItemAvailablityStatus(item.status);

        if (item.group === ITEM_TYPE.BUNDLE_PRODUCT) {
          items.push(
            <ItemCard
              key={item.id}
              basketItem={item}
              currency={currency.locale}
              fullwidth={false}
              outOfStock={isStocked}
              basketTranslation={translation}
              isPreOrder={this.isPreOrder(basketItem.status)}
            />
          );
        } else {
          if (
            basketItem.group &&
            basketItem.group.toLowerCase() !== ITEM_TYPE.TARIFF
          ) {
            addOns.push(
              <Addon
                key={item.id}
                currency={currency.locale}
                basketItem={item}
                translation={translation}
                setDeleteBasketItemModal={this.props.setDeleteBasketItemModal}
              />
            );
          }
        }
      });
    }

    return (
      <>
        <CardsWrap>{items} </CardsWrap>
        {addOns}
      </>
    );
  }

  updateItemQuantity(basketItem: IBasketItem): (count: number) => void {
    return (quantity: number) => {
      const id = basketItem.id;
      const payload = {
        cartItems: [
          {
            id,
            quantity
          }
        ]
      };
      this.props.updateItemQuantity(payload);
      sendCTAClicks(EVENT_NAME.BASKET.EVENTS.STEPPER, window.location.href);
    };
  }
  renderStepper = (item: IBasketItem) => {
    return (
      <Stepper
        autoUpdate={false}
        defaultValue={item.quantity as number}
        minRange={1}
        iconMinus="ec-minus"
        iconPlus="ec-plus"
        size="xsmall"
        onPlusClickHandler={this.updateItemQuantity(item)}
        onMinusClickHandler={this.updateItemQuantity(item)}
      />
    );
  };

  close(): void {
    if (this.props.removeBasketItemModalState.modalOpen) {
      this.props.setDeleteBasketItemModal({
        showModal: false,
        selectedItem: null,
        resetModalLoading: false
      });
    }
  }

  remove(currentItem: IBasketItem | null): void {
    if (currentItem) {
      const id = currentItem.id;
      const payload = {
        cartItems: [
          {
            id,
            action: BASKET_API_ACTION.DELETE_ITEM_BASKET,
            group: currentItem.group
          }
        ]
      };
      sendBasketDeleteItemEvent(currentItem.product.id as string);
      this.props.removeBasketItem(payload);
    }
  }

  clickDelete(selectedItem: IBasketItem): void {
    sendClickOnRemoveButtonEvent(
      selectedItem.product.name,
      selectedItem.itemPrice,
      selectedItem.group
    );
    sendOnBasketRemoveParticulars(
      selectedItem.product.name,
      selectedItem.itemPrice,
      selectedItem.group
    );
    this.props.setDeleteBasketItemModal({
      showModal: true,
      selectedItem,
      resetModalLoading: false
    });
  }

  onKeyDownDelete(
    event: React.KeyboardEvent<Element>,
    selectedItem: IBasketItem
  ): void {
    if (event.keyCode === KEY_CODE.ENTER) {
      this.clickDelete(selectedItem);
    }
  }

  renderItemCard(item: IBasketItem): ReactNode {
    const { configuration, translation, currency } = this.props;
    const monthlyIndex = getPriceTypeIndex(
      item.totalPrice,
      ITEM_PRICE_TYPE.RECURRING_FEE
    );
    const upfrontIndex = getPriceTypeIndex(
      item.totalPrice,
      ITEM_PRICE_TYPE.UPFRONT
    );

    return (
      <CardHolder key={item.id}>
        <Bundle>{this.renderItems(item)}</Bundle>
        <ItemTotalPrice>
          {item.totalPrice &&
          item.totalPrice.length &&
          monthlyIndex !== -1 &&
          item.totalPrice[monthlyIndex].totalPrice ? (
            <MonthlyAmount
              amount={item.totalPrice[monthlyIndex].totalPrice}
              isUppercase={true}
              translation={translation}
              currency={currency.locale}
            />
          ) : null}
          {item.totalPrice && item.totalPrice.length ? (
            <UpfrontAmount
              amount={
                upfrontIndex !== -1 && item.totalPrice[upfrontIndex].totalPrice
                  ? item.totalPrice[upfrontIndex].totalPrice
                  : 0
              }
              isUppercase={true}
              translation={translation}
              currency={currency.locale}
            />
          ) : null}
          {this.props.isStepperDisable ||
          (item.product.categoryId ===
            configuration.cms_configuration.modules.checkout.mnp.categoryId ||
            item.product.categoryId ===
              configuration.cms_configuration.modules.checkout.mns
                .categoryId) ? null : (
            <>
              {item.isModifiable && (
                <ActionsWrap>
                  {/* {item.status === ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK ||
                  (item.group && item.group.toLowerCase() === ITEM_TYPE.TARIFF)
                    ? null
                    : this.renderStepper(item)} */}
                  <DeleteIconWrap
                    tabIndex={0}
                    onClick={this.clickDelete.bind(this, item)}
                    onKeyDown={(event: React.KeyboardEvent<Element>) => {
                      this.onKeyDownDelete(event, item);
                    }}
                  >
                    <Icon
                      name="ec-solid-trash"
                      size="xsmall"
                      color={colors.mediumGray}
                    />
                  </DeleteIconWrap>
                </ActionsWrap>
              )}
            </>
          )}
        </ItemTotalPrice>
      </CardHolder>
    );
  }

  render(): ReactNode {
    const { translation, showMoreFlag } = this.props;
    let { basketItems, basket } = this.props;
    const { removeBasketItemModalState } = this.props;
    const { isCheckoutEnable } = this.props.basket;
    basketItems = showMoreFlag ? basketItems : basketItems.slice(0, 1);

    return (
      <>
        <AuxiliaryRoute
          isChildrenRender={removeBasketItemModalState.modalOpen}
          hashPath={auxillaryRouteConfig.BASKET.REMOVE}
          isModal={true}
          onClose={this.close}
        >
          <BasketItemDeleteModal
            open={removeBasketItemModalState.modalOpen}
            onClose={this.close}
            onRemove={this.remove.bind(
              this,
              removeBasketItemModalState.selectedItem
            )}
            basketTranslation={translation}
            removeBasketItemModalState={removeBasketItemModalState}
          />
        </AuxiliaryRoute>
        {Array.isArray(basket.basketItems) && basket.basketItems.length ? (
          <>
            {isCheckoutEnable && (
              <InformationText
                description={translation.clearDeviceText}
                keyMessage={translation.outOfStock}
              />
            )}
            {basketItems.map((item: IBasketItem) => this.renderItemCard(item))}
          </>
        ) : (
          <EmptyBasketComponent translation={translation} />
        )}
      </>
    );
  }
}

export default connect<
  IComponentStateToProps,
  IComponentDispatchToProps,
  IProps,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(BasketWrap);
