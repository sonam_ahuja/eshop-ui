import { RootState } from '@common/store/reducers';
import { Dispatch } from 'redux';
import actions from '@basket/store/actions';
import {
  IRemoveBasketItemParams,
  ISetDeleteBasketItemModalPayload,
  IUpdateBasketItemPayload
} from '@basket/store/types';

export const mapStateToProps = (state: RootState) => ({
  currency: state.configuration.cms_configuration.global.currency,
  basket: state.basket,
  cartId: state.basket.cartId,
  removeBasketItemModalState: state.basket.removeBasketItem,
  translation: state.translation.cart.basket,
  configuration: state.configuration,
  undoAbleItems: state.basket.undoAbleItems,
  basketItems: state.basket.basketItems
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  removeBasketItem(payload: IRemoveBasketItemParams): void {
    dispatch(actions.removeBasketItem(payload));
  },
  updateItemQuantity(payload: IUpdateBasketItemPayload): void {
    dispatch(actions.updateBasketItem(payload));
  },
  setDeleteBasketItemModal(payload: ISetDeleteBasketItemModalPayload): void {
    dispatch(actions.setDeleteBasketItemModal(payload));
  },
  clearRemoveBasketItemError(): void {
    dispatch(actions.clearRemoveBasketItemError());
  }
});
