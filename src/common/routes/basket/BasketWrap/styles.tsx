import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const CardHolder = styled.div<{}>`
  display: flex;
  flex-direction: column;
  background: ${colors.white};
  padding: 0.5rem;
  border-radius: 0.5rem;
  margin-bottom: 1rem;
  @media (min-width: ${breakpoints.desktop}px) {
    flex-wrap: nowrap;
    flex-direction: row;
  }
`;
export const Bundle = styled.div`
  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    flex-wrap: wrap;
    width: 100%;
  }
`;
export const CardsWrap = styled.div`
  display: block;
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: flex;
    flex: auto;
    justify-content: space-between;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: 100%;
  }
`;
export const ItemTotalPrice = styled.div`
  padding: 1.25rem 0.75rem 0.63rem 0.75rem;
  display: flex;
  flex-wrap: wrap;
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    width: 100%;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 1.2rem 0.75rem 0.7rem 0.75rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0.75rem 0.75rem 0.5rem;
    max-width: 9.25rem;
    flex-direction: column;
    align-items: flex-end;
    text-align: right;
    > div {
      margin: 0;
      width: 100%;
    }
  }
`;
export const ActionsWrap = styled.div`
  display: flex;
  flex: auto;
  align-items: flex-end;
  margin-top: 1.25rem;

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    margin-top: 1rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 0;
  }
`;
export const DeleteIconWrap = styled.div`
  margin-left: auto;
  margin-bottom: 0.25rem;
  cursor: pointer;
`;
