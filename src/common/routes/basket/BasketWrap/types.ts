import {
  IBasketItem,
  IBasketState,
  IRemoveBasketItemParams,
  ISetDeleteBasketItemModalPayload,
  IUpdateBasketItemPayload
} from '@basket/store/types';
import { IBasket as IBasketTraslation } from '@store/types/translation';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@store/types/configuration';

export interface IProps {
  style?: React.CSSProperties;
  isStepperDisable?: boolean;
  showMoreFlag?: boolean;
}
export interface IComponentStateToProps {
  currency: ICurrencyConfiguration;
  basket: IBasketState;
  basketItems: IBasketItem[];
  cartId: string | null;
  translation: IBasketTraslation;
  configuration: IConfigurationState;
  undoAbleItems: IBasketItem[];
  removeBasketItemModalState: IBasketState['removeBasketItem'];
}

export interface IComponentDispatchToProps {
  removeBasketItem(payload: IRemoveBasketItemParams): void;
  updateItemQuantity(paylod: IUpdateBasketItemPayload): void;
  setDeleteBasketItemModal(payload: ISetDeleteBasketItemModalPayload): void;
  clearRemoveBasketItemError(): void;
}

export interface IState {
  currentItem?: IBasketItem;
}
