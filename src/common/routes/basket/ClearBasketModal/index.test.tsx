import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';

import ClearBasketModal, { IProps as IClearBasketModalProps } from '.';

describe('<ClearBasketModal />', () => {
  test('should render properly', () => {
    const props: IClearBasketModalProps = {
      open: false,
      onClose: jest.fn(),
      onRemove: jest.fn(),
      clearBasketModalState: {
        modalOpen: true,
        error: null,
        loading: false
      },
      translation: translationState().cart.basket
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ClearBasketModal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
