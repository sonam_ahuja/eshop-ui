import React, { FunctionComponent } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { colors } from '@common/variables';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import { IBasketState } from '@basket/store/types';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import EVENT_NAME from '@events/constants/eventName';

export interface IProps {
  open: boolean;
  translation: IBasketTranslation;
  clearBasketModalState: IBasketState['clearBasket'];
  onClose(): void;
  onRemove(): void;
}

const ClearBasketModal: FunctionComponent<IProps> = (props: IProps) => {
  const { onRemove, clearBasketModalState } = props;
  const { open, onClose } = props;
  const basketTranslation = props.translation;

  return (
    <DialogBoxApp
      isOpen={open}
      closeOnBackdropClick={true}
      closeOnEscape={true}
      onEscape={onClose}
      onBackdropClick={onClose}
      backgroundScroll={false}
      type='flexibleHeight'
      onClose={onClose}
    >
      <div className='dialogBoxHeader'>
        <Icon name='ec-solid-trash' size='xlarge' color={colors.magenta} />
      </div>

      <div className='dialogBoxBody'>
        <Title className='infoMessage' size='large' weight='ultra'>
          {basketTranslation.popUps.removeAllItem}
        </Title>
      </div>

      <div className='dialogBoxFooter'>
        <Button
          type='secondary'
          size='medium'
          loading={clearBasketModalState.loading}
          data-event-id={EVENT_NAME.BASKET.EVENTS.CLEAR}
          data-event-message={basketTranslation.popUps.removeButton}
          onClickHandler={onRemove}
        >
          {basketTranslation.popUps.removeButton}
        </Button>
      </div>
    </DialogBoxApp>
  );
};

export default ClearBasketModal;
