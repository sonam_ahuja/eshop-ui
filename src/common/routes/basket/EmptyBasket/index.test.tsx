import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import EmptyBasket from '.';

describe('<EmptyBasket />', () => {
  test('should render properly', () => {
    const props = {
      translation: appState().translation.cart.basket
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <EmptyBasket {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
