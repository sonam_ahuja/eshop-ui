import React, { FunctionComponent } from 'react';
import { Anchor, Title } from 'dt-components';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import appConstants from '@src/common/constants/appConstants';

import { StyledEmptyCard } from './styles';

export interface IBasketContainerIProps {
  style?: React.CSSProperties;
  translation: IBasketTranslation;
}

const EmptyBasket: FunctionComponent<IBasketContainerIProps> = props => {
  const { style } = props;
  const {
    translation: { emptyBasket }
  } = props;

  return (
    <>
      <StyledEmptyCard style={style} className='empty-card-wrap'>
        <Title weight='bold' size='normal'>
          {emptyBasket.emptyBasketText}
        </Title>
        <Title weight='bold' size='normal'>
          {emptyBasket.suggestionText}{' '}
          <Anchor
            hreflang={appConstants.LANGUAGE_CODE}
            title={emptyBasket.bestDealText}
            size='large'
            href={emptyBasket.bestDealLink}
          >
            {emptyBasket.bestDealText}
          </Anchor>
        </Title>
      </StyledEmptyCard>
    </>
  );
};
export default EmptyBasket;
