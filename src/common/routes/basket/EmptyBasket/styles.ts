import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const StyledEmptyCard = styled.div`
  color: ${colors.mediumGray};
  border: 0.1rem dashed ${colors.lightGray};
  background: none;
  padding: 1.9375rem 3.6875rem 1.9375rem 1.25rem;
  border-radius: 0.4rem;
  margin-bottom: 1rem;
  margin-top: 6.05rem;
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 1.9375rem 1.25rem;
  }
`;
