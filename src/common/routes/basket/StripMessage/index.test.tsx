import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

import NotificationBar, { IProps as INotificationBarProps } from '.';

const messageContent = 'Message content';

describe('<NotificationBar />', () => {
  test('should render properly', () => {
    const props: INotificationBarProps = {
      isIconRequired: true,
      undoAbleItems: BASKET_ALL_ITEM.cartItems,
      message: messageContent,
      timer: 3000
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <NotificationBar {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without undoableItems', () => {
    const props: INotificationBarProps = {
      isIconRequired: true,
      undoAbleItems: [],
      message: messageContent,
      timer: 3000
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <NotificationBar {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when item had no id', () => {
    const basketItems = BASKET_ALL_ITEM.cartItems;
    delete basketItems[0].id;
    const props: INotificationBarProps = {
      isIconRequired: true,
      undoAbleItems: basketItems,
      message: messageContent,
      timer: 3000
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <NotificationBar {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
