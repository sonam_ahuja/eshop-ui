import React, { Component, ReactNode } from 'react';
import { Paragraph, StripMessage } from 'dt-components';
import { IBasketItem } from '@basket/store/types';
import { getBasketItemsImages } from '@basket/store/selector';

export interface IProps {
  isIconRequired: boolean;
  undoAbleItems: IBasketItem[];
  message: string;
  itemRemoved?: boolean;
  timer: number;
}

export interface IState {
  isStripVisible: boolean;
}

class NotificationBar extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isStripVisible: true
    };
    this.getStripMessagesWithImages = this.getStripMessagesWithImages.bind(
      this
    );
    this.onStripDismiss = this.onStripDismiss.bind(this);
  }

  onStripDismiss(): void {
    this.setState({
      isStripVisible: !this.state.isStripVisible
    });
  }

  getStripMessagesWithImages(
    productItemImages: string[],
    itemIndex: number
  ): ReactNode {
    const { timer } = this.props;

    return (
      <StripMessage
        key={itemIndex}
        isOpen={true}
        closeOnDismiss={true}
        images={productItemImages}
        itemRemoved={true}
        onDismiss={this.onStripDismiss}
        timer={timer}
        onTimerExpire={this.onStripDismiss}
      >
        <Paragraph size='small'>
          <div>{this.props.message}</div>
        </Paragraph>
      </StripMessage>
    );
  }

  render(): ReactNode {
    const { undoAbleItems, message, isIconRequired } = this.props;
    const { timer } = this.props;
    const { isStripVisible } = this.state;

    return (
      <>
        {undoAbleItems && undoAbleItems.length ? (
          undoAbleItems.map((item: IBasketItem, index: number) => {
            const productItemImages: string[] = getBasketItemsImages(item);

            return this.getStripMessagesWithImages(productItemImages, index);
          })
        ) : (
          <>
            {isStripVisible ? (
              <div className='stripMessageWrapper'>
                <StripMessage
                  timer={timer}
                  onTimerExpire={this.onStripDismiss}
                  icon={isIconRequired ? 'ec-solid-trash' : undefined}
                  isOpen={true}
                  closeOnDismiss={true}
                  itemRemoved={true}
                  onDismiss={this.onStripDismiss}
                >
                  <Paragraph size='small'>{message}</Paragraph>
                </StripMessage>
              </div>
            ) : null}
          </>
        )}
      </>
    );
  }
}

export default NotificationBar;
