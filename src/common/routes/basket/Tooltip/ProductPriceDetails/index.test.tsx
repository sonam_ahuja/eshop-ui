import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import MonthlyToolTip, { IProps } from '@basket/Tooltip/ProductPriceDetails';
import { StaticRouter } from 'react-router';
import appState from '@store/states/app';
import { IItemPrice } from '@basket/store/types';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';

describe('<MonthlyToolTip />', () => {
  const price: IItemPrice[] = [
    {
      priceAlterations: [
        {
          priceType: ITEM_PRICE_TYPE.UPFRONT,
          duration: {
            timePeriod: '12',
            type: 'upfront'
          },
          price: 123
        }
      ],
      price: 12,
      totalPrice: 34,
      priceType: ITEM_PRICE_TYPE.RECURRING_FEE,
      duration: {
        timePeriod: 123,
        type: 'upfornt'
      }
    }
  ];
  const props: IProps = {
    isTariff: false,
    ProductName: 'product',
    itemPrice: price,
    group: '',
    currency: 'INR',
    translation: appState().translation.cart.basket
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <MonthlyToolTip {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    try {
      const component = componentWrapper(props);
      expect(component).toMatchSnapshot();

      (component
        .find('MonthlyToolTip')
        .instance() as MonthlyToolTip).toggleToolTip();
    } catch (err) {
      // tslint:disable-next-line:no-console
      console.log('err', err);
    }
  });
});
