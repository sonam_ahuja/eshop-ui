import React, { Component, ReactNode } from 'react';
import { Icon } from 'dt-components';
import { IItemPrice } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { AppTooltip } from '@src/common/components/Tooltip/styles';
import {
  sendOnBasketToolTipEvent,
  sendOnBasketTooltipParticulars
} from '@events/basket';
import { isMobile } from '@src/common/utils';

import TooltipContent from './tooltipContent';

export interface IProps {
  ProductName: string;
  itemPrice: IItemPrice[];
  translation: IBasket;
  currency: string;
  isTariff: boolean;
  group: string;
}

interface IState {
  tooltipOpen: boolean;
}

class MonthlyToolTip extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tooltipOpen: false
    };
    this.toggleToolTip = this.toggleToolTip.bind(this);
  }

  toggleToolTip(): void {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render(): ReactNode {
    const { itemPrice, isTariff, group } = this.props;
    const { ProductName, translation, currency } = this.props;

    return (
      <AppTooltip
        tooltipStyles={{ zIndex: 1001 }}
        autoClose={true}
        position='top'
        trigger={isMobile.phone ? 'click' : 'hover'}
        onVisibilityChanges={tooltipOpen => {
          this.setState(
            {
              tooltipOpen
            },
            () => {
              if (this.state.tooltipOpen) {
                sendOnBasketToolTipEvent(ProductName, itemPrice, group);
                sendOnBasketTooltipParticulars(ProductName, itemPrice, group);
              }
            }
          );
        }}
        destroyOnTriggerOut={true}
        isOpen={this.state.tooltipOpen}
        tooltipContent={
          <div>
            <TooltipContent
              translation={translation}
              ProductName={ProductName}
              itemPrice={itemPrice}
              currency={currency}
              onClose={this.toggleToolTip}
              isTariff={isTariff}
            />
          </div>
        }
      >
        <span className='iconWrap'>
          <Icon
            onClick={this.toggleToolTip}
            size='inherit'
            color='currentColor'
            name='ec-information'
          />
        </span>
      </AppTooltip>
    );
  }
}

export default MonthlyToolTip;
