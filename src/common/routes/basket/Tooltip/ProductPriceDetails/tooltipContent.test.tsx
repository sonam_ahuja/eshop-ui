import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import appState from '@store/states/app';
import { IItemPrice } from '@basket/store/types';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';

import TooltipContent, { IProps } from './tooltipContent';

describe('<MonthlyToolTip />', () => {
  const price: IItemPrice[] = [
    {
      priceAlterations: [
        {
          priceType: ITEM_PRICE_TYPE.UPFRONT,
          duration: {
            timePeriod: '12',
            type: 'upfront'
          },
          price: 123
        }
      ],
      price: 12,
      totalPrice: 34,
      priceType: ITEM_PRICE_TYPE.UPFRONT,
      duration: {
        timePeriod: 123,
        type: 'upfornt'
      }
    },
    {
      priceAlterations: [
        {
          priceType: ITEM_PRICE_TYPE.UPFRONT,
          duration: {
            timePeriod: '12',
            type: 'upfront'
          },
          price: 123
        }
      ],
      price: 12,
      totalPrice: 34,
      priceType: ITEM_PRICE_TYPE.RECURRING_FEE,
      duration: {
        timePeriod: 123,
        type: 'upfornt'
      }
    },
    {
      priceAlterations: [
        {
          priceType: ITEM_PRICE_TYPE.UPFRONT,
          duration: {
            timePeriod: '12',
            type: 'upfront'
          },
          price: 123
        }
      ],
      price: 12,
      totalPrice: 34,
      priceType: ITEM_PRICE_TYPE.BASEPRICE,
      duration: {
        timePeriod: 123,
        type: 'upfornt'
      }
    }
  ];
  const props: IProps = {
    ProductName: 'product',
    itemPrice: price,
    isTariff: true,
    currency: 'INR',
    translation: appState().translation.cart.basket,
    onClose: jest.fn()
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <TooltipContent {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly with is Tariff false', () => {
    const newProps = { ...props };
    newProps.isTariff = false;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
