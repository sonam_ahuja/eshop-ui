import React from 'react';
import { Divider, Icon, Section } from 'dt-components';
import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { IDuration, IItemPrice, IPriceAlteration } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { findIndexOnKey } from '@src/common/utils/arrayHelper';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';
import basketConstant from '@basket/constants';
import { formatCurrency } from '@common/utils/currency';

export interface IProps {
  isTariff: boolean;
  ProductName: string;
  itemPrice: IItemPrice[];
  currency: string;
  translation: IBasket;
  onClose(): void;
}

const StyledTooltipContent = styled.div`
  color: ${colors.cloudGray};
`;

export default (props: IProps) => {
  const { itemPrice, isTariff } = props;
  const { ProductName, onClose, translation, currency } = props;

  const monthlyIndex = findIndexOnKey(
    itemPrice,
    basketConstant.priceType,
    ITEM_PRICE_TYPE.RECURRING_FEE
  );

  const upfrontIndex = findIndexOnKey(
    itemPrice,
    basketConstant.priceType,
    ITEM_PRICE_TYPE.UPFRONT
  );

  let basePriceIndex = findIndexOnKey(
    itemPrice,
    basketConstant.priceType,
    ITEM_PRICE_TYPE.BASEPRICE
  );

  if (isTariff) {
    basePriceIndex = monthlyIndex;
  }

  return (
    <StyledTooltipContent>
      <Section
        className='titleMain'
        size='large'
        weight='bold'
        transform={'capitalize'}
      >
        {ProductName}
        <Icon
          className='iconClose'
          name='ec-cancel'
          color='currentColor'
          size='xsmall'
          onClick={onClose}
        />
      </Section>
      {itemPrice[basePriceIndex] ? (
        <div className='section'>
          <ul>
            <li className='label'>
              {isTariff ? translation.monthlyPrice : translation.basePrice}
            </li>
            <li className='value'>
              {formatCurrency(itemPrice[basePriceIndex].price, currency)}
            </li>
          </ul>
          {itemPrice[basePriceIndex].priceAlterations &&
          itemPrice[basePriceIndex].priceAlterations.length
            ? itemPrice[basePriceIndex].priceAlterations.map(
                (alterationType: IPriceAlteration, index: number) => {
                  return (
                    <ul key={index}>
                      <li className='label'>
                        {translation[alterationType.priceType]}
                      </li>
                      <li className='value'>
                        {formatCurrency(alterationType.price, currency)}
                      </li>
                    </ul>
                  );
                }
              )
            : null}

          <Divider className='dt_divider' />
          <ul>
            <li className='label'>{translation.finalPrice}</li>
            <li className='value'>
              {formatCurrency(itemPrice[basePriceIndex].totalPrice, currency)}
            </li>
          </ul>
        </div>
      ) : null}
      {isTariff ? null : (
        <div className='section'>
          <ul>
            <li className='label'>
              <Section size='large' weight='bold'>
                {translation.upFront}
              </Section>
            </li>
            <li className='value'>
              {formatCurrency(itemPrice[upfrontIndex].totalPrice, currency)}
            </li>
          </ul>
          <ul>
            <li className='label'>
              {' '}
              <Section size='large' weight='bold'>
                {translation.installments}
              </Section>
            </li>
            <li className='value'>
              {formatCurrency(itemPrice[monthlyIndex].totalPrice, currency)}
            </li>
          </ul>

          {itemPrice[monthlyIndex].duration && (
            <Section className='totalMonths' size='small'>{`x${
              (itemPrice[monthlyIndex].duration as IDuration).timePeriod
            } ${translation.months}`}</Section>
          )}
        </div>
      )}
    </StyledTooltipContent>
  );
};
