import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import DiscountToolTip, {
  IProps
} from '@src/common/routes/basket/Tooltip/discountPriceDetails/discountToolTip';
import { StaticRouter } from 'react-router';
import appState from '@store/states/app';

describe('<DiscountToolTip />', () => {
  const props: IProps = {
    discount: {
      priceType: 'discount',
      price: 0,
      duration: {
        timePeriod: '1',
        type: 'x'
      },
      subPriceAlterations: [
        {
          priceType: 'discount',
          price: 0,
          name: '',
          label: ''
        }
      ]
    },
    currency: 'INR',
    translation: appState().translation.cart.basket,
    onClose: jest.fn()
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <DiscountToolTip {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
