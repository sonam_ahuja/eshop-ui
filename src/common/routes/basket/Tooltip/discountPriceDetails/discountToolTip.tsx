import React from 'react';
import { Divider, Icon, Section } from 'dt-components';
import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { IPriceAlteration, ISubPriceAlterations } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { formatCurrency } from '@common/utils/currency';

export interface IProps {
  discount: IPriceAlteration;
  currency: string;
  translation: IBasket;
  onClose(): void;
}

const StyledTooltipContent = styled.div`
  color: ${colors.cloudGray};
`;

export default (props: IProps) => {
  const { currency } = props;
  const { discount, onClose, translation } = props;
  const subAlterations = props.discount.subPriceAlterations;

  return (
    <StyledTooltipContent>
      <Section
        className='titleMain'
        size='large'
        weight='bold'
        transform={'capitalize'}
      >
        {translation[discount.priceType]}
        <Icon
          className='iconClose'
          name='ec-cancel'
          color='currentColor'
          size='xsmall'
          onClick={onClose}
        />
      </Section>
      <div className='section'>
        {subAlterations &&
          Array.isArray(subAlterations) &&
          subAlterations.length &&
          subAlterations.map(
            (alteration: ISubPriceAlterations, idx: number) => {
              return (
                <ul key={idx}>
                  <li className='label'>
                    {alteration.label && alteration.label.length
                      ? alteration.label
                      : alteration.priceType}
                  </li>
                  <li className='value'>
                    {formatCurrency(alteration.price, currency)}
                  </li>
                </ul>
              );
            }
          )}

        <Divider className='dt_divider' />
        <ul>
          <li className='label'>{translation.total}</li>
          <li className='value'>{formatCurrency(discount.price, currency)}</li>
        </ul>
      </div>
    </StyledTooltipContent>
  );
};
