import React, { Component, ReactNode } from 'react';
import { Icon } from 'dt-components';
import { IPriceAlteration } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { AppTooltip } from '@src/common/components/Tooltip/styles';
import styled from 'styled-components';
import { sendOnBasketSummaryToolTipEvent } from '@events/basket';

import TooltipContent from './discountToolTip';

export const StyledSpan = styled.span`
  margin-left: 6px;
`;

export interface IProps {
  discount: IPriceAlteration;
  currency: string;
  translation: IBasket;
}

interface IState {
  tooltipOpen: boolean;
}

class DiscountToolTip extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tooltipOpen: false
    };
    this.toggleTooltip = this.toggleTooltip.bind(this);
    this.closeTooltip = this.closeTooltip.bind(this);
    this.openTooltip = this.openTooltip.bind(this);
  }

  openTooltip(): void {
    this.setState({
      tooltipOpen: true
    });
  }

  closeTooltip(): void {
    this.setState({
      tooltipOpen: false
    });
  }

  toggleTooltip(): void {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render(): ReactNode {
    const { currency } = this.props;
    const { discount, translation } = this.props;

    return (
      <AppTooltip
        autoClose={true}
        position='top'
        onVisibilityChanges={tooltipOpen => {
          this.setState(
            {
              ...this.state,
              tooltipOpen
            },
            () => {
              if (this.state.tooltipOpen) {
                sendOnBasketSummaryToolTipEvent(
                  translation[discount.priceType],
                  discount.price
                );
              }
            }
          );
        }}
        destroyOnTriggerOut={true}
        isOpen={this.state.tooltipOpen}
        tooltipContent={
          <TooltipContent
            discount={discount}
            translation={translation}
            currency={currency}
            onClose={this.closeTooltip}
          />
        }
      >
        <span style={{ marginLeft: '6px' }}>
          <span className='iconWrap'>
            <Icon
              onClick={this.openTooltip}
              size='inherit'
              color='currentColor'
              name='ec-information'
            />
          </span>
        </span>
      </AppTooltip>
    );
  }
}

export default DiscountToolTip;
