import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import MonthlyToolTip, { IProps } from '@basket/Tooltip/monthly';
import { StaticRouter } from 'react-router';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

describe('<MonthlyToolTip />', () => {
  const props: IProps = {
    title: 'phone',
    currency: 'INR',
    cartRecurringBreakUp: [
      {
        afterDuration: {
          timePeriod: 10,
          type: 'type'
        },
        afterDurationPrice: 10
      }
    ],
    translation: appState().translation.cart.basket
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <MonthlyToolTip {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('simulation open and close event for modal', () => {
    const component = componentWrapper(props);
    const clickEventListEl = component.find('i[onClick]');
    const clickEventSpanEl = component.find('span[onClick]');
    clickEventListEl.at(0).simulate('click');
    clickEventSpanEl.at(0).simulate('click');
  });

  test('handle trigger', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<MonthlyToolTip>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MonthlyToolTip {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('MonthlyToolTip')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as MonthlyToolTip).toggleTooltip({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      // tslint:disable-next-line: no-empty
      stopPropagation: () => {}
    } as React.SyntheticEvent);
  });
});
