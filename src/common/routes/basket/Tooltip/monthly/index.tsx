import React, { Component, ReactNode, SyntheticEvent } from 'react';
import { Icon } from 'dt-components';
import { ICartRecurringBreakUp } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { AppTooltip } from '@src/common/components/Tooltip/styles';
import { sendOnBasketSummaryToolTipEvent } from '@events/basket';

import TooltipContent from './tooltipContent';

export interface IProps {
  title: string;
  currency: string;
  cartRecurringBreakUp: ICartRecurringBreakUp[];
  translation: IBasket;
}

interface IState {
  tooltipOpen: boolean;
}

class MonthlyToolTip extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tooltipOpen: false
    };
    this.toggleTooltip = this.toggleTooltip.bind(this);
  }

  toggleTooltip(event: SyntheticEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

  render(): ReactNode {
    const { currency, cartRecurringBreakUp } = this.props;
    const { title, translation } = this.props;

    return (
      <AppTooltip
        tooltipStyles={{ zIndex: 1001 }}
        autoClose={true}
        position='top'
        destroyOnTriggerOut={true}
        isOpen={this.state.tooltipOpen}
        onVisibilityChanges={tooltipOpen => {
          this.setState(
            {
              tooltipOpen
            },
            () => {
              if (this.state.tooltipOpen) {
                sendOnBasketSummaryToolTipEvent(
                  title,
                  cartRecurringBreakUp[0] &&
                    cartRecurringBreakUp[0].afterDurationPrice
                );
              }
            }
          );
        }}
        tooltipContent={
          <TooltipContent
            translation={translation}
            title={title}
            currency={currency}
            cartRecurringBreakUp={cartRecurringBreakUp}
            onClose={this.toggleTooltip}
          />
        }
      >
        <span className='tooltipCta'>
          <Icon
            size='inherit'
            color='currentColor'
            onClick={this.toggleTooltip}
            name='ec-information'
          />
        </span>
      </AppTooltip>
    );
  }
}

export default MonthlyToolTip;
