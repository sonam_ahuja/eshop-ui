import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import TooltipContent, { IProps } from '@basket/Tooltip/monthly/tooltipContent';
import { StaticRouter } from 'react-router';
import appState from '@store/states/app';

describe('<MonthlyToolTip />', () => {
  const props: IProps = {
    title: 'phone',
    currency: 'INR',
    cartRecurringBreakUp: [],
    translation: appState().translation.cart.basket,
    onClose: jest.fn()
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <TooltipContent {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
