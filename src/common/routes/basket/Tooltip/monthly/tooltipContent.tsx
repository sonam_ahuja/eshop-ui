import React, { ReactNode, SyntheticEvent } from 'react';
import { Icon, Section } from 'dt-components';
import styled from 'styled-components';
import { ICartRecurringBreakUp } from '@basket/store/types';
import { IBasket } from '@src/common/store/types/translation';
import { formatCurrency } from '@common/utils/currency';

export interface IProps {
  title: string;
  currency: string;
  cartRecurringBreakUp: ICartRecurringBreakUp[];
  translation: IBasket;
  onClose(event?: SyntheticEvent): void;
}

const StyledTooltipContent = styled.div``;

const renderMonthlySummaryBreakup = (
  currency: string,
  cartRecurringBreakUp: ICartRecurringBreakUp[],
  translation: IBasket
) => {
  const arry: ReactNode[] = [];

  cartRecurringBreakUp.forEach(
    (monthPeriod: ICartRecurringBreakUp, index: number) => {
      arry.push(
        <ul key={index}>
          <li className='label'>
            {translation.after} {monthPeriod.afterDuration.timePeriod}{' '}
            {translation.months}
          </li>
          <li className='value'>
            {formatCurrency(monthPeriod.afterDurationPrice, currency)}
          </li>
        </ul>
      );
    }
  );

  return arry;
};

export default (props: IProps) => {
  const { cartRecurringBreakUp, currency } = props;

  const { title, onClose, translation } = props;

  return (
    <StyledTooltipContent>
      <Section
        className='titleMain'
        size='large'
        weight='bold'
        transform={'capitalize'}
      >
        {title}
        <span className='xyz' onClick={onClose}>
          <Icon
            className='iconClose'
            name='ec-cancel'
            color='currentColor'
            size='xsmall'
          />
        </span>
      </Section>
      <div className='section'>
        {renderMonthlySummaryBreakup(
          currency,
          cartRecurringBreakUp,
          translation
        )}
      </div>
    </StyledTooltipContent>
  );
};
