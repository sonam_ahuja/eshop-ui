import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { StaticRouter } from 'react-router';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { RootState } from '@src/common/store/reducers';

import { Basket, IProps as IBasketProps } from '.';

describe('<Basket 2 />', () => {
  const newbasket = { ...appState().basket };
  newbasket.basketItems = BASKET_ALL_ITEM.cartItems;
  newbasket.cartSummary = BASKET_ALL_ITEM.cartSummary;
  const props: IBasketProps = {
    commonError: {
      code: '',
      retryable: false
    },
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    ...histroyParams,
    translation: appState().translation,
    configuration: appState().configuration,
    checkoutState: appState().checkout,
    basket: newbasket,
    totalItems: 1,
    undoAbleItems: BASKET_ALL_ITEM.cartItems,
    showRemoveItemNotification: true,
    isCheckoutEnable: true,
    cartId: '12345',
    isIdentityVerificationRequired: false,
    possibleNextRouteWhenSummaryButtonClick: jest.fn(),
    fetchBasket: jest.fn(),
    clearBasket: jest.fn(),
    updateBasketItem: jest.fn(),
    clearRemoveBasketItemError: jest.fn(),
    deleteBasketItem: jest.fn(),
    removeBasketItem: jest.fn(),
    addBasketNotification: jest.fn(),
    updateItemQuantity: jest.fn(),
    setDeleteBasketModal: jest.fn(),
    setDeleteBasketItemModal: jest.fn(),
    goToLogin: jest.fn(),
    removeBasketItemModalState: {
      modalOpen: false,
      error: null,
      loading: true,
      selectedItem: BASKET_ALL_ITEM.cartItems[0]
    },
    clearBasketModalState: {
      modalOpen: false,
      error: null,
      loading: true
    },
    isUserLoggedIn: false,
    stickySummary: false
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('Basket handleClearBasket should called, when cartId is defined ', () => {
    const onClose = jest.spyOn(Basket.prototype, 'handleClearBasket');
    const component = mount<Basket>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Basket {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('Basket').instance() as Basket).handleClearBasket();
    expect(onClose).toHaveBeenCalled();
  });

  test('Basket handleClearBasket should called, when cartId is null ', () => {
    const newProps = { ...props };
    newProps.cartId = null;
    newProps.undoAbleItems = [];
    newProps.basket.basketItems = [];
    newProps.basket.showAppShell = false;
    const onClose = jest.spyOn(Basket.prototype, 'handleClearBasket');
    const component = mount<Basket>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Basket {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('Basket').instance() as Basket).handleClearBasket();
    expect(onClose).toHaveBeenCalled();
  });

  test('Basket error screen condition need to be handle', () => {
    const newProps = { ...props };
    newProps.cartId = null;
    newProps.basket.basketItems = [];
    newProps.basket.showAppShell = false;
    newProps.commonError.httpStatusCode = 501;
    const component = mount<Basket>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Basket {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();
  });

  test('Basket error screen condition need to be handle with undoable itemss', () => {
    const newProps = { ...props };
    newProps.cartId = null;
    newProps.showRemoveItemNotification = true;
    // tslint:disable-next-line:no-any
    newProps.undoAbleItems = ['1'] as any;
    newProps.basket.basketItems = [];
    newProps.basket.showAppShell = false;
    const component = mount<Basket>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Basket {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();
  });
});
