import Error from '@common/components/Error';
import { ITranslationState } from '@store/types/translation';
import { Loader } from 'dt-components';
import Helmet from 'react-helmet';
import React, { Component, Fragment, ReactNode } from 'react';
import { connect } from 'react-redux';
import {
  IBasketItem,
  IBasketState,
  IFetchBasketArgs,
  INotification,
  IRemoveBasketItemParams,
  ISetDeleteBasketItemModalPayload,
  IUpdateBasketItemPayload
} from '@basket/store/types';
import Loadable from 'react-loadable';
import { RouteComponentProps, withRouter } from 'react-router';
import BreadCrumb from '@common/components/CatalogBreadCrumb';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@store/types/configuration';
import Header from '@common/components/Header';
import BasketSummary from '@common/components/Summary/desktop';
import { IState as ICheckoutState } from '@checkout/store/types';
import routeConstants from '@common/constants/routes';
import { IError } from '@common/store/types/common';
import { pageViewEvent } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import { VIRTUAL_BASKET } from '@common/routes/basket/store/enums';
import { removeQueryParam } from '@common/utils/parseQuerySrting';

import BasketHeader from './BasketHeader';
import BasketWrap from './BasketWrap';
import { StyledBasketContainer } from './styles';
import NotificationBar from './StripMessage';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

const BasketWrapShellComponent = Loadable({
  loading: () => null,
  loader: () =>
    import(
      '@src/common/routes/basket/BasketWrap/index.shell'
    )
});

const BasketSummaryShellComponent = Loadable({
  loading: () => null,
  loader: () =>
    import(
      '@common/components/Summary/index.shell'
    )
});

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  commonError: IError;
  currency: ICurrencyConfiguration;
  translation: ITranslationState;
  basket: IBasketState;
  checkoutState: ICheckoutState;
  totalItems: number;
  cartId: string | null;
  configuration: IConfigurationState;
  removeBasketItemModalState: IBasketState['removeBasketItem'];
  clearBasketModalState: IBasketState['clearBasket'];
  isCheckoutEnable: boolean;
  undoAbleItems: IBasketItem[];
  showRemoveItemNotification: boolean;
  isUserLoggedIn: boolean;
  stickySummary: boolean;
  isIdentityVerificationRequired: boolean;
  fetchBasket(data: IFetchBasketArgs): void;
  updateBasketItem(id: number, item: IBasketItem): void;
  clearBasket(): void;
  deleteBasketItem(id: string): void;
  removeBasketItem(payload: IRemoveBasketItemParams): void;
  addBasketNotification(data: INotification): void;
  updateItemQuantity(payload: IUpdateBasketItemPayload): void;
  setDeleteBasketModal(visibility: boolean): void;
  setDeleteBasketItemModal(payload: ISetDeleteBasketItemModalPayload): void;
  clearRemoveBasketItemError(): void;
  goToLogin(isRoutableFromLogin: boolean): void;
  possibleNextRouteWhenSummaryButtonClick(): void;
}

export class Basket extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.handleClearBasket = this.handleClearBasket.bind(this);
    this.getVirtualBasketId = this.getVirtualBasketId.bind(this);
  }

  componentDidMount(): void {
    window.scrollTo({
      top: 0
    });
    const virtualBasketId = this.getVirtualBasketId();
    this.props.fetchBasket({
      showFullPageError: true,
      isFromBasket: true,
      virtualBasketId
    });
    pageViewEvent(PAGE_VIEW.BASKET);
  }

  getVirtualBasketId(): string | null {
    const searchParams = this.props.location.search;
    if (searchParams && searchParams.length) {
      const queryParams = new URLSearchParams(searchParams);
      const virtualBasketId = queryParams.get(VIRTUAL_BASKET.ID);
      if (virtualBasketId) {
        const updateURL = removeQueryParam(
          window.location.href,
          VIRTUAL_BASKET.ID
        );
        const urlparts = updateURL.split('?');

        this.props.history.replace({
          pathname: this.props.history.location.pathname,
          search:
            urlparts && urlparts[1] && urlparts[1].length ? urlparts[1] : ''
        });

        return virtualBasketId;
      }
    }

    return null;
  }

  handleClearBasket(): void {
    this.props.clearBasket();
  }

  getBasketSummary(): ReactNode {
    const { basket, configuration, currency } = this.props;
    const { translation, isCheckoutEnable, goToLogin } = this.props;
    const { isUserLoggedIn, checkoutState, stickySummary } = this.props;
    const { isIdentityVerificationRequired } = this.props;
    const { possibleNextRouteWhenSummaryButtonClick } = this.props;

    return basket.basketItems &&
      basket.basketItems.length > 0 &&
      Array.isArray(basket.cartSummary) &&
      basket.cartSummary.length > 0 ? (
      <BasketSummary
        configuration={configuration}
        currency={currency}
        cartSummary={basket.cartSummary}
        translation={translation}
        isCheckoutEnable={isCheckoutEnable}
        goToLogin={goToLogin}
        isUserLoggedIn={isUserLoggedIn}
        isSticky={true}
        isTermsConditionChecked={
          checkoutState.orderReview.agreeOnTermsAndCondition
        }
        isIdentityVerificationRequired={isIdentityVerificationRequired}
        possibleNextRouteWhenSummaryButtonClick={
          possibleNextRouteWhenSummaryButtonClick
        }
        stickySummary={stickySummary}
        proceedToCheckoutLoading={basket.proceedToCheckoutLoading}
      />
    ) : null;
  }

  getCartRestrictionMessage(): ReactNode {
    const { basket, totalItems } = this.props;
    const { translation, configuration } = this.props;

    return totalItems &&
      (totalItems > 1 || basket.showCartRestrictionMessage) ? (
      <NotificationBar
        timer={configuration.cms_configuration.global.messageStripTimer}
        isIconRequired={false}
        undoAbleItems={[]}
        message={translation.cart.basket.stripMessage.cartRestrictionMessage}
      />
    ) : null;
  }

  getPriceChangedMessage(): ReactNode {
    const {
      basket: { priceNotificationStrip },
      totalItems,
      configuration
    } = this.props;

    return totalItems && priceNotificationStrip.show ? (
      <NotificationBar
        timer={configuration.cms_configuration.global.messageStripTimer}
        isIconRequired={false}
        undoAbleItems={[]}
        message={priceNotificationStrip.notification}
      />
    ) : null;
  }

  render(): ReactNode {
    const { basket, totalItems, configuration } = this.props;
    const { clearBasketModalState, commonError } = this.props;
    const { setDeleteBasketModal, translation } = this.props;
    const { undoAbleItems } = basket;

    const breadCrumbItems = [
      {
        title: translation.cart.global.home,
        active: false,
        link: routeConstants.HOME
      },
      {
        title: translation.cart.global.basket,
        active: true
      }
    ];

    return (
      <Fragment>
        <Helmet title={translation.cart.global.basket} />
        <Loader
          open={basket.loading}
          label={translation.cart.basket.loadingQuantity}
        />
        <StyledBasketContainer>
          <div className='basketMain'>
            <Header showBasketIcon={false} isBasketRoute={true} />
            {commonError.httpStatusCode ? (
              <Error />
            ) : (
              <div className='basketContent'>
                {basket.showAppShell ? (
                  <BasketWrapShellComponent />
                ) : (
                  <>
                    <BreadCrumb
                      children={null}
                      breadCrumbItems={breadCrumbItems}
                    />
                    <BasketHeader
                      enableClear={totalItems > 0}
                      clearBasket={this.handleClearBasket}
                      translation={translation.cart.basket}
                      clearBasketModalState={clearBasketModalState}
                      setDeleteBasketModal={setDeleteBasketModal}
                    />
                    {this.getPriceChangedMessage()}
                    {basket.showRemoveItemNotification && (
                      <NotificationBar
                        timer={
                          configuration.cms_configuration.global
                            .messageStripTimer
                        }
                        isIconRequired={true}
                        undoAbleItems={undoAbleItems}
                        message={
                          undoAbleItems && undoAbleItems.length
                            ? translation.cart.basket.stripMessage.oneItemRemove
                            : translation.cart.basket.stripMessage.allItemRemove
                        }
                      />
                    )}
                    {this.getCartRestrictionMessage()}
                    <BasketWrap showMoreFlag={true} />
                  </>
                )}
              </div>
            )}
          </div>

          {basket.showAppShell ? (
            <BasketSummaryShellComponent isButtonVisible={true} />
          ) : (
            this.getBasketSummary()
          )}
        </StyledBasketContainer>
      </Fragment>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Basket)
  // tslint:disable-next-line:max-file-line-count
);
