import { commonAction } from '@store/actions';
import { Dispatch } from 'redux';
import actions from '@basket/store/actions';
import { RootState } from '@common/store/reducers';
import { IFetchBasketArgs, INotification } from '@basket/store/types';

export const mapStateToProps = (state: RootState) => ({
  commonError: state.common.error,
  currency: state.configuration.cms_configuration.global.currency,
  basket: state.basket,
  checkoutState: state.checkout,
  totalItems: state.basket.pagination.total,
  configuration: state.configuration,
  cartId: state.basket.cartId,
  clearBasketModalState: state.basket.clearBasket,
  translation: state.translation,
  isCheckoutEnable: state.basket.isCheckoutEnable,
  showRemoveItemNotification: state.basket.showRemoveItemNotification,
  isUserLoggedIn: state.common.isLoggedIn,
  isIdentityVerificationRequired:
    state.common.userIdentityVerificationDetails.verificationRequired,
  stickySummary: state.basket.stickySummary
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchBasket(data: IFetchBasketArgs): void {
    dispatch(actions.fetchBasket(data));
  },
  clearBasket(): void {
    dispatch(actions.clearBasket());
  },
  addBasketNotification(data: INotification): void {
    dispatch(actions.addBasketNotification(data));
  },
  setDeleteBasketModal(visibility: boolean): void {
    dispatch(actions.setDeleteBasketModal(visibility));
  },
  goToLogin(isRoutableFromLogin: boolean): void {
    dispatch(commonAction.routeToLoginFromBasket(isRoutableFromLogin));
  },
  possibleNextRouteWhenSummaryButtonClick(): void {
    dispatch(actions.validateCartPrice());
  }
});
