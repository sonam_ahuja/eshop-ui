import { Provider } from 'react-redux';
import React from 'react';
import { RootState } from '@src/common/store/reducers';
import { INotification } from '@basket/store/types';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import constants from '@basket/store/constants';
import IState from '@basket/store/state';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';

import Basket, { IProps as IBasketProps } from '.';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

describe('<Basket />', () => {
  const newBasket = { ...IState() };
  newBasket.cartSummary = BASKET_ALL_ITEM.cartSummary;
  newBasket.showAppShell = false;
  const props: IBasketProps = {
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    checkoutState: appState().checkout,
    goToLogin: jest.fn(),
    ...histroyParams,
    translation: appState().translation,
    configuration: appState().configuration,
    basket: newBasket,
    showRemoveItemNotification: true,
    undoAbleItems: [],
    totalItems: 1,
    isCheckoutEnable: true,
    cartId: '123473',
    fetchBasket: jest.fn(),
    clearBasket: jest.fn(),
    updateBasketItem: jest.fn(),
    clearRemoveBasketItemError: jest.fn(),
    deleteBasketItem: jest.fn(),
    removeBasketItem: jest.fn(),
    addBasketNotification: jest.fn(),
    updateItemQuantity: jest.fn(),
    setDeleteBasketModal: jest.fn(),
    setDeleteBasketItemModal: jest.fn(),
    removeBasketItemModalState: {
      modalOpen: false,
      error: null,
      loading: true,
      selectedItem: null
    },
    clearBasketModalState: {
      modalOpen: false,
      error: null,
      loading: true
    },
    isUserLoggedIn: false,
    isIdentityVerificationRequired: false,
    possibleNextRouteWhenSummaryButtonClick: jest.fn(),
    stickySummary: false
  };
  const mockStore = configureStore();

  test('should render properly', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Basket {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly', () => {
    const newBasketWithoutCartSummary = { ...newBasket };
    newBasketWithoutCartSummary.cartSummary = [];
    newBasketWithoutCartSummary.basketItems = [];
    newBasketWithoutCartSummary.showAppShell = true;
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Basket {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    const initStateValue: RootState = appState();
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const fetchBasketExpected = {
      type: constants.FETCH_BASKET_REQUESTED,
      payload: {}
    };
    const addBasketNotificationData: INotification = {
      content: ['line 1', 'line 2'],
      type: 'remove',
      isOpen: true
    };
    const addBasketNotificationExpected = {
      type: constants.ADD_BASKET_NOTIFICATION,
      payload: addBasketNotificationData
    };
    const clearBasketExpected = {
      payload: undefined,
      type: constants.CLEAR_BASKET_DATA
    };

    mapDispatchToProps(dispatch).fetchBasket({});
    mapDispatchToProps(dispatch).clearBasket();
    mapDispatchToProps(dispatch).addBasketNotification(
      addBasketNotificationData
    );
    mapDispatchToProps(dispatch).setDeleteBasketModal(true);
    mapDispatchToProps(dispatch).goToLogin(true);
    mapDispatchToProps(dispatch).possibleNextRouteWhenSummaryButtonClick();
    expect(dispatch.mock.calls[0][0]).toEqual(fetchBasketExpected);
    expect(dispatch.mock.calls[1][0]).toEqual(clearBasketExpected);
    expect(dispatch.mock.calls[2][0]).toEqual(addBasketNotificationExpected);
    expect(dispatch.mock.calls[3][0]).toEqual({
      type: constants.SET_DELETE_BASKET_MODAL,
      payload: true
    });
    expect(dispatch.mock.calls[4][0]).toEqual({
      type: 'ESHOP_ROUTE_TO_LOGIN_FROM_BASKET',
      payload: true
    });
  });
});
