import actions from '@basket/store/actions';
import constants from '@basket/store/constants';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import {
  IBasketItem,
  INotification,
  IRemoveBasketItemParams,
  IResponse
} from '@basket/store/types';
import { IError } from '@common/store/types/common';

// tslint:disable-next-line:no-big-function
describe('basket actions', () => {
  it('fetchBasket action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.FETCH_BASKET_REQUESTED,
      payload: {}
    };
    expect(actions.fetchBasket({})).toEqual(expectedAction);
  });

  it('clearRemoveBasketItemError action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.CLEAR_REMOVE_BASKET_ITEM_ERROR
    };
    expect(actions.clearRemoveBasketItemError()).toEqual(expectedAction);
  });

  it('removeBasketItemSuccess action creator should set basket item visibility', () => {
    const expectedAction = {
      type: constants.REMOVE_BASKET_ITEM_SUCCESS
    };
    expect(actions.removeBasketItemSuccess()).toEqual(expectedAction);
  });

  it('removeBasketItemError action creator should set basket item visibility', () => {
    const expectedAction = {
      type: constants.REMOVE_BASKET_ITEM_ERROR,
      payload: null
    };
    expect(actions.removeBasketItemError(null)).toEqual(expectedAction);
  });

  it('setDeleteBasketItemModal action creator should set basket item visibility', () => {
    const payload = {
      showModal: true,
      selectedItem: null,
      resetModalLoading: false
    };
    const expectedAction = {
      type: constants.SET_DELETE_BASKET_ITEM_MODAL,
      payload
    };
    expect(actions.setDeleteBasketItemModal(payload)).toEqual(expectedAction);
  });

  it('clearBasketSuccess action creator should set set clear basket modal success', () => {
    const expectedAction = {
      type: constants.CLEAR_BASKET_SUCCESS
    };
    expect(actions.clearBasketSuccess()).toEqual(expectedAction);
  });

  it('clearBasketError action creator should set clear basket modal error', () => {
    const expectedAction = {
      type: constants.CLEAR_BASKET_ERROR,
      payload: null
    };
    expect(actions.clearBasketError(null)).toEqual(expectedAction);
  });

  it('setDeleteBasketModal action creator should set clear basket visibility', () => {
    const expectedAction = {
      type: constants.SET_DELETE_BASKET_MODAL,
      payload: true
    };
    expect(actions.setDeleteBasketModal(true)).toEqual(expectedAction);
  });

  it('updateBasketItemSuccess action creator should return a object without payload', () => {
    const data: { id: string | null; quantity: number } = {
      id: '1234',
      quantity: 3
    };
    const expectedAction = {
      type: constants.UPDATE_BASKET_ITEM_QUANTITY_SUCCESS,
      payload: data
    };
    expect(actions.updateBasketItemSuccess(data)).toEqual(expectedAction);
  });

  it('updateBasketItemError action creator should return a object without payload', () => {
    const data: IError = {
    code: 'string',
    retryable: false
    };
    const expectedAction = {
      type: constants.UPDATE_BASKET_ITEM_QUANTITY_SUCCESS_ERROR,
      payload: data
    };
    expect(actions.updateBasketItemError(data)).toEqual(expectedAction);
  });

  it('removeBasketNotification action creator should remove notification', () => {
    const data: INotification = {
      content: ['line 1', 'line 2'],
      type: 'remove',
      isOpen: true
    };
    const expectedAction = {
      type: constants.REMOVE_BASKET_NOTIFICATION,
      payload: data
    };
    expect(actions.removeBasketNotification(data)).toEqual(expectedAction);
  });

  it('addBasketNotification action creator should remove notification', () => {
    const data: INotification = {
      content: ['line 1', 'line 2'],
      type: 'remove',
      isOpen: true
    };
    const expectedAction = {
      type: constants.ADD_BASKET_NOTIFICATION,
      payload: data
    };
    expect(actions.addBasketNotification(data)).toEqual(expectedAction);
  });

  it('setCheckoutButtonStatus action creator should return a object without payload', () => {
    const data: IBasketItem[] = BASKET_ALL_ITEM.cartItems;
    const expectedAction = {
      type: constants.SET_CHECKOUT_BUTTON_STATUS,
      payload: data
    };
    expect(actions.setCheckoutButtonStatus(data)).toEqual(expectedAction);
  });

  it('setBasketData action creator should set basket response', () => {
    const data: IResponse = {
      cartItems: BASKET_ALL_ITEM.cartItems,
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      totalItems: BASKET_ALL_ITEM.totalItems
    };
    const expectedAction = {
      type: constants.SET_BASKET_DATA,
      payload: data
    };
    expect(actions.setBasketData(data)).toEqual(expectedAction);
  });

  it('fetchBasketLoading action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.FETCH_BASKET_LOADING
    };
    expect(actions.fetchBasketLoading()).toEqual(expectedAction);
  });
  it('fetchBasketError action creator should return a object without payload', () => {
    const data: IError = {
      code: '',
      retryable: true
    };
    const expectedAction = {
      type: constants.FETCH_BASKET_ERROR,
      payload: data
    };
    expect(actions.fetchBasketError(data)).toEqual(expectedAction);
  });

  it('fetchBasketSuccess action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.FETCH_BASKET_SUCCESS
    };
    expect(actions.fetchBasketSuccess()).toEqual(expectedAction);
  });

  it('deletesetCartItems action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.DELETE_CART_ITEMS
    };
    expect(actions.deletesetCartItems()).toEqual(expectedAction);
  });

  it('updateBasketItem action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.UPDATE_BASKET_ITEM_QUANTITY,
      payload: {
        cartItems: [{ id: '1234', quantity: 2 }]
      }
    };
    expect(
      actions.updateBasketItem({
        cartItems: [{ id: '1234', quantity: 2 }]
      })
    ).toEqual(expectedAction);
  });
  it('clearBasket action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.CLEAR_BASKET_DATA,
      payload: undefined
    };
    expect(actions.clearBasket()).toEqual(expectedAction);
  });

  it('resetBasket action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.RESET_BASKET_DATA,
      payload: undefined
    };
    expect(actions.resetBasket()).toEqual(expectedAction);
  });

  it('requestClearBasket action creator should return a object without payload', () => {
    const expectedAction = {
      type: constants.DELETE_BASKET_REQUESTED
    };
    expect(actions.requestClearBasket()).toEqual(expectedAction);
  });

  it('removeBasketItemRequested action creator should return a object without payload', () => {
    const data: IRemoveBasketItemParams = {
      cartItems: []
    };
    const expectedAction = {
      type: constants.REMOVE_BASKET_ITEM,
      payload: data
    };
    expect(actions.removeBasketItem(data)).toEqual(expectedAction);
  });

  it('setUndoAbleItems action creator should set undoAbleItems in store', () => {
    const expectedAction = {
      type: constants.SET_UNDOABLE_ITEMS,
      payload: null
    };
    expect(actions.setUndoAbleItems(null)).toEqual(expectedAction);
  });

  it('resetUndoAbleItems action creator should mutate undoAbleItems in store', () => {
    const expectedAction = {
      type: constants.RESET_UNDOABLE_ITEMS
    };
    expect(actions.resetUndoAbleItems()).toEqual(expectedAction);
  });
  it('setShowDeleteCardNotification action creator should mutate showRemoveItemNotification in store', () => {
    const expectedAction = {
      type: constants.SET_DELETE_CART_NOTIFICATION
    };
    expect(actions.setShowDeleteCardNotification()).toEqual(expectedAction);
  });
  it('updateBasketItemRequested action creator should mutate loading and error in store', () => {
    const expectedAction = {
      type: constants.UPDATE_BASKET_ITEM_REQUESTED
    };
    expect(actions.updateBasketItemRequested()).toEqual(expectedAction);
  });
  it('updateBasketSuccess action creator should mutate loading and error in store', () => {
    const expectedAction = {
      type: constants.UPDATE_BASKET_SUCCESS
    };
    expect(actions.updateBasketSuccess()).toEqual(expectedAction);
  });
});
