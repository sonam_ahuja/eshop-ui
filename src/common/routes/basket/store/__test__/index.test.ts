import { basketReducer } from '@basket/store';
import IBasketState from '@basket/store/state';

describe('basketReducer ', () => {
  it('basketReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(basketReducer(IBasketState(), definedAction)).toBeDefined();
  });
});
