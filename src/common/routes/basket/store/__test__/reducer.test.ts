import basket from '@basket/store/reducer';
import basketState from '@basket/store/state';
import actions from '@basket/store/actions';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { IBasketItem, IBasketState, IResponse } from '@basket/store/types';
import { IError } from '@common/store/types/common';

describe('Basket Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IBasketState = basket(undefined, definedAction);
  let expectedState: IBasketState = basketState();
  const initializeValue = () => {
    initialStateValue = basket(undefined, definedAction);
    expectedState = basketState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('FETCH_BASKET_REQUESTED should mutate basket state', () => {
    const expected: IBasketState = { ...expectedState, cartId: null };
    const action = actions.fetchBasket({});
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('FETCH_BASKET_LOADING should mutate loading in state', () => {
    const expected: IBasketState = { ...expectedState, loading: false };
    const action = actions.fetchBasketLoading();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('FETCH_BASKET_ERROR should mutate error in state', () => {
    const errorData: IError = {
      code: 'Some error',
      retryable: false
    };
    const expected: IBasketState = {
      ...expectedState,
      loading: false,
      ...{ error: errorData },
      showAppShell: false
    };
    const action = actions.fetchBasketError(errorData);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('FETCH_BASKET_SUCCESS should mutate loading error in state', () => {
    const expected: IBasketState = {
      ...expectedState,
      loading: false,
      error: null,
      showAppShell: false
    };
    const action = actions.fetchBasketSuccess();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('RESET_BASKET_DATA should mutate cartId for new cartId in state', () => {
    const expected: IBasketState = {
      ...expectedState
    };
    expected.pagination.total = 0;
    const action = actions.resetBasket();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SET_BASKET_DATA action should mutate basketItems, cartSummary and pagination total  item state', () => {
    const params: IResponse = {
      cartItems: BASKET_ALL_ITEM.cartItems,
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      totalItems: BASKET_ALL_ITEM.totalItems
    };
    const expected: IBasketState = {
      ...expectedState,
      ...{ basketItems: BASKET_ALL_ITEM.cartItems },
      ...{ cartSummary: BASKET_ALL_ITEM.cartSummary },
      ...{ cartSummary: BASKET_ALL_ITEM.cartSummary }
    };

    expected.pagination.total = BASKET_ALL_ITEM.totalItems;
    const action = actions.setBasketData(params);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SET_DELETE_BASKET_ITEM_MODAL should mutate modal visibility in state', () => {
    const expected: IBasketState = { ...expectedState };
    expected.removeBasketItem.modalOpen = true;
    const payload = {
      showModal: true,
      selectedItem: null,
      resetModalLoading: false
    };
    const action = actions.setDeleteBasketItemModal(payload);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SET_CHECKOUT_BUTTON_STATUS should mutate isCheckoutEnable status in state', () => {
    const initalData: IBasketState = {
      ...expectedState,
      isCheckoutEnable: false
    };
    const data: IBasketItem[] = BASKET_ALL_ITEM.cartItems;
    const action = actions.setCheckoutButtonStatus(data);
    const state = basket(initalData, action);
    expect(state).toEqual(initalData);
  });

  it('REMOVE_BASKET_ITEM_ERROR should mutate loading error and modal status in state', () => {
    const expected: IBasketState = { ...expectedState };
    expected.removeBasketItem.error = null;
    expected.removeBasketItem.loading = false;
    expected.removeBasketItem.modalOpen = false;
    const action = actions.removeBasketItemError(null);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('CLEAR_REMOVE_BASKET_ITEM_ERROR should mutate loading error and modal status in state', () => {
    const expected: IBasketState = { ...expectedState };
    expected.removeBasketItem.error = null;
    const action = actions.clearRemoveBasketItemError();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SET_CART_SUMMARY_DATA should mutate loading error and modal status in state', () => {
    const action = actions.setCartSummary(BASKET_ALL_ITEM.cartSummary);
    const state = basket(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('REMOVE_BASKET_ITEM_SUCCESS should mutate loading error and modal status in state', () => {
    const initalData: IBasketState = { ...expectedState };
    initalData.basketItems = BASKET_ALL_ITEM.cartItems;
    const expected: IBasketState = { ...expectedState };
    expected.basketItems = BASKET_ALL_ITEM.cartItems;
    expected.removeBasketItem.error = null;
    expected.removeBasketItem.loading = false;
    expected.removeBasketItem.modalOpen = false;
    const action1 = actions.removeBasketItemSuccess();
    const state1 = basket(initalData, action1);
    expect(state1).toEqual(expected);
  });

  it('UPDATE_BASKET_ITEM_QUANTITY_SUCCESS should mutate modal visibility in state', () => {
    const initalData: IBasketState = { ...expectedState };
    initalData.basketItems = BASKET_ALL_ITEM.cartItems;
    const data: { id: string | null; quantity: number } = {
      id: '1234',
      quantity: 3
    };
    const dataWithCorrectId: { id: string | null; quantity: number } = {
      id: '123457',
      quantity: 3
    };
    const expectedValue: IBasketState = { ...expectedState };
    BASKET_ALL_ITEM.cartItems[0].quantity = 3;
    expectedValue.basketItems = BASKET_ALL_ITEM.cartItems;
    const action = actions.updateBasketItemSuccess(data);
    const actionWithCorrentId = actions.updateBasketItemSuccess(
      dataWithCorrectId
    );
    const state = basket(initalData, action);
    const stateWithCorrectId = basket(initalData, actionWithCorrentId);
    expect(state).toEqual(initalData);
    expect(stateWithCorrectId).toEqual(expectedValue);
  });
});
