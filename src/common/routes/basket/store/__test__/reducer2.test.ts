import basket from '@basket/store/reducer';
import basketState from '@basket/store/state';
import actions from '@basket/store/actions';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { IBasketState, IRemoveBasketItemParams } from '@basket/store/types';

describe('Basket Reducer 2', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IBasketState = basket(undefined, definedAction);
  let expectedState: IBasketState = basketState();
  const initializeValue = () => {
    initialStateValue = basket(undefined, definedAction);
    expectedState = basketState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('CLEAR_BASKET_SUCCESS should mutate clear basket modal visibility in state', () => {
    const initalData: IBasketState = { ...expectedState };
    initalData.basketItems = BASKET_ALL_ITEM.cartItems;
    initalData.cartSummary = BASKET_ALL_ITEM.cartSummary;
    const action = actions.clearBasketSuccess();
    const state = basket(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('CLEAR_BASKET_ERROR should mutate clear basket modal visibility in state', () => {
    const initalData: IBasketState = { ...expectedState };
    initalData.basketItems = BASKET_ALL_ITEM.cartItems;
    initalData.cartSummary = BASKET_ALL_ITEM.cartSummary;
    const expectedValue: IBasketState = { ...expectedState };
    expectedValue.clearBasket.error = null;
    expectedValue.clearBasket.loading = false;
    expectedValue.clearBasket.modalOpen = false;
    const action = actions.clearBasketError(null);
    const state = basket(initalData, action);
    expect(state).toEqual(initalData);
  });

  it('SET_DELETE_BASKET_MODAL should mutate clear basket modal visibility in state', () => {
    const initalData: IBasketState = { ...expectedState };
    const expectedValue: IBasketState = { ...expectedState };
    expectedValue.clearBasket.error = null;
    expectedValue.clearBasket.loading = false;
    expectedValue.clearBasket.modalOpen = true;
    const action = actions.setDeleteBasketModal(true);
    const state = basket(initalData, action);
    expect(state).toEqual(expectedValue);
  });

  it('SET_UNDOABLE_ITEMS should mutate clear basket modal visibility in state', () => {
    const initalData: IBasketState = { ...expectedState };
    const initalDataWithId: IBasketState = { ...expectedState };
    const expectedValue: IBasketState = { ...expectedState };
    expectedValue.undoAbleItems = [];
    const expectedValueWithId: IBasketState = { ...expectedState };
    expectedValueWithId.basketItems = [];
    const action = actions.setUndoAbleItems(null);
    const actionWithId = actions.setUndoAbleItems('123457');
    const state = basket(initalData, action);
    const stateWithId = basket(initalDataWithId, actionWithId);
    expect(state).toEqual(expectedValue);
    expect(stateWithId).toEqual(expectedValueWithId);
  });

  it('RESET_UNDOABLE_ITEMS should mutate clear basket modal visibility in state', () => {
    const expectedValue: IBasketState = { ...expectedState, undoAbleItems: [] };
    const action = actions.resetUndoAbleItems();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });
  it('SET_DELETE_CART_NOTIFICATION should mutate clear basket modal visibility in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState,
      showRemoveItemNotification: true
    };
    const action = actions.setShowDeleteCardNotification();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });
  it('UPDATE_BASKET_ITEM_REQUESTED should mutate loading and error in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState,
      loading: true,
      error: null
    };
    const action = actions.updateBasketItemRequested();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });
  it('UPDATE_BASKET_SUCCESS should mutate clear basket loading and error in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState,
      loading: false,
      error: null
    };
    const action = actions.updateBasketSuccess();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });
  it('REMOVE_BASKET_ITEM should mutate removeBasketItem error and loading  in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState
    };
    expectedValue.removeBasketItem.loading = true;
    expectedValue.removeBasketItem.error = null;
    const data: IRemoveBasketItemParams = {
      cartItems: []
    };
    const action = actions.removeBasketItem(data);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });

  it('CLEAR_BASKET_DATA should mutate error and loading  in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState
    };
    expectedValue.clearBasket.loading = true;
    expectedValue.clearBasket.modalOpen = true;
    const action = actions.clearBasket();
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });

  it('SET_STICKY_SUMMARY should mutate error and loading in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState
    };
    expectedValue.stickySummary = true;
    const action = actions.setStickySummary(true);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });

  it('SHOW_BASKET_SIDE_BAR should mutate error and loading in state', () => {
    const expectedValue: IBasketState = {
      ...expectedState
    };
    expectedValue.showBasketSideBar = true;
    const action = actions.showBasketSideBar(true);
    const state = basket(initialStateValue, action);
    expect(state).toEqual(expectedValue);
  });
});
