import basketSaga, {
  clearBasket,
  fetchBasket,
  removeBasketItem,
  updateBasketItemQuantity
} from '@basket/store/sagas';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

describe('Basket Saga', () => {
  const mock = new MockAdapter(axios);

  it('basketSaga saga test', () => {
    const logoutGenerator = basketSaga();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });
  it('fetchBasket saga test', async () => {
    const logoutGenerator = fetchBasket({
      type: '',
      payload: {
        isCalledFromCheckout: true
      }
    });
    expect(logoutGenerator.next().value).toBeDefined();
    mock.onGet(apiEndpoints.BASKET.GET_BASKET_DATA.url('123')).replyOnce(200, {
      cartItems: BASKET_ALL_ITEM.cartItems,
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      totalItems: BASKET_ALL_ITEM.totalItems
    });
    axios.get(apiEndpoints.BASKET.GET_BASKET_DATA.url('123')).then(async () => {
      await logoutGenerator.next().value;
    });

    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('clearBasket saga test', () => {
    const logoutGenerator = clearBasket();
    expect(logoutGenerator.next().value).toBeDefined();
    mock.onPost(apiEndpoints.BASKET.DELETE_BASKET.url).replyOnce(200, {
      cartItems: BASKET_ALL_ITEM.cartItems,
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      totalItems: BASKET_ALL_ITEM.totalItems
    });
    axios.post(apiEndpoints.BASKET.DELETE_BASKET.url).then(async () => {
      await logoutGenerator.next().value;
    });

    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
  });

  it('updateBasketItemQuantity saga test', () => {
    const updateBasketItemQuantityGenerator = updateBasketItemQuantity({
      type: 'string',
      payload: {
        cartItems: [{ id: null, quantity: 1 }]
      }
    });
    expect(updateBasketItemQuantityGenerator.next().value).toBeDefined();
    expect(updateBasketItemQuantityGenerator.next().value).toBeDefined();
    expect(updateBasketItemQuantityGenerator.next().value).toBeDefined();
    expect(updateBasketItemQuantityGenerator.next().value).toBeDefined();
    expect(updateBasketItemQuantityGenerator.next().value).toBeUndefined();
  });

  it('removeBasketItem saga test', () => {
    const removeBasketItemGenerator = removeBasketItem({
      type: 'string',
      payload: {
        cartItems: [{ id: null, action: 'added', group: '' }]
      }
    });
    expect(removeBasketItemGenerator.next().value).toBeDefined();
    expect(removeBasketItemGenerator.next().value).toBeDefined();
    expect(removeBasketItemGenerator.next().value).toBeUndefined();
  });
});
