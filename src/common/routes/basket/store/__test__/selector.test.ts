import {
  getBasketItem,
  getBasketItemsImages,
  getCartID,
  isCheckoutButtonDisable
} from '@basket/store/selector';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import appState from '@store/states/app';

describe('Basket Selector', () => {
  it('isCheckoutButtonDisable test', () => {
    expect(isCheckoutButtonDisable(BASKET_ALL_ITEM.cartItems)).toBeDefined();
  });
  it('isCheckoutButtonDisable test when basketItem is undefined', () => {
    expect(isCheckoutButtonDisable(undefined)).toBeDefined();
  });
  it('isCheckoutButtonDisable test when basketItem is outOfStock', () => {
    BASKET_ALL_ITEM.cartItems[0].status = 'OutOfStock';
    expect(isCheckoutButtonDisable(BASKET_ALL_ITEM.cartItems)).toBeDefined();
  });

  it('getBasketItem test', () => {
    expect(getBasketItem('123457', BASKET_ALL_ITEM.cartItems)).toBeDefined();
  });
  it('getBasketItem test when Id is worng', () => {
    expect(getBasketItem('1234575', BASKET_ALL_ITEM.cartItems)).toBeDefined();
  });

  it('getBasketItemsImages test', () => {
    expect(getBasketItemsImages(BASKET_ALL_ITEM.cartItems[0])).toBeDefined();
  });

  it('getBasketItemsImages test when cartItems is not present', () => {
    const basketItem = { ...BASKET_ALL_ITEM.cartItems[0] };
    delete basketItem.cartItems;
    expect(getBasketItemsImages(basketItem)).toBeDefined();
  });
  it('getCartID test when cartItems is not present', () => {
    expect(getCartID(appState().basket)).toBeDefined();
  });
});
