import {
  deleteBasketItemService,
  deleteBasketService,
  fetchBasketService,
  updateBasketService
} from '@basket/store/services';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

describe('basket Service', () => {
  const mock = new MockAdapter(axios);
  const loginPayload = {};
  it('fetchBasketService success test', async () => {
    mock
      .onGet(apiEndpoints.BASKET.GET_BASKET_DATA.url())
      .replyOnce(200, loginPayload);
    axios
      .get(apiEndpoints.BASKET.GET_BASKET_DATA.url())
      .then(async () => {
        const result = await fetchBasketService({});
        expect(result).toEqual(loginPayload);
      })
      .catch(error => logError(error));
  });

  it('deleteBasketService success test', async () => {
    mock
      .onPost(apiEndpoints.BASKET.DELETE_BASKET.url)
      .replyOnce(200, loginPayload);
    axios
      .post(apiEndpoints.BASKET.DELETE_BASKET.url)
      .then(async () => {
        const result = await deleteBasketService();
        expect(result).toEqual(loginPayload);
      })
      .catch(error => logError(error));
  });

  it('deleteBasketItemService success test', async () => {
    mock
      .onPost(apiEndpoints.BASKET.DELETE_BASKET.url)
      .replyOnce(200, loginPayload);
    axios.post(apiEndpoints.BASKET.DELETE_BASKET.url).then(async () => {
      const result = await deleteBasketItemService({
        cartItems: [{ id: null, action: 'action', group: '' }]
      });
      expect(result).toEqual(loginPayload);
    });
  });

  it('updateBasketService success test', async () => {
    mock
      .onPost(apiEndpoints.BASKET.DELETE_BASKET.url)
      .replyOnce(200, loginPayload);
    axios
      .post(apiEndpoints.BASKET.DELETE_BASKET.url)
      .then(async () => {
        const result = await updateBasketService({
          cartItems: [{ id: null, quantity: 1 }]
        });
        expect(result).toEqual(loginPayload);
      })
      .catch(error => logError(error));
  });
});
