import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@basket/store/constants';
import {
  IBasketItem,
  ICartSummary,
  IFetchBasketArgs,
  INotification,
  IPriceNotificationStrip,
  IRemoveBasketItemParams,
  IResponse,
  ISetDeleteBasketItemModalPayload,
  IUpdateBasketItemPayload
} from '@basket/store/types';
import { IError } from '@src/common/store/types/common';

export default {
  fetchBasket: actionCreator<IFetchBasketArgs>(
    CONSTANTS.FETCH_BASKET_REQUESTED
  ),
  fetchBasketLoading: actionCreator(CONSTANTS.FETCH_BASKET_LOADING),

  setBasketData: actionCreator<IResponse>(CONSTANTS.SET_BASKET_DATA),
  setCartSummary: actionCreator<ICartSummary[]>(
    CONSTANTS.SET_CART_SUMMARY_DATA
  ),
  fetchBasketSuccess: actionCreator(CONSTANTS.FETCH_BASKET_SUCCESS),
  fetchBasketError: actionCreator<IError>(CONSTANTS.FETCH_BASKET_ERROR),

  setCheckoutButtonStatus: actionCreator<IBasketItem[]>(
    CONSTANTS.SET_CHECKOUT_BUTTON_STATUS
  ),
  deletesetCartItems: actionCreator(CONSTANTS.DELETE_CART_ITEMS),
  removeBasketNotification: actionCreator<INotification>(
    CONSTANTS.REMOVE_BASKET_NOTIFICATION
  ),
  clearBasket: actionCreator<void>(CONSTANTS.CLEAR_BASKET_DATA),
  resetBasket: actionCreator<void>(CONSTANTS.RESET_BASKET_DATA),
  requestClearBasket: actionCreator(CONSTANTS.DELETE_BASKET_REQUESTED),
  addBasketNotification: actionCreator<INotification>(
    CONSTANTS.ADD_BASKET_NOTIFICATION
  ),

  updateBasketItemRequested: actionCreator(
    CONSTANTS.UPDATE_BASKET_ITEM_REQUESTED
  ),
  updateBasketSuccess: actionCreator(CONSTANTS.UPDATE_BASKET_SUCCESS),
  updateBasketItem: actionCreator<IUpdateBasketItemPayload>(
    CONSTANTS.UPDATE_BASKET_ITEM_QUANTITY
  ),
  updateBasketItemSuccess: actionCreator<{
    id: string | null;
    quantity: number;
  }>(CONSTANTS.UPDATE_BASKET_ITEM_QUANTITY_SUCCESS),
  updateBasketItemError: actionCreator<IError | null>(
    CONSTANTS.UPDATE_BASKET_ITEM_QUANTITY_SUCCESS_ERROR
  ),

  removeBasketItem: actionCreator<IRemoveBasketItemParams>(
    CONSTANTS.REMOVE_BASKET_ITEM
  ),
  removeBasketItemSuccess: actionCreator(CONSTANTS.REMOVE_BASKET_ITEM_SUCCESS),
  removeBasketItemError: actionCreator<null | IError>(
    CONSTANTS.REMOVE_BASKET_ITEM_ERROR
  ),
  clearRemoveBasketItemError: actionCreator(
    CONSTANTS.CLEAR_REMOVE_BASKET_ITEM_ERROR
  ),

  setDeleteBasketItemModal: actionCreator<ISetDeleteBasketItemModalPayload>(
    CONSTANTS.SET_DELETE_BASKET_ITEM_MODAL
  ),
  setDeleteBasketModal: actionCreator<boolean>(
    CONSTANTS.SET_DELETE_BASKET_MODAL
  ),
  clearBasketSuccess: actionCreator(CONSTANTS.CLEAR_BASKET_SUCCESS),
  clearBasketError: actionCreator<null | IError>(CONSTANTS.CLEAR_BASKET_ERROR),

  setUndoAbleItems: actionCreator<string | null>(CONSTANTS.SET_UNDOABLE_ITEMS),

  resetUndoAbleItems: actionCreator(CONSTANTS.RESET_UNDOABLE_ITEMS),

  setShowDeleteCardNotification: actionCreator(
    CONSTANTS.SET_DELETE_CART_NOTIFICATION
  ),
  setStickySummary: actionCreator<boolean>(CONSTANTS.SET_STICKY_SUMMARY),

  showBasketSideBar: actionCreator<boolean>(CONSTANTS.SHOW_BASKET_SIDE_BAR),
  setCartRestrictionMessage: actionCreator<boolean>(
    CONSTANTS.SET_CART_RESTRICTION_MESSAGE_VISIBILITY
  ),
  setPriceNotificationStrip: actionCreator<IPriceNotificationStrip>(
    CONSTANTS.SET_PRICE_NOTIFICATION_STRIP
  ),
  validateCartPrice: actionCreator(CONSTANTS.VALIDATE_CART_PRICE),
  proceedToCheckoutLoading: actionCreator<boolean>(
    CONSTANTS.PROCEED_TO_CHECKOUT_LOADING
  )
};
