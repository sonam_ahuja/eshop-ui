export enum ITEM_AVAILABILITY_STATUS {
  // OUT_OF_STOCK = 'OutOfStock',
  OUT_OF_STOCK = 'OUT_OF_STOCK',
  ACTIVE = 'Active',
  SAVED_FOR_LATER = 'SavedForLater',
  // PRE_ORDER = 'PreOrder',
  PRE_ORDER = 'PRE_ORDER'
}

export enum ITEM_TYPE {
  BUNDLE = 'bundle',
  ADDON = 'addon',
  SIM = 'SIM',
  SERVICE = 'service',
  DISCOUNT = 'discount',
  DEVICE = 'device',
  BUNDLE_PRODUCT = 'bundledProduct',
  TARIFF = 'tariff',
  TOPUP = 'topup'
}

export enum ITEM_PRICE_TYPE {
  RECURRING_FEE = 'recurringFee',
  UPFRONT = 'upfrontPrice',
  BASEPRICE = 'basePrice'
}

export enum BASKET_API_ACTION {
  DELETE_ITEM_BASKET = 'delete',
  ADD_ITEM_BASKET = 'add'
}

export enum BASKET_GROUP {
  TARIFF = 'tariff',
  DEVICE = 'device'
}

export enum INSTALLMENT_PERIOD {
  MONTH = 'month'
}

export enum VIRTUAL_BASKET {
  ID = 'virtualBasketId'
}
