import {
  IBasketItem,
  IBasketState,
  ICartSummary,
  IPriceNotificationStrip,
  IResponse,
  ISetDeleteBasketItemModalPayload
} from '@src/common/routes/basket/store/types';
import CONSTANTS from '@basket/store/constants';
import initialState from '@basket/store/state';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import { getBasketItem, isCheckoutButtonDisable } from '@basket/store/selector';
import { StateType } from 'typesafe-actions';
import { IError } from '@src/common/store/types/common';

const reducers = {
  [CONSTANTS.SET_BASKET_DATA]: (state: IBasketState, payload: IResponse) => {
    const { cartItems: basketItems, cartSummary, totalItems } = payload;

    const pagination = state.pagination;
    pagination.total = totalItems ? totalItems : 0;
    Object.assign(state, {
      basketItems,
      cartSummary,
      pagination
    });
  },
  [CONSTANTS.FETCH_BASKET_LOADING]: (state: IBasketState) => {
    state.showAppShell = true;
    state.error = null;
    state.showRemoveItemNotification = false;
  },
  [CONSTANTS.FETCH_BASKET_ERROR]: (state: IBasketState, payload: IError) => {
    state.error = payload;
    state.showAppShell = false;
  },
  [CONSTANTS.FETCH_BASKET_SUCCESS]: (state: IBasketState) => {
    state.showAppShell = false;
    state.error = null;
  },

  [CONSTANTS.UPDATE_BASKET_ITEM_REQUESTED]: (state: IBasketState) => {
    state.loading = true;
    state.error = null;
  },

  [CONSTANTS.UPDATE_BASKET_SUCCESS]: (state: IBasketState) => {
    state.loading = false;
    state.error = null;
  },

  [CONSTANTS.RESET_BASKET_DATA]: (state: IBasketState) => {
    state.basketItems = [];
    state.cartSummary = [];
    state.pagination.total = 0;
  },

  [CONSTANTS.SET_CART_SUMMARY_DATA]: (
    state: IBasketState,
    payload: ICartSummary[]
  ) => {
    state.cartSummary = payload;
  },
  [CONSTANTS.SET_DELETE_BASKET_ITEM_MODAL]: (
    state: IBasketState,
    payload: ISetDeleteBasketItemModalPayload
  ) => {
    state.removeBasketItem.modalOpen = payload.showModal;
    state.removeBasketItem.selectedItem = payload.selectedItem;
    state.removeBasketItem.loading = payload.resetModalLoading;
  },

  [CONSTANTS.SET_CHECKOUT_BUTTON_STATUS]: (
    state: IBasketState,
    payload: IBasketItem[]
  ) => {
    state.isCheckoutEnable = isCheckoutButtonDisable(payload);
  },

  [CONSTANTS.REMOVE_BASKET_ITEM]: (state: IBasketState) => {
    state.removeBasketItem.error = null;
    state.removeBasketItem.loading = true;
  },
  [CONSTANTS.REMOVE_BASKET_ITEM_SUCCESS]: (state: IBasketState) => {
    state.removeBasketItem.error = null;
    state.removeBasketItem.loading = false;
    state.removeBasketItem.modalOpen = false;
  },
  [CONSTANTS.REMOVE_BASKET_ITEM_ERROR]: (
    state: IBasketState,
    payload: IError
  ) => {
    state.removeBasketItem.error = payload;
    state.removeBasketItem.loading = false;
    state.removeBasketItem.modalOpen = false;
  },

  [CONSTANTS.SET_DELETE_BASKET_MODAL]: (
    state: IBasketState,
    payload: boolean
  ) => {
    state.clearBasket.modalOpen = payload;
  },
  [CONSTANTS.CLEAR_BASKET_DATA]: (state: IBasketState) => {
    state.clearBasket.error = null;
    state.clearBasket.loading = true;
    state.clearBasket.modalOpen = true;
  },
  [CONSTANTS.CLEAR_BASKET_SUCCESS]: (state: IBasketState) => {
    state.clearBasket.error = null;
    state.clearBasket.loading = false;
    state.clearBasket.modalOpen = false;
  },
  [CONSTANTS.CLEAR_BASKET_ERROR]: (state: IBasketState, payload: IError) => {
    state.clearBasket.error = payload;
    state.clearBasket.loading = false;
    state.clearBasket.modalOpen = false;
  },

  [CONSTANTS.CLEAR_REMOVE_BASKET_ITEM_ERROR]: (state: IBasketState) => {
    state.removeBasketItem.error = null;
  },

  [CONSTANTS.UPDATE_BASKET_ITEM_QUANTITY_SUCCESS]: (
    state: IBasketState,
    { id, quantity }: { id: string; quantity: number }
  ) => {
    const idx = state.basketItems.findIndex((basketItem: IBasketItem) => {
      return basketItem.id === id;
    });
    if (idx !== -1 && state.basketItems[idx].quantity) {
      state.basketItems[idx].quantity = quantity;
    }
  },

  [CONSTANTS.SET_UNDOABLE_ITEMS]: (
    state: IBasketState,
    payload: string | null
  ) => {
    if (payload) {
      state.undoAbleItems = [];
      state.undoAbleItems = getBasketItem(payload, state.basketItems);
    }
  },

  [CONSTANTS.RESET_UNDOABLE_ITEMS]: (state: IBasketState) => {
    state.undoAbleItems = [];
  },

  [CONSTANTS.SET_DELETE_CART_NOTIFICATION]: (state: IBasketState) => {
    state.showRemoveItemNotification = true;
  },
  [CONSTANTS.SET_STICKY_SUMMARY]: (state: IBasketState, payload: boolean) => {
    state.stickySummary = payload;
  },
  [CONSTANTS.SHOW_BASKET_SIDE_BAR]: (state: IBasketState, payload: boolean) => {
    state.showBasketSideBar = payload;
  },
  [CONSTANTS.SET_CART_RESTRICTION_MESSAGE_VISIBILITY]: (
    state: IBasketState,
    payload: boolean
  ) => {
    state.showCartRestrictionMessage = payload;
  },
  [CONSTANTS.SET_PRICE_NOTIFICATION_STRIP]: (
    state: IBasketState,
    payload: IPriceNotificationStrip
  ) => {
    state.priceNotificationStrip = payload;
  },
  [CONSTANTS.PROCEED_TO_CHECKOUT_LOADING]: (
    state: IBasketState,
    payload: boolean
  ) => {
    state.proceedToCheckoutLoading = payload;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IBasketState,
  AnyAction
>;

export type BasketState = StateType<typeof withProduce>;
