import { isBrowser, logError } from '@common/utils';
import {
  IFetchBasketArgs,
  IRemoveBasketItemParams,
  IResponse,
  IUpdateBasketItemPayload,
  IValidateCartPriceResponse
} from '@basket/store/types';
import { Effect } from 'redux-saga';
import actions from '@basket/store/actions';
import history from '@src/client/history';
import constants from '@basket/store/constants';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  deleteBasketItemService,
  deleteBasketService,
  fetchBasketService,
  updateBasketService,
  validateCartPriceService
} from '@basket/store/services';
import checkoutActions from '@checkout/store/actions';
import { getValueFromLocalStorage } from '@src/common/utils/localStorage';
import routeConstants from '@common/constants/routes';
import CONSTANTS from '@common/constants/appConstants';
import { sendOnBasketLoadEvent, sendonBasketLoadTrigger } from '@events/basket';
import store from '@common/store';
import { getBasketTranslation } from '@src/common/store/common';
import { IBasket as IBasketTranslation } from '@src/common/store/types/translation';
import { commonAction } from '@store/actions';

export type SagaIterator = IterableIterator<
  Effect | Effect[] | Promise<IResponse | Error>
>;

export function* fetchBasket(action: {
  type: string;
  payload: IFetchBasketArgs;
}): SagaIterator {
  try {
    const { payload } = action;
    yield put(actions.fetchBasketLoading());
    const response = yield fetchBasketService({
      showFullPageError: payload.showFullPageError,
      virtualBasketId: payload.virtualBasketId
    });

    const isUserLoggedIn = getValueFromLocalStorage(
      CONSTANTS.IS_USER_LOGGED_IN
    );
    const {
      enabled
    } = store.getState().configuration.cms_configuration.modules.checkout.identityVerification;

    if (response.cartItems) {
      const { cartItems, cartSummary, totalItems } = response;

      yield put(
        actions.setBasketData({
          cartItems,
          cartSummary,
          totalItems
        })
      );
      yield put(actions.setCheckoutButtonStatus(cartItems));

      if (payload.isCalledFromCheckout && isBrowser && isUserLoggedIn) {
        yield put(checkoutActions.isCreditCheckRequired());
        yield put(checkoutActions.evaluateCartSummaryPrice(cartSummary));
      }

      if (
        payload.isCalledFromCheckout &&
        isBrowser &&
        !isUserLoggedIn &&
        !enabled &&
        history
      ) {
        history.push(routeConstants.BASKET);
      }
    } else {
      if (payload.isCalledFromCheckout && history) {
        history.push(routeConstants.BASKET);
      }
      yield put(actions.resetBasket());
    }

    if (payload.isFromBasket) {
      sendonBasketLoadTrigger();
      sendOnBasketLoadEvent();
    }

    yield put(actions.resetUndoAbleItems());
    yield put(actions.fetchBasketSuccess());
  } catch (error) {
    yield put(actions.fetchBasketError(error));
  }
}

export function* clearBasket(): SagaIterator {
  try {
    yield call(deleteBasketService);
    yield put(actions.resetBasket());
    yield put(actions.resetUndoAbleItems());
    yield put(actions.setShowDeleteCardNotification());
    yield put(actions.clearBasketSuccess());
    sendOnBasketLoadEvent();
  } catch (error) {
    yield put(actions.clearBasketError(error));
  }
}

export function* updateBasketItemQuantity({
  payload
}: {
  type: string;
  payload: IUpdateBasketItemPayload;
}): Generator {
  try {
    yield put(actions.updateBasketItemRequested());
    const { cartItems, cartSummary, totalItems } = yield updateBasketService(
      payload
    );
    const { id, quantity } = payload.cartItems[0];
    yield put(actions.updateBasketItemSuccess({ id, quantity }));
    yield put(actions.setBasketData({ cartItems, cartSummary, totalItems }));
    sendOnBasketLoadEvent();
  } catch (error) {
    // no use of this can be removed but once remove throw from service after that it can be removed
    yield put(actions.updateBasketItemError(error));
  } finally {
    yield put(actions.updateBasketSuccess());
  }
}
export function* removeBasketItem({
  payload
}: {
  type: string;
  payload: IRemoveBasketItemParams;
}): Generator {
  try {
    const {
      cartItems,
      cartSummary,
      totalItems
    } = yield deleteBasketItemService(payload);
    yield put(actions.setUndoAbleItems(payload.cartItems[0].id));
    yield put(actions.setShowDeleteCardNotification());
    yield put(actions.setBasketData({ cartItems, cartSummary, totalItems }));
    yield put(actions.setCheckoutButtonStatus(cartItems));
    if (totalItems < 2) {
      yield put(actions.setCartRestrictionMessage(false));
    }
    yield put(actions.removeBasketItemSuccess());
  } catch (error) {
    yield put(actions.removeBasketItemError(error));
  }
}

export function* validateCartPrice(): Generator {
  try {
    yield put(actions.proceedToCheckoutLoading(true));
    const response: IValidateCartPriceResponse = yield validateCartPriceService();
    const { priceChanged, cartItems, cartSummary, totalItems } = response;
    const basketTranslation: IBasketTranslation = yield select(
      getBasketTranslation
    );
    const priceChangedMessage: string = basketTranslation.priceChanged;
    yield put(
      actions.setPriceNotificationStrip({
        show: priceChanged,
        notification: priceChangedMessage
      })
    );
    if (history && priceChanged && cartItems && cartSummary && totalItems) {
      yield put(
        actions.setBasketData({
          cartItems,
          cartSummary,
          totalItems
        })
      );
      yield put(actions.setCheckoutButtonStatus(cartItems));
      yield put(actions.resetUndoAbleItems());
      if (!history.location.pathname.includes(routeConstants.BASKET)) {
        history.push(routeConstants.BASKET);
      }
    } else {
      yield put(commonAction.goToNextRoute());
    }
  } catch (error) {
    logError(error);
  } finally {
    yield put(actions.proceedToCheckoutLoading(false));
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(constants.FETCH_BASKET_REQUESTED, fetchBasket);
  yield takeLatest(constants.CLEAR_BASKET_DATA, clearBasket);
  yield takeLatest(constants.REMOVE_BASKET_ITEM, removeBasketItem);
  yield takeLatest(constants.VALIDATE_CART_PRICE, validateCartPrice);
  yield takeLatest(
    constants.UPDATE_BASKET_ITEM_QUANTITY,
    updateBasketItemQuantity
  );
}
export default watcherSaga;
