import { IBasketItem, IBasketState } from '@common/routes/basket/store/types';
import { ITEM_AVAILABILITY_STATUS, ITEM_TYPE } from '@basket/store/enums';
import { checkSafariAndIEBrowser } from '@src/client/DOMUtils';
import { IMAGE_TYPE } from '@src/common/store/enums';

export const isCheckoutButtonDisable = (
  cartItems: IBasketItem[] | undefined
): boolean => {
  return !!(
    cartItems &&
    getFlattenBasketItems(cartItems)
      .map(item => item.status)
      .indexOf(ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK) > -1
  );
};

export const getBasketItem = (
  id: string | null,
  basketItems: IBasketItem[]
): IBasketItem[] => {
  return getFlattenBasketItems(basketItems).filter(
    (basketItem: IBasketItem) => id === basketItem.id
  );
};

export const getBasketItemsImages = (basketItem: IBasketItem): string[] => {
  const isSAfariBrowser = checkSafariAndIEBrowser();
  const imageType = isSAfariBrowser ? IMAGE_TYPE.PNG : IMAGE_TYPE.WEBP;
  const images: string[] = [];
  if (basketItem.product.imageUrl) {
    images.push(`${basketItem.product.imageUrl}&q=90&w=50&t=${imageType}`);
  }

  if (
    basketItem.group.toLowerCase() !== ITEM_TYPE.TARIFF &&
    basketItem.cartItems &&
    basketItem.cartItems.length
  ) {
    basketItem.cartItems.forEach((basketItemChild: IBasketItem) => {
      if (basketItemChild.product.imageUrl) {
        images.push(
          `${basketItemChild.product.imageUrl}&q=90&w=50&t=${imageType}`
        );
      }
    });
  }

  return images;
};

export const getFlattenBasketItems = (
  basketItems: IBasketItem[]
): IBasketItem[] => {
  return basketItems.reduce(
    (basketItemsFlattened: IBasketItem[], basketItem) => {
      if (basketItem.cartItems && basketItem.cartItems.length) {
        return basketItemsFlattened.concat([
          ...getFlattenBasketItems(basketItem.cartItems),
          basketItem
        ]);
      }

      return basketItemsFlattened.concat(basketItem);
    },
    []
  );
};

export const getCartID = (basket: IBasketState) => {
  return basket.cartId;
};
