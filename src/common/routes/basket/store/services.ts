import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import {
  IRemoveBasketItemParams,
  IResponse,
  IUpdateBasketItemPayload,
  IValidateCartPriceResponse
} from '@basket/store/types';
import { logError } from '@src/common/utils';
import { IError } from '@src/common/store/types/common';

export const fetchBasketService = async (payload: {
  showFullPageError?: boolean;
  virtualBasketId?: string | null;
}): Promise<IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.BASKET.GET_BASKET_DATA.method.toLowerCase()
    ](apiEndpoints.BASKET.GET_BASKET_DATA.url(payload.virtualBasketId), {
      showFullPageError: payload.showFullPageError
    });
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const deleteBasketService = async (): Promise<IResponse | IError> => {
  try {
    return await apiCaller.delete(apiEndpoints.BASKET.DELETE_BASKET.url);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const deleteBasketItemService = async (
  payload: IRemoveBasketItemParams
): Promise<IResponse | IError> => {
  try {
    return await apiCaller.patch(
      apiEndpoints.BASKET.DELETE_BASKET_ITEM.url,
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const updateBasketService = async (
  payload: IUpdateBasketItemPayload
): Promise<{} | IError> => {
  try {
    return await apiCaller.patch(
      apiEndpoints.BASKET.UPDATE_BASKET_ITEM.url,
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const validateCartPriceService = async (): Promise<
  IValidateCartPriceResponse | IError
> => {
  try {
    return await apiCaller.patch(
      apiEndpoints.BASKET.VALIDATE_CART_PRICE.url,
      {}
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};
