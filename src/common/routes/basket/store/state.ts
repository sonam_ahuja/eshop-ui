import { IBasketState } from '@basket/store/types';

export default (): IBasketState => ({
  priceNotificationStrip: {
    show: false,
    notification: ''
  },
  showCartRestrictionMessage: false,
  showRemoveItemNotification: false,
  basketItems: [],
  cartId: null,
  loading: false,
  error: null,
  pagination: {
    total: 0,
    current: null
  },
  isCheckoutEnable: true,
  proceedToCheckoutLoading: false,
  cartSummary: [],
  notification: [],
  undoItems: [],
  oldCartId: null,
  removeBasketItem: {
    modalOpen: false,
    error: null,
    loading: false,
    selectedItem: null
  },
  clearBasket: {
    modalOpen: false,
    error: null,
    loading: false
  },
  undoAbleItems: [],
  showAppShell: true,
  stickySummary: true,
  showBasketSideBar: false
});
