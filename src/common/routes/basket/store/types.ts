import { IError } from '@src/common/store/types/common';

export type notificationType =
  | 'recent'
  | 'remove'
  | 'removedAll'
  | 'outOfStock';

export interface IFetchBasketArgs {
  isCalledFromCheckout?: boolean;
  isEventSend?: boolean;
  isFromBasket?: boolean;
  showFullPageError?: boolean;
  virtualBasketId?: string | null;
}

export interface IPriceAlteration {
  priceType: string;
  duration: {
    timePeriod: string;
    type: string;
  };
  price: number;
  subPriceAlterations?: ISubPriceAlterations[];
}

export interface ISubPriceAlterations {
  price: number;
  priceType: string;
  name: string;
  label: string;
}

export interface ITotalPrice {
  priceAlterations: IPriceAlteration[];
  priceType: string;
  totalPrice: number;
  price: number;
}

export interface ICharacteristic {
  name: string;
  value: string;
}
export interface IProduct {
  id?: string;
  imageUrl?: string;
  name: string;
  brandName?: string;
  description: string;
  shortDescription: string;
  productGroupId: string;
  specId: string;
  categorySlug: string;
  characteristic?: ICharacteristic[];
  variantName?: string;
  categoryId?: string;
  tariffLoyaltyLabel?: string;
}

export interface ICartTerms {
  name: string;
  duration: {
    timePeriod: string;
    type: string;
  };
}

export interface IItemPrice {
  priceAlterations: IPriceAlteration[];
  price: number;
  totalPrice: number;
  priceType: string;
  duration?: IDuration;
}

export interface IDuration {
  timePeriod: number;
  type: string;
}
export interface IBasketItem {
  addons?: IAddons;
  id: string;
  isModifiable: boolean;
  status: string;
  totalPrice: ITotalPrice[];
  quantity?: number | null;
  group: string;
  product: IProduct;
  cartTerms: ICartTerms[];
  itemPrice: IItemPrice[];
  cartItems: IBasketItem[];
}

export interface IAddons {
  [key: string]: IAddon;
}

export interface IAddon {
  categoryName: string;
  cartItems: IBasketItem[];
}
export interface INotification {
  content: string[];
  type: notificationType;
  isOpen: boolean;
  item?: IBasketItem;
}
export interface ICartSummaryPriceAlteration extends IPriceAlteration {
  details: {
    name: string;
    value: string;
  }[];
}

export interface ICartSummary {
  priceAlterations: ICartSummaryPriceAlteration[];
  priceType: string;
  price: number;
  totalPrice: number;
  cartRecurringBreakUp?: ICartRecurringBreakUp[];
}

export interface ICartRecurringBreakUp {
  afterDuration: IAfterDuration;
  afterDurationPrice: number;
}

export interface IAfterDuration {
  timePeriod: number;
  type: string;
}

export interface IBasketState {
  priceNotificationStrip: IPriceNotificationStrip;
  showCartRestrictionMessage: boolean;
  basketItems: IBasketItem[];
  cartId: string | null;
  loading: boolean;
  error: IError | null;
  pagination: {
    total: number;
    current: number | null;
  };
  cartSummary: ICartSummary[];
  notification: INotification[];
  undoItems: IBasketItem[];
  oldCartId: string | null;
  removeBasketItem: {
    modalOpen: boolean;
    error: IError | null;
    loading: boolean;
    selectedItem: IBasketItem | null;
  };
  clearBasket: {
    modalOpen: boolean;
    error: IError | null;
    loading: boolean;
  };
  isCheckoutEnable: boolean;
  proceedToCheckoutLoading: boolean;
  undoAbleItems: IBasketItem[];
  showRemoveItemNotification: boolean;
  showAppShell: boolean;
  stickySummary: boolean;
  showBasketSideBar: boolean;
}

export interface IPriceNotificationStrip {
  show: boolean;
  notification: string;
}

export interface IResponse {
  cartItems: IBasketItem[];
  cartSummary: ICartSummary[];
  totalItems: number;
}

export interface IValidateCartPriceResponse {
  priceChanged: boolean;
  cartItems?: IBasketItem[];
  cartSummary?: ICartSummary[];
  totalItems?: number;
}

export interface IRemoveBasketItemParams {
  cartItems: { id: string | null; action: string; group: string }[];
}

export interface IUpdateBasketItemPayload {
  cartItems: { id: string | null; quantity: number }[];
}

export interface IApiError {
  isObject: boolean;
  message: string;
  status: string;
  response: object | null;
}

export interface ISetDeleteBasketItemModalPayload {
  showModal: boolean;
  selectedItem: IBasketItem | null;
  resetModalLoading: boolean;
}
