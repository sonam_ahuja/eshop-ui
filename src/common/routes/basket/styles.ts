import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';
import { StyledBreadcrumbWrap } from '@src/common/components/CatalogBreadCrumb/styles';

export const StyledBasketContainer = styled.div`
  background: ${colors.fogGray};
  display: flex;
  flex-direction: column;
  flex: 1;
  .stripMessageWrapper {
    &:empty {
      & + div {
        margin-top: 6.05rem;
      }
    }
    & + div {
      margin-top: 0;
    }
  }
  .dt_stripMessage {
    margin-bottom: 1rem;
    color: ${colors.ironGray};
    padding-top: 1rem;
    padding-bottom: 1rem;
    display: flex;
    align-items: center;
    .leftContainer {
      margin-top: 0;
    }
    .mainContent {
      align-items: center;
    }
    & + .empty-card-wrap {
      margin-top: 0;
    }
  }

  .basketMain {
    flex: 1;
    display: flex;
    flex-direction: column;
    /* padding-right: 18rem; */
    padding-bottom: 0;
    padding-bottom: 3rem;
    .basketContent {
      flex: 1;
      padding: 0 1.25rem;

      ${StyledBreadcrumbWrap} {
        padding: 1.25rem 0 1rem 0;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    flex-direction: row;
    .dt_stripMessage {
      padding-top: 1.25rem;
      padding-bottom: 1.25rem;
    }
    .basketMain {
      padding-bottom: 0;
      .basketContent {
        padding: 0 2.2em;
      }
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .basketMain {
      .basketContent {
        padding: 0 3em;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .basketMain {
      .basketContent {
        padding: 0 4.9em;
      }
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    .basketMain {
      .basketContent {
        padding: 0 5.5rem;
      }
    }
  }
`;
