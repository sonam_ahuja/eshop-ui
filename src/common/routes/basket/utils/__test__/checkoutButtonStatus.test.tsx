import { isCheckoutButtonDisable } from '@basket/utils/checkoutButtonStatus';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

import { ITEM_AVAILABILITY_STATUS } from '../../store/enums';

describe('<getInstallmentsText />', () => {
  test('isCheckoutButtonDisable should return false when item type is not Out of stock', () => {
    expect(isCheckoutButtonDisable(BASKET_ALL_ITEM.cartItems)).toBe(false);
  });
  test('isCheckoutButtonDisable should return false when item type Out of stock', () => {
    const basketItems = BASKET_ALL_ITEM.cartItems;
    basketItems[0].status = ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK;
    expect(isCheckoutButtonDisable(basketItems)).toBe(true);
  });
});
