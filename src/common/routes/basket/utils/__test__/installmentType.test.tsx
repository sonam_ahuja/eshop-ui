import { getInstallmentsText } from '@basket/utils/installmentType';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import translationState from '@store/states/translation';

describe('<getInstallmentsText />', () => {
  test('case - 1 ', () => {
    const basketItemforCase1 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase1.itemPrice[0].price = 0;
    basketItemforCase1.itemPrice[0].totalPrice = 0;
    basketItemforCase1.itemPrice[0].priceType = 'recurringFee';
    const element = getInstallmentsText(
      basketItemforCase1,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 2 ', () => {
    const basketItemforCase2 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase2.itemPrice[0].price = 0;
    basketItemforCase2.itemPrice[0].totalPrice = 0;
    basketItemforCase2.itemPrice[0].priceType = 'recurringFee';
    basketItemforCase2.cartTerms = [];
    const element = getInstallmentsText(
      basketItemforCase2,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 3 ', () => {
    const basketItemforCase3 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase3.itemPrice[0].price = 0;
    basketItemforCase3.itemPrice[0].totalPrice = 1;
    basketItemforCase3.itemPrice[0].priceType = 'recurringFee';
    const element = getInstallmentsText(
      basketItemforCase3,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 4 ', () => {
    const basketItemforCase4 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase4.itemPrice[0].price = 0;
    basketItemforCase4.itemPrice[0].totalPrice = 1;
    basketItemforCase4.itemPrice[0].priceType = 'recurringFee';
    basketItemforCase4.cartTerms = [];
    const element = getInstallmentsText(
      basketItemforCase4,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 5 ', () => {
    const basketItemforCase5 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase5.itemPrice[0].price = 0;
    basketItemforCase5.itemPrice[0].totalPrice = 1;
    basketItemforCase5.itemPrice[0].priceType = 'upfront';
    const element = getInstallmentsText(
      basketItemforCase5,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 6', () => {
    const basketItemforCase6 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase6.itemPrice[0].price = 0;
    basketItemforCase6.itemPrice[0].totalPrice = 0;
    basketItemforCase6.itemPrice[0].priceType = 'upfront';
    const totalPrice = basketItemforCase6.totalPrice[0];
    totalPrice.price = 0;
    totalPrice.totalPrice = 0;
    basketItemforCase6.totalPrice = [totalPrice];
    const element = getInstallmentsText(
      basketItemforCase6,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 7', () => {
    const basketItemforCase8 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase8.itemPrice[0].price = 0;
    basketItemforCase8.itemPrice[0].totalPrice = 0;
    basketItemforCase8.itemPrice[0].priceType = 'upfront';
    const totalPrice = basketItemforCase8.totalPrice[0];
    totalPrice.price = 0;
    totalPrice.totalPrice = 1;
    basketItemforCase8.totalPrice = [totalPrice];
    const element = getInstallmentsText(
      basketItemforCase8,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-identical-functions
  test('case - 8', () => {
    const basketItemforCase8 = { ...BASKET_ALL_ITEM.cartItems[0] };
    basketItemforCase8.itemPrice[0].price = 0;
    basketItemforCase8.itemPrice[0].totalPrice = 0;
    basketItemforCase8.itemPrice[0].priceType = '';
    const totalPrice = basketItemforCase8.totalPrice[0];
    totalPrice.price = 0;
    totalPrice.totalPrice = 1;
    basketItemforCase8.totalPrice = [totalPrice];
    const element = getInstallmentsText(
      basketItemforCase8,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 9', () => {
    const basketItemforCase9 = BASKET_ALL_ITEM.cartItems[0];
    basketItemforCase9.itemPrice[0].price = 0;
    basketItemforCase9.itemPrice[0].totalPrice = 0;
    basketItemforCase9.itemPrice[0].priceType = 'upfront';
    const totalPrice = basketItemforCase9.totalPrice[0];
    basketItemforCase9.totalPrice = [totalPrice];
    const element = getInstallmentsText(
      basketItemforCase9,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 10', () => {
    const basketItemforCase10 = BASKET_ALL_ITEM.cartItems[0];
    basketItemforCase10.itemPrice[0].price = 0;
    basketItemforCase10.itemPrice[0].totalPrice = 0;
    basketItemforCase10.itemPrice[0].priceType = 'upfront';
    const totalPrice = basketItemforCase10.totalPrice[0];
    totalPrice.price = 0;
    totalPrice.totalPrice = 1;
    basketItemforCase10.totalPrice = [totalPrice];
    basketItemforCase10.group = 'bundle';
    const element = getInstallmentsText(
      basketItemforCase10,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });

  test('case - 11', () => {
    const basketItemforCase11 = BASKET_ALL_ITEM.cartItems[0];
    basketItemforCase11.itemPrice = [];
    const element = getInstallmentsText(
      basketItemforCase11,
      translationState().cart.basket,
      'INR'
    );
    expect(element).toMatchSnapshot();
  });
});
