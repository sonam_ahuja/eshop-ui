import { IBasketItem } from '@src/common/routes/basket/store/types';
import { ITEM_AVAILABILITY_STATUS } from '@basket/store/enums';

export const isCheckoutButtonDisable = (cartItems: IBasketItem[]) => {
  for (const item of cartItems) {
    if (item.status === ITEM_AVAILABILITY_STATUS.OUT_OF_STOCK) {
      return true;
    }
    if (item.cartItems && item.cartItems.length) {
      isCheckoutButtonDisable(item.cartItems);
    }
  }

  return false;
};
