import { ITotalPrice } from '@basket/store/types';
import { findIndexOnKey } from '@utils/arrayHelper';
import basketConstants from '@basket/constants';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';

export const getPriceTypeIndex = (
  itemPrice: ITotalPrice[],
  priceType: ITEM_PRICE_TYPE
) => {
  return findIndexOnKey(itemPrice, basketConstants.priceType, priceType);
};
