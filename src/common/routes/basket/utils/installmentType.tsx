import React from 'react';
import {
  INSTALLMENT_PERIOD,
  ITEM_PRICE_TYPE,
  ITEM_TYPE
} from '@basket/store/enums';
import ProductPriceDetails from '@basket/Tooltip/ProductPriceDetails/index';
import { convertToFloat } from '@src/common/utils/maths';
import { IBasketItem } from '@basket/store/types';
import { Paragraph } from 'dt-components';
import { IBasket as IBasketTranslation } from '@store/types/translation';
import styled from 'styled-components';
import { getPriceTypeIndex } from '@basket/utils/getPriceTypeIndex';
import { formatCurrency } from '@common/utils/currency';
import { colors } from '@src/common/variables';

const StyledAmount = styled(Paragraph)`
  display: flex;
  align-items: center;

  .iconWrap {
    display: inline-flex;
    i {
      cursor: pointer;
      font-size: 1rem;
      &:focus {
        outline: none;
      }
    }
  }

  .dt_icon {
    margin-left: 5px;
    opacity: 0.8;
    cursor: pointer;
  }
`;

const StyledLoyaltyText = styled(Paragraph).attrs({
  size: 'small',
  weight: 'normal'
})`
  color: ${colors.ironGray};
`;

export const getInstallmentsText = (
  basketItem: IBasketItem,
  basketTranslation: IBasketTranslation,
  currency: string
): React.ReactNode => {
  if (basketItem.itemPrice && basketItem.itemPrice.length) {
    const monthlyIndex = getPriceTypeIndex(
      basketItem.itemPrice,
      ITEM_PRICE_TYPE.RECURRING_FEE
    );
    const upfrontIndex = getPriceTypeIndex(
      basketItem.itemPrice,
      ITEM_PRICE_TYPE.UPFRONT
    );

    const getItemCharName =
      basketItem.cartTerms &&
      basketItem.cartTerms[0] &&
      basketItem.cartTerms[0].name
        ? `${basketItem.cartTerms[0].name} `
        : '';

    const getItemCharInstallment =
      basketItem.cartTerms &&
      basketItem.cartTerms[0] &&
      basketItem.cartTerms[0].duration &&
      basketItem.cartTerms[0].duration.timePeriod
        ? `${basketItem.cartTerms[0].duration.timePeriod}
          ${
            basketItem.cartTerms[0].duration.type === INSTALLMENT_PERIOD.MONTH
              ? basketTranslation.months
              : null
          }`
        : null;

    const checkTotalPriceEquals = (index: number) =>
      convertToFloat(basketItem.itemPrice[index].price) ===
      convertToFloat(basketItem.itemPrice[index].totalPrice);

    if (monthlyIndex !== -1) {
      if (checkTotalPriceEquals(monthlyIndex)) {
        return (
          <>
            {basketItem.group.toLowerCase() === ITEM_TYPE.TARIFF ? (
              basketItem.product.tariffLoyaltyLabel ? (
                <StyledLoyaltyText>
                  {basketItem.product.tariffLoyaltyLabel}
                </StyledLoyaltyText>
              ) : (
                ''
              )
            ) : (
              <>
                {' '}
                {getItemCharInstallment && (
                  <Paragraph size='small'>
                    {`(${getItemCharName}${
                      getItemCharInstallment
                        ? `x${getItemCharInstallment}`
                        : null
                    })`}
                  </Paragraph>
                )}
              </>
            )}
          </>
        );
      } else {
        return (
          <>
            <StyledAmount size='small' className='amount'>
              <div>
                {formatCurrency(
                  basketItem.itemPrice[monthlyIndex].totalPrice,
                  currency
                )}
              </div>
              {basketItem.group.toLowerCase() === ITEM_TYPE.TARIFF ? (
                basketItem.product.tariffLoyaltyLabel ? (
                  <Paragraph size='small'>{`(${
                    basketItem.product.tariffLoyaltyLabel
                  })`}</Paragraph>
                ) : (
                  ''
                )
              ) : (
                <>
                  {' '}
                  {getItemCharInstallment && (
                    <Paragraph size='small'>
                      {`(${getItemCharName}${
                        getItemCharInstallment
                          ? `x${getItemCharInstallment}`
                          : null
                      })`}
                    </Paragraph>
                  )}
                </>
              )}
              {basketItem.quantity && basketItem.quantity > 1 ? (
                <ProductPriceDetails
                  itemPrice={basketItem.itemPrice}
                  group={basketItem.group}
                  ProductName={basketItem.product.name}
                  translation={basketTranslation}
                  currency={currency}
                  isTariff={basketItem.group.toLowerCase() === ITEM_TYPE.TARIFF}
                />
              ) : null}
            </StyledAmount>
            <Paragraph className='lineThrough' size='small'>
              <span>
                {formatCurrency(
                  basketItem.itemPrice[monthlyIndex].price,
                  currency
                )}
              </span>
            </Paragraph>
          </>
        );
      }
    } else {
      if (
        (monthlyIndex === -1 ||
          (basketItem.itemPrice[upfrontIndex].totalPrice === 0 &&
            basketItem.itemPrice[upfrontIndex].price === 0)) &&
        upfrontIndex !== -1
      ) {
        if (
          basketItem.itemPrice[upfrontIndex].totalPrice === 0 &&
          basketItem.itemPrice[upfrontIndex].price === 0
        ) {
          return <Paragraph size='small'>{basketTranslation.free}</Paragraph>;
        } else {
          if (!checkTotalPriceEquals(upfrontIndex)) {
            return (
              <>
                <Paragraph size='small'>
                  {formatCurrency(
                    basketItem.itemPrice[upfrontIndex].totalPrice,
                    currency
                  )}
                </Paragraph>
                <Paragraph className='lineThrough'>
                  <span>
                    {formatCurrency(
                      basketItem.itemPrice[upfrontIndex].price,
                      currency
                    )}
                  </span>
                </Paragraph>
              </>
            );
          }
        }
      } else if (upfrontIndex !== -1 && monthlyIndex !== -1) {
        if (checkTotalPriceEquals(upfrontIndex)) {
          return (
            <Paragraph size='small'>
              {' '}
              {formatCurrency(
                basketItem.itemPrice[upfrontIndex].price,
                currency
              )}
            </Paragraph>
          );
        } else if (!checkTotalPriceEquals(upfrontIndex)) {
          return (
            <>
              <Paragraph size='small'>
                {basketItem.itemPrice[upfrontIndex].totalPrice}
              </Paragraph>
              <Paragraph className='lineThrough'>
                <span>
                  {formatCurrency(
                    basketItem.itemPrice[upfrontIndex].price,
                    currency
                  )}
                </span>
              </Paragraph>
            </>
          );
        }
      }
    }
  }

  return null;
};
