import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import BestDevices, { IProps } from '@category/BestDevices';
import { BestDeviceOffers } from '@mocks/category/category.mock';
import { PAGE_THEME } from '@store/enums';
import { BrowserRouter as Router } from 'react-router-dom';
import appState from '@store/states/app';

describe('<BestDevices />', () => {
  const props: IProps = {
    theme: PAGE_THEME.PRIMARY,
    currency: {
      currencySymbol: 'Ft',
      isPrecede: false,
      locale: 'pl'
    },
    bestDeviceOffers: BestDeviceOffers,
    categoryTranslation: appState().translation.cart.category,
    bestDeviceOffersSlug: '',
    selectedCategoryId: '',
    productListTranslation: appState().translation.cart.productList,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    considerInventory: false,
    openNotificationModal: jest.fn(),
    setDeviceDetailedList: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <Router>
        <ThemeProvider theme={{}}>
          <BestDevices {...props} />
        </ThemeProvider>
      </Router>
    );
    expect(component).toMatchSnapshot();
  });
});
