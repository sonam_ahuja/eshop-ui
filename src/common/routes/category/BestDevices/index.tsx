import htmlKeys from '@common/constants/appkeys';
import { PRICES } from '@productList/store/enum';
import SectionHeading from '@category/common/SectionHeading';
import {
  eligibilityUnavailabilityReasonType,
  IBestDeviceOffers
} from '@category/store/types';
import {
  getCTAText,
  getImage,
  getOutOfStock,
  getPrice,
  getSpecialOffer
} from '@productList/utils/getProductDetail';
import React, { Fragment } from 'react';
import { Column } from '@common/components/Grid/styles';
import {
  ICategoryTranslation,
  IProductListTranslation
} from '@store/types/translation';
import { StyledHorizontalScrollRow } from '@common/components/styles';
import { ITEM_STATUS, pageThemeType } from '@src/common/store/enums';
import { sendClickedProductEvent } from '@events/DeviceList/index';
import { DEVICE_LIST } from '@events/constants/eventName';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration
} from '@src/common/store/types/configuration';
import ProductCard from '@productList/common/ProductCard';

import { StyledBestDevices } from './styles';

export interface IProps {
  considerInventory: boolean;
  bestDeviceOffersSlug: string;
  currency: ICurrencyConfiguration;
  bestDeviceOffers: IBestDeviceOffers[];
  theme: pageThemeType;
  selectedCategoryId: string;
  categoryTranslation: ICategoryTranslation;
  productListTranslation: IProductListTranslation;
  catalogConfiguration: ICatalogConfiguration;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
  setDeviceDetailedList(list: string): void;
}

const getLink = (
  bestDeviceOffersSlug: string,
  variantName: string,
  variantId: string,
  productId: string
) => {
  return `/${bestDeviceOffersSlug}/${variantName}/${productId}/${variantId}`;
};

const isPreOrder = (
  eligibilityUnavailabilityReason: eligibilityUnavailabilityReasonType[],
  considerInventory: boolean
): boolean => {
  if (!eligibilityUnavailabilityReason.includes(ITEM_STATUS.PRE_ORDER)) {
    return false;
  }

  if (
    eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK) &&
    considerInventory
  ) {
    return false;
  }

  return true;
};

const BestDevices = (props: IProps) => {
  const {
    bestDeviceOffers,
    categoryTranslation,
    theme,
    currency,
    bestDeviceOffersSlug,
    setDeviceDetailedList,
    considerInventory,
    productListTranslation,
    catalogConfiguration,
    openNotificationModal
  } = props;

  return (
    <StyledBestDevices theme={theme}>
      <Fragment>
        <SectionHeading size='main' className='sectionHeading'>
          {categoryTranslation.bestDeviceOfferHeading}
        </SectionHeading>
        <StyledHorizontalScrollRow>
          {bestDeviceOffers &&
            bestDeviceOffers.map(productItem => (
              <Column
                className='horizontalScrollableItem'
                colMobile={6}
                colTabletPortrait={3}
                colDesktop={3}
                key={productItem.id}
              >
                <ProductCard
                  heading={productItem.productName}
                  alt={productItem.name}
                  productId={productItem.id}
                  frontImageUrl={getImage(
                    productItem.attachments[htmlKeys.THUMBNAIL_IMAGE],
                    htmlKeys.PRODUCT_IMAGE_FRONT
                  )}
                  frontAndBackImageUrl={getImage(
                    productItem.attachments[htmlKeys.THUMBNAIL_IMAGE],
                    htmlKeys.PRODUCT_IMAGE_FRONT_AND_BACK
                  )}
                  isPreOrder={isPreOrder(
                    productItem.unavailabilityReasonCodes,
                    considerInventory
                  )}
                  onLinkClick={() => {
                    sendClickedProductEvent(
                      bestDeviceOffers,
                      productItem.productId,
                      bestDeviceOffersSlug,
                      DEVICE_LIST.BEST_DEVICE
                    );
                    setDeviceDetailedList(DEVICE_LIST.BEST_DEVICE);
                  }}
                  monthlyAmount={getPrice(
                    productItem.prices,
                    PRICES.MONTHLY,
                    currency
                  )}
                  baseAmount={getPrice(
                    productItem.prices,
                    PRICES.BASE_PRICE,
                    currency
                  )}
                  upfrontAmount={getPrice(
                    productItem.prices,
                    PRICES.UPFRONT,
                    currency
                  )}
                  openNotificationModal={openNotificationModal}
                  headingIconName='ec-solid-gift'
                  specialOffer={getSpecialOffer(productItem.characteristics)}
                  productDetailUrl={getLink(
                    bestDeviceOffersSlug,
                    productItem.name,
                    productItem.id,
                    productItem.productId
                  )}
                  outOfStock={getOutOfStock(
                    productItem.unavailabilityReasonCodes,
                    catalogConfiguration.preorder.considerInventory
                  )}
                  buttonText={getCTAText(
                    productItem.unavailabilityReasonCodes,
                    catalogConfiguration.preorder.considerInventory,
                    productListTranslation
                  )}
                  translation={productListTranslation}
                />
              </Column>
            ))}
        </StyledHorizontalScrollRow>
      </Fragment>
    </StyledBestDevices>
  );
};

export default BestDevices;
