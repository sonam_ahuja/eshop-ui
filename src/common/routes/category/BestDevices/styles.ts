import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Column } from '@common/components/Grid/styles';
import { StyledHorizontalScrollRow } from '@common/components/styles';
import { pageThemeType } from '@src/common/store/enums';
import { theme } from '@category/theme';

export const StyledBestDevices = styled.div<{ theme: pageThemeType }>`
  .sectionHeading {
    padding: 4rem 0 2rem;
    color: ${props => theme[props.theme].sectionHeading.color};
  }
  .title {
    padding: 2.5rem 0 3rem;
    color: ${colors.white};
  }

  ${StyledHorizontalScrollRow} {
    margin: 0 -1.375rem;
    padding: 0 1.25rem;

    ${Column} {
      padding: 0 0.125rem;
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .sectionHeading {
      width: 20rem;
    }
    ${StyledHorizontalScrollRow} {
      margin: 0 -2.25rem;
      padding: 0 2.125rem;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -3rem;
      padding: 0 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .sectionHeading {
      padding-bottom: 4rem;
      /* margin-top: -9.5rem; */
    }

    ${StyledHorizontalScrollRow} {
      margin: 0 -4.9rem;
      padding: 0 4.9rem;
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
  }
`;
