import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import BottomSection, { IProps } from '@category/CategoryList/BottomSection';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';

describe('<BottomSection />', () => {
  const props: IProps = {
    type: 'topSection',
    categories: appState().categories.categories,
    redirectToProductListing: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BottomSection {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when type is not defined', () => {
    const newProps: IProps = { ...props, type: undefined };
    newProps.categories.subCategories = [
      appState().categories.categories,
      appState().categories.categories,
      appState().categories.categories,
      appState().categories.categories,
      appState().categories.categories
    ];
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BottomSection {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with descriptipn', () => {
    const newProps: IProps = { ...props, type: 'topSection' };
    const subCategories = {
      ...appState().categories.categories,
      description: ' hell o'
    };
    newProps.categories.subCategories = [
      subCategories,
      subCategories,
      subCategories,
      subCategories,
      subCategories
    ];
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BottomSection {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when sub categories is undefine ', () => {
    const newProps = { ...props };
    // tslint:disable-next-line:no-any
    newProps.categories.subCategories = null as any;
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BottomSection {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
