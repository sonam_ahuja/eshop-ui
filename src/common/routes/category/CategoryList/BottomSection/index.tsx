import React from 'react';
import { ICategory } from '@category/store/types';
import { Column, Row } from '@common/components/Grid/styles';
import CategoryCard from '@category/common/CategoryCard';

export type listTypes = 'topSection';
export interface IProps {
  type?: listTypes;
  categories: ICategory;
  redirectToProductListing(categoryId: string, slug: string): void;
}

const BottomSection = (props: IProps) => {
  const { redirectToProductListing, categories } = props;

  const bottomSectionCategories = Array.isArray(categories.subCategories)
    ? categories.subCategories.slice(3)
    : [];

  if (bottomSectionCategories.length === 0) {
    return null;
  }

  return (
    <div className='allCategories'>
      <Row>
        {bottomSectionCategories.map(category => (
          <Column
            colMobile={
              category.description && category.description !== '' ? 12 : 6
            }
            colDesktop={
              category.description && category.description !== '' ? 6 : 3
            }
          >
            {' '}
            <CategoryCard
              alt={category.name}
              key={category.id}
              slug={category.slug}
              redirectToProductListingPage={redirectToProductListing}
              title={category.name}
              id={category.id}
              description={category.description}
              highLightProduct={category.highLightProduct}
              imageUrl={category.image}
            />
          </Column>
        ))}
      </Row>
    </div>
  );
};

export default BottomSection;
