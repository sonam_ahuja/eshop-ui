import React from 'react';
import { ICategory } from '@category/store/types';
import { Column, Row } from '@common/components/Grid/styles';
import CategoryCard from '@category/common/CategoryCard';

export type listTypes = 'topSection';
export interface IProps {
  type?: listTypes;
  categories: ICategory;
  redirectToProductListing(categoryId: string, slug: string): void;
}

const TopSection = (props: IProps) => {
  const { redirectToProductListing, categories } = props;
  const topSectionCategories = Array.isArray(categories.subCategories)
    ? categories.subCategories.slice(0, 3)
    : [];

  return (
    <div className='perfectGifts'>
      <Row>
        {topSectionCategories.map(category => (
          <Column
            key={category.id}
            colMobile={
              category.description && category.description !== '' ? 12 : 6
            }
            colTabletLandscape={
              category.description && category.description !== '' ? 6 : 3
            }
            className='columns'
          >
            <CategoryCard
              key={category.id}
              id={category.id}
              slug={category.slug}
              redirectToProductListingPage={redirectToProductListing}
              title={category.name}
              description={category.description}
              highLightProduct={category.highLightProduct}
              imageUrl={category.image}
              alt={category.name}
            />
          </Column>
        ))}
      </Row>
    </div>
  );
};

export default TopSection;
