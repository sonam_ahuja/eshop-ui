import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import CategoryList, { IProps } from '@category/CategoryList';
import appState from '@store/states/app';
import { LIST_TYPE } from '@category/store/enum';

describe('<CategoryList />', () => {
  const props: IProps = {
    type: LIST_TYPE.TOP_SECTION,
    categories: appState().categories.categories,
    redirectToProductListing: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CategoryList {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when type is not defined', () => {
    const newProps: IProps = { ...props, type: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <CategoryList {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
