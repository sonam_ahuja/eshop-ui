import React from 'react';
import { ICategory } from '@category/store/types';
import BottomSection from '@category/CategoryList/BottomSection';
import TopSection from '@category/CategoryList/TopSection';
import { LIST_TYPE } from '@category/store/enum';

import { StyledCategoryList } from './styles';

export interface IProps {
  type?: LIST_TYPE;
  categories: ICategory;
  redirectToProductListing(categoryId: string, slug: string): void;
}

const CategoryList = (props: IProps) => {
  const { type } = props;

  const listEl =
    type === LIST_TYPE.TOP_SECTION ? (
      <TopSection {...props} />
    ) : (
      <BottomSection {...props} />
    );

  return <StyledCategoryList>{listEl}</StyledCategoryList>;
};

export default CategoryList;
