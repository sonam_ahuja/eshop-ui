import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledCategoryList = styled.div`
  .allCategories {
    padding-top: 4rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
  }
`;
