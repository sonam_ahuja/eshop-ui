import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import FantasticDevicesHtml from '@category/HTML/FantasticDevicesHtml';

describe('<FantasticDevicesHtml />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <FantasticDevicesHtml />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
