import React from 'react';
import styled from 'styled-components';

const StyledFantasticDevicesHtml = styled.div`
  .fantasticDevicesWrap {
    margin-top: 8rem;

    .headingWrap {
      .heading {
        font-size: 6.5rem;
        line-height: 1.08;
        letter-spacing: -0.2px;
        color: #fff;
        margin-bottom: 3rem;
        font-weight: 900;
      }

      .link {
        font-size: 1.75rem;
        color: #fff;
        text-decoration: none;
        display: inline-block;
        margin-bottom: 4rem;
      }
    }

    .productsWrap {
      display: flex;
      flex-wrap: wrap;
      margin: -0.25rem;

      .productCard {
        border-radius: 1rem;
        overflow: hidden;
        margin: 0.25rem;
        flex: 1;

        .top {
          background: #f6f6f6;
          display: flex;
          flex-direction: column;
          justify-content: space-between;

          .heading {
            font-size: 1.75rem;
            font-weight: 500;
            line-height: 1.43;
            color: #383838;
            margin: 2rem 2rem 3rem;
            min-height: 5rem;
          }

          .image {
            width: 17.5rem;
            height: 17.5rem;
            margin: 0 auto 3rem;
            overflow: hidden;

            img {
              width: 100%;
              display: block;
            }
          }
        }

        .bottom {
          background: #fff;
          padding: 2rem 3.5rem 2rem 2rem;

          .upfront {
            .label {
              font-size: 1.25rem;
              line-height: 1.2;
              letter-spacing: 0.3px;
              color: #a3a3a3;
              margin-bottom: 0.5rem;
            }
            .value {
              font-size: 2.5rem;
              font-weight: bold;
              line-height: 1.2;
              color: #383838;
            }
          }
        }
      }
    }
  }

  @media (min-width: 768px) {
  }

  @media (min-width: 1024px) {
    .fantasticDevicesWrap {
      display: flex;
      flex-direction: row-reverse;
      align-items: center;

      .headingWrap,
      .productsWrap {
        flex: 1;
        min-width: 50%;
        max-width: 50%;
      }
      .headingWrap {
        padding-left: 3rem;
      }

      .link {
        margin: 0;
      }
    }
  }

  @media (min-width: 1366px) {
    .fantasticDevicesWrap {
      .headingWrap {
        padding-left: 5rem;
      }
    }
  }
`;

const FantasticDevicesHtml = () => {
  return (
    <StyledFantasticDevicesHtml>
      <div className='fantasticDevicesWrap'>
        <div className='headingWrap'>
          <h2 className='heading'>Fantastic devices with special prices.</h2>
          <a href='#' className='link'>
            view all
          </a>
        </div>
        <div className='productsWrap'>
          <a href='#' className='productCard'>
            <div className='top'>
              <h2 className='heading'>Marshall Killburn</h2>
              <div className='image'>
                <img src='https://i.ibb.co/Htyr1Zc/Untitled-1.jpg' />
              </div>
            </div>
            <div className='bottom'>
              <ul className='upfront'>
                <li className='label'>UPFRONT</li>
                <li className='value'>13 090 Ft</li>
              </ul>
            </div>
          </a>
          <a href='#' className='productCard'>
            <div className='top'>
              <h2 className='heading'>Marshall Killburn</h2>
              <div className='image'>
                <img src='https://i.ibb.co/Htyr1Zc/Untitled-1.jpg' />
              </div>
            </div>
            <div className='bottom'>
              <ul className='upfront'>
                <li className='label'>UPFRONT</li>
                <li className='value'>13 090 Ft</li>
              </ul>
            </div>
          </a>
        </div>
      </div>
    </StyledFantasticDevicesHtml>
  );
};

export default FantasticDevicesHtml;
