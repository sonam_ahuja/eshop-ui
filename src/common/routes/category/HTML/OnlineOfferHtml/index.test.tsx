import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import OnlineOfferHtml from '@category/HTML/OnlineOfferHtml';

describe('<OnlineOfferHtml />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <OnlineOfferHtml />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
