import React from 'react';
import styled from 'styled-components';

const StyledOnlineOfferHtml = styled.div`
  .onlinOfferWrap {
    margin-top: 8rem;

    .everyThingYouNeed {
      .heading {
        font-size: 6.5rem;
        line-height: 1.08;
        letter-spacing: -0.2px;
        color: #fff;
        font-weight: 900;
        max-width: 35rem;
        margin-bottom: 4rem;
      }

      .filterChipsWrap {
        margin: -0.5rem;

        a {
          display: inline-block;
          text-decoration: none;
          background: rgba(255, 255, 255, 0.1);
          border-radius: 1rem;
          border: 1px solid #ddd;
          margin: 0.5rem;

          font-size: 1.75rem;
          font-weight: 500;
          line-height: 1.43;
          color: #fff;
          padding: 1rem 1.5rem;
        }
      }
    }

    .onlineOffer {
      margin-top: 8rem;
      display: flex;
      flex-direction: column;
      text-decoration: none;

      background-image: url(https://i.ibb.co/cxs0xww/Untitled-1.jpg);
      background-size: cover;
      padding: 4rem 3rem;
      border-radius: 1rem;

      color: #fff;

      .heading {
        margin-bottom: 2rem;
        font-size: 12px;
        line-height: 1.33;
        letter-spacing: 0.2px;
      }
      .description {
        max-width: 22.5rem;
        font-weight: 900;
        font-size: 30px;
        line-height: 1.07;
      }
      .link {
        margin-top: 11rem;
        font-size: 1.75rem;
        line-height: 1.43;
      }
    }
  }

  @media (min-width: 768px) {
    .onlinOfferWrap {
      .everyThingYouNeed {
        .heading {
          max-width: 50rem;
        }
      }
      .onlineOffer {
        height: 37.5rem;

        .link {
          margin-top: auto;
        }
      }
    }
  }

  @media (min-width: 1024px) {
    .onlinOfferWrap {
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      flex-direction: row-reverse;

      .everyThingYouNeed,
      .onlineOffer {
        min-width: 50%;
        max-width: 50%;
      }

      .everyThingYouNeed {
        padding-left: 5rem;
        .heading {
          max-width: 100%;
        }
      }
      .onlineOffer {
        margin-top: 0;
        height: 48.75rem;
      }
    }
  }
`;

const OnlineOfferHTML = () => {
  return (
    <StyledOnlineOfferHtml>
      <div className='onlinOfferWrap'>
        <div className='everyThingYouNeed'>
          <div className='heading'>Everything you need, and more.1</div>
          <div className='filterChipsWrap'>
            <a href='#'>Mobile Phones</a>
            <a href='#'>Accessories</a>
            <a href='#'>Tablets</a>
            <a href='#'>Fixed Services</a>
            <a href='#'>Laptops</a>
            <a href='#'>TVs</a>
            <a href='#'>Other Devices</a>
            <a href='#'>Special Offers</a>
            <a href='#'>Christmas Deals</a>
          </div>
        </div>
        <a href='#' className='onlineOffer'>
          <div className='heading'>Online Offer</div>
          <div className='description'>
            Shipment free of charge over 10 000 Ft.
          </div>
          <div className='link'>Microsoft Surface Laptop 2</div>
        </a>
      </div>
    </StyledOnlineOfferHtml>
  );
};

export default OnlineOfferHTML;
