import React from 'react';
import styled from 'styled-components';

const StyledSpecialOffersHtml = styled.div`
  .specialOffer {
    padding: 0 0.25rem;
    display: block;

    .specialOfferContent {
      width: 100%;
      border-radius: 1rem;
      background-color: #e20074;
      background-image: url('https://i.ibb.co/6ysRdFg/mobile-In-Hand.png');
      background-position: bottom right;
      background-repeat: no-repeat;
      padding: 4rem 3rem 5.5rem;
      color: #fff;
      text-decoration: none;
      display: block;

      .heading {
        font-size: 1.5rem;
        line-height: 1.33;
        letter-spacing: 0.2px;
        margin-bottom: 2rem;
      }

      .description {
        font-size: 3rem;
        line-height: 1.17;
        font-weight: 900;
        max-width: 28.125rem;
      }
    }
  }

  @media (min-width: 1024px) {
    .specialOffer {
      .heading {
        font-size: 1.875rem;
      }

      .description {
        font-size: 2.75rem;
      }
    }
  }
`;

const SpecialOffersHtml = () => {
  return (
    <StyledSpecialOffersHtml>
      <a href='#' className='specialOffer'>
        <div className='specialOfferContent'>
          <h2 className='heading'>Special Offer1</h2>
          <div className='description'>
            Merge into Magenta 1 and get a 30 000 HUF discount on a new device!
          </div>
        </div>
      </a>
    </StyledSpecialOffersHtml>
  );
};

export default SpecialOffersHtml;
