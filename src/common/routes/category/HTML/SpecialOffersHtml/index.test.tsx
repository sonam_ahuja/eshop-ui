import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SpecialOffersHtml from '@category/HTML/SpecialOffersHtml';
import SpecialOffersHtml1 from '@category/HTML/SpecialOffersHtml/index.1';

describe('<SpecialOffersHtml />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SpecialOffersHtml />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, SpecialOffersHtml1', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SpecialOffersHtml1 />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
