import React from 'react';
import styled from 'styled-components';

const StyledSpecialOffersHtml = styled.div`
  .specialOfferWrap {
    margin: 8rem -2.5rem 0 -2.5rem;
    padding: 0 2.5rem;

    /* HORIZONTAL SCROLL START */
    display: flex;
    flex-wrap: nowrap;
    /* Make this scrollable when needed */
    overflow-x: auto;
    /* We don't want vertical scrolling */
    overflow-y: hidden;
    /* Make an auto-hiding scroller for the 3 people using a IE */
    -ms-overflow-style: -ms-autohiding-scrollbar;
    /* For WebKit implementations, provide inertia scrolling */
    -webkit-overflow-scrolling: touch;
    /* We don't want internal inline elements to wrap */
    /* white-space: nowrap; */
    /* Remove the default scrollbar for WebKit implementations */
    &::-webkit-scrollbar {
      display: none;
      width: 0 !important ;
      scrollbar-width: none;
      overflow: -moz-scrollbars-none;
    }
    &,
    * {
      scrollbar-width: none;
      overflow: -moz-scrollbars-none;
    }

    /* HORIZONTAL SCROLL END */

    .specialOffer {
      padding: 0 0.25rem;
      min-width: 100%;
      flex: 1;

      .specialOfferContent {
        width: 100%;
        margin-bottom: 1rem;
        border-radius: 1rem;
        background-color: #e20074;
        background-image: url('https://i.ibb.co/6ysRdFg/mobile-In-Hand.png');
        background-position: bottom right;
        background-repeat: no-repeat;
        padding: 4rem 3rem 5.5rem;
        color: #fff;
        text-decoration: none;
        display: block;

        .heading {
          font-size: 1.5rem;
          line-height: 1.33;
          letter-spacing: 0.2px;
          margin-bottom: 2rem;
        }

        .description {
          font-size: 3rem;
          line-height: 1.17;
          font-weight: 900;
          max-width: 28.125rem;
        }
      }
    }
  }
  @media (min-width: 768px) {
    .specialOfferWrap {
      margin-left: -4.5rem;
      margin-right: -4.5rem;
      padding: 0 4.5rem;
    }
  }

  @media (min-width: 1024px) {
    .specialOfferWrap {
      margin-left: -6rem;
      margin-right: -6rem;
      padding: 0 6rem;

      .specialOffer {
        min-width: 50%;
        flex: 1;

        .heading {
          font-size: 1.875rem;
        }

        .description {
          font-size: 2.75rem;
        }
      }
    }
  }

  @media (min-width: 1366px) {
    .specialOfferWrap {
      margin-left: -9.8rem;
      margin-right: -9.8rem;
      padding: 0 9.8rem;
    }
  }
`;

const SpecialOffersHtml = () => {
  return (
    <StyledSpecialOffersHtml>
      <div className='specialOfferWrap'>
        <a href='#' className='specialOffer'>
          <div className='specialOfferContent'>
            <h2 className='heading'>Special Offer1</h2>
            <div className='description'>
              Merge into Magenta 1 and get a 30 000 HUF discount on a new
              device!
            </div>
          </div>
        </a>
        <a href='#' className='specialOffer'>
          <div className='specialOfferContent'>
            <h2 className='heading'>Special Offer1</h2>
            <div className='description'>
              Merge into Magenta 1 and get a 30 000 HUF discount on a new
              device!
            </div>
          </div>
        </a>
      </div>
    </StyledSpecialOffersHtml>
  );
};

export default SpecialOffersHtml;
