import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Header, { IProps } from '@category/Header';
import appState from '@store/states/app';
import { theme } from '@category/theme';
import { StaticRouter } from 'react-router-dom';
import { PAGE_THEME } from '@src/common/store/enums';

describe('<Header />', () => {
  const props: IProps = {
    theme: PAGE_THEME.PRIMARY,
    categoryTranslation: appState().translation.cart.category,
    globalTranslation: appState().translation.cart.global
  };

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={theme}>
          <Header {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
