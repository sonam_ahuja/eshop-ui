import React from 'react';
import { ICategoryTranslation, IGlobalTranslation } from '@store/types/translation';
import CatalogBreadCrumb from '@common/components/CatalogBreadCrumb';
import { pageThemeType } from '@src/common/store/enums';
import SectionHeading from '@category/common/SectionHeading';

import { StyledHeader, StyledMainHeading } from './styles';

export interface IProps {
  theme: pageThemeType;
  categoryTranslation: ICategoryTranslation;
  globalTranslation: IGlobalTranslation;
}

// tslint:disable-next-line:no-commented-code
// const StyledHeader = styled.div<{ theme: 'primary' | 'secondary' }>`
//   .sectionHeading {
//     /* padding: 5rem 0 6rem; */
//     /* vaishali */
//     padding-bottom: 3rem;
//     color: ${({ theme }) => themeObj[theme].sectionHeading.color};
//   }
// `;

const Header = (props: IProps) => {
  const breadCrumbItems = [
    {
      title: props.globalTranslation.home,
      active: false,
      link: '/'
    },
    {
      title: props.globalTranslation.products,
      active: true
    }
  ];

  return (
    <StyledMainHeading theme={props.theme}>
      <CatalogBreadCrumb children={null} breadCrumbItems={breadCrumbItems} />
      <StyledHeader theme={props.theme}>
        <SectionHeading size='xlarge' className='sectionHeading'>
          {props.categoryTranslation.categoryTitle}{' '}
          <div>{props.categoryTranslation.categorySubTitle}</div>
        </SectionHeading>
      </StyledHeader>
    </StyledMainHeading>
  );
};

export default Header;
