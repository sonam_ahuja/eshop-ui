import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

import { theme as themeObj } from '../theme';

export const StyledMainHeading = styled.div<{ theme: 'primary' | 'secondary' }>`
  .dt_breadcrumb {
    li.dt_breadcrumbItem {
      a {
        color: ${({ theme }) => {
          return themeObj[theme].breadcrumb.inactiveColor;
        }};
      }
      &.active {
        color: ${({ theme }) => {
          return themeObj[theme].breadcrumb.activeColor;
        }};
      }
    }
  }
`;

export const StyledHeader = styled.div<{ theme: 'primary' | 'secondary' }>`
  .sectionHeading {
    /* RESPONSIVE_CATEGORY_LANDING_PAGE */
    padding-bottom: 2.5rem;
    color: ${({ theme }) => themeObj[theme].sectionHeading.color};
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .sectionHeading {
      padding-bottom: 2.875rem;
      padding-top: 0.2rem;
    }
  }
`;
