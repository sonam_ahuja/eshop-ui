import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SpecialOffers, { IProps } from '@category/SpecialOffers';
import { StaticRouter } from 'react-router-dom';

describe('<SpecialOffers />', () => {
  const props: IProps = {
    specialSpecialOfferHtml1: 'html1',
    specialSpecialOfferHtml2: 'html2',
    onRouteChange: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SpecialOffers {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
