import React from 'react';
import { Column } from '@common/components/Grid/styles';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { StyledHorizontalScrollRow } from '@common/components/styles';
import { sendPromotionClickEvent } from '@events/category/index';

import { StyledSpecialOffers } from './styles';

export interface IProps {
  specialSpecialOfferHtml1: string;
  specialSpecialOfferHtml2: string;
  onRouteChange(url: string): void;
}
const SpecialOffers = (props: IProps) => {
  const { specialSpecialOfferHtml1, specialSpecialOfferHtml2 } = props;

  return (
    <StyledSpecialOffers>
      <StyledHorizontalScrollRow>
        {specialSpecialOfferHtml1 && (
          <Column
            className='horizontalScrollableItem'
            colMobile={12}
            colTabletPortrait={6}
          >
            <HTMLTemplate
              template={specialSpecialOfferHtml1}
              templateData={{}}
              sendEvents={() => {
                sendPromotionClickEvent('specialCategoryOfferHtml1', 'slot1');
              }}
              onRouteChange={props.onRouteChange}
            />
          </Column>
        )}
        {specialSpecialOfferHtml2 && (
          <Column
            className='horizontalScrollableItem'
            colMobile={12}
            colTabletPortrait={6}
          >
            <HTMLTemplate
              template={specialSpecialOfferHtml2}
              templateData={{}}
              sendEvents={() => {
                sendPromotionClickEvent(
                  'specialCategoryOfferHtml2',
                  specialSpecialOfferHtml1 ? 'slot2' : 'slot1'
                );
              }}
              onRouteChange={props.onRouteChange}
            />
          </Column>
        )}
      </StyledHorizontalScrollRow>
    </StyledSpecialOffers>
  );
};

export default SpecialOffers;
