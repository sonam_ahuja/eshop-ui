import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { Column } from '@common/components/Grid/styles';
import { StyledHorizontalScrollRow } from '@common/components/styles';

export const StyledSpecialOffers = styled.div`
  margin-top: 4rem;

  ${StyledHorizontalScrollRow} {
    margin: 0 -1.375rem;
    padding: 0 1.25rem;

    ${Column} {
      padding: 0 0.125rem;
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -2.25rem;
      padding: 0 2.125rem;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -3rem;
      padding: 0 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -4.9rem;
      padding: 0 4.9rem;
    }
  }
`;
