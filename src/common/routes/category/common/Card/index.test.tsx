import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Card, { IProps } from '@category/common/Card';

describe('<Card />', () => {
  const props: IProps = {
    children: <>children</>,
    bgColor: 'white'
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without children props', () => {
    const newProps = { ...props, children: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without bgColor props', () => {
    const newProps = { ...props, bgColor: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
