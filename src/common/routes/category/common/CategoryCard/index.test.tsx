import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import CategoryCard, { IProps } from '@category/common/CategoryCard';
import { StaticRouter } from 'react-router';
import { histroyParams } from '@mocks/common/histroy';

describe('<CategoryCard />', () => {
  const props: IProps = {
    ...histroyParams,
    title: 'Product',
    monthlyAmount: '100',
    upfrontAmount: '210',
    description: 'some description',
    id: '1',
    highLightProduct: {
      name: 'name',
      value: 'value'
    },
    productName: 'product name',
    imageUrl: 'http://localhost:3001/demo.jpg',
    color: ['white', 'black'],
    slug: '/product',
    redirectToProductListingPage: jest.fn()
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <CategoryCard {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when monthly amount not passed', () => {
    const newProps: IProps = { ...props, monthlyAmount: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when upfront amount not passed', () => {
    const newProps: IProps = { ...props, upfrontAmount: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when description not passed', () => {
    const newProps: IProps = { ...props, description: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when product name not passed', () => {
    const newProps: IProps = { ...props, productName: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when image url not passed', () => {
    const newProps: IProps = { ...props, imageUrl: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when color not passed', () => {
    const newProps: IProps = { ...props, color: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly when highLightProduct not passed', () => {
    const newProps: IProps = { ...props, highLightProduct: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
