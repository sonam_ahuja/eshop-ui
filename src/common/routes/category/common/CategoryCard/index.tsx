import React, { FunctionComponent } from 'react';
import { Paragraph, Title } from 'dt-components';
import PreloadImage from '@src/common/components/Preload';
import { colors } from '@src/common/variables';
import Card from '@category/common/Card';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { IMAGE_TYPE, IMGAE_WIDTH_RESOLUTION } from '@common/store/enums';
import { getLink, getValidTargetLink } from '@src/common/utils/openTargetLink';

import { StyledCategoryCard, StyledCategoryCardBig } from './styles';

export interface IProps extends RouteComponentProps<IRouterParams> {
  title: string;
  monthlyAmount?: string;
  upfrontAmount?: string;
  description?: string;
  id: string;
  highLightProduct?: {
    name: string;
    value: string;
  };
  productName?: string;
  imageUrl?: string;
  color?: string[];
  slug: string;
  alt?: string;
  redirectToProductListingPage(categoryId: string, slug: string): void;
}

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

const redirectToProductListingPage = (
  categoryId: string,
  slug: string,
  onClickHandler: IProps['redirectToProductListingPage']
) => (_event: React.MouseEvent<HTMLDivElement, MouseEvent>) =>
  onClickHandler(categoryId, slug);

const CategoryCard: FunctionComponent<IProps> = (props: IProps) => {
  const {
    title,
    description,
    highLightProduct,
    imageUrl,
    id,
    slug,
    alt
  } = props;

  const ImgEl = (
    <div className='img'>
      <PreloadImage
        isObserveOnScroll={true}
        imageHeight={10}
        width={IMGAE_WIDTH_RESOLUTION.LARGE_DESKTOP_WIDTH}
        mobileWidth={IMGAE_WIDTH_RESOLUTION.MEDIUM_MOBILE_WIDTH}
        type={IMAGE_TYPE.WEBP}
        imageWidth={10}
        imageUrl={imageUrl as string}
        alt={alt}
      />
    </div>
  );

  const CategoryCardEl = (
    <StyledCategoryCard
      onClick={redirectToProductListingPage(
        id,
        slug,
        props.redirectToProductListingPage
      )}
    >
      <Link className='primaryLink' to={`/${slug}`} />
      <div className='contentWrap'>
        <Title tag='h2' className='title' size='xsmall' weight='bold'>
          <span>{title}</span>
        </Title>
        <div className='imgWrap'>{ImgEl} </div>
        {highLightProduct ? (
          <a
            className='secondaryLink'
            href={getLink(highLightProduct.value)}
            onClick={event => getValidTargetLink(highLightProduct.value, event)}
          >
            <Paragraph size='small'>{highLightProduct.name}</Paragraph>
          </a>
        ) : null}
      </div>
    </StyledCategoryCard>
  );

  const CategoryCardBigEl = (
    <StyledCategoryCardBig
      onClick={redirectToProductListingPage(
        id,
        slug,
        props.redirectToProductListingPage
      )}
    >
      <Link className='primaryLink' to={`/${slug}`} />
      <div className='imgWrap'> {ImgEl} </div>
      <div className='headingAndDecsWrap'>
        <Title
          tag='h2'
          title={title}
          size='xsmall'
          weight='bold'
          className='title'
        >
          <span>{title}</span>
        </Title>

        <Paragraph
          title={description}
          className='description'
          size='small'
          weight='medium'
        >
          {description}
        </Paragraph>
      </div>
      {highLightProduct ? (
        <a
          className='secondaryLink'
          href={getLink(highLightProduct.value)}
          onClick={event => getValidTargetLink(highLightProduct.value, event)}
        >
          <Paragraph size='small'>{highLightProduct.name}</Paragraph>
        </a>
      ) : null}
      {/* <StyledProductName
            onClick={goToPage(highLightProduct.value, props.history.push)}
          ></StyledProductName> */}
    </StyledCategoryCardBig>
  );

  const contentEl = description ? CategoryCardBigEl : CategoryCardEl;

  return <Card bgColor={colors.white}>{contentEl}</Card>;
};

export default withRouter(CategoryCard);
