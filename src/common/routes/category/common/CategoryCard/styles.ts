import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

// small card
export const StyledCategoryCard = styled.div`
  padding: 1rem 0 1rem 1rem;
  display: flex;
  flex: 1;
  max-width: 100%;
  cursor: pointer;
  height: 100%;
  position: relative;

  /* LINKS START */
  .primaryLink {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    z-index: 1;
  }

  .secondaryLink {
    margin-top: auto;
    color: ${colors.magenta};
    padding-right: 0.25rem;
    position: relative;
    z-index: 2;
    height: 1.2rem;
    display: inline-flex;
    /* Same as StyledCategoryCardBig except following css - align-self: flex-start  */
    align-self: flex-start;
    max-width: 100%;
    .dt_paragraph {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
  }
  /* LINKS END */

  .imgWrap {
    /* RESPONSIVE_CATEGORY_LANDING_PAGE */
    /* margin: 2.75rem 0 2.5rem auto; */
    margin: 2.75rem 0 2rem auto;

    flex-shrink: 0;
    width: 9.25rem;
    .img img {
      margin: auto;
    }
  }

  .contentWrap {
    display: flex;
    flex: 1;
    max-width: 100%;
    flex-direction: column;
    justify-content: center;

    .title {
      color: ${colors.darkGray};
      position: relative;
      height: 1.5rem;

      span {
        position: absolute;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        color: ${colors.darkGray};
        width: 100%;
        padding-right: 1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    justify-content: space-between;
    padding: 1.25rem 0 1.25rem 1.25rem;
    padding-bottom: 1.5rem;

    .imgWrap {
      margin: 0.5rem 0 0.25rem auto;
      width: 10rem;
      height: 10rem;
    }
  }
`;

// big card
export const StyledCategoryCardBig = styled.div`
  padding: 1rem 0 1rem 1rem;
  width: 100%;
  cursor: pointer;
  position: relative;

  /* LINKS START */
  .primaryLink {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    z-index: 1;
  }

  .secondaryLink {
    margin-top: auto;
    color: ${colors.magenta};
    padding-right: 0.25rem;
    position: relative;
    z-index: 2;
    height: 1.2rem;
    display: inline-flex;
    max-width: 100%;
    .dt_paragraph {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
  }
  /* LINKS END */

  .imgWrap {
    float: right;
    margin: 4rem 0 0 0;
    shape-outside: inset(4rem 0 0 0);
    width: 12rem;
    height: 12rem;
    overflow: hidden;
    .img img {
      margin: auto;
    }
  }

  .headingAndDecsWrap {
    .title {
      position: relative;
      height: 1.5rem;

      span {
        position: absolute;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        color: ${colors.darkGray};
        width: 100%;
        padding-right: 1.25rem;
      }
    }

    &:after {
      content: '';
      display: block;
      clear: both;
    }

    .description {
      width: 100%;
      margin: 0.75rem 0 0.5rem 0;
      color: ${colors.ironGray};
      word-break: break-word;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    width: 100%;
    padding: 1.25rem 0 1.25rem 1.25rem;
    padding-bottom: 1.5rem;

    .imgWrap {
      margin: 0;
      margin-top: 0.5rem;
      shape-outside: none;
      width: 12.5rem;
      height: 12.5rem;
    }

    .headingAndDecsWrap {
      .title span {
        width: 50%;
      }
      .description {
        line-height: 1rem;
      }
    }
  }
`;
