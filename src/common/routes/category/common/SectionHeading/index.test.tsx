import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SectionHeading, { IProps } from '@category/common/SectionHeading';
import { theme } from '@category/theme';

describe('<SectionHeading />', () => {
  const props: IProps = {
    children: <> childern</>,
    className: 'className',
    size: 'xlarge'
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={theme}>
        <SectionHeading {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without size props', () => {
    const newProps: IProps = { ...props, size: undefined };
    const component = mount(
      <ThemeProvider theme={theme}>
        <SectionHeading {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without className props', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={theme}>
        <SectionHeading {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
