import React, { ReactNode } from 'react';

import { StyledSectionHeading } from './styles';

type sizeTypes = 'xlarge' | 'main';

export interface IProps {
  children: ReactNode;
  className?: string;
  size?: sizeTypes;
}

const SectionHeading = (props: IProps) => {
  const { size = 'main', children, className } = props;

  return (
    <StyledSectionHeading tag='h1' size={size} className={className} {...props}>
      {children}
    </StyledSectionHeading>
  );
};

export default SectionHeading;
