import styled from 'styled-components';
import { Title } from 'dt-components';
import { breakpoints } from '@src/common/variables';

import { IProps } from './index';

export const StyledSectionHeading = styled(Title).attrs({
  weight: 'ultra'
})<IProps>`
  @media (min-width: ${breakpoints.desktop}px) {
    font-size: 3.25rem;
    line-height: 1.08;
    letter-spacing: -0.3px;
  }
`;
