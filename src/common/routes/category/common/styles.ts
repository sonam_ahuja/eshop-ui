// tslint:disable:no-commented-code
// import styled from 'styled-components';
// import { breakpoints } from '@src/common/variables';
// import { theme as themeObj } from '@category/theme';
// import { Column, Row } from '@src/common/components/Grid/styles';

// export const LandingPageWrapper = styled.div<{
//   backgroundImage: string;
//   theme: 'primary' | 'secondary';
// }>`
//   background-color: ${({ theme }) => themeObj[theme].backgroundColor};
//   background-image: url('${({ backgroundImage }) => backgroundImage}');
//   background-size: 100% auto;
//   background-repeat: repeat;
//   background-position: top left;
//   width: 100%;
//   ${Row} {
//     margin: -0.125rem;

//     ${Column} {
//       padding: 0.125rem;
//     }
//   }

//   @media (min-width: ${breakpoints.desktop}px) {
//     display: flex;
//   }
// `;

// export const Container = styled.div`
//   padding: 1.25rem 1.25rem 4.75rem 1.25rem;

//   @media (min-width: ${breakpoints.tabletPortrait}px) {
//     padding-left: 2.25rem;
//     padding-right: 2.25rem;
//   }
//   @media (min-width: ${breakpoints.tabletLandscape}px) {
//     padding-left: 3rem;
//     padding-right: 3rem;
//   }
//   @media (min-width: ${breakpoints.desktop}px) {
//     padding: 1.25rem 4.9rem 4.75rem;
//     width: 100%;
//   }
//   @media (min-width: ${breakpoints.desktopLarge}px) {
//     padding: 1.25rem 5.5rem 4.75rem;
//   }
// `;
