import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { Column, Row } from '@common/components/Grid/styles';
import { breakpoints } from '@src/common/variables';

const StyledCategoryShell = styled.div`
  .breadCrumb {
    height: 1rem;
    width: 6rem;
    margin-top: 0.75rem;
    margin-bottom: 2.5rem;
  }

  .box {
    height: 18.75rem;
  }

  ${Row} {
    margin: -0.125rem;

    ${Column} {
      padding: 0.125rem;
    }
  }

  /* SECTION */
  .sectionPerfectGift {
    .heading {
      width: 100%;
      height: 5.5rem;
      margin-bottom: 3rem;
    }
  }

  /* SECTION */
  .sectionEverthingYouNeed {
    margin-top: 3.75rem;
    .box {
      margin-top: 4rem;
    }

    ${Row} {
      flex-direction: column-reverse;
    }

    .filterAndHeading {
      .heading {
        height: 10.5rem;
        margin-bottom: 2rem;
      }
      .filtersWrap {
        margin: -0.25rem;
        display: flex;
        flex-wrap: wrap;
        .filter {
          height: 2.25rem;
          margin: 0.25rem;

          &:nth-child(1) {
            width: 6.8rem;
          }
          &:nth-child(2) {
            width: 5.75rem;
          }
          &:nth-child(3) {
            width: 6.65rem;
          }
          &:nth-child(4) {
            width: 4.1rem;
          }
          &:nth-child(5) {
            width: 4.4rem;
          }
          &:nth-child(6) {
            width: 3rem;
          }
          &:nth-child(7) {
            width: 6.5rem;
          }
          &:nth-child(8) {
            width: 6.5rem;
          }
          &:nth-child(9) {
            width: 7.25rem;
          }
        }
      }
    }
  }

  /* SECTION */
  .sectionCategories {
    margin-top: 4rem;
  }

  /* SECTION */
  .sectionBestDeviceOffer {
    margin-top: 4rem;
    .heading {
      width: 100%;
      height: 7rem;
      margin-bottom: 3rem;
    }
    .bottom {
      margin-top: 4rem;
    }
  }

  /* SECTION */
  .sectionFantasticDevice {
    margin-top: 4rem;
    > ${Row} {
      flex-direction: column-reverse;
    }

    .heading {
      margin-top: 2.5rem;
      height: 7rem;
    }
    .viewAll {
      margin-top: 1rem;
      height: 1rem;
      width: 2rem;
      display: inline-flex;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .box {
      height: 16rem;
    }

    .sectionPerfectGift {
      .heading {
        width: 22rem;
        height: 7rem;
      }
    }

    /* SECTION */
    .sectionEverthingYouNeed {
      ${Row} {
        flex-direction: row;
      }
      .box {
        height: 25rem;
        margin-top: 0;
      }
      .filterAndHeading {
        padding: 3.5rem 2.5rem;
        .heading {
          height: 7rem;
          margin-bottom: 2rem;
        }
      }
    }

    .sectionBestDeviceOffer {
      .heading {
        width: 22rem;
      }
      .box {
        height: 23.5rem;
      }
    }

    /* SECTION */
    .sectionFantasticDevice {
      ${Row} {
        flex-direction: row;
      }

      .heading {
        margin-top: 2.5rem;
        margin-left: 2.5rem;
      }
      .viewAll {
        margin-left: 2.5rem;
      }
    }
  }
`;

const CategoryWrapShell = () => {
  return (
    <StyledShell className='primary'>
      <StyledCategoryShell>
        <div className='shine breadCrumb' />
        <section className='sectionPerfectGift'>
          <h2 className='heading shine' />
          <Row>
            <Column colMobile={12} colDesktop={6}>
              <div className='box shine' />
            </Column>
            <Column colMobile={6} colDesktop={3}>
              <div className='box shine' />
            </Column>
            <Column colMobile={6} colDesktop={3}>
              <div className='box shine' />
            </Column>
          </Row>
        </section>

        <section className='sectionEverthingYouNeed'>
          <Row>
            <Column colDesktop={6}>
              <div className='box shine' />
            </Column>
            <Column colDesktop={6}>
              <div className='filterAndHeading'>
                <h2 className='heading shine' />
                <div className='filtersWrap'>
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                  <div className='filter shine' />
                </div>
              </div>
            </Column>
          </Row>
        </section>

        <section className='sectionCategories'>
          <Row>
            <Column colMobile={12} colDesktop={3}>
              <div className='box shine' />
            </Column>
            <Column colMobile={12} colDesktop={3}>
              <div className='box shine' />
            </Column>
            <Column colMobile={12} colDesktop={6}>
              <div className='box shine' />
            </Column>
          </Row>
        </section>

        <section className='sectionBestDeviceOffer'>
          <h1 className='heading shine' />
          <Row>
            <Column colMobile={6} colDesktop={3}>
              <div className='box shine' />
            </Column>
            <Column colMobile={6} colDesktop={3}>
              <div className='box shine' />
            </Column>
            <Column colMobile={12} colDesktop={3}>
              <div className='box shine' />
            </Column>
            <Column colMobile={12} colDesktop={3}>
              <div className='box shine' />
            </Column>
          </Row>
          <Row className='bottom'>
            <Column colMobile={12} colDesktop={6}>
              <div className='box shine' />
            </Column>
            <Column colMobile={12} colDesktop={6}>
              <div className='box shine' />
            </Column>
          </Row>
        </section>

        <section className='sectionFantasticDevice'>
          <Row>
            <Column colDesktop={6}>
              <Row>
                <Column colMobile={6} colDesktop={6}>
                  <div className='box shine' />
                </Column>
                <Column colMobile={6} colDesktop={6}>
                  <div className='box shine' />
                </Column>
              </Row>
            </Column>
            <Column colDesktop={6}>
              <h2 className='heading shine' />
              <a className='viewAll shine' />
            </Column>
          </Row>
        </section>
      </StyledCategoryShell>
    </StyledShell>
  );
};
export default CategoryWrapShell;
