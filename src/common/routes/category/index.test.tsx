import { BestDeviceOffers } from '@mocks/category/category.mock';
import { Provider } from 'react-redux';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import React from 'react';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { theme } from '@category/theme';
import { MODAL_TYPE } from '@productList/store/enum';
import { NotificationData } from '@src/common/mock-api/productList/productList.mock';

import { Category, IProps, mapDispatchToProps, mapStateToProps } from '.';

describe('<Category />', () => {
  const props: IProps = {
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    bestDeviceOffersSlug: './Mobile-Device',
    location: {
      pathname: '/basket',
      search: '',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    },
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    scrollPosition: 23,
    globalTranslation: appState().translation.cart.global,
    setScrollPosition: jest.fn(),
    error: null,
    notificationLoading: false,
    notificationModalType: MODAL_TYPE.CONFIRMATION_MODAL,
    notificationData: NotificationData,
    showModal: true,
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,
    sendStockNotification: jest.fn(),
    setError: jest.fn(),
    openModal: jest.fn(),
    setNotificationData: jest.fn(),
    translation: appState().translation,
    configuration: appState().configuration,
    productListTranslation: appState().translation.cart.productList,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    categories: appState().categories.categories,
    loading: true,
    receivedCategoryData: true,
    bestDeviceOffers: BestDeviceOffers,
    fetchCategories: jest.fn(),
    setCategoryDataFromServer: jest.fn(),
    setSelectedCategory: jest.fn(),
    clearScrollPosition: jest.fn(),
    setDeviceList: jest.fn(),
    setDeviceDetailedList: jest.fn(),
    selectedCategoryId: ''
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={theme}>
          <Provider store={store}>
            <Category {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Category {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('Category').instance() as Category).onScroll();
    (component.find('Category').instance() as Category).componentWillUnmount();
    (component.find('Category').instance() as Category).redirectToNotFound();
    (component
      .find('Category')
      .instance() as Category).redirectToProductListing(
      'variantName',
      'variantId'
    );
    (component.find('Category').instance() as Category).onRouteChange(
      'variantName'
    );
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).fetchCategories('1234');
    mapDispatchToProps(dispatch).setCategoryDataFromServer(true);
    mapDispatchToProps(dispatch).setSelectedCategory('1234');
    mapDispatchToProps(dispatch).setScrollPosition(23);
    mapDispatchToProps(dispatch).clearScrollPosition();
  });
});
