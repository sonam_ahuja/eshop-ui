import BestDevices from '@category/BestDevices';
import CategoryWrapShell from '@category/index.shell';
import { Helmet } from 'react-helmet';
import React, { Component, Fragment, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import actions from '@category/store/actions';
import commonActions from '@common/store/actions/common';
import { RootState } from '@common/store/reducers';
import {
  IGlobalTranslation,
  IProductListTranslation,
  ITranslationState
} from '@store/types/translation';
import {
  ICatalogConfiguration,
  IConfigurationState,
  ICurrencyConfiguration,
  ILoginFormRules
} from '@store/types/configuration';
import CategoryList from '@category/CategoryList';
import SpecialOffers from '@category/SpecialOffers';
import { getCategorySlug, getCharacterstic } from '@category/utils/helper';
import { IBestDeviceOffers, ICategory } from '@category/store/types';
import CategoryHeader from '@category/Header';
import HTMLTemplate from '@common/components/HtmlTemplate';
import htmlKeys from '@common/constants/appkeys';
import { HISTORY_STATES } from '@common/constants/appConstants';
import { PAGE_THEME, pageThemeType } from '@src/common/store/enums';
import Header from '@common/components/Header';
import Footer from '@common/components/Footer';
import BasketStripWrapper from '@common/components/BasketStripWrapper';
import { updatedImageURL } from '@src/common/utils/index';
import { pageViewEvent } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import Error from '@src/common/components/Error';
import { IError as IGenricError } from '@common/store/types/common';
import CommonModal from '@productList/Modals/CommonModal';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';
import productActions from '@productList/store/actions';
import {
  IError,
  IGetNotificationRequestTemplatePayload,
  INotificationData
} from '@productList/store/types';

import { LIST_TYPE } from './store/enum';
import {
  Container,
  StyledCategory,
  StyledContentWrapper,
  StyledMainContent,
  StyledShellWithStripContainer
} from './styles';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  commonError: IGenricError;
  currency: ICurrencyConfiguration;
  translation: ITranslationState;
  configuration: IConfigurationState;
  categories: ICategory;
  loading: boolean;
  theme?: pageThemeType;
  backgroundImage?: string;
  scrollPosition?: number;
  // categoryId: string;
  receivedCategoryData: boolean;
  globalTranslation: IGlobalTranslation;
  productListTranslation: IProductListTranslation;
  catalogConfiguration: ICatalogConfiguration;
  bestDeviceOffers: IBestDeviceOffers[];
  bestDeviceOffersSlug: string;
  showModal: boolean;
  notificationModalType: MODAL_TYPE;
  selectedCategoryId: string;
  error: IError | null;
  notificationLoading: boolean;
  notificationData: INotificationData;
  formConfiguration: ILoginFormRules;
  openModal(isModalOpen: boolean): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
  fetchCategories(
    categoryId: string,
    scrollPosition?: number,
    showFullPageError?: boolean
  ): void;
  setCategoryDataFromServer(receivedCategoryData: boolean): void;
  setScrollPosition(position: number): void;
  setSelectedCategory(categoryId: string): void;
  clearScrollPosition(): void;
  setDeviceList(list: string): void;
  setNotificationData(data: INotificationData): void;
  setDeviceDetailedList(list: string): void;
}

export class Category extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.redirectToProductListing = this.redirectToProductListing.bind(this);
    this.onRouteChange = this.onRouteChange.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.openNotificationModal = this.openNotificationModal.bind(this);
  }

  onScroll(): void {
    this.props.setScrollPosition(window.scrollY);
  }

  openNotificationModal(
    event: React.MouseEvent,
    variantId: string,
    deviceName: string
  ): void {
    event.stopPropagation();
    this.props.setNotificationData({
      variantId,
      deviceName,
      mediumValue: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    });
    this.props.openModal(true);
  }

  componentDidMount(): void {
    window.scrollTo({
      top: 0
    });
    window.addEventListener('scroll', this.onScroll);
    const categorySlug = getCategorySlug(this.props.location.pathname);
    pageViewEvent(`${PAGE_VIEW.CATEGORY_LANDING_PAGE}/${categorySlug}`);
    this.props.setSelectedCategory(categorySlug);
    const frontNavigation = this.props.history.action === HISTORY_STATES.PUSH;
    if (frontNavigation) {
      this.props.clearScrollPosition();
    }
    if (!this.props.receivedCategoryData) {
      this.props.fetchCategories(
        categorySlug,
        frontNavigation ? 0 : this.props.scrollPosition,
        true // showFullPageError
      );
    }
    this.props.setCategoryDataFromServer(false);
  }

  componentWillUnmount(): void {
    window.removeEventListener('scroll', this.onScroll);
  }

  redirectToNotFound(): void {
    this.props.history.push('/not-found');
  }

  redirectToProductListing(_categoryId: string, slug: string): void {
    this.props.setDeviceList('category landing page');
    this.props.setSelectedCategory(slug);
  }

  onRouteChange(route: string): void {
    this.props.history.push(route);
  }

  // tslint:disable-next-line:cognitive-complexity
  render(): ReactNode {
    const {
      translation,
      categories,
      loading,
      bestDeviceOffers,
      globalTranslation,
      productListTranslation,
      catalogConfiguration,
      selectedCategoryId,
      commonError,
      setDeviceDetailedList,
      currency,
      notificationData,
      showModal,
      notificationModalType,
      error,
      setError,
      sendStockNotification,
      notificationLoading,
      formConfiguration,
      openModal
    } = this.props;
    const fallbackTheme = this.props.configuration.cms_configuration.modules
      .category.theme;
    const theme =
      this.props.theme ||
      ([PAGE_THEME.PRIMARY, PAGE_THEME.SECONDARY].includes(fallbackTheme)
        ? fallbackTheme
        : PAGE_THEME.SECONDARY);
    const fallbackBackgroundImage = this.props.configuration.cms_configuration
      .modules.category.backgroundImage;
    const backgroundImage = this.props.backgroundImage
      ? updatedImageURL(this.props.backgroundImage)
      : fallbackBackgroundImage;

    const onlineOfferHTML = getCharacterstic(
      categories.characteristics,
      htmlKeys.CATEGORY_LANDING_ONLINE_OFFER
    );
    const specialOffer1Characterstic = getCharacterstic(
      categories.characteristics,
      htmlKeys.CATEGORY_SPECIAL_OFFER_SECTION_1
    );
    const specialOfferHtml2Characterstic = getCharacterstic(
      categories.characteristics,
      htmlKeys.CATEGORY_SPECIAL_OFFER_SECTION_2
    );
    const fantasticDeviceOfferHtml = getCharacterstic(
      categories.characteristics,
      htmlKeys.CATEGOERY_FANTASTIC_DEVICE_OFFERS
    );
    const metaTagTitle = getCharacterstic(
      categories.characteristics,
      htmlKeys.META_TAG_TITLE
    );
    const metaTagDescription = getCharacterstic(
      categories.characteristics,
      htmlKeys.META_TAG_DESCRIPTION
    );

    const showMetaTitle =
      metaTagTitle && metaTagTitle !== '' && metaTagTitle.length !== 0
        ? metaTagTitle
        : globalTranslation.categoryLandingPage;

    return (
      <Fragment>
        <Helmet
          title={showMetaTitle ? showMetaTitle : ''}
          meta={[
            {
              name: 'description',
              content: metaTagDescription ? metaTagDescription : ''
            }
          ]}
        />

        <Header showBasketStrip={false} />
        {loading && (
          <StyledShellWithStripContainer>
            <Container>
              <CategoryWrapShell />
            </Container>
            <Footer pageName={PAGE_VIEW.CATEGORY_LANDING_PAGE} />
          </StyledShellWithStripContainer>
        )}
        {!loading &&
          (commonError.httpStatusCode ? (
            <Error />
          ) : (
            <>
              <CommonModal
                isOpen={showModal}
                deviceName={notificationData.deviceName}
                variantId={notificationData.variantId}
                errorMessage={error && error.message}
                sendStockNotification={sendStockNotification}
                setError={setError}
                notificationLoading={notificationLoading}
                modalType={notificationModalType}
                translation={productListTranslation}
                notificationMediumType={notificationData.type}
                notificationMediumValue={notificationData.mediumValue}
                openModal={openModal}
                formConfiguration={formConfiguration}
              />
              <StyledCategory>
                <StyledContentWrapper>
                  <StyledMainContent
                    theme={theme}
                    backgroundImage={backgroundImage}
                  >
                    <Container>
                      <CategoryHeader
                        theme={theme}
                        categoryTranslation={translation.cart.category}
                        globalTranslation={globalTranslation}
                      />
                      <CategoryList
                        type={'topSection' as LIST_TYPE}
                        categories={categories}
                        key={0}
                        redirectToProductListing={this.redirectToProductListing}
                      />
                      {/* <OnlineOfferHTML /> */}
                      <HTMLTemplate
                        onRouteChange={this.onRouteChange}
                        template={onlineOfferHTML ? onlineOfferHTML : ''}
                        templateData={{}}
                        // style={{ padding: '4rem 0' }}
                      />
                      <CategoryList
                        categories={categories}
                        key={1}
                        redirectToProductListing={this.redirectToProductListing}
                      />
                      {!!(bestDeviceOffers && bestDeviceOffers.length) && (
                        <BestDevices
                          theme={theme}
                          selectedCategoryId={selectedCategoryId}
                          bestDeviceOffers={bestDeviceOffers}
                          currency={currency}
                          openNotificationModal={this.openNotificationModal}
                          categoryTranslation={translation.cart.category}
                          bestDeviceOffersSlug={this.props.bestDeviceOffersSlug}
                          setDeviceDetailedList={setDeviceDetailedList}
                          productListTranslation={productListTranslation}
                          catalogConfiguration={catalogConfiguration}
                          considerInventory={
                            this.props.configuration.cms_configuration.modules
                              .catalog.preorder.considerInventory
                          }
                        />
                      )}
                      {/* <SpecialOffersHtml /> */}
                      <SpecialOffers
                        onRouteChange={this.onRouteChange}
                        specialSpecialOfferHtml1={
                          specialOffer1Characterstic
                            ? specialOffer1Characterstic
                            : ''
                        }
                        specialSpecialOfferHtml2={
                          specialOfferHtml2Characterstic
                            ? specialOfferHtml2Characterstic
                            : ''
                        }
                      />
                      {/* <FantasticDevicesHtml /> */}
                      <HTMLTemplate
                        onRouteChange={this.onRouteChange}
                        template={
                          fantasticDeviceOfferHtml
                            ? fantasticDeviceOfferHtml
                            : ''
                        }
                        templateData={{}}
                        // style={{ padding: '4rem 0' }}
                      />{' '}
                    </Container>
                  </StyledMainContent>
                </StyledContentWrapper>
                <BasketStripWrapper />
              </StyledCategory>
              <Footer pageName={PAGE_VIEW.CATEGORY_LANDING_PAGE} />
            </>
          ))}
      </Fragment>
    );
  }
}

export const mapStateToProps = (state: RootState) => ({
  commonError: state.common.error,
  currency: state.configuration.cms_configuration.global.currency,
  theme: state.categories.categories.theme,
  backgroundImage: state.categories.categories.backgroundImage,
  categories: state.categories.categories,
  translation: state.translation,
  configuration: state.configuration,
  selectedCategoryId: state.categories.selectedCategoryId,
  loading: state.categories.loading,
  receivedCategoryData: state.categories.receivedCategoryData,
  bestDeviceOffers: state.categories.bestDeviceOffers,
  globalTranslation: state.translation.cart.global,
  scrollPosition: state.categories.scrollPosition,
  bestDeviceOffersSlug: state.categories.bestDeviceOffersSlug,
  catalogConfiguration: state.configuration.cms_configuration.modules.catalog,
  productListTranslation: state.translation.cart.productList,
  formConfiguration:
    state.configuration.cms_configuration.global.loginFormRules,
  notificationLoading: state.productList.notificationLoading,
  notificationModalType: state.productList.notificationModal,
  notificationData: state.productList.notificationData,
  showModal: state.productList.openModal,
  error: state.productList.error
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchCategories(
    categoryId: string,
    scrollPosition?: number,
    // tslint:disable-next-line:bool-param-default
    showFullPageError?: boolean
  ): void {
    dispatch(
      actions.fetchCategories({ categoryId, scrollPosition, showFullPageError })
    );
  },
  setCategoryDataFromServer(receivedCategoryData: boolean): void {
    dispatch(actions.setReceivedCategoryDataFromServer(receivedCategoryData));
  },
  setSelectedCategory(scategorySlug: string): void {
    dispatch(actions.setSelectedCategory(scategorySlug));
  },
  setScrollPosition(position: number): void {
    dispatch(actions.setScrollPosition(position));
  },
  clearScrollPosition(): void {
    dispatch(actions.clearScrollPosition());
  },
  setDeviceList(list: string): void {
    dispatch(commonActions.setDeviceList(list));
  },
  setDeviceDetailedList(list: string): void {
    dispatch(commonActions.setDeviceDetailedList(list));
  },
  setNotificationData(data: INotificationData): void {
    dispatch(productActions.setNotificationData(data));
  },
  openModal(isModalOpen: boolean): void {
    dispatch(productActions.openModal(isModalOpen));
  },
  setError(error: IError): void {
    dispatch(productActions.setError(error));
  },
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void {
    dispatch(productActions.sendStockNotification(payload));
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Category)
  // tslint:disable-next-line: max-file-line-count
);
