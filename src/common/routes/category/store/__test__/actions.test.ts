import categoryActions from '@category/store/actions';
import categoryState from '@category/store/state';
import configureMockStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';
import categorySaga from '@category/store/sagas';
import CONSTANTS from '@category/store/constants';
import * as categoryApi from '@src/common/types/api/categories';
import { PAGE_THEME } from '@src/common/store/enums';

describe('actions test', () => {
  const sagaMiddleware = createSagaMiddleware();
  const mockStore = configureMockStore([sagaMiddleware]);
  const store = mockStore({});
  sagaMiddleware.run(categorySaga);

  it('setCategoriesData action creator should return a object with expected value', () => {
    const expected: { type: string; payload: categoryApi.GET.IResponse } = {
      type: CONSTANTS.SET_CATEGORIES_DATA,
      payload: {
        id: '',
        name: '',
        parentSlug: null,
        slug: '',
        characteristics: [],
        highLightProduct: { name: '', value: '' },
        image: '',
        description: '',
        priority: 0,
        subCategories: [],
        backgroundImage: '',
        theme: PAGE_THEME.SECONDARY
      }
    };
    const newState = store.dispatch(
      categoryActions.setCategoriesData(categoryState().categories)
    );
    expect(newState).toEqual(expected);
  });

  it('setSelectedCategory action creator should return a object with expected value', () => {
    const expected: { type: string; payload: string } = {
      type: CONSTANTS.SET_SELECTED_CATEGORY_ID,
      payload: '1234'
    };
    const newState = store.dispatch(
      categoryActions.setSelectedCategory('1234')
    );
    expect(newState).toEqual(expected);
  });
});
