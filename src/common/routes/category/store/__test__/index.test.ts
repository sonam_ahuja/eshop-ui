import categoryState from '@category/store/state';
import categoryReducer from '@category/store/reducer';

describe('categoryReducer', () => {
  it('categoryReducer test', () => {
    const action: { type: string } = { type: '' };
    expect(categoryReducer(categoryState(), action)).toEqual(categoryState());
  });
});
