import category from '@category/store/reducer';
import categoryState from '@category/store/state';
import { IBestDeviceOffers, ICategoryState } from '@category/store/types';
import actions from '@category/store/actions';
import { BestDeviceOffers } from '@mocks/category/category.mock';

describe('Category Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ICategoryState = category(undefined, definedAction);
  const initializeValue = () => {
    initialStateValue = category(undefined, definedAction);
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('SET_CATEGORIES_DATA should mutate parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };

    const action = actions.setCategoriesData(categoryState().categories);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(newState);
  });

  it('FETCH_CATEGORIES_LOADING should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation = {
      loading: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchCategoriesLoading();
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_CATEGORIES_ERROR should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const error: Error = {
      name: 'error',
      stack: 'stack',
      message: 'message'
    };
    const mutation: { loading: boolean; error: Error } = {
      loading: false,
      error
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchCategoriesError(error);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_CATEGORIES_SUCCESS should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation: { loading: boolean } = {
      loading: false
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchCategoriesSuccess();
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('RECEIVED_CATEGORY_DATA should mutate receivedCategoryData parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation: { receivedCategoryData: boolean } = {
      receivedCategoryData: true
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setReceivedCategoryDataFromServer(true);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SELECTED_CATEGORY_ID should mutate selectedCategoryId parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation: { selectedCategoryId: string } = {
      selectedCategoryId: '1234'
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setSelectedCategory('1234');
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_BEST_DEVICE_OFFER_SUCESS should mutate selectedCategoryId parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation: {
      bestDeviceOffers: IBestDeviceOffers[];
      loading: boolean;
    } = {
      bestDeviceOffers: BestDeviceOffers,
      loading: true
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setBestDeviceOfferData(BestDeviceOffers);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_BEST_DEVICE_OFFER_ERROR should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const error: Error = {
      name: 'error',
      stack: 'stack',
      message: 'message'
    };
    const mutation: { loading: boolean; error: Error } = {
      loading: false,
      error
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchBestDeviceOfferError(error);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SCROLL_POSITION should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation: { scrollPosition: number } = {
      scrollPosition: 23
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setScrollPosition(23);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('CLEAR_SCROLL_POSITION should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const mutation: { scrollPosition: number } = {
      scrollPosition: 0
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.clearScrollPosition();
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_BEST_DEVICE_OFFERS_SLUG should mutate loading parameter in state', () => {
    const newState: ICategoryState = { ...categoryState() };
    const newCategory = 'Mobile-Devices';
    const mutation: { bestDeviceOffersSlug: string } = {
      bestDeviceOffersSlug: newCategory
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setBestDeviceOffersSlug(newCategory);
    const newMutatedState = category(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
});
