import {
  fetchCategories,
  setCategoryDataFromServer
} from '@category/store/sagas';
import constants from '@category/store/constants';

describe('Category Saga', () => {
  it('fetchCategories saga test', () => {
    const action: {
      type: string;
      payload: {
        categoryId: string;
        scrollPosition: number;
        showFullPageError: boolean;
      };
    } = {
      type: constants.FETCH_CATEGORIES_REQUESTED,
      payload: {
        categoryId: '12',
        scrollPosition: 34,
        showFullPageError: false
      }
    };

    const closingModalGenerator = fetchCategories(action);
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeUndefined();
  });

  it('setCategoryDataFromServer saga test', () => {
    const action: { type: string; payload: boolean } = {
      type: constants.FETCH_CATEGORIES_REQUESTED,
      payload: true
    };

    const closingModalGenerator = setCategoryDataFromServer(action);
    expect(closingModalGenerator.next().value).toBeDefined();
    expect(closingModalGenerator.next().value).toBeUndefined();
  });
});
