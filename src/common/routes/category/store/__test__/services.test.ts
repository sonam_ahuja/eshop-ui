import {
  fetchBestDeviceOfferService,
  fetchCategoriesService
} from '@category/store/services';
import { apiEndpoints } from '@common/constants';
import categoryState from '@category/store/state';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

describe('category Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchCategoriesService test', async () => {
    const url: string = apiEndpoints.CATEGORY.GET_CATEGORIES.url('1234');
    const payload = categoryState();
    mock.onGet(url).replyOnce(200, payload);
    axios
      .get(url)
      .then(async () => {
        const result = await fetchCategoriesService('1234');
        expect(result).toEqual(payload);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('fetchBestDeviceOfferService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url;
    const payload = categoryState();
    mock.onPost(url).replyOnce(200, payload);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchBestDeviceOfferService('1234');
        expect(result).toEqual(payload);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });
});
