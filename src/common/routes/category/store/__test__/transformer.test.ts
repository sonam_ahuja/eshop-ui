import {
  getBestDeviceOffers,
  transformCategoriesResponse
} from '@category/store/transformer';
import { ProductListResponse } from '@mocks/productList/productList.mock';
import { ICategory } from '@category/store/types';

describe('category Service', () => {
  it('transformCategoriesResponse test', () => {
    const data: ICategory = {
      id: '123',
      name: 'transformer',
      slug: '/',
      image: '',
      parentSlug: '/',
      description: 'description',
      subCategories: [
        {
          id: '123',
          slug: '/',
          image: '',
          parentSlug: '/',
          name: 'transformer',
          description: 'description',
          subCategories: [],
          characteristics: []
        }
      ],
      characteristics: [
        {
          name: 'book',
          values: [{ value: 'seven star' }]
        }
      ]
    };
    expect(transformCategoriesResponse(data)).toMatchSnapshot();
  });

  it('transformCategoriesResponse test', () => {
    expect(getBestDeviceOffers(ProductListResponse)).toMatchSnapshot();
  });
});
