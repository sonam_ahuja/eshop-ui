import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@category/store/constants';
import * as categoryApi from '@src/common/types/api/categories';
import { IBestDeviceOffers } from '@category/store/types';

export default {
  fetchCategories: actionCreator<{
    categoryId: string;
    scrollPosition?: number;
    showFullPageError?: boolean;
  }>(CONSTANTS.FETCH_CATEGORIES_REQUESTED),
  fetchCategoriesLoading: actionCreator(CONSTANTS.FETCH_CATEGORIES_LOADING),
  fetchCategoriesError: actionCreator<Error>(CONSTANTS.FETCH_CATEGORIES_ERROR),
  fetchCategoriesSuccess: actionCreator(CONSTANTS.FETCH_CATEGORIES_SUCCESS),
  setCategoriesData: actionCreator<categoryApi.GET.IResponse>(
    CONSTANTS.SET_CATEGORIES_DATA
  ),
  setSelectedCategory: actionCreator<string>(
    CONSTANTS.SET_SELECTED_CATEGORY_ID
  ),
  receivedCategoryDataFromServer: actionCreator<boolean>(
    CONSTANTS.RECEIVED_CATEGORY_DATA_FROM_SERVER
  ),
  setReceivedCategoryDataFromServer: actionCreator<boolean>(
    CONSTANTS.RECEIVED_CATEGORY_DATA
  ),
  fetchBestDeviceOffers: actionCreator<string>(
    CONSTANTS.FETCH_BEST_DEVICE_OFFER_REQUESTED
  ),
  setBestDeviceOffersSlug: actionCreator<string>(
    CONSTANTS.SET_BEST_DEVICE_OFFERS_SLUG
  ),
  setBestDeviceOfferData: actionCreator<IBestDeviceOffers[]>(
    CONSTANTS.FETCH_BEST_DEVICE_OFFER_SUCESS
  ),
  fetchBestDeviceOfferError: actionCreator<Error>(
    CONSTANTS.FETCH_BEST_DEVICE_OFFER_ERROR
  ),
  setScrollPosition: actionCreator<number>(CONSTANTS.SET_SCROLL_POSITION),
  clearScrollPosition: actionCreator(CONSTANTS.CLEAR_SCROLL_POSITION)
};
