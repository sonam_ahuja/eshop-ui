import { AnyAction, Reducer } from 'redux';
import { StateType } from 'typesafe-actions';
import {
  IBestDeviceOffers,
  ICategory,
  ICategoryState
} from '@category/store/types';
import CONSTANTS from '@category/store/constants';
import initialState from '@category/store/state';
import withProduce from '@utils/withProduce';

const reducers = {
  [CONSTANTS.SET_CATEGORIES_DATA]: (
    state: ICategoryState,
    payload: ICategory
  ) => {
    const categories = payload;
    state.categories = categories;
  },
  [CONSTANTS.FETCH_CATEGORIES_LOADING]: (state: ICategoryState) => {
    state.loading = true;
    state.error = null;
  },
  [CONSTANTS.FETCH_CATEGORIES_ERROR]: (
    state: ICategoryState,
    payload: Error
  ) => {
    state.error = payload;
    state.loading = false;
  },
  [CONSTANTS.FETCH_CATEGORIES_SUCCESS]: (state: ICategoryState) => {
    state.loading = false;
  },
  [CONSTANTS.RECEIVED_CATEGORY_DATA]: (
    state: ICategoryState,
    payload: boolean
  ) => {
    state.receivedCategoryData = payload;
  },
  [CONSTANTS.SET_SELECTED_CATEGORY_ID]: (
    state: ICategoryState,
    payload: string
  ) => {
    state.selectedCategoryId = payload;
  },
  [CONSTANTS.FETCH_BEST_DEVICE_OFFER_SUCESS]: (
    state: ICategoryState,
    payload: IBestDeviceOffers[]
  ) => {
    state.bestDeviceOffers = [...payload];
  },
  [CONSTANTS.FETCH_BEST_DEVICE_OFFER_ERROR]: (
    state: ICategoryState,
    payload: Error
  ) => {
    state.loading = false;
    state.error = payload;
  },
  [CONSTANTS.SET_SCROLL_POSITION]: (state: ICategoryState, payload: number) => {
    state.scrollPosition = payload;
  },
  [CONSTANTS.SET_BEST_DEVICE_OFFERS_SLUG]: (
    state: ICategoryState,
    payload: string
  ) => {
    state.bestDeviceOffersSlug = payload;
  },
  [CONSTANTS.CLEAR_SCROLL_POSITION]: (state: ICategoryState) => {
    state.scrollPosition = 0;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  ICategoryState,
  AnyAction
>;

export type BasketState = StateType<typeof withProduce>;
