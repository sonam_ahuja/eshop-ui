import {
  getBestDeviceOffers,
  transformCategoriesResponse
} from '@category/store/transformer';
import {
  fetchBestDeviceOfferService,
  fetchCategoriesService
} from '@category/store/services';
import { Effect } from 'redux-saga';
import actions from '@category/store/actions';
import * as categoryApi from '@src/common/types/api/categories';
import constants from '@category/store/constants';
import * as productListApi from '@common/types/api/productList';
import { call, put, takeLatest } from 'redux-saga/effects';
import { logError } from '@src/common/utils';
import {
  getBestDeviceOfferCategoryId,
  getCharacterstic
} from '@category/utils/helper';
import { isBrowser } from '@common/utils';
import { sendPromotionViewEvent } from '@events/category/index';
import htmlKeys from '@common/constants/appkeys';

export type SagaIterator = IterableIterator<
  Effect | Effect[] | Promise<categoryApi.GET.IResponse | Error>
>;

export function* fetchCategories(action: {
  type: string;
  payload: {
    categoryId: string;
    scrollPosition: number;
    showFullPageError: boolean;
  };
}): SagaIterator {
  try {
    const {
      payload: { categoryId, scrollPosition, showFullPageError }
    } = action;
    yield put(actions.fetchCategoriesLoading());
    const categories = yield fetchCategoriesService(
      categoryId,
      showFullPageError
    );

    const categoryData = transformCategoriesResponse(categories);

    yield put(actions.setCategoriesData(categoryData));

    const specialOffer1Characterstic = getCharacterstic(
      categories.characteristics,
      htmlKeys.CATEGORY_SPECIAL_OFFER_SECTION_1
    );
    const specialOfferHtml2Characterstic = getCharacterstic(
      categories.characteristics,
      htmlKeys.CATEGORY_SPECIAL_OFFER_SECTION_2
    );
    sendPromotionViewEvent(
      specialOffer1Characterstic ? 'specialCategoryOfferHtml1' : null,
      specialOfferHtml2Characterstic ? 'specialCategoryOfferHtml2' : null
    );

    const bestDeviceOfferCategoryId = getBestDeviceOfferCategoryId(categories);

    const products: productListApi.POST.IResponse = yield call<string>(
      fetchBestDeviceOfferService,
      bestDeviceOfferCategoryId
    );
    const { bestDeviceOffers, bestDeviceOffersSlug } = getBestDeviceOffers(
      products
    );

    yield put(actions.setBestDeviceOfferData(bestDeviceOffers));
    yield put(actions.setBestDeviceOffersSlug(bestDeviceOffersSlug));
    yield put(actions.fetchCategoriesSuccess());
    if (isBrowser && scrollPosition !== undefined) {
      window.scrollTo({
        top: scrollPosition
      });
    }
  } catch (error) {
    yield put(actions.fetchCategoriesError(error));
  }
}

export function* setCategoryDataFromServer(action: {
  type: string;
  payload: boolean;
}): SagaIterator {
  try {
    const receivedCategoryData = action.payload;
    yield put(actions.setReceivedCategoryDataFromServer(receivedCategoryData));
  } catch (error) {
    logError(error);
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(constants.FETCH_CATEGORIES_REQUESTED, fetchCategories);
  yield takeLatest(
    constants.RECEIVED_CATEGORY_DATA_FROM_SERVER,
    setCategoryDataFromServer
  );
}
export default watcherSaga;
