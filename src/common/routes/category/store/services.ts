import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import * as categoryApi from '@src/common/types/api/categories';
import * as productListApi from '@src/common/types/api/productList';
import { getProductListingRequestTemplate } from '@category/utils/requestTemplate';
import { logError } from '@src/common/utils';

export const fetchCategoriesService = async (
  id: string,
  // tslint:disable-next-line:bool-param-default
  showFullPageError?: boolean
): Promise<categoryApi.GET.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CATEGORY.GET_CATEGORIES.method.toLowerCase()
    ](apiEndpoints.CATEGORY.GET_CATEGORIES.url(id), {
      showFullPageError: !!showFullPageError
    });
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchBestDeviceOfferService = async (
  category: string
): Promise<productListApi.POST.IResponse> => {
  const requestBody = getProductListingRequestTemplate(category);
  try {
    return await apiCaller.post<productListApi.POST.IResponse>(
      apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url,
      requestBody
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};
