import { ICategoryState } from '@category/store/types';
import { PAGE_THEME } from '@src/common/store/enums';

export default (): ICategoryState => ({
  scrollPosition: 0,
  categories: {
    id: '',
    name: '',
    slug: '',
    backgroundImage: '',
    theme: PAGE_THEME.SECONDARY,
    characteristics: [],
    highLightProduct: {
      name: '',
      value: ''
    },
    image: '',
    description: '',
    priority: 0,
    subCategories: [],
    parentSlug: null
  },
  error: null,
  selectedCategoryId: '',
  bestDeviceOffersSlug: '',
  receivedCategoryData: false,
  loading: true,
  bestDeviceOffers: []
});
