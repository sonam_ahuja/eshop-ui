import * as productListApi from '@src/common/types/api/productList';
import { IProductListItem, IVariant } from '@productList/store/types';
import htmlKeys from '@common/constants/appkeys';
import { pageThemeType } from '@src/common/store/enums';
import * as categoryApi from '@src/common/types/api/categories';

import { ICategory } from './types';

export const getBestDeviceOffers = (
  products: productListApi.POST.IResponse
) => {
  const bestDeviceOffers = products.data.reduce(
    (result: IVariant[], product: IProductListItem): IVariant[] => {
      result.push(product.variant);

      return result;
    },
    []
  );
  const bestDeviceOffersSlugObj = products.characteristics.filter(
    characteristic =>
      characteristic.name === htmlKeys.BEST_DEVICE_OFFERS_CATEGORY_SLUG
  )[0];

  return {
    bestDeviceOffers,
    bestDeviceOffersSlug:
      bestDeviceOffersSlugObj &&
      bestDeviceOffersSlugObj.values[0] &&
      bestDeviceOffersSlugObj.values[0].value
        ? bestDeviceOffersSlugObj.values[0].value
        : ''
  };
};

const getCategorySlugImage = (
  characteristics: categoryApi.GET.IResponse['characteristics']
) => {
  const slugObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_SLUG
  )[0];
  const imageObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_IMAGE
  )[0];
  const slug = slugObj && slugObj.values ? slugObj.values[0].value : '';
  const image = imageObj && imageObj.values ? imageObj.values[0].value : '';

  return { slug, image };
};

const getParentCategorySlug = (
  characteristics: categoryApi.GET.IResponse['characteristics']
) => {
  const slugObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_PARENT_SLUG
  )[0];

  return slugObj && slugObj.values ? slugObj.values[0].value : '';
};

const getCharacteristicValue = (
  characteristics: categoryApi.GET.IResponse['characteristics'],
  key: string
) => {
  const nameObj = characteristics.filter(
    characteristic => characteristic.name === key
  )[0];

  if (nameObj && nameObj.values) {
    return nameObj.values[0].value;
  }

  return '';
};

const getHighLightProduct = (
  characteristics: categoryApi.GET.IResponse['characteristics']
) => {
  const nameObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_HIGHLIGHTPRODUCT
  )[0];
  const name = nameObj && nameObj.label ? nameObj.label : '';
  const value = nameObj && nameObj.values ? nameObj.values[0].value : '';

  return { name, value };
};

export const transformCategoriesResponse = (
  categoriesResponse: categoryApi.GET.IResponse
): ICategory => {
  const {
    subCategories,
    characteristics,
    ...categoryRest
  } = categoriesResponse;
  const { slug, image } = getCategorySlugImage(characteristics);
  const highLightProduct = getHighLightProduct(characteristics);
  const parentSlug = getParentCategorySlug(characteristics);
  const theme = getCharacteristicValue(
    characteristics,
    htmlKeys.CATEGORY_THEME
  ) as pageThemeType;
  const backgroundImage = getCharacteristicValue(
    characteristics,
    htmlKeys.CATEGORY_BACKGROUND_IMAGE
  );

  const mappedSubCategories = subCategories.map(s => {
    const {
      subCategories: SsubCategories,
      characteristics: Scharacteristics,
      ...subcategoryRest
    } = s;
    const slugImage = getCategorySlugImage(Scharacteristics);
    const sHighLightProduct = getHighLightProduct(Scharacteristics);

    const sTheme = getCharacteristicValue(
      Scharacteristics,
      htmlKeys.CATEGORY_THEME
    ) as pageThemeType;
    const sbackgroundImage = getCharacteristicValue(
      Scharacteristics,
      htmlKeys.CATEGORY_BACKGROUND_IMAGE
    );

    return {
      ...subcategoryRest,
      backgroundImage: sbackgroundImage,
      theme: sTheme,
      slug: slugImage.slug,
      image: slugImage.image,
      characteristics: Scharacteristics,
      highLightProduct: sHighLightProduct,
      subCategories: [],
      parentSlug: null
    };
  });

  return {
    ...categoryRest,
    backgroundImage,
    theme,
    slug,
    image,
    characteristics,
    highLightProduct,
    subCategories: mappedSubCategories,
    parentSlug
  };
};
