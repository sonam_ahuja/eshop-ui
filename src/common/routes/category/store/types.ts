import { IFilterCharacteristic } from '@src/common/routes/productList/store/types';
import { ITEM_STATUS, pageThemeType } from '@src/common/store/enums';
import { IPrices } from '@tariff/store/types';

export interface ICategoryState {
  categories: ICategory;
  loading: boolean;
  scrollPosition: number;
  receivedCategoryData: boolean;
  error: Error | null;
  selectedCategoryId: string;
  bestDeviceOffers: IBestDeviceOffers[];
  bestDeviceOffersSlug: string;
}

export interface ICharacteristic {
  name: string;
  label?: string;
  values: IValue[];
}

export interface IValue {
  value: string;
}

export interface ICategory {
  id: string;
  name: string;
  slug: string;
  image: string;
  backgroundImage?: string;
  theme?: pageThemeType;
  characteristics: ICharacteristic[];
  description: string;
  parentSlug: string | null;
  // TODO highLightProduct & priority are missing right now, will be added in future
  highLightProduct?: {
    name: string;
    value: string;
  };
  priority?: number;
  subCategories: ICategory[];
}

export type eligibilityUnavailabilityReasonType =
  | ITEM_STATUS.IN_STOCK
  | ITEM_STATUS.OUT_OF_STOCK
  | ITEM_STATUS.PRE_ORDER;

export interface IBestDeviceOffers {
  id: string;
  productId: string;
  name: string;
  productName: string;
  description: string;
  group: string;
  bundle: boolean;
  characteristics: IKeyValue[];
  prices: IPrices[];
  attachments: IAttachments;
  // eligibilityUnavailabilityReason: IKeyValue[];
  unavailabilityReasonCodes: eligibilityUnavailabilityReasonType[];
}

export interface IAttachments {
  [key: string]: IImageURL[];
}

export interface IImageURL {
  name: string;
  url: string;
}
export interface IKeyValue {
  key: string;
  value: string;
}

export interface IProductBestDeviceOfferRequest {
  channel: string;
  page: number;
  itemPerPage: number;
  categories: ICategoryRequest[];
  filters: IFilterCharacteristic[];
  filterCharacteristics: IFilterCharacteristicBestDeviceOffer[];
}

export interface IFilterCharacteristicBestDeviceOffer {
  id: string;
  characteristicValues: string[];
  provideQuantity: boolean;
}

export interface ICategoryRequest {
  id: string;
  characteristics: ICharacteristic[];
}

export interface ICondition {
  id: string;
  value: string;
}
