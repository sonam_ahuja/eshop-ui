import {
  getBestDeviceOfferCategoryId,
  getCategorySlug,
  getCharacterstic
} from '@category/utils/helper';
import { Characteristics } from '@mocks/productList/productList.mock';
import appState from '@store/states/app';

describe('Helper', () => {
  it('getBestDeviceOfferCategoryId ::', () => {
    expect(
      typeof getBestDeviceOfferCategoryId(appState().categories.categories)
    ).toBe('string');
  });

  it('getCategorySlug ::', () => {
    expect(typeof getCategorySlug('/category/')).toBe('string');
    expect(typeof getCategorySlug('busket')).toBe('string');
  });

  it('getCharacterstic ::', () => {
    expect(typeof getCharacterstic(Characteristics, 'name')).toBe('string');
  });
});
