import { ICharacteristic } from '@category/store/types';
import * as categoryApi from '@src/common/types/api/categories';
import htmlKeys from '@common/constants/appkeys';

export const getCategorySlug = (pathname: string): string => {
  if (pathname && pathname.indexOf('/category/') !== -1) {
    const categorySlug = pathname.replace('/category/', '');
    if (categorySlug.substring(categorySlug.length - 1) === '/') {
      return categorySlug.substring(0, categorySlug.length - 1);
    }

    return categorySlug;
  } else {
    return '';
  }
};

/** USED ON PROLONGATION */
export const getCharacterstic = (
  characterstics: ICharacteristic[],
  key: string
) => {
  const characterstic = characterstics.find(charact => {
    return charact.name === key;
  });

  return characterstic && characterstic.values
    ? characterstic.values[0].value
    : '';
};

export const getBestDeviceOfferCategoryId = (
  categories: categoryApi.GET.IResponse
): string => {
  const characterstic = categories.characteristics.find(charact => {
    return charact.name === htmlKeys.BEST_DEVICE_OFFER_CATEGORY_ID;
  });

  return characterstic && characterstic.values
    ? characterstic.values[0].value
    : '';
};
