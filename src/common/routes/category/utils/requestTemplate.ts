import { IProductBestDeviceOfferRequest } from '@category/store/types';
import htmlKeys from '@common/constants/appkeys';
import APP_CONSTANTS from '@common/constants/appConstants';

const getProductListRequest = (): IProductBestDeviceOfferRequest => {
  return {
    channel: APP_CONSTANTS.CHANNEL,
    page: 0,
    itemPerPage: 4,
    filterCharacteristics: [
      {
        id: htmlKeys.CATEGORY_BEST_DEVICE_OFFERS,
        characteristicValues: [htmlKeys.CATEGORY_BEST_DEVICE_OFFERS],
        provideQuantity: false
      }
    ],
    categories: [],
    filters: []
  };
};

export const getProductListingRequestTemplate = (categoryId: string) => {
  const requestBody = getProductListRequest();
  requestBody.categories.push({
    id: categoryId,
    characteristics: []
  });

  return requestBody;
};
