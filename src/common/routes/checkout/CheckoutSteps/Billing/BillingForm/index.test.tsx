import React from 'react';
import { mount, shallow } from 'enzyme';
import { IProps } from '@checkout/CheckoutSteps/Billing/BillingForm/types';
import appState from '@store/states/app';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import Form, { BillingForm } from '.';

// tslint:disable-next-line: no-big-function
describe('<Shipping Billing />', () => {
  const initalValue = appState();
  const mockStore = configureStore();
  const store = mockStore(initalValue);
  window.scrollTo = jest.fn();
  const props: IProps = {
    billingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.billingInfo,
    billingTranslation: appState().translation.cart.checkout.billingInfo,
    billingInfo: appState().checkout.billing,
    shippingInfo: appState().checkout.shippingInfo,
    personalInfo: appState().checkout.personalInfo,
    updateBillingAddress: jest.fn(),
    setProceedButton: jest.fn(),
    updateFormChangeField: jest.fn(),
    updatePersonalInfoDummyState: jest.fn(),
    updateShippingInfoDummyState: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Form {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when billing info is same as personal', () => {
    const newProps: IProps = { ...props };
    const mutation: { sameAsPersonalInfo: boolean } = {
      sameAsPersonalInfo: true
    };
    newProps.billingInfo = { ...appState().checkout.billing, ...mutation };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Form {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when billing info is same as sameAsShippingInfo', () => {
    const newProps: IProps = { ...props };
    const mutation: { sameAsShippingInfo: boolean } = {
      sameAsShippingInfo: true
    };
    newProps.billingInfo = { ...appState().checkout.billing, ...mutation };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Form {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when billing info is not same as sameAsShippingInfo', () => {
    const newProps: IProps = { ...props };
    // tslint:disable-next-line:no-any
    newProps.billingInfo = '' as any;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Form {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('componentWillReceiveProps test for input field', () => {
    const copyComponentProps = { ...props };
    const component = shallow<Form>(
      <ThemeProvider theme={{}}>
        <Form {...copyComponentProps} />
      </ThemeProvider>
    );
    component.setProps(copyComponentProps);
    component.update();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <BillingForm {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component
      .find('BillingForm')
      .instance() as BillingForm).componentWillReceiveProps(copyComponentProps);
    (component.find('BillingForm').instance() as BillingForm).detectScroll();
    (component
      .find('BillingForm')
      .instance() as BillingForm).updateBillingAddress(
      'address',
      'address',
      true,
      'error'
    );
    (component.find('BillingForm').instance() as BillingForm).setSearchAddress({
      id: '1',
      streetNumber: '23',
      address: 'string',
      flatNumber: 'string',
      city: 'string',
      postCode: 'string',
      deliveryNote: 'string'
    });
    (component
      .find('BillingForm')
      .instance() as BillingForm).checkForGoogleSuggestionFlag({
      id: '1',
      streetNumber: '23',
      address: 'string',
      title: 'string',
      flatNumber: 'string',
      city: 'string',
      postCode: 'string',
      deliveryNote: 'string'
    });
    (component
      .find('BillingForm')
      .instance() as BillingForm).checkIfFieldExists('address');
    (component.find('BillingForm').instance() as BillingForm).onBlur('city');
    (component.find('BillingForm').instance() as BillingForm).onFocus(
      'address',
      // tslint:disable-next-line: no-object-literal-type-assertion
      { currentTarget: { value: 'string' } } as React.FocusEvent<
        HTMLInputElement
      >
    );
    (component
      .find('BillingForm')
      .instance() as BillingForm).componentWillUnmount();
  });

  test('test for billingAddressChangingInfo', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.billingTranslation.billingAddressChangingInfo = '';

    const component = mount<Form>(
      <ThemeProvider theme={{}}>
        <Form {...copyComponentProps} />
      </ThemeProvider>
    );
    (component
      .find('BillingForm')
      .instance() as Form).billingAddressChangingInfo();
  });

  test('handle all cases of component will recive props ', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <BillingForm {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    const copyComponentProps = { ...props };
    copyComponentProps.billingInfo.sameAsPersonalInfo = true;
    (component
      .find('BillingForm')
      .instance() as BillingForm).componentWillReceiveProps(copyComponentProps);
    const newProps1 = { ...copyComponentProps };
    newProps1.billingInfo.sameAsPersonalInfo = true;
    component.setProps(copyComponentProps);
    component.update();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.billingInfo.sameAsPersonalInfo = true;
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <BillingForm {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('BillingForm').instance() as BillingForm).setSearchAddress({
      id: '1',
      streetNumber: '23',
      address: 'string',
      flatNumber: 'string',
      city: 'string',
      postCode: 'string',
      deliveryNote: 'string'
    });
  });
});
