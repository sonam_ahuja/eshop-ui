import {
  IBillingFormField,
  IFormFieldValue
} from '@checkout/store/billing/types';
import { FormWrapper } from '@checkout/Common/FormWrapper/styles';
import { billingFormState } from '@checkout/CheckoutSteps/Billing/formGrid';
import React, { Component, ReactNode } from 'react';
import { isMobile } from '@src/common/utils';
import {
  IAddressResponse,
  IPersonalInfoFormFields
} from '@checkout/store/personalInfo/types';
import { Autocomplete, FloatLabelInput, Icon, Section } from 'dt-components';
import { checkIfValidateField } from '@checkout/CheckoutSteps/utils';
import { Column, Row } from '@common/components/Grid/styles';
import {
  getLabelValueForFormFields,
  isFormFieldValidAsPerType,
  isInputAllowed,
  readyToProceed
} from '@checkout/CheckoutSteps/Billing/utils/formFields';
import { getAddresses } from '@src/common/routes/checkout/store/personalInfo/services';
import {
  IProps,
  IState
} from '@checkout/CheckoutSteps/Billing/BillingForm/types';
import { BILLING_CONSTANTS } from '@src/common/routes/checkout/store/billing/enums';
import store from '@src/common/store';
import { fetchGeoMetadata } from '@checkout/utils/geoDataConverter';
import { IListItem } from '@src/common/routes/checkout/store/shipping/types';
import { sendContentFilledOutEvent } from '@events/checkout';
import { FORM_NAME } from '@events/constants/eventName';

import { StyledBillingForm, StyledInfo } from './styles';

export class BillingForm extends Component<IProps, IState> {
  filledOffSet: { name: string; offsetTop: number }[] = [];
  scrollPos = 0;

  constructor(props: IProps) {
    super(props);
    this.state = billingFormState();
    this.createForm = this.createForm.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.updateFromField = this.updateFromField.bind(this);
    this.updateBillingAddress = this.updateBillingAddress.bind(this);
    this.billingAddressChangingInfo = this.billingAddressChangingInfo.bind(
      this
    );
    this.checkForGoogleSuggestionFlag = this.checkForGoogleSuggestionFlag.bind(
      this
    );
  }

  componentDidMount(): void {
    if (this.props.billingInfo.sameAsPersonalInfo) {
      this.updateFromField(
        this.props.billingInfo.personalInfoDummyState,
        true,
        false
      );
    } else if (this.props.billingInfo.sameAsShippingInfo) {
      this.updateFromField(
        this.props.billingInfo.shippingInfoDummyState,
        true,
        false
      );
    } else if (this.props.billingInfo) {
      this.updateFromField(this.props.billingInfo, true, false);
    }
    window.addEventListener('scroll', this.detectScroll, {
      passive: true
    });
  }

  componentWillUnmount(): void {
    window.removeEventListener('scroll', this.detectScroll);
  }

  detectScroll(): void {
    this.scrollPos = document.body.getBoundingClientRect().top;
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const { billingInfo, personalInfo, shippingInfo } = this.props;

    if (
      nextProps.billingInfo.sameAsPersonalInfo !==
        billingInfo.sameAsPersonalInfo &&
      nextProps.billingInfo.sameAsPersonalInfo
    ) {
      this.updateFromField(personalInfo.formFields, true, false);
    } else if (
      nextProps.billingInfo.sameAsShippingInfo !==
        billingInfo.sameAsShippingInfo &&
      nextProps.billingInfo.sameAsShippingInfo
    ) {
      this.updateFromField(shippingInfo, true, false);
    } else if (nextProps.billingInfo.id !== billingInfo.id) {
      this.updateFromField(nextProps.billingInfo, true, false);
    } else if (
      nextProps.billingInfo.sameAsPersonalInfo !==
        billingInfo.sameAsPersonalInfo ||
      nextProps.billingInfo.sameAsShippingInfo !==
        billingInfo.sameAsShippingInfo
    ) {
      // case when same as different address is selected
      this.updateFromField(nextProps.billingInfo, true, false);
    }
  }

  handleChange(
    field: string,
    event: React.ChangeEvent<HTMLInputElement>
  ): void {
    const {
      fields: cmsFormConfiguration
    } = this.props.billingConfiguration.form;
    const inputValue = (event.target as HTMLInputElement).value.trim();

    const specificConfiguration = cmsFormConfiguration[field];

    if (
      !isInputAllowed(
        specificConfiguration.inputType,
        inputValue,
        specificConfiguration.validation
      )
    ) {
      return;
    }

    const isValidValue = isFormFieldValidAsPerType(
      specificConfiguration.validation.value,
      inputValue,
      specificConfiguration.validation.type
    );

    event.persist();

    this.setState(
      state => ({
        ...state,
        formFields: {
          ...state.formFields,
          [field]: {
            ...this.state.formFields[field],
            value: inputValue,
            isValid: isValidValue,
            validationMessage: specificConfiguration.validation.message
          }
        }
      }),
      () => {
        const status = readyToProceed(
          this.state.formFields,
          cmsFormConfiguration
        );

        if (
          !this.props.billingInfo.sameAsPersonalInfo &&
          !this.props.billingInfo.sameAsShippingInfo &&
          isValidValue
        ) {
          this.updateBillingAddress(
            field,
            inputValue,
            isValidValue,
            specificConfiguration.validation.message
          );
        }

        if (isValidValue && this.props.billingInfo.sameAsPersonalInfo) {
          this.props.updateFormChangeField();
          this.props.updatePersonalInfoDummyState({
            [field]: {
              value: this.state.formFields[field].value
            }
          });
        }

        if (isValidValue && this.props.billingInfo.sameAsShippingInfo) {
          this.props.updateFormChangeField();
          this.props.updateShippingInfoDummyState({
            [field]: {
              value: this.state.formFields[field].value
            }
          });
        }

        this.props.setProceedButton(status);
      }
    );
  }

  updateBillingAddress(
    field: string,
    inputValue: string,
    isValidState: boolean,
    validationMessage: string
  ): void {
    this.props.updateBillingAddress({
      key: field,
      value: inputValue,
      isValid: isValidState,
      validationMessage
    });
  }

  // tslint:disable-next-line:cognitive-complexity
  setSearchAddress(searchAddressResponse: IAddressResponse): void {
    for (const field in searchAddressResponse) {
      if (field === BILLING_CONSTANTS.ADDRESS) {
        if (this.state.formFields[BILLING_CONSTANTS.STREET_ADDRESS]) {
          if (this.props.billingInfo.sameAsPersonalInfo) {
            this.props.updatePersonalInfoDummyState({
              [BILLING_CONSTANTS.STREET_ADDRESS as string]: {
                value: searchAddressResponse[field]
              }
            });
          } else if (this.props.billingInfo.sameAsShippingInfo) {
            this.props.updateShippingInfoDummyState({
              [BILLING_CONSTANTS.STREET_ADDRESS as string]: {
                value: searchAddressResponse[field]
              }
            });
          } else {
            this.updateBillingAddress(
              BILLING_CONSTANTS.STREET_ADDRESS,
              searchAddressResponse[field] as string,
              true,
              this.props.billingConfiguration.form.fields[
                BILLING_CONSTANTS.STREET_ADDRESS
              ].validation.message
            );
          }
        }
      } else {
        let fieldTemp = field;

        if (field === BILLING_CONSTANTS.ID) {
          fieldTemp = BILLING_CONSTANTS.STREET_ADDRESS_ID;
        }

        if (this.state.formFields[field]) {
          if (this.props.billingInfo.sameAsPersonalInfo) {
            this.props.updatePersonalInfoDummyState({
              [fieldTemp]: {
                value: searchAddressResponse[field]
              }
            });
          } else if (this.props.billingInfo.sameAsShippingInfo) {
            this.props.updateShippingInfoDummyState({
              [fieldTemp]: {
                value: searchAddressResponse[field]
              }
            });
          } else {
            if (field === 'id') {
              this.updateBillingAddress(
                field,
                searchAddressResponse[field] as string,
                true,
                ''
              );
            } else {
              this.updateBillingAddress(
                field,
                searchAddressResponse[field] as string,
                true,
                this.props.billingConfiguration.form.fields[field].validation
                  .message
              );
            }
          }
        }
      }
    }

    this.updateFromField(searchAddressResponse, true, true);
  }

  checkIfFieldExists(field: string): boolean {
    return Object.keys(this.state.formFields).indexOf(field) > -1;
  }

  // tslint:disable-next-line:cognitive-complexity
  updateFromField(
    address: IAddressResponse | IBillingFormField | IPersonalInfoFormFields,
    isValid: boolean = false,
    isSearchData: boolean = false
  ): IBillingFormField {
    const newFormFields = billingFormState().formFields;
    const {
      fields: cmsFormConfiguration
    } = this.props.billingConfiguration.form;

    // tslint:disable:forin
    for (const field in newFormFields) {
      if (field === BILLING_CONSTANTS.STREET_ADDRESS) {
        if (address.hasOwnProperty(BILLING_CONSTANTS.ADDRESS)) {
          newFormFields[field as string].value =
            address[BILLING_CONSTANTS.ADDRESS];
          newFormFields[field as string].isValid = checkIfValidateField(
            field,
            address[BILLING_CONSTANTS.ADDRESS],
            isSearchData,
            isValid,
            cmsFormConfiguration
          );
        } else if (address.hasOwnProperty(BILLING_CONSTANTS.STREET_ADDRESS)) {
          newFormFields[field as string].value =
            address[BILLING_CONSTANTS.STREET_ADDRESS];
          newFormFields[field as string].isValid = checkIfValidateField(
            field,
            address[BILLING_CONSTANTS.STREET_ADDRESS],
            isSearchData,
            isValid,
            cmsFormConfiguration
          );
        }
      } else if (address.hasOwnProperty(field)) {
        newFormFields[field].value = address[field] ? address[field] : '';
        newFormFields[field].isValid = checkIfValidateField(
          field,
          newFormFields[field].value,
          isSearchData,
          isValid,
          cmsFormConfiguration
        );
      }
    }

    this.setState(
      state => ({
        ...state,
        formFields: newFormFields
      }),
      () => {
        const status = readyToProceed(
          this.state.formFields,
          cmsFormConfiguration
        );
        this.props.setProceedButton(status);
      }
    );

    return newFormFields;
  }

  onBlur(field: string): void {
    document.body.classList.remove('hideDrawer');
    this.props.updateFormChangeField();
    sendContentFilledOutEvent(
      FORM_NAME.BILLING_INFO,
      getLabelValueForFormFields(this.props, field)
    );
  }

  onFocus(field: string, event: React.FocusEvent<HTMLInputElement>): void {
    document.body.classList.add('hideDrawer');
    if (event) {
      this.filledOffSet.forEach(
        (element: { name: string; offsetTop: number }) => {
          if (
            element.name === field &&
            element.offsetTop > window.screen.height / 2
          ) {
            window.scrollTo(0, element.offsetTop / 1.5);
          }
        }
      );
    }
  }

  floatLabelReference(
    name: string,
    element: HTMLDivElement | null,
    index: number
  ): void {
    if (element) {
      this.filledOffSet[index] = { name, offsetTop: element.offsetTop };
    }
  }

  checkForGoogleSuggestionFlag(data: IListItem): void {
    const {
      enableGoogleAddressSuggestion
    } = store.getState().configuration.cms_configuration.global;

    if (enableGoogleAddressSuggestion) {
      fetchGeoMetadata(data.id as string, data.title).then(response => {
        this.setSearchAddress(response);
      });
    } else {
      this.setSearchAddress(data as IAddressResponse);
    }
  }

  createForm(): ReactNode | ReactNode[] {
    const { formFields } = this.state;
    const { sameAsPersonalInfo } = this.props.billingInfo;

    const {
      fields: cmsFormConfiguration
    } = this.props.billingConfiguration.form;

    return Object.keys(formFields).map((field, index) => {
      return (
        <>
          {cmsFormConfiguration[field] && cmsFormConfiguration[field].show && (
            <Column
              key={field}
              ref={element => {
                this.floatLabelReference(field, element, index);
              }}
              colMobile={formFields[field].colMobile}
              colDesktop={formFields[field].colDesktop}
              orderMobile={cmsFormConfiguration[field].order}
              orderDesktop={cmsFormConfiguration[field].order}
              className={field}
            >
              {field === BILLING_CONSTANTS.STREET_ADDRESS ||
              field === BILLING_CONSTANTS.ADDRESS ? (
                <Autocomplete
                  label={getLabelValueForFormFields(this.props, field)}
                  value={(formFields[field] as IFormFieldValue).value}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange(field, event);
                  }}
                  onBlur={() => {
                    this.onBlur(field);
                  }}
                  error={!(formFields[field] as IFormFieldValue).isValid}
                  errorMessage={
                    (formFields[field] as IFormFieldValue).validationMessage
                  }
                  tabIndex={cmsFormConfiguration[field].order}
                  disabled={
                    cmsFormConfiguration[field].readOnly && sameAsPersonalInfo
                  }
                  getItems={getAddresses}
                  onItemSelect={val =>
                    this.checkForGoogleSuggestionFlag(val as IListItem)
                  }
                  debounceInterval={300}
                  autoCompleteValuesOnly={false}
                />
              ) : (
                <FloatLabelInput
                  id={field}
                  label={getLabelValueForFormFields(this.props, field)}
                  type={cmsFormConfiguration[field].inputType}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange(field, event);
                  }}
                  onFocus={(event: React.FocusEvent<HTMLInputElement>) => {
                    this.onFocus(field, event);
                  }}
                  onBlur={() => {
                    this.onBlur(field);
                  }}
                  value={formFields[field].value}
                  error={!formFields[field].isValid}
                  errorMessage={formFields[field].validationMessage}
                  tabIndex={cmsFormConfiguration[field].order}
                  disabled={
                    cmsFormConfiguration[field].readOnly && sameAsPersonalInfo
                  }
                  isMobile={isMobile.phone}
                />
              )}
            </Column>
          )}
        </>
      );
    });
  }

  billingAddressChangingInfo(): ReactNode | ReactNode[] {
    const billingAddressChangingInfo = this.props.billingTranslation
      .billingAddressChangingInfo;
    const billingAddressChangingInfoEl =
      billingAddressChangingInfo.trim().length > 0 ? (
        <StyledInfo>
          <Icon color='currentColor' size='inherit' />
          <Section size='small'>{billingAddressChangingInfo}</Section>
        </StyledInfo>
      ) : null;

    return (
      !this.props.billingInfo.sameAsPersonalInfo &&
      !this.props.billingInfo.sameAsShippingInfo &&
      billingAddressChangingInfoEl
    );
  }

  render(): ReactNode {
    return (
      <StyledBillingForm>
        <FormWrapper className='formWrapper'>
          <Row>{this.createForm()}</Row>
          {this.billingAddressChangingInfo()}
        </FormWrapper>
      </StyledBillingForm>
    );
  }
}

// tslint:disable-next-line:max-file-line-count
export default BillingForm;
