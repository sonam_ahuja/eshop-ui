import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledInfo = styled.p`
  color: ${colors.mediumGray};
  margin-top: 1rem;
  display: flex;

  .dt_icon {
    margin-right: 0.25rem;
  }
  .dt_section {
    align-self: center;
  }
`;

export const StyledBillingForm = styled.div`
  .formWrapper {
    padding: 1.5rem 0 2rem;

    > div {
      margin-bottom: -1.4rem;
    }
  }
`;
