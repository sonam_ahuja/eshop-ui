import { IBillingInfoTranslation } from '@src/common/store/types/translation';
import { IBillingConfiguration } from '@src/common/store/types/configuration';
import {
  IBillingFormField,
  IBillingState,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import { IShippingAddressState } from '@checkout/store/shipping/types';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';

export interface IProps {
  billingConfiguration: IBillingConfiguration;
  billingTranslation: IBillingInfoTranslation;
  billingInfo: IBillingState;
  shippingInfo: IShippingAddressState;
  personalInfo: IPersonalInfoState;
  updateBillingAddress(data: IUpdateBillingAddress): void;
  setProceedButton(data: boolean): void;
  updateFormChangeField(): void;
  updatePersonalInfoDummyState(data: IBillingFormField): void;
  updateShippingInfoDummyState(data: IBillingFormField): void;
}

export interface IState {
  id: string;
  isDefault: boolean;
  isFormChanged: boolean;
  formFields: IBillingFormField;
}
