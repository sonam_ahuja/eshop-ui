import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import CardType from '.';

describe('<Payment CardType />', () => {
  const props = {};

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CardType {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
