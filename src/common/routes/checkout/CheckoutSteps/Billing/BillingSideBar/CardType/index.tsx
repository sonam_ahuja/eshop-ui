import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { PreloadImage } from 'dt-components';

const StyledCardType = styled.div`
  .cardImage {
    width: 3.75rem;
    height: 2.3125rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .cardImage {
      margin-top: 5.775rem;
      margin-bottom: 1.8rem;
      margin-right: 0;
      min-width: 9.8rem;
      max-width: 19, 3rem;
      height: 5.95rem;
    }
  }
`;

const CardType: FunctionComponent<{}> = () => {
  return (
    <StyledCardType>
      <div className='cardImage'>
        <PreloadImage
          imageUrl='https://i.ibb.co/C2Z0G5t/gray-card.png'
          imageHeight={10}
          imageWidth={10}
        />
      </div>
    </StyledCardType>
  );
};

export default CardType;
