import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import configureStore from 'redux-mock-store';

import MobileNav, { getBillingAddressType, IProps } from '.';

describe('<MobileNav />', () => {
  const props: IProps = {
    billing: appState().checkout.billing,
    billingTranslation: appState().translation.cart.checkout.billingInfo,
    setAddressBillingType: jest.fn()
  };

  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <MobileNav {...newProps} />
      </ThemeProvider>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, same as personal info is true', () => {
    const newProps: IProps = { ...props };
    newProps.billing.sameAsPersonalInfo = true;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, same as shipping info is true', () => {
    const newProps: IProps = { ...props };
    newProps.billing.sameAsShippingInfo = true;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('getBillingAddressType test ::', () => {
    const sameAsPersonalInfo = getBillingAddressType('sameAsPersonalInfo');
    expect(getBillingAddressType('sameAsPersonalInfo')).toBe(
      sameAsPersonalInfo
    );
    const sameAsShippingInfo = getBillingAddressType('sameAsShippingInfo');
    expect(getBillingAddressType('sameAsShippingInfo')).toBe(
      sameAsShippingInfo
    );
    const different = getBillingAddressType('different');
    expect(getBillingAddressType('different')).toBe(different);
  });

  test('render with store ', () => {
    const initalValue = appState();
    initalValue.checkout.shippingInfo.shippingType = SHIPPING_TYPE.SAME_DAY;
    const mockStore = configureStore();
    const store = mockStore(initalValue);
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileNav {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
  test('render with store without shipping type', () => {
    const initalValue = appState();
    // tslint:disable-next-line:no-any
    initalValue.checkout.shippingInfo.shippingType = '' as any;
    const mockStore = configureStore();
    const store = mockStore(initalValue);
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileNav {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
