import { IBillingInfoTranslation } from '@src/common/store/types/translation';
import { Select } from 'dt-components';
import {
  billingAddressType,
  IBillingState
} from '@checkout/store/billing/types';
import { BILLING_ADDRESS_TYPE, SHIPPING_TYPE } from '@checkout/store/enums';
import SelectWithIcon from '@common/components/Select/SelectWithIcon';
import React, { FunctionComponent } from 'react';
import SvgSaveAsPersonal from '@src/common/components/Svg/save-as-personal';
import SvgSameAsShipping from '@src/common/components/Svg/same-as-shipping';
import SvgDifferentAddress from '@src/common/components/Svg/different-address';
import store from '@src/common/store';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledPaymentBillingNav } from './styles';

export interface IProps {
  billing: IBillingState;
  billingTranslation: IBillingInfoTranslation;
  className?: string;
  setAddressBillingType(type: billingAddressType): void;
}

export const getSelectedNode = (
  billing: IBillingState,
  billingTranslation: IBillingInfoTranslation
) => {
  if (billing.sameAsPersonalInfo) {
    return {
      id: 'sameAsPersonalInfo',
      title: billingTranslation.addressType.sameAsPersonalInfo
    };
  }

  if (billing.sameAsShippingInfo) {
    return {
      id: 'sameAsShippingInfo',
      title: billingTranslation.addressType.sameAsShippingInfo
    };
  }

  return {
    id: 'differentAddress',
    title: billingTranslation.addressType.differentAddress
  };
};

export const getMobileNavList = (
  billingTranslation: IBillingInfoTranslation
) => {
  const shippingType = store.getState().checkout.shippingInfo.shippingType;

  if (
    shippingType === SHIPPING_TYPE.STANDARD ||
    shippingType === SHIPPING_TYPE.EXACT_DAY ||
    shippingType === SHIPPING_TYPE.SAME_DAY
  ) {
    return [
      {
        id: 'sameAsPersonalInfo',
        title: billingTranslation.addressType.sameAsPersonalInfo
      },
      {
        id: 'sameAsShippingInfo',
        title: billingTranslation.addressType.sameAsShippingInfo
      },
      {
        id: 'differentAddress',
        title: billingTranslation.addressType.differentAddress
      }
    ];
  } else {
    return [
      {
        id: 'sameAsPersonalInfo',
        title: billingTranslation.addressType.sameAsPersonalInfo
      },
      {
        id: 'differentAddress',
        title: billingTranslation.addressType.differentAddress
      }
    ];
  }
};

export const getBillingAddressType = (selectedId: string) => {
  if (selectedId === 'sameAsPersonalInfo') {
    return BILLING_ADDRESS_TYPE.PERSONAL_INFO;
  }

  if (selectedId === 'sameAsShippingInfo') {
    return BILLING_ADDRESS_TYPE.SHIPPING_INFO;
  }

  return BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS;
};

export const getSVG = (selectedId: string) => {
  if (selectedId === 'sameAsPersonalInfo') {
    return <SvgSaveAsPersonal />;
  }

  if (selectedId === 'sameAsShippingInfo') {
    return <SvgSameAsShipping />;
  }

  return <SvgDifferentAddress />;
};

export const MobileNav: FunctionComponent<IProps> = (props: IProps) => {
  const {
    setAddressBillingType,
    billing,
    billingTranslation,
    className
  } = props;

  const selectedNode = getSelectedNode(billing, billingTranslation);

  return (
    <StyledPaymentBillingNav className={className}>
      {/* <Avatar
        imageUrl='https://i.ibb.co/gw5gHks/thumbnail.png'
        size='large'
        className='avatar'
        shape='rectangle'
      /> */}
      <Select
        useNativeDropdown={true}
        onItemSelect={selectedItem => {
          sendDropdownClickEvent(selectedItem.title);
          setAddressBillingType(
            getBillingAddressType(selectedItem.id as string)
          );
        }}
        SelectedItemComponent={selectedItemProps => (
          <SelectWithIcon
            {...selectedItemProps}
            svgNode={getSVG(selectedNode.id)}
            description={
              selectedItemProps.selectedItem
                ? selectedItemProps.selectedItem.title
                : ''
            }
          />
        )}
        items={getMobileNavList(billingTranslation)}
        selectedItem={selectedNode}
      />
    </StyledPaymentBillingNav>
  );
};

export default MobileNav;
