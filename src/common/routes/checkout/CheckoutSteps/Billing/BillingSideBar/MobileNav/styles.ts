import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPaymentBillingNav = styled.div`
  > div {
    width: 100%;
  }

  margin-left: -1.25rem;
  margin-right: -1.25rem;

  .dt_outsideClick {
    > div {
      background: #fff;
      padding: 0 1.25rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
  }
`;
