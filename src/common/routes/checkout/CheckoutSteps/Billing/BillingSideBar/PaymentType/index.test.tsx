import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import PaymentType, { IProps } from '.';

describe('<PaymentType/>', () => {
  const props: IProps = {
    billing: appState().checkout.billing,
    billingTranslation: appState().translation.cart.checkout.billingInfo,
    setAddressBillingType: jest.fn()
  };
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <ThemeProvider theme={{}}>
        <PaymentType {...newProps} />
      </ThemeProvider>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when address same as personal info', () => {
    const newProps = { ...props };
    newProps.billing = {
      ...newProps.billing,
      sameAsPersonalInfo: true,
      sameAsShippingInfo: false
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly,  when address flag set for both', () => {
    const newProps = { ...props };
    newProps.billing = {
      ...newProps.billing,
      sameAsPersonalInfo: true,
      sameAsShippingInfo: true
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly,  when address is different', () => {
    const newProps = { ...props };
    newProps.billing = {
      ...newProps.billing,
      sameAsPersonalInfo: false,
      sameAsShippingInfo: false
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-commented-code
  // test('on bill address select', () => {
  //   const component = componentWrapper(props);
  //   const newComponent = component.find('div[onClick]');
  //   newComponent.at(0).simulate('click');
  //   newComponent.at(1).simulate('click');
  //   newComponent.at(2).simulate('click');
  // });
});
