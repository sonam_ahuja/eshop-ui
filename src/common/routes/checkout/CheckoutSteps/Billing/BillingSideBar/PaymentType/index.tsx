import React, { Fragment, ReactNode } from 'react';
import {
  billingAddressType,
  IBillingState
} from '@checkout/store/billing/types';
import { BILLING_ADDRESS_TYPE, SHIPPING_TYPE } from '@checkout/store/enums';
import { IBillingInfoTranslation } from '@src/common/store/types/translation';
import SvgSaveAsPersonal from '@src/common/components/Svg/save-as-personal';
import SvgSameAsShipping from '@src/common/components/Svg/same-as-shipping';
import SvgDifferentAddress from '@src/common/components/Svg/different-address';
import SideNavTabs from '@src/common/routes/checkout/Common/SideNavTabs';
import store from '@src/common/store';
import { sendCTAClicks } from '@events/index';

export interface IProps {
  billing: IBillingState;
  billingTranslation: IBillingInfoTranslation;
  setAddressBillingType(type: billingAddressType): void;
}

const setBillingType = (
  adressType: billingAddressType,
  fn: (type: billingAddressType) => void,
  addressName: string
) => {
  return () => {
    sendCTAClicks(addressName, window.location.href);
    fn(adressType);
  };
};

class PaymentType extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);

    this.getBillingSvgs = this.getBillingSvgs.bind(this);
  }

  getBillingSvgs(): ReactNode {
    const { billing } = this.props;

    switch (true) {
      case billing.sameAsPersonalInfo:
        return <SvgSaveAsPersonal />;
      case billing.sameAsShippingInfo:
        return <SvgSameAsShipping />;
      default:
        return <SvgDifferentAddress />;
    }
  }

  render(): ReactNode {
    const { setAddressBillingType, billing, billingTranslation } = this.props;

    const shippingType = store.getState().checkout.shippingInfo.shippingType;

    let showShipping = false;

    if (
      shippingType === SHIPPING_TYPE.STANDARD ||
      shippingType === SHIPPING_TYPE.EXACT_DAY ||
      shippingType === SHIPPING_TYPE.SAME_DAY
    ) {
      showShipping = true;
    }

    return (
      <Fragment>
        <div className='iconWrap'>{this.getBillingSvgs()}</div>

        <SideNavTabs
          tabIndex={0}
          onKeyDown={setBillingType(
            BILLING_ADDRESS_TYPE.PERSONAL_INFO,
            setAddressBillingType,
            billingTranslation.addressType.sameAsPersonalInfo
          )}
          onClick={setBillingType(
            BILLING_ADDRESS_TYPE.PERSONAL_INFO,
            setAddressBillingType,
            billingTranslation.addressType.sameAsPersonalInfo
          )}
          isActive={billing.sameAsPersonalInfo}
          translatedValue={billingTranslation.addressType.sameAsPersonalInfo}
        />

        {showShipping && (
          <SideNavTabs
            tabIndex={0}
            onKeyDown={setBillingType(
              BILLING_ADDRESS_TYPE.SHIPPING_INFO,
              setAddressBillingType,
              billingTranslation.addressType.sameAsShippingInfo
            )}
            onClick={setBillingType(
              BILLING_ADDRESS_TYPE.SHIPPING_INFO,
              setAddressBillingType,
              billingTranslation.addressType.sameAsShippingInfo
            )}
            isActive={billing.sameAsShippingInfo}
            translatedValue={billingTranslation.addressType.sameAsShippingInfo}
          />
        )}

        <SideNavTabs
          tabIndex={0}
          onKeyDown={setBillingType(
            BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS,
            setAddressBillingType,
            billingTranslation.addressType.differentAddress
          )}
          onClick={setBillingType(
            BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS,
            setAddressBillingType,
            billingTranslation.addressType.differentAddress
          )}
          isActive={!billing.sameAsPersonalInfo && !billing.sameAsShippingInfo}
          translatedValue={billingTranslation.addressType.differentAddress}
        />
      </Fragment>
    );
  }
}

export default PaymentType;
