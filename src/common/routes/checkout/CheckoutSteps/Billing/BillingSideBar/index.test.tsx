import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import PaymentBilling, { IProps as IPaymentBillingProps } from '.';

describe('<Billing/>', () => {
  const props: IPaymentBillingProps = {
    billing: appState().checkout.billing,
    billingTranslation: appState().translation.cart.checkout.billingInfo,
    billingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.billingInfo,
    setAddressBillingType: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentBilling {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
