import React, { FunctionComponent } from 'react';
import PaymentType from '@checkout/CheckoutSteps/Billing/BillingSideBar/PaymentType';
import {
  billingAddressType,
  IBillingState
} from '@checkout/store/billing/types';
import { IBillingInfoTranslation } from '@src/common/store/types/translation';
import { IBillingConfiguration } from '@src/common/store/types/configuration';

import { StyledBillingSideBar } from './styles';

export interface IProps {
  billing: IBillingState;
  billingTranslation: IBillingInfoTranslation;
  billingConfiguration: IBillingConfiguration;
  setAddressBillingType(type: billingAddressType): void;
}
const BillingSideBar: FunctionComponent<IProps> = (props: IProps) => {
  return (
    <StyledBillingSideBar>
      <PaymentType {...props} />
    </StyledBillingSideBar>
  );
};

export default BillingSideBar;
