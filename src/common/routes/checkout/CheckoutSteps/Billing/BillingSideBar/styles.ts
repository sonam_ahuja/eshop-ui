import styled from 'styled-components';

export const StyledBillingSideBar = styled.div`
  .iconWrap {
    font-size: 120px;
    text-align: center;
    line-height: 0;
    margin-bottom: 1.7rem;
    margin-top: 2.2rem;
    svg {
      height: 1em;
      width: 1em;
    }
  }
`;
