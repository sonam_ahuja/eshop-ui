import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import CardInfo, { IProps } from '.';

describe('<BillingAppShell />', () => {
  const props: IProps = {
    cardNumber: 'cardnumber',
    nameOnCard: 'name',
    expirationDate: '',
    title: '',
    billingTranslation: appState().translation.cart.checkout.billingInfo,
    onClickHandler: jest.fn(),
    paymentType: 'string'
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CardInfo {...props} />
      </ThemeProvider>
    );
    component.find('div[onClick]').simulate('click');
    expect(component).toMatchSnapshot();
  });
});
