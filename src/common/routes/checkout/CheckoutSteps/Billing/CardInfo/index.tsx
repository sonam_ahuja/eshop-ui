import React, { FunctionComponent } from 'react';
import { StyledCardInfo } from '@checkout/CheckoutSteps/Billing/CardInfo/styles';
import { IBillingInfoTranslation } from '@src/common/store/types/translation';
import { Column, Row } from '@src/common/components/Grid/styles';
import KeyValue from '@src/common/components/KeyValue';
import { Divider, Section } from 'dt-components';
import LinkWithIcon from '@src/common/components/LinkWithIcon';
import { ReadOnlyForm } from '@checkout/Common/ReadOnlyForm/styles';

export interface IProps {
  title: string;
  cardNumber: string;
  nameOnCard?: string;
  expirationDate: string;
  billingTranslation: IBillingInfoTranslation;
  paymentType: string;
  onClickHandler(data: string): void;
}
const CardInfo: FunctionComponent<IProps> = (props: IProps) => {
  const {
    cardNumber,
    nameOnCard,
    expirationDate,
    onClickHandler,
    paymentType,
    title
  } = props;
  const { cardDetail } = props.billingTranslation;

  const onClick = (): void => {
    onClickHandler(paymentType);
  };

  return (
    <StyledCardInfo>
      {/* <StyledEdit onClick={onClick} onKeyDown={onKeyDownEnter} tabIndex={0}>
        <StyledLabel>{cardDetail.edit}</StyledLabel>
        <Icon name='ec-edit' size='xsmall' color='#e20074' className='' />
      </StyledEdit> */}

      <div className='heading'>
        <div className='textWrap'>
          <Section size='small' transform='uppercase'>
            {title}
          </Section>
          <Section size='small' firstLetterTransform='uppercase'>
            <LinkWithIcon text={cardDetail.edit} onClick={onClick} />
          </Section>
        </div>
        <Divider dashed orientation='horizontal' />
      </div>

      <ReadOnlyForm>
        <Row>
          <Column
            className='cardNumber'
            colDesktop={4}
            colMobile={5}
            orderDesktop={1}
          >
            <KeyValue
              labelName={cardDetail.cardNumber}
              labelValue={cardNumber}
            />
          </Column>

          <Column
            className='nameOnCard'
            colDesktop={4}
            colMobile={4}
            orderDesktop={2}
          >
            <KeyValue
              labelName={cardDetail.nameOnCard}
              labelValue={nameOnCard as string}
            />
          </Column>

          <Column
            className='expiryDate'
            colMobile={3}
            colDesktop={3}
            orderDesktop={3}
          >
            <KeyValue
              labelName={cardDetail.expirationDate}
              labelValue={expirationDate}
            />
          </Column>
        </Row>
      </ReadOnlyForm>
    </StyledCardInfo>
  );
};

export default CardInfo;
