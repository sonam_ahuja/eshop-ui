import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Column, Row } from '@src/common/components/Grid/styles';

export const StyledCardInfo = styled.div`
  margin-bottom: 2.5rem;

  .heading {
    color: ${colors.mediumGray};
    .textWrap {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }

    .dt_divider {
      margin: 0;
      margin-top: 0.5rem;
      margin-bottom: 1.75rem;
    }
  }

  ${Row} {
    ${Column} {
      margin-bottom: 0;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    margin-bottom: 6.1rem;
  }
`;
