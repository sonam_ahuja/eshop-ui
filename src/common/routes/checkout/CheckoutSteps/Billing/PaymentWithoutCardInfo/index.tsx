import React, { FunctionComponent } from 'react';
import {
  IBillingInfoTranslation,
  IOrderReviewTranslation
} from '@src/common/store/types/translation';
import { Column, Row } from '@src/common/components/Grid/styles';
import KeyValue from '@src/common/components/KeyValue';
import { Divider, Section } from 'dt-components';
import LinkWithIcon from '@src/common/components/LinkWithIcon';
import { ReadOnlyForm } from '@checkout/Common/ReadOnlyForm/styles';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

import { StyledPaymentWithoutCardInfo } from './styles';

export interface IProps {
  paymentTypeSelected: PAYMENT_TYPE;
  orderReviewTranslation: IOrderReviewTranslation;
  isEditNeeded?: boolean;
  isBillingSection?: boolean;
  title?: string;
  paymentType?: string;
  billingTranslation?: IBillingInfoTranslation;
  onClickHandler?(data: string): void;
}
const PaymentWithoutCardInfo: FunctionComponent<IProps> = (props: IProps) => {
  const {
    paymentModeSelected,
    paymentModeText
  } = props.orderReviewTranslation.paymentBilling;
  const {
    paymentTypeSelected,
    title,
    onClickHandler,
    paymentType
  } = props;
  let cardDetail = {
    cardNumber: '',
    nameOnCard: '',
    expirationDate: '',
    edit: ''
  };

  if (props.billingTranslation) {
    cardDetail = props.billingTranslation.cardDetail;
  }

  const onClick = (): void => {
    if (onClickHandler) {
      onClickHandler(paymentType as string);
    }
  };

  return (
    <StyledPaymentWithoutCardInfo>
      <div className='heading'>
        <div className='textWrap'>
          <Section size='small' transform='uppercase'>
            {title}
          </Section>
          <Section size='small' firstLetterTransform='uppercase'>
            <LinkWithIcon text={cardDetail.edit} onClick={onClick} />
          </Section>
        </div>
        <Divider dashed orientation='horizontal' />
      </div>

      <ReadOnlyForm>
        <Row>
          <Column
            colMobile={12}
          >
            <KeyValue
              labelName={paymentModeSelected}
              labelValue={paymentModeText[paymentTypeSelected]}
            />
          </Column>
        </Row>
      </ReadOnlyForm>
    </StyledPaymentWithoutCardInfo>
  );
};

export default PaymentWithoutCardInfo;
