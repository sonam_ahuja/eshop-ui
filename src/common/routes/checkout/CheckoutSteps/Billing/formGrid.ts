import { IState } from '@checkout/CheckoutSteps/Billing/BillingForm/types';

export const billingFormState = (): IState => {
  return {
    id: '',
    isDefault: true,
    isFormChanged: false,
    formFields: {
      streetAddress: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 12,
        colMobile: 12
      },
      streetNumber: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      flatNumber: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      city: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      postCode: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      unit: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      deliveryNote: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 12,
        colMobile: 12
      }
    }
  };
};
