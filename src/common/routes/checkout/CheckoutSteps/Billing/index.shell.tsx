import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { Button, Switch } from 'dt-components';

const StyledBillingAddressShell = styled.div`
  color: #dadada;
  padding: 5rem 2.25rem;
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  margin: 0 0 5.65rem 0;

  ul {
    display: flex;
    margin: 0 -1rem;
  }
  ul li {
    flex: 1;
    width: 100%;
    padding: 1rem;
    margin-bottom: 1rem;
  }
  ul li span {
    height: 0.5rem;
    display: block;
    background: currentColor;
  }
  footer {
    margin-top: 6rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
const BillingInformationShell = () => {
  return (
    <StyledShell>
      <StyledBillingAddressShell>
        <section>
          <ul>
            <li>
              <span className='shine' />
            </li>
            <li>
              <span className='shine' />
            </li>
          </ul>
          <ul>
            <li>
              <span className='shine' />
            </li>
            <li>
              <span className='shine' />
            </li>
          </ul>
          <ul>
            <li>
              <span className='shine' />
            </li>
            <li>
              <span className='shine' />
            </li>
          </ul>
          <ul>
            <li>
              <span className='shine' />
            </li>
            <li>
              <span className='shine' />
            </li>
            <li>
              <span className='shine' />
            </li>
          </ul>
        </section>
        <footer>
          <Switch on={false} disabled />
          <Button size='small' disabled />
        </footer>
      </StyledBillingAddressShell>
    </StyledShell>
  );
};
export default BillingInformationShell;
