import React from 'react';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { BILLING_TYPE } from '@checkout/store/enums';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { cardDetails } from '@mocks/checkout/payment';

import Billing, { mapDispatchToProps } from '.';
import BillingInformationShell from './index.shell';

describe('<Billing />', () => {
  let initalValue = appState();
  const mockStore = configureStore();
  const store = mockStore(initalValue);
  window.scrollTo = jest.fn();
  const componentWrapper = () => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Billing />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };

  beforeEach(() => {
    initalValue = appState();
  });

  test('should render properly', () => {
    const component = componentWrapper();
    expect(component).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = {};
    const component = mount(
      <ThemeProvider theme={{}}>
        <BillingInformationShell {...appShellProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render with default upfront card detail ', () => {
    initalValue.checkout.payment.upFront.cardDetails = [
      {
        cardNumber: '',
        isDefault: true,
        cardType: '',
        expiryDate: '',
        securityCode: '',
        nameOnCard: '',
        id: '12212',
        isCardSaved: true,
        lastFourDigits: '4211',
        brand: 'visa'
      }
    ];
    const component = componentWrapper();
    expect(component).toMatchSnapshot();
  });
  test('should render with upfront card detail when default is false', () => {
    initalValue.checkout.payment.upFront.cardDetails = [cardDetails];
    const component = componentWrapper();
    expect(component).toMatchSnapshot();
  });
  test('should render with Billing type and monthly price is false', () => {
    initalValue.checkout.checkout.isMonthlyPrice = false;
    initalValue.checkout.billing.typeOfBill = BILLING_TYPE.PAPER_BILL;
    initalValue.checkout.payment.upFront.cardDetails = [cardDetails];
    const component = componentWrapper();
    expect(component).toMatchSnapshot();
  });
  test('should render with Monthly card detail when default is false', () => {
    initalValue.checkout.payment.monthly.cardDetails = [cardDetails];
    const component = componentWrapper();
    expect(component).toMatchSnapshot();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const mapDispatch = mapDispatchToProps(dispatch);

    const data = {
      key: 'key',
      value: 'qwq',
      isValid: true,
      validationMessage: 'msg'
    };
    const billingFormData = {};
    mapDispatch.sendBillingAddress();
    mapDispatch.updateFormChangeField();
    mapDispatch.updateFormChangeField();
    mapDispatch.updateBillingAddress(data);
    mapDispatch.enableProceedButton(true);
    mapDispatch.updatePersonalInfoDummyState(billingFormData);
    mapDispatch.updateShippingInfoDummyState(billingFormData);
    mapDispatch.setBillingType(BILLING_TYPE.E_BILL, true);
  });
});
