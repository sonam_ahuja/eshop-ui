import {
  ICompDispatchToProps,
  IStateToProps
} from '@checkout/CheckoutSteps/Billing/types';
import {
  billingAddressType,
  billingType,
  IBillingFormField,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import React from 'react';
import {
  StyledBilling,
  StyledBillingCard,
  StyledBillingCardFull,
  StyledBillingTypeCard,
  StyledMonthlyCardInfo,
  StyledUpfrontCardInfo
} from '@checkout/CheckoutSteps/Billing/styles';
import { connect } from 'react-redux';
import MobileNav from '@checkout/CheckoutSteps/Billing/BillingSideBar/MobileNav';
import BillingSideBar from '@checkout/CheckoutSteps/Billing/BillingSideBar';
import billingActions from '@checkout/store/billing/actions';
import { RootState } from '@common/store/reducers';
import { Dispatch } from 'redux';
import { Button, Loader, Paragraph } from 'dt-components';
import StepNavigation from '@checkout/CheckoutSteps/StepNavigation';
import paymentActions from '@checkout/store/payment/actions';
import { ICardDetails } from '@checkout/store/payment/types';
import { BILLING_TYPE, PAYMENT_TYPE } from '@checkout/store/enums';
import { isMobile } from '@common/utils';
import SideNavigation from '@checkout/SideNavigation';
import BillingForm from '@checkout/CheckoutSteps/Billing/BillingForm';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { sendProceedEvent } from '@events/checkout/index';
import EVENT_NAME, {
  CHECKOUT_VIEW,
  PAGE_VIEW
} from '@events/constants/eventName';
import { pageViewEvent, sendCTAClicks } from '@events/index';
import Footer from '@src/common/components/FooterWithTermsAndConditions';
import { skipMonthlyStep } from '@checkout/store/payment/transformer';
import Loadable from 'react-loadable';
import checkoutActions from '@checkout/store/actions';

import CardInfo from './CardInfo';
import PaymentWithoutCardInfo from './PaymentWithoutCardInfo';

const CheckoutAppShellComponent = Loadable({
  loading: () => null,
  loader: () => import('@common/routes/checkout/CheckoutSteps/index.shell')
});

interface IRouterParams {
  history?: string;
  location?: string;
  martch?: string;
}
type IProps = IStateToProps &
  ICompDispatchToProps &
  RouteComponentProps<IRouterParams>;

class Billing extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  componentDidMount(): void {
    this.props.setBillingType(this.props.billingInfo.typeOfBill, true);
    pageViewEvent(PAGE_VIEW.CHECKOUT_BILLING_INFO);
    window.scrollTo(0, 0);
    sendProceedEvent(CHECKOUT_VIEW.BILLING.STEP, CHECKOUT_VIEW.BILLING.VIEW);
  }

  // tslint:disable:cognitive-complexity
  // tslint:disable-next-line:no-big-function
  render(): JSX.Element {
    const {
      eBill,
      paperBill,
      billingAddress,
      upfrontText,
      monthlyText,
      reviewOrder,
      discountDisableTitle,
      discountDisableDesc
    } = this.props.billingTranslation;

    const {
      paymentInfo: { upFront, monthly },
      isMonthlyPrice,
      isUpfrontPrice,
      billingInfo,
      editPaymentStep,
      billingTranslation,
      setAddressBillingType,
      billingConfiguration,
      checkoutTranslation,
      editPaymentStepClick,
      currency,
      translation,
      mainAppShell,
      fetchCheckoutAddressLoading
    } = this.props;

    // tslint:disable-next-line:max-line-length
    const eBillDiscountText =
      billingInfo.eBillAmount < 0
        ? `${-billingInfo.eBillAmount} ${currency} ${eBill.discountText}`
        : `${paperBill.discountText}`;
    // tslint:disable-next-line:max-line-length
    const paperBillDiscountText =
      billingInfo.paperBillAmount < 0
        ? `${-billingInfo.paperBillAmount} ${currency} ${eBill.discountText}`
        : `${paperBill.discountText}`;

    const onEBillClick = (): void => {
      if (billingInfo.typeOfBill === BILLING_TYPE.PAPER_BILL) {
        this.props.setBillingType(BILLING_TYPE.E_BILL, false);
        sendCTAClicks(BILLING_TYPE.E_BILL, window.location.href);
      }
    };

    const onPaperBillClick = (): void => {
      if (billingInfo.typeOfBill === BILLING_TYPE.E_BILL) {
        this.props.setBillingType(BILLING_TYPE.PAPER_BILL, false);
        sendCTAClicks(BILLING_TYPE.PAPER_BILL, window.location.href);
      }
    };

    const submitBilling = (): void => {
      this.props.sendBillingAddress();
    };

    const { orderReview: orderReviewTranslation } = translation.cart.checkout;

    return (
      <StyledBilling>
        {!mainAppShell && billingInfo.billLoader && (
          <Loader
            open={billingInfo.billLoader}
            label={billingTranslation.loadingText}
          />
        )}

        {mainAppShell ? (
          <CheckoutAppShellComponent
            checkoutTranslation={checkoutTranslation}
            editPaymentStepClick={editPaymentStepClick}
            fetchCheckoutAddressLoading={fetchCheckoutAddressLoading}
          />
        ) : (
          <>
            {!isMobile.phone && (
              <SideNavigation>
                <BillingSideBar
                  billingConfiguration={billingConfiguration}
                  billingTranslation={billingTranslation}
                  setAddressBillingType={setAddressBillingType}
                  billing={billingInfo}
                />
              </SideNavigation>
            )}
            {/*** psr comment starts here */}
            <div className='mainContent'>
              <StepNavigation
                checkoutTranslation={checkoutTranslation}
                editPaymentStepClick={editPaymentStepClick}
              />
              {isUpfrontPrice ? (
                <StyledUpfrontCardInfo>
                  {upFront.paymentTypeSelected !==
                  PAYMENT_TYPE.CREDIT_DEBIT_CARD ? (
                    <PaymentWithoutCardInfo
                      paymentTypeSelected={upFront.paymentTypeSelected}
                      orderReviewTranslation={orderReviewTranslation}
                      title={upfrontText}
                      billingTranslation={this.props.billingTranslation}
                      isEditNeeded={true}
                      isBillingSection={true}
                      paymentType={'upfront'}
                      onClickHandler={editPaymentStep}
                    />
                  ) : null}
                  {upFront.cardDetails.map(
                    (card: ICardDetails, index: number) =>
                      card.isDefault ? (
                        <CardInfo
                          title={upfrontText}
                          key={`'#upfront' + ${index}`}
                          billingTranslation={this.props.billingTranslation}
                          cardNumber={card.cardNumber}
                          nameOnCard={card.nameOnCard}
                          paymentType={'upfront'}
                          expirationDate={card.expiryDate}
                          onClickHandler={editPaymentStep}
                        />
                      ) : null
                  )}
                </StyledUpfrontCardInfo>
              ) : null}

              {isMonthlyPrice ? (
                <StyledMonthlyCardInfo>
                  {monthly.paymentTypeSelected !==
                  PAYMENT_TYPE.CREDIT_DEBIT_CARD ? (
                    <PaymentWithoutCardInfo
                      paymentTypeSelected={monthly.paymentTypeSelected}
                      orderReviewTranslation={orderReviewTranslation}
                      title={monthlyText}
                      billingTranslation={this.props.billingTranslation}
                      isEditNeeded={skipMonthlyStep(
                        monthly.enabledPaymentMethods
                      )}
                      isBillingSection={true}
                      paymentType={'monthly'}
                      onClickHandler={editPaymentStep}
                    />
                  ) : (
                    monthly.cardDetails.map(
                      (card: ICardDetails, index: number) =>
                        card.isDefault ? (
                          <CardInfo
                            title={monthlyText}
                            billingTranslation={this.props.billingTranslation}
                            key={`'#monthly' + ${index}`}
                            cardNumber={card.cardNumber}
                            nameOnCard={card.nameOnCard}
                            paymentType={'monthly'}
                            expirationDate={card.expiryDate}
                            onClickHandler={editPaymentStep}
                          />
                        ) : null
                    )
                  )}
                </StyledMonthlyCardInfo>
              ) : null}

              {isMobile.phone && (
                <MobileNav
                  className='mobileNavAddressType'
                  billing={billingInfo}
                  billingTranslation={billingTranslation}
                  setAddressBillingType={setAddressBillingType}
                />
              )}

              <Paragraph className='addressHeading' size='large' weight='bold'>
                {billingAddress}
              </Paragraph>
              <BillingForm
                updateFormChangeField={this.props.updateFormChangeField}
                setProceedButton={this.props.enableProceedButton}
                updateBillingAddress={this.props.updateBillingAddress}
                updatePersonalInfoDummyState={
                  this.props.updatePersonalInfoDummyState
                }
                updateShippingInfoDummyState={
                  this.props.updateShippingInfoDummyState
                }
                billingConfiguration={this.props.billingConfiguration}
                // setOrderReviewNavigationButton={setOrderReviewNavigationButton}
                billingInfo={this.props.billingInfo}
                personalInfo={this.props.personalInfo}
                shippingInfo={this.props.shippingInfo}
                // updateInputField={updateInputField}
                billingTranslation={this.props.billingTranslation}
              />

              {/*** psr comment starts here */}

              {billingInfo.billApplicable &&
              billingConfiguration.eBillDiscount ? (
                <StyledBillingTypeCard>
                  {billingInfo.ebillApplicable && (
                    <StyledBillingCard
                      children={null}
                      titleClassName={'title'}
                      descClassName={'description'}
                      detailClassName={'detail'}
                      onClick={onEBillClick}
                      title={eBill.typeText}
                      description={eBill.infoText}
                      detail={eBillDiscountText}
                      className={
                        billingInfo.typeOfBill === BILLING_TYPE.E_BILL
                          ? 'active'
                          : undefined
                      }
                    />
                  )}
                  {billingInfo.paperBillApplicable && (
                    <StyledBillingCard
                      children={null}
                      titleClassName={'title'}
                      descClassName={'description'}
                      detailClassName={'detail'}
                      onClick={onPaperBillClick}
                      title={paperBill.typeText}
                      description={paperBill.infoText}
                      detail={paperBillDiscountText}
                      className={
                        billingInfo.typeOfBill === BILLING_TYPE.PAPER_BILL
                          ? 'active'
                          : undefined
                      }
                    />
                  )}
                </StyledBillingTypeCard>
              ) : (
                <StyledBillingTypeCard>
                  <StyledBillingCardFull
                    children={null}
                    onClick={onEBillClick}
                    detail={discountDisableTitle}
                    description={discountDisableDesc}
                  />
                </StyledBillingTypeCard>
              )}

              <div className='bottomCTA'>
                <Button
                  size='small'
                  disabled={!this.props.buttonEnable}
                  onClickHandler={submitBilling}
                  loading={billingInfo.loading}
                  data-event-id={EVENT_NAME.CHECKOUT.EVENTS.REVIEW_ORDER}
                  data-event-message={reviewOrder}
                >
                  {reviewOrder}
                </Button>
              </div>
            </div>
            )
          </>
        )}
        {isMobile.tablet ? (
          <Footer
            className='footerCheckout'
            termsAndConditionsUrl={
              this.props.configuration.cms_configuration.global
                .termsAndConditionsUrl
            }
            shouldTermsAndConditionsOpenInNewTab={
              this.props.configuration.cms_configuration.global
                .shouldTermsAndConditionsOpenInNewTab
            }
            globalTranslation={this.props.translation.cart.global}
          />
        ) : null}
      </StyledBilling>
    );
  }
}

export const mapStateToProps = (state: RootState): IStateToProps => ({
  paymentInfo: state.checkout.payment,
  billingConfiguration:
    state.configuration.cms_configuration.modules.checkout.billingInfo,
  billingTranslation: state.translation.cart.checkout.billingInfo,
  buttonEnable: state.checkout.billing.enableProceedButton,
  billingInfo: state.checkout.billing,
  shippingInfo: state.checkout.shippingInfo,
  personalInfo: state.checkout.personalInfo,
  isMonthlyPrice: state.checkout.checkout.isMonthlyPrice,
  isUpfrontPrice: state.checkout.checkout.isUpfrontPrice,
  checkoutTranslation: state.translation.cart.checkout,
  currency: state.common.currency,
  configuration: state.configuration,
  translation: state.translation,
  mainAppShell: state.checkout.checkout.mainAppShell
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): ICompDispatchToProps => ({
  sendBillingAddress(): void {
    dispatch(billingActions.sendBillingAddress());
  },
  updateBillingAddress(data: IUpdateBillingAddress): void {
    sendProceedEvent(CHECKOUT_VIEW.BILLING.STEP, CHECKOUT_VIEW.BILLING.VIEW);
    dispatch(billingActions.updateBillingAddress(data));
  },
  enableProceedButton(enable: boolean): void {
    dispatch(billingActions.enableProceedButton(enable));
  },
  updatePersonalInfoDummyState(data: IBillingFormField): void {
    dispatch(billingActions.updatePersonalInfoDummyState(data));
  },
  updateShippingInfoDummyState(data: IBillingFormField): void {
    dispatch(billingActions.updateShippingInfoDummyState(data));
  },
  updateFormChangeField(): void {
    dispatch(billingActions.updateFormChangeField());
  },
  setBillingType(data: billingType, showAppShell: boolean): void {
    dispatch(billingActions.setBillingType({ billType: data, showAppShell }));
  },
  editPaymentStep(data: string): void {
    dispatch(billingActions.editPaymentStep(data));
  },
  setAddressBillingType(type: billingAddressType): void {
    dispatch(billingActions.setBillingAddressType(type));
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  },
  fetchCheckoutAddressLoading(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddressLoading(data));
  }
});

export default withRouter(
  connect<IStateToProps, ICompDispatchToProps, void, RootState>(
    mapStateToProps,
    mapDispatchToProps
  )(Billing)
  // tslint:disable-next-line:max-file-line-count
);
