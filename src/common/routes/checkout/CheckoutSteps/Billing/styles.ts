import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { BillingCard } from 'dt-components';

import { StyledCardInfo } from './CardInfo/styles';

export const StyledUpfrontCardInfo = styled.div``;
export const StyledMonthlyCardInfo = styled.div`
  @media (min-width: ${breakpoints.desktop}px) {
    ${StyledCardInfo} {
      margin-bottom: 4rem;
    }
  }
`;

export const StyledBilling = styled.div`
  background: ${colors.coldGray};

  .mainContent {
    width: 100%;
    padding: 3.25rem 1.25rem 1.25rem;
    width: 100%;
    .mobileNavAddressType {
      margin-top: 2.5rem;
      margin-bottom: 2.25rem;
    }

    .addressHeading {
      color: ${colors.darkGray};
    }

    .bottomCTA {
      .dt_button {
        margin-top: 1.5rem;
        width: 100%;
      }
    }

    .heading {
      .dt_divider {
        margin-top: 0.0625rem;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    flex-wrap: wrap;
    height: 100%;
    .mainContent {
      padding: 1rem 2.25rem 2.25rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    flex-wrap: nowrap;
    .mainContent {
      padding: 0rem 3rem 2.25rem;
      display: flex;
      flex-direction: column;
      .dt_progressStepper {
        padding-left: 0;
        padding-right: 0;
        margin-bottom: 2rem;
      }

      .bottomCTA {
        justify-self: flex-end;
        margin-left: auto;
        .dt_button {
          width: auto;
        }
      }

      .heading {
        .dt_divider {
          margin-top: 0.45rem;
          margin-bottom: 1.5rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .mainContent {
      padding: 0rem 2.25rem 2.25rem;
      .bottomCTA {
        .dt_button {
          height: 3rem;
        }
      }
      .heading {
        .dt_divider {
          margin-top: 0.0625rem;
          margin-bottom: 1.65rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .mainContent {
      padding: 0rem 5.5rem 2.25rem;
      .dt_progressStepper {
        margin-bottom: 4rem;
      }
    }
  }
`;

export const StyledBillingTypeCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: row;
  }
`;

export const StyledBillingCard = styled(BillingCard)`
  padding: 1rem;
  background: #e6e6e6;
  border-radius: 0.5rem;
  min-width: 14.75rem;
  max-width: 100%;
  margin-right: 0;
  margin-bottom: 1rem;
  cursor: pointer;
  padding-left: 6.875rem;
  position: relative;
  min-height: 5.625rem;
  border: 1px solid transparent;

  &.active {
    border: 1px solid #f70075;
    background: #fff;

    .title {
      color: ${colors.magenta};
    }
  }

  .title {
    margin-bottom: 1.5rem;
    font-size: 1.125rem;
    line-height: 1.11;
    letter-spacing: normal;
    font-weight: bold;
    margin-bottom: 1.5rem;
    color: ${colors.ironGray};
    position: absolute;
    left: 1rem;
    top: 1rem;
  }
  .detail {
    font-size: 0.875rem;
    line-height: 1.43;
    letter-spacing: -0.015rem;
    font-weight: bold;
    color: ${colors.ironGray};
  }
  .description {
    font-size: 0.875rem;
    line-height: 1.43;
    letter-spacing: -0.015rem;
    font-weight: 500;
    color: ${colors.mediumGray};
  }
  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 0;
    margin-right: 0.25rem;
    max-width: 14.75rem;

    display: block;
    min-height: 8.5rem;
    padding-left: 1.25rem;

    .title {
      position: static;
    }
  }
`;

export const StyledBillingCardFull = styled(BillingCard)`
  padding: 1.25rem;
  border-radius: 0.5rem;
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 0.5rem;
  &:last-child {
    margin-bottom: 1.5625rem;
  }
  .dt_title {
    display: none;
  }

  label,
  .dt_paragraph {
    font-size: 17.5px;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
  }
  .dt_paragraph {
    font-weight: 500;
    color: ${colors.mediumGray};
  }
  label {
    font-weight: bold;
    color: ${colors.ironGray};
  }
`;
