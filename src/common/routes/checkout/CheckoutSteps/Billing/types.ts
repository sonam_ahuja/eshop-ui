import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import {
  billingAddressType,
  billingType,
  IBillingFormField,
  IBillingState,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import {
  IBillingInfoTranslation,
  ICheckoutTranslation,
  ITranslationState
} from '@src/common/store/types/translation';
import { IBillingConfiguration } from '@src/common/store/types/configuration';
import { IShippingAddressState } from '@checkout/store/shipping/types';
import { IPaymentState } from '@checkout/store/payment/types';
import { IConfigurationState } from '@common/store/types/configuration';
export interface IStateToProps {
  billingConfiguration: IBillingConfiguration;
  billingTranslation: IBillingInfoTranslation;
  buttonEnable: boolean;
  billingInfo: IBillingState;
  shippingInfo: IShippingAddressState;
  personalInfo: IPersonalInfoState;
  paymentInfo: IPaymentState;
  isMonthlyPrice: boolean;
  isUpfrontPrice: boolean;
  checkoutTranslation: ICheckoutTranslation;
  currency: string;
  configuration: IConfigurationState;
  translation: ITranslationState;
  mainAppShell: boolean;
}

export interface ICompDispatchToProps {
  sendBillingAddress(): void;
  // updateInputField(data: { key: string; value: string }): void;
  enableProceedButton(enable: boolean): void;
  updateBillingAddress(data: IUpdateBillingAddress): void;
  updatePersonalInfoDummyState(data: IBillingFormField): void;
  updateShippingInfoDummyState(data: IBillingFormField): void;
  updateFormChangeField(): void;
  setBillingType(billingType: billingType, showAppShell: boolean): void;
  editPaymentStep(data: string): void;
  setAddressBillingType(type: billingAddressType): void;
  editPaymentStepClick(): void;
  fetchCheckoutAddressLoading(data: boolean): void;
}
