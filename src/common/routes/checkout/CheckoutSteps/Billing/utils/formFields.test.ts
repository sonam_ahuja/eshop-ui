import {
  getLabelValueForFormFields,
  isFormFieldValidAsPerType,
  isInputAllowed
} from '@checkout/CheckoutSteps/Billing/utils/formFields';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { IProps as IFormBodyProps } from '@checkout/CheckoutSteps/Billing/BillingForm/types';
import appState from '@store/states/app';

describe('<formField />', () => {
  test('isInputAllowed', () => {
    expect(
      isInputAllowed('number', 'type', {
        type: VALIDATION_TYPE.MAX,
        value: 'value',
        message: 'error'
      })
    ).toBeDefined();
    expect(
      isInputAllowed('number', 'e', {
        type: VALIDATION_TYPE.MAX,
        value: 'value',
        message: 'error'
      })
    ).toBeDefined();
    expect(
      isInputAllowed('number', '122413', {
        type: VALIDATION_TYPE.MAX,
        value: '2',
        message: 'error'
      })
    ).toBeDefined();
    expect(
      isInputAllowed('text', '122413', {
        type: VALIDATION_TYPE.MAX,
        value: '20',
        message: 'error'
      })
    ).toBeDefined();
  });

  test('isFormFieldValidAsPerType', () => {
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.MAX)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.MIN)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.EXACT)
    ).toBe(false);
    expect(isFormFieldValidAsPerType('1-4', '2', VALIDATION_TYPE.BETWEEN)).toBe(
      true
    );
    expect(
      isFormFieldValidAsPerType(
        'number',
        'value',
        VALIDATION_TYPE.MIN_LOWERCASE
      )
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType(
        'number',
        'value',
        VALIDATION_TYPE.MIN_UPPERCASE
      )
    ).toBe(false);

    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.MIN_DIGIT)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.REGEX)
    ).toBe(false);

    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.NONE)
    ).toBe(true);
    expect(isFormFieldValidAsPerType('number', 'value', '')).toBe(true);
  });

  test('getLabelValueForFormFields', () => {
    const stateValue = appState();
    const billingConfiguration =
      stateValue.configuration.cms_configuration.modules.checkout.billingInfo;
    billingConfiguration.form.fields.flatNumber.mandatory = false;
    const props: IFormBodyProps = {
      billingConfiguration,
      billingTranslation: stateValue.translation.cart.checkout.billingInfo,
      billingInfo: stateValue.checkout.billing,
      shippingInfo: stateValue.checkout.shippingInfo,
      personalInfo: stateValue.checkout.personalInfo,
      updateBillingAddress: jest.fn(),
      setProceedButton: jest.fn(),
      updateFormChangeField: jest.fn(),
      updatePersonalInfoDummyState: jest.fn(),
      updateShippingInfoDummyState: jest.fn()
    };
    expect(getLabelValueForFormFields(props, 'flatNumber')).toBe(
      'Flat Number (Optional)'
    );
    expect(getLabelValueForFormFields(props, 'streetAddress')).toBeDefined();
  });
});
