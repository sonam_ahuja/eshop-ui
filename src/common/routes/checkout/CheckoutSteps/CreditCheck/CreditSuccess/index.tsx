import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { ITranslationState } from '@src/common/store/types/translation';
import { IConfigurationState } from '@src/common/store/types/configuration';
import EVENT_NAME from '@events/constants/eventName';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  activeStep: CREDIT_SCORE_CODE;
  isOpen: boolean;
  creditSuccess(): void;
  closeModal(): void;
}

export class CreditSuccess extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { isOpen, creditSuccess, closeModal } = this.props;

    const { creditCheck } = this.props.translation.cart.checkout;

    return (
      <DialogBoxApp
        isOpen={isOpen}
        closeOnBackdropClick={true}
        showCloseButton={true}
        onBackdropClick={closeModal}
        closeOnEscape={true}
        onClose={closeModal}
        onEscape={closeModal}
        backgroundScroll={false}
        type='flexibleHeight'
      >
        <div className='dialogBoxHeader'>
          <Icon name='ec-solid-confirm' size='xlarge' />
        </div>
        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {creditCheck.creditSuccess}
          </Title>
        </div>
        <div className='dialogBoxFooter'>
          <Button
            size='medium'
            type='secondary'
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.CREDIT_SUCCESS}
            data-event-message={creditCheck.okGotIt}
            onClickHandler={creditSuccess}
          >
            {creditCheck.okGotIt}
          </Button>
        </div>
      </DialogBoxApp>
    );
  }
}

export default CreditSuccess;
