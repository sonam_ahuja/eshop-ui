import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ITranslationState } from '@src/common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  activeStep: CREDIT_SCORE_CODE;
  isOpen: boolean;
  reviewInfo(): void;
  closeModal(): void;
}

export class IdNotMatch extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { isOpen, reviewInfo, closeModal } = this.props;
    const { creditCheck } = this.props.translation.cart.checkout;

    return (
      <DialogBoxApp
        isOpen={isOpen}
        closeOnBackdropClick={true}
        onClose={closeModal}
        closeOnEscape={true}
        onBackdropClick={closeModal}
        onEscape={closeModal}
        backgroundScroll={false}
        type='flexibleHeight'
      >
        <div className='dialogBoxHeader'>
          <Icon name='ec-sad-face-round' size='xlarge' />
        </div>
        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {creditCheck.idNotMatch}
          </Title>
        </div>
        <div className='dialogBoxFooter'>
          <Button
            size='medium'
            type='secondary'
            onClickHandler={reviewInfo}
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.RENEW_INFO}
            data-event-message={creditCheck.idNotMatchButtonText}
          >
            {creditCheck.idNotMatchButtonText}
          </Button>
        </div>
      </DialogBoxApp>
    );
  }
}

export default IdNotMatch;
