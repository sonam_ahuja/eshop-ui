import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';

import PayFull, { IProps as IPayFullPlusOtherDeviceProps } from '.';

describe('<CreditCheck PayFull />', () => {
  const props: IPayFullPlusOtherDeviceProps = {
    configuration: configurationState(),
    translation: translationState(),
    activeStep: CREDIT_SCORE_CODE.CREDIT_SUCCESS,
    isOpen: false,
    payFullPrice: jest.fn(),
    closeModal: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PayFull {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
