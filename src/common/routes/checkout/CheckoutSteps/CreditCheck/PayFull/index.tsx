import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { ITranslationState } from '@src/common/store/types/translation';
import { IConfigurationState } from '@src/common/store/types/configuration';
import EVENT_NAME from '@events/constants/eventName';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  activeStep: CREDIT_SCORE_CODE;
  isOpen: boolean;
  payFullPrice(): void;
  closeModal(): void;
}

export class PayFull extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { isOpen, payFullPrice, closeModal } = this.props;

    const { creditCheck } = this.props.translation.cart.checkout;

    return (
      <DialogBoxApp
        isOpen={isOpen}
        closeOnBackdropClick={true}
        onClose={closeModal}
        onBackdropClick={closeModal}
        closeOnEscape={true}
        onEscape={closeModal}
        backgroundScroll={false}
        type='flexibleHeight'
      >
        <div className='dialogBoxHeader'>
          <Icon name='ec-sad-face-round' size='xlarge' />
        </div>
        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {creditCheck.payFull}
          </Title>
        </div>
        <div className='dialogBoxFooter'>
          <Button
            size='medium'
            type='primary'
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.PAY_FULL_PRICE}
            data-event-message={creditCheck.payFullButtonText}
            onClickHandler={payFullPrice}
          >
            {creditCheck.payFullButtonText}
          </Button>
        </div>
      </DialogBoxApp>
    );
  }
}

export default PayFull;
