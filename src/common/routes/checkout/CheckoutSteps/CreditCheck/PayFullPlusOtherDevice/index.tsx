import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ITranslationState } from '@src/common/store/types/translation';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import EVENT_NAME from '@events/constants/eventName';

import { StyledButtonsWrap } from './styles';
export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  activeStep: CREDIT_SCORE_CODE;
  isOpen: boolean;
  payFullPrice(): void;
  selectOtherDevice(): void;
  closeModal(): void;
}

export class PayFullPlusOtherDevice extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { isOpen, selectOtherDevice, payFullPrice, closeModal } = this.props;

    const { creditCheck } = this.props.translation.cart.checkout;

    return (
      <DialogBoxApp
        isOpen={isOpen}
        closeOnBackdropClick={true}
        onClose={closeModal}
        onBackdropClick={closeModal}
        closeOnEscape={true}
        onEscape={closeModal}
        backgroundScroll={false}
        type='flexibleHeight'
      >
        <div className='dialogBoxHeader'>
          <Icon name='ec-sad-face-round' size='xlarge' />
        </div>
        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {creditCheck.payFullAndDifferentDevice}
          </Title>
        </div>
        <div className='dialogBoxFooter'>
          <StyledButtonsWrap>
            <Button
              size='medium'
              type='primary'
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.PAY_FULL_PRICE}
              data-event-message={creditCheck.payFullButtonText}
              onClickHandler={payFullPrice}
            >
              {creditCheck.payFullButtonText}
            </Button>
            <Button
              size='medium'
              type='secondary'
              onClickHandler={selectOtherDevice}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.SELECT_DIFF_DEVICE}
              data-event-message={creditCheck.selectDifferentDeviceText}
              data-event-path={
                'cart.checkout.creditCheck.selectDifferentDeviceText'
              }
            >
              {creditCheck.selectDifferentDeviceText}
            </Button>
          </StyledButtonsWrap>
        </div>
      </DialogBoxApp>
    );
  }
}

export default PayFullPlusOtherDevice;
