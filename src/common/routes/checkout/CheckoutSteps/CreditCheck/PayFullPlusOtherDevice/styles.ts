import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledButtonsWrap = styled.div`
  .dt_button:last-child {
    margin-top: 0.75rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .dt_button:last-child {
      margin-top: 0;
      margin-left: 1rem;
    }
  }
`;
