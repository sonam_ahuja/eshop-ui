import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import {
  IProps as IRequestSupportCallProps,
  RequestSupportCall
} from '@checkout/CheckoutSteps/CreditCheck/RequestSupportCall';

const event = {
  preventDefault: jest.fn(),
  stopPropagation: jest.fn(),
  persist: jest.fn(),
  target: { value: '' }
  // tslint:disable-next-line:no-any
} as any;

describe('<CreditCheck RequestSupportCall />', () => {
  const props: IRequestSupportCallProps = {
    configuration: configurationState(),
    translation: translationState(),
    activeStep: CREDIT_SCORE_CODE.CREDIT_SUCCESS,
    isOpen: false,
    requestSupportCall: jest.fn(),
    closeModal: jest.fn()
  };
  const componentWrapper = (propsPrams: IRequestSupportCallProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <RequestSupportCall {...propsPrams} />
      </ThemeProvider>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('function test', () => {
    const newProps: IRequestSupportCallProps = {
      ...props
    };
    const component = componentWrapper(newProps);
    (component
      .find('RequestSupportCall')
      .instance() as RequestSupportCall).onClickFunction(event);
    (component
      .find('RequestSupportCall')
      .instance() as RequestSupportCall).handleChange(event);
    event.target.value = 'abc';
    (component
      .find('RequestSupportCall')
      .instance() as RequestSupportCall).handleChange(event);
    (component
      .find('RequestSupportCall')
      .instance() as RequestSupportCall).handleClose();
    (component
      .find('RequestSupportCall')
      .instance() as RequestSupportCall).resetState();
    (component
      .find('RequestSupportCall')
      .instance() as RequestSupportCall).componentWillUnmount();
  });
});
