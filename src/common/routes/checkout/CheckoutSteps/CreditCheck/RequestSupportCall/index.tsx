import React, { Component, ReactNode } from 'react';
import { Button, FloatLabelInput, Icon, Title } from 'dt-components';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ITranslationState } from '@src/common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';

import { StyledFooter } from './styles';
export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  activeStep: CREDIT_SCORE_CODE;
  isOpen: boolean;
  requestSupportCall(data: string): void;
  closeModal(): void;
}

interface IState {
  phoneNumber: string;
  disableButton: boolean;
}

export class RequestSupportCall extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      phoneNumber: '',
      disableButton: true
    };
    this.onClickFunction = this.onClickFunction.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount(): void {
    this.resetState();
  }

  componentWillUnmount(): void {
    this.resetState();
  }

  handleClose = () => {
    this.resetState();
    this.props.closeModal();
  }

  resetState = () => {
    this.setState({
      phoneNumber: '',
      disableButton: true
    });
  }

  onClickFunction(_event: React.SyntheticEvent): void {
    this.props.requestSupportCall(this.state.phoneNumber);
    this.resetState();
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputValue = (event.target as HTMLInputElement).value;

    if (inputValue !== '') {
      this.setState({
        phoneNumber: inputValue,
        disableButton: false
      });
    } else {
      this.setState({
        phoneNumber: inputValue,
        disableButton: true
      });
    }
  }

  render(): ReactNode {
    const { isOpen } = this.props;
    const { creditCheck } = this.props.translation.cart.checkout;

    return (
      <DialogBoxApp
        isOpen={isOpen}
        closeOnBackdropClick={true}
        onClose={this.handleClose}
        onBackdropClick={this.handleClose}
        closeOnEscape={true}
        onEscape={this.handleClose}
        backgroundScroll={false}
        type='fullHeight'
      >
        <div className='dialogBoxHeader'>
          <Icon name='ec-solid-call' size='xlarge' />
        </div>
        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {creditCheck.requestSupportCall}
          </Title>
        </div>
        <StyledFooter className='dialogBoxFooter'>
          <FloatLabelInput
            label='Phone Number'
            type='number'
            value={this.state.phoneNumber}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              this.handleChange(event);
            }}
          />
          <Button
            size='medium'
            type='secondary'
            disabled={this.state.disableButton}
            onClickHandler={(event: React.SyntheticEvent) =>
              this.onClickFunction(event)
            }
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.REQUEST_SUPPORT_CALL}
            data-event-message={creditCheck.requestSupportCallButtonText}
          >
            {creditCheck.requestSupportCallButtonText}
          </Button>
        </StyledFooter>
      </DialogBoxApp>
    );
  }
}

export default RequestSupportCall;
