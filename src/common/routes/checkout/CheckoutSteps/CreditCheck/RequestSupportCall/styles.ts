import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledFooter = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-between;

  margin-top: -0.65rem;

  .dt_button {
    margin-top: 1.5rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: row;

    margin-bottom: -1rem;

    .dt_floatLabelInput {
      margin-top: 0.45rem;
      width: 12.4rem;
    }
    .dt_button {
      margin-top: 0;
      margin-left: 2rem;
      width: 11.6rem;
      white-space: nowrap;
    }
  }
`;
