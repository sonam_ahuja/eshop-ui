import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';

import SelectOtherDevice, { IProps as ISelectOtherDeviceProps } from '.';

describe('<CreditCheck SelectOtherDevice />', () => {
  const props: ISelectOtherDeviceProps = {
    configuration: configurationState(),
    translation: translationState(),
    activeStep: CREDIT_SCORE_CODE.CREDIT_SUCCESS,
    isOpen: false,
    selectOtherDevice: jest.fn(),
    closeModal: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SelectOtherDevice {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
