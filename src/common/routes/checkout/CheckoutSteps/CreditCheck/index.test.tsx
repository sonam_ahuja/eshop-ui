import { mount } from 'enzyme';
import { RootState } from '@common/store/reducers';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import IState from '@basket/store/state';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import configureStore from 'redux-mock-store';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import {
  CreditCheckModals,
  IComponentProps as ICreditCheckModalsProps,
  mapDispatchToProps,
  mapStateToProps,
  mergeProps
} from '.';

const mockStore = configureStore();

describe('<CreditCheckModals />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;

  const initStateValue: RootState = appState();

  const componentProps: ICreditCheckModalsProps = {
    selectOtherDevice: jest.fn(),
    requestSupportCall: jest.fn(),
    payFullPrice: jest.fn(),
    reviewInfo: jest.fn(),
    creditSuccess: jest.fn(),
    configuration: configurationState(),
    translation: translationState(),
    closeModal: jest.fn(),
    isOpen: false,
    activeStep: CREDIT_SCORE_CODE.CREDIT_SUCCESS
  };
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly with diffrent cart type is ID_NOT_MATCH', () => {
    const copyProps = { ...componentProps };
    copyProps.activeStep = CREDIT_SCORE_CODE.ID_NOT_MATCH;
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...copyProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wapper
      .find('CreditCheckModals')
      .instance() as CreditCheckModals).renderModals();
  });

  test('should render properly with diffrent cart type is PAY_FULL', () => {
    const copyProps = { ...componentProps };
    copyProps.activeStep = CREDIT_SCORE_CODE.PAY_FULL;
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...copyProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wapper
      .find('CreditCheckModals')
      .instance() as CreditCheckModals).renderModals();
  });

  test('should render properly with diffrent cart type is PAY_FULL_PLUS_OTHER_DEVICE', () => {
    const copyProps = { ...componentProps };
    copyProps.activeStep = CREDIT_SCORE_CODE.PAY_FULL_PLUS_OTHER_DEVICE;
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...copyProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wapper
      .find('CreditCheckModals')
      .instance() as CreditCheckModals).renderModals();
  });

  test('should render properly with diffrent cart type is REQUEST_SUPPORT_CALL', () => {
    const copyProps = { ...componentProps };
    copyProps.activeStep = CREDIT_SCORE_CODE.REQUEST_SUPPORT_CALL;
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...copyProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wapper
      .find('CreditCheckModals')
      .instance() as CreditCheckModals).renderModals();
  });

  test('should render properly with diffrent cart type is SELECT_OTHER_DEVICE', () => {
    const copyProps = { ...componentProps };
    copyProps.activeStep = CREDIT_SCORE_CODE.SELECT_OTHER_DEVICE;
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...copyProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wapper
      .find('CreditCheckModals')
      .instance() as CreditCheckModals).renderModals();
  });

  test('should render properly with diffrent cart type is NONE', () => {
    const copyProps = { ...componentProps };
    delete copyProps.activeStep;
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CreditCheckModals {...copyProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wapper
      .find('CreditCheckModals')
      .instance() as CreditCheckModals).renderModals();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mergeProps test', () => {
    const rootState = {
      activeStep: CREDIT_SCORE_CODE.CREDIT_SUCCESS,
      configuration: configurationState(),
      translation: translationState(),
      isOpen: true
    };
    expect(mergeProps(rootState)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).selectOtherDevice();
    mapDispatchToProps(dispatch).payFullPrice();
    mapDispatchToProps(dispatch).reviewInfo();
    mapDispatchToProps(dispatch).creditSuccess();
    mapDispatchToProps(dispatch).requestSupportCall('support');
    mapDispatchToProps(dispatch).closeModal();
  });
});
