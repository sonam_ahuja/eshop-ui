import React, { Component, ReactNode } from 'react';
import { connect } from 'react-redux';
import { RootState } from '@common/store/reducers';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { Dispatch } from 'redux';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ITranslationState } from '@src/common/store/types/translation';
import creditCheckActions from '@checkout/store/creditCheck/actions';

import RequestSupportCall from './RequestSupportCall';
import IdNotMatch from './IdNotMatch';
import PayFull from './PayFull';
import SelectOtherDevice from './SelectOtherDevice';
import PayFullPlusOtherDevice from './PayFullPlusOtherDevice';
import CreditSuccess from './CreditSuccess';

export interface IStateToProps {
  activeStep: CREDIT_SCORE_CODE;
  configuration: IConfigurationState;
  translation: ITranslationState;
  isOpen: boolean;
}

export interface ICompDispatchToProps {
  selectOtherDevice(): void;
  requestSupportCall(data: string): void;
  payFullPrice(): void;
  reviewInfo(): void;
  creditSuccess(): void;
  closeModal(): void;
}

export type IComponentProps = IStateToProps & ICompDispatchToProps;

export class CreditCheckModals extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
  }

  renderModals = () => {
    const {
      activeStep,
      selectOtherDevice,
      requestSupportCall,
      payFullPrice,
      reviewInfo,
      creditSuccess,
      configuration,
      translation,
      isOpen,
      closeModal
    } = this.props;

    switch (activeStep) {
      case CREDIT_SCORE_CODE.CREDIT_SUCCESS:
        return (
          <CreditSuccess
            configuration={configuration}
            translation={translation}
            activeStep={activeStep}
            creditSuccess={creditSuccess}
            isOpen={isOpen}
            closeModal={closeModal}
          />
        );

      case CREDIT_SCORE_CODE.REQUEST_SUPPORT_CALL:
        return (
          <RequestSupportCall
            configuration={configuration}
            translation={translation}
            activeStep={activeStep}
            requestSupportCall={requestSupportCall}
            isOpen={isOpen}
            closeModal={closeModal}
          />
        );

      case CREDIT_SCORE_CODE.ID_NOT_MATCH:
        return (
          <IdNotMatch
            configuration={configuration}
            translation={translation}
            activeStep={activeStep}
            reviewInfo={reviewInfo}
            isOpen={isOpen}
            closeModal={closeModal}
          />
        );

      case CREDIT_SCORE_CODE.PAY_FULL:
        return (
          <PayFull
            configuration={configuration}
            translation={translation}
            activeStep={activeStep}
            payFullPrice={payFullPrice}
            isOpen={isOpen}
            closeModal={closeModal}
          />
        );

      case CREDIT_SCORE_CODE.SELECT_OTHER_DEVICE:
        return (
          <SelectOtherDevice
            configuration={configuration}
            translation={translation}
            activeStep={activeStep}
            selectOtherDevice={selectOtherDevice}
            isOpen={isOpen}
            closeModal={closeModal}
          />
        );

      case CREDIT_SCORE_CODE.PAY_FULL_PLUS_OTHER_DEVICE:
        return (
          <PayFullPlusOtherDevice
            configuration={configuration}
            translation={translation}
            activeStep={activeStep}
            selectOtherDevice={selectOtherDevice}
            payFullPrice={payFullPrice}
            isOpen={isOpen}
            closeModal={closeModal}
          />
        );

      default:
        return null;
    }
  }

  render(): ReactNode {
    return <>{this.renderModals()}</>;
  }
}

export const mapStateToProps = (state: RootState): IStateToProps => ({
  activeStep: state.checkout.checkout.activeCreditStep,
  configuration: state.configuration,
  translation: state.translation,
  isOpen: state.checkout.creditCheck.isOpen
});

export const mergeProps = (state: IStateToProps) => {
  return state;
};
export const mapDispatchToProps = (
  dispatch: Dispatch
): ICompDispatchToProps => ({
  selectOtherDevice(): void {
    dispatch(creditCheckActions.selectOtherDevice());
  },
  requestSupportCall(data: string): void {
    dispatch(creditCheckActions.requestSupportCall(data));
  },
  payFullPrice(): void {
    dispatch(creditCheckActions.payFullPrice());
  },
  reviewInfo(): void {
    dispatch(creditCheckActions.reviewInfo());
  },
  creditSuccess(): void {
    dispatch(creditCheckActions.creditSuccess());
  },
  closeModal(): void {
    dispatch(creditCheckActions.closeModal());
  }
});

export default connect<IStateToProps, ICompDispatchToProps, void, RootState>(
  mapStateToProps,
  mapDispatchToProps
)(CreditCheckModals);
