import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import EnterNumber from '@checkout/CheckoutSteps/NumberPorting/EnterNumber';
import { IProps } from '@checkout/CheckoutSteps/NumberPorting/index';

describe('<EnterNumber />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      numberPorting: appState().checkout.numberPorting,
      phoneNumber: appState().checkout.numberPorting.phoneNumber,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn(),
      setNumberPortingStatus: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterNumber {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('event simulate', () => {
    const newProps = { ...props };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterNumber {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('EnterNumber').instance() as EnterNumber).onValidateNumber(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        // tslint:disable-next-line: no-empty
        preventDefault: () => {}
      } as React.SyntheticEvent
    );
    (component.find('EnterNumber').instance() as EnterNumber).closeModal();
  });
  test('event simulate for without otp ', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mnp.verificationMethods.MSISDNOTP = false;
    newProps.checkoutConfiguration.mns.show = false;
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterNumber {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('EnterNumber').instance() as EnterNumber).onValidateNumber(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        // tslint:disable-next-line: no-empty
        preventDefault: () => {}
      } as React.SyntheticEvent
    );
  });
});
