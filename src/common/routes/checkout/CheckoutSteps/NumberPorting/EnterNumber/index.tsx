import React from 'react';
import { Button, FloatLabelInput, Section, Title } from 'dt-components';
import cx from 'classnames';
import { IProps } from '@checkout/CheckoutSteps/NumberPorting/index';
import {
  charEPresent,
  ignoreSpaces,
  isFormFieldValidAsPerType,
  isInputAllowed
} from '@checkout/CheckoutSteps/PersonalInfo/utils';
import EVENT_NAME from '@events/constants/eventName';

import PortingHeader from '../PortingHeader';
import checkHeaderSwitch from '../utils/checkHeaderSwitch';
import getFlowType from '../utils/getFlowType';
import { otpRequestDataConverter } from '../utils/otpRequestValidationApiConverter';

import { StyledEnterNumber } from './styles';

export interface IState {
  value: string;
  isValid: boolean;
  validationMessage: string;
}
class EnterNumber extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      value: props.phoneNumber || '',
      isValid: true,
      validationMessage: this.props.checkoutConfiguration.form.fields
        .phoneNumber.validation.message
    };

    this.onValidateNumber = this.onValidateNumber.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  handleKeydown(event: React.KeyboardEvent<HTMLInputElement>): void {
    if (ignoreSpaces(event)) {
      event.preventDefault();
    }
    if (charEPresent(event)) {
      event.preventDefault();

      return;
    }
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputValue = (event.target as HTMLInputElement).value;
    const {
      fields: cmsFormConfiguration
    } = this.props.checkoutConfiguration.form;

    const specificConfiguration = cmsFormConfiguration.phoneNumber;
    if (
      !isInputAllowed(
        specificConfiguration.inputType,
        inputValue,
        specificConfiguration.validation
      )
    ) {
      return;
    }

    const isValidValue = isFormFieldValidAsPerType(
      specificConfiguration.validation.value,
      inputValue,
      specificConfiguration.validation.type
    );

    this.setState({ value: inputValue, isValid: isValidValue });
  }

  onValidateNumber(event: React.SyntheticEvent): void {
    event.preventDefault();

    const { setPhoneNumber, checkoutConfiguration } = this.props;
    setPhoneNumber(this.state.value);

    if (checkoutConfiguration.mnp.verificationMethods.MSISDNOTP) {
      const data = otpRequestDataConverter(this.state.value);
      this.props.requestOtp(data);
    } else {
      this.props.proceedToNextScreen();
    }
  }

  closeModal(): void {
    this.props.showNumberPortModal(false);
    this.props.setFlowType(getFlowType(this.props.checkoutConfiguration));
  }

  render(): React.ReactNode {
    const { checkoutConfiguration, checkoutTranslation } = this.props;
    const { mnp } = checkoutTranslation;

    const isPortingHeader = checkHeaderSwitch(this.props.checkoutConfiguration);

    const headerEl = isPortingHeader ? (
      <PortingHeader {...this.props} />
    ) : (
      <Title className='numberPortingHeader' size='small' weight='ultra'>
        {mnp.headingText}
      </Title>
    );
    const buttonDisable = !this.state.value.trim() || !this.state.isValid;

    const classNames = cx('numberPortingEnterNumber', {
      tabHeaderContainer: isPortingHeader
    });

    return (
      <StyledEnterNumber className={classNames}>
        <div className='numberPortingInner'>
          {headerEl}
          <div className='numberPortingBody'>
            <FloatLabelInput
              autoComplete='off'
              id={'phoneNumber'}
              label={mnp.phoneNumberLabelText}
              type={checkoutConfiguration.form.fields.phoneNumber.inputType}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                this.handleChange(event);
              }}
              onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
                this.handleKeydown(event);
              }}
              value={this.state.value}
              error={!this.state.isValid}
              errorMessage={this.state.validationMessage}
            />
            <Section className='noteText' size='small'>
              {this.props.checkoutConfiguration.mnp.verificationMethods
                .MSISDNOTP && mnp.message}
            </Section>
          </div>

          <div className='numberPortingFooter'>
            <Button
              type='complementary'
              disabled={buttonDisable}
              onClickHandler={this.onValidateNumber}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.MNP_VALIDATE_NUMBER}
              data-event-message={mnp.validateNumberText}
            >
              {mnp.validateNumberText}
            </Button>
          </div>
        </div>
      </StyledEnterNumber>
    );
  }
}

export default EnterNumber;
