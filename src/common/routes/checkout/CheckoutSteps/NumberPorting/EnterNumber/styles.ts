import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledEnterNumber = styled.div`
  height: 100%;
  min-height: 100%;

  .numberPortingHeader {
    color: ${colors.mediumGray};
  }

  .numberPortingFooter {
    justify-self: flex-end;
    margin-top: auto;
  }

  &.tabHeaderContainer {
    .numberPortingInner {
      background: ${colors.white};
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .numberPortingHeader {
      margin-bottom: 5.7rem !important;
    }

    .noteText {
      margin-top: 1rem;
      padding-right: 4rem;
      font-size: 0.75rem;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.33;
      letter-spacing: normal;
      color: ${colors.ironGray};
    }
  }
`;
