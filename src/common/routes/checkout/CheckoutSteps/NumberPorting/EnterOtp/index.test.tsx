import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';

import { IProps } from '../index';

import EnterOtp from '.';

// tslint:disable:no-big-function
describe('<Number Porting />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      numberPorting: appState().checkout.numberPorting,
      phoneNumber: appState().checkout.numberPorting.phoneNumber,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn(),
      setNumberPortingStatus: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterOtp {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly with key code 32', () => {
    const newProps = { ...props };
    const inputEvent = { currentTarget: { value: ' ' }, keyCode: 32 };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterOtp {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    const input = component.find('input');
    input.at(0).simulate('keydown', inputEvent);
  });

  test('should render properly masking value empty string', () => {
    const newInitialValue = { ...initStateValue };
    newInitialValue.configuration.cms_configuration.global.authentication.masking.phone =
      '';
    const newStore = mockStore(newInitialValue);

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={newStore}>
            <EnterOtp {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
  test('should update state properly', () => {
    const newProps = { ...props };
    const inputEvent = { currentTarget: { value: ' ' }, keyCode: 32 };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterOtp {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    const input = component.find('input');
    input.at(0).simulate('keydown', inputEvent);
    component.instance().setState({ otp: '1234' });
    expect(component).toMatchSnapshot();
    component.find('button').simulate('click');
    (component.find('EnterOtp').instance() as EnterOtp).validateOtp(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        // tslint:disable-next-line: no-empty
        preventDefault: () => {}
      } as React.SyntheticEvent
    );
    (component.find('EnterOtp').instance() as EnterOtp).onCloseModal();
    (component.find('EnterOtp').instance() as EnterOtp).onBackClick();
    (component.find('EnterOtp').instance() as EnterOtp).requestOtp();
  });
});
