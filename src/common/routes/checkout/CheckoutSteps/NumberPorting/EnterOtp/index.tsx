import React from 'react';
import { Anchor, Button, OtpInput, Title } from 'dt-components';
import { IProps } from '@checkout/CheckoutSteps/NumberPorting/index';
import CONSTANTS from '@common/constants/appConstants';
import { NUMBER_PORTING_STEPS } from '@checkout/store/numberPorting/enum';
import EVENT_NAME from '@events/constants/eventName';
import appConstants from '@src/common/constants/appConstants';

import { getFlowType, getMaskingNumber } from '../utils';
import {
  otpRequestDataConverter,
  otpValidateDataConverter
} from '../utils/otpRequestValidationApiConverter';
import { BackIcon } from '../styles';

import { StyledEnterOtp } from './styles';

export interface IState {
  otp: string;
}

class EnterOtp extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      otp: ''
    };

    this.validateOtp = this.validateOtp.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onBackClick = this.onBackClick.bind(this);
    this.requestOtp = this.requestOtp.bind(this);
  }

  validateOtp(event: React.SyntheticEvent): void {
    event.preventDefault();
    const payload = otpValidateDataConverter(
      this.props.numberPorting,
      this.state.otp
    );
    this.props.validateOtp(payload);
    // @ TODO: otp enryption pending from backend
    // otpValidateDataConverter(this.props.numberPorting, this.state.otp).then(
    //   payload => this.props.validateOtp(payload)
    // );
  }

  onCloseModal(): void {
    this.props.showNumberPortModal(false);
    this.props.setFlowType(getFlowType(this.props.checkoutConfiguration));
  }

  onBackClick(): void {
    this.props.setActiveStep(NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER);
  }

  requestOtp(): void {
    const { requestOtp, numberPorting, setActiveStep } = this.props;
    setActiveStep(NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER);

    const data = otpRequestDataConverter(numberPorting.phoneNumber);
    requestOtp(data);
  }

  render(): React.ReactNode {
    const { numberPorting } = this.props;
    const { mnp } = this.props.checkoutTranslation;
    const disbaleButton =
      CONSTANTS.NUMBER_OF_OTP_INPUTS !== this.state.otp.length;

    return (
      <StyledEnterOtp className='numberPortingEnterOtp'>
        <BackIcon onClick={this.onBackClick} />

        <div className='numberPortingInner'>
          <div className='numberPortingHeader'>
            <Title size='small' weight='ultra'>
              {mnp.otpHeadingText.replace(
                '{0}',
                getMaskingNumber(numberPorting.phoneNumber)
              )}
            </Title>
            <Anchor
              hreflang={appConstants.LANGUAGE_CODE}
              title={mnp.sendAgainText}
              className='sendAgain'
              onClickHandler={this.requestOtp}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.MNP_PORT_SEND_AGAIN}
              data-event-message={mnp.sendAgainText}
            >
              {mnp.sendAgainText}
            </Anchor>
          </div>
          <div className='numberPortingBody'>
            <OtpInput
              numInputs={CONSTANTS.NUMBER_OF_OTP_INPUTS}
              getOtpValue={otp => {
                this.setState({ otp });
              }}
              shouldAutoFocus={true}
            />
          </div>

          <div className='numberPortingFooter'>
            <Button
              type='complementary'
              disabled={disbaleButton}
              onClickHandler={this.validateOtp}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.MNP_VALIDATE_NUMBER}
              data-event-message={mnp.validateNumberText}
              loading={numberPorting.loading}
            >
              {mnp.nextText}
            </Button>
          </div>
        </div>
      </StyledEnterOtp>
    );
  }
}

export default EnterOtp;
