import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledEnterOtp = styled.div`
  height: 100%;
  min-height: 100%;

  .numberPortingHeader {
    color: ${colors.mediumGray};

    .dt_title {
      margin-bottom: 0.25rem;
    }

    .sendAgain {
      font-size: 0.625rem;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.2;
      letter-spacing: normal;
    }
  }

  .numberPortingFooter {
    justify-self: flex-end;
    margin-top: auto;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .numberPortingInner {
      .numberPortingHeader {
        margin-bottom: 3.25rem;
      }
    }
  }
`;
