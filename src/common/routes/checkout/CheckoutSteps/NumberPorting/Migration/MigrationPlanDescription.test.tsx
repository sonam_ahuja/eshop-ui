import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { MIGRATION_TYPE } from '@checkout/store/numberPorting/enum';
import MigrationPlanDescription, {
  IProps
} from '@checkout/CheckoutSteps/NumberPorting/Migration/MigrationPlanDescription';
import { getConvertedDate } from '@checkout/CheckoutSteps/utils';

describe('<MigrationPlanDescription />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      translation: appState().translation.cart.checkout,
      maxDays: '20',
      migrationMessage: 'string',
      date: '30/08/2016',
      headerTitle: 'title',
      prevMigrationType: MIGRATION_TYPE.DESIRED_DATE,
      migrationType: MIGRATION_TYPE.END_OF_PROMOTIOM,
      setMigrationType: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MigrationPlanDescription {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when date list is coming from HAL API', () => {
    const stateValue = { ...initStateValue };
    const convertedDate = getConvertedDate([
      1565252831,
      1565262831,
      1565272831,
      1565282831,
      1565292831,
      1565352831
    ]);
    stateValue.checkout.numberPorting.mnpEligibleDates = convertedDate;
    stateValue.configuration.cms_configuration.modules.checkout.mnp.showDateList = true;
    stateValue.checkout.numberPorting.migrationType =
      MIGRATION_TYPE.DESIRED_DATE;
    const newStore = mockStore(stateValue);
    const newProps = {
      ...props,
      showDateList: true,
      mnpEligibleDates: convertedDate
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={newStore}>
            <MigrationPlanDescription {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).headerEL(' port on = ');
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MigrationPlanDescription {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.WITHDRAWAL
    );

    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.DESIRED_DATE
    );

    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.EMPTY
    );
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.END_OF_PROMOTIOM
    );

    (component
      .find('MigrationPlanDescription')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as MigrationPlanDescription).handleChange({
      target: { value: 'value' }
    } as React.ChangeEvent<HTMLInputElement>);
    (component
      .find('MigrationPlanDescription')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as MigrationPlanDescription).handleDefault({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      target: { value: 'value' }
    } as React.ChangeEvent<HTMLInputElement>);
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).handleItemSelect(
      '09 August 1993'
    );
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).renderCalander();
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).renderDateList();
  });
});
