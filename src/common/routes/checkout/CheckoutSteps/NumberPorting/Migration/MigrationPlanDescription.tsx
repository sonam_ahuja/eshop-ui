import React, { ReactNode } from 'react';
import {
  Accordion,
  FloatLabelInput,
  Icon,
  Paragraph,
  Section,
  Select
} from 'dt-components';
import SimpleSelect from '@src/common/components/Select/SimpleSelect';
import { MIGRATION_TYPE } from '@checkout/store/numberPorting/enum';
import { ISetMigrationType } from '@checkout/store/numberPorting/types';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import moment from 'moment';
import { isMobile } from '@src/common/utils';
import styled from 'styled-components';
import { IDateListItem } from '@checkout/store/shipping/types';

const StyledCustomDropDown = styled.div`
  display: flex;
  white-space: nowrap;

  .customSelect {
    margin-left: 5px;
    align-self: flex-start;
    display: flex;
    align-items: center;
    position: relative;

    .dt_icon {
      margin-left: 5px;
      font-size: 0.6rem;
      display: none;
      line-height: 0;
    }

    .dt_floatLabelInput {
      display: none;
      position: absolute;
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      max-height: 100%;
      max-width: 100%;
      padding: 0;

      .dt_icon {
        display: none !important;
      }

      input {
        min-height: 0;
        font-size: 0;
        border: none;
        padding: 0;
        cursor: pointer;
      }
    }

    .dt_select {
      display: none;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      z-index: 2;

      .dt_outsideClick {
        display: block;
        width: 100%;
        height: 100%;
      }
      .styledSimpleSelect {
        width: 100%;
        height: 100%;
        opacity: 0.02;
      }
    }
  }
`;

export interface IProps {
  date?: string;
  headerTitle: string;
  migrationType: MIGRATION_TYPE;
  prevMigrationType: MIGRATION_TYPE;
  migrationMessage: string;
  translation: ICheckoutTranslation;
  maxDays?: string;
  mnpEligibleDates?: IDateListItem[];
  disableWeekends?: boolean;
  showDateList?: boolean;
  setMigrationType(data: ISetMigrationType): void;
}
export interface IState {
  date: string;
}

class MigrationPlanDescription extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      date: props.date ? props.date : ''
    };
  }

  onMigrationPlanClickHandler(
    migrationType: MIGRATION_TYPE,
    value?: string
  ): void {
    this.props.setMigrationType({ migrationType, value });
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const title = event.target.value;
    this.setState({ date: title }, () => {
      this.onMigrationPlanClickHandler(MIGRATION_TYPE.DESIRED_DATE, title);
    });
  }

  handleItemSelect = (value: string) => {
    this.setState({ date: value }, () => {
      this.onMigrationPlanClickHandler(MIGRATION_TYPE.DESIRED_DATE, value);
    });
  }

  handleDefault = (event: React.ChangeEvent<HTMLInputElement>): void => {
    event.preventDefault();
  }

  renderCalander = () => (
    <FloatLabelInput
      type={'date'}
      className={'mnpDatePicker'}
      placeholder={this.props.translation.mnp.desiredDate}
      onChange={this.handleChange}
      dateFormat={'DD MMMM YYYY'}
      value={this.state.date}
      min={moment().format('YYYY-MM-DD')}
      max={moment()
        .add(this.props.maxDays, 'days')
        .format('YYYY-MM-DD')}
      isMobile={isMobile.phone}
      disableWeekends={!!this.props.disableWeekends}
    />
  )

  renderDateList = () => {
    const { mnpEligibleDates } = this.props;

    return (
      <Select
        useNativeDropdown={isMobile.phone}
        items={mnpEligibleDates as IDateListItem[]}
        selectedItem={{ id: this.state.date, title: this.state.date }}
        onItemSelect={item => this.handleItemSelect(item.title)}
        SelectedItemComponent={selectedItemProps => (
          <>
            <SimpleSelect
              {...selectedItemProps}
              isOpen
              selectedItem={{
                id:
                  selectedItemProps && selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.id
                    : '',
                title:
                  selectedItemProps && selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.title
                    : ''
              }}
            />
          </>
        )}
      />
    );
  }

  headerEL = (_title: ReactNode) => {
    const { translation, mnpEligibleDates, showDateList } = this.props;
    const {
      portOn: portOnText,
      desiredDate: desiredDateText
    } = translation.mnp;
    const displayDateList =
      showDateList && mnpEligibleDates && mnpEligibleDates.length > 0;

    return (
      <Paragraph className='textWithDropDown' size='small' weight='bold'>
        <StyledCustomDropDown className='text'>
          {portOnText}
          <div className='customSelect'>
            {' '}
            {this.state.date ? this.state.date : desiredDateText}
            <Icon size='inherit' name='ec-triangle-down' />
            {displayDateList ? this.renderDateList() : this.renderCalander()}
          </div>
        </StyledCustomDropDown>
      </Paragraph>
    );
  }

  render(): React.ReactNode {
    const {
      headerTitle,
      migrationType,
      migrationMessage,
      prevMigrationType
    } = this.props;

    const isOpen = prevMigrationType === migrationType;
    const classNames = `accordionSelector ${isOpen ? 'active' : ''}`;

    if (migrationType === MIGRATION_TYPE.DESIRED_DATE) {
      return (
        <Accordion
          style={{ cursor: 'pointer' }}
          className={classNames}
          isOpen={isOpen}
          headerTitle={headerTitle}
          Header={props => this.headerEL(props.headerTitle)}
          onClick={() =>
            this.onMigrationPlanClickHandler(migrationType, this.state.date)
          }
        >
          <Section className='accordionContent' size='small'>
            {isOpen ? migrationMessage : ''}
          </Section>
        </Accordion>
      );
    }

    return (
      <Accordion
        style={{ cursor: 'pointer' }}
        className={classNames}
        isOpen={isOpen}
        headerTitle={headerTitle}
        Header={props => (
          <Paragraph size='small' weight='bold'>
            {props.headerTitle}
          </Paragraph>
        )}
        onClick={() => this.onMigrationPlanClickHandler(migrationType)}
      >
        <Section className='accordionContent' size='small'>
          {isOpen ? migrationMessage : ''}
        </Section>
      </Accordion>
    );
  }
}

export default MigrationPlanDescription;
