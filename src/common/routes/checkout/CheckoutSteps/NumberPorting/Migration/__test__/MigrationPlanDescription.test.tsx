import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { MIGRATION_TYPE } from '@checkout/store/numberPorting/enum';
import MigrationPlanDescription, {
  IProps
} from '@checkout/CheckoutSteps/NumberPorting/Migration/MigrationPlanDescription';

describe('<MigrationPlanDescription />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      translation: appState().translation.cart.checkout,
      maxDays: '20',
      migrationMessage: 'string',
      date: '30/08/2016',
      headerTitle: 'title',
      prevMigrationType: MIGRATION_TYPE.DESIRED_DATE,
      migrationType: MIGRATION_TYPE.END_OF_PROMOTIOM,
      setMigrationType: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MigrationPlanDescription {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MigrationPlanDescription {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.WITHDRAWAL
    );

    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.DESIRED_DATE
    );

    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.EMPTY
    );
    (component
      .find('MigrationPlanDescription')
      .instance() as MigrationPlanDescription).onMigrationPlanClickHandler(
      MIGRATION_TYPE.END_OF_PROMOTIOM
    );

    (component
      .find('MigrationPlanDescription')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as MigrationPlanDescription).handleChange({
      target: { value: 'value' }
    } as React.ChangeEvent<HTMLInputElement>);
    (component
      .find('MigrationPlanDescription')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as MigrationPlanDescription).handleDefault({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      target: { value: 'value' }
    } as React.ChangeEvent<HTMLInputElement>);
  });
});
