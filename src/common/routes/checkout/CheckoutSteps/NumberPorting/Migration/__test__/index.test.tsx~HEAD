import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { PLAN_TYPE } from '@checkout/store/numberPorting/enum';

import { IProps } from '../../index';
import Migration, { IList } from '../';

describe('<Migration />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      numberPorting: appState().checkout.numberPorting,
      phoneNumber: appState().checkout.numberPorting.phoneNumber,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Migration {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly', () => {
    const newProps = { ...props };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Migration {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    newProps.checkoutConfiguration.mnp.serviceStatusType.hybrid.migrationOptions.endOfPromotion.show = false;
    newProps.checkoutConfiguration.mnp.serviceStatusType.hybrid.migrationOptions.endOfAgreement.show = false;
    newProps.checkoutConfiguration.mnp.serviceStatusType.hybrid.migrationOptions.desiredDate.show = false;
    newProps.checkoutConfiguration.mns.show = false;
    component.setProps(newProps);
    component.update();
    expect(component).toMatchSnapshot();
  });
  test('componentWillReceiveProps test for input field', () => {
    const newProps = { ...props };
    const componentWillReceivePropsSpy = jest.spyOn(
      Migration.prototype,
      'componentWillReceiveProps'
    );
    const component = shallow(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Migration {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    component.setProps(newProps);
    expect(componentWillReceivePropsSpy).not.toHaveBeenCalled();
    component.update();
  });

  test('new list updated to state', () => {
    const newList: IList[] = [
      {
        title: 'title',
        id: 1,
        selected: true,
        migrationOptions: {
          desiredDate: {
            show: true,
            labelKey: 'label',
            value: 12
          },
          endOfPromotion: {
            show: true,
            labelKey: 'key'
          },
          endOfAgreement: {
            show: true,
            labelKey: 'key'
          }
        }
      }
    ];

    const component = shallow(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Migration {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
    component.setState({ disbaleButton: true, listItems: newList });
    component.update();
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Migration {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('Migration')
      .instance() as Migration).componentWillReceiveProps(props);
    (component.find('Migration').instance() as Migration).onClickHandler(
      PLAN_TYPE.HYBRID
    );
    (component.find('Migration').instance() as Migration).closeModal();
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component.find('Migration').instance() as Migration).onButtonClick({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {}
    } as React.SyntheticEvent);
    (component.find('Migration').instance() as Migration).getSelectedPlan([
      {
        title: 'string',
        id: 2,
        selected: true
      }
    ]);
  });
});
