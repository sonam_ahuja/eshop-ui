import getListOfPlan from '@checkout/CheckoutSteps/NumberPorting/utils/getListOfPlan';
import { convertdataForPatchRequest } from '@checkout/CheckoutSteps/NumberPorting/utils';
import { IProps } from '@checkout/CheckoutSteps/NumberPorting';
import { IMigrationOption } from '@common/store/types/configuration';
import {
  MIGRATION_TITLE,
  MIGRATION_TYPE,
  OPERATION_TYPE,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';
import { Button, Paragraph, Section, Select, Title } from 'dt-components';
import React from 'react';
import PortingHeader from '@checkout/CheckoutSteps/NumberPorting/PortingHeader';
import SimpleSelect from '@src/common/components/Select/SimpleSelect';
import EVENT_NAME from '@events/constants/eventName';
import { sendPortMyNumberEvent } from '@events/checkout/index';
import { sendDropdownClickEvent } from '@src/common/events/common';

import checkHeaderSwitch from '../utils/checkHeaderSwitch';
import getFlowType from '../utils/getFlowType';

import MigrationPlanDescription from './MigrationPlanDescription';
import { StyledMigration } from './styles';

export interface IList {
  title: string;
  id: number;
  selected: boolean;
  migrationOptions?: IMigrationOption;
}
export interface IState {
  disbaleButton: boolean;
  listItems?: IList[];
}
class Migration extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      disbaleButton: this.getButtonStatus(props),
      listItems: getListOfPlan(this.props.checkoutConfiguration)
    };

    this.onClickHandler = this.onClickHandler.bind(this);
    this.onButtonClick = this.onButtonClick.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.replaceNumber = this.replaceNumber.bind(this);
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const { migrationType, date } = nextProps.numberPorting;
    if (migrationType === MIGRATION_TYPE.DESIRED_DATE) {
      if (date) {
        this.setState({ disbaleButton: false });
      } else {
        this.setState({ disbaleButton: true });
      }
    } else if (!migrationType) {
      this.setState({ disbaleButton: true });
    } else {
      this.setState({ disbaleButton: false });
    }
  }

  onClickHandler(title: PLAN_TYPE): void {
    this.props.setPlanType(title);
    this.props.setMigrationType({
      migrationType: MIGRATION_TYPE.EMPTY,
      value: ''
    });
  }

  getButtonStatus(props: IProps): boolean {
    const { migrationType, date } = props.numberPorting;
    if (migrationType === MIGRATION_TYPE.DESIRED_DATE) {
      if (date) {
        return false;
      } else {
        return true;
      }
    } else if (!migrationType) {
      return true;
    } else {
      return false;
    }
  }

  closeModal(): void {
    this.props.showNumberPortModal(false);
    this.props.setFlowType(getFlowType(this.props.checkoutConfiguration));
  }

  onButtonClick(event: React.SyntheticEvent): void {
    event.preventDefault();
    const payload = convertdataForPatchRequest(
      this.props.numberPorting,
      OPERATION_TYPE.MNP,
      this.props.numberPorting.selectedNumber
    );
    const { currentPlan, migrationType } = this.props.numberPorting;
    let migrationTitle = '';
    if (migrationType === MIGRATION_TYPE.WITHDRAWAL) {
      migrationTitle = MIGRATION_TITLE.WITHDRAWAL;
    } else if (migrationType === MIGRATION_TYPE.DESIRED_DATE) {
      migrationTitle = MIGRATION_TITLE.DESIRED_DATE;
    } else if (migrationType === MIGRATION_TYPE.END_OF_PROMOTIOM) {
      migrationTitle = MIGRATION_TITLE.END_OF_PROMOTIOM;
    }
    sendPortMyNumberEvent(migrationTitle, currentPlan.toLocaleLowerCase());
    this.props.requestForPortingNumber(payload);
  }

  replaceNumber(text: string): string {
    const { phoneNumber } = this.props.numberPorting;

    return text.replace('{0}', phoneNumber);
  }

  getSelectedPlan(listItems: IList[]): IList {
    const selectedPlan = listItems.filter(({ selected }) => selected)[0];

    return selectedPlan ? selectedPlan : listItems[0];
  }

  getMaxDays = (): number => {
    const serviceStatus = this.props.checkoutConfiguration.mnp
      .serviceStatusType;
    const currentPlan = this.props.numberPorting.currentPlan.toLowerCase();
    const maxDays =
      serviceStatus[currentPlan] &&
      serviceStatus[currentPlan].migrationOptions &&
      serviceStatus[currentPlan].migrationOptions.desiredDate &&
      serviceStatus[currentPlan].migrationOptions.desiredDate.value;

    return maxDays || 0;
  }

  renderMigrationList(listItems: IList[]): React.ReactNode {
    const { mnp } = this.props.checkoutTranslation;
    const migrationList = listItems.filter(item => item.selected)[0]
      .migrationOptions as IMigrationOption;

    const { endOfAgreement, desiredDate, endOfPromotion } = migrationList;

    const endOfPromotionEl = endOfPromotion.show ? (
      <MigrationPlanDescription
        translation={this.props.checkoutTranslation}
        date={this.props.numberPorting.date}
        headerTitle={mnp.portOnEndOfPromotion}
        prevMigrationType={this.props.numberPorting.migrationType}
        migrationType={MIGRATION_TYPE.END_OF_PROMOTIOM}
        migrationMessage={this.replaceNumber(mnp.portOnEndOfPromotionMessage)}
        setMigrationType={this.props.setMigrationType}
      />
    ) : null;

    const endOfAgreementEl = endOfAgreement.show ? (
      <MigrationPlanDescription
        translation={this.props.checkoutTranslation}
        date={this.props.numberPorting.date}
        headerTitle={mnp.portOnEndOfAgreement}
        prevMigrationType={this.props.numberPorting.migrationType}
        migrationType={MIGRATION_TYPE.WITHDRAWAL}
        migrationMessage={this.replaceNumber(mnp.portOnEndOfAgreementMessage)}
        setMigrationType={this.props.setMigrationType}
      />
    ) : null;

    const desiredDateEl = desiredDate.show ? (
      <MigrationPlanDescription
        translation={this.props.checkoutTranslation}
        date={this.props.numberPorting.date}
        mnpEligibleDates={this.props.numberPorting.mnpEligibleDates}
        headerTitle={mnp.portOnDesiredDate}
        prevMigrationType={this.props.numberPorting.migrationType}
        migrationType={MIGRATION_TYPE.DESIRED_DATE}
        migrationMessage={this.replaceNumber(mnp.portOnDesiredDateMessage)}
        setMigrationType={this.props.setMigrationType}
        disableWeekends={this.props.checkoutConfiguration.mnp.disableWeekends}
        maxDays={`${this.getMaxDays()}`}
        showDateList={this.props.checkoutConfiguration.mnp.showDateList}
      />
    ) : null;

    return (
      <>
        {endOfPromotionEl}
        {endOfAgreementEl}
        {desiredDateEl}
      </>
    );
  }

  renderDropDown(listItems: IList[]): React.ReactNode {
    if (listItems.length === 0) {
      return null;
    }

    const { mnp } = this.props.checkoutTranslation;
    const items = listItems.map((item, index) => {
      return {
        id: index,
        title: mnp[item.title],
        selected: item.selected
      };
    });
    const item = items.filter(({ selected }) => selected)[0];
    let placeholder = mnp[`${items[0].title.toLowerCase()}Text`];
    if (placeholder) {
      placeholder = item.title;
    }

    return (
      <Select
        className='inlineSelect'
        placeholder={placeholder}
        items={items}
        onItemSelect={({ title }) => {
          sendDropdownClickEvent(title);
          this.onClickHandler(title as PLAN_TYPE);
        }}
        selectedItem={{
          id: item.id,
          title: item.title
        }}
        SelectedItemComponent={selectedItemProps => (
          <>
            <SimpleSelect
              {...selectedItemProps}
              inheritFontStyles={true}
              isOpen
              selectedItem={{
                id:
                  selectedItemProps && selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.id
                    : '',
                title:
                  selectedItemProps && selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.title
                    : ''
              }}
            />
          </>
        )}
      />
    );
  }

  render(): React.ReactNode {
    const { checkoutTranslation, numberPorting } = this.props;
    const { mnp } = checkoutTranslation;
    let { listItems = [] } = this.state;

    listItems = listItems.map(item => {
      const selected =
        item.title.toLowerCase() ===
        `${numberPorting.currentPlan}Text`.toLowerCase();

      return { ...item, selected };
    });

    const headerEl = checkHeaderSwitch(this.props.checkoutConfiguration) ? (
      <>
        <PortingHeader {...this.props} />
        <Paragraph className='numberPortingHeader' size='large' weight='bold'>
          {mnp.migrationHeaderText} <br /> {numberPorting.phoneNumber} {mnp.is}
          <span className='selectWrap'>{this.renderDropDown(listItems)}</span>
        </Paragraph>
      </>
    ) : (
      <Title className='numberPortingHeader' size='small' weight='ultra'>
        {mnp.headingText}

        <div className='subHeadingText'>
          {mnp.migrationHeaderText}
          <br />
          {numberPorting.phoneNumber} {mnp.is}
          <span className='selectWrap'>{this.renderDropDown(listItems)}</span>
        </div>
      </Title>
    );

    return (
      <StyledMigration className='numberPortingMigration'>
        <div className='numberPortingInner'>
          {headerEl}

          <div className='numberPortingBody'>
            {this.renderMigrationList(listItems)}
          </div>

          <div className='numberPortingFooter'>
            <Section className='noteText' size='large'>
              {mnp.consentMessageText}
            </Section>
            <Button
              type='complementary'
              disabled={this.state.disbaleButton}
              onClickHandler={this.onButtonClick}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.PORT_MY_NUMBER}
              data-event-message={mnp.portMyNumberText}
              loading={numberPorting.loading}
            >
              {mnp.portMyNumberText}
            </Button>
          </div>
        </div>
      </StyledMigration>
    );
  }
}
// tslint:disable:max-file-line-count
export default Migration;
