import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const StyledMigration = styled.div`
  height: 100%;
  min-height: 100%;

  .mnpDatePicker {
  }

  .numberPortingHeader {
    color: ${colors.mediumGray};

    .subHeadingText {
      color: ${colors.darkGray};

      .selectWrap {
        display: inline-flex;
      }
    }
  }

  .numberPortingFooter {
    justify-self: flex-end;
    margin-top: auto;

    .noteText {
      color: ${colors.mediumGray};
      margin-bottom: 1rem;
    }
  }

  .accordionSelector {
    border: 1px solid transparent;
    border-radius: 0.5rem;
    padding: 1rem 1.25rem;
    margin-bottom: 0.5rem;
    background: ${colors.cloudGray};

    .textWithDropDown {
      display: inline-flex;

      .text {
        margin-right: 3px;
      }
    }
  }

  .accordionSelector.active {
    box-shadow: 0 0 0 5px ${colors.coldGray} inset;
    border: 1px solid ${hexToRgbA(colors.magenta, 0.98)};
    background: ${colors.white};

    .accordionContent {
      color: ${colors.ironGray};
      margin-top: 1rem;
    }

    .customSelect {
      .dt_icon {
        display: block;
      }
      .dt_floatLabelInput, .dt_select {
        display: block;
      }
    }
  }

  .numberPortingInner {
    .portingTabHeader {
      margin: 0 -1.25rem 2.25rem;
      & + {
        .numberPortingHeader {
          margin-top: 0;
          color: ${colors.darkGray};
          margin-bottom: 2.25rem;
          }
        }
      }
    }

    .numberPortingBody {
      .accordionSelector {
        padding: 0.8125rem 1.25rem 0.75rem;
        margin-bottom:0.5rem;
        &.isOpen {
          .textWithDropDown {
            .customSelect {
              color: ${colors.magenta};
            }
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .numberPortingInner {
      .portingTabHeader {
        margin: 0 -5.25rem 2rem;
        & + {
          .numberPortingHeader {
            margin-top: 0;
            color: ${colors.darkGray};
            margin-bottom: 1rem;
          }
        }
      }

      .numberPortingBody {
        .accordionSelector {
          padding: 0.5rem 1.25rem;
          &.isOpen {
            .textWithDropDown {
              .customSelect {
                color: ${colors.magenta};
              }
            }
          }
        }
      }
    }
  }
`;
