import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import NumberPortingBanner, {
  IProps
} from '@checkout/CheckoutSteps/NumberPorting/NumberPortingBanner';

describe('<Number Porting />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      className: '',
      banerImageUrl: '',
      selectedNumber: {
        id: 'string',
        displayId: 'string',
        selected: true
      },
      migrationType: 'type',
      portingNumber: '',
      personalInfo: appState().checkout.personalInfo,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      requestForPortingNumber: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setNumberPortingStatus: jest.fn(),
      showCancelModal: jest.fn(),
      requestForCancelPortingNumber: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NumberPortingBanner {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const component = mount<NumberPortingBanner>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NumberPortingBanner {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component
      .find('NumberPortingBanner')
      .instance() as NumberPortingBanner).openModal();
    (component
      .find('NumberPortingBanner')
      .instance() as NumberPortingBanner).openCancelModal();
    (component
      .find('NumberPortingBanner')
      .instance() as NumberPortingBanner).closeCancelModal();
    (component
      .find('NumberPortingBanner')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as NumberPortingBanner).cancelSelection({
      // tslint:disable-next-line: no-empty
      preventDefault: () => {}
    } as React.SyntheticEvent);
  });
  test('handle trigger of open modal', () => {
    const newProps = { ...props };
    newProps.personalInfo.isNumberPorting = true;
    const component = mount<NumberPortingBanner>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NumberPortingBanner {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component
      .find('NumberPortingBanner')
      .instance() as NumberPortingBanner).openModal();
  });
  // @
  test('handle trigger of open modal when otp is disable', () => {
    const newProps = { ...props };
    newProps.personalInfo.isNumberPorting = true;
    newProps.checkoutConfiguration.mnp.verificationMethods.MSISDNOTP = false;
    newProps.personalInfo.isNumberPortingModalOpen = true;
    const component = mount<NumberPortingBanner>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NumberPortingBanner {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('NumberPortingBanner')
      .instance() as NumberPortingBanner).openModal();
  });
});
