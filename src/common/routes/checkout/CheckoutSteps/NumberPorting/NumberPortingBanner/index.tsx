import NumberPorting from '@checkout/CheckoutSteps/NumberPorting';
import React, { Component, ReactNode } from 'react';
import { Anchor, Button, Icon, Paragraph, Title } from 'dt-components';
import { ICheckoutTranslation } from '@common/store/types/translation';
import { ICheckout } from '@common/store/types/configuration';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import getFlowType from '@checkout/CheckoutSteps/NumberPorting/utils/getFlowType';
import {
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  OPERATION_TYPE
} from '@checkout/store/numberPorting/enum';
import { IResourceNumber } from '@checkout/store/numberPorting/types';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import PreloadImageWrapper from '@src/common/components/Preload';
import store from '@src/common/store';
import {
  composeCancelRequest,
  getMigrationText,
  getPortButtonText
} from '@checkout/CheckoutSteps/NumberPorting/utils';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import { colors } from '@src/common/variables';
import EVENT_NAME from '@events/constants/eventName';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import appConstants from '@src/common/constants/appConstants';

import { BannerWrapper } from './styles';

export interface IProps {
  className?: string;
  checkoutConfiguration: ICheckout;
  checkoutTranslation: ICheckoutTranslation;
  personalInfo: IPersonalInfoState;
  selectedNumber: IResourceNumber;
  migrationType: string;
  desiredDate?: string;
  banerImageUrl: string;
  portingNumber: string;
  setActiveStep(step: NUMBER_PORTING_STEPS): void;
  showNumberPortModal(status: boolean): void;
  setFlowType(flowType: NUMBER_PORTING_TYPES): void;
  showCancelModal(status: boolean): void;
  setNumberPortingStatus(status: boolean): void;
  requestForPortingNumber(payload: numberPortTypesApi.PATCH.IRequest): void;
  requestForCancelPortingNumber(
    payload: numberPortTypesApi.PATCH.IRequest
  ): void;
}

class NumberPortingBanner extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.openModal = this.openModal.bind(this);
    this.openCancelModal = this.openCancelModal.bind(this);
    this.closeCancelModal = this.closeCancelModal.bind(this);
    this.cancelSelection = this.cancelSelection.bind(this);
  }

  componentDidMount(): void {
    const config = { ...this.props.checkoutConfiguration };
    const { availableNumber } = store.getState().checkout.numberPorting;

    config.mns.show = availableNumber.length === 0 ? false : config.mns.show;
    this.props.setFlowType(getFlowType(config));
  }

  openModal(): void {
    const { checkoutConfiguration, setActiveStep, setFlowType } = this.props;
    const { isNumberPorting } = this.props.personalInfo;

    if (isNumberPorting) {
      if (checkoutConfiguration.mnp.verificationMethods.MSISDNOTP) {
        setFlowType(NUMBER_PORTING_TYPES.MSISDN_WITH_OTP);
      } else {
        setFlowType(NUMBER_PORTING_TYPES.MSISDN_WITHOUT_OTP);
      }
      setActiveStep(NUMBER_PORTING_STEPS.SELECT_MIGRATION_PLAN);
    }
    this.props.showNumberPortModal(true);
  }

  openCancelModal(): void {
    this.props.showCancelModal(true);
  }

  closeCancelModal(): void {
    this.props.showCancelModal(false);
  }

  cancelSelection(event: React.SyntheticEvent): void {
    event.preventDefault();
    const payload = composeCancelRequest(
      store.getState().checkout.numberPorting,
      OPERATION_TYPE.MNS
    );
    this.props.requestForCancelPortingNumber(payload);
  }

  getAnchorElement(
    text: ReactNode,
    onClickHandler: () => void,
    iconName: string
  ): React.ReactNode {
    return (
      <Anchor
        type='primary'
        className='link'
        hreflang={appConstants.LANGUAGE_CODE}
        disabled={false}
        underline={false}
        data-event-id={EVENT_NAME.CHECKOUT.EVENTS.NUMBER_PORTING}
        data-event-message={text}
        isLoading={false}
        onClickHandler={() => onClickHandler()}
      >
        <Paragraph size='small' weight='normal'>
          {text} {iconName ? <Icon name={iconName as iconNamesType} /> : ''}
        </Paragraph>
      </Anchor>
    );
  }

  render(): React.ReactNode {
    const {
      checkoutTranslation,
      personalInfo,
      selectedNumber,
      banerImageUrl,
      className,
      portingNumber
    } = this.props;
    const { personalInfo: translation, mnp } = checkoutTranslation;
    const { editPort, cancel, mnpPortedText, mnpInitialText } = translation;
    const { availableNumber } = store.getState().checkout.numberPorting;

    const buttonText = getPortButtonText(this.props);
    const migrationText = getMigrationText(this.props);
    const { phoneNumber } = store.getState().checkout.numberPorting;

    if (!buttonText) {
      return null;
    }
    const anchorOpenModalEl = this.getAnchorElement(
      buttonText,
      this.openModal,
      ''
    );
    const anchorEdit = this.getAnchorElement(
      editPort,
      this.openModal,
      'ec-edit'
    );
    const anchorCancel = this.getAnchorElement(
      cancel,
      this.openCancelModal,
      'ec-cancel'
    );
    const bannerHeadingText = availableNumber.length ? (
      <>
        {mnpInitialText} <br />
        {translation.numberIs}
        <span className='mobNumber'>
          {' '}
          {selectedNumber && selectedNumber.displayId}{' '}
        </span>
      </>
    ) : (
      <>{mnp.mnpBannerText}</>
    );

    const BannerEl = personalInfo.isNumberPorting ? (
      <>
        <Title firstLetterTransform='uppercase' size='small' weight='ultra'>
          {mnpPortedText.replace('{0}', ` ${phoneNumber}`)} {migrationText}
        </Title>
        <div className='linksWrap'>
          {anchorEdit}
          {anchorCancel}
        </div>
      </>
    ) : (
      <>
        <Title firstLetterTransform='uppercase' size='small' weight='ultra'>
          {bannerHeadingText}
        </Title>
        <div className='linksWrap'>{anchorOpenModalEl}</div>
      </>
    );

    return (
      <>
        <DialogBoxApp
          className='cancelModal'
          isOpen={personalInfo.isNumberPortingCancelModal}
          type='flexibleHeight'
          onClose={this.closeCancelModal}
        >
          <div className='dialogBoxHeader'>
            <Icon
              name='ec-emergency-triangle'
              size='xlarge'
              color={colors.magenta}
            />
          </div>

          <div className='dialogBoxBody'>
            <Title className='infoMessage' size='large' weight='ultra'>
              {translation.cancelMessage.replace(
                '{0}',
                `${portingNumber}` || ' number'
              )}
            </Title>
          </div>

          <div className='dialogBoxFooter'>
            <Button
              type='secondary'
              size='medium'
              onClickHandler={this.cancelSelection}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.CANCEL_PORT}
              data-event-message={translation.cancelPortText}
            >
              {translation.cancelPortText}
            </Button>
          </div>
        </DialogBoxApp>

        {personalInfo.isNumberPortingModalOpen && <NumberPorting />}
        <BannerWrapper className={className}>
          <div className='productWrap'>
            <div className='productImages'>
              <div className='big'>
                <PreloadImageWrapper
                  imageUrl={banerImageUrl}
                  mobileWidth={260}
                  width={260}
                  imageHeight={10}
                  imageWidth={10}
                />
              </div>
            </div>
          </div>

          <div className='bannerTextWrap'>{BannerEl}</div>
        </BannerWrapper>
      </>
    );
  }
}

// tslint:disable-next-line:max-file-line-count
export default NumberPortingBanner;
