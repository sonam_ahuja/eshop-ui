import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const BannerWrapper = styled.div`
  border-radius: 0.5rem;
  background: ${colors.white};
  overflow: hidden;

  .mobNumber {
    white-space: nowrap;
  }

  /* PRODUCT IMAEGS SECTION START*/
  .productWrap {
    background: ${colors.cloudGray};
    padding: 1.25rem 1.25rem 1rem;
    display: flex;
    flex-direction: column;

    .productImages {
      display: flex;
      align-items: flex-end;
      .big {
        width: 5.5rem;
        height: 5.5rem;
        display: flex;
      }
      .big img {
        max-width: 100%;
        max-height: 100%;
        display: block;
        align-self: flex-end;
      }
    }

    .productName {
      margin-top: auto;
      padding-top: 0.75rem;
      color: ${colors.darkGray};
      ul {
        display: flex;

        li:first-child:before {
          display: none;
        }
        li:before {
          content: '+';
          display: inline-flex;
          margin: 0 0.25rem;
        }
      }
    }
  }
  /* PRODUCT IMAEGS SECTION END*/

  /* TEXT AND DESCRIPTION START*/
  .bannerTextWrap {
    padding: 1.5rem 1.25rem 1.25rem;
    min-height: 9.5rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .linksWrap {
      display: flex;
      justify-content: space-between;
      padding-top: 2rem;

      .link {
        display: inline-flex;
        align-items: center;

        .dt_paragraph {
          display: inline-flex;
          align-items: center;
          .dt_icon {
            margin-left: 0.5rem;
          }
        }
      }
    }
  }
  /* TEXT AND DESCRIPTION END*/

  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;

    /* PRODUCT IMAEGS SECTION START*/
    .productWrap {
      width: 9.5rem;
      flex-shrink: 0;
      .productImages {
        justify-content: center;
      }

      .productName {
        font-size: 0.75rem;
        padding-top: 0.25rem;
      }
    }
    /* PRODUCT IMAEGS SECTION END*/

    /* TEXT AND DESCRIPTION START*/
    .bannerTextWrap {
      padding: 1rem 1.25rem 1rem;
      min-height: 9.5rem;
    }
    /* TEXT AND DESCRIPTION END*/
  }
`;
