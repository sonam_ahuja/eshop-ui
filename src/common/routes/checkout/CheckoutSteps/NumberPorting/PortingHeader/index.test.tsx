import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_TYPES
} from '@checkout/store/numberPorting/enum';

import { IProps } from '../index';

import Header from '.';

describe('<Number Porting />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      numberPorting: appState().checkout.numberPorting,
      phoneNumber: appState().checkout.numberPorting.phoneNumber,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn(),
      setNumberPortingStatus: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Header {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('Simulate the click on Header selection', () => {
    const newProps = { ...props };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <Header {...props} />
        </Provider>
      </ThemeProvider>
    );
    // tslint:disable:no-duplicate-string
    const tabEl = component.find('.tabHeader');
    tabEl.at(0).simulate('click');
    expect(component).toMatchSnapshot();
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
    newProps.checkoutConfiguration.mnp.verificationMethods.MSISDNOTP = false;
    component.setProps(newProps);
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
    newProps.numberPorting.migrationType = MIGRATION_TYPE.WITHDRAWAL;
    component.setProps(newProps);
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
  });
  test('Simulate the click on Change Number', () => {
    const newProps = { ...props };
    newProps.numberPorting.flowType = NUMBER_PORTING_TYPES.CHANGE_NUMBER;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <Header {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    const tabEl = component.find('.tabHeader');
    tabEl.at(0).simulate('click');
    expect(component).toMatchSnapshot();
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
  });

  test('Simulate the click and otp validation falser', () => {
    const newProps = { ...props };
    newProps.numberPorting.flowType = NUMBER_PORTING_TYPES.CHANGE_NUMBER;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <Header {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    const tabEl = component.find('.tabHeader');
    tabEl.at(0).simulate('click');
    expect(component).toMatchSnapshot();
    newProps.checkoutConfiguration.mnp.verificationMethods.MSISDNOTP = false;
    component.setProps(newProps);
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
  });
});
