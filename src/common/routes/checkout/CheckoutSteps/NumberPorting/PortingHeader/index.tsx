import React from 'react';
import { Icon, Section } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES
} from '@checkout/store/numberPorting/enum';

import { IProps } from '../index';

import { StyledHeader } from './styles';

class PortingHeader extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.setFlow = this.setFlow.bind(this);
  }

  setFlow(flowType: NUMBER_PORTING_TYPES): void {
    const { setFlowType, checkoutConfiguration, setActiveStep } = this.props;

    if (flowType === this.props.numberPorting.flowType) {
      return;
    }

    if (flowType === NUMBER_PORTING_TYPES.CHANGE_NUMBER) {
      setFlowType(NUMBER_PORTING_TYPES.CHANGE_NUMBER);
      setActiveStep(NUMBER_PORTING_STEPS.SELECT_NUMBER);
    } else {
      checkoutConfiguration.mnp.verificationMethods.MSISDNOTP
        ? setFlowType(NUMBER_PORTING_TYPES.MSISDN_WITH_OTP)
        : setFlowType(NUMBER_PORTING_TYPES.MSISDN_WITHOUT_OTP);
      if (this.props.numberPorting.migrationType === MIGRATION_TYPE.EMPTY) {
        setActiveStep(NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER);
      } else {
        setActiveStep(NUMBER_PORTING_STEPS.SELECT_MIGRATION_PLAN);
      }
    }
  }

  render(): React.ReactNode {
    const {
      newNumberText,
      portNumberText
    } = this.props.checkoutTranslation.mnp;

    const newNumberClass =
      this.props.numberPorting.flowType === NUMBER_PORTING_TYPES.CHANGE_NUMBER
        ? ' tabHeader active'
        : 'tabHeader';
    const portNumberClass =
      this.props.numberPorting.flowType !== NUMBER_PORTING_TYPES.CHANGE_NUMBER
        ? ' tabHeader active'
        : 'tabHeader';

    return (
      <StyledHeader className='portingTabHeader'>
        <div
          className={newNumberClass}
          onClick={() => this.setFlow(NUMBER_PORTING_TYPES.CHANGE_NUMBER)}
        >
          <Icon
            color='currentColor'
            name={'ec-rotate-clockwise' as iconNamesType}
            size='inherit'
          />
          <Section size='large' weight='bold'>
            {newNumberText}
          </Section>
        </div>
        <div
          className={portNumberClass}
          onClick={() => this.setFlow(NUMBER_PORTING_TYPES.MSISDN_WITH_OTP)}
        >
          <Icon
            color='currentColor'
            name={'ec-smartphone-data-transfer' as iconNamesType}
            size='inherit'
          />
          <Section size='large' weight='bold'>
            {portNumberText}
          </Section>
        </div>
      </StyledHeader>
    );
  }
}

export default PortingHeader;
