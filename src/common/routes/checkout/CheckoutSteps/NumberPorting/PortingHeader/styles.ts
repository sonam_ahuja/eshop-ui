import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  padding: 1.25rem 1.25rem 1.125rem;
  background: ${colors.cloudGray};
  height: 140px;
  flex-shrink: 0;

  .dt_icon {
    font-size: 30px;
    margin-bottom: 0.5rem;
  }

  .tabHeader {
    text-align: center;
    margin: 0 0.25rem;
    width: 7.5rem;
    color: ${colors.mediumGray};
    cursor: pointer;
  }
  .tabHeader.active {
    color: ${colors.magenta};
    .dt_section {
      color: ${colors.darkGray};
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
  }
`;
