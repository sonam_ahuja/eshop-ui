import React from 'react';
import { Selector } from 'dt-components';
import { IResourceNumber } from '@checkout/store/numberPorting/types';

export interface IProps {
  phoneNumber: IResourceNumber;
  active: boolean;
  setSelectedPhoneNumber(phoneNumber: IResourceNumber): void;
}

class Number extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.onClickHandler = this.onClickHandler.bind(this);
  }

  onClickHandler(): void {
    this.props.setSelectedPhoneNumber(this.props.phoneNumber);
  }

  shouldComponentUpdate(nextProps: IProps): boolean {
    return nextProps.active !== this.props.active;
  }

  render(): React.ReactNode {
    const { phoneNumber, active } = this.props;

    return (
      <Selector
        size='medium'
        onClick={this.onClickHandler}
        label={phoneNumber.displayId}
        active={active}
      />
    );
  }
}

export default Number;
