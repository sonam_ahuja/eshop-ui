import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';

import { IProps } from '../index';
import { NUMBER_PORTING_TYPES } from '../../../store/numberPorting/enum';

import SelectNumber from '.';

describe('<Number Porting />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      numberPorting: appState().checkout.numberPorting,
      phoneNumber: appState().checkout.numberPorting.phoneNumber,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn(),
      setNumberPortingStatus: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SelectNumber {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('Simulate the click on Header selection', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mnp.show = true;
    newProps.checkoutConfiguration.mns.show = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <SelectNumber {...props} />
        </Provider>
      </ThemeProvider>
    );
    // tslint:disable:no-duplicate-string
    const tabEl = component.find('.tabHeader');
    // @
    tabEl.at(0).simulate('click');
    expect(component).toMatchSnapshot();
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
    newProps.checkoutConfiguration.mnp.verificationMethods.MSISDNOTP = false;
    component.setProps(newProps);
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
  });
  test('Simulate the click on Change Number', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mnp.show = true;
    newProps.checkoutConfiguration.mns.show = true;
    newProps.numberPorting.flowType = NUMBER_PORTING_TYPES.CHANGE_NUMBER;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <SelectNumber {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    // @
    const tabEl = component.find('.tabHeader');
    tabEl.at(0).simulate('click');
    expect(component).toMatchSnapshot();
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
  });

  test('Simulate the click and otp validation falser', () => {
    const newProps = { ...props };
    newProps.numberPorting.flowType = NUMBER_PORTING_TYPES.CHANGE_NUMBER;
    newProps.checkoutConfiguration.mnp.show = true;
    newProps.checkoutConfiguration.mns.show = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <SelectNumber {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    // @
    const tabEl = component.find('.tabHeader');
    tabEl.at(0).simulate('click');
    expect(component).toMatchSnapshot();
    newProps.checkoutConfiguration.mnp.verificationMethods.MSISDNOTP = false;
    component.setProps(newProps);
    // @
    tabEl.at(1).simulate('click');
    expect(component).toMatchSnapshot();
  });
  test('available number length ', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mnp.show = false;
    newProps.numberPorting.availableNumber = [
      { id: '1', selected: true, displayId: '1' },
      { id: '2', selected: false, displayId: '2' },
      { id: '3', selected: false, displayId: '3' },
      { id: '4', selected: false, displayId: '4' },
      { id: '5', selected: false, displayId: '5' },
      { id: '6', selected: false, displayId: '6' },
      { id: '7', selected: false, displayId: '7' },
      { id: '8', selected: false, displayId: '8' }
    ];
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <SelectNumber {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    newProps.numberPorting.selectedNumber = {
      id: '1',
      selected: true,
      displayId: '1'
    };
    expect(component).toMatchSnapshot();
    component.setProps(newProps);
    component.update();
    component.find('button').simulate('click');
    expect(component).toMatchSnapshot();
    newProps.numberPorting.selectedNumber = {
      id: '4',
      selected: true,
      displayId: '4'
    };
    expect(component).toMatchSnapshot();
    component.setProps(newProps);
    component.update();
    component.find('button').simulate('click');
    expect(component).toMatchSnapshot();
  });
  test('available number length ', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mnp.show = false;
    newProps.numberPorting.availableNumber = [
      { id: '1', selected: true, displayId: '1' },
      { id: '2', selected: false, displayId: '2' },
      { id: '3', selected: false, displayId: '3' },
      { id: '1', selected: false, displayId: '4' },
      { id: '5', selected: false, displayId: '5' },
      { id: '6', selected: false, displayId: '6' },
      { id: '7', selected: false, displayId: '7' },
      { id: '8', selected: false, displayId: '8' }
    ];
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <SelectNumber {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
    (component.find('SelectNumber').instance() as SelectNumber).drawAllNumber();
  });
});
