import React from 'react';
import { Anchor, Button, Paragraph, Title } from 'dt-components';
import { MIGRATION_TYPE } from '@checkout/store/numberPorting/enum';
import PortingHeader from '@checkout/CheckoutSteps/NumberPorting/PortingHeader';
import { IProps } from '@checkout/CheckoutSteps/NumberPorting/index';
import checkHeaderSwitch from '@checkout/CheckoutSteps/NumberPorting/utils/checkHeaderSwitch';
import { convertdataForPatchRequest } from '@checkout/CheckoutSteps/NumberPorting/utils';
import EVENT_NAME from '@events/constants/eventName';
import appConstants from '@src/common/constants/appConstants';

import getFlowType from '../utils/getFlowType';
import getSelectedNumber from '../utils/getSelectedNumber';

import Number from './Number';
import { StyledSelectNumber } from './styles';

class SelectNumber extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.closeModal = this.closeModal.bind(this);
    this.onConfirmNumberClick = this.onConfirmNumberClick.bind(this);
    this.drawAllNumber = this.drawAllNumber.bind(this);
  }

  onConfirmNumberClick(event: React.SyntheticEvent): void {
    event.preventDefault();
    event.persist();
    const selectedNumber = getSelectedNumber(
      this.props.numberPorting.availableNumber
    );
    this.props.setNumberPortingStatus(false);
    this.props.setMigrationType({ migrationType: MIGRATION_TYPE.EMPTY });
    if (this.props.numberPorting.selectedNumber.id === selectedNumber.id) {
      this.closeModal();
    } else {
      const payload = convertdataForPatchRequest(
        this.props.numberPorting,
        'MNS',
        selectedNumber
      );
      this.props.requestForPortingNumber(payload);
    }
  }

  closeModal(): void {
    this.props.showNumberPortModal(false);
    this.props.setFlowType(getFlowType(this.props.checkoutConfiguration));
  }

  drawAllNumber(): void {
    this.props.showAllNumber(true);
  }

  render(): React.ReactNode {
    const { checkoutTranslation, numberPorting } = this.props;
    const { showAllNumber, availableNumber, loading } = numberPorting;
    const { mns } = checkoutTranslation;
    const headerEl = checkHeaderSwitch(this.props.checkoutConfiguration) ? (
      <PortingHeader {...this.props} />
    ) : (
      <Title className='numberPortingHeader' size='large' weight='ultra'>
        {mns.headingText}
      </Title>
    );

    const drawAllNumberEl = !showAllNumber && availableNumber.length > 6 && (
      <Paragraph className='linkWrap' size='small' weight='normal'>
        <Anchor
          hreflang={appConstants.LANGUAGE_CODE}
          title={mns.otherNumbers}
          onClickHandler={this.drawAllNumber}
          data-event-id={EVENT_NAME.CHECKOUT.EVENTS.MNS_OTHER_NUMBER}
          data-event-message={mns.otherNumbers}
        >
          {mns.otherNumbers}
        </Anchor>
      </Paragraph>
    );

    return (
      <StyledSelectNumber className='numberPortingSelectNumber'>
        <div className='numberPortingInner'>
          <div className='numberPortingBody'>
            {headerEl}
            <div className='numberWrap'>
              {availableNumber.map((phoneNumber, index) => {
                return !showAllNumber && index > 5 ? null : (
                  <Number
                    key={phoneNumber.id}
                    phoneNumber={phoneNumber}
                    active={!!phoneNumber.selected}
                    setSelectedPhoneNumber={this.props.setSelectedPhoneNumber}
                  />
                );
              })}
            </div>
            {drawAllNumberEl}
          </div>

          <div className='numberPortingFooter'>
            <Button
              type='complementary'
              disabled={availableNumber.length === 0}
              onClickHandler={this.onConfirmNumberClick}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.CONFIRM_NUMBER}
              data-event-message={mns.confirmNumber}
              loading={loading}
            >
              {mns.confirmNumber}
            </Button>
          </div>
        </div>
      </StyledSelectNumber>
    );
  }
}

export default SelectNumber;
