import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';

import PhoneNumber, { IProps } from './Number';

describe('<Number Porting />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      phoneNumber: appState().checkout.numberPorting.selectedNumber,
      active: false,
      setSelectedPhoneNumber: jest.fn()
    };
  });

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <PhoneNumber {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('available number length ', () => {
    const newProps = { ...props };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <PhoneNumber {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
    (component.find('Number').instance() as PhoneNumber).onClickHandler();
    expect(component).toMatchSnapshot();
    (component.find('Number').instance() as PhoneNumber).shouldComponentUpdate(
      newProps
    );
  });
});
