import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSelectNumber = styled.div`
  height: 100%;
  min-height: 100%;

  .numberPortingHeader {
    color: ${colors.ironGray};
  }

  .numberPortingFooter {
    justify-self: flex-end;
    margin-top: auto;
  }

  .numberWrap {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin: -0.125rem 0;

    .dt_selector {
      flex-shrink: 0;
      width: calc(50% - 0.125rem);
      margin: 0.125rem 0;
    }
  }

  .linkWrap {
    margin-top: 1.5rem;
    text-align: center;

    a {
      font-size: inherit;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .numberPortingHeader {
      font-size: 1.5rem;
      line-height: 1.17;
      letter-spacing: normal;
    }
  }
`;
