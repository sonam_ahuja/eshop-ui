import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';

import {
  ACTION_TYPE,
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  OPERATION_TYPE,
  PLAN_TYPE
} from '../../store/numberPorting/enum';
import { IValidateNumberRequest } from '../../store/numberPorting/types';

import NumberPorting, {
  IProps,
  mapDispatchToProps,
  mapStateToProps,
  NumberPorting as NumberPortingType
} from '.';
import EnterNumber from './EnterNumber';

// tslint:disable:no-big-function
describe('<Number Porting />', () => {
  const initStateValue = appState();
  let props: IProps;
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  beforeEach(() => {
    props = {
      numberPorting: appState().checkout.numberPorting,
      phoneNumber: appState().checkout.numberPorting.phoneNumber,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn(),
      setNumberPortingStatus: jest.fn()
    };
  });

  test('should render properly', () => {
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('Render Enter OTP component', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.ENTER_OTP;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Render select migration plan component', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.SELECT_MIGRATION_PLAN;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Render Select Number component', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.SELECT_NUMBER;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('Render loading screen ', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.loading = true;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('Render loading screen ', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER;
    initaialValue.checkout.numberPorting.loading = true;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
  test('handleChange test for Enter number input field', () => {
    const newProps = { ...props };
    const inputEvent = { currentTarget: { value: ' ' }, keyCode: 32 };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterNumber {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    const input = component.find('input');
    input.at(0).simulate('keydown', inputEvent);
    input.at(0).simulate('keydown', { currentTarget: { value: 'e ' } });
    input.at(0).simulate('change', { currentTarget: { value: 'a' } });
    input.at(0).simulate('change', inputEvent);

    expect(component).toMatchSnapshot();
  });
  test('handleChange test for Enter number input field with max length validation ', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.form.fields.phoneNumber.validation = {
      type: 'max',
      value: '2',
      message: 'Length is exceeded'
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EnterNumber {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    const input = component.find('input');

    input.at(0).simulate('keydown', { currentTarget: { value: 'e ' } });
    input.at(0).simulate('change', { currentTarget: { value: 'a' } });
    input.at(0).simulate('change', { currentTarget: { value: '1234' } });

    expect(component).toMatchSnapshot();
  });
  test('call componentWill unmount', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER;
    initaialValue.checkout.numberPorting.loading = true;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wrapper
      .find('NumberPorting')
      .instance() as NumberPortingType).componentWillUnmount();
    expect(wrapper).toMatchSnapshot();
  });
  test('call close modal ', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER;
    initaialValue.checkout.numberPorting.loading = false;

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wrapper
      .find('NumberPorting')
      .instance() as NumberPortingType).closeModal();
    expect(wrapper).toMatchSnapshot();
  });

  test('call component did with Tnc flag true ', () => {
    const initaialValue = { ...initStateValue };
    initaialValue.checkout.numberPorting.activeStep =
      NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER;
    initaialValue.checkout.numberPorting.termConditionId = '1231';

    const initalMockStore = mockStore(initStateValue);
    const wrapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={initalMockStore}>
            <NumberPorting />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (wrapper
      .find('NumberPorting')
      .instance() as NumberPortingType).closeModal();
    expect(wrapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const phoneNumber = {
      id: '9876543210',
      displayId: '9876543210',
      selected: true
    };
    const migrationType = {
      migrationType: MIGRATION_TYPE.DESIRED_DATE,
      value: '2019-12-10'
    };
    const requestOtp: IValidateNumberRequest = {
      otpContext: 'otp',
      action: OPERATION_TYPE.SEND,
      mobileNumber: '+9155443322',
      nonce: 'nonoe',
      otp: 'otp'
    };
    // tslint:disable-next-line:no-any
    const payload: any = {
      consent: true,
      operationType: OPERATION_TYPE.MNS,
      cartItem: [
        {
          action: ACTION_TYPE.DELETE,
          cartItemId: props.numberPorting.numberPortingCartItemId
        }
      ]
    };

    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.proceedToNextScreen();
    mapDispatch.requestForPortingNumber(payload);
    mapDispatch.setActiveStep(NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER);
    mapDispatch.setFlowType(NUMBER_PORTING_TYPES.CHANGE_NUMBER);
    mapDispatch.setMigrationType(migrationType);
    mapDispatch.setPhoneNumber(phoneNumber.id);
    mapDispatch.setPlanType(PLAN_TYPE.HYBRID);
    mapDispatch.setSelectedPhoneNumber(phoneNumber);
    mapDispatch.showAllNumber(true);
    mapDispatch.requestOtp(requestOtp);
    mapDispatch.confirmSelectedPhoneNumber(phoneNumber);
    mapDispatch.validateOtp(requestOtp);
    mapDispatch.showNumberPortModal(false);
    mapDispatch.setNumberPortingStatus(false);
  });
  // tslint:disable-next-line:max-file-line-count
});
