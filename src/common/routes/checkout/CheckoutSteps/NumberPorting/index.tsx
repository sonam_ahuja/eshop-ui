import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Loadable, { LoadingComponentProps } from 'react-loadable';
import { RootState } from '@common/store/reducers';
import React, { FunctionComponent } from 'react';
import { ICheckoutTranslation } from '@common/store/types/translation';
import { ICheckout } from '@common/store/types/configuration';
import personalInfoActions from '@checkout/store/personalInfo/actions';
import numberPortingActions from '@checkout/store/numberPorting/actions';
import * as numberPortTypesApi from '@common/types/api/checkout/numberPorting';
import {
  INumberPortingState,
  IResourceNumber,
  ISetMigrationType
} from '@checkout/store/numberPorting/types';
import {
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';
import { AppProgressModal } from '@src/common/components/ProgressModal/styles';

import { NumberPortingFlowPanel } from './styles';
import getLoadingMessage from './utils/getLoadingMessage';

export interface ILocalState {
  numberPorting: INumberPortingState;
  checkoutConfiguration: ICheckout;
  checkoutTranslation: ICheckoutTranslation;
  phoneNumber: string;
}
export interface INumberPortingDispatchToProps {
  proceedToNextScreen(): void;
  setPhoneNumber(phoneNumber: string): void;
  requestForPortingNumber(payload: numberPortTypesApi.PATCH.IRequest): void;
  requestOtp(payload: numberPortTypesApi.PHONE_POST.IRequest): void;
  setMigrationType(plan: ISetMigrationType): void;
  setPlanType(plan: PLAN_TYPE): void;
  validateOtp(payload: numberPortTypesApi.PHONE_POST.IRequest): void;
  setFlowType(flowType: NUMBER_PORTING_TYPES): void;
  setActiveStep(step: NUMBER_PORTING_STEPS): void;
  showNumberPortModal(status: boolean): void;
  setSelectedPhoneNumber(phoneNumber: IResourceNumber): void;
  confirmSelectedPhoneNumber(phoneNumber: IResourceNumber): void;
  showAllNumber(status: boolean): void;
  requestForTermAndCondition(): void;
  setNumberPortingStatus(status: boolean): void;
}

export type IProps = ILocalState & INumberPortingDispatchToProps;

const Loading: FunctionComponent<LoadingComponentProps> = () => null;

export const EnterNumber = Loadable<IProps, {}>({
  loading: Loading,
  loader: () => import(/* webpackChunkName: 'EnterNumber' */ './EnterNumber'),
  render(
    loaded: typeof import('./EnterNumber'),
    props: IProps
  ): React.ReactNode {
    const EnterNumberLoaded = loaded.default;

    return <EnterNumberLoaded {...props} />;
  }
});

export const SelectNumber = Loadable<IProps, {}>({
  loading: Loading,
  loader: () => import(/* webpackChunkName: 'SelectNumber' */ './SelectNumber'),
  render(
    loaded: typeof import('./SelectNumber'),
    props: IProps
  ): React.ReactNode {
    const SelectNumberLoaded = loaded.default;

    return <SelectNumberLoaded {...props} />;
  }
});
export const EnterOtp = Loadable<IProps, {}>({
  loading: Loading,
  loader: () => import(/* webpackChunkName: 'EnterOtp' */ './EnterOtp'),
  render(loaded: typeof import('./EnterOtp'), props: IProps): React.ReactNode {
    const EnterOtpLoaded = loaded.default;

    return <EnterOtpLoaded {...props} />;
  }
});

export const Migration = Loadable<IProps, {}>({
  loading: Loading,
  loader: () => import(/* webpackChunkName: 'Migration' */ './Migration'),
  render(loaded: typeof import('./Migration'), props: IProps): React.ReactNode {
    const MigrationLoaded = loaded.default;

    return <MigrationLoaded {...props} />;
  }
});
export class NumberPorting extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.closeModal = this.closeModal.bind(this);
  }
  renderPortingStep = () => {
    const { activeStep } = this.props.numberPorting;
    switch (activeStep) {
      case NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER:
        return <EnterNumber {...this.props} />;
      case NUMBER_PORTING_STEPS.ENTER_OTP:
        return <EnterOtp {...this.props} />;
      case NUMBER_PORTING_STEPS.SELECT_MIGRATION_PLAN:
        return <Migration {...this.props} />;
      case NUMBER_PORTING_STEPS.SELECT_NUMBER:
      default:
        return <SelectNumber {...this.props} />;
    }
  }

  componentDidMount(): void {
    const { checkoutConfiguration, numberPorting } = this.props;
    const { show } = checkoutConfiguration.mnp;
    if (show && !numberPorting.termConditionId) {
      this.props.requestForTermAndCondition();
    }
  }

  componentWillUnmount(): void {
    document.body.style.overflow = '';
  }

  closeModal(): void {
    this.props.showNumberPortModal(false);
  }

  render(): React.ReactNode {
    const { activeStep, loading } = this.props.numberPorting;

    const { verificationMethods } = this.props.checkoutConfiguration.mnp;
    const isLoading =
      loading &&
      NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER === activeStep &&
      verificationMethods.MSISDNOTP;

    return (
      <>
        <NumberPortingFlowPanel isOpen onClose={this.closeModal}>
          {this.renderPortingStep()}
        </NumberPortingFlowPanel>
        <AppProgressModal
          isOpen={isLoading}
          title={getLoadingMessage(this.props)}
        />
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): ILocalState => ({
  numberPorting: state.checkout.numberPorting,
  checkoutConfiguration: state.configuration.cms_configuration.modules.checkout,
  checkoutTranslation: state.translation.cart.checkout,
  phoneNumber: state.checkout.personalInfo.formFields.phoneNumber
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): INumberPortingDispatchToProps => ({
  proceedToNextScreen(): void {
    dispatch(numberPortingActions.proceedToNextScreen());
  },
  setPhoneNumber(phoneNumber: string): void {
    dispatch(numberPortingActions.setPhoneNumber(phoneNumber));
  },
  requestForPortingNumber(payload: numberPortTypesApi.PATCH.IRequest): void {
    dispatch(numberPortingActions.requestForPortingNumber(payload));
  },
  requestOtp(payload: numberPortTypesApi.PHONE_POST.IRequest): void {
    dispatch(numberPortingActions.requestOtpForMsisdnNumberPort(payload));
  },
  setMigrationType(plan: ISetMigrationType): void {
    dispatch(numberPortingActions.setMigrationType(plan));
  },
  setPlanType(plan: PLAN_TYPE): void {
    dispatch(numberPortingActions.setPlanType(plan));
  },
  validateOtp(payload: numberPortTypesApi.PHONE_POST.IRequest): void {
    dispatch(
      numberPortingActions.requestOtpValidationForMsisdnNumberPort(payload)
    );
  },
  setFlowType(flowType: NUMBER_PORTING_TYPES): void {
    dispatch(numberPortingActions.setFlowType(flowType));
  },
  setActiveStep(step: NUMBER_PORTING_STEPS): void {
    dispatch(numberPortingActions.setActiveStep(step));
  },
  showNumberPortModal(status: boolean): void {
    dispatch(personalInfoActions.showNumberPortModal(status));
    document.body.style.overflow = '';
  },
  setSelectedPhoneNumber(phoneNumber: IResourceNumber): void {
    dispatch(numberPortingActions.setSelectedPhoneNumber(phoneNumber));
  },
  confirmSelectedPhoneNumber(phoneNumber: IResourceNumber): void {
    dispatch(numberPortingActions.setSelectedPhoneNumber(phoneNumber));
  },
  showAllNumber(status: boolean): void {
    dispatch(numberPortingActions.showAllNumber(status));
  },
  requestForTermAndCondition(): void {
    dispatch(numberPortingActions.requestTermAndConditions());
  },
  setNumberPortingStatus(status: boolean): void {
    dispatch(personalInfoActions.setNumberPortingStatus(status));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NumberPorting);
