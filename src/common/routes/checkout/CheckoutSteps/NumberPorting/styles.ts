import { AppFlowPanel } from '@src/common/components/FlowPanel/styles';
import styled from 'styled-components';
import { Icon } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

export const BackIcon = styled(Icon).attrs({
  className: 'backButton',
  size: 'large',
  name: 'ec-arrow-left'
})`
  position: absolute;
  top: 1.5rem;
  left: 1.25rem;

  @media (min-width: ${breakpoints.desktop}px) {
    width: 1.5rem;
    top: 1rem;
    left: 1.5rem;
  }
`;

export const NumberPortingFlowPanel = styled(AppFlowPanel)`
  /* COMMON CSS FOR ALL FLOW PANNEL IN NUMBER PORTING START*/
  .numberPortingInner {
    padding: 0 1.25rem 2rem;
    height: 100%;
    overflow-y: auto;
    display: flex;
    flex-direction: column;
    background: ${colors.coldGray};

    /* Header with TABS */
    .portingTabHeader {
      margin: 0 -1.25rem 2.25rem;
    }
    .portingTabHeader + .numberPortingBody {
      background: ${colors.white};
    }

    /* Main Text Heading */
    .numberPortingHeader {
      margin-top: 4.8125rem;
      margin-bottom: 2.5rem;

      .selectWrap {
        display: inline-block;
        margin-left: 5px;
        .dt_paragraph {
          color: ${colors.magenta};
        }
        .optionsList {
          right: inherit;
        }
      }
    }

    /* Main Content */
    .numberPortingBody {
    }

    /* Footer - mainly contains button */
    .numberPortingFooter {
      flex-shrink: 0;
      .dt_button {
        width: 100%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .numberPortingInner {
      width: 28.75rem;
      padding: 0 5.25rem 3rem;

      .portingTabHeader {
        margin: 0 -5.25rem 2.5rem;
      }

      .numberPortingHeader {
        margin-top: 4rem;
        margin-bottom: 2rem;

        .selectWrap {
          display: inline-block;
          margin-left: 5px;
        }
      }

      .numberPortingBody {
      }

      .numberPortingFooter {
      }
    }
  }
  /* COMMON CSS FOR ALL FLOW PANNEL IN NUMBER PORTING END*/
`;
