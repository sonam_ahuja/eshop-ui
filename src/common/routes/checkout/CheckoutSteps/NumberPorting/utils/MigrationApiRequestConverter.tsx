import moment from 'moment';
import {
  ICharecterstics,
  INumberPortingState,
  IResourceNumber
} from '@checkout/store/numberPorting/types';
import * as numberPortTypesApi from '@common/types/api/checkout/numberPorting';
import {
  ACTION_TYPE,
  OPERATION_TYPE,
  OPERATOR_TYPE
} from '@checkout/store/numberPorting/enum';

export default function convertdataForPatchRequest(
  numberPorting: INumberPortingState,
  operationType: string,
  selectedNumber: IResourceNumber
): numberPortTypesApi.PATCH.IRequest {
  // tslint:disable:no-any
  const payload: any = {
    consent: true,
    cartItems: []
  };
  payload.operationType = operationType;

  if (numberPorting.numberPortingCartItemId) {
    payload.cartItems.push({
      action: ACTION_TYPE.DELETE,
      cartItemId: numberPorting.numberPortingCartItemId,
      product: {
        id: numberPorting.productOfferingId
      }
    });
  }
  const characteristics: ICharecterstics = {
    selectedNumber:
      operationType === OPERATION_TYPE.MNP
        ? numberPorting.phoneNumber
        : selectedNumber.id
  };

  if (operationType === OPERATION_TYPE.MNP) {
    characteristics.operator = OPERATOR_TYPE.NON_DT_OPERATOR;
    characteristics.numberType = numberPorting.currentPlan.toUpperCase();
    characteristics.otpNonce = numberPorting.nonce;
    characteristics.transferDate = moment(
      numberPorting.date,
      'DD MMMM YYYY'
    ).format('DD-MM-YYYY');
    characteristics.migrationType = numberPorting.migrationType;
  }

  payload.cartItems.push({
    action: ACTION_TYPE.ADD,
    product: {
      id: numberPorting.productOfferingId,
      characteristics
    }
  });

  return payload;
}
