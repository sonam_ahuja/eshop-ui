import convertdataForPatchRequest from '@checkout/CheckoutSteps/NumberPorting/utils/MigrationApiRequestConverter';
import appState from '@store/states/app';
import * as numberPortTypesApi from '@common/types/api/checkout/numberPorting';
import { OPERATION_TYPE } from '@checkout/store/numberPorting/enum';

describe('<Utils />', () => {
  test('convertdataForPatchRequest ::', () => {
    const result: numberPortTypesApi.PATCH.IRequest = convertdataForPatchRequest(
      appState().checkout.numberPorting,
      OPERATION_TYPE.MNP,
      {
        id: '123',
        displayId: '12',
        selected: true
      }
    );
    expect(result).toBe(result);
  });
  test('convertdataForPatchRequest :: when numberPorting cart item id is there ', () => {
    const numberPorting = { ...appState().checkout.numberPorting };
    numberPorting.numberPortingCartItemId = '1234';
    const result: numberPortTypesApi.PATCH.IRequest = convertdataForPatchRequest(
      numberPorting,
      OPERATION_TYPE.MNP,
      {
        id: '123',
        displayId: '12',
        selected: true
      }
    );
    expect(result).toBe(result);
  });
});
