import checkHeaderSwitch from '@checkout/CheckoutSteps/NumberPorting/utils/checkHeaderSwitch';
import appState from '@store/states/app';

describe('<Utils />', () => {
  test('checkHeaderSwitch ::', () => {
    const result: boolean = checkHeaderSwitch(
      appState().configuration.cms_configuration.modules.checkout
    );
    expect(result).toBe(false);
  });
});
