import getFlowType from '@checkout/CheckoutSteps/NumberPorting/utils/getFlowType';
import appState from '@store/states/app';
import { NUMBER_PORTING_TYPES } from '@checkout/store/numberPorting/enum';

describe('<Utils />', () => {
  test('getFlowType ::', () => {
    const result: NUMBER_PORTING_TYPES = getFlowType(
      appState().configuration.cms_configuration.modules.checkout
    );
    expect(typeof result).toBe('string');
  });
});
