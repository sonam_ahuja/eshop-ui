import getListOfPlan from '@checkout/CheckoutSteps/NumberPorting/utils/getListOfPlan';
import appState from '@store/states/app';

import { IList } from '../../Migration';

describe('<Utils />', () => {
  test('getListOfPlan ::', () => {
    const result: IList[] = getListOfPlan(
      appState().configuration.cms_configuration.modules.checkout
    );
    expect(result).toBeDefined();
  });
  test('getListOfPlan with different configuration', () => {
    const checkoutConf = {
      ...appState().configuration.cms_configuration.modules.checkout
    };
    checkoutConf.mnp.serviceStatusType.hybrid.show = false;
    checkoutConf.mnp.serviceStatusType.prepaid.show = false;
    checkoutConf.mnp.serviceStatusType.postpaid.show = false;
    const result: IList[] = getListOfPlan(checkoutConf);
    expect(result).toBeDefined();
  });
});
