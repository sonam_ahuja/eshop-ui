import getLoadingMessage from '@checkout/CheckoutSteps/NumberPorting/utils/getLoadingMessage';
import appState from '@store/states/app';

import { IProps } from '../../index';

describe('<Utils />', () => {
  test('getLoadingMessage ::', () => {
    const props: IProps = {
      numberPorting: appState().checkout.numberPorting,
      checkoutConfiguration: appState().configuration.cms_configuration.modules
        .checkout,
      checkoutTranslation: appState().translation.cart.checkout,
      phoneNumber: '99980756444',
      proceedToNextScreen: jest.fn(),
      setPhoneNumber: jest.fn(),
      requestForPortingNumber: jest.fn(),
      requestOtp: jest.fn(),
      setMigrationType: jest.fn(),
      setPlanType: jest.fn(),
      validateOtp: jest.fn(),
      setFlowType: jest.fn(),
      setActiveStep: jest.fn(),
      showNumberPortModal: jest.fn(),
      setSelectedPhoneNumber: jest.fn(),
      confirmSelectedPhoneNumber: jest.fn(),
      showAllNumber: jest.fn(),
      requestForTermAndCondition: jest.fn(),
      setNumberPortingStatus: jest.fn()
    };
    const result: string = getLoadingMessage(props);
    expect(result).toBeDefined();
  });
});
