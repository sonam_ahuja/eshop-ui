import {
  otpRequestDataConverter,
  otpValidateDataConverter
} from '@checkout/CheckoutSteps/NumberPorting/utils/otpRequestValidationApiConverter';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import { INumberPortingState } from '@checkout/store/numberPorting/types';
import numberPortingState from '@src/common/routes/checkout/store/numberPorting/state';

describe('<otpRequestValidationApiConverter />', () => {
  test('otpRequestDataConverter ::', () => {
    const result: numberPortTypesApi.PHONE_POST.IRequest = otpRequestDataConverter(
      '+919998075168'
    );
    expect(result).toBe(result);
  });

  test('otpValidateDataConverter ::', () => {
    const data: INumberPortingState = numberPortingState();
    const result: numberPortTypesApi.PHONE_POST.IRequest = otpValidateDataConverter(
      data,
      '+919998075168'
    );
    expect(result).toBe(result);
  });
});
