import { ICheckout } from '@src/common/store/types/configuration';

const checkHeaderSwitch = (checkoutConfiguration: ICheckout): boolean => {
  const { mnp, mns } = checkoutConfiguration;

  return mnp.show && mns.show;
};

export default checkHeaderSwitch;
