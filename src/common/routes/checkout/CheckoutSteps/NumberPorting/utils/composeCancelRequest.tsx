import {
  ICharecterstics,
  INumberPortingState
} from '@checkout/store/numberPorting/types';
import * as numberPortTypesApi from '@common/types/api/checkout/numberPorting';
import {
  ACTION_TYPE,
  OPERATION_TYPE
} from '@checkout/store/numberPorting/enum';

export default function composeCancelRequest(
  numberPorting: INumberPortingState,
  operationType: OPERATION_TYPE
): numberPortTypesApi.PATCH.IRequest {
  const characteristics: ICharecterstics = {
    selectedNumber: numberPorting.selectedNumber.id
  };

  return {
    consent: true,
    operationType,
    cartItems: [
      {
        action: ACTION_TYPE.DELETE,
        cartItemId: numberPorting.numberPortingCartItemId,
        product: {
          id: ''
        }
      },
      {
        action: ACTION_TYPE.ADD,
        product: {
          id: numberPorting.productOfferingId,
          characteristics
        }
      }
    ]
  };
}
