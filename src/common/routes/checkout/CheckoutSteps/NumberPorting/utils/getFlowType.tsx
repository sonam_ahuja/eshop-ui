import { ICheckout } from '@common/store/types/configuration';
import { NUMBER_PORTING_TYPES } from '@checkout/store/numberPorting/enum';

const getFlowType = (
  checkoutConfiguration: ICheckout
): NUMBER_PORTING_TYPES => {
  const { mns, mnp } = checkoutConfiguration;
  let flowType = NUMBER_PORTING_TYPES.CHANGE_NUMBER;
  if (mns.show) {
    flowType = NUMBER_PORTING_TYPES.CHANGE_NUMBER;
  } else if (mnp.show && mnp.verificationMethods.MSISDNOTP) {
    flowType = NUMBER_PORTING_TYPES.MSISDN_WITH_OTP;
  } else if (mnp.show) {
    flowType = NUMBER_PORTING_TYPES.MSISDN_WITHOUT_OTP;
  }

  return flowType;
};

export default getFlowType;
