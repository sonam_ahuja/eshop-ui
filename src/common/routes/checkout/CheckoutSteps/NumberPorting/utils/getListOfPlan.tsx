import { ICheckout } from '@src/common/store/types/configuration';

import { IList } from '../Migration';

export default function getListOfPlan(
  checkoutConfiguration: ICheckout
): IList[] {
  const { serviceStatusType } = checkoutConfiguration.mnp;

  const serviceStatus = Object.keys(serviceStatusType)
    .map((key, index) => {
      return serviceStatusType[key].show
        ? {
            title: serviceStatusType[key].labelKey,
            selected: false,
            id: index,
            migrationOptions: serviceStatusType[key].migrationOptions
          }
        : null;
    })
    .filter(key => key);

  if (serviceStatus.length === 0) {
    const migrationOptions = serviceStatusType.prepaid.migrationOptions;
    migrationOptions.desiredDate.show = false;
    migrationOptions.endOfPromotion.show = false;
    migrationOptions.endOfAgreement.show = false;

    serviceStatus.push({
      title: serviceStatusType.prepaid.labelKey,
      selected: true,
      id: 1,
      migrationOptions
    });
  }
  (serviceStatus[0] as IList).selected = true;

  return serviceStatus as IList[];
}
