import { NUMBER_PORTING_STEPS } from '@checkout/store/numberPorting/enum';

import { IProps } from '../index';

const getLoadingMessage = (props: IProps): string => {
  const { numberPorting, checkoutTranslation } = props;
  const { activeStep } = numberPorting;

  if (activeStep === NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER) {
    return checkoutTranslation.mnp.otpSendingMessage;
  }

  return '';
};

export default getLoadingMessage;
