import store from '@src/common/store';

export default function getMaskingNumber(maskInputValue: string): string {
  const globalAuthConfiguration = store.getState().configuration
    .cms_configuration.global.authentication;
  const maskingpattern =
    globalAuthConfiguration.masking.phone &&
    globalAuthConfiguration.masking.phone.length
      ? new RegExp(globalAuthConfiguration.masking.phone)
      : null;
  let maskedValue = maskInputValue;

  if (maskingpattern) {
    maskedValue = (maskInputValue || '').replace(
      maskingpattern,
      (_, a, b, c) => a + b.replace(/./g, '*') + c
    );
  }

  return maskedValue;
}
