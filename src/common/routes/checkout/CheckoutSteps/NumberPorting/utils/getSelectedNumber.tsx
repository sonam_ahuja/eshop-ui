import { IResourceNumber } from '@checkout/store/numberPorting/types';

export default function getSelectedNumber(
  resources: IResourceNumber[]
): IResourceNumber {
  return resources
    .map(phone => {
      return phone.selected
        ? phone
        : { id: '', selected: false, displayId: '' };
    })
    .filter(res => res.selected)[0];
}
