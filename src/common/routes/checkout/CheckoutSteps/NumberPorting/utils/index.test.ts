import appState from '@store/states/app';
import { IProps as INumberPortingBannerProps } from '@checkout/CheckoutSteps/NumberPorting/NumberPortingBanner';
import { MIGRATION_TYPE } from '@checkout/store/numberPorting/enum';

import { getMigrationText, getPortButtonText } from '.';

describe('< number porting util indexing  />', () => {
  const props: INumberPortingBannerProps = {
    className: '',
    checkoutConfiguration: appState().configuration.cms_configuration.modules
      .checkout,
    checkoutTranslation: appState().translation.cart.checkout,
    personalInfo: appState().checkout.personalInfo,
    selectedNumber: appState().checkout.numberPorting.selectedNumber,
    migrationType: '',
    desiredDate: '',
    banerImageUrl: '',
    portingNumber: '',
    setActiveStep: jest.fn(),
    showNumberPortModal: jest.fn(),
    setFlowType: jest.fn(),
    showCancelModal: jest.fn(),
    setNumberPortingStatus: jest.fn(),
    requestForPortingNumber: jest.fn(),
    requestForCancelPortingNumber: jest.fn()
  };

  test('MNS is true and mnp is false', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mns.show = true;
    newProps.checkoutConfiguration.mnp.show = false;
    expect(getPortButtonText(props)).toBeDefined();
  });

  test('MNS is false and mnp is true', () => {
    const newProps = { ...props };
    newProps.checkoutConfiguration.mns.show = false;
    newProps.checkoutConfiguration.mnp.show = true;
    expect(getPortButtonText(props)).toBeDefined();
  });

  test('call function getMigration Text ', () => {
    const newProps = { ...props };
    const {
      endOfPromotionText,
      endOfAgreementText
    } = newProps.checkoutTranslation.mnp;

    newProps.migrationType = MIGRATION_TYPE.END_OF_PROMOTIOM;
    expect(getMigrationText(newProps)).toEqual(endOfPromotionText);

    newProps.migrationType = MIGRATION_TYPE.WITHDRAWAL;
    expect(getMigrationText(newProps)).toEqual(endOfAgreementText);

    newProps.migrationType = MIGRATION_TYPE.DESIRED_DATE;
    expect(getMigrationText(newProps)).toEqual(newProps.desiredDate);
  });
});
