export { default as numberPortingFlow } from './numberPortingFlow';
export { default as checkHeaderSwitch } from './checkHeaderSwitch';
export { default as getFlowType } from './getFlowType';
export { default as getListOfPlan } from './getListOfPlan';
export { default as getLoadingMessage } from './getLoadingMessage';
export { default as getSelectedNumber } from './getSelectedNumber';
export { default as getMaskingNumber } from './getMaskingNumber';
export { default as composeCancelRequest } from './composeCancelRequest';
import { IProps as INumberPortingBannerProps } from '@checkout/CheckoutSteps/NumberPorting/NumberPortingBanner';
import { MIGRATION_TYPE } from '@checkout/store/numberPorting/enum';
export {
  default as convertdataForPatchRequest
} from './MigrationApiRequestConverter';
export {
  otpRequestDataConverter,
  otpValidateDataConverter
} from './otpRequestValidationApiConverter';

export function getPortButtonText(props: INumberPortingBannerProps): string {
  const { checkoutTranslation, checkoutConfiguration } = props;
  const { mns, mnp } = checkoutConfiguration;
  const { personalInfo: translation } = checkoutTranslation;

  return mns.show
    ? mnp.show
      ? translation.changeOrPortNumberText
      : translation.changeNumber
    : mnp.show
    ? translation.portNumber
    : '';
}

export function getMigrationText(props: INumberPortingBannerProps): string {
  const { checkoutTranslation, migrationType } = props;
  if (migrationType === MIGRATION_TYPE.END_OF_PROMOTIOM) {
    return checkoutTranslation.mnp.endOfPromotionText;
  } else if (migrationType === MIGRATION_TYPE.WITHDRAWAL) {
    return checkoutTranslation.mnp.endOfAgreementText;
  } else if (migrationType === MIGRATION_TYPE.DESIRED_DATE) {
    return props.desiredDate as string;
  }

  return '';
}
