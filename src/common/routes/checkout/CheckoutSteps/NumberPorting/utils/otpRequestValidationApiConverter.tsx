import { OPERATION_TYPE } from '@checkout/store/numberPorting/enum';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import { INumberPortingState } from '@checkout/store/numberPorting/types';

export function otpRequestDataConverter(
  phonenumber: string
): numberPortTypesApi.PHONE_POST.IRequest {
  return {
    otpContext: OPERATION_TYPE.MNP.toLowerCase(),
    action: OPERATION_TYPE.SEND,
    mobileNumber: phonenumber
  };
}

export function otpValidateDataConverter(
  numberPorting: INumberPortingState,
  otp: string
): numberPortTypesApi.PHONE_POST.IRequest {
  return {
    otpContext: OPERATION_TYPE.MNP.toLowerCase(),
    action: OPERATION_TYPE.VALIDATE,
    mobileNumber: numberPorting.phoneNumber,
    nonce: numberPorting.nonce,
    otp // @todo apply encryption on otp
  };

  // return encrypt(otp).then((hashedOtp: string) => {
  //   return {
  //     otpContext: OPERATION_TYPE.MNP,
  //     action: OPERATION_TYPE.VALIDATE,
  //     mobileNumber: numberPorting.phoneNumber,
  //     nonce: numberPorting.nonce,
  //     otp : hashedOtp // @todo apply encryption on otp
  //   };
  // };
  // }
}
