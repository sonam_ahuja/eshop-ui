import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import FloatLabelInputCardNumber, {
  ICustomProps as IFloatLabelInputCardNumberProps
} from '.';

describe('<Payment AllPaymentMode />', () => {
  test('should render properly', () => {
    const props: IFloatLabelInputCardNumberProps = {
      brand: 'visa'
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FloatLabelInputCardNumber {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('render without brand', () => {
    const props: IFloatLabelInputCardNumberProps = {};
    const component = mount(
      <ThemeProvider theme={{}}>
        <FloatLabelInputCardNumber {...props} />
      </ThemeProvider>
    );
    expect(component.find('FloatLabelInputCardNumber').props()).toEqual({});
  });

  test('render props with brand americanexpress', () => {
    const props: IFloatLabelInputCardNumberProps = {
      brand: 'americanexpress'
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FloatLabelInputCardNumber {...props} />
      </ThemeProvider>
    );
    expect(component.find('FloatLabelInputCardNumber').props()).toEqual({
      brand: 'americanexpress'
    });
  });

  // tslint:disable-next-line:no-identical-functions
  test('render props with brand maestro', () => {
    const props: IFloatLabelInputCardNumberProps = {
      brand: 'maestro'
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FloatLabelInputCardNumber {...props} />
      </ThemeProvider>
    );
    expect(component.find('FloatLabelInputCardNumber').props()).toEqual({
      brand: 'maestro'
    });
  });

  // tslint:disable-next-line:no-identical-functions
  test('render props with brand mastercard', () => {
    const props: IFloatLabelInputCardNumberProps = {
      brand: 'mastercard'
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FloatLabelInputCardNumber {...props} />
      </ThemeProvider>
    );
    expect(component.find('FloatLabelInputCardNumber').props()).toEqual({
      brand: 'mastercard'
    });
  });
});
