import React from 'react';
import { IProps } from 'dt-components/lib/es/components/atoms/float-label-input/types';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import {
  // FloatLabelInput,
  Icon,
  MaskInput
  //  MaskInput
} from 'dt-components';

import { StyledFloatLabelInputCardNumber } from './styles';

export interface ICustomProps extends IProps {
  brand?: string;
}
export type cardTypes = 'americanexpress' | 'maestro' | 'mastercard' | 'visa';

const FloatLabelInputCardNumber = (props: ICustomProps) => {
  const {
    brand,
    id,
    label,
    disabled,
    autoComplete,
    value,
    onChange,
    onFocus,
    onBlur,
    error,
    errorMessage
  } = props;

  const cardIconNameMap = (typeOfCard: string): string => {
    switch (typeOfCard) {
      case 'americanexpress':
        return 'ec-americanexpress';
      case 'maestro':
        return 'ec-maestro';
      case 'mastercard':
        return 'ec-mastercard';
      case 'visa':
        return 'ec-visa';
      default:
        return 'ec-solid-credit-card';
    }
  };

  const cardIconName = cardIconNameMap(brand as string);

  const iconClasses = ['cardicon', brand ? brand : ''].join(' ');

  return (
    <StyledFloatLabelInputCardNumber>
      <MaskInput
        guide={false}
        mask={[
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          /\d/,
          /\d/
        ]}
        id={id}
        label={label}
        autoComplete={autoComplete}
        disabled={disabled}
        value={value}
        // type='number'
        onChange={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        error={error}
        errorMessage={errorMessage}
      />
      {/* <MaskInput
        id={id}
        label={label}
        format={'#### #### #### ####'}
        disabled={disabled}
        value={value as number}
        onChange={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        patternError={errorMessage}
      /> */}
      <Icon
        name={cardIconName as iconNamesType}
        size='small'
        className={iconClasses}
      />
    </StyledFloatLabelInputCardNumber>
  );
};

export default FloatLabelInputCardNumber;
