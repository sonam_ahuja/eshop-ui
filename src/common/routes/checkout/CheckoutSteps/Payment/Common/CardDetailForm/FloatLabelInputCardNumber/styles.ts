import styled from 'styled-components';

export const StyledFloatLabelInputCardNumber = styled.div`
  position: relative;

  input {
    padding-right: 1.5rem;
  }

  .cardicon {
    position: absolute;
    right: 0;
    bottom: 1.25rem;
  }

  i.visa {
    path {
      fill: #1a1f71;
    }
    path:nth-child(2) {
      fill: none;
    }
  }
`;
