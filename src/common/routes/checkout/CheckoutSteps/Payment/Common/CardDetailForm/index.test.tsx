import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import addNewCardDetails from '@checkout/store/payment/state';
import appState from '@store/states/app';
import CardDetailFormComponent, {
  CardDetailForm,
  IProps as IMonthlyFormProps
} from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm';
import { StyledCardDetailForm } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/styles';
import { cardDetailsState } from '@src/common/mock-api/checkout/payment';
describe('<CardDetailForm />', () => {
  const props: IMonthlyFormProps = {
    configuration: appState().configuration,
    translation: appState().translation,
    setCardDetails: jest.fn(),
    addNewCardDetails: addNewCardDetails().addNewCard,
    resetCardDetails: jest.fn(),
    setActiveCardField: jest.fn(),
    useSameCardBool: true,
    buttonLabel: '',
    braintreeClientId: '',
    savePaymentMethod: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CardDetailFormComponent {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('StyledCardDetailForm, should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StyledCardDetailForm />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('action trigger', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CardDetailForm {...props} />
      </ThemeProvider>
    );

    (component
      .find('CardDetailForm')
      .instance() as CardDetailForm).setCardDetailsWithDelay({
      cardNumber: {
        value: '',
        isValid: true,
        validationMessage: 'Card Number must be of 16 characters.',
        isDirty: false
      },
      nameOnCard: {
        value: '',
        isValid: true,
        // tslint:disable-next-line:no-duplicate-string
        validationMessage: 'Cannot be blank.',
        isDirty: false
      },
      expiryDate: {
        value: '',
        isValid: true,
        validationMessage: 'Cannot be blank.',
        isDirty: false
      },
      securityCode: {
        value: '',
        isValid: true,
        validationMessage: 'Cannot be blank.',
        isDirty: false
      },
      cardType: '',
      brand: '',
      showForm: true
    });
    (component
      .find('CardDetailForm')
      .instance() as CardDetailForm).setCardDetailsWithDelay(cardDetailsState);
  });
});
