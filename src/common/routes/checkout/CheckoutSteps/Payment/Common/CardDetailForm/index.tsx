import { activeFieldTypes, IAddNewCard } from '@checkout/store/payment/types';
import React, { Component, ReactNode } from 'react';
import { Utils } from 'dt-components';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ITranslationState } from '@src/common/store/types/translation';
import braintree from 'braintree-web';
import { logError } from '@common/utils';

import { ICardState, INVALID_FIELD_KEYS, INVALID_FIELD_MAP } from './types';
import { StyledCardDetailForm } from './styles';

export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  addNewCardDetails: IAddNewCard;
  useSameCardBool?: boolean;
  buttonLabel: string;
  braintreeClientId: string;
  setCardDetails(data: string): void;
  resetCardDetails(data: ICardState): void;
  setActiveCardField(data: activeFieldTypes): void;
  savePaymentMethod(): void;
}

export class CardDetailForm extends Component<IProps, ICardState> {
  constructor(props: IProps) {
    super(props);

    const {
      cardNumberValidationMsg,
      cvvValidationMsg,
      expirationDateValidationMsg,
      cardHolderNameValidationMsg
    } = this.props.translation.cart.checkout.paymentInfo.creditCardForm;

    this.state = {
      showForm: false,
      cardNumber: {
        value: '',
        isValid: true,
        validationMessage: cardNumberValidationMsg,
        isDirty: false
      },
      nameOnCard: {
        value: '',
        isValid: true,
        validationMessage: cardHolderNameValidationMsg,
        isDirty: false
      },
      expiryDate: {
        value: '',
        isValid: true,
        validationMessage: expirationDateValidationMsg,
        isDirty: false
      },
      securityCode: {
        value: '',
        isValid: true,
        validationMessage: cvvValidationMsg,
        isDirty: false
      },
      cardType: '',
      brand: ''
    };

    this.setCardDetailsWithDelay = Utils.debounce(
      this.setCardDetailsWithDelay,
      300
    ).bind(this);

    this.checkValidFormFields = this.checkValidFormFields.bind(this);
    this.removeValidationMessage = this.removeValidationMessage.bind(this);
  }

  componentWillMount(): void {
    const authorization = this.props.braintreeClientId;

    braintree.client.create(
      {
        authorization
      },
      // tslint:disable: no-any
      (err: any, clientInstance: any): void => {
        if (err) {
          logError(err);

          return;
        }
        this.createHostedFields(clientInstance);
      }
    );
  }

  // tslint:disable-next-line:cognitive-complexity
  createHostedFields(clientInstance: any): void {
    const {
      cardNumberPlaceHolder,
      cvvPlaceHolder,
      expirationDatePlaceHolder
    } = this.props.translation.cart.checkout.paymentInfo.creditCardForm;

    this.setState({ showForm: true }, () => {
      const form = document.querySelector('#cardForm');
      braintree.hostedFields.create(
        {
          client: clientInstance,
          styles: {
            input: {
              'font-size': '16px',
              'font-weight': 'bold',
              'font-style': 'normal',
              'font-stretch': 'normal',
              'line-height': '1.5',
              'letter-spacing': 'normal'
            },
            ':focus': {},
            '.valid': {}
          },
          fields: {
            number: {
              selector: '#card-number',
              placeholder: cardNumberPlaceHolder
            },
            cvv: {
              selector: '#cvv',
              placeholder: cvvPlaceHolder
            },
            expirationDate: {
              selector: '#expiration-date',
              placeholder: expirationDatePlaceHolder
            }
          }
        },
        (_error: any, hostedFieldsInstance: any): void => {
          if (hostedFieldsInstance) {
            hostedFieldsInstance.on('focus', (event: any) => {
              this.props.setActiveCardField(
                event.emittedBy as activeFieldTypes
              );
            });

            hostedFieldsInstance.on('validityChange', (event: any) => {
              const hostedFieldState = hostedFieldsInstance.getState();
              const emittedBy = event.emittedBy;
              const fieldElement = hostedFieldState.fields[emittedBy].container;
              if (
                fieldElement.lastElementChild.className.includes('error') &&
                event.fields[emittedBy].isValid
              ) {
                fieldElement.removeChild(fieldElement.lastElementChild);
              }
            });
          }

          hostedFieldsInstance.on('blur', () => {
            this.props.setActiveCardField('');
          });

          const teardown = (event: any) => {
            event.preventDefault();

            if (hostedFieldsInstance) {
              hostedFieldsInstance.tokenize(
                {
                  cardholderName: event.target.cardholderName.value
                },
                (tokenizeErr: any, payload: any) => {
                  this.checkValidFormFields(hostedFieldsInstance.getState());

                  if (
                    tokenizeErr &&
                    tokenizeErr.details &&
                    tokenizeErr.details.invalidFieldKeys &&
                    tokenizeErr.details.invalidFieldKeys.length
                  ) {
                    logError(
                      `Error in tokenize submit, and error is ${JSON.stringify(
                        tokenizeErr
                      )}`
                    );

                    return;
                  }

                  if (payload) {
                    this.props.setCardDetails(payload.nonce);
                    this.props.savePaymentMethod();
                  }
                }
              );
            }
          };

          (form as any).addEventListener('submit', teardown, false);
        }
      );
    });
  }

  checkValidFormFields(hostedFieldState: any): void {
    Object.keys(hostedFieldState.fields).forEach(field => {
      const invalidElement = hostedFieldState.fields[field].container;
      let stateField = '';

      switch (field) {
        case INVALID_FIELD_KEYS.CARD_NUMBER:
          stateField = INVALID_FIELD_MAP[INVALID_FIELD_KEYS.CARD_NUMBER];
          break;
        case INVALID_FIELD_KEYS.CVV:
          stateField = INVALID_FIELD_MAP[INVALID_FIELD_KEYS.CVV];
          break;
        case INVALID_FIELD_KEYS.EXPIRY_DATE:
          stateField = INVALID_FIELD_MAP[INVALID_FIELD_KEYS.EXPIRY_DATE];
          break;
        default:
          break;
      }
      this.removeValidationMessage(
        invalidElement,
        stateField,
        hostedFieldState.fields[field].isValid
      );
    });
  }

  removeValidationMessage(
    fieldElement: any,
    field: string,
    isValid: boolean
  ): void {
    if (isValid) {
      if (fieldElement.lastElementChild.className.includes('error')) {
        fieldElement.removeChild(fieldElement.lastElementChild);
      }
    } else {
      if (!fieldElement.lastElementChild.className.includes('error')) {
        fieldElement.insertAdjacentHTML(
          'beforeend',
          `<span class=error>${this.state[field].validationMessage}</span>`
        );
      }
    }
  }

  setCardDetailsWithDelay(state: ICardState): void {
    this.props.setCardDetails(state.cardNumber.value);
  }

  render(): ReactNode {
    const { buttonLabel } = this.props;

    return (
      <>
        {
          <StyledCardDetailForm
            dangerouslySetInnerHTML={{
              __html: `<div class="demo-frame">
        <form id="cardForm" >
          <div class='inputWrap cardNumber'>
            <label class="hosted-fields--label" for="card-number">Card Number</label>
            <div id="card-number" class="hosted-field"></div>

          </div>
          <div class='inputWrap expirationDate'>
            <label class="hosted-fields--label" for="expiration-date">Expiration Date</label>
            <div id="expiration-date" class="hosted-field"></div>

          </div>
          <div class='inputWrap cardHolderName'>
            <label for="cardholder-name">Cardholder Name</label>
            <input type='text' id="cardholder-name" name="cardholderName" placeholder="Cardholder Name"/>
          </div>
          <div class='inputWrap cvv'>
            <label class="hosted-fields--label" for="cvv">CVV</label>
            <div id="cvv" class="hosted-field"></div>

          </div>

          <div class='buttonWrap'>
            <div class="button-container formButton">
            <input type="submit" class="button button--small button--green" value="${buttonLabel}" id="submit"/>
          </div>
        </form>
      </div>`
            }}
          />
        }
      </>
    );
  }
}

// tslint:disable-next-line:max-file-line-count
export default CardDetailForm;
