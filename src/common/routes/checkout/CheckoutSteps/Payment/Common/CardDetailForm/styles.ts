import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledCardDetailForm = styled.div`
  margin-top: 2.25rem;
  width: 100%;
  display: flex;
  flex-direction: column;

  .error {
    position: absolute;
    margin-top: 0.2rem;
    font-size: 0.625rem;
    color: #ff9a1f;
    line-height: 1;
    font-weight: bold;
  }

  form {
    display: flex;
    flex-wrap: wrap;
  }
  form > div {
    margin-bottom: auto !important;
  }

  /* inputWrap start */
  .inputWrap {
    margin-bottom: 1.5rem !important;
    width: 100%;

    &.cardNumber {
      order: 0;
    }
    &.cardHolderName {
      order: 1;
    }
    &.expirationDate {
      order: 2;
      width: 50%;
      padding-right: 0.5rem;
    }
    &.cvv {
      order: 3;
      width: 50%;
      padding-left: 0.5rem;
    }
  }
  .buttonWrap {
    order: 4;
  }
  /* inputWrap end */

  label {
    font-size: 0.625rem;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: ${colors.mediumGray};
    display: block;
  }

  input[type='text'] {
    border: none;
    box-shadow: none;
    border-radius: 0;
    width: 100%;
    border-bottom: 1px solid ${colors.silverGray};
    -webkit-transition: border-color 160ms;
    transition: border-color 160ms;
    height: 1.75rem;
    background: none;
    padding-left: 0;

    font-size: 16px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    outline: none;
  }

  .buttonWrap {
    margin-top: 1rem;
    width: 100%;
    .button {
      height: 3rem;
      width: 100%;
      align-items: center;
      justify-content: center;
      background: ${colors.magenta};
      box-shadow: none;
      border: none;
      font-size: 16px;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.5;
      letter-spacing: -0.1px;
      text-align: center;
      color: ${colors.white};
      outline: none;
      padding: 0 1.5rem;
      -webkit-appearance: none;
      appearance: none;
      border-radius: 0;
      /* text-transform: capitalize; */
      &.disabled {
        color: ${colors.mediumGray};
        background: ${colors.warmGray};
      }
    }
  }

  #card-number,
  #expiration-date,
  #cvv {
    border-bottom: 1px solid ${colors.silverGray};
    -webkit-transition: border-color 160ms;
    transition: border-color 160ms;
    height: 1.75rem;
  }

  #card-number.braintree-hosted-fields-focused,
  #expiration-date.braintree-hosted-fields-focused,
  #cvv.braintree-hosted-fields-focused {
    border-color: ${colors.magenta};
  }

  #card-number.braintree-hosted-fields-invalid,
  #expiration-date.braintree-hosted-fields-invalid,
  #cvv.braintree-hosted-fields-invalid {
    border-color: ${colors.orange};
  }

  #card-number.braintree-hosted-fields-valid {
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    form {
      display: flex;
      flex-wrap: wrap;

      /* inputWrap start */
      .inputWrap {
        margin-bottom: 1.6rem !important;
        &.cardNumber,
        &.cardHolderName {
          order: 0;
          width: 66%;
          padding-right: 0.375rem;
        }
        &.expirationDate,
        &.cvv {
          order: 0;
          width: 33%;
          padding-left: 0.375rem;
        }
      }
      .buttonWrap {
        text-align: right;
        .formButton {
          display: inline-block;
        }
        .button {
          font-size: 0.875rem;
          line-height: 1.25rem;
          letter-spacing: -0.08px;
          height: 2.5rem;
          cursor: pointer;
          border: 1px solid transparent;
        }
      }
      /* inputWrap end */
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    form {
      .buttonWrap {
        .button {
          height: 3rem;
        }
      }
    }
  }
`;
