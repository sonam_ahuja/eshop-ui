export interface IValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
  isDirty: boolean;
}

export interface ICardState {
  cardNumber: IValue;
  nameOnCard: IValue;
  expiryDate: IValue;
  securityCode: IValue;
  cardType: string;
  brand: string;
  showForm: boolean;
}

export enum INVALID_FIELD_KEYS {
  CARD_NUMBER = 'number',
  CVV = 'cvv',
  EXPIRY_DATE = 'expirationDate'
}

export enum INVALID_FIELD_MAP {
  number = 'cardNumber',
  cvv = 'securityCode',
  expirationDate = 'expiryDate'
}
