import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import CreditDebitCard, { IProps as ICreditDebitCardProps } from '.';

describe('<CreditDebitCard Payment Content />', () => {
  const newPayment = { ...appState().checkout.payment };
  newPayment.upFront.cardDetails = [
    {
      cardNumber: '',
      isDefault: false,
      cardType: '',
      expiryDate: '',
      securityCode: '',
      nameOnCard: '',
      id: '0',
      isCardSaved: false,
      lastFourDigits: 'xxxxxxxxxxxx',
      brand: ''
    }
  ];
  const props: ICreditDebitCardProps = {
    configuration: appState().configuration,
    translation: appState().translation,
    checkout: appState().checkout.checkout,
    paymentInfo: newPayment,
    setSaveThisCard: jest.fn(),
    setCardDetails: jest.fn(),
    resetCardDetails: jest.fn(),
    useSameCard: jest.fn(),
    editUpFront: jest.fn(),
    addNewCardDetails: appState().checkout.payment.addNewCard,
    changeSelectedCard: jest.fn(),
    setActiveCardField: jest.fn(),
    savePaymentMethod: jest.fn(),
    buttonLabel: ''
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CreditDebitCard {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test(' monthlyPaymentCheck should render properly', () => {
    const newProps = { ...props };
    newProps.paymentInfo.isUpfront = true;
    newProps.checkout.isUpfrontPrice = false;

    const component = mount(
      <ThemeProvider theme={{}}>
        <CreditDebitCard {...props} />
      </ThemeProvider>
    );

    (component
      .find('CreditDebitCard')
      .instance() as CreditDebitCard).monthlyPaymentCheck(true);

    (component
      .find('CreditDebitCard')
      .instance() as CreditDebitCard).upfrontPaymentCheck(true);
  });
});
