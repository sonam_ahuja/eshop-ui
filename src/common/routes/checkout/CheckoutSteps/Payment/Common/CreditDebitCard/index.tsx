import { ICheckoutState } from '@checkout/store/types';
import Monthly from '@checkout/CheckoutSteps/Payment/Common/Monthly';
import React, { Component, ReactNode } from 'react';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ITranslationState } from '@src/common/store/types/translation';
import Upfront from '@checkout/CheckoutSteps/Payment/Common/Upfront';
import {
  activeFieldTypes,
  IAddNewCard,
  ICardDetails,
  IPaymentState
} from '@checkout/store/payment/types';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/types';

import { StyledCreditDebitCard } from './styles';

export interface IProps {
  configuration: IConfigurationState;
  translation: ITranslationState;
  checkout: ICheckoutState;
  paymentInfo: IPaymentState;
  addNewCardDetails: IAddNewCard;
  buttonLabel: string;
  setSaveThisCard(): void;
  setCardDetails(data: string): void;
  resetCardDetails(data: ICardState): void;
  useSameCard(): void;
  editUpFront(): void;
  changeSelectedCard(data: ICardDetails): void;
  setActiveCardField(data: activeFieldTypes): void;
  savePaymentMethod(): void;
}

export class CreditDebitCard extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.upfrontPaymentCheck = this.upfrontPaymentCheck.bind(this);
    this.monthlyPaymentCheck = this.monthlyPaymentCheck.bind(this);
  }

  monthlyPaymentCheck(isMonthlyExist: boolean): ReactNode {
    const {
      checkout,
      translation,
      paymentInfo,
      configuration,
      setCardDetails,
      useSameCard,
      resetCardDetails,
      addNewCardDetails,
      changeSelectedCard,
      setActiveCardField,
      buttonLabel,
      savePaymentMethod
    } = this.props;

    if (isMonthlyExist) {
      if (!(paymentInfo.isUpfront && checkout.isUpfrontPrice)) {
        return (
          <Monthly
            configuration={configuration}
            translation={translation}
            addNewCardDetails={addNewCardDetails}
            monthlyPaymentInfo={paymentInfo.monthly}
            setCardDetails={setCardDetails}
            resetCardDetails={resetCardDetails}
            useSameCard={useSameCard}
            setActiveCardField={setActiveCardField}
            showUseSameCard={paymentInfo.showUseSameCard}
            changeSelectedCard={changeSelectedCard}
            buttonLabel={buttonLabel}
            savePaymentMethod={savePaymentMethod}
          />
        );
      } else {
        return null;
      }
    }

    return null;
  }

  upfrontPaymentCheck(isUpfrontExist: boolean): ReactNode {
    const {
      translation,
      paymentInfo,
      configuration,
      setCardDetails,
      resetCardDetails,
      addNewCardDetails,
      editUpFront,
      changeSelectedCard,
      setActiveCardField,
      buttonLabel,
      savePaymentMethod
    } = this.props;

    if (isUpfrontExist && paymentInfo.isUpfront) {
      return (
        <Upfront
          upfrontPaymentInfo={paymentInfo.upFront}
          configuration={configuration}
          translation={translation}
          setCardDetails={setCardDetails}
          addNewCardDetails={addNewCardDetails}
          resetCardDetails={resetCardDetails}
          isUpfront={paymentInfo.isUpfront}
          editUpFront={editUpFront}
          changeSelectedCard={changeSelectedCard}
          setActiveCardField={setActiveCardField}
          buttonLabel={buttonLabel}
          savePaymentMethod={savePaymentMethod}
        />
      );
    }

    return null;
  }

  render(): ReactNode {
    const { checkout } = this.props;

    return (
      <StyledCreditDebitCard>
        {/* <MobileNav /> */}

        {this.upfrontPaymentCheck(checkout.isUpfrontPrice)}

        {this.monthlyPaymentCheck(checkout.isMonthlyPrice)}
      </StyledCreditDebitCard>
    );
  }
}

export default CreditDebitCard;
