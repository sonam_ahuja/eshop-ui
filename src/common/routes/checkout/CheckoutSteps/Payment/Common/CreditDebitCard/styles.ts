import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledCreditDebitCard = styled.div`
  margin-bottom: 1.75rem;
  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 1.5rem;
  }
`;
