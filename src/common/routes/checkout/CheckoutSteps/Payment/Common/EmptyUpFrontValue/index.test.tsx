import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import EmptyUpFrontValue, { IProps } from '.';

describe('<Checkout Payment EmptyUpFrontValue />', () => {
  const props: IProps = {
    paymentTranslation: appState().translation.cart.checkout.paymentInfo,
    forUpfront: true
  };

  test('should render properly when upfront is true', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <EmptyUpFrontValue {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when upfront is false', () => {
    const newProps = { ...props };
    newProps.forUpfront = false;
    const component = mount(
      <ThemeProvider theme={{}}>
        <EmptyUpFrontValue {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
