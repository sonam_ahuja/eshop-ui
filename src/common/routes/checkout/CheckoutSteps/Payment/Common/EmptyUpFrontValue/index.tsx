import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Divider, Section, Title } from 'dt-components';
import { IPaymentInfoTranslation } from '@src/common/store/types/translation';

const NoValueWrap = styled.div`
  margin-bottom: 2.4rem;
  color: ${colors.mediumGray};
  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 0.75rem;
  }
`;

export interface IProps {
  paymentTranslation: IPaymentInfoTranslation;
  forUpfront: boolean;
}

const EmptyValue: FunctionComponent<IProps> = (props: IProps) => {
  const { paymentTranslation, forUpfront } = props;

  return (
    <NoValueWrap>
      {!forUpfront && (
        <>
          <Section>{paymentTranslation.monthly}</Section>
          <Divider
            dashed={true}
            align='left'
            orientation='horizontal'
            className='divider'
          />
        </>
      )}

      <Title size='xsmall' className='titleheading' weight='bold'>
        {forUpfront
          ? paymentTranslation.noUpfrontText
          : paymentTranslation.noMonthlyText}
      </Title>
    </NoValueWrap>
  );
};

export default EmptyValue;
