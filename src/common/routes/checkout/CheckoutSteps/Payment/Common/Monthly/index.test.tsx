import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { RootState } from '@src/common/store/reducers';

import Monthly, { IProps as ISaveCardProps } from '.';

describe('<Checkout Payment Monthly />', () => {
  const props: ISaveCardProps = {
    monthlyPaymentInfo: appState().checkout.payment.monthly,
    configuration: appState().configuration,
    translation: appState().translation,
    setCardDetails: jest.fn(),
    resetCardDetails: jest.fn(),
    useSameCard: jest.fn(),
    addNewCardDetails: appState().checkout.payment.addNewCard,
    setActiveCardField: jest.fn(),
    changeSelectedCard: jest.fn(),
    showUseSameCard: false,
    buttonLabel: '',
    savePaymentMethod: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Monthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with same card ', () => {
    const newProps = { ...props, showUseSameCard: true };
    const cardDetails = [
      {
        cardNumber: '',
        isDefault: true,
        cardType: '',
        expiryDate: '',
        securityCode: '',
        nameOnCard: '',
        id: '',
        isCardSaved: true,
        lastFourDigits: '',
        brand: ''
      }
    ];
    newProps.monthlyPaymentInfo.cardDetails = cardDetails;
    newProps.monthlyPaymentInfo.addingNewCard = true;

    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    initStateValue.checkout.checkout.isUpfrontPrice = true;

    const store = mockStore(initStateValue);

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Monthly {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
