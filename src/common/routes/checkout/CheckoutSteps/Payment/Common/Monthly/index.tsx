import CardDetailForm from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/';
import {
  activeFieldTypes,
  IAddNewCard,
  ICardDetails,
  IMonthly
} from '@checkout/store/payment/types';
import { Divider, Section } from 'dt-components';
import React, { FunctionComponent } from 'react';
import { ITranslationState } from '@src/common/store/types/translation';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm//types';
import PaymentCards from '@checkout/CheckoutSteps/Payment/Common/PaymentCards';
import store from '@src/common/store';

import { StyledSwitch } from '../styles';

import { StyledMonthly } from './styles';

export interface IProps {
  monthlyPaymentInfo: IMonthly;
  configuration: IConfigurationState;
  translation: ITranslationState;
  addNewCardDetails: IAddNewCard;
  showUseSameCard: boolean;
  buttonLabel: string;
  setCardDetails(data: string): void;
  resetCardDetails(data: ICardState): void;
  useSameCard(): void;
  setActiveCardField(data: activeFieldTypes): void;
  changeSelectedCard(data: ICardDetails): void;
  savePaymentMethod(): void;
}

const Monthly: FunctionComponent<IProps> = (props: IProps) => {
  const {
    configuration,
    translation,
    setCardDetails,
    monthlyPaymentInfo,
    resetCardDetails,
    useSameCard,
    addNewCardDetails,
    setActiveCardField,
    showUseSameCard,
    changeSelectedCard,
    buttonLabel,
    savePaymentMethod
  } = props;
  const isUpfrontPrice = store.getState().checkout.checkout.isUpfrontPrice;

  return (
    <StyledMonthly>
      <div className='heading'>
        <div className='textWrap'>
          <Section size='small' transform='uppercase'>
            {translation.cart.checkout.monthly}
          </Section>
          {showUseSameCard && isUpfrontPrice && (
            <StyledSwitch
              disabled={false}
              onLabel={translation.cart.checkout.paymentInfo.useSameCard}
              offLabel={translation.cart.checkout.paymentInfo.useSameCard}
              on={monthlyPaymentInfo.useSameCard}
              onToggle={useSameCard}
            />
          )}
        </div>
        <Divider dashed orientation='horizontal' />
      </div>

      {monthlyPaymentInfo.cardDetails &&
        monthlyPaymentInfo.cardDetails.length > 0 && (
          <PaymentCards
            cardDetails={monthlyPaymentInfo.cardDetails}
            changeSelectedCard={changeSelectedCard}
            paymentTranslation={translation.cart.checkout.paymentInfo}
          />
        )}

      {monthlyPaymentInfo.addingNewCard && (
        <CardDetailForm
          configuration={configuration}
          translation={translation}
          addNewCardDetails={addNewCardDetails}
          setCardDetails={setCardDetails}
          useSameCardBool={monthlyPaymentInfo.useSameCard}
          resetCardDetails={resetCardDetails}
          setActiveCardField={setActiveCardField}
          buttonLabel={buttonLabel}
          savePaymentMethod={savePaymentMethod}
          braintreeClientId={monthlyPaymentInfo.id as string}
        />
      )}
    </StyledMonthly>
  );
};

export default Monthly;
