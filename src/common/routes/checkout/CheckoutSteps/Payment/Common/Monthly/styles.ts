import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledMonthly = styled.div`
  .heading {
    color: ${colors.lightGray};
    .dt_switch {
      margin: 0;
      margin-bottom: 0.125rem;
    }
    .dt_divider {
      margin: 0;
      margin-top: 0.5rem;
      margin-bottom: 1.78125rem;
    }
  }

  form {
    padding: 0;
    padding-bottom: 1rem;
    > div {
      margin-bottom: -1.40625rem !important;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .dt_divider {
      margin-bottom: 1.75rem;
    }
  }
`;
