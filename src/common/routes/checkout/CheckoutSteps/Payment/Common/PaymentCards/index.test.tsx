import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import Card from '.';

describe('<Payment Card />', () => {
  const card = {
    brand: 'dummy',
    cardNumber: '',
    cardType: 'credit',
    expiryDate: '',
    id: '0',
    isCardSaved: false,
    isDefault: true,
    lastFourDigits: '',
    nameOnCard: '',
    securityCode: ''
  };
  const props = {
    cardDetails: appState().checkout.payment.upFront.cardDetails,
    paymentTranslation: appState().translation.cart.checkout.paymentInfo,
    changeSelectedCard: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('brandType method should return brand type', () => {
    const copyComponentProps = { ...props };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...copyComponentProps} />
      </ThemeProvider>
    );

    (component.find('Card').instance() as Card).brandType('Visa');
    (component.find('Card').instance() as Card).brandType('masterCard');
    (component.find('Card').instance() as Card).brandType('dummy');
    (component.find('Card').instance() as Card).brandType('Maestro');
    (component.find('Card').instance() as Card).brandType('none');
  });

  test('logoType method should return logo type', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.cardDetails = [card];
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...copyComponentProps} />
      </ThemeProvider>
    );

    (component.find('Card').instance() as Card).logoType('Visa');
    (component.find('Card').instance() as Card).logoType('masterCard');
    (component.find('Card').instance() as Card).logoType('dummy');
    (component.find('Card').instance() as Card).logoType('Maestro');
    (component.find('Card').instance() as Card).logoType('none');
    (component.find('Card').instance() as Card).onClickHandler(card)();
  });
});
