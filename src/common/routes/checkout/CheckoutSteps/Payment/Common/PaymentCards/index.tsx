import React, { Component, ReactNode } from 'react';
import { PaymentCard } from 'dt-components';
import { ICardDetails } from '@checkout/store/payment/types';
import { IPaymentInfoTranslation } from '@src/common/store/types/translation';

import { StyledPaymentCards } from './styles';

interface IProps {
  cardDetails: ICardDetails[];
  paymentTranslation: IPaymentInfoTranslation;
  changeSelectedCard(data: ICardDetails): void;
}

type BrandTypes = 'dummy' | 'masterCard' | 'visa' | undefined;
type LogoTypes = 'ec-visa' | 'ec-mastercard' | undefined;

export class Card extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.brandType = this.brandType.bind(this);
    this.logoType = this.logoType.bind(this);
    this.renderCardItems = this.renderCardItems.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  brandType(type: string): BrandTypes {
    if (type.toLowerCase() === 'Visa'.toLowerCase()) {
      return 'visa';
    } else if (type.toLowerCase() === 'masterCard'.toLowerCase()) {
      return 'masterCard';
    } else if (type.toLowerCase() === 'dummy'.toLowerCase()) {
      return 'dummy';
    } else if (type.toLowerCase() === 'Maestro'.toLowerCase()) {
      return 'masterCard';
    } else {
      return undefined;
    }
  }

  logoType(type: string): LogoTypes {
    if (type.toLowerCase() === 'Visa'.toLowerCase()) {
      return 'ec-visa';
    } else if (type.toLowerCase() === 'masterCard'.toLowerCase()) {
      return 'ec-masterCard' as LogoTypes;
    } else if (type.toLowerCase() === 'dummy'.toLowerCase()) {
      return 'ec-dummy' as LogoTypes;
    } else if (type.toLowerCase() === 'Maestro'.toLowerCase()) {
      return 'ec-masterCard' as LogoTypes;
    } else {
      return 'ec-visa';
    }
  }

  onClickHandler(card: ICardDetails): () => void {
    return () => {
      this.props.changeSelectedCard(card);
    };
  }

  renderCardItems(cardDetails: ICardDetails[]): ReactNode {
    const cardItems: ReactNode[] = [];
    const { paymentTranslation } = this.props;

    cardDetails.forEach((item: ICardDetails, index: number) => {
      cardItems.push(
        <PaymentCard
          key={index}
          lastFourDigits={item.lastFourDigits}
          type={this.brandType(item.brand)}
          text={item.brand === 'dummy' ? paymentTranslation.addNewCard : paymentTranslation.cardEnding}
          active={item.isDefault}
          onClick={this.onClickHandler(item)}
        />
      );
    });

    return cardItems;
  }

  render(): ReactNode {
    const { cardDetails } = this.props;

    return (
      <StyledPaymentCards>
        <div className='multipleCardWrap'>
          <div className='inner'>{this.renderCardItems(cardDetails)}</div>
        </div>
      </StyledPaymentCards>
    );
  }
}

export default Card;
