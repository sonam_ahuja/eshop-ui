import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPaymentCards = styled.div`
/* display: flex;
  flex-direction: row;
  justify-content: flex-start;
  margin-top: 1.775rem;
  flex: 1;

  flex-wrap: nowrap;
  overflow: auto;
  margin: 0 -1.25rem;
  padding: 0 1.25rem;
  -webkit-overflow-scrolling: touch;

  .inner {
    display: flex;
    padding-right: 1rem;
  }

  .card {
    min-width: 190px;
    max-width: 190px;
    margin: 0 0.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin: 0 -2.25rem;
    padding: 0 2.25rem;
    .inner {
      width: 100px;
    }
  } */

  position: relative;
  height: 120px;

  .multipleCardWrap {
    position: absolute;
    display: flex;
    overflow: auto;
    left: -1.25rem;
    right: -1.25rem;
    padding: 0 1.25rem;
    -webkit-overflow-scrolling: touch;

    &::-webkit-scrollbar {
      display: none;
      width: 0 !important ;
    }

    .inner {
      display: flex;
      justify-content: space-between;
      padding-right: 1.25rem;
      margin: 0 -0.25rem;

      .dt_paymentCard {
        min-width: 190px;
        max-width: 190px;
        margin: 0 0.25rem;
        .dt_icon{
          font-size: 2.5rem;
        }
        .dt_section{
          font-size:0.9375rem;
        }
      }
    }


  }

  @media (min-width: ${breakpoints.desktop}px) {
    .multipleCardWrap {
      left: -2.25rem;
      right: -2.25rem;
      padding: 0 2.25rem;
    }
    .inner {
      .dt_paymentCard {
        .dt_icon{
          font-size: 2rem!important;
        }
        .dt_section{
          font-size:0.75rem;
        }
      }
    }
  }
`;
