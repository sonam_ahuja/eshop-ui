import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import Upfront, { IProps as IUpfrontProps } from '.';

describe('<Checkout Payment />', () => {
  const props: IUpfrontProps = {
    upfrontPaymentInfo: appState().checkout.payment.upFront,
    configuration: appState().configuration,
    translation: appState().translation,
    setCardDetails: jest.fn(),
    resetCardDetails: jest.fn(),
    isUpfront: true,
    addNewCardDetails: appState().checkout.payment.addNewCard,
    editUpFront: jest.fn(),
    changeSelectedCard: jest.fn(),
    setActiveCardField: jest.fn(),
    buttonLabel: '',
    savePaymentMethod: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Upfront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when upfron is false', () => {
    const newProps = { ...props, isUpfront: false };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Upfront {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with card details ', () => {
    const newProps = { ...props };
    const cardDetails = [
      {
        cardNumber: '',
        isDefault: true,
        cardType: '',
        expiryDate: '',
        securityCode: '',
        nameOnCard: '',
        id: '',
        isCardSaved: true,
        lastFourDigits: '',
        brand: ''
      }
    ];
    newProps.upfrontPaymentInfo.cardDetails = cardDetails;
    newProps.upfrontPaymentInfo.addingNewCard = true;

    const component = mount(
      <ThemeProvider theme={{}}>
        <Upfront {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
