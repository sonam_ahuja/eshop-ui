import PaymentCards from '@checkout/CheckoutSteps/Payment/Common/PaymentCards';
import { Divider, Section } from 'dt-components';
import {
  activeFieldTypes,
  IAddNewCard,
  ICardDetails,
  IUpFront
} from '@checkout/store/payment/types';
import React, { FunctionComponent } from 'react';
import CardDetailForm from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm';
import { ITranslationState } from '@src/common/store/types/translation';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/types';

import { StyledUpfront } from './styles';

export interface IProps {
  upfrontPaymentInfo: IUpFront;
  configuration: IConfigurationState;
  translation: ITranslationState;
  isUpfront: boolean;
  addNewCardDetails: IAddNewCard;
  buttonLabel: string;
  setCardDetails(data: string): void;
  resetCardDetails(data: ICardState): void;
  editUpFront(): void;
  changeSelectedCard(data: ICardDetails): void;
  setActiveCardField(data: activeFieldTypes): void;
  savePaymentMethod(): void;
}

const Upfront: FunctionComponent<IProps> = (props: IProps) => {
  const {
    upfrontPaymentInfo,
    configuration,
    translation,
    setCardDetails,
    resetCardDetails,
    isUpfront,
    addNewCardDetails,
    changeSelectedCard,
    setActiveCardField,
    buttonLabel,
    savePaymentMethod
  } = props;

  return (
    <StyledUpfront>
      <div className='heading'>
        <Section size='small' transform='uppercase'>
          {translation.cart.checkout.upfront}
        </Section>
        <Divider dashed orientation='horizontal' />
      </div>

      {isUpfront &&
        upfrontPaymentInfo.cardDetails &&
        upfrontPaymentInfo.cardDetails.length > 0 && (
          <PaymentCards
            cardDetails={upfrontPaymentInfo.cardDetails}
            changeSelectedCard={changeSelectedCard}
            paymentTranslation={translation.cart.checkout.paymentInfo}
          />
        )}

      {isUpfront ? (
        upfrontPaymentInfo.addingNewCard ? (
          <CardDetailForm
            addNewCardDetails={addNewCardDetails}
            configuration={configuration}
            translation={translation}
            setCardDetails={setCardDetails}
            resetCardDetails={resetCardDetails}
            setActiveCardField={setActiveCardField}
            buttonLabel={buttonLabel}
            savePaymentMethod={savePaymentMethod}
            braintreeClientId={upfrontPaymentInfo.id as string}
          />
        ) : null
      ) : null}
    </StyledUpfront>
  );
};

export default Upfront;
