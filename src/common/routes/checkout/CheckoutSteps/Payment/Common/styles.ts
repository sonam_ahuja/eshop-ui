import styled from 'styled-components';
import { Switch } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSwitch = styled(Switch)`
  display: inline-flex;
  align-items: center;
  font-size: 0.75rem;
  line-height: 1.33;
  color: ${colors.darkGray};
  margin-bottom: 2.5rem;

  label {
    margin-left: 0.75rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    label {
      margin-left: 0.5rem;
      font-size: 0.625rem;
      line-height: 0.75rem;
      color: ${colors.darkGray};
    }
  }
`;
