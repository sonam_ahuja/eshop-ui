import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import UpfrontEditForm, { IProps as IUpfrontEditProps } from '.';
describe('<Checkout Payment UpfrontEditForm />', () => {
  const cardDetails = {
    cardNumber: '',
    isDefault: false,
    cardType: '',
    expiryDate: '',
    securityCode: '',
    nameOnCard: '',
    id: '0',
    isCardSaved: false,
    lastFourDigits: 'xxxxxxxxxxxx',
    brand: ''
  };
  const props: IUpfrontEditProps = {
    selectedCard: cardDetails,
    editUpFront: jest.fn(),
    paymentTranslation: appState().translation.cart.checkout.paymentInfo
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <UpfrontEditForm {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
