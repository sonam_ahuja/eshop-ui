import React, { FunctionComponent } from 'react';
import { IPaymentInfoTranslation } from '@src/common/store/types/translation';
import { ICardDetails } from '@checkout/store/payment/types';
import { Column, Row } from '@common/components/Grid/styles';
import { ReadOnlyForm } from '@checkout/Common/ReadOnlyForm/styles';
import KeyValue from '@src/common/components/KeyValue';

export interface IProps {
  paymentTranslation: IPaymentInfoTranslation;
  selectedCard: ICardDetails;
  editUpFront(): void;
}

const UpfrontReadOnlyForm: FunctionComponent<IProps> = (props: IProps) => {
  const { paymentTranslation, selectedCard } = props;

  return (
    <ReadOnlyForm>
      <Row>
        <Column
          className='cardNumber'
          colDesktop={4}
          colMobile={5}
          orderDesktop={1}
        >
          <KeyValue
            labelName={paymentTranslation.cardNumber}
            labelValue={selectedCard.cardNumber}
          />
        </Column>

        <Column
          className='nameOnCard'
          colDesktop={4}
          colMobile={4}
          orderDesktop={2}
        >
          <KeyValue
            labelName={paymentTranslation.nameOnCard}
            labelValue={selectedCard && selectedCard.nameOnCard}
          />
        </Column>

        <Column
          className='expiryDate'
          colMobile={3}
          colDesktop={3}
          orderDesktop={3}
        >
          <KeyValue
            labelName={paymentTranslation.expirationDate}
            labelValue={selectedCard && selectedCard.expiryDate}
          />
        </Column>
      </Row>
    </ReadOnlyForm>
  );
};

export default UpfrontReadOnlyForm;
