import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import { cardDetails, cardDetailsState } from '@mocks/checkout/payment';

import {
  IComponentProps,
  mapDispatchToProps,
  mapStateToProps,
  MonthlyPayment
} from '.';

// tslint:disable-next-line:no-big-function
describe('<MonthlyPayment />', () => {
  const props: IComponentProps = {
    paymentInfo: appState().checkout.payment,
    checkout: appState().checkout.checkout,
    configuration: appState().configuration,
    translation: appState().translation,
    activeCardField: 'number',
    setSaveThisCard: jest.fn(),
    setCardDetails: jest.fn(),
    editPaymentStepClick: jest.fn(),
    resetCardDetails: jest.fn(),
    useSameCard: jest.fn(),
    editUpFront: jest.fn(),
    fetchPaymentMethods: jest.fn(),
    changeSelectedCard: jest.fn(),
    setActiveCardField: jest.fn(),
    setPaymentStep: jest.fn(),
    savePaymentMethod: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MonthlyPayment {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when payament type is pay by link', () => {
    const newProps: IComponentProps = { ...props };
    newProps.paymentInfo.monthly.paymentTypeSelected = PAYMENT_TYPE.PAY_BY_LINK;
    const wapper = componentWrapper(newProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when payament type is pay on delivery', () => {
    const newProps: IComponentProps = { ...props };
    newProps.paymentInfo.monthly.paymentTypeSelected =
      PAYMENT_TYPE.PAY_ON_DELIVERY;
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when payament type is bank account', () => {
    const newProps: IComponentProps = { ...props };
    newProps.paymentInfo.monthly.paymentTypeSelected =
      PAYMENT_TYPE.BANK_ACCOUNT;
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when payament type is manual payments', () => {
    const newProps: IComponentProps = { ...props };
    newProps.paymentInfo.monthly.paymentTypeSelected =
      PAYMENT_TYPE.MANUAL_PAYMENTS;
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(appState())).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.setSaveThisCard();
    mapDispatch.fetchPaymentMethods();
    mapDispatch.setCardDetails('');
    mapDispatch.resetCardDetails(cardDetailsState);
    mapDispatch.savePaymentMethod();
    mapDispatch.useSameCard();
    mapDispatch.editUpFront();
    mapDispatch.changeSelectedCard(cardDetails);
    mapDispatch.editPaymentStepClick();
    mapDispatch.setActiveCardField('cvv');
    mapDispatch.setPaymentStep(PAYMENT_TYPE.BANK_ACCOUNT);
  });

  test('render UI with PAY_BY_LINK', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.PAY_BY_LINK;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with PAY_ON_DELIVERY', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.PAY_ON_DELIVERY;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with BANK_ACCOUNT', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.BANK_ACCOUNT;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with MANUAL_PAYMENTS', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.MANUAL_PAYMENTS;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with NONE', () => {
    const copyComponentProps = { ...props };
    delete copyComponentProps.paymentInfo.paymentTypeSelected;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with paymentTypeSelected', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    copyComponentProps.paymentInfo.monthly.addingNewCard = true;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with paymentTypeSelected with isUpfron', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    copyComponentProps.paymentInfo.monthly.addingNewCard = true;
    copyComponentProps.paymentInfo.isUpfront = false;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with checkProceedStatus with isUpfron and payment type credit debit card ', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.upFront.paymentTypeSelected =
      PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    copyComponentProps.checkout.isMonthlyPrice = true;

    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).checkProceedStatus();

    copyComponentProps.paymentInfo.isUpfront = true;
    const component1 = componentWrapper(copyComponentProps);
    (component1
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).checkProceedStatus();
  });
  test('render UI with checkProceedStatus with isUpfron', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.upFront.paymentTypeSelected =
      PAYMENT_TYPE.PAY_BY_LINK;

    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).checkProceedStatus();
  });

  test('render UI with renderUpfrontSummary without isUpfront', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.checkout.isUpfrontPrice = true;
    copyComponentProps.paymentInfo.isUpfront = false;
    copyComponentProps.paymentInfo.upFront.cardDetails = [
      {
        cardNumber: '',
        isDefault: true,
        cardType: '',
        expiryDate: '',
        securityCode: '',
        nameOnCard: '',
        id: '',
        isCardSaved: true,
        lastFourDigits: '',
        brand: ''
      }
    ];

    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderUpfrontSummary();
    copyComponentProps.paymentInfo.isUpfront = true;
    component.update();
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderUpfrontSummary();
  });

  test('should render properly, when payament type is empty string  ', () => {
    const newProps: IComponentProps = { ...props };
    // tslint:disable-next-line:no-any
    newProps.paymentInfo.monthly.paymentTypeSelected = '' as any;
    const wapper = componentWrapper(newProps);
    expect(wapper).toMatchSnapshot();
    (wapper
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('render UI with CREDIT_DEBIT_CARD with isUpfronRice shpuld be true', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    copyComponentProps.checkout.isUpfrontPrice = true;
    copyComponentProps.paymentInfo.monthly.addingNewCard = true;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('MonthlyPayment')
      .instance() as MonthlyPayment).renderPaymentStep();
  });

  test('on component unmount', () => {
    const component = componentWrapper(props);
    component.unmount();
  });
});
