import { PAYMENT_TYPE } from '@checkout/store/enums';
import { RootState } from '@common/store/reducers';
import { Dispatch } from 'redux';
import { IMainState } from '@common/store/reducers/types';
import CreditDebitCard from '@checkout/CheckoutSteps/Payment/Common/CreditDebitCard';
import React, { Component, ReactNode } from 'react';
import paymentActions from '@checkout/store/payment/actions';
import { ICheckoutState } from '@checkout/store/types';
import {
  activeFieldTypes,
  ICardDetails,
  IPaymentState,
  paymentTypes
} from '@checkout/store/payment/types';
import { IConfigurationState } from '@common/store/types/configuration';
import { ITranslationState } from '@common/store/types/translation';
import { connect } from 'react-redux';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/types';
import MobileNav from '@checkout/CheckoutSteps/Payment/PaymentSideBar/MobileNav';
import { isMobile } from '@src/common/utils';
import { Button, Divider, Section } from 'dt-components';
import { getDefaultCard } from '@checkout/CheckoutSteps/Payment/utils/convertData';
import EmptyValue from '@checkout/CheckoutSteps/Payment/Common/EmptyUpFrontValue';
import SideNavigation from '@checkout/SideNavigation';
import PaymentSideBar from '@checkout/CheckoutSteps/Payment/PaymentSideBar';
import StepNavigation from '@checkout/CheckoutSteps/StepNavigation';
import LinkWithIcon from '@src/common/components/LinkWithIcon';
import EVENT_NAME from '@events/constants/eventName';
import Footer from '@src/common/components/FooterWithTermsAndConditions';

import { StyledSwitch } from '../Common/styles';
import Information from '../../../Common/Information';

import UpfrontReadOnlyForm from './UpfrontReadOnlyForm';
import { StyledMonthlyPayment } from './styles';

export interface IComponentStateProps {
  checkout: ICheckoutState;
  configuration: IConfigurationState;
  translation: ITranslationState;
  paymentInfo: IPaymentState;
  activeCardField: activeFieldTypes;
}

export interface ICompDispatchToProps {
  setSaveThisCard(): void;
  setCardDetails(data: string): void;
  resetCardDetails(data: ICardState): void;
  useSameCard(): void;
  editUpFront(): void;
  fetchPaymentMethods(): void;
  changeSelectedCard(data: ICardDetails): void;
  setActiveCardField(data: activeFieldTypes): void;
  setPaymentStep(type: paymentTypes): void;
  savePaymentMethod(): void;
  editPaymentStepClick(): void;
}

export type IComponentProps = IComponentStateProps & ICompDispatchToProps;

export class MonthlyPayment extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
    this.renderPaymentStep = this.renderPaymentStep.bind(this);
    this.renderUpfrontSummary = this.renderUpfrontSummary.bind(this);
  }

  showSaveCardSwitch(): ReactNode {
    const { translation, setSaveThisCard, paymentInfo } = this.props;
    const { upFront, monthly } = this.props.paymentInfo;
    if (
      paymentInfo.upFront.paymentTypeSelected ===
        PAYMENT_TYPE.CREDIT_DEBIT_CARD &&
      (upFront.addingNewCard || monthly.addingNewCard)
    ) {
      return (
        <StyledSwitch
          disabled={false}
          onLabel={translation.cart.checkout.paymentInfo.saveThisCard}
          offLabel={translation.cart.checkout.paymentInfo.saveThisCard}
          on={
            paymentInfo.isUpfront
              ? paymentInfo.upFront.saveNewCard
              : paymentInfo.monthly.saveNewCard
          }
          onToggle={setSaveThisCard}
        />
      );
    }

    return null;
  }

  checkProceedStatus(): boolean {
    let proceedDisableStatus = true;

    const { isMonthlyPrice } = this.props.checkout;
    const { paymentInfo } = this.props;
    if (
      paymentInfo.upFront.paymentTypeSelected === PAYMENT_TYPE.CREDIT_DEBIT_CARD
    ) {
      if (isMonthlyPrice) {
        proceedDisableStatus = paymentInfo.isUpfront
          ? paymentInfo.upFront.proceedDisabled
          : paymentInfo.monthly.proceedDisabled;
      } else {
        proceedDisableStatus = paymentInfo.upFront.proceedDisabled;
      }
    } else {
      proceedDisableStatus = false;
    }

    return proceedDisableStatus;
  }

  checkProceedText(): string {
    const { isMonthlyPrice } = this.props.checkout;
    const { paymentInfo, translation } = this.props;

    let proceedButtonText = '';

    if (isMonthlyPrice) {
      proceedButtonText = paymentInfo.isUpfront
        ? translation.cart.checkout.paymentInfo.proceedToMonthly
        : translation.cart.checkout.paymentInfo.proceedToBilling;
    } else {
      proceedButtonText =
        translation.cart.checkout.paymentInfo.proceedToBilling;
    }

    return proceedButtonText;
  }

  getProceedPath = (): string => {
    const { isMonthlyPrice } = this.props.checkout;
    const { paymentInfo } = this.props;

    let proceedButtonText = '';

    if (isMonthlyPrice) {
      proceedButtonText = paymentInfo.isUpfront
        ? 'cart.checkout.paymentInfo.proceedToMonthly'
        : 'cart.checkout.paymentInfo.proceedToBilling';
    } else {
      proceedButtonText = 'cart.checkout.paymentInfo.proceedToBilling';
    }

    return proceedButtonText;
  }

  renderUpfrontSummary(): ReactNode {
    const { translation, checkout, paymentInfo, editUpFront } = this.props;
    const defaultUpfrontCard = getDefaultCard(paymentInfo.upFront.cardDetails);

    if (!checkout.isUpfrontPrice) {
      return (
        <EmptyValue
          paymentTranslation={translation.cart.checkout.paymentInfo}
          forUpfront={true}
        />
      );
    } else if (
      checkout.isUpfrontPrice &&
      !paymentInfo.isUpfront &&
      defaultUpfrontCard
    ) {
      return (
        <UpfrontReadOnlyForm
          paymentTranslation={translation.cart.checkout.paymentInfo}
          selectedCard={defaultUpfrontCard}
          editUpFront={editUpFront}
        />
      );
    }

    return null;
  }

  renderPaymentStep(): ReactNode {
    const {
      checkout,
      configuration,
      translation,
      paymentInfo,
      setSaveThisCard,
      setCardDetails,
      resetCardDetails,
      useSameCard,
      editUpFront,
      changeSelectedCard,
      setActiveCardField,
      savePaymentMethod
    } = this.props;

    switch (paymentInfo.monthly.paymentTypeSelected) {
      case PAYMENT_TYPE.CREDIT_DEBIT_CARD:
        return (
          <CreditDebitCard
            addNewCardDetails={paymentInfo.addNewCard}
            translation={translation}
            configuration={configuration}
            checkout={checkout}
            paymentInfo={paymentInfo}
            useSameCard={useSameCard}
            setSaveThisCard={setSaveThisCard}
            setCardDetails={setCardDetails}
            resetCardDetails={resetCardDetails}
            editUpFront={editUpFront}
            changeSelectedCard={changeSelectedCard}
            setActiveCardField={setActiveCardField}
            savePaymentMethod={savePaymentMethod}
            buttonLabel={this.checkProceedText()}
          />
        );

      case PAYMENT_TYPE.PAY_BY_LINK:
        return null;

      case PAYMENT_TYPE.PAY_ON_DELIVERY:
        return (
          <Information
            informationText={
              translation.cart.checkout.paymentInfo.payOnDeliveryInfoText
            }
          />
        );
      case PAYMENT_TYPE.BANK_ACCOUNT:
        return null;
      case PAYMENT_TYPE.MANUAL_PAYMENTS:
        return (
          <Information
            informationText={
              translation.cart.checkout.paymentInfo.manualPaymentInfoText
            }
          />
        );
      default:
        return null;
    }
  }

  render(): ReactNode {
    const {
      checkout,
      configuration,
      translation,
      paymentInfo,
      setPaymentStep,
      savePaymentMethod,
      editUpFront,
      activeCardField,
      editPaymentStepClick
    } = this.props;

    const proceedDisableStatus = this.checkProceedStatus();

    return (
      <>
        {!isMobile.phone && (
          <SideNavigation>
            <PaymentSideBar
              checkout={checkout}
              activeCardField={activeCardField}
              paymentMethodConfiguration={
                configuration.cms_configuration.global.paymentMethods
              }
              paymentInfoTranslation={translation.cart.checkout.paymentInfo}
              paymentInfo={paymentInfo}
              setPaymentStep={setPaymentStep}
            />
          </SideNavigation>
        )}

        <div className='mainContent'>
          <StepNavigation
            checkoutTranslation={translation.cart.checkout}
            editPaymentStepClick={editPaymentStepClick}
          />

          <StyledMonthlyPayment>
            {paymentInfo.upFront.paymentTypeSelected ===
              PAYMENT_TYPE.CREDIT_DEBIT_CARD && (
              <div className='heading'>
                <div className='textWrap'>
                  <Section size='small' transform='uppercase'>
                    {translation.cart.checkout.upfront}
                  </Section>

                  {checkout.isUpfrontPrice && (
                    <Section size='small' firstLetterTransform='uppercase'>
                      <LinkWithIcon
                        text={translation.cart.checkout.paymentInfo.edit}
                        onClick={editUpFront}
                      />
                    </Section>
                  )}
                </div>

                <Divider dashed orientation='horizontal' />
              </div>
            )}

            {paymentInfo.upFront.paymentTypeSelected ===
              PAYMENT_TYPE.CREDIT_DEBIT_CARD && this.renderUpfrontSummary()}

            {isMobile.phone || isMobile.tablet ? (
              <MobileNav
                className='mobileNavMonthly'
                checkout={checkout}
                paymentMethodConfiguration={
                  configuration.cms_configuration.global.paymentMethods
                }
                paymentInfoTranslation={translation.cart.checkout.paymentInfo}
                paymentInfo={paymentInfo}
                setPaymentStep={setPaymentStep}
              />
            ) : null}

            {this.renderPaymentStep()}

            <div className='bottomCTA'>
              {this.showSaveCardSwitch()}
              {!paymentInfo.monthly.addingNewCard ||
              paymentInfo.monthly.paymentTypeSelected !==
                PAYMENT_TYPE.CREDIT_DEBIT_CARD ? (
                <Button
                  fullwidth={false}
                  size='small'
                  disabled={proceedDisableStatus}
                  className='proceedbtn'
                  loading={paymentInfo.loading}
                  type='primary'
                  onClickHandler={savePaymentMethod}
                  data-event-id={EVENT_NAME.CHECKOUT.EVENTS.PROCEED}
                  data-event-message={this.checkProceedText()}
                  data-event-path={this.getProceedPath()}
                >
                  {this.checkProceedText()}
                </Button>
              ) : null}
            </div>
          </StyledMonthlyPayment>
          {isMobile.tablet ? (
            <Footer
              className='footerCheckout'
              termsAndConditionsUrl={
                this.props.configuration.cms_configuration.global
                  .termsAndConditionsUrl
              }
              shouldTermsAndConditionsOpenInNewTab={
                this.props.configuration.cms_configuration.global
                  .shouldTermsAndConditionsOpenInNewTab
              }
              globalTranslation={this.props.translation.cart.global}
            />
          ) : null}
        </div>
      </>
    );
  }
}

export const mapStateToProps = (state: IMainState): IComponentStateProps => ({
  checkout: state.checkout.checkout,
  configuration: state.configuration,
  translation: state.translation,
  paymentInfo: state.checkout.payment,
  activeCardField: state.checkout.payment.activeCardField
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): ICompDispatchToProps => ({
  fetchPaymentMethods(): void {
    dispatch(paymentActions.fetchPaymentInfoRequested(undefined));
  },
  setSaveThisCard(): void {
    dispatch(paymentActions.setSaveThisCard());
  },
  setCardDetails(data: string): void {
    dispatch(paymentActions.setCardDetails(data));
  },
  resetCardDetails(data: ICardState): void {
    dispatch(paymentActions.resetCardDetails(data));
  },
  useSameCard(): void {
    dispatch(paymentActions.useSameCard());
  },
  editUpFront(): void {
    dispatch(paymentActions.editUpFront());
  },
  changeSelectedCard(data: ICardDetails): void {
    dispatch(paymentActions.changeSelectedCard(data));
  },
  setActiveCardField(data: activeFieldTypes): void {
    dispatch(paymentActions.changeActiveCardField(data));
  },
  setPaymentStep(type: paymentTypes): void {
    dispatch(paymentActions.setPaymentStepChange(type));
  },
  savePaymentMethod(): void {
    dispatch(paymentActions.savePaymentMethod());
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  }
});

export default connect<
  IComponentStateProps,
  ICompDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
  // tslint:disable-next-line:max-file-line-count
)(MonthlyPayment);
