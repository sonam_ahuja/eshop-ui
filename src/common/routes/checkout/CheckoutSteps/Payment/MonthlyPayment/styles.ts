import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledPaymentCards } from '../Common/PaymentCards/styles';
import { StyledCardDetailForm } from '../Common/CardDetailForm/styles';
import { StyledMonthly } from '../Common/Monthly/styles';

export const StyledMonthlyPayment = styled.div`
  padding: 0rem 1.25rem 1.25rem;
  .heading {
    color: ${colors.mediumGray};
    margin-top: 3.25rem;
    .textWrap {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }

    .dt_divider {
      margin: 0;
      margin-top: 0.5rem;
      margin-bottom: 1.75rem;
    }
  }

  .heading + ${StyledPaymentCards} {
    margin-top: -0.3rem;
  }

  .mobileNavMonthly {
    margin: 0rem -1.25rem 2.5rem;
  }

  .bottomCTA {
    .dt_button {
      width: 100%;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 1rem 2.25rem 1.25rem;
    flex: 1;
    display: flex;
    flex-direction: column;
    .heading {
      margin-top: 0rem;
      .dt_divider {
        margin-top: 0.0625rem;
        margin-bottom: 1.65rem;
      }
    }
    .mobileNavMonthly {
      margin: 0.78rem -2.25rem 2.5rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 2.5rem 3rem 2.25rem;
    position: relative;
    .heading + ${StyledPaymentCards} {
      margin-top: 0;
    }

    .bottomCTA {
      display: flex;
      flex-direction: row;
      align-self: flex-end;
      align-items: center;
      margin-top: auto;
      height: 2.5rem;
      width: 100%;
      .dt_switch {
        margin: 0;
      }

      .dt_button {
        width: auto;
        justify-self: flex-end;
        margin-left: auto;
      }
    }

    ${StyledCardDetailForm} {
      .buttonWrap {
        position: absolute;
        bottom: 0;
        margin-bottom: 2.25rem !important;
        right: 0;
        padding: 0 3rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2.25rem;
    ${StyledMonthly} {
      margin-top: 2.8rem;
    }
    ${StyledCardDetailForm} {
      .buttonWrap {
        padding: 0 2.25rem;
      }
    }
    .bottomCTA {
      height: 3rem;
      .dt_button {
        height: 3rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 4.15rem 5.5rem 2.25rem;
    ${StyledMonthly} {
      margin-top: 4.65rem;
    }
    ${StyledCardDetailForm} {
      .buttonWrap {
        padding: 0 5.5rem;
      }
    }
  }
`;

// export const SaveCardButtonWrap = styled.div`
//   position: relative;
//   display: flex;
//   flex-direction: row;
//   justify-content: flex-end;
//   align-items: center;
//   margin-top: 1.5rem;
//   margin-bottom: 1rem;

//   button {
//     width: 100%;
//   }

//   @media (min-width: ${breakpoints.desktop}px) {
//     button {
//       width: auto;
//     }
//   }
// `;

// export const PaymentModeWrap = styled.div`
//   margin: 2rem;

//   @media (min-width: ${breakpoints.desktop}px) {
//     margin: 3rem 0;
//   }
// `;

// export const UpfrontWrap = styled.div`
//   width: 100%;
//   display: flex;
//   flex-direction: column;
//   margin: 3rem 0;
//   color: #a3a3a3;

//   @media (min-width: ${breakpoints.desktop}px) {
//     margin: 3rem 0 0 0;
//   }

//   .divider {
//     :before {
//       border-width: 0.2rem;
//       color: #c2c2c2;
//     }
//   }
// `;
