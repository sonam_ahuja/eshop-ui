import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import CardType from '@checkout/CheckoutSteps/Payment/PaymentSideBar/CardType';
import { StyledCardType } from '@checkout/CheckoutSteps/Payment/PaymentSideBar/CardType/styles';

describe('<CardType />', () => {
  const props = {};

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CardType {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('StyledCardType should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StyledCardType {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
