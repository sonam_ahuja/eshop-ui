import React, { FunctionComponent } from 'react';
import { PreloadImage } from 'dt-components';

import { StyledCardType } from './styles';

const CardType: FunctionComponent<{}> = () => {
  return (
    <StyledCardType className='cardType'>
      <div className='cardImage'>
        <PreloadImage
          imageUrl='https://i.ibb.co/C2Z0G5t/gray-card.png'
          imageHeight={10}
          imageWidth={10}
        />
      </div>
    </StyledCardType>
  );
};

export default CardType;
