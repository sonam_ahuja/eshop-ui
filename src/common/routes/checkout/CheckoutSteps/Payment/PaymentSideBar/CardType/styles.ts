import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledCardType = styled.div`
  .cardImage {
    width: 3.75rem;
    height: 3.75rem;

    @media (min-width: ${breakpoints.desktop}px) {
      margin-top: 5.775rem;
      margin-bottom: 1.8rem;
      margin-right: 0;
      min-width: 9.8rem;
      max-width: 19, 3rem;
      height: 5.95rem;
    }
  }
`;
