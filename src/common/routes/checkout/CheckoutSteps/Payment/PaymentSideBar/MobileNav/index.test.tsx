import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { PAYMENT_TYPE } from '@checkout/store/enums';

import MobileNav, { getSVG, IProps as IMobileNavProps } from '.';

describe('<Payment MobileNav />', () => {
  const props: IMobileNavProps = {
    checkout: appState().checkout.checkout,
    paymentMethodConfiguration: appState().configuration.cms_configuration
      .global.paymentMethods,
    paymentInfoTranslation: appState().translation.cart.checkout.paymentInfo,
    paymentInfo: appState().checkout.payment,
    setPaymentStep: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <MobileNav {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('getSVG ::', () => {
    const bankAccount = getSVG(PAYMENT_TYPE.BANK_ACCOUNT);
    expect(getSVG(PAYMENT_TYPE.BANK_ACCOUNT)).toEqual(bankAccount);
    const creditCard = getSVG(PAYMENT_TYPE.CREDIT_DEBIT_CARD);
    expect(getSVG(PAYMENT_TYPE.CREDIT_DEBIT_CARD)).toEqual(creditCard);
    const manulPayment = getSVG(PAYMENT_TYPE.MANUAL_PAYMENTS);
    expect(getSVG(PAYMENT_TYPE.MANUAL_PAYMENTS)).toEqual(manulPayment);
    const payByLink = getSVG(PAYMENT_TYPE.PAY_BY_LINK);
    expect(getSVG(PAYMENT_TYPE.PAY_BY_LINK)).toEqual(payByLink);
    const payOnDelivery = getSVG(PAYMENT_TYPE.PAY_ON_DELIVERY);
    expect(getSVG(PAYMENT_TYPE.PAY_ON_DELIVERY)).toEqual(payOnDelivery);
    // tslint:disable: no-any
    const emptyString = getSVG('' as any);
    expect(getSVG('' as any)).toEqual(emptyString);
  });

  test('should render properly with  create render data', () => {
    const newProps = { ...props };
    newProps.paymentInfo.isUpfront = false;

    const component = mount(
      <ThemeProvider theme={{}}>
        <MobileNav {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
