import {
  renderPaymentMethodsMonthly,
  renderPaymentMethodsUpfront
} from '@checkout/CheckoutSteps/Payment/PaymentSideBar/PaymentType/util';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import { ICheckoutState } from '@checkout/store/types';
import { IPaymentInfoTranslation } from '@src/common/store/types/translation';
import { IPaymentMethods } from '@src/common/store/types/configuration';
import { Select } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import {
  IPaymentGenerateType,
  IPaymentState,
  paymentTypes
} from '@checkout/store/payment/types';
import SelectWithIcon from '@common/components/Select/SelectWithIcon';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import SvgPaymentCard from '@src/common/components/Svg/payment-card';
import SvgPayByLink from '@src/common/components/Svg/pay-by-link';
import SvgPayOnDelivery from '@src/common/components/Svg/pay-on-delivery';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledPaymentBillingNav } from './styles';

export interface IProps {
  checkout: ICheckoutState;
  paymentMethodConfiguration: IPaymentMethods;
  paymentInfoTranslation: IPaymentInfoTranslation;
  paymentInfo: IPaymentState;
  className?: string;
  setPaymentStep(type: paymentTypes): void;
}

export interface IState {
  selectedPaymentType: ReactNode;
}

export const getSVG = (selectedId: string) => {
  switch (selectedId) {
    case PAYMENT_TYPE.BANK_ACCOUNT:
      return null;

    case PAYMENT_TYPE.CREDIT_DEBIT_CARD:
      return <SvgPaymentCard />;

    case PAYMENT_TYPE.MANUAL_PAYMENTS:
      return null;

    case PAYMENT_TYPE.PAY_BY_LINK:
      return <SvgPayByLink />;

    case PAYMENT_TYPE.PAY_ON_DELIVERY:
      return <SvgPayOnDelivery />;

    default:
      return null;
  }
};

export class MobileNav extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.createRenderData = this.createRenderData.bind(this);
  }

  mapPaymentMethods(paymentMethods: IPaymentGenerateType[]): IListItem[] {
    const { paymentInfoTranslation } = this.props;

    return paymentMethods.map((paymentMethodsType: IPaymentGenerateType) => {
      return {
        id: paymentMethodsType.labelKey as string,
        title: paymentInfoTranslation[paymentMethodsType.labelKey as string]
      };
    });
  }

  createRenderData(): IListItem[] {
    const { paymentInfo, paymentMethodConfiguration } = this.props;

    const upfrontPaymentMethods = renderPaymentMethodsUpfront(
      paymentMethodConfiguration.upfront, paymentInfo.upFront.enabledPaymentMethods
    );
    const monthlyPaymentMethods = renderPaymentMethodsMonthly(
      paymentMethodConfiguration.monthly, paymentInfo.monthly.enabledPaymentMethods
    );

    if (paymentInfo.isUpfront) {
      return this.mapPaymentMethods(upfrontPaymentMethods);
    }

    return this.mapPaymentMethods(monthlyPaymentMethods);
  }

  render(): ReactNode {
    const { paymentInfo, className, paymentInfoTranslation } = this.props;

    const selectedNode = paymentInfo.isUpfront
      ? paymentInfo.upFront.paymentTypeSelected
      : paymentInfo.monthly.paymentTypeSelected;

    return (
      <StyledPaymentBillingNav className={className}>
        <Select
          useNativeDropdown={true}
          onItemSelect={selectedItem => {
            sendDropdownClickEvent(selectedItem.title);
            this.props.setPaymentStep(selectedItem.id as PAYMENT_TYPE);
          }}
          SelectedItemComponent={selectedItemProps => (
            <>
              <SelectWithIcon
                {...selectedItemProps}
                svgNode={getSVG(
                  selectedItemProps.selectedItem
                    ? (selectedItemProps.selectedItem.id as string)
                    : ' 0'
                )}
                description={
                  selectedItemProps.selectedItem
                    ? selectedItemProps.selectedItem.title
                    : ''
                }
              />
            </>
          )}
          items={this.createRenderData()}
          selectedItem={{
            id: selectedNode,
            title: paymentInfoTranslation[selectedNode]
          }}
        />
      </StyledPaymentBillingNav>
    );
  }
}

export default MobileNav;
