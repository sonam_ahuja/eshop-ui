import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledPaymentBillingNav = styled.div`
  background: ${colors.white};
  padding: 0 1.25rem;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 0 2.25rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: none;
  }
`;
