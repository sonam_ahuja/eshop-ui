import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configurationState from '@store/states/configuration';
import translationState from '@store/states/translation';
import paymentState from '@checkout/store/payment/state';
import checkoutState from '@checkout/store/state';

import PaymentType, { IProps as IPaymentTypeProps } from '.';

describe('<SideNavigation PaymentType />', () => {
  const props: IPaymentTypeProps = {
    checkout: checkoutState(),
    paymentMethodConfiguration: configurationState().cms_configuration.global
      .paymentMethods,
    paymentInfoTranslation: translationState().cart.checkout.paymentInfo,
    paymentInfo: paymentState(),
    setPaymentStep: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentType {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
