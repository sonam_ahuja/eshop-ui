import {
  IPaymentGenerateType,
  IPaymentState,
  paymentTypes
} from '@checkout/store/payment/types';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import { ICheckoutState } from '@checkout/store/types';
import React, { Component, ReactNode } from 'react';
import { IPaymentInfoTranslation } from '@common/store/types/translation';
import { IPaymentMethods } from '@common/store/types/configuration';
import {
  renderPaymentMethodsMonthly,
  renderPaymentMethodsUpfront
} from '@checkout/CheckoutSteps/Payment/PaymentSideBar/PaymentType/util';
import SideNavTabs from '@src/common/routes/checkout/Common/SideNavTabs';
import { sendCTAClicks } from '@events/index';
import { sendModifyPaymentOptionEvent } from '@events/checkout';

export interface IProps {
  checkout: ICheckoutState;
  paymentMethodConfiguration: IPaymentMethods;
  paymentInfoTranslation: IPaymentInfoTranslation;
  paymentInfo: IPaymentState;
  setPaymentStep(type: paymentTypes): void;
}

export class PaymentType extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.createRenderData = this.createRenderData.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(item: PAYMENT_TYPE): () => void {
    return () => {
      sendCTAClicks(
        this.props.paymentInfoTranslation[item],
        window.location.href
      );

      sendModifyPaymentOptionEvent(this.props.paymentInfoTranslation[item]);
      this.props.setPaymentStep(item);
    };
  }

  mapPaymentMethods(paymentMethods: IPaymentGenerateType[]): ReactNode {
    const { paymentInfoTranslation, paymentInfo } = this.props;
    const paymentState = paymentInfo.isUpfront ? 'upFront' : 'monthly';

    return paymentMethods.map(
      (paymentMethodsType: IPaymentGenerateType, index: number) => (
        <SideNavTabs
          tabIndex={index}
          key={paymentMethodsType.labelKey}
          onClick={this.handleClick(
            paymentMethodsType.labelKey as PAYMENT_TYPE
          )}
          onKeyDown={this.handleClick(
            paymentMethodsType.labelKey as PAYMENT_TYPE
          )}
          isActive={
            paymentInfo[paymentState].paymentTypeSelected ===
            paymentMethodsType.labelKey
          }
          translatedValue={
            paymentInfoTranslation[paymentMethodsType.labelKey as PAYMENT_TYPE]
          }
        />
      )
    );
  }

  createRenderData(): ReactNode {
    const { paymentInfo, paymentMethodConfiguration } = this.props;

    const upfrontPaymentMethods = renderPaymentMethodsUpfront(
      paymentMethodConfiguration.upfront,
      paymentInfo.upFront.enabledPaymentMethods
    );
    const monthlyPaymentMethods = renderPaymentMethodsMonthly(
      paymentMethodConfiguration.monthly,
      paymentInfo.monthly.enabledPaymentMethods
    );

    if (paymentInfo.isUpfront) {
      return this.mapPaymentMethods(upfrontPaymentMethods);
    } else {
      return this.mapPaymentMethods(monthlyPaymentMethods);
    }
  }

  render(): ReactNode {
    return <>{this.createRenderData()}</>;
  }
}

export default PaymentType;
