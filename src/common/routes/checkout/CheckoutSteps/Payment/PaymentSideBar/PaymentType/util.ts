import {
  IMonthlyPayment,
  IUpfrontPayment
} from '@common/store/types/configuration';
import { IEnabledPaymentMethods, IPaymentGenerateType } from '@checkout/store/payment/types';

export function checkIfMethodsExist(
  info: IUpfrontPayment | IMonthlyPayment, enableMethods: IEnabledPaymentMethods
): IPaymentGenerateType[] {
  const arr: IPaymentGenerateType[] = [];

  Object.keys(info).forEach(key => {
    if (info[key] && info[key].show && enableMethods[key]) {
      arr.push(info[key]);
    }
  });

  return arr;
}

export function renderPaymentMethodsUpfront(
  upfrontInfo: IUpfrontPayment, enableMethods: IEnabledPaymentMethods
): IPaymentGenerateType[] {
  return checkIfMethodsExist(upfrontInfo, enableMethods);
}

export function renderPaymentMethodsMonthly(
  monthlyInfo: IMonthlyPayment, enableMethods: IEnabledPaymentMethods
): IPaymentGenerateType[] {
  return checkIfMethodsExist(monthlyInfo, enableMethods);
}
