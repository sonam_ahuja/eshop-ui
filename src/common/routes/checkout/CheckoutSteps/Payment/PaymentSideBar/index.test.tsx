import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import PaymentBilling, { IProps as IPaymentBillingProps } from '.';

describe('<SideNavigation PaymentBilling />', () => {
  const props: IPaymentBillingProps = {
    checkout: appState().checkout.checkout,
    paymentMethodConfiguration: appState().configuration.cms_configuration
      .global.paymentMethods,
    paymentInfoTranslation: appState().translation.cart.checkout.paymentInfo,
    paymentInfo: appState().checkout.payment,
    setPaymentStep: jest.fn(),
    activeCardField: 'number'
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentBilling {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
