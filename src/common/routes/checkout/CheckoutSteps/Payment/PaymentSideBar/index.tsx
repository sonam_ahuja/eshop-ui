import React, { FunctionComponent } from 'react';
import PaymentType from '@checkout/CheckoutSteps/Payment/PaymentSideBar/PaymentType';
import {
  activeFieldTypes,
  IPaymentState,
  paymentTypes
} from '@checkout/store/payment/types';
import Thumbnail from '@checkout/CheckoutSteps/Payment/Thumbnail';
import { IPaymentInfoTranslation } from '@src/common/store/types/translation';
import { IPaymentMethods } from '@src/common/store/types/configuration';
import { ICheckoutState } from '@checkout/store/types';

import { StyledPaymentSideBar, StyledPaymentTabs } from './styles';

export interface IProps {
  checkout: ICheckoutState;
  paymentMethodConfiguration: IPaymentMethods;
  paymentInfoTranslation: IPaymentInfoTranslation;
  paymentInfo: IPaymentState;
  activeCardField: activeFieldTypes;
  setPaymentStep(type: paymentTypes): void;
}

const PaymentSideBar: FunctionComponent<IProps> = (props: IProps) => {
  const { activeCardField } = props;

  return (
    <StyledPaymentSideBar>
      {/* <CardType /> */}
      <Thumbnail activeField={activeCardField} />
      <StyledPaymentTabs>
        <PaymentType {...props} />
      </StyledPaymentTabs>
      {/* <AddressType /> */}
    </StyledPaymentSideBar>
  );
};

export default PaymentSideBar;
