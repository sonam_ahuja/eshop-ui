import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledPaymentTabs = styled.div`
  width: 100%;
`;

export const StyledPaymentSideBar = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 1rem 1.25rem;
  background: ${colors.white};

  .paragraph {
    padding: 0.25rem 1rem;
    font-size: 1rem;
    line-height: 1.5;
    color: ${colors.mediumGray};
    border: 1px solid transparent;
    border-radius: 0.5rem;
    font-weight: normal;
    margin-bottom: 0.25rem;
  }

  .paragraph.active {
    border-color: ${colors.steelGray};
    color: ${colors.darkGray};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    flex-direction: column;
    padding: 0;
    color: ${colors.mediumGray};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    align-items: initial;
    padding: 0 0.775rem;
  }
`;
