import React from 'react';

// tslint:disable
const CardSvgBack = () => {
  return (
    <div
      className="card"
      dangerouslySetInnerHTML={{
        __html: `<?xml version="1.0" encoding="UTF-8"?>
        <svg width="1em" height="1em" viewBox="0 0 195 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <!-- Generator: Sketch 52.6 (67491) - http://www.bohemiancoding.com/sketch -->
            <title>Foundations / Illustrations / Card / Cvv Selected</title>
            <desc>Created with Sketch.</desc>
            <defs>
                <polygon id="path-1" points="0 120 195 120 195 0 0 0"></polygon>
                <polygon id="path-3" points="0 0 195 0 195 118.7919 0 118.7919"></polygon>
            </defs>
            <g id="Foundations-/-Illustrations-/-Card-/-Cvv-Selected" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="card">
                    <path d="M185,120 L10,120 C4.477,120 0,115.523 0,110 L0,10 C0,4.477 4.477,0 10,0 L185,0 C190.522,0 195,4.477 195,10 L195,110 C195,115.523 190.522,120 185,120" id="Fill-1" fill="#EDEDED"></path>
                    <path d="M185,0 L10,0 C4.477,0 0,4.477 0,10 L0,110 C0,115.523 4.477,120 10,120 L185,120 C190.522,120 195,115.523 195,110 L195,10 C195,4.477 190.522,0 185,0 M185,3 C188.859,3 192,6.14 192,10 L192,110 C192,113.86 188.859,117 185,117 L10,117 C6.14,117 3,113.86 3,110 L3,10 C3,6.14 6.14,3 10,3 L185,3" id="Fill-3" fill="#C2C2C2"></path>
                    <mask id="mask-2" fill="white">
                        <use xlink:href="#path-1"></use>
                    </mask>
                    <g id="Clip-40"></g>
                    <rect id="Rectangle" fill="#C2C2C2" fill-rule="nonzero" mask="url(#mask-2)" x="3" y="20" width="189" height="25"></rect>
                </g>
                <g id="shading" opacity="0.086406">
                    <mask id="mask-4" fill="white">
                        <use xlink:href="#path-3"></use>
                    </mask>
                    <g id="Clip-2"></g>
                    <path d="M185,-0.0001 L10,-0.0001 C4.478,-0.0001 0,4.4769 0,9.9999 L0,109.9999 C0,113.7959 2.115,117.0989 5.231,118.7919 C4.875,117.9509 5.067,116.8819 6.018,116.2969 L185.855,5.6269 C189.854,3.1669 195,6.0429 195,10.7369 L195,9.9999 C195,4.4769 190.522,-0.0001 185,-0.0001" id="Fill-1" fill="#A3A3A3" mask="url(#mask-4)"></path>
                </g>
                <g id="cvv_selected" transform="translate(77.000000, 69.000000)">
                    <path d="M5,9 L35,9 L35,3 L5,3 L5,9 Z M6,8 L34,8 L34,4 L6,4 L6,8 Z" id="Fill-39" fill="#A4A4A4"></path>
                    <rect id="Rectangle" stroke="#E20074" fill-rule="nonzero" x="0.5" y="0.5" width="40" height="11" rx="5.5"></rect>
                </g>
            </g>
        </svg>`
      }}
    />
  );
};

export default CardSvgBack;

// tslint:enable
