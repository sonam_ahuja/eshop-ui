import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import ShippingThumbnail, { activeFieldTypes } from '.';

describe('<ShippingThumbnail />', () => {
  const props = {};
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <ShippingThumbnail {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with svg back', () => {
    const newProps = { ...props, activeField: 'cvv' as activeFieldTypes };
    const component = mount(
      <ThemeProvider theme={{}}>
        <ShippingThumbnail {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
