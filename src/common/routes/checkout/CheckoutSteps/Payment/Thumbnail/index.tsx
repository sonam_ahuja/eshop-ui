import React from 'react';
import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import CardSvg from '@src/common/routes/checkout/CheckoutSteps/Payment/Thumbnail/CardSvg';
import CardSvgBack from '@src/common/routes/checkout/CheckoutSteps/Payment/Thumbnail/CardSvgBack';
import cx from 'classnames';

export type activeFieldTypes =
  | 'number'
  | 'cvv'
  | 'nameOnCard'
  | 'expirationDate'
  | '';

const StyledShippingThumbnail = styled.div<{ activeField: activeFieldTypes }>`
  display: flex;
  flex-direction: row;
  align-self: flex-start;
  padding: 0;
  background: #fff;
  width: 100%;
  padding: 0 15px;
  rect {
    stroke: none;
  }
  .card {
    font-size: 195px;
    line-height: 0;
    margin-top: 2px;
    svg {
      width: 100%;
      height: 100%;
    }
  }

  &.number {
    #number_selected {
      stroke: ${colors.magenta};
    }
  }

  &.cvv {
    #cvv_selected {
      stroke: ${colors.magenta};
      stroke-width: 0.3px;
    }
  }

  &.nameOnCard {
    #name_selected {
      stroke: ${colors.magenta};
    }
  }

  &.expirationDate {
    #date_selected {
      stroke: ${colors.magenta};
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: flex;
    flex-direction: column;
    align-items: initial;
    color: ${colors.mediumGray};
    padding: 0;
  }
`;

export interface IProps {
  activeField?: activeFieldTypes;
}
const ShippingThumbnail = (props: IProps) => {
  const { activeField } = props;
  const classes = cx(activeField);
  const CardEl = activeField === 'cvv' ? <CardSvgBack /> : <CardSvg />;

  return (
    <StyledShippingThumbnail
      activeField={activeField as activeFieldTypes}
      className={classes}
    >
      {CardEl}
    </StyledShippingThumbnail>
  );
};

export default ShippingThumbnail;
