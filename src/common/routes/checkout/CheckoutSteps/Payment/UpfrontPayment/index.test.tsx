import { RootState } from '@src/common/store/reducers';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import { Provider } from 'react-redux';
import React from 'react';
import { cardDetails, cardDetailsState } from '@mocks/checkout/payment';
import { StaticRouter } from 'react-router-dom';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';

import {
  IComponentProps,
  mapDispatchToProps,
  mapStateToProps,
  UpfrontPayment
} from '.';

describe('<UpfrontPayment />', () => {
  const props: IComponentProps = {
    paymentInfo: appState().checkout.payment,
    checkout: appState().checkout.checkout,
    configuration: appState().configuration,
    translation: appState().translation,
    setSaveThisCard: jest.fn(),
    setCardDetails: jest.fn(),
    resetCardDetails: jest.fn(),
    useSameCard: jest.fn(),
    editPaymentStepClick: jest.fn(),
    editUpFront: jest.fn(),
    activeCardField: 'number',
    fetchPaymentMethods: jest.fn(),
    changeSelectedCard: jest.fn(),
    setActiveCardField: jest.fn(),
    setPaymentStep: jest.fn(),
    savePaymentMethod: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <UpfrontPayment {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(appState())).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.setSaveThisCard();
    mapDispatch.fetchPaymentMethods();
    mapDispatch.setCardDetails('set');
    mapDispatch.resetCardDetails(cardDetailsState);
    mapDispatch.savePaymentMethod();
    mapDispatch.useSameCard();
    mapDispatch.editUpFront();
    mapDispatch.changeSelectedCard(cardDetails);
    mapDispatch.setActiveCardField('number');
    mapDispatch.editPaymentStepClick();
    mapDispatchToProps(dispatch).setPaymentStep(PAYMENT_TYPE.PAY_BY_LINK);
  });

  test('render properly with pay by link', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      ...{ paymentTypeSelected: PAYMENT_TYPE.PAY_BY_LINK }
    };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ paymentTypeSelected: PAYMENT_TYPE.PAY_BY_LINK }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('render UI with PAY_ON_DELIVERY', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      ...{ paymentTypeSelected: PAYMENT_TYPE.PAY_ON_DELIVERY }
    };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ paymentTypeSelected: PAYMENT_TYPE.PAY_ON_DELIVERY }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('render UI with BANK_ACCOUNT', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      ...{ paymentTypeSelected: PAYMENT_TYPE.BANK_ACCOUNT }
    };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ paymentTypeSelected: PAYMENT_TYPE.BANK_ACCOUNT }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('render UI with MANUAL_PAYMENTS', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      ...{ paymentTypeSelected: PAYMENT_TYPE.MANUAL_PAYMENTS }
    };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ paymentTypeSelected: PAYMENT_TYPE.MANUAL_PAYMENTS }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });
  test('render UI with MANUAL_PAYMENTS with empty string', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      // tslint:disable:no-any
      ...{ paymentTypeSelected: '' as any }
    };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ paymentTypeSelected: '' as any }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('render UI with NONE', () => {
    const copyComponentProps = { ...props };
    delete copyComponentProps.paymentInfo.paymentTypeSelected;
    const component = componentWrapper(copyComponentProps);

    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('render properly with upfront new card', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;
    const copyComponentProps = { ...props, proceedDisableStatus: false };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ addingNewCard: true }
    };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      isUpfront: false
    };
    copyComponentProps.checkout = {
      ...copyComponentProps.checkout,
      ...{ isMonthlyPrice: true }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('render properly with upfront new card upfornt is true', () => {
    // tslint:disable-next-line:no-string-literal
    window['isMobile'] = true;
    const copyComponentProps = { ...props, proceedDisableStatus: true };
    copyComponentProps.paymentInfo.upFront = {
      ...copyComponentProps.paymentInfo.upFront,
      ...{ addingNewCard: true }
    };
    copyComponentProps.paymentInfo = {
      ...copyComponentProps.paymentInfo,
      isUpfront: true
    };
    copyComponentProps.checkout = {
      ...copyComponentProps.checkout,
      ...{ isMonthlyPrice: true }
    };
    const component = componentWrapper(copyComponentProps);
    (component
      .find('UpfrontPayment')
      .instance() as UpfrontPayment).renderPaymentStep();
  });

  test('on component unmount', () => {
    const wapper = componentWrapper(props);
    wapper.unmount();
  });
});
