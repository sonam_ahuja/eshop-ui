import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

import { StyledPaymentCards } from '../Common/PaymentCards/styles';
import { StyledCardDetailForm } from '../Common/CardDetailForm/styles';

export const StyledUpfrontPayment = styled.div`
  padding: 1.75rem 1.25rem 1.25rem;

  .heading + ${StyledPaymentCards} {
    margin-top: -0.3rem;
  }

  .mobileNavMonthly {
    margin: -1rem -1.25rem 2.5rem;
  }

  .bottomCTA {
    display: flex;
    flex-direction: column-reverse;
    .dt_button {
      width: 100%;
    }
    .dt_switch {
      & + .dt_button {
        margin-bottom: 1.5rem;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 1.75rem 2.25rem 1.75rem;
    display: flex;
    flex-direction: column;
    flex: 1;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 2.5rem 3rem 2.25rem;
    position: relative;
    .heading + ${StyledPaymentCards} {
      margin-top: 0;
    }
    .bottomCTA {
      display: flex;
      flex-direction: row;
      align-self: flex-end;
      align-items: center;
      margin-top: auto;
      height: 2.5rem;
      width: 100%;
      .dt_switch {
        margin: 0;
        & + .dt_button {
          margin-bottom: 0;
        }
      }
      .dt_button {
        width: auto;
        justify-self: flex-end;
        margin-left: auto;
        font-size: 0.875rem;
        line-height: 1.25rem;
        letter-spacing: -0.08px;
        height: 2.5rem;
      }
    }
    ${StyledCardDetailForm} {
      .buttonWrap {
        position: absolute;
        bottom: 0;
        margin-bottom: 2.25rem !important;
        right: 0;
        padding: 0 3rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2.1rem 2.25rem 2.25rem;
    ${StyledCardDetailForm} {
      .buttonWrap {
        padding: 0 2.25rem;
      }
    }
    .bottomCTA {
      height: 3rem;
      .dt_button {
        height: 3rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 4.15rem 5.5rem 2.75rem;
  }
`;
