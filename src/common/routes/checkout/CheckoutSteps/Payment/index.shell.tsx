import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';

const PaymentBillingShellWrapper = styled.div`
  padding: 4rem 2rem;
  float: left;
  width: 85%;
  p {
    width: 100%;
    height: 1rem;
    background-color: #dadada;
    border-radius: 4rem;
    margin-top: 5.5rem;
    display: inline-block;
  }

  hr {
    margin-top: 1.55rem;
    margin-bottom: 3.55rem;
    border-color: #dadada;
    border-style: dotted;
  }

  h1 {
    height: 1.3rem;
    margin-bottom: 3.2rem;
    background-color: #dadada;
    border-radius: 4rem;
    display: inline-block;
    width: 26%;
    margin: 1.2rem 0;
  }
  button {
    width: 16.1rem;
    height: 4rem;
    border: none;
    margin-top: 0rem;
    background-color: #eee;
    float: right;
  }
  ul {
    display: inline-block;
  }
  li {
    background-color: #adadad;
    display: inline;
    padding: 0.5rem;
  }
`;

const CardsOption = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 4.5rem;
  div {
    width: 33%;
    padding: 1em;
    border: 0.5px solid #eee;
    margin: 0.1em;
    height: 13.6rem;
    border-radius: 0.8rem;
    position: relative;
    background-color: #eee;
    p {
      height: 1rem;
      margin-top: 0;
      padding-top: 1rem;
      width: 40%;
      display: block;
    }
    p.dimension {
      position: absolute;
      bottom: 1rem;
      height: 1rem;
      width: 4.1rem;
      background-color: #dadada;
    }
  }
  div:nth-child(2) {
    margin-left: 1rem;
    margin-right: 1rem;
  }
`;
const CardInputDetails = styled.div`
  position: relative;
  display: inline;
  p {
    height: 1rem;
    display: inline-block;
    width: 33%;
    margin-top: 0;
  }
  p:nth-child(2) {
    width: 10%;
    position: relative;
    left: 23%;
  }
  p:nth-child(3) {
    float: right;
    width: 20%;
  }
`;

const Userinput = styled.div`
  margin-top: 3rem;
  margin-bottom: 8.6rem;
  p {
    height: 1rem;
    display: inline-block;
    width: 66%;
    margin-top: 0;
  }
  p:nth-child(2) {
    width: 33%;
    float: right;
  }
`;

const PaymentAppShell: FunctionComponent = () => {
  return (
    <StyledShell>
      <PaymentBillingShellWrapper>
        <p />
        <hr />
        <CardsOption>
          <div>
            <p />
            <p className='dimension' />
          </div>

          <div>
            <p />
            <p className='dimension' />
          </div>

          <div>
            <p />
            <p className='dimension' />
          </div>
        </CardsOption>
        <CardInputDetails>
          <p />
          <p />
          <p />
        </CardInputDetails>
        <Userinput>
          <p />
          <p />
        </Userinput>
        <h1 />
        <button />
      </PaymentBillingShellWrapper>
    </StyledShell>
  );
};

export default PaymentAppShell;
