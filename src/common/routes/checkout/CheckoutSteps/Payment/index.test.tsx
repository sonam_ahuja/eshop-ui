import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import { PAYMENT_TYPE } from '@checkout/store/enums';

import {
  IComponentProps,
  mapDispatchToProps,
  mapStateToProps,
  PaymentMethods
} from '.';
import PaymentAppShell from './index.shell';

describe('<Payment />', () => {
  const props: IComponentProps = {
    paymentInfo: appState().checkout.payment,
    checkout: appState().checkout.checkout,
    translation: appState().translation,
    fetchPaymentMethods: jest.fn(),
    editPaymentStepClick: jest.fn(),
    fetchCheckoutAddressLoading: jest.fn()
  };
  window.scrollTo = jest.fn();
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('PaymentAppShell, should render app shell properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentAppShell />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  window.scrollTo = jest.fn();

  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <PaymentMethods {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(appState())).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const mapDispatch = mapDispatchToProps(dispatch);

    mapDispatch.fetchPaymentMethods();
  });

  test('render UI with PAY_BY_LINK', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.PAY_BY_LINK;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('PaymentMethods')
      .instance() as PaymentMethods).renderPaymentStep();
  });

  test('render UI with PAY_ON_DELIVERY', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.PAY_ON_DELIVERY;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('PaymentMethods')
      .instance() as PaymentMethods).renderPaymentStep();
  });

  test('render UI with BANK_ACCOUNT', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.BANK_ACCOUNT;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('PaymentMethods')
      .instance() as PaymentMethods).renderPaymentStep();
  });

  test('render UI with MANUAL_PAYMENTS', () => {
    const copyComponentProps = { ...props };
    copyComponentProps.paymentInfo.paymentTypeSelected =
      PAYMENT_TYPE.MANUAL_PAYMENTS;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('PaymentMethods')
      .instance() as PaymentMethods).renderPaymentStep();
  });

  test('render UI with NONE', () => {
    const copyComponentProps = { ...props };
    delete copyComponentProps.paymentInfo.paymentTypeSelected;
    const component = componentWrapper(copyComponentProps);
    (component
      .find('PaymentMethods')
      .instance() as PaymentMethods).renderPaymentStep();
  });

  test('on component unmount', () => {
    const component = componentWrapper(props);
    component.unmount();
  });
});
