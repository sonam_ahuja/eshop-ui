import { IPaymentState } from '@checkout/store/payment/types';
import { ICheckoutState } from '@checkout/store/types';
import { Dispatch } from 'redux';
import { ITranslationState } from '@common/store/types/translation';
import { IMainState } from '@common/store/reducers/types';
import React, { Component, ReactNode } from 'react';
import paymentActions from '@checkout/store/payment/actions';
import { connect } from 'react-redux';
import { RootState } from '@common/store/reducers';
import UpfrontPayment from '@checkout/CheckoutSteps/Payment/UpfrontPayment';
import MonthlyPayment from '@checkout/CheckoutSteps/Payment/MonthlyPayment';
import { pageViewEvent } from '@events/index';
import Loadable from 'react-loadable';
import checkoutActions from '@checkout/store/actions';
import { sendProceedEvent } from '@events/checkout/index';
import { CHECKOUT_VIEW, PAGE_VIEW } from '@events/constants/eventName';

import { StyledPayment } from './styles';

const CheckoutAppShellComponent = Loadable({
  loading: () => null,
  loader: () => import('@common/routes/checkout/CheckoutSteps/index.shell')
});
export interface IComponentStateProps {
  checkout: ICheckoutState;
  paymentInfo: IPaymentState;
  translation: ITranslationState;
}

export interface ICompDispatchToProps {
  fetchPaymentMethods(data?: boolean): void;
  editPaymentStepClick(): void;
  fetchCheckoutAddressLoading(data: boolean): void;
}

export type IComponentProps = IComponentStateProps & ICompDispatchToProps;

export class PaymentMethods extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
    this.renderPaymentStep = this.renderPaymentStep.bind(this);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.CHECKOUT_PAYMENT_INFO);
    sendProceedEvent(CHECKOUT_VIEW.PAYMENT.STEP, CHECKOUT_VIEW.PAYMENT.VIEW);
    this.props.fetchPaymentMethods(true);
    window.scrollTo(0, 0);
  }

  renderPaymentStep(): ReactNode {
    const { paymentInfo } = this.props;

    if (paymentInfo.isUpfront) {
      return <UpfrontPayment />;
    }

    return <MonthlyPayment />;
  }

  render(): ReactNode {
    const {
      checkout,
      translation,
      editPaymentStepClick,
      fetchCheckoutAddressLoading
    } = this.props;

    return (
      <>
        {checkout.mainAppShell ? (
          <CheckoutAppShellComponent
            checkoutTranslation={translation.cart.checkout}
            editPaymentStepClick={editPaymentStepClick}
            fetchCheckoutAddressLoading={fetchCheckoutAddressLoading}
          />
        ) : (
          <StyledPayment>{this.renderPaymentStep()}</StyledPayment>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state: IMainState): IComponentStateProps => ({
  checkout: state.checkout.checkout,
  paymentInfo: state.checkout.payment,
  translation: state.translation
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): ICompDispatchToProps => ({
  fetchPaymentMethods(data = false): void {
    dispatch(paymentActions.fetchPaymentInfoRequested(data));
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  },
  fetchCheckoutAddressLoading(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddressLoading(data));
  }
});

export default connect<
  IComponentStateProps,
  ICompDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(PaymentMethods);
