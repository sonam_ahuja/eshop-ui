import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledPayment = styled.div`
  display: flex;

  .mainContent {
    width: 100%;
    /* 80px is the height of progress stepper */
    min-height: calc(100vh - 80px);
    background: ${colors.coldGray};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    height: 100%;
    .mainContent {
      min-height: auto;
      display: flex;
      flex-direction: column;
    }
  }
`;
