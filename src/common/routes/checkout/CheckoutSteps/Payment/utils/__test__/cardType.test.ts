import getCardType from '@checkout/CheckoutSteps/Payment/utils/cardType';

describe('<getCardType />', () => {
  test('visa number testing', () => {
    expect(getCardType('4111111111111111')).toBe('Visa');
  });
  test('American Express number testing', () => {
    expect(getCardType('371449635398431')).toBe('American Express');
  });
  test('MasterCard number testing', () => {
    expect(getCardType('5555555555554444')).toBe('MasterCard');
  });
  test('Discover number testing', () => {
    expect(getCardType('6011000990139424')).toBe('Discover');
  });
  test('random number testing', () => {
    expect(getCardType('12233445')).toBe('');
  });
});
