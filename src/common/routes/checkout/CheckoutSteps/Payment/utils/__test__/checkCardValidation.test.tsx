import { savedCardIndex } from '@src/common/routes/checkout/CheckoutSteps/Payment/utils/checkCardValidation';
import { cardDetails } from '@mocks/checkout/payment';

describe('<getCardType />', () => {
  test('savedCardIndex test', () => {
    expect(savedCardIndex([cardDetails])).toBeDefined();
  });

  test('savedCardIndex test', () => {
    expect(
      savedCardIndex([{ ...cardDetails, isDefault: false }])
    ).toBeDefined();
  });
});
