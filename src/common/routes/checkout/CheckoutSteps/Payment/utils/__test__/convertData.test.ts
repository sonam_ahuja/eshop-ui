import {
  cardNumberModify,
  changeDefaultCard,
  getAddingNewCardIndex,
  getDefaultCard,
  localToMainConversion,
  mainToLocalConversion,
  patchPayloadForOtherPayment,
  patchPayloadForPayment,
  payloadForUpdateMonthlyCard,
  payloadForUpdateUpfrontCard,
  postPayloadForPayment
} from '@checkout/CheckoutSteps/Payment/utils/convertData';
import { addNewCard, cardDetails } from '@mocks/checkout/payment';
import { PAYMENT_TYPE } from '@common/routes/checkout/store/enums';

describe('<convertData />', () => {
  test('localToMainConversion test', () => {
    expect(localToMainConversion(addNewCard)).toBeDefined();
  });

  test('cardNumberModify test', () => {
    expect(cardNumberModify('12345')).toBeDefined();
  });
  test('cardNumberModify test with empty string', () => {
    expect(cardNumberModify('')).toBeDefined();
  });
  test('patchPayloadForOtherPayment test with empty string', () => {
    expect(
      patchPayloadForOtherPayment('1', '2', PAYMENT_TYPE.BANK_ACCOUNT)
    ).toBeDefined();
  });

  test('postPayloadForPayment test', () => {
    expect(
      postPayloadForPayment(addNewCard, 'debit', PAYMENT_TYPE.BANK_ACCOUNT)
    ).toBeDefined();
  });

  test('patchPayloadForPayment test', () => {
    expect(
      patchPayloadForPayment(
        [cardDetails],
        'debit',
        PAYMENT_TYPE.CREDIT_DEBIT_CARD
      )
    ).toBeDefined();
  });

  test('mainToLocalConversion test', () => {
    expect(mainToLocalConversion(addNewCard, cardDetails)).toBeDefined();
  });

  test('changeDefaultCard test', () => {
    expect(changeDefaultCard([cardDetails], cardDetails)).toBeDefined();
  });

  test('getDefaultCard test', () => {
    expect(getDefaultCard([cardDetails])).toBeDefined();
  });

  test('getDefaultCard test when not avialbe', () => {
    expect(
      getDefaultCard([{ ...cardDetails, isDefault: false }])
    ).toBeUndefined();
  });

  test('getAddingNewCardIndex test', () => {
    expect(getAddingNewCardIndex([cardDetails])).toBeDefined();
  });

  test('getAddingNewCardIndex test with dummy card', () => {
    const dummyCardDetails = { ...cardDetails, brand: 'dummy' };
    expect(getAddingNewCardIndex([dummyCardDetails])).toBeDefined();
  });
  test('payloadForUpdateUpfrontCard test', () => {
    const upFrontData = {
      cardDetails: [cardDetails],
      saveNewCard: false,
      showEditIcon: false,
      proceedDisabled: false,
      addingNewCard: false,
      id: '32',
      paymentTypeSelected: PAYMENT_TYPE.BANK_ACCOUNT,
      enabledPaymentMethods: { tokenizedCard: false, payByLink: false, payOnDelivery: false, manualPayment: false, bankAccount: false }
    };
    expect(payloadForUpdateUpfrontCard(upFrontData)).toBeDefined();
    expect(
      // tslint:disable-next-line:max-line-length
      payloadForUpdateMonthlyCard({ ...upFrontData, useSameCard: false, enabledPaymentMethods: { tokenizedCard: false, payByLink: false, payOnDelivery: false, manualPayment: false, bankAccount: false } })
    ).toBeDefined();
  });
});
