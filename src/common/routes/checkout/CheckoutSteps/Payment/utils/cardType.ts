export default function getCardType(cardNumber: string): string {
  let re = new RegExp('^4');
  if (cardNumber.match(re) != null) {
    return 'Visa';
  }

  re = new RegExp('^(34|37)');
  if (cardNumber.match(re) != null) { return 'American Express'; }

  re = new RegExp('^5[1-5]');
  if (cardNumber.match(re) != null) { return 'MasterCard'; }

  re = new RegExp('^6011');
  if (cardNumber.match(re) != null) { return 'Discover'; }

  return '';
}
