import { ICardDetails } from '@checkout/store/payment/types';

export function savedCardIndex(savedCards: ICardDetails[]): number {
  let returnIndex = 0;

  savedCards.forEach((item: ICardDetails, index: number) => {
    if (item.isDefault) {
      returnIndex = index;
    }
  });

  return returnIndex;
}
