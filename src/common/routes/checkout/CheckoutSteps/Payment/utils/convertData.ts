import {
  IAddNewCard,
  ICardDetails,
  IMonthly,
  IPaymentInfoPatchReq,
  IUpFront
} from '@checkout/store/payment/types';
import { PAYMENT_TYPES } from '@src/common/types/modules/common';
import { PAYMENT_TYPE } from '@common/routes/checkout/store/enums';

export function localToMainConversion(cardDetail: IAddNewCard): ICardDetails {
  return {
    cardNumber: cardDetail.cardNumber.value,
    nameOnCard: cardDetail.nameOnCard.value,
    cardType: cardDetail.cardType,
    isDefault: cardDetail.isDefault,
    isCardSaved: cardDetail.isCardSaved,
    expiryDate: cardDetail.expiryDate.value,
    securityCode: cardDetail.securityCode.value,
    id: '0',
    brand: cardDetail.brand,
    lastFourDigits: cardDetail.cardNumber.value.slice(12, 16)
  };
}

export function cardNumberModify(payload: string): string {
  let result = '';
  if (payload) {
    const splitData = payload.split(' ');
    result = splitData[0] + splitData[1] + splitData[2] + splitData[3];
  }

  return result;
}

export function postPayloadForPayment(
  cardDetail: IAddNewCard,
  paymentType: string,
  optionType: PAYMENT_TYPE
): IPaymentInfoPatchReq {
  return {
    id: '',
    nonce: cardDetail.nonce,
    type: paymentType,
    saveCard: cardDetail.isCardSaved,
    selectedPaymentMethod: optionType
  };
}

export function patchPayloadForPayment(
  cardDetails: ICardDetails[],
  paymentType: string,
  optionType: PAYMENT_TYPE
): IPaymentInfoPatchReq {
  const selectedCard = getDefaultCard(cardDetails);

  return {
    id: selectedCard.id,
    type: paymentType,
    selectedPaymentMethod: optionType
  };
}

export function patchPayloadForOtherPayment(
  id: string,
  paymentType: string,
  optionType: PAYMENT_TYPE
): IPaymentInfoPatchReq {
  return {
    id,
    type: paymentType,
    selectedPaymentMethod: optionType
  };
}

export function mainToLocalConversion(
  localCardDetail: IAddNewCard,
  mainCardDetail: ICardDetails
): IAddNewCard {
  localCardDetail.cardNumber.value = mainCardDetail.cardNumber;
  localCardDetail.expiryDate.value = mainCardDetail.expiryDate;
  localCardDetail.nameOnCard.value = mainCardDetail.nameOnCard;
  localCardDetail.securityCode.value = mainCardDetail.securityCode;
  localCardDetail.cardType = mainCardDetail.cardType;
  localCardDetail.cardId = mainCardDetail.id;
  localCardDetail.lastFourDigits = mainCardDetail.lastFourDigits;

  localCardDetail.cardNumber.isValid = true;
  localCardDetail.expiryDate.isValid = true;
  localCardDetail.nameOnCard.isValid = true;
  localCardDetail.securityCode.isValid = true;
  localCardDetail.isDefault = true;
  localCardDetail.brand = mainCardDetail.brand;

  return localCardDetail;
}

export function changeDefaultCard(
  cardList: ICardDetails[],
  selectedCard: ICardDetails
): ICardDetails[] {
  cardList.forEach((item: ICardDetails) => {
    item.isDefault = item.id === selectedCard.id;
  });

  return cardList;
}

export function getDefaultCard(cardList: ICardDetails[]): ICardDetails {
  let selectedIndex = -1;

  cardList.forEach((item: ICardDetails, index: number) => {
    if (item.isDefault === true) {
      selectedIndex = index;
    }
  });

  return cardList[selectedIndex];
}

export function getAddingNewCardIndex(cardList: ICardDetails[]): number | null {
  let selectedIndex = null;

  cardList.forEach((item: ICardDetails, index: number) => {
    if (item.brand === 'dummy') {
      selectedIndex = index;
    }
  });

  return selectedIndex;
}

export function payloadForUpdateUpfrontCard(
  data: IUpFront
): IPaymentInfoPatchReq {
  const selectedCard = getDefaultCard(data.cardDetails);

  return {
    id: selectedCard.id,
    type: PAYMENT_TYPES.UPFRONT,
    selectedPaymentMethod: data.paymentTypeSelected
  };
}

export function payloadForUpdateMonthlyCard(
  data: IMonthly
): IPaymentInfoPatchReq {
  const selectedCard = getDefaultCard(data.cardDetails);

  return {
    id: selectedCard.id,
    type: PAYMENT_TYPES.MONTHLY,
    selectedPaymentMethod: data.paymentTypeSelected
  };
}
