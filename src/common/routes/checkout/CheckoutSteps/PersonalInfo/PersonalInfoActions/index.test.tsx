import React from 'react';
import { shallow } from 'enzyme';
import appState from '@store/states/app';

import FormFooter, { IProps as IFormFooterProps } from '.';

const props: IFormFooterProps = {
  personalInfoTranslation: appState().translation.cart.checkout.personalInfo,
  personalInfo: appState().checkout.personalInfo,
  checkoutConfiguration: appState().configuration.cms_configuration.modules
    .checkout,
  setSmsNotification: jest.fn(),
  setPersonalInfoRequested: jest.fn(),
  updatePersonalInfoRequested: jest.fn(),
  updateInputField: jest.fn(),
  setProceedToShippingButton: jest.fn(),
  globalConfiguration: appState().configuration.cms_configuration.global
};

describe('<FormFooter />', () => {
  test('form footer should render properly', () => {
    const component = shallow<FormFooter>(<FormFooter {...props} />);

    expect(component).toMatchSnapshot();
  });
  test('test should simulate proceed tp shipping when showSmsNotificationSwitch is false', () => {
    appState().configuration.cms_configuration.modules.checkout.orderNotification.sms = false;
    const component = shallow<FormFooter>(<FormFooter {...props} />);
    // tslint:disable-next-line: no-any
    const newSyntheticEvent: any = { target: {}, preventDefault: jest.fn() };
    component.instance().onProceedClick(newSyntheticEvent);

    expect(component).toMatchSnapshot();
  });

  test('test should simulate proceed tp shipping when showSmsNotificationSwitch is true', () => {
    const component = shallow<FormFooter>(<FormFooter {...props} />);
    // tslint:disable-next-line: no-any
    const newSyntheticEvent: any = { target: {}, preventDefault: jest.fn() };
    component.instance().onProceedClick(newSyntheticEvent);

    expect(component).toMatchSnapshot();
  });

  test('test should simulate proceed tp shipping when showSmsNotificationSwitch is true', () => {
    const component = shallow<FormFooter>(<FormFooter {...props} />);
    const updateProps = props.personalInfo;
    updateProps.isPersonalInfoPrefilled = true;
    component.setProps({
      personalInfo: updateProps
    });
    component.update();
    // tslint:disable-next-line: no-any
    const newSyntheticEvent: any = { target: {}, preventDefault: jest.fn() };
    component.instance().onProceedClick(newSyntheticEvent);

    expect(component).toMatchSnapshot();
  });
  test('test should simulate handle toggle ', () => {
    appState().configuration.cms_configuration.modules.checkout.orderNotification.sms = false;
    const updateProps = props;

    const component = shallow<FormFooter>(<FormFooter {...updateProps} />);

    component.instance().handleSmsNotification();

    expect(component).toMatchSnapshot();
  });

  test('switch should render properly when sms is true ', () => {
    const updateProps = { ...props };
    updateProps.checkoutConfiguration.orderNotification.sms = false;
    const component = shallow<FormFooter>(<FormFooter {...updateProps} />);
    expect(component).toMatchSnapshot();
  });
});
