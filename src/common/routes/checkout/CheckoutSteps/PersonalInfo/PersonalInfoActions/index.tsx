import React from 'react';
import { Button, Section, Switch } from 'dt-components';
import { IPersonalInfoTranslation } from '@common/store/types/translation';
import {
  ICheckout,
  IGlobalConfiguration
} from '@common/store/types/configuration';
import {
  IInputKeyValue,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';
import * as personalInfoApi from '@common/types/api/checkout/personalInfo';
import {
  composeSetApiBody,
  composeUpdateApiBody
} from '@checkout/store/selector';
import store from '@common/store';
import EVENT_NAME from '@events/constants/eventName';

import { StyledPersonalInfoActions, SwitchContentWrap } from './styles';

export interface IProps {
  checkoutConfiguration: ICheckout;
  personalInfo: IPersonalInfoState;
  personalInfoTranslation: IPersonalInfoTranslation;
  globalConfiguration: IGlobalConfiguration;
  setPersonalInfoRequested(data: personalInfoApi.POST.IRequest): void;
  updatePersonalInfoRequested(data: personalInfoApi.PATCH.IRequest): void;
  updateInputField(data: IInputKeyValue): void;
  setSmsNotification(): void;
  setProceedToShippingButton(data: IInputKeyValue): void;
}

class PersonalInfoActions extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.onProceedClick = this.onProceedClick.bind(this);
    this.switchEl = this.switchEl.bind(this);
    this.handleSmsNotification = this.handleSmsNotification.bind(this);
  }

  onProceedClick(event: React.SyntheticEvent): void {
    const {
      setPersonalInfoRequested,
      personalInfo,
      updatePersonalInfoRequested
    } = this.props;
    const { isPersonalInfoPrefilled } = personalInfo;

    event.preventDefault();
    if (isPersonalInfoPrefilled) {
      const updateBody = composeUpdateApiBody(this.props);
      updatePersonalInfoRequested(updateBody);
    } else {
      const data = composeSetApiBody(this.props);
      setPersonalInfoRequested(data);
    }
  }

  handleSmsNotification(): void {
    const { setSmsNotification } = this.props;

    setSmsNotification();
  }

  switchEl(): JSX.Element {
    const { checkoutConfiguration } = this.props;
    const {
      sms: isSmsNotificationEnable
    } = checkoutConfiguration.orderNotification;

    const {
      isSmsEnabled,
      smsNotificationName,
      smsNotificationDesc
    } = this.props.personalInfo;

    return (
      <SwitchContentWrap>
        <Section size='large' className='textWrap'>
          <div className='viaEmail'>{smsNotificationDesc}</div>
          <div className='viaSms'>{smsNotificationName}</div>
        </Section>
        <Switch
          disabled={!isSmsNotificationEnable}
          on={isSmsEnabled}
          onToggle={this.handleSmsNotification}
        />
      </SwitchContentWrap>
    );
  }

  render(): JSX.Element {
    const {
      checkoutConfiguration,
      personalInfoTranslation,
      personalInfo,
      globalConfiguration
    } = this.props;
    const { enableProceedButton } = this.props.personalInfo;
    const showSmsNotificationSwitch = checkoutConfiguration;
    const isCreditCheckRequired =
      store.getState().checkout.checkout.isCreditCheckRequired &&
      globalConfiguration.creditCheckRequired;

    return (
      <StyledPersonalInfoActions>
        {showSmsNotificationSwitch.orderNotification.sms
          ? this.switchEl()
          : null}

        <div className='btnAndConsentTextWrap'>
          <Button
            size='medium'
            disabled={!enableProceedButton}
            loading={personalInfo.proceedLoader}
            type='primary'
            onClickHandler={this.onProceedClick}
            data-event-id={
              !isCreditCheckRequired
                ? EVENT_NAME.CHECKOUT.EVENTS.PROCEED_TO_SHIPPING
                : EVENT_NAME.CHECKOUT.EVENTS.PROCEED_TO_CREDIT
            }
            data-event-message={
              !isCreditCheckRequired
                ? personalInfoTranslation.proceedToShipping
                : personalInfoTranslation.proceedToCreditButton
            }
          >
            {!isCreditCheckRequired
              ? personalInfoTranslation.proceedToShipping
              : personalInfoTranslation.proceedToCreditButton}
          </Button>

          {isCreditCheckRequired ? (
            <Section size='large' weight='normal' className='consentText'>
              {personalInfoTranslation.consentText}
            </Section>
          ) : null}
        </div>
      </StyledPersonalInfoActions>
    );
  }
}

export default PersonalInfoActions;
