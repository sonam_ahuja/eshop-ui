import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const SwitchContentWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 3rem;

  .textWrap {
    .viaEmail {
      color: ${colors.mediumGray};
    }
    .viaSms {
      color: ${colors.darkGray};
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    margin-top: 0.5rem;
    flex-direction: row-reverse;
    .textWrap {
      margin-left: 0.75rem;
    }
  }
`;

export const StyledPersonalInfoActions = styled.div`
  .dt_button {
    width: 100%;
    margin-top: 2.5rem;
  }

  .consentText {
    text-align: center;
    padding: 0.75rem;
    padding-bottom: 0;
    color: ${colors.mediumGray};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    margin-top: auto;
    ${SwitchContentWrap} {
      margin-top: 3.4375rem;
    }
    .dt_button {
      margin-top: 2.8125rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    padding-top: 3rem;

    .dt_button {
      margin: 0;
      height: 2.5rem;
      font-size: 0.875rem;
      line-height: 1.25rem;
      letter-spacing: -0.08px;
    }
    .consentText {
      font-size: 0.75rem;
      line-height: 1rem;
      letter-spacing: 0.24px;
      text-align: left;
      margin-top: 0.75rem;
      color: ${colors.mediumGray};
      padding: 0;
    }
    .btnAndConsentTextWrap {
      justify-self: flex-end;
      margin-left: auto;
    }
    ${SwitchContentWrap} {
      margin-top: 0;
      flex-direction: column-reverse;
      align-items: flex-start;
      padding-right: 2.1875rem;
      .textWrap {
        margin-left: 0;
        margin-top: 0.5rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    align-items: center;
    ${SwitchContentWrap} {
      flex-direction: row-reverse;
      align-items: center;
      .textWrap {
        margin-left: 0.75rem;
        margin-top: 0;
        font-size: 0.625rem;
        line-height: 0.75rem;
        letter-spacing: 0;
      }
    }
    .dt_button {
      height: 3rem;
      font-size: 1rem;
      line-height: 1.5rem;
      letter-spacing: -0.11px;
    }
    .consentText {
      font-size: 0.625rem;
      line-height: 0.75rem;
      letter-spacing: 0;
      margin-top: 0.25rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .dt_button {
      height: 2.5rem;
      font-size: 0.875rem;
      letter-spacing: 0;
      line-height: 1.25rem;
    }
  }
`;
