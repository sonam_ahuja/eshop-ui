import React from 'react';
import { mount, shallow } from 'enzyme';
import appState from '@store/states/app';
import { ThemeProvider } from 'dt-components';

import PersonalInfoContent, { IProps as IPersonalInfoContentProps } from '.';

const props: IPersonalInfoContentProps = {
  selectedNumber: {
    id: '1',
    displayId: '1',
    selected: true
  },
  portingNumber: '99980764596',
  isCreditCheckRequired: true,
  setFlowType: jest.fn(),
  showCancelModal: jest.fn(),
  migrationType: 'migrationType',
  desiredDate: '20/12/2019',
  banerImageUrl: '',
  showNumberPortModal: jest.fn(),
  setNumberPortingStatus: jest.fn(),
  enableProceedButtonFunc: jest.fn(),
  setPersonalInfoRequested: jest.fn(),
  updatePersonalInfoRequested: jest.fn(),
  requestForPortingNumber: jest.fn(),
  requestForCancelPortingNumber: jest.fn(),
  setSmsNotification: jest.fn(),
  personalInfo: appState().checkout.personalInfo,
  checkoutTranslation: appState().translation.cart.checkout,
  checkoutConfiguration: appState().configuration.cms_configuration.modules
    .checkout,
  personalInfoTranslation: appState().translation.cart.checkout.personalInfo,
  updateInputField: jest.fn(),
  setProceedToShippingButton: jest.fn(),
  syncDataInStore: jest.fn(),
  setActiveStep: jest.fn(),
  showNumberPortingBanner: false,
  globalConfiguration: appState().configuration.cms_configuration.global
};
window.scrollTo = jest.fn();

describe('<PersonalInfoContent />', () => {
  test('should render properly', () => {
    const component = shallow<PersonalInfoContent>(
      <PersonalInfoContent {...props} />
    );
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    const component = mount<PersonalInfoContent>(
      <ThemeProvider theme={{}}>
        <PersonalInfoContent {...copyComponentProps} />
      </ThemeProvider>
    );
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).setProceedButton(
      'value',
      'value',
      true,
      'value',
      true
    );
    (component
      .find('PersonalInfoContent')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as PersonalInfoContent).handleChange('streetNumber', {
      target: { value: 'value' },
      // tslint:disable-next-line: no-empty
      preventDefault: () => {}
    } as React.ChangeEvent<HTMLInputElement>);
    (component
      .find('PersonalInfoContent')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as PersonalInfoContent).handleKeydown('streetNumber', {
      currentTarget: { value: 'value' },
      // tslint:disable-next-line: no-empty
      preventDefault: () => {},
      keyCode: 9
    } as React.KeyboardEvent<HTMLInputElement>);
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).setSearchAddress({
      id: '1',
      streetNumber: '23',
      address: 'string',
      flatNumber: 'string',
      city: 'string',
      postCode: 'string',
      deliveryNote: 'string'
    });
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).checkForGoogleSuggestionFlag({
      id: '1',
      streetNumber: '23',
      address: 'string',
      title: 'string',
      flatNumber: 'string',
      city: 'string',
      postCode: 'string',
      deliveryNote: 'string'
    });
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).onBlur(
      'address',
      // tslint:disable-next-line: no-object-literal-type-assertion
      { target: { value: 'string' } } as React.FocusEvent<HTMLInputElement>
    );
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).onFocus(
      'address',
      // tslint:disable-next-line: no-object-literal-type-assertion
      { currentTarget: { value: 'string' } } as React.FocusEvent<
        HTMLInputElement
      >
    );
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).setProceedButton(
      'field',
      'inputValue',
      true,
      'validationMessage',
      true
    );
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).setProceedButton(
      'field',
      'inputValue',
      true,
      'validationMessage',
      false
    );
    (component
      .find('PersonalInfoContent')
      .instance() as PersonalInfoContent).componentWillUnmount();
  });

  test('should render properly, on new props', () => {
    const component = shallow<PersonalInfoContent>(
      <PersonalInfoContent {...props} />
    );
    component.setProps(props);
    component.update();
    expect(component).toMatchSnapshot();
  });
});
