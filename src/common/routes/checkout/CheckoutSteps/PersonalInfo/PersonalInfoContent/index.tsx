// tslint:disable:max-file-line-count
import {
  ICheckoutTranslation,
  IPersonalInfoTranslation
} from '@common/store/types/translation';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import { Autocomplete, FloatLabelInput } from 'dt-components';
import {
  ILocalFormState,
  ILocalState
} from '@checkout/CheckoutSteps/PersonalInfo/formGrid';
import PERSONAL_INFO_CONSTANTS from '@checkout/CheckoutSteps/PersonalInfo/constants';
import { getAddresses } from '@checkout/store/personalInfo/services';
import { Column, Row } from '@common/components/Grid/styles';
import {
  charEPresent,
  formValidAsPerSmsNotification,
  getCurrentDate,
  getFieldsToBeShown,
  getLabelValueForFormFields,
  ignoreSpaces,
  isFormFieldValidAsPerType,
  isInputAllowed,
  readyToProceed
} from '@checkout/CheckoutSteps/PersonalInfo/utils';
import React, { Component, ReactNode } from 'react';
import {
  IAddressResponse,
  IInputKeyValue,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';
import {
  ICheckout,
  IGlobalConfiguration
} from '@common/store/types/configuration';
import { ILocalFormStateWithStatus } from '@checkout/store/personalInfo/actions';
import * as personalInfoApi from '@common/types/api/checkout/personalInfo';
import {
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES
} from '@checkout/store/numberPorting/enum';
import { IResourceNumber } from '@checkout/store/numberPorting/types';
import { CONSTANTS } from '@checkout/CheckoutSteps/constants';
import NumberPortingBanner from '@checkout/CheckoutSteps/NumberPorting/NumberPortingBanner';
import PersonalInfoActions from '@checkout/CheckoutSteps/PersonalInfo/PersonalInfoActions';
import { Moment } from 'moment';
import { isMobile } from '@src/common/utils';
import { fetchGeoMetadata } from '@checkout/utils/geoDataConverter';
import { IListItem } from '@src/common/routes/checkout/store/shipping/types';
import { sendContentFilledOutEvent } from '@events/checkout';
import { FORM_NAME } from '@events/constants/eventName';

import { Form } from './styles';

export interface IProps {
  checkoutConfiguration: ICheckout;
  personalInfo: IPersonalInfoState;
  checkoutTranslation: ICheckoutTranslation;
  personalInfoTranslation: IPersonalInfoTranslation;
  selectedNumber: IResourceNumber;
  migrationType: string;
  desiredDate?: string;
  banerImageUrl: string;
  isCreditCheckRequired: boolean;
  portingNumber: string;
  showNumberPortingBanner: boolean;
  globalConfiguration: IGlobalConfiguration;
  updateInputField(data: IInputKeyValue): void;
  setProceedToShippingButton(data: IInputKeyValue): void;
  syncDataInStore(data: ILocalFormStateWithStatus): void;
  enableProceedButtonFunc(): void;
  showNumberPortModal(status: boolean): void;
  setFlowType(flowType: NUMBER_PORTING_TYPES): void;
  showCancelModal(status: boolean): void;
  setNumberPortingStatus(status: boolean): void;
  setPersonalInfoRequested(data: personalInfoApi.POST.IRequest): void;
  updatePersonalInfoRequested(data: personalInfoApi.PATCH.IRequest): void;
  setSmsNotification(): void;
  setActiveStep(step: NUMBER_PORTING_STEPS): void;
  requestForPortingNumber(payload: numberPortTypesApi.PATCH.IRequest): void;
  requestForCancelPortingNumber(
    payload: numberPortTypesApi.PATCH.IRequest
  ): void;
}

type IState = ILocalState;
type proceedButtonType = (
  field: string,
  inputValue: string,
  isValidState: boolean,
  validationMessage: string,
  status: boolean
) => void;

const debounceFn = (fn: proceedButtonType) => {
  const map = {};

  return (
    field: string,
    inputValue: string,
    isValidState: boolean,
    validationMessage: string,
    status: boolean
  ) => {
    if (map[field]) {
      clearTimeout(map[field]);
    }
    const timeout = setTimeout(
      fn,
      250,
      field,
      inputValue,
      isValidState,
      validationMessage,
      status
    );
    map[field] = timeout;
  };
};

class PersonalInfoContent extends Component<IProps, IState> {
  filledOffSet: { name: string; offsetTop: number }[] = [];

  constructor(props: IProps) {
    super(props);
    this.state = {
      id: '',
      isDefault: true,
      isSmsEnabled: false,
      enableProceedButton: false,
      isFormChanged: false,
      isPersonalInfoPrefilled: false,
      formFields: getFieldsToBeShown(
        this.props.checkoutConfiguration.form.fields,
        this.props.isCreditCheckRequired
      )
    };
    this.createForm = this.createForm.bind(this);
    this.setSearchAddress = this.setSearchAddress.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.setProceedButton = debounceFn(this.setProceedButton.bind(this));
    this.checkForGoogleSuggestionFlag = this.checkForGoogleSuggestionFlag.bind(
      this
    );
    this.dateChange = this.dateChange.bind(this);
    this.checkIsKeyBoardOpen = this.checkIsKeyBoardOpen.bind(this);
  }

  componentDidMount(): void {
    const newFormField: ILocalFormState = {};
    const { formFields: updatedFormFields } = this.props.personalInfo;

    Object.keys(this.state.formFields).forEach(key => {
      newFormField[key] = { ...this.state.formFields[key] };
      newFormField[key].value = updatedFormFields[key]
        ? updatedFormFields[key]
        : '';
    });
    this.setState(
      state => ({
        ...state,
        ...this.props.personalInfo,
        formFields: newFormField
      }),
      () => {
        this.props.enableProceedButtonFunc();
      }
    );
    this.checkIsKeyBoardOpen();
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const { formFields: updatedFormFields } = nextProps.personalInfo;

    const newFormField: ILocalFormState = {};

    // tslint:disable-next-line:no-identical-functions
    Object.keys(this.state.formFields).forEach(key => {
      newFormField[key] = { ...this.state.formFields[key] };
      newFormField[key].value = updatedFormFields[key]
        ? updatedFormFields[key]
        : '';
    });
    if (
      nextProps.personalInfo.isSmsEnabled !==
        this.props.personalInfo.isSmsEnabled &&
      nextProps.personalInfo.isSmsEnabled
    ) {
      const {
        fields: cmsFormConfiguration
      } = this.props.checkoutConfiguration.form;
      const specificConfiguration =
        cmsFormConfiguration[CONSTANTS.PHONE_NUMBER];

      if (newFormField.phoneNumber) {
        newFormField.phoneNumber.isValid = isFormFieldValidAsPerType(
          specificConfiguration.validation.value,
          nextProps.personalInfo.formFields.phoneNumber,
          specificConfiguration.validation.type
        );

        if (!newFormField.phoneNumber.isValid) {
          newFormField.phoneNumber.validationMessage =
            specificConfiguration.validation.message;
        }
      }
    } else if (newFormField.phoneNumber && !newFormField.phoneNumber.value) {
      newFormField.phoneNumber.isValid = true;
    }

    this.setState(
      state => ({
        ...state,
        ...nextProps.personalInfo,
        formFields: newFormField
      }),
      () => {
        this.props.enableProceedButtonFunc();
      }
    );
  }

  componentWillUnmount(): void {
    window.removeEventListener('resize', this.checkIsKeyBoardOpen);
  }

  checkForGoogleSuggestionFlag(data: IListItem): void {
    const { enableGoogleAddressSuggestion } = this.props.globalConfiguration;

    if (enableGoogleAddressSuggestion) {
      fetchGeoMetadata(data.id as string, data.title).then(response => {
        this.setSearchAddress(response);
      });
    } else {
      this.setSearchAddress(data as IAddressResponse);
    }
  }

  setProceedButton(
    field: string,
    inputValue: string,
    isValidState: boolean,
    validationMessage: string,
    status: boolean
  ): void {
    this.props.setProceedToShippingButton({
      key: field,
      value: inputValue,
      isValid: isValidState,
      validationMessage,
      status
    });
  }

  handleKeydown(
    field: string,
    event: React.KeyboardEvent<HTMLInputElement>
  ): void {
    if (ignoreSpaces(event)) {
      event.preventDefault();
    }
    if (
      this.props.checkoutConfiguration.form.fields[field].inputType ===
        PERSONAL_INFO_CONSTANTS.NUMBER &&
      charEPresent(event)
    ) {
      event.preventDefault();

      return;
    }
    if (
      field === PERSONAL_INFO_CONSTANTS.DOB &&
      event.keyCode !== 9 &&
      event.keyCode
    ) {
      event.preventDefault();
    }
  }

  handleChange(
    field: string,
    event: React.ChangeEvent<HTMLInputElement>
  ): void {
    const inputValue = (event.target as HTMLInputElement).value;
    this.updateChange(field, inputValue);
  }

  updateChange(field: string, inputValue: string): void {
    const {
      fields: cmsFormConfiguration
    } = this.props.checkoutConfiguration.form;

    const specificConfiguration = cmsFormConfiguration[field];
    if (
      !isInputAllowed(
        specificConfiguration.inputType,
        inputValue,
        specificConfiguration.validation
      )
    ) {
      return;
    }

    let isValidValue = isFormFieldValidAsPerType(
      specificConfiguration.validation.value,
      inputValue,
      specificConfiguration.validation.type
    );

    if (
      isValidValue &&
      this.state.formFields.phoneNumber &&
      field === PERSONAL_INFO_CONSTANTS.PHONE_NUMBER
    ) {
      isValidValue = formValidAsPerSmsNotification(
        this.props.personalInfo.isSmsEnabled,
        this.state.formFields.phoneNumber.value
      );
    }

    this.setState(
      state => ({
        ...state,
        formFields: {
          ...state.formFields,
          [field]: {
            ...this.state.formFields[field],
            value: inputValue,
            isValid: isValidValue,
            validationMessage: specificConfiguration.validation.message
          }
        }
      }),
      () => {
        const status = readyToProceed(
          this.state.formFields,
          cmsFormConfiguration
        );

        this.setProceedButton(
          field,
          inputValue,
          isValidValue,
          specificConfiguration.validation.message,
          status && isValidValue
        );
      }
    );
  }

  setSearchAddress(searchAddressResponse: IAddressResponse): void {
    const newFormFields = { ...this.state.formFields };
    const {
      fields: cmsFormConfiguration
    } = this.props.checkoutConfiguration.form;

    Object.keys(searchAddressResponse).forEach(field => {
      const specificConfiguration = cmsFormConfiguration[field];
      if (newFormFields[field]) {
        newFormFields[field].value = searchAddressResponse[field];
        newFormFields[field].isValid =
          !specificConfiguration.mandatory &&
          searchAddressResponse[field].length === 0
            ? true
            : isFormFieldValidAsPerType(
                specificConfiguration.validation.value,
                searchAddressResponse[field],
                specificConfiguration.validation.type
              );
      }
    });

    this.setState(
      state => ({
        ...state,
        formFields: newFormFields
      }),
      () => {
        const status = readyToProceed(
          this.state.formFields,
          cmsFormConfiguration
        );

        this.props.syncDataInStore({
          ...newFormFields,
          status
        });
      }
    );
  }

  onBlur(field: string, event: React.ChangeEvent<HTMLInputElement>): void {
    const inputValue = (event.target as HTMLInputElement).value;
    sendContentFilledOutEvent(
      FORM_NAME.PERSONAL_INFO,
      getLabelValueForFormFields(this.props, field)
    );
    if (inputValue === '') {
      const {
        fields: cmsFormConfiguration
      } = this.props.checkoutConfiguration.form;

      const specificConfiguration = cmsFormConfiguration[field];

      const isSmsEnabled = this.props.personalInfo.isSmsEnabled;

      if (isSmsEnabled && field === 'phoneNumber') {
        return;
      }
      if (!specificConfiguration.mandatory) {
        this.setState(state => ({
          ...state,
          formFields: {
            ...state.formFields,
            [field]: {
              ...this.state.formFields[field],
              value: inputValue,
              isValid: true,
              validationMessage: specificConfiguration.validation.message
            }
          }
        }));
      }
    }
  }

  checkIsKeyBoardOpen(): void {
    const originalSize = window.innerHeight + window.innerWidth;
    const summary = document.querySelector('.isSticky') as HTMLElement;

    window.addEventListener('resize', () => {
      const resizeValue = window.innerHeight + window.innerWidth;

      if (summary) {
        !(resizeValue > originalSize - 150 && resizeValue < originalSize + 150)
          ? (summary.style.display = 'none')
          : (summary.style.display = '');
      }
    });
  }

  onFocus(field: string, event: React.FocusEvent<HTMLInputElement>): void {
    if (event) {
      this.filledOffSet.forEach(
        (element: { name: string; offsetTop: number }) => {
          if (
            element.name === field &&
            element.offsetTop > window.screen.height / 2
          ) {
            window.scrollTo({
              top: element.offsetTop / 1.5,
              behavior: 'smooth'
            });
          }
        }
      );
    }
  }

  floatLabelReference(
    name: string,
    element: HTMLDivElement | null,
    index: number
  ): void {
    if (element) {
      this.filledOffSet[index] = { name, offsetTop: element.offsetTop };
    }
  }

  dateChange(field: string, value: Moment | string): void {
    this.updateChange(field, (value as Moment).format('DD/MM/YYYY'));
  }

  checkContentEditable = (key: string) => {
    const {
      fields: cmsFormConfiguration
    } = this.props.checkoutConfiguration.form;

    if (
      cmsFormConfiguration[key] &&
      cmsFormConfiguration[key].inputType === 'date' &&
      isMobile.androidPhone
    ) {
      return true;
    } else {
      return !(
        cmsFormConfiguration[key].readOnly && this.state.isPersonalInfoPrefilled
      );
    }
  }

  // tslint:disable-next-line:cognitive-complexity
  createForm(): ReactNode | ReactNode[] {
    const { formFields } = this.state;
    const {
      fields: cmsFormConfiguration
    } = this.props.checkoutConfiguration.form;
    const {checkoutTranslation} = this.props;

    const sortedFormFields = Object.keys(formFields).map(key => ({
      ...formFields[key],
      key
    }));

    sortedFormFields.sort((a, b) => (a.order < b.order ? -1 : 1));

    return sortedFormFields.map((field, index) => {
      let useNativeCalendar = false;
      if (
        cmsFormConfiguration[field] &&
        cmsFormConfiguration[field].inputType === 'date' &&
        isMobile.phone
      ) {
        useNativeCalendar = true;
      }

      return (
        <Column
          key={field.key}
          ref={element => {
            this.floatLabelReference(field.key, element, index);
          }}
          colMobile={field.colMobile}
          colDesktop={field.colDesktop}
          orderMobile={cmsFormConfiguration[field.key].order}
          orderDesktop={cmsFormConfiguration[field.key].order}
          className={field.key}
        >
          {field.key === PERSONAL_INFO_CONSTANTS.ADDRESS ? (
            <Autocomplete
              label={getLabelValueForFormFields(this.props, field.key)}
              value={field.value}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                this.handleChange(field.key, event);
              }}
              onBlur={(event: React.ChangeEvent<HTMLInputElement>) => {
                this.onBlur(field.key, event);
              }}
              error={!field.isValid}
              errorMessage={field.validationMessage}
              tabIndex={cmsFormConfiguration[field.key].order}
              disabled={
                cmsFormConfiguration[field.key].readOnly &&
                this.state.isPersonalInfoPrefilled
              }
              getItems={getAddresses}
              onItemSelect={address =>
                this.checkForGoogleSuggestionFlag(address as IListItem)
              }
              debounceInterval={300}
              autoCompleteValuesOnly={false}
              loadingText={checkoutTranslation.loadingText}
            />
          ) : (
            <FloatLabelInput
              autoComplete='off'
              id={field.key}
              label={getLabelValueForFormFields(this.props, field.key)}
              type={cmsFormConfiguration[field.key].inputType}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                this.handleChange(field.key, event);
              }}
              onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
                this.handleKeydown(field.key, event);
              }}
              // onFocus={(event: React.FocusEvent<HTMLInputElement>) => {
              //   this.onFocus(field.key, event);
              // }}
              onBlur={(event: React.ChangeEvent<HTMLInputElement>) => {
                this.onBlur(field.key, event);
              }}
              max={
                cmsFormConfiguration[field.key].inputType === 'date'
                  ? getCurrentDate()
                  : ''
              }
              value={field.value}
              error={!field.isValid}
              errorMessage={field.validationMessage}
              tabIndex={cmsFormConfiguration[field.key].order}
              useNativeCalendar={useNativeCalendar}
              disabled={
                cmsFormConfiguration[field.key].readOnly &&
                this.state.isPersonalInfoPrefilled
              }
              dateFormat={'YYYY-MM-DD'}
              isMobile={isMobile.phone}
              contentEditable={this.checkContentEditable(field.key)}
            />
          )}
        </Column>
      );
    });
  }

  render(): ReactNode {
    const {
      personalInfo,
      checkoutConfiguration,
      checkoutTranslation,
      showNumberPortModal,
      selectedNumber,
      migrationType,
      desiredDate,
      setFlowType,
      showCancelModal,
      setNumberPortingStatus,
      setSmsNotification,
      updateInputField,
      setPersonalInfoRequested,
      updatePersonalInfoRequested,
      setProceedToShippingButton,
      banerImageUrl,
      setActiveStep,
      requestForPortingNumber,
      requestForCancelPortingNumber,
      portingNumber,
      showNumberPortingBanner,
      globalConfiguration
    } = this.props;

    return (
      <Form>
        <Row>{this.createForm()}</Row>
        {showNumberPortingBanner ? (
          <NumberPortingBanner
            className='personalInfo_numberPortingBanner'
            personalInfo={personalInfo}
            checkoutConfiguration={checkoutConfiguration}
            checkoutTranslation={checkoutTranslation}
            showNumberPortModal={showNumberPortModal}
            selectedNumber={selectedNumber}
            migrationType={migrationType}
            desiredDate={desiredDate}
            setFlowType={setFlowType}
            showCancelModal={showCancelModal}
            setNumberPortingStatus={setNumberPortingStatus}
            banerImageUrl={banerImageUrl}
            setActiveStep={setActiveStep}
            requestForPortingNumber={requestForPortingNumber}
            requestForCancelPortingNumber={requestForCancelPortingNumber}
            portingNumber={portingNumber}
          />
        ) : null}
        <PersonalInfoActions
          checkoutConfiguration={checkoutConfiguration}
          personalInfo={personalInfo}
          setSmsNotification={setSmsNotification}
          updateInputField={updateInputField}
          personalInfoTranslation={checkoutTranslation.personalInfo}
          setPersonalInfoRequested={setPersonalInfoRequested}
          updatePersonalInfoRequested={updatePersonalInfoRequested}
          setProceedToShippingButton={setProceedToShippingButton}
          globalConfiguration={globalConfiguration}
        />
      </Form>
    );
  }
}

export default PersonalInfoContent;
