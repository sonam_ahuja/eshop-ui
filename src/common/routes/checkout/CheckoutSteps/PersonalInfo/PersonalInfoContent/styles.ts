import styled from 'styled-components';
import { FormWrapper } from '@checkout/Common/FormWrapper/styles';

export const Form = styled(FormWrapper)`
  .personalInfo_numberPortingBanner {
    margin-top: 2.5rem;
  }
`;
