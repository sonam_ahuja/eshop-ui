export default {
  PHONE_NUMBER: 'phoneNumber',
  NUMBER: 'number',
  ADDRESS: 'address',
  CHAR_E: 'e',
  STREET_NUMBER: 'streetNumber',
  EMAIL: 'email',
  MAIN_ADDRESS: 'mainAddress',
  CITY: 'city',
  FLAT_NUMBER: 'flatNumber',
  POST_CODE: 'postCode',
  COUNTRY: 'country',
  ID: 'id',
  SMS_NOTIFICATION: 'smsNotification',
  DOB: 'dob'
};

export const KEY_CODE_INCLUDES = [
  48,
  49,
  50,
  51,
  52,
  53,
  54,
  55,
  56,
  57,
  58,
  96,
  97,
  98,
  99,
  100,
  101,
  102,
  103,
  104,
  105
];

export const KEY_CODE_EXCLUDES = [8, 9, 46, 37, 39, 91];

export const COPY_PASTE_KEY_CODES = [67, 65, 86, 88];

export const COUNTRY_KEYBOARD = {
  CZ: 'cz',
  EN: 'en'
};
