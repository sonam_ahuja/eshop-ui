export interface IFormField {
  value: string;
  isValid: boolean;
  validationMessage: string;
  colMobile: number;
  colDesktop: number;
  order: number;
}

export interface ILocalFormState {
  firstName?: IFormField;
  lastName?: IFormField;
  dob?: IFormField;
  email?: IFormField;
  type?: IFormField;
  phoneNumber?: IFormField;
  streetAddress?: IFormField;
  address?: IFormField;
  flatNumber?: IFormField;
  city?: IFormField;
  postCode?: IFormField;
  streetNumber?: IFormField;
  idNumber?: IFormField;
  oibNumber?: IFormField;
  company?: IFormField;
  pesel?: IFormField;
}

export interface ILocalState {
  id: string;
  isDefault: boolean;
  isSmsEnabled: boolean;
  enableProceedButton: boolean;
  isFormChanged: boolean;
  isPersonalInfoPrefilled: boolean;
  formFields: ILocalFormState;
}

export const personalInfoState: ILocalState = {
  id: '',
  isDefault: true,
  isSmsEnabled: false,
  enableProceedButton: false,
  isFormChanged: false,
  isPersonalInfoPrefilled: false,
  formFields: {
    firstName: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 3,
      order: 1
    },
    lastName: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 5,
      order: 2
    },
    dob: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 3
    },
    email: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 12,
      colDesktop: 8,
      order: 4
    },
    phoneNumber: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 5
    },
    address: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 12,
      colDesktop: 12,
      order: 6
    },
    city: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 7
    },
    postCode: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 8
    },
    streetNumber: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 12,
      order: 9
    },
    flatNumber: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 10
    },
    idNumber: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 11
    },
    oibNumber: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 12
    },
    company: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 12,
      colDesktop: 4,
      order: 13
    },
    pesel: {
      value: '',
      isValid: true,
      validationMessage: '',
      colMobile: 6,
      colDesktop: 4,
      order: 14
    }
  }
};
