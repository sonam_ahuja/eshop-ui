import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import {
  ACTION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  OPERATION_TYPE
} from '@checkout/store/numberPorting/enum';

import PersonalInfo, { mapDispatchToProps, mapStateToProps } from '.';

describe('<PersonaInfo />', () => {
  window.scrollTo = jest.fn();
  const initStateValue = appState();
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  window.scrollTo = jest.fn();

  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <PersonalInfo />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const updateInputValue = { key: 'firstname', value: 'Shubham' };
    const personalInfo = {
      email: {
        value: 'xxxxxx@gmail.com'
      },
      phoneNumber: {
        value: '9998976453'
      },
      mainAddress: [{}],
      isSmsEnabled: true
    };
    const updateRequest = {
      email: {
        id: '123email',
        value: 'saini2345@gmail.com'
      },
      phoneNumber: {
        id: '123number',
        value: '9417599999'
      },
      mainAddress: [
        {
          id: '123mainaddress',
          streetNumber: '123',
          address: 'Hudson Lane'
        }
      ],
      isSmsEnabled: true,
      isChanged: true
    };
    // tslint:disable-next-line:no-any
    const payload: any = {
      consent: true,
      operationType: OPERATION_TYPE.MNS,
      cartItem: [
        {
          action: ACTION_TYPE.DELETE,
          cartItemId: '1233'
        }
      ]
    };
    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.setSmsNotification();
    mapDispatch.updateInputField(updateInputValue);
    mapDispatch.setProceedToShippingButton(updateInputValue);
    mapDispatch.updatePersonalInfoRequested(updateRequest);
    mapDispatch.setPersonalInfoRequested(personalInfo);
    mapDispatch.syncDataInStore({ status: true });
    mapDispatch.showNumberPortModal(false);
    mapDispatch.showCancelModal(false);
    mapDispatch.setNumberPortingStatus(false);
    mapDispatch.editPaymentStepClick();
    mapDispatch.setFlowType(NUMBER_PORTING_TYPES.CHANGE_NUMBER);
    mapDispatch.setActiveStep(NUMBER_PORTING_STEPS.SELECT_NUMBER);
    mapDispatch.requestForPortingNumber(payload);
    mapDispatch.requestForCancelPortingNumber(payload);
    mapDispatch.fetchCheckoutAddressLoading(false);
  });
});
