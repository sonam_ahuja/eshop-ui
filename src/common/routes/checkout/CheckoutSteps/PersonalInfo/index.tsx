import * as personalInfoApi from '@common/types/api/checkout/personalInfo';
import {
  ICheckout,
  IConfigurationState
} from '@common/store/types/configuration';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import Popover from '@checkout/CheckoutSteps/Popover';
import React, { Component, ReactNode } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { RootState } from '@common/store/reducers';
import PersonalInfoContent from '@checkout/CheckoutSteps/PersonalInfo/PersonalInfoContent';
import numberPortingAction from '@checkout/store/numberPorting/actions';
import personalInfoActions, {
  ILocalFormStateWithStatus
} from '@checkout/store/personalInfo/actions';
import {
  IInputKeyValue,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';
import {
  ICheckoutTranslation,
  ITranslationState
} from '@common/store/types/translation';
import {
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES
} from '@checkout/store/numberPorting/enum';
import { IResourceNumber } from '@checkout/store/numberPorting/types';
import IconWithDescription from '@src/common/components/IconWithDescription';
import StepNavigation from '@checkout/CheckoutSteps/StepNavigation';
import paymentActions from '@checkout/store/payment/actions';
import SideNavigation from '@checkout/SideNavigation';
import SvgIdCard from '@src/common/components/Svg/id-card';
import { pageViewEvent } from '@events/index';
import Footer from '@src/common/components/FooterWithTermsAndConditions';
import { isMobile } from '@src/common/utils';
import Loadable from 'react-loadable';
import checkoutActions from '@checkout/store/actions';
import { CHECKOUT_VIEW, PAGE_VIEW } from '@events/constants/eventName';
import { sendProceedEvent } from '@events/checkout/index';

import { StyledPersonalInfo } from './styles';

const CheckoutAppShellComponent = Loadable({
  loading: () => null,
  loader: () =>
    import(
      '@common/routes/checkout/CheckoutSteps/index.shell'
    )
});

export interface IPersonalInfoStateProps {
  personalInfo: IPersonalInfoState;
  checkoutConfiguration: ICheckout;
  configuration: IConfigurationState;
  translation: ITranslationState;
  checkoutTranslation: ICheckoutTranslation;
  selectedNumber: IResourceNumber;
  migrationType: string;
  desiredDate?: string;
  banerImageUrl: string;
  isCreditCheckRequired: boolean;
  showNumberPortingBanner: boolean;
  portingNumber: string;
  mainAppShell: boolean;
}
export interface IPersonalInfoDispatchToProps {
  setSmsNotification(): void;
  syncDataInStore(data: ILocalFormStateWithStatus): void;
  updateInputField(data: IInputKeyValue): void;
  setProceedToShippingButton(data: IInputKeyValue): void;
  setPersonalInfoRequested(data: personalInfoApi.POST.IRequest): void;
  updatePersonalInfoRequested(data: personalInfoApi.PATCH.IRequest): void;
  enableProceedButton(): void;
  showNumberPortModal(status: boolean): void;
  setFlowType(flowType: NUMBER_PORTING_TYPES): void;
  showCancelModal(status: boolean): void;
  setNumberPortingStatus(status: boolean): void;
  editPaymentStepClick(): void;
  setActiveStep(step: NUMBER_PORTING_STEPS): void;
  requestForPortingNumber(payload: numberPortTypesApi.PATCH.IRequest): void;
  requestForCancelPortingNumber(
    payload: numberPortTypesApi.PATCH.IRequest
  ): void;
  fetchCheckoutAddressLoading(data: boolean): void;
}

export type IPersonalInfoProps = IPersonalInfoStateProps &
  IPersonalInfoDispatchToProps;

export class PersonalInfo extends Component<IPersonalInfoProps, {}> {
  constructor(props: IPersonalInfoProps) {
    super(props);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.CHECKOUT_PERSONAL_INFO);
    window.scrollTo(0, 0);
    sendProceedEvent(
      CHECKOUT_VIEW.PERSONAL_INFO.STEP,
      CHECKOUT_VIEW.PERSONAL_INFO.VIEW
    );
  }

  render(): ReactNode {
    const {
      checkoutConfiguration,
      checkoutTranslation,
      setSmsNotification,
      personalInfo,
      updateInputField,
      setProceedToShippingButton,
      setPersonalInfoRequested,
      updatePersonalInfoRequested,
      syncDataInStore,
      enableProceedButton,
      showNumberPortModal,
      selectedNumber,
      setFlowType,
      showCancelModal,
      migrationType,
      desiredDate,
      setNumberPortingStatus,
      banerImageUrl,
      editPaymentStepClick,
      setActiveStep,
      requestForPortingNumber,
      requestForCancelPortingNumber,
      isCreditCheckRequired,
      portingNumber,
      showNumberPortingBanner,
      configuration,
      mainAppShell,
      fetchCheckoutAddressLoading
    } = this.props;

    return (
      <>
        {mainAppShell ? (
          <CheckoutAppShellComponent
            checkoutTranslation={checkoutTranslation}
            editPaymentStepClick={editPaymentStepClick}
            fetchCheckoutAddressLoading={fetchCheckoutAddressLoading}
          />
        ) : (
          <StyledPersonalInfo>
            <SideNavigation>
              <IconWithDescription
                className='idCard'
                svgNode={<SvgIdCard />}
                description={checkoutTranslation.personalInfo.sideNavText}
              />
            </SideNavigation>
            <div className='mainContent'>
              <StepNavigation
                checkoutTranslation={checkoutTranslation}
                editPaymentStepClick={editPaymentStepClick}
              />
              <PersonalInfoContent
                checkoutConfiguration={checkoutConfiguration}
                personalInfo={personalInfo}
                updateInputField={updateInputField}
                setProceedToShippingButton={setProceedToShippingButton}
                personalInfoTranslation={checkoutTranslation.personalInfo}
                checkoutTranslation={checkoutTranslation}
                syncDataInStore={syncDataInStore}
                enableProceedButtonFunc={enableProceedButton}
                showNumberPortModal={showNumberPortModal}
                selectedNumber={selectedNumber}
                migrationType={migrationType}
                desiredDate={desiredDate}
                setFlowType={setFlowType}
                showCancelModal={showCancelModal}
                setNumberPortingStatus={setNumberPortingStatus}
                setSmsNotification={setSmsNotification}
                setPersonalInfoRequested={setPersonalInfoRequested}
                updatePersonalInfoRequested={updatePersonalInfoRequested}
                banerImageUrl={banerImageUrl}
                setActiveStep={setActiveStep}
                requestForPortingNumber={requestForPortingNumber}
                requestForCancelPortingNumber={requestForCancelPortingNumber}
                isCreditCheckRequired={isCreditCheckRequired}
                portingNumber={portingNumber}
                showNumberPortingBanner={showNumberPortingBanner}
                globalConfiguration={configuration.cms_configuration.global}
              />
              <Popover />
              {/* TODO - popover may not be used */}
              {isMobile.tablet ? (
                <Footer
                  className='footerCheckout'
                  termsAndConditionsUrl={
                    this.props.configuration.cms_configuration.global
                      .termsAndConditionsUrl
                  }
                  shouldTermsAndConditionsOpenInNewTab={
                    this.props.configuration.cms_configuration.global
                      .shouldTermsAndConditionsOpenInNewTab
                  }
                  globalTranslation={this.props.translation.cart.global}
                />
              ) : null}
            </div>
          </StyledPersonalInfo>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IPersonalInfoStateProps => ({
  personalInfo: state.checkout.personalInfo,
  checkoutConfiguration: state.configuration.cms_configuration.modules.checkout,
  configuration: state.configuration,
  translation: state.translation,
  checkoutTranslation: state.translation.cart.checkout,
  selectedNumber: state.checkout.numberPorting.selectedNumber,
  portingNumber: state.checkout.numberPorting.phoneNumber,
  migrationType: state.checkout.numberPorting.migrationType,
  desiredDate: state.checkout.numberPorting.date,
  banerImageUrl: state.checkout.numberPorting.bannerImageUrl,
  showNumberPortingBanner: state.checkout.numberPorting.showBanner,
  isCreditCheckRequired: state.checkout.checkout.isCreditCheckRequired,
  mainAppShell: state.checkout.checkout.mainAppShell
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IPersonalInfoDispatchToProps => ({
  setSmsNotification(): void {
    dispatch(personalInfoActions.setSMSNotification());
  },
  updateInputField(data: { key: string; value: string }): void {
    dispatch(personalInfoActions.updateInputField(data));
  },
  setProceedToShippingButton(data: { key: string; value: string }): void {
    dispatch(personalInfoActions.setProceedToShippingButton(data));
  },
  setPersonalInfoRequested(data: personalInfoApi.POST.IRequest): void {
    dispatch(personalInfoActions.setPersonalInfoRequested(data));
  },
  updatePersonalInfoRequested(data: personalInfoApi.PATCH.IRequest): void {
    dispatch(personalInfoActions.updatePersonalInfoRequested(data));
  },
  syncDataInStore(data: ILocalFormStateWithStatus): void {
    dispatch(personalInfoActions.syncDataInStore(data));
  },
  enableProceedButton(): void {
    dispatch(personalInfoActions.enableProceedButton());
  },
  showNumberPortModal(status: boolean): void {
    dispatch(personalInfoActions.showNumberPortModal(status));
  },
  setFlowType(flowType: NUMBER_PORTING_TYPES): void {
    dispatch(numberPortingAction.setFlowType(flowType));
  },
  showCancelModal(status: boolean): void {
    dispatch(personalInfoActions.showNumberPortingCancelModal(status));
  },
  setNumberPortingStatus(status: boolean): void {
    dispatch(personalInfoActions.setNumberPortingStatus(status));
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  },
  setActiveStep(step: NUMBER_PORTING_STEPS): void {
    dispatch(numberPortingAction.setActiveStep(step));
  },
  requestForPortingNumber(payload: numberPortTypesApi.PATCH.IRequest): void {
    dispatch(numberPortingAction.requestForPortingNumber(payload));
  },
  requestForCancelPortingNumber(
    payload: numberPortTypesApi.PATCH.IRequest
  ): void {
    dispatch(numberPortingAction.requestForCancelPortingNumber(payload));
  },
  fetchCheckoutAddressLoading(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddressLoading(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonalInfo);
