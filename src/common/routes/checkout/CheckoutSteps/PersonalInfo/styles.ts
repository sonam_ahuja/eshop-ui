import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { FormWrapper } from '@checkout/Common/FormWrapper/styles';
import { StyledIconWithDescription } from '@src/common/components/IconWithDescription/styles';

import { StyledSideNavigation } from '../../SideNavigation/styles';
import { BannerWrapper } from '../NumberPorting/NumberPortingBanner/styles';

export const StyledPersonalInfo = styled.div`
  background: ${colors.coldGray};

  ${StyledIconWithDescription} {
    .description {
      padding-right: 5rem;
    }
  }

  ${FormWrapper} {
    padding-bottom: 1.25rem;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    flex-direction: column;
    height: 100%;
    .mainContent {
      width: 100%;
      display: flex;
      flex-direction: column;
      flex: 1;
    }
    ${FormWrapper} {
      padding: 3.25rem 2.25rem 4rem;
      display: flex;
      flex-direction: column;
      flex: 1;
    }
    ${StyledSideNavigation} {
      display: flex;
      padding: 0 2.25rem;
    }
    ${StyledIconWithDescription} {
      min-height: 6.5rem;
      .description {
        max-width: 10.375rem;
        padding-right: 0;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    flex-direction: row;
    ${FormWrapper} {
      padding: 3.25rem 3rem 1.5rem;
    }
    ${StyledSideNavigation} {
      ${StyledIconWithDescription} {
        align-items: flex-start;
        .description {
          padding-right: 0;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${FormWrapper} {
      padding: 3.5rem 2.25rem 1.25rem;
    }
    ${BannerWrapper} {
      .bannerTextWrap {
        .link {
          .dt_paragraph {
            .dt_icon {
              font-size: 1rem;
              margin-left: 0.25rem;
            }
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    ${FormWrapper} {
      padding: 4.15rem 5.5rem 1.75rem;
    }
    ${BannerWrapper} {
      flex-direction: row-reverse;
      .bannerTextWrap {
        width: 100%;
      }
      .productWrap {
        padding: 1.25rem 1rem 1rem;
      }
    }

    ${StyledSideNavigation} {
      ${StyledIconWithDescription} {
        .iconWrap {
          font-size: 12.75rem;
          line-height: 0;
        }
      }
    }
  }
`;
