import {
  formValidAsPerSmsNotification,
  maxLengthReached
} from '@checkout/CheckoutSteps/PersonalInfo/utils';

describe('PersonaInfo utils test', () => {
  test('formValidAsPerSmsNotification true test', () => {
    expect(formValidAsPerSmsNotification(true, 'value')).toBeDefined();
  });

  test('formValidAsPerSmsNotification false test', () => {
    expect(formValidAsPerSmsNotification(false, 'value')).toBeDefined();
  });

  test('maxLengthReached test', () => {
    expect(
      maxLengthReached('', { type: '', value: 'value', message: 'error' })
    ).toBeDefined();
  });
});
