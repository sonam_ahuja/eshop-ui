import {
  IFields,
  IFieldValdation,
  validationType
} from '@common/store/types/configuration';
import {
  ILocalFormState,
  personalInfoState
} from '@checkout/CheckoutSteps/PersonalInfo/formGrid';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { IProps as IFormBodyProps } from '@checkout/CheckoutSteps/PersonalInfo/PersonalInfoContent';
import PERSONAL_INFO_CONSTANTS, {
  COPY_PASTE_KEY_CODES,
  COUNTRY_KEYBOARD,
  KEY_CODE_EXCLUDES,
  KEY_CODE_INCLUDES
} from '@checkout/CheckoutSteps/PersonalInfo/constants';
import { IPersonalInfoFormFields } from '@checkout/store/personalInfo/types';
import { getGlobalConfig } from '@checkout/utils';

export function getFieldsToBeShown(
  formFields: IFields,
  creditRequired: boolean
): ILocalFormState {
  const newState: ILocalFormState = {};

  const mandatoryShownFields = Object.keys(formFields).filter(field => {
    return formFields[field].show;
  });

  if (creditRequired) {
    mandatoryShownFields.forEach(field => {
      newState[field] = {
        ...personalInfoState.formFields[field],
        order: formFields[field].order
      };
    });
  } else {
    const newCreditNotRequiredField = Object.keys(formFields).filter(field => {
      return formFields[field].show && !formFields[field].creditCheckField;
    });

    // tslint:disable-next-line:no-identical-functions
    newCreditNotRequiredField.forEach(field => {
      newState[field] = {
        ...personalInfoState.formFields[field],
        order: formFields[field].order
      };
    });
  }

  return newState;
}

export function getLabelValueForFormFields(
  props: IFormBodyProps,
  field: string
): string {
  const labelKey = props.checkoutConfiguration.form.fields[field].labelKey;
  const isFieldMandatory =
    props.checkoutConfiguration.form.fields[field].mandatory;
  let labelValue = props.personalInfoTranslation[labelKey];

  if (!isFieldMandatory && props.checkoutTranslation.optionalText) {
    labelValue += ` ${props.checkoutTranslation.optionalText}`;
  }

  return labelValue;
}

export function formValidAsPerSmsNotification(
  isSmsNotificationChecked: boolean,
  inputValue: string
): boolean {
  if (isSmsNotificationChecked && !inputValue.length) {
    return false;
  }

  return true;
}

export function charEPresent(
  event: React.KeyboardEvent<HTMLInputElement>
): boolean {
  const { countryKeyboard } = getGlobalConfig();
  if (event.shiftKey && countryKeyboard.toLowerCase() !== COUNTRY_KEYBOARD.CZ) {
    // not allowing special charecter
    return true;
  } else if (
    (event.ctrlKey || event.metaKey) &&
    COPY_PASTE_KEY_CODES.includes(event.keyCode)
  ) {
    return false;
  } else {
    return (
      !KEY_CODE_EXCLUDES.includes(event.keyCode) &&
      !KEY_CODE_INCLUDES.includes(event.keyCode)
    );
  }
}

export function ignoreSpaces(
  event: React.KeyboardEvent<HTMLInputElement>
): boolean {
  const value = event.currentTarget.value;

  return (
    (event.keyCode === 32 && value.trim().length === 0) ||
    (event.keyCode === 32 && value.length === value.trim().length + 1)
  );
}
export function maxLengthReached(
  value: string,
  validation: IFieldValdation
): boolean {
  return (
    validation.type === VALIDATION_TYPE.MAX &&
    value.length > parseInt(validation.value, 0)
  );
}

export function isInputAllowed(
  inputType: string,
  inputValue: string,
  validation: IFieldValdation
): boolean {
  // tslint:disable-next-line:no-small-switch
  switch (inputType) {
    case PERSONAL_INFO_CONSTANTS.NUMBER:
      if (maxLengthReached(inputValue, validation)) {
        return false;
      }
      break;
    default:
      break;
  }

  return true;
}

export function getCurrentDate(): string {
  const d = new Date();
  let month = `${d.getMonth() + 1}`;
  let day = `${d.getDate()}`;
  const year = `${d.getFullYear()}`;

  if (month.length < 2) {
    month = `0${month}`;
  }
  if (day.length < 2) {
    day = `0${day}`;
  }

  return [year, month, day].join('-');
}

export function readyToProceed(
  formFields: ILocalFormState,
  cmsFormConfiguration: IFields
): boolean {
  let isFormValid = true;

  Object.keys(formFields).forEach(field => {
    if (
      cmsFormConfiguration[field] &&
      cmsFormConfiguration[field].show &&
      cmsFormConfiguration[field].mandatory &&
      (!formFields[field].value || !formFields[field].isValid)
    ) {
      isFormValid = false;
    }
  });

  return isFormValid;
}

export function checkReadyToProceedEnablity(
  formFields: IPersonalInfoFormFields,
  cmsFormConfiguration: IFields
): boolean {
  let isFormValid = true;

  Object.keys(formFields).forEach(field => {
    if (cmsFormConfiguration[field] && cmsFormConfiguration[field].show) {
      if (
        cmsFormConfiguration[field].mandatory &&
        (!formFields[field] ||
          !isFormFieldValidAsPerType(
            cmsFormConfiguration[field].validation.value,
            formFields[field],
            cmsFormConfiguration[field].validation.type
          ))
      ) {
        isFormValid = false;
      } else if (
        !cmsFormConfiguration[field].mandatory &&
        (formFields[field] &&
          !isFormFieldValidAsPerType(
            cmsFormConfiguration[field].validation.value,
            formFields[field],
            cmsFormConfiguration[field].validation.type
          ))
      ) {
        isFormValid = false;
      }
    }
  });

  return isFormValid;
}

export function isFormFieldValidAsPerType(
  value: string | number,
  inputValue: string,
  type: validationType
): boolean {
  const {
    MAX,
    MIN,
    REGEX,
    BETWEEN,
    NONE,
    MIN_LOWERCASE,
    MIN_UPPERCASE,
    MIN_DIGIT,
    EXACT
  } = VALIDATION_TYPE;

  if (inputValue) {
    switch (type) {
      case MAX:
        return inputValue.length <= value;
      case MIN:
        return inputValue.length >= value;
      case EXACT:
        return String(inputValue.length) === String(value);
      case BETWEEN:
        const values = (value as string).split('-');

        return (
          inputValue.length <= parseInt(values[1], 0) &&
          inputValue.length >= parseInt(values[0], 0)
        );
      case REGEX:
        const newRegex = new RegExp(value as string);

        return newRegex.test(inputValue);
      case NONE:
        return true;
      case MIN_LOWERCASE:
        return value <= inputValue.replace(/[^a-z]/g, '').length;
      case MIN_UPPERCASE:
        return value <= inputValue.replace(/[^A-Z]/g, '').length;
      case MIN_DIGIT:
        return value <= inputValue.replace(/[^0-9]/g, '').length;
      default:
        return true;
    }
  } else {
    return false;
  }
}
