import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import PopOver from '.';

describe('<Payment PopOver />', () => {
  const props = {};

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PopOver {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
