import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { Avatar, Button, Modal, Title } from 'dt-components';

const PopOverWrap = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0rem 1rem;
  color: #383838;
  padding: 2rem 0 1.875rem 0;
  max-width: 28.5rem;
  height: auto;

  @media (min-width: ${breakpoints.desktop}px) {
    margin: 0rem 1.5rem;
  }

  .avatar {
    margin-bottom: 2rem;
  }

  .title {
    margin-bottom: 2.4375rem;
  }
`;

const StyledButtonWrap = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: row;
  }
  .proceedbtnprimary {
    margin-bottom: 0.8125rem;

    @media (min-width: ${breakpoints.desktop}px) {
      margin-bottom: 0;
      margin-right: 1rem;
    }
  }
`;

const PopOver: FunctionComponent<{}> = () => {
  return (
    <Modal isOpen={false} size='medium'>
      <PopOverWrap>
        <Avatar
          imageUrl='https://i.ibb.co/cFNWZH1/sad-face.png'
          size='medium'
          className='avatar'
          shape='rectangle'
        />
        <Title size='large' className='title' weight='ultra'>
          There is an issue with your credit, but you can always pay in full or
          select a different device.
        </Title>
        <StyledButtonWrap>
          <Button
            fullwidth={false}
            size='medium'
            disabled={false}
            className='proceedbtnprimary'
            loading={false}
            type='primary'
          >
            Pay Device in full
          </Button>
          <Button
            fullwidth={false}
            size='medium'
            disabled={false}
            className='proceedbtn'
            loading={false}
            type='secondary'
          >
            Select a different Device
          </Button>
        </StyledButtonWrap>
      </PopOverWrap>
    </Modal>
  );
};

export default PopOver;
