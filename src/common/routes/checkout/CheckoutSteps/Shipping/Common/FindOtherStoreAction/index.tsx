import React from 'react';
import { RoundButton } from 'dt-components';

import { StyledFindOtherStoreAction } from './styles';

interface IProps {
  text: string;
  onClick(): void;
}

const FindOtherStoreAction = (props: IProps) => {
  const { text, onClick } = props;

  return (
    <StyledFindOtherStoreAction
      onClick={onClick}
      className='find-other-store-wrap'
    >
      <div className='textWrap'>
        <RoundButton className='roundBtn' iconName='ec-search' />
        <p className='text'>{text}</p>
      </div>
    </StyledFindOtherStoreAction>
  );
};

export default FindOtherStoreAction;
