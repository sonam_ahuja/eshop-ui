import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const StyledFindOtherStoreAction = styled.div`
  display: flex;
  align-items: center;
  padding-left: 0rem;
  position: relative;
  z-index: 1;
  overflow: hidden;
  height: 100px;

  &:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background: ${hexToRgbA(colors.black, 0.3)};
    background-image: url('https://i.ibb.co/m0Mp9fG/Screenshot-2019-05-14-at-6-21-17-AM.png');
    z-index: -1;
    background-size: cover;
  }

  .textWrap {
    display: flex;
    align-items: center;
    height: 100%;
    width: 100%;
    background: ${hexToRgbA(colors.black, 0.3)};
    padding: 5rem 0 5rem 1.75rem;
    .roundBtn {
      font-size: 1.4rem;
      .dt_icon {
        color: ${colors.ironGray};
      }
    }
    .text {
      padding-left: 1.75rem;
      font-size: 1.25rem;
      font-weight: 500;
      line-height: 1.2;
      letter-spacing: normal;
      color: ${colors.white};
    }
  }

  .googlemap {
    position: absolute;
    z-index: -1;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
  }
  @media (min-width: ${breakpoints}px) {
    height: auto;
    .textWrap {
      height: auto;
    }
  }
`;
