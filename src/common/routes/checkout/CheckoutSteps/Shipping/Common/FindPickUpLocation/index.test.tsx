// tslint:disable:no-any
import React from 'react';
import { render } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import store from '@common/store';
import translation from '@src/common/store/states/translation';

import FindPickUpLocation from '.';

describe('<PickUpAtStore />', () => {
  const state = store.getState();

  const props: any = {
    checkoutTranslation: translation().cart.checkout,
    shippingInfo: state.checkout.shippingInfo,
    markers: [],
    setOtherPersonPickup: jest.fn(),
    onSelectItem: jest.fn(),
    isUserPickedLocation: jest.fn(),
    setSelectedStore: jest.fn(),
    onEnterPressOnSearch: jest.fn(),
    closeModal: jest.fn(),
    onClickOnSelectAddress: jest.fn(),
    fetchPickupLocation: jest.fn()
  };

  test('should render properly', () => {
    const component = render(
      <ThemeProvider theme={{}}>
        <FindPickUpLocation {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
