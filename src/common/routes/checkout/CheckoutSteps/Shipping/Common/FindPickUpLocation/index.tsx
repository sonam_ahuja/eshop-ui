import { ICheckoutTranslation } from '@src/common/store/types/translation';
import StoreAddress from '@checkout/CheckoutSteps/Shipping/Common/StoreAddress';
import SuggestiveSearch from '@checkout/CheckoutSteps/Shipping/Common/SuggestiveSearch';
import { Paragraph, RoundButton } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import { isMobile } from '@utils/index';
import {
  IPickUpLocationPayload,
  ISelectedStoreAddress,
  IShippingAddressState
} from '@checkout/store/shipping/types';
import SimpleMap, {
  IMarker
} from '@src/common/routes/checkout/CheckoutSteps/Shipping/Common/Map';
import { IListItem } from 'dt-components/lib/es/components/molecules/autocomplete/list';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';

import { StyledFindPickUpLocation } from './styles';

export interface IProps {
  checkoutTranslation: ICheckoutTranslation;
  shippingInfo: IShippingAddressState;
  markers: IMarker[];
  markerIconName?: iconNamesType;
  setOtherPersonPickup(data: boolean): void;
  onSelectItem(value: IListItem): void;
  isUserPickedLocation(): void;
  setSelectedStore(data: ISelectedStoreAddress | null): void;
  onEnterPressOnSearch(value: string): void;
  closeModal?(): void;
  onClickOnSelectAddress?(): void;
  fetchPickupLocation(data: IPickUpLocationPayload): void;
}

class FindPickUpLocation extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { checkoutTranslation, shippingInfo, setSelectedStore } = this.props;
    const { onSelectItem, markers, isUserPickedLocation } = this.props;
    const { markerIconName, onEnterPressOnSearch } = this.props;
    const { onClickOnSelectAddress, fetchPickupLocation } = this.props;

    const mobileEl = (
      <div className='mapHeader'>
        <div className='labelAndCloseIcon'>
          <Paragraph size='medium' className='label'>
            {checkoutTranslation.shippingInfo.findStore}
          </Paragraph>
          <RoundButton
            iconName='ec-cancel'
            shadow
            onClick={this.props.closeModal}
          />
        </div>
        <SuggestiveSearch
          checkoutTranslation={checkoutTranslation}
          onEnterPressOnSearch={onEnterPressOnSearch}
          onSelectItem={onSelectItem}
          loadingText={checkoutTranslation.loadingText}
        />
      </div>
    );

    const desktopEl = (
      <SuggestiveSearch
        checkoutTranslation={checkoutTranslation}
        onEnterPressOnSearch={onEnterPressOnSearch}
        onSelectItem={onSelectItem}
        loadingText={checkoutTranslation.loadingText}
      />
    );

    return (
      <StyledFindPickUpLocation>
        <SimpleMap
          markerIconName={markerIconName as iconNamesType}
          onMarkerClick={setSelectedStore}
          markers={markers}
          onUserLocationClicked={fetchPickupLocation}
        />

        {isMobile.phone ? mobileEl : desktopEl}

        <StoreAddress
          isUserPickedLocation={isUserPickedLocation}
          shippingInfo={shippingInfo}
          checkoutTranslation={checkoutTranslation}
          onClickOnSelectAddress={onClickOnSelectAddress}
          boxShadow={true}
          size='small'
        />
      </StyledFindPickUpLocation>
    );
  }
}

export default FindPickUpLocation;
