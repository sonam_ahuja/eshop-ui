import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledFindPickUpLocation = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  z-index: 1;
  padding: 1.25rem;

  .dt_outsideClick,
  .dt_outsideClick > div,
  .dt_outsideClick > div > .input {
    width: 100%;
    display: block;
  }

  .googlemap {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: -1;

    &:after {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      height: 141px;
      background-image: linear-gradient(
        to bottom,
        #f8f8f8,
        rgba(248, 248, 248, 0)
      );
      margin-top: -1.25rem;
      padding: 1.25rem;
      width: 100%;
      z-index: 1;
    }
  }

  .mapHeader {
    height: 141px;
    background-image: linear-gradient(
      to bottom,
      #f8f8f8,
      rgba(248, 248, 248, 0)
    );
    margin: -1.25rem -1.25rem 0;
    padding: 1.25rem;

    .labelAndCloseIcon {
      display: flex;
      justify-content: space-between;
      align-items: center;

      .label {
        color: ${colors.darkGray};
      }
      .dt_roundButton {
        z-index: 1;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 0.5rem 2.25rem 1.75rem;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 0.5rem 2.8125rem 1.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2.25rem 2.25rem 1.25rem;
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 2.9rem 5.5rem 1.75rem;
  }
`;
