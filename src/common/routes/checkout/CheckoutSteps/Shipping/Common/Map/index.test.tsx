import React from 'react';
import { mount, shallow } from 'enzyme';
import * as CommonUtils from '@common/utils';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';

import SimpleMap from '.';

Object.defineProperty(CommonUtils, 'isBrowser', {
  get(): boolean {
    return false;
  },
  // tslint:disable-next-line:no-empty
  set(): void {}
});

describe('<Shipping SimpleMap />', () => {
  // tslint:disable-next-line:no-any
  const props: any = {
    markers: [],
    onMarkerClick: jest.fn(),
    onUserLocationClicked: jest.fn()
  };

  test('should render properly', () => {
    const component = shallow(<SimpleMap {...props} />);

    const spy1 = jest.spyOn(SimpleMap.prototype, 'componentDidMount');
    const spy2 = jest.spyOn(SimpleMap.prototype, 'componentWillReceiveProps');

    expect(spy1).toHaveBeenCalledTimes(0);

    component.setProps({
      markers: [
        {
          id: '1',
          geoX: '50.712319',
          goeY: '7.1210148',
          name: 'sdf'
        }
      ]
    });

    component.update();

    expect(spy2).toHaveBeenCalledTimes(1);
    expect(component).toMatchSnapshot();
  });

  test('event simulate', () => {
    const initStateValue = appState();
    const mockStore = configureStore();
    const newProps = { ...props };
    const store = mockStore(initStateValue);

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SimpleMap {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('SimpleMap').instance() as SimpleMap).handleLocation({
      coords: {
        latitude: '78.22323',
        longitude: '42.4555'
      }
    });
    (component.find('SimpleMap').instance() as SimpleMap).markerClickHandler(2);
    (component.find('SimpleMap').instance() as SimpleMap).createMapOptions({
      ControlPosition: {
        TOP_RIGHT: 'topCenter',
        RIGHT_CENTER: 'rightCenter'
      },
      ZoomControlStyle: {
        SMALL: 'small'
      }
    });
    (component.find('SimpleMap').instance() as SimpleMap).onZoomInClick();
    (component.find('SimpleMap').instance() as SimpleMap).onZoomOutClick();
  });
});
