// tslint:disable:no-any
import {
  ILatLong,
  ISelectedStoreAddress
} from '@src/common/routes/checkout/store/shipping/types';
import React, { Component, ReactNode } from 'react';
import { getCurrentLocation } from '@checkout/CheckoutSteps/Shipping/PickUp/utils';
import MapMarker from '@checkout/CheckoutSteps/Shipping/Common/MapMarker';
import store from '@common/store';
import GoogleMapReact from 'google-map-react';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import { Icon } from 'dt-components';
import { isBrowser } from '@src/common/utils';

import ZoomInOut from '../ZoomInOut';

import {
  googleMapStyle,
  StyledGoogleMap,
  StyledLocationIcon,
  StyledMapIcons
} from './styles';

const googleMap = store.getState().configuration.cms_configuration.global
  .googleMap;

export interface IProps {
  markers: any[];
  markerIconName?: iconNamesType;
  onMarkerClick(data: ISelectedStoreAddress | null): void;
  onUserLocationClicked(data: ILatLong): void;
}
export interface IMarker {
  geoX: string;
  geoY: string;
  [key: string]: string;
}

export interface IState {
  center: ICenter;
  zoom: number;
  selectedLocationIdx: number;
}

interface ICenter {
  lat: number;
  lng: number;
}
class SimpleMap extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      center: {
        lat: googleMap.center.lat,
        lng: googleMap.center.lng
      },
      zoom: googleMap.zoom,
      selectedLocationIdx: 0
    };
    this.handleLocation = this.handleLocation.bind(this);
    this.markerClickHandler = this.markerClickHandler.bind(this);
    this.onUserLocationClickHandler = this.onUserLocationClickHandler.bind(
      this
    );

    this.createMapOptions = this.createMapOptions.bind(this);
  }

  componentDidMount(): void {
    if (!this.props.markers.length) {
      this.onUserLocationClickHandler();
    }
  }

  componentWillReceiveProps(nextProps: IProps): void {
    if (nextProps.markers.length) {
      const centerGeoX = nextProps.markers[0].geoX;
      const centerGeoY = nextProps.markers[0].geoY;
      if (nextProps.markers.length && centerGeoX && centerGeoY) {
        this.setState({
          center: {
            lat: parseFloat(centerGeoX),
            lng: parseFloat(centerGeoY)
          }
        });
      }
    }
  }

  onUserLocationClickHandler(): void {
    getCurrentLocation(this.handleLocation);
  }

  // tslint:disable-next-line:no-any
  handleLocation(pos: any): void {
    const crd = pos.coords;
    const lat = crd.latitude;
    const lng = crd.longitude;
    this.setState({ center: { lat: crd.latitude, lng: crd.longitude } });
    if (this.props.onUserLocationClicked) {
      this.props.onUserLocationClicked({ geoX: lat, geoY: lng });
    }
  }

  markerClickHandler(index: number): void {
    this.setState({
      selectedLocationIdx: index
    });

    this.props.onMarkerClick(this.props.markers[index]);
  }

  renderMarkers(): ReactNode[] | ReactNode {
    const { markerIconName } = this.props;
    if (
      isBrowser &&
      Array.isArray(this.props.markers) &&
      this.props.markers.length
    ) {
      return this.props.markers.map((marker, index: number) => {
        return (
          <MapMarker
            markerIconName={markerIconName as iconNamesType}
            key={index}
            lat={marker.geoX}
            lng={marker.geoY}
            markerName={marker.name}
            active={this.state.selectedLocationIdx === index}
            onClick={() => this.markerClickHandler(index)}
          />
        );
      });
    }

    return null;
  }

  // tslint:disable:no-any
  createMapOptions(maps: any): any {
    return {
      zoomControl: false,
      zoomControlOptions: {
        position: maps.ControlPosition.RIGHT_CENTER,
        style: maps.ZoomControlStyle.SMALL
      },
      mapTypeControlOptions: {
        position: maps.ControlPosition.TOP_RIGHT
      },
      disableDefaultUI: true,
      styles: googleMapStyle
    };
  }
  onZoomInClick = (): void => {
    this.setState({ zoom: this.state.zoom + 1 });
  }

  onZoomOutClick = (): void => {
    this.setState({ zoom: this.state.zoom - 1 });
  }

  render(): ReactNode {
    const { center, zoom } = this.state;

    return (
      <>
        <StyledMapIcons>
          <StyledLocationIcon>
            <Icon
              onClick={this.onUserLocationClickHandler}
              size='inherit'
              name={'ec-current-localization'}
              color='currentColor'
            />
          </StyledLocationIcon>
          <ZoomInOut
            onZoomInClick={this.onZoomInClick}
            onZoomOutClick={this.onZoomOutClick}
          />
        </StyledMapIcons>
        <StyledGoogleMap className='googlemap'>
          <GoogleMapReact
            bootstrapURLKeys={{ key: googleMap.apiKey }}
            center={center}
            defaultZoom={zoom}
            zoom={zoom}
            options={this.createMapOptions}
          >
            {this.renderMarkers()}
          </GoogleMapReact>
        </StyledGoogleMap>
      </>
    );
  }
}

export default SimpleMap;
