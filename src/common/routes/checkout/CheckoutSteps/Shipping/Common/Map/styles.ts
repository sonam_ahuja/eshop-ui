import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

const LABEL_TEXT_FILL = 'labels.text.fill';
const GEOMETRY_FILL = 'geometry.fill';
const ROAD_ARTERIAL = 'road.arterial';
const ROAD_HIGHWAY = 'road.highway';

export const StyledMapIcons = styled.div`
  position: absolute;
  right: 10px;
  top: 0;
  bottom: 0;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const StyledLocationIcon = styled.div`
  cursor: pointer;
  border-radius: 50%;
  height: 40px;
  width: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${colors.white};
  margin-bottom: 0.75rem;

  .dt_icon {
    color: ${colors.gray};
    font-size: 16px;
  }
`;

export const StyledGoogleMap = styled.div`
  width: '100%';

  .gmnoprint {
    display: none;
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    &:after {
      margin-top: 0 !important; /****** specially added for tablet portrait ******/
    }
    .gmnoprint {
      display: block;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    &:after {
      margin-top: -1.25rem; /****** used for top blured space of map as per default ******/
    }
  }
`;

export const googleMapStyle = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#f5f5f5'
      }
    ]
  },
  {
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off'
      }
    ]
  },
  {
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#616161'
      }
    ]
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#f5f5f5'
      }
    ]
  },
  {
    featureType: 'administrative.land_parcel',
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ededed'
      }
    ]
  },
  {
    featureType: 'administrative.land_parcel',
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#ffffff'
      }
    ]
  },
  {
    featureType: 'landscape.man_made',
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ffffff'
      }
    ]
  },
  {
    featureType: 'landscape.natural.landcover',
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ff293d'
      }
    ]
  },
  {
    featureType: 'landscape.natural.terrain',
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ffffff'
      }
    ]
  },
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee'
      }
    ]
  },
  {
    featureType: 'poi',
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#757575'
      }
    ]
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5'
      }
    ]
  },
  {
    featureType: 'poi.park',
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#9e9e9e'
      }
    ]
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: '#ffffff'
      }
    ]
  },
  {
    featureType: ROAD_ARTERIAL,
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ededed'
      }
    ]
  },
  {
    featureType: ROAD_ARTERIAL,
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#757575'
      }
    ]
  },
  {
    featureType: ROAD_ARTERIAL,
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#ededed'
      }
    ]
  },
  {
    featureType: ROAD_HIGHWAY,
    elementType: 'geometry',
    stylers: [
      {
        color: '#dadada'
      }
    ]
  },
  {
    featureType: ROAD_HIGHWAY,
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ffffff'
      },
      {
        visibility: 'off'
      }
    ]
  },
  {
    featureType: ROAD_HIGHWAY,
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#616161'
      }
    ]
  },
  {
    featureType: 'road.highway.controlled_access',
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ededed'
      }
    ]
  },
  {
    featureType: 'road.local',
    elementType: GEOMETRY_FILL,
    stylers: [
      {
        color: '#ededed'
      },
      {
        saturation: 10
      },
      {
        weight: 6
      }
    ]
  },
  {
    featureType: 'road.local',
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#9e9e9e'
      }
    ]
  },
  {
    featureType: 'transit.line',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5'
      }
    ]
  },
  {
    featureType: 'transit.station',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee'
      }
    ]
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#c9c9c9'
      }
    ]
  },
  {
    featureType: 'water',
    elementType: LABEL_TEXT_FILL,
    stylers: [
      {
        color: '#9e9e9e'
      }
    ]
  }
  // tslint:disable-next-line:max-file-line-count
];
