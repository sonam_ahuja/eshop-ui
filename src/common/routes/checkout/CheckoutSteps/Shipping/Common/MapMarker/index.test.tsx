import React from 'react';
import { mount } from 'enzyme';
import * as CommonUtils from '@common/utils';
import { ThemeProvider } from 'dt-components';

import MapMarker, { IMapMarker } from '.';

Object.defineProperty(CommonUtils, 'isBrowser', {
  get(): boolean {
    return false;
  },
  // tslint:disable-next-line:no-empty
  set(): void {}
});

describe('<MapMarker />', () => {
  const props: IMapMarker = {
    markerName: 'ec-solid-shop-localizer',
    lat: '23.45671',
    lng: '73.23451',
    active: true,
    markerIconName: 'ec-solid-shop-localizer',
    onClick: jest.fn()
  };
  const componentWrapper = (newpProps: IMapMarker) =>
    mount(
      <ThemeProvider theme={{}}>
        <MapMarker {...newpProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
