import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';

import { StyledMapMarker } from './styles';

export interface IMapMarker {
  markerName?: string;
  lat: number | string;
  lng: number | string;
  active: boolean;
  markerIconName: iconNamesType;
  onClick(): void;
}

export default class MapMarker extends Component<IMapMarker, {}> {
  static propTypes = {
    text: PropTypes.string
  };

  static defaultProps = {
    text: ''
  };

  render(): React.ReactNode {
    const { active, markerIconName } = this.props;

    const classes = active ? 'active' : '';

    return (
      <StyledMapMarker className={classes} {...this.props}>
        <Icon
          color='currentColor'
          size='inherit'
          name={markerIconName ? markerIconName : 'ec-solid-shop-localizer'}
        />
      </StyledMapMarker>
    );
  }
}
