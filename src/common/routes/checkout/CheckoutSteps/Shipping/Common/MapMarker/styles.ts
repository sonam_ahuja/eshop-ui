import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledMapMarker = styled.div`
  color: ${colors.darkGray};
  font-size: 1.5625rem;

  &.active {
    color: ${colors.magenta};
    font-size: 2.125rem;
  }
`;
