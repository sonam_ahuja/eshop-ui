import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import OtherPersonForm from '.';

describe('<Shipping OtherPersonForm />', () => {
  const props = {};

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <OtherPersonForm {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
