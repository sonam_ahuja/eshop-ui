import React, { Component, ReactNode } from 'react';
import { FloatLabelInput } from 'dt-components';
import { breakpoints } from '@src/common/variables';
import styled from 'styled-components';
import { Column, Row } from '@common/components/Grid/styles';

export const Form = styled.form`
  @media (min-width: ${breakpoints.desktop}px) {
    margin: 0;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0;
  }
  ${Column}:first-child {
    padding-right: 0;
  }
  .lastName {
    padding-left: 0;
  }
`;

class OtherPersonForm extends Component<{}, {}> {
  render(): ReactNode {
    return (
      <Form>
        <Row>
          <Column colMobile={6}>
            <FloatLabelInput
              id='firstname'
              label='First Name'
              autoComplete='off'
            />
          </Column>
          <Column colMobile={6}>
            <FloatLabelInput
              id='lastname'
              label='Last Name'
              className='lastName'
              autoComplete='off'
            />
          </Column>

          <Column colMobile={6}>
            <FloatLabelInput id='id' label='ID Number' autoComplete='off' />
          </Column>

          <Column colMobile={6}>
            <FloatLabelInput
              id='phone'
              label='Phone Number'
              autoComplete='off'
            />
          </Column>
        </Row>
      </Form>
    );
  }
}

export default OtherPersonForm;
