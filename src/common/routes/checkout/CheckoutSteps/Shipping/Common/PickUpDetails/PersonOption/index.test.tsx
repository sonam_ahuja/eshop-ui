import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import store from '@common/store';

import PersonOption from '.';

describe('<Shipping PersonOption />', () => {
  const state = store.getState().checkout;

  const props = {
    shippingInfo: state.shippingInfo,
    setOtherPersonPickup: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PersonOption {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
