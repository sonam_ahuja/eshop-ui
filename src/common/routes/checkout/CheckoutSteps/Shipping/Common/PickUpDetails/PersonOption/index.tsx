import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { IShippingAddressState } from '@src/common/routes/checkout/store/shipping/types';

const StyledPersonOption = styled.div`
  margin-top: 2rem;
  margin-bottom: 2.25rem;
  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    flex-direction: row;
    width: 100%;
    margin-top: 1rem;
  }
`;

const AllNameWrap = styled.div`
  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    flex-direction: column;
    font-size: 1.25rem;
    font-weight: bold;
  }
`;

const NameWrap = styled.div`
  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    flex-direction: row;
    margin-bottom: 1rem;
    color: #a3a3a3;

    input[type='radio'] {
      color: #a3a3a3;
    }

    input[type='radio']:checked {
      color: #6c6c6c;
    }
  }
`;
export interface IProps {
  shippingInfo: IShippingAddressState;
  setOtherPersonPickup(data: boolean): void;
}
const PersonOption: FunctionComponent<IProps> = (_props: IProps) => {
  return (
    <StyledPersonOption>
      <AllNameWrap>
        <NameWrap>
          {/* <Radio // not merge
            label='Me, Martin'
            name='radio'
            id='usa'
            size='large'
            checked={isOtherPersonPickup ? false : true}
            value='usa'
            // tslint:disable-next-line:jsx-no-lambda
            onChange={() => props.setOtherPersonPickup(false)}
          /> */}
        </NameWrap>
        <NameWrap>
          {/* <Radio // not merge
            label='Other Person'
            name='radio'
            id='usa'
            size='large'
            checked={isOtherPersonPickup ? true : false}
            value='usa'
            // tslint:disable-next-line:jsx-no-lambda
            onChange={() => props.setOtherPersonPickup(true)}
          /> */}
        </NameWrap>
      </AllNameWrap>
    </StyledPersonOption>
  );
};

export default PersonOption;
