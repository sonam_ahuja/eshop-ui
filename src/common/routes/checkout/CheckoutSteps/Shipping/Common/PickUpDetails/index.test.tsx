import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@src/common/store/states/translation';
import initialState from '@checkout/store/shipping/state';

import StockImmediately from '.';

describe('<Shipping StockImmediately />', () => {
  test('should render properly', () => {
    const props = {
      checkoutTranslation: translationState().cart.checkout,
      shippingInfo: initialState(),
      isUserPickedLocation: jest.fn(),
      sendShippingAddress: jest.fn(),
      onClickOnSelectedAddress: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <StockImmediately {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
