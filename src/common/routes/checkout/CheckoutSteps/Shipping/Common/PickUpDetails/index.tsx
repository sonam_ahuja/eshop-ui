import React, { FunctionComponent } from 'react';
import { Button, Divider, Section } from 'dt-components';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import StoreAddress from '@checkout/CheckoutSteps/Shipping/Common/StoreAddress';
import {
  IFee,
  IShippingAddressState
} from '@src/common/routes/checkout/store/shipping/types';
import { SHIPPING_TYPE } from '@src/common/routes/checkout/store/enums';
import EVENT_NAME from '@events/constants/eventName';

import { StyledPickUpDetails } from './styles';

export interface IProps {
  shippingInfo: IShippingAddressState;
  checkoutTranslation: ICheckoutTranslation;
  isUserPickedLocation(): void;
  sendShippingAddress(): void;
  onClickOnSelectedAddress?(): void;
}

const PickUpDetails: FunctionComponent<IProps> = (props: IProps) => {
  const { shippingInfo, checkoutTranslation } = props;
  const {
    isUserPickedLocation,
    sendShippingAddress,
    onClickOnSelectedAddress
  } = props;

  const onProceedToPayment = () => {
    sendShippingAddress();
  };

  const getShippingFee = (): string => {
    if (shippingInfo.shippingFee[shippingInfo.shippingType]) {
      const shippingFee = (shippingInfo.shippingFee[
        shippingInfo.shippingType
      ] as IFee).fee;

      return shippingFee === 0
        ? checkoutTranslation.shippingInfo.free
        : shippingFee > 0
        ? shippingFee.toString()
        : '';
    }

    return '';
  };

  const pickUpText =
    shippingInfo.shippingType === SHIPPING_TYPE.PARCEL_LOCKER
      ? checkoutTranslation.shippingInfo.parcelLockerText
      : checkoutTranslation.shippingInfo.pickUpStoreText;

  return (
    <StyledPickUpDetails>
      <StoreAddress
        isUserPickedLocation={isUserPickedLocation}
        shippingInfo={shippingInfo}
        checkoutTranslation={checkoutTranslation}
        onClickOnSelectedAddress={onClickOnSelectedAddress}
        boxShadow
        size='small'
      />

      <div className='personalInfoWrap'>
        <Divider dashed={true} />
        <div className='header'>
          {/* <Paragraph className='mainHeading' size='large' weight='bold'>
            {pickUpText}
          </Paragraph> */}
          <Section size='small' weight='medium' className='mainHeading'>
            {pickUpText}
          </Section>
          <Section size='large' className='shippingFee'>
            <div className='label'>
              {checkoutTranslation.shippingInfo.shippingFee}
            </div>
            <div className='value'>{getShippingFee()}</div>
          </Section>
        </div>

        <div className='content'>
          <Button
            size='medium'
            type='primary'
            onClickHandler={onProceedToPayment}
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.PROCEED_TO_PAYMENT}
            data-event-message={
              checkoutTranslation.shippingInfo.proceedToPayment
            }
          >
            {checkoutTranslation.shippingInfo.proceedToPayment}
          </Button>
        </div>
      </div>
    </StyledPickUpDetails>
  );
};

export default PickUpDetails;
