import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledStoreAddressMobile } from '../StoreAddress/styles';

export const StyledPickUpDetails = styled.div`
  background: ${colors.coldGray};
  padding: 1rem 1.25rem 1.25rem;
  height: 100%;
  .personalInfoWrap {
    .dt_divider {
      height: 3px;
      min-height: 3px;
      color: ${colors.lightGray};
      margin: 1.25rem 0 0.75rem;
    }

    /* Header Start */
    .header {
      display: flex;
      justify-content: space-between;
      margin-bottom: 1rem;

      .mainHeading {
        color: ${colors.darkGray};
        width: 50%;
      }
      .shippingFee {
        text-align: right;
        .label {
          color: ${colors.mediumGray};
        }
        .value {
          margin-left: 1.5rem;
          color: ${colors.darkGray};
        }
      }
    }
    /* Header End */

    /* Content */
    .content {
      .dt_button {
        margin-top: 1.5rem;
        width: 100%;
      }
    }
  }

  ${StyledStoreAddressMobile} {
    padding: 1.25rem 1rem 1rem;
    .textWrap {
      .addressWrap {
        .shopName.dt_title {
          font-size: 1.25rem;
          font-weight: 500;
          line-height: 1.5rem;
        }
      }
    }
    .button.dt_button {
      margin-top: 1.25rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 1rem 2.25rem 1.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 3.45rem 2.25rem 2.25rem;
    display: flex;
    flex-direction: column;
    flex: 1;

    .personalInfoWrap {
      display: flex;
      flex-direction: column;
      flex: 1;
      /* divider */
      .dt_divider {
        margin-bottom: 1.25rem;
      }
      /* Header Start */
      .header {
        .shippingFee {
          display: flex;
        }
      }
      /* Header End */
      /* Content */
      .content {
        display: flex;
        flex-direction: column;
        flex: 1;

        .dt_button {
          margin-left: auto;
          margin-top: auto;
          width: auto;
          align-self: flex-end;
          justify-self: flex-end;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 4.15rem 5.5rem 2.75rem;
  }
`;
