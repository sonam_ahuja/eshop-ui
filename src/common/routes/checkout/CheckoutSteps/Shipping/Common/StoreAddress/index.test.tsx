import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import * as CommonUtils from '@common/utils';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';

import StoreAddress, { IProps as IStoreAddressProps } from '.';

Object.defineProperty(CommonUtils, 'isBrowser', {
  get(): boolean {
    return false;
  },
  // tslint:disable-next-line:no-empty
  set(): void {}
});

describe('<StoreAddress />', () => {
  const props: IStoreAddressProps = {
    shippingInfo: appState().checkout.shippingInfo,
    checkoutTranslation: appState().translation.cart.checkout,
    isUserPickedLocation: jest.fn(),
    onClickOnSelectAddress: jest.fn(),
    onClickOnSelectedAddress: jest.fn()
  };

  const componentWrapper = (newpProps: IStoreAddressProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <StoreAddress {...newpProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is parcel locker', () => {
    const newProps: IStoreAddressProps = { ...props };
    newProps.shippingInfo.isPickUpLocationSelected = true;
    newProps.shippingInfo.selectedStoreAddress = {
      streetNumber: '12',
      address: '2ds',
      flatNumber: '12',
      postCode: '382345',
      city: 'Ahmedabad',
      state: 'gujarat',
      country: 'india',
      distance: '40',
      distanceUnit: 'km',
      name: 'Unknow',
      geoX: '74.34555',
      geoY: '42.4444'
    };

    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('event simulate', () => {
    const initStateValue = appState();
    const mockStore = configureStore();
    const newProps = { ...props };
    const store = mockStore(initStateValue);
    const { dummyState } = initStateValue.checkout.shippingInfo;

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <StoreAddress {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('StoreAddress')
      .instance() as StoreAddress).onClickHandler();
    (component.find('StoreAddress').instance() as StoreAddress).getSelectText();
    (component
      .find('StoreAddress')
      .instance() as StoreAddress).getMobileElement(dummyState);
  });
});
