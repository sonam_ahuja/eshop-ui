import React, { ReactNode } from 'react';
import { Button, Icon, Section, Title } from 'dt-components';
import cx from 'classnames';
import {
  IDummyState,
  IShippingAddressState
} from '@src/common/routes/checkout/store/shipping/types';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import { isMobile } from '@utils/index';
import { SHIPPING_TYPE } from '@src/common/routes/checkout/store/enums';
import { shippingAddressToDummyAddress } from '@src/common/routes/checkout/store/shipping/selector';
import EVENT_NAME from '@events/constants/eventName';
import WorkingHours from '@checkout/CheckoutSteps/Shipping/Common/WorkingHour';
import {
  sendSelectMachineEvent,
  sendSelectPointEvent,
  sendSelectStoreEvent
} from '@events/checkout/index';

import {
  StyledOpenHourText,
  StyledStoreAddress,
  StyledStoreAddressDesktop,
  StyledStoreAddressMobile
} from './styles';

type sizeType = 'small';

export interface IProps {
  shippingInfo: IShippingAddressState;
  checkoutTranslation: ICheckoutTranslation;
  boxShadow?: boolean;
  size?: sizeType;
  isUserPickedLocation(): void;
  onClickOnSelectAddress?(): void;
  onClickOnSelectedAddress?(): void;
}

export interface IState {
  showList: boolean;
}
class StoreAddress extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      showList: false
    };
    this.onClickHandler = this.onClickHandler.bind(this);
  }
  showWorkingHour = () => {
    this.setState(({ showList }) => ({ showList: !showList }));
  }

  onClickHandler(): void {
    const {
      isUserPickedLocation,
      shippingInfo: { isPickUpLocationSelected }
    } = this.props;
    const { onClickOnSelectAddress, onClickOnSelectedAddress } = this.props;

    isUserPickedLocation();
    if (isPickUpLocationSelected && onClickOnSelectedAddress) {
      onClickOnSelectedAddress();
    }
    if (!isPickUpLocationSelected && onClickOnSelectAddress) {
      if (
        this.props.shippingInfo.shippingType === SHIPPING_TYPE.PARCEL_LOCKER
      ) {
        sendSelectMachineEvent();
      } else if (
        this.props.shippingInfo.shippingType === SHIPPING_TYPE.PICK_UP_POINT
      ) {
        sendSelectPointEvent();
      } else if (
        this.props.shippingInfo.shippingType === SHIPPING_TYPE.PICK_UP_STORE &&
        this.props.shippingInfo &&
        this.props.shippingInfo.selectedStoreAddress &&
        this.props.shippingInfo.selectedStoreAddress.name
      ) {
        sendSelectStoreEvent(this.props.shippingInfo.selectedStoreAddress.name);
      }
      onClickOnSelectAddress();
    }
  }

  getSelectText(): string {
    switch (this.props.shippingInfo.shippingType) {
      case SHIPPING_TYPE.PARCEL_LOCKER:
        return this.props.checkoutTranslation.shippingInfo.selectMachine;
      case SHIPPING_TYPE.PICK_UP_POINT:
        return this.props.checkoutTranslation.shippingInfo.selectPoint;

      case SHIPPING_TYPE.PICK_UP_STORE:
        return this.props.checkoutTranslation.shippingInfo.selectStore;
      default:
        return this.props.checkoutTranslation.shippingInfo.selectStore;
    }
  }

  getSelectTextPath = () => {
    switch (this.props.shippingInfo.shippingType) {
      case SHIPPING_TYPE.PARCEL_LOCKER:
        return 'cart.checkout.shippingInfo.selectMachine';
      case SHIPPING_TYPE.PICK_UP_POINT:
        return 'cart.checkout.shippingInfo.selectPoint';

      case SHIPPING_TYPE.PICK_UP_STORE:
        return 'cart.checkout.shippingInfo.selectStore';
      default:
        return 'cart.checkout.shippingInfo.selectStore';
    }
  }

  getChooseOtherText(): string {
    return this.props.checkoutTranslation.shippingInfo.chooseOther;
  }

  getMobileElement(dummyState: IDummyState): ReactNode {
    const { shippingInfo } = this.props;

    return dummyState ? (
      <>
        <StyledStoreAddressMobile>
          <div className='textWrap'>
            {dummyState.distance ? (
              <div className='pin'>
                <Icon name='ec-solid-shop-localizer' size='medium' />
                <Section size='large'>
                  {dummyState.distance} {dummyState.distanceUnit}
                </Section>
              </div>
            ) : null}
            <div className='addressWrap'>
              <Title className='shopName' size='normal' weight='bold'>
                {dummyState.name}
              </Title>
              <Section className='address' size='large'>
                {dummyState.city}.{dummyState.streetAddress}{' '}
                {dummyState.flatNumber}, {dummyState.postCode}{' '}
                <div>
                  {dummyState.state}, {dummyState.country}
                </div>
              </Section>
            </div>
          </div>
          <Button
            className='button'
            size='small'
            type={
              shippingInfo.isPickUpLocationSelected ? 'secondary' : 'primary'
            }
            data-event-id={
              shippingInfo.isPickUpLocationSelected
                ? EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_OTHER
                : EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_SHIPPING
            }
            data-event-message={
              shippingInfo.isPickUpLocationSelected
                ? this.getChooseOtherText()
                : this.getSelectText()
            }
            data-event-path={
              shippingInfo.isPickUpLocationSelected
                ? 'cart.checkout.shippingInfo.chooseOther'
                : this.getSelectTextPath()
            }
            onClickHandler={this.onClickHandler}
          >
            {shippingInfo.isPickUpLocationSelected
              ? this.getChooseOtherText()
              : this.getSelectText()}
          </Button>
        </StyledStoreAddressMobile>
        {this.getOpenHourTextElement()}
      </>
    ) : null;
  }

  getDesktopElement(dummyState: IDummyState): ReactNode {
    const { shippingInfo } = this.props;

    return dummyState ? (
      <>
        <StyledStoreAddressDesktop>
          <div className='titleAndPin'>
            <div className='shopName'>{dummyState.name}</div>
            {dummyState.distance ? (
              <div className='pin'>
                <Icon name='ec-solid-shop-localizer' size='xsmall' />
                <div className='distanceUnit'>
                  {dummyState.distance} {dummyState.distanceUnit}
                </div>
              </div>
            ) : null}
          </div>
          <Section className='address' size='large'>
            {dummyState.city}.{dummyState.streetAddress} {dummyState.flatNumber}
            , {dummyState.postCode}{' '}
            <div>
              {dummyState.state}, {dummyState.country}
            </div>
          </Section>
          <Button
            className='button'
            size='small'
            type={
              shippingInfo.isPickUpLocationSelected ? 'secondary' : 'primary'
            }
            data-event-id={
              shippingInfo.isPickUpLocationSelected
                ? EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_OTHER
                : EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_SHIPPING
            }
            data-event-path={
              shippingInfo.isPickUpLocationSelected
                ? 'cart.checkout.shippingInfo.chooseOther'
                : this.getSelectTextPath()
            }
            data-event-message={
              shippingInfo.isPickUpLocationSelected
                ? this.getChooseOtherText()
                : this.getSelectText()
            }
            onClickHandler={this.onClickHandler}
          >
            {shippingInfo.isPickUpLocationSelected
              ? this.getChooseOtherText()
              : this.getSelectText()}
          </Button>
        </StyledStoreAddressDesktop>
        {this.getOpenHourTextElement()}
      </>
    ) : null;
  }

  getOpenHourTextElement = () => {
    const { checkoutTranslation, shippingInfo } = this.props;

    if (
      !shippingInfo.dummyState.openHours ||
      shippingInfo.dummyState.openHours.length === 0
    ) {
      return null;
    }

    const {
      showWorkingHours,
      hideWorkingHours,
      openHourDescription
    } = checkoutTranslation.shippingInfo;
    const text = this.state.showList ? hideWorkingHours : showWorkingHours;

    return (
      <>
        <StyledOpenHourText onClick={() => this.showWorkingHour()}>
          {text}
          <span className='timer-icon-wrapper'>
            <Icon name='ec-history' className='timer-icon' />
          </span>
        </StyledOpenHourText>
        {this.state.showList && (
          <WorkingHours
            openHours={shippingInfo.dummyState.openHours}
            description={openHourDescription}
          />
        )}
      </>
    );
  }

  render(): React.ReactNode {
    const { dummyState } = this.props.shippingInfo;

    const shippingInfo = this.props.shippingInfo;
    let shippingAddress: IDummyState = {};

    const { boxShadow, size } = this.props;
    const classes = cx('storeAddressWrap', size, { boxShadow });

    if (
      shippingInfo.isPickUpLocationSelected &&
      shippingInfo.selectedStoreAddress
    ) {
      shippingAddress = shippingAddressToDummyAddress(
        shippingInfo.selectedStoreAddress
      );
    } else {
      shippingAddress = dummyState;
    }

    return shippingAddress.geoX && shippingAddress.geoY ? (
      <StyledStoreAddress className={classes}>
        {isMobile.phone || isMobile.tablet
          ? this.getMobileElement(shippingAddress)
          : this.getDesktopElement(shippingAddress)}
      </StyledStoreAddress>
    ) : null;
  }
}

// tslint:disable-next-line:max-file-line-count
export default StoreAddress;
