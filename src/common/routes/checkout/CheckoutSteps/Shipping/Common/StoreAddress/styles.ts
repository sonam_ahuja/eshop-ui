import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledStoreAddressMobile = styled.div`
  padding: 2rem 1.25rem 1.5rem;
  .textWrap {
    display: flex;
    padding-left: 1rem;

    .pin {
      padding-right: 2rem;
      display: flex;
      flex-direction: column;
      align-items: center;

      i {
        padding-bottom: 0.5rem;
        font-size: 2rem !important;
      }
    }

    .addressWrap {
      .shopName {
        color: ${colors.darkGray};
        margin-bottom: 0.45rem;
      }
      .address {
        letter-spacing: 0.24px;
      }
    }
  }

  .button {
    margin-top: 2rem;
    width: 100%;
  }
`;

export const StyledStoreAddressDesktop = styled.div`
  display: flex;
  align-items: center;

  .titleAndPin {
    width: 8rem;
    flex-grow: 0;
    .shopName {
      font-size: 1rem;
      margin-bottom: 0.07rem;
      font-weight: 500;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.25;
      letter-spacing: normal;
      color: ${colors.darkGray};
    }
    .pin {
      display: flex;
      align-items: center;
      font-size: 0.75rem;
      .dt_icon {
        margin-right: 0.25rem;
        font-size: inherit;
      }
    }
  }

  .address {
    color: ${colors.ironGray};
  }

  .button {
    margin-left: auto;
  }
`;

export const StyledStoreAddress = styled.div`
  position: relative;

  background-color: ${colors.white};
  color: ${colors.ironGray};

  &.boxShadow {
    border-radius: 0.5rem;
    box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.1);
  }

  /* IF SIZE IS SMALL */
  &.small {
    /* For Mobile */
    ${StyledStoreAddressMobile} {
      padding: 1.25rem 1rem;
      .textWrap {
        padding-left: 0.4rem;

        .pin {
          padding-right: 1.5rem;
          i {
            font-size: 1.5rem;
          }
        }

        .addressWrap {
          .shopName {
            font-size: 1.25rem;
            font-weight: 500;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.2;
            letter-spacing: normal;
          }
        }
      }

      .button {
        margin-top: 1.5rem;
        width: 100%;
      }
    }

    /* For Desktop */
    ${StyledStoreAddressDesktop} {
      padding: 1rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    ${StyledStoreAddressMobile} {
      padding: 2rem 1.25rem 1.25rem;
      .textWrap {
        .pin {
          padding-right: 1.875rem;
        }
        .addressWrap {
          .shopName.dt_title {
            font-size: 1.75rem;
            font-weight: bold;
            line-height: 2rem;
            margin-bottom: 0.5rem;
          }
        }
      }
      .button.dt_button {
        height: 3rem;
        margin-top: 1.75rem;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    &.small {
      ${StyledStoreAddressDesktop} {
        padding: 1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    &.small {
      /* For Desktop */
      ${StyledStoreAddressDesktop} {
        padding: 1rem;
      }
    }
  }
`;

export const StyledOpenHourText = styled.div`
  display: flex;
  justify-content: center;
  padding: 10px 10px;
  color: #e20074;
`;
