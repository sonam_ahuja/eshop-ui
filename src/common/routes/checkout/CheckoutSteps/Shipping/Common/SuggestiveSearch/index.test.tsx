import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';

import SuggestiveSearch, { IProps as ISuggestiveSearchProps } from '.';

describe('<SuggestiveSearch />', () => {
  const props: ISuggestiveSearchProps = {
    className: 'someClass',
    checkoutTranslation: appState().translation.cart.checkout,
    onSelectItem: jest.fn(),
    loadingText: '',
    onEnterPressOnSearch: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SuggestiveSearch {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('event simulate', () => {
    const initStateValue = appState();
    const mockStore = configureStore();
    const newProps = { ...props };
    const store = mockStore(initStateValue);

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SuggestiveSearch {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('SuggestiveSearch')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as SuggestiveSearch).onEnterPress({
      // tslint:disable-next-line: no-empty
      persist: () => {},
      currentTarget: {
        value: 'value'
      },
      charCode: 13
    } as React.KeyboardEvent<HTMLInputElement>);
    (component
      .find('SuggestiveSearch')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as SuggestiveSearch).onEnterPress({
      // tslint:disable-next-line: no-empty
      persist: () => {},
      currentTarget: {
        value: 'value'
      },
      charCode: 12
    } as React.KeyboardEvent<HTMLInputElement>);
    (component
      .find('SuggestiveSearch')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as SuggestiveSearch).onChange({
      // tslint:disable-next-line: no-empty
      persist: () => {},
      target: {
        value: 'value'
      },
      currentTarget: {
        value: 'value'
      }
    } as React.ChangeEvent<HTMLInputElement>);

    (component
      .find('SuggestiveSearch')
      .instance() as SuggestiveSearch).onSearchClick();
    (component
      .find('SuggestiveSearch')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as SuggestiveSearch).errorComponent();
    (component.find('SuggestiveSearch').instance() as SuggestiveSearch)
      .getAddressesWrapper()
      // tslint:disable-next-line: no-empty
      .then(() => {})
      // tslint:disable-next-line: no-empty
      .catch(() => {});
  });
});
