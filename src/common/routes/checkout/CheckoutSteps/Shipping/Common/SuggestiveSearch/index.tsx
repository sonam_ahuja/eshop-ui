import React, { Component, ReactNode } from 'react';
import { Autocomplete, Icon, Paragraph } from 'dt-components';
import {
  getAddresses,
  getAddressViaGoogle
} from '@checkout/store/personalInfo/services';
import { IListItem } from 'dt-components/lib/es/components/molecules/autocomplete/list';
import { logError } from '@src/common/utils';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import { replacedString } from '@authentication/utils';
import { fetchGeoMetadata } from '@src/common/routes/checkout/utils/geoDataConverter';
import store from '@src/common/store';
import { IListItem as IShippingListItem } from '@src/common/routes/checkout/store/shipping/types';

import { StyledSuggestiveSearch } from './styles';

export interface IProps {
  className?: string;
  checkoutTranslation: ICheckoutTranslation;
  loadingText: string;
  onSelectItem(value: IListItem): void;
  onEnterPressOnSearch(value: string): void;
}

export interface IState {
  searchValue: string;
  noResultFromGoogleApi: boolean;
  isSearchEmpty: boolean;
  isFallbackCalled: boolean;
}

class SuggestiveSearch extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      searchValue: '',
      noResultFromGoogleApi: false,
      isSearchEmpty: false,
      isFallbackCalled: false
    };

    this.onEnterPress = this.onEnterPress.bind(this);
    this.onChange = this.onChange.bind(this);
    this.getAddressesWrapper = this.getAddressesWrapper.bind(this);
    this.errorComponent = this.errorComponent.bind(this);
    this.onSearchClick = this.onSearchClick.bind(this);
    this.getAddressFromGoogleApi = this.getAddressFromGoogleApi.bind(this);
    this.checkForGoogleSuggestionFlag = this.checkForGoogleSuggestionFlag.bind(
      this
    );
  }

  onEnterPress(event: React.KeyboardEvent<HTMLInputElement>): void {
    event.persist();
    if (event.charCode === 13) {
      // tslint:disable:no-commented-code
      // this.props.onEnterPressOnSearch(event.currentTarget.value.trim());
    }
  }

  onChange(event: React.ChangeEvent<HTMLInputElement>): void {
    event.persist();
    this.setState({
      searchValue: event.currentTarget.value
    });

    if (!event.target.value.trim().length) {
      this.setState({
        isSearchEmpty: false
      });
    }
  }

  onSearchClick(): void {
    // this.props.onEnterPressOnSearch(this.state.searchValue.trim());
  }

  getAddressFromGoogleApi(searchTerm: string): Promise<IListItem[]> {
    const {
      enableGoogleAddressSuggestion
    } = store.getState().configuration.cms_configuration.global;
    if (!enableGoogleAddressSuggestion) {
      return (
        getAddressViaGoogle(searchTerm)
          .then(googleResponse => {
            if (googleResponse.length > 0) {
              this.setState({ isSearchEmpty: false, isFallbackCalled: true });

              return googleResponse;
            } else {
              this.setState({ isSearchEmpty: true });
            }

            return googleResponse;
          })
          // tslint:disable-next-line:no-any
          .catch((error: any) => {
            logError(error);

            return Promise.resolve([]);
          })
      );
    } else {
      this.setState({ isSearchEmpty: false });

      return Promise.resolve([]);
    }
  }

  getAddressesWrapper(): Promise<IListItem[]> {
    const searchValueLength = this.state.searchValue.trim().length;
    if (searchValueLength && searchValueLength > 3) {
      return (
        getAddresses(this.state.searchValue)
          .then(response => {
            if (response.length > 0) {
              this.setState({ isSearchEmpty: false });

              return response;
            } else {
              return this.getAddressFromGoogleApi(this.state.searchValue);
            }

            return response;
          })
          // tslint:disable:no-any
          // tslint:disable-next-line:no-identical-functions
          .catch((error: any) => {
            logError(error);

            return Promise.resolve([]);
          })
      );
    } else {
      this.setState({ isSearchEmpty: false });

      return Promise.resolve([]);
    }
  }

  errorComponent(): ReactNode {
    return (
      <div className='resultNotFound'>
        <Paragraph weight='bold'>
          {replacedString(
            this.props.checkoutTranslation.shippingInfo.searchErrorMsg,
            '{0}',
            this.state.searchValue.trim()
          )}
        </Paragraph>
      </div>
    );
  }

  checkForGoogleSuggestionFlag(data: IShippingListItem): void {
    const {
      enableGoogleAddressSuggestion
    } = store.getState().configuration.cms_configuration.global;

    if (enableGoogleAddressSuggestion || this.state.isFallbackCalled) {
      fetchGeoMetadata(data.id as string, data.title).then(response => {
        this.setState({ isFallbackCalled: false });
        this.props.onSelectItem(response as IListItem);
      });
    } else {
      this.props.onSelectItem(data);
    }
  }

  render(): ReactNode {
    const { className, loadingText } = this.props;
    const { isSearchEmpty } = this.state;

    return (
      <StyledSuggestiveSearch className={className}>
        <Autocomplete
          className='suggestiveSearchInput'
          getItems={this.getAddressesWrapper}
          onChange={this.onChange}
          onItemSelect={this.checkForGoogleSuggestionFlag}
          debounceInterval={300}
          autoCompleteValuesOnly={false}
          minCharacter={2}
          onKeyPress={this.onEnterPress}
          showDismissIcon={false}
          loadingText={loadingText}
        />
        <Icon
          size='small'
          color='currentColor'
          className='icon'
          name='ec-search'
          onClick={this.onSearchClick}
        />

        {isSearchEmpty && this.errorComponent()}
      </StyledSuggestiveSearch>
    );
  }
}

export default SuggestiveSearch;
