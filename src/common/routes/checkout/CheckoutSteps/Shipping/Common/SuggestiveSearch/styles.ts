import { colors } from '@src/common/variables';
import styled from 'styled-components';

export const StyledSuggestiveSearch = styled.div`
  position: relative;
  margin: 1.25rem 0;

  .suggestiveSearchInput {
    padding: 0;

    input:focus {
      border-bottom-color: transparent;
    }

    input {
      width: 100%;
      height: 3rem;
      border-radius: 0.5rem;
      box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.1);
      background-color: ${colors.white};
      color: ${colors.darkGray};
      padding: 0 3rem 0 1rem;
    }
  }
  .suggestiveSearchInput + ul {
    margin-top: 0.25rem;
  }

  .icon {
    position: absolute;
    z-index: 10;
    right: 1rem;
    top: 0.85rem;
    color: ${colors.charcoalGray};
  }

  .resultNotFound {
    padding: 1.25rem;
    border-radius: 0.5rem;
    box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.1);
    background-color: ${colors.white};
    color: ${colors.mediumGray};
    position: absolute;
    top: 100%;
    margin-top: 0.25rem;
    z-index: 20;
    width: 100%;
  }
`;
