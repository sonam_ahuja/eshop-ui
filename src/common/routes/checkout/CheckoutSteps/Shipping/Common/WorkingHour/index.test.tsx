import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
// @
// import appState from '@store/states/app';
// import { Provider } from 'react-redux';
// import configureStore from 'redux-mock-store';
// import { StaticRouter } from 'react-router-dom';

import WorkingHour from '.';
import { IProps } from './types';

describe('<SuggestiveSearch />', () => {
  const props: IProps = {
    description: 'description',
    openHours: [
      {
        day: ['monday'],
        hourPeriod: [
          {
            startHour: '8:00',
            endHour: '13:00'
          }
        ]
      },
      {
        day: ['tuesday', 'friday'],
        hourPeriod: [
          {
            startHour: '8:00',
            endHour: '13:00'
          }
        ]
      }
    ]
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <WorkingHour {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly with empty open hour ', () => {
    const newProps = { ...props };
    const component = mount(
      <ThemeProvider theme={{}}>
        <WorkingHour {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
    (component
      .find('WorkingHour')
      .instance() as WorkingHour).getFlattedOpenHours([]);
    (component.find('WorkingHour').instance() as WorkingHour).formatOpenHourDay(
      ['Mon']
    );
    (component.find('WorkingHour').instance() as WorkingHour).formatOpenHourDay(
      []
    );
    (component.find('WorkingHour').instance() as WorkingHour).formatOpenHourDay(
      ['Mon', 'TUE']
    );
  });
});
