import React from 'react';
import { IOpenHours } from '@src/common/routes/checkout/store/shipping/types';
import { SHIPPING_OPEN_HOUR_DAY_SLICE } from '@checkout/CheckoutSteps/constants';
import { logError } from '@src/common/utils';
import { Divider } from 'dt-components';
import { getGlobalTranslation } from '@store/common';

import { IProps } from './types';
import { StyledDescription, StyledRow, StyledWorkingHours } from './styles';

export default class WorkingHour extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  getFlattedOpenHours(openHours: IOpenHours[]): string[][] {
    if (openHours && openHours.length === 0) {
      return [''][''];
    }

    return openHours.map(openHour => {
      const slots = openHour.hourPeriod.map(period => {
        return `${period.startHour} - ${period.endHour}`;
      });
      const dayName = this.formatOpenHourDay(openHour.day);

      return [dayName, ...slots];
    });
  }

  formatOpenHourDay(day: string[]): string {
    const daysTranslation = getGlobalTranslation().days;

    try {
      if (day.length === 1) {
        return (
          daysTranslation[day[0]] &&
          `${daysTranslation[day[0]]
            .trim()
            .slice(0, SHIPPING_OPEN_HOUR_DAY_SLICE)}`
        );
      } else if (day.length > 1) {
        return `${daysTranslation[day[0]]
          .trim()
          .slice(0, SHIPPING_OPEN_HOUR_DAY_SLICE)}- ${daysTranslation[
          day[day.length - 1]
        ]
          .trim()
          .slice(0, SHIPPING_OPEN_HOUR_DAY_SLICE)}`;
      } else {
        return daysTranslation[day[0]];
      }
    } catch (error) {
      logError(error);

      return '';
    }
  }

  render(): React.ReactNode {
    const { openHours } = this.props;
    const formattedList = this.getFlattedOpenHours(openHours);

    return (
      <StyledWorkingHours>
        <StyledRow>
          {formattedList.map(list => (
            <>
              <li key={list[0]}>
                {list.map(hoursInfo => (
                  <span className='open-hour-info' key={hoursInfo}>
                    {hoursInfo}
                  </span>
                ))}
              </li>
              <Divider dashed />
            </>
          ))}
        </StyledRow>
        <StyledDescription>{this.props.description}</StyledDescription>
      </StyledWorkingHours>
    );
  }
}
