import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledWorkingHours = styled.div`
  background: ${colors.white};
  padding: 1rem 0;

  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
  }
`;

export const StyledRow = styled.ul`
  li {
    display: flex;
    padding: 0.5rem 1rem;
    > span {
      width: 100%;
      &:nth-child(1) {
        width: 5rem;
        flex-shrink: 0;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    width: 75%;
  }
`;

export const StyledDescription = styled.div`
  padding: 0.25rem 1rem 0;

  @media (min-width: ${breakpoints.desktop}px) {
    width: 25%;
  }
`;
