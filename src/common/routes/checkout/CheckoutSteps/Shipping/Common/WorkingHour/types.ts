import { IOpenHours } from '@checkout/store/shipping/types';

export interface IProps {
  openHours: IOpenHours[];
  description: string;
}

export interface IFlatOpenHours {
  [key: string]: string;
}
