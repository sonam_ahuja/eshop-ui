import React from 'react';
import { Icon } from 'dt-components';

import { StyledIconWrap, StyledZoomInOut } from './styles';

interface IProps {
  onZoomInClick?(): void;
  onZoomOutClick?(): void;
}

const ZoomInOut = (props: IProps) => {
  const { onZoomInClick, onZoomOutClick } = props;

  return (
    <StyledZoomInOut>
      <StyledIconWrap onClick={onZoomInClick}>
        <Icon color='currentColor' size='inherit' name='ec-solid-plus' />
      </StyledIconWrap>
      <StyledIconWrap onClick={onZoomOutClick}>
        <Icon color='currentColor' size='inherit' name='ec-solid-minus' />
      </StyledIconWrap>
    </StyledZoomInOut>
  );
};

export default ZoomInOut;
