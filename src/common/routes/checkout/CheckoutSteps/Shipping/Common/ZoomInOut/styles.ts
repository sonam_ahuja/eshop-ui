import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledIconWrap = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  align-items: center;
  justify-content: center;
  color: ${colors.gray};
  font-size: 16px;
  padding: 7px 0;
  border-radius: inherit;
  opacity: 0.8;
  transition: color 0.2s linear 0s;
  cursor: pointer;

  &:hover {
    opacity: 1;
    color: ${colors.darkGray};
  }
`;

export const StyledZoomInOut = styled.div`
  display: none;
  border-radius: 22px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.09);
  background-color: ${colors.white};

  flex-direction: column;
  width: 40px;
  height: 80px;
  align-items: center;
  justify-content: space-between;

  @media (min-width: ${breakpoints.desktop}px) {
    display: inline-flex;
  }
`;
