import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import DeliveryOptions, { IProps as IDeliveryOptionsProps } from '.';

describe('< DeliveryOptions />', () => {
  const props: IDeliveryOptionsProps = {
    shippingInfo: appState().checkout.shippingInfo,
    shippingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.shipping,
    shippingTranslation: appState().translation.cart.checkout.shippingInfo,
    globalCurrency: appState().configuration.cms_configuration.global.currency,
    setDeliveryOptions: jest.fn(),
    updateSelectedDate: jest.fn(),
    fetchDeliveryDate: jest.fn(),
    evaluateCartSummary: jest.fn()
  };
  const componentWrapper = (newpProps: IDeliveryOptionsProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <DeliveryOptions {...newpProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, handle click on DeliveryOptionCard', () => {
    const component = componentWrapper(props);
    const deliveryOptionsEl = component.find('div[onClick]');
    deliveryOptionsEl.at(0).simulate('click');
    deliveryOptionsEl.at(1).simulate('click');
    deliveryOptionsEl.at(2).simulate('click');
    deliveryOptionsEl.at(3).simulate('click');
  });
});
