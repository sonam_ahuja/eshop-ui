import {
  ICurrencyConfiguration,
  IShipping
} from '@src/common/store/types/configuration';
import React, { FunctionComponent } from 'react';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import {
  IFee,
  IShippingAddressState,
  shippingType
} from '@checkout/store/shipping/types';
import { IShippingInfoTranslation } from '@src/common/store/types/translation';
import { Section, Select, Selector, Title } from 'dt-components';
import SimpleSelect from '@src/common/components/Select/SimpleSelect';
import { isMobile } from '@src/common/utils';
import {
  getConvertedDate,
  getSelectedDate
} from '@checkout/CheckoutSteps/Shipping/utils/dateConvert';
import { ICartSummary } from '@src/common/routes/basket/store/types';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledDeliveryOptions } from './styles';

export interface IProps {
  shippingInfo: IShippingAddressState;
  shippingConfiguration: IShipping;
  shippingTranslation: IShippingInfoTranslation;
  globalCurrency: ICurrencyConfiguration;
  setDeliveryOptions(type: shippingType): void;
  updateSelectedDate(type: string): void;
  fetchDeliveryDate(type: string): void;
  evaluateCartSummary(cartSummary: ICartSummary[]): void;
}

const DeliveryOptions: FunctionComponent<IProps> = (props: IProps) => {
  const { shippingType: typeOfShipping } = props.shippingInfo;
  const {
    setDeliveryOptions,
    shippingInfo,
    shippingTranslation,
    globalCurrency,
    evaluateCartSummary
  } = props;
  const {
    deliveryOptionTxt,
    standard: standardTxt,
    withIn24: withIn24txt,
    pickADate: pickADateTxt,
    nextDay
  } = props.shippingTranslation.deliveryOptions;

  const onDeliveryOptionClick = (shippingOption: shippingType): void => {
    if (shippingInfo.shippingType !== shippingOption) {
      setDeliveryOptions(shippingOption);
      if (shippingInfo && shippingInfo.shippingFee[shippingOption]) {
        evaluateCartSummary(
          (shippingInfo.shippingFee[shippingOption] as IFee).cartSummary
        );
      }
    }
  };

  const checkShippingFee = (
    fee: number,
    currency: ICurrencyConfiguration
  ): string => {
    if (fee === -1) {
      return '';
    } else if (fee === 0) {
      return shippingTranslation.free;
    } else {
      return `${fee.toString()} ${currency.currencySymbol}`;
    }
  };

  const convertedPickADate = getConvertedDate(shippingInfo.pickADateList);

  return (
    <StyledDeliveryOptions>
      <Title className='mainHeading' size='small' weight='bold'>
        {deliveryOptionTxt}
      </Title>
      <div className='selectorWrap'>
        {Object.keys(shippingInfo.shippingFee).map(deliveryMethod => {
          if (deliveryMethod === 'standard') {
            return (
              <Selector
                type='detailed'
                onClick={() => onDeliveryOptionClick(SHIPPING_TYPE.STANDARD)}
                active={typeOfShipping === SHIPPING_TYPE.STANDARD}
                className='standard'
              >
                <div className='textWrap'>
                  <div className='heading'>{standardTxt}</div>
                  <div className='subHeading'>
                    {shippingTranslation.businessDays}
                  </div>
                </div>
                <Section size='small' weight='bold' className='fee'>
                  {checkShippingFee(
                    (shippingInfo.shippingFee.standard as IFee).fee,
                    globalCurrency
                  )}
                </Section>
              </Selector>
            );
          }

          if (deliveryMethod === 'withinTime') {
            return (
              <Selector
                type='detailed'
                onClick={() => onDeliveryOptionClick(SHIPPING_TYPE.SAME_DAY)}
                active={typeOfShipping === SHIPPING_TYPE.SAME_DAY}
                className='withinTime'
              >
                <div className='textWrap'>
                  <div className='heading'>{withIn24txt}</div>
                  <div className='subHeading'>{nextDay}</div>
                </div>
                <Section size='small' weight='bold' className='fee'>
                  {checkShippingFee(
                    (shippingInfo.shippingFee.withinTime as IFee).fee,
                    globalCurrency
                  )}
                </Section>
              </Selector>
            );
          }

          if (deliveryMethod === 'pickADate') {
            return (
              <Selector
                type='detailed'
                onClick={() => onDeliveryOptionClick(SHIPPING_TYPE.EXACT_DAY)}
                active={typeOfShipping === SHIPPING_TYPE.EXACT_DAY}
                className='pickADate'
              >
                <div className='textWrap'>
                  <div className='heading'>{pickADateTxt}</div>
                  <div className='subHeading'>
                    <Select
                      useNativeDropdown={isMobile.phone}
                      items={convertedPickADate}
                      selectedItem={getSelectedDate(
                        convertedPickADate,
                        shippingInfo.selectedDeliveryDate
                      )}
                      onItemSelect={item => {
                        sendDropdownClickEvent(item.title);
                        props.updateSelectedDate(item.value);
                      }}
                      SelectedItemComponent={selectedItemProps => (
                        <>
                          <SimpleSelect
                            {...selectedItemProps}
                            isOpen
                            selectedItem={{
                              id:
                                selectedItemProps &&
                                selectedItemProps.selectedItem
                                  ? selectedItemProps.selectedItem.id
                                  : '',
                              title:
                                selectedItemProps &&
                                selectedItemProps.selectedItem
                                  ? selectedItemProps.selectedItem.title
                                  : ''
                            }}
                          />
                        </>
                      )}
                    />
                  </div>
                </div>
                <Section size='small' weight='bold' className='fee'>
                  {checkShippingFee(
                    (shippingInfo.shippingFee.pickADate as IFee).fee,
                    globalCurrency
                  )}
                </Section>
              </Selector>
            );
          }

          return null;
        })}
      </div>
    </StyledDeliveryOptions>
  );
};

export default DeliveryOptions;
