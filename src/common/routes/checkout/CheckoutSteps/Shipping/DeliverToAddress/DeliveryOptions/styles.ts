import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledDeliveryOptions = styled.div`
  .mainHeading {
    margin: 1.7rem 0 1.7rem;
    color: ${colors.mediumGray};
  }

  .selectorWrap {
    .dt_selector {
      width: 100%;
      justify-content: space-between;
      margin-top: 0.125rem;
      margin-bottom: 0.125rem;
      padding: 1.25rem;

      .heading {
        font-size: 1rem;
        font-weight: bold;
        line-height: 1;
        letter-spacing: normal;
        color: ${colors.ironGray};
      }

      .subHeading,
      .subHeading .dt_paragraph {
        font-size: 1rem;
        font-weight: bold;
        line-height: 1;
        letter-spacing: normal;
        color: ${colors.darkGray};
      }

      .fee {
        color: ${colors.ironGray};
        align-self: flex-start;
      }

      &.active {
        .fee {
          color: ${colors.magenta};
        }
        .subHeading {
          .dt_icon {
            svg {
              path {
                fill: ${colors.magenta};
              }
            }
          }
        }
        .heading {
          color: ${colors.mediumGray};
        }
      }

      .subHeading {
        .dt_icon {
          svg {
            path {
              fill: ${colors.mediumGray};
            }
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .mainHeading {
      font-weight: 900;
      margin: 1.5rem 0 2rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .selectorWrap {
      display: flex;
      justify-content: space-between;
      margin: -0.125rem;

      .dt_selector {
        flex: 1;
        margin: 0.125rem;
        padding: 1rem;
        min-height: 8.5rem;
        align-items: flex-start;
        flex-direction: column;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .selectorWrap {
      margin: -0.15rem;

      .dt_selector {
        margin: 0.15rem;
        min-height: 7rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .mainHeading {
      margin: 1.4rem 0 2.05rem;
    }
  }
`;
