import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { IProps } from '@checkout/CheckoutSteps/Shipping/DeliverToAddress/Form/types';

import Form from '.';

describe('<Checkout Form />', () => {
  const props: IProps = {
    shippingInfo: appState().checkout.shippingInfo,
    loadingText: '',
    shippingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.shipping,
    shippingTranslation: appState().translation.cart.checkout.shippingInfo,
    buttonEnable: false,
    personalInfo: appState().checkout.personalInfo,
    updateShippingAddress: jest.fn(),
    setProceedButton: jest.fn(),
    updateFormChangeField: jest.fn(),
    updateDummyState: jest.fn(),
    fetchShippingFee: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Form {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    const component = mount<Form>(
      <ThemeProvider theme={{}}>
        <Form {...copyComponentProps} />
      </ThemeProvider>
    );
    const inputEvent = {
      target: { value: 'dummy' },
      currentTarget: { value: 'dummy' },
      persist: jest.fn(),
      preventDefault: jest.fn()
    };
    (component.find('Form').instance() as Form).handleChange(
      'streetNumber',
      // tslint:disable-next-line:no-any
      inputEvent as any
    );
    (component.find('Form').instance() as Form).handleKeydown(
      'streetNumber',
      // tslint:disable-next-line:no-any
      inputEvent as any
    );
    (component.find('Form').instance() as Form).onFocus(
      'streetNumber',
      // tslint:disable-next-line:no-any
      inputEvent as any
    );
    (component.find('Form').instance() as Form).setSearchAddress({});
    (component.find('Form').instance() as Form).checkForGoogleSuggestionFlag({
      id: '',
      title: ''
    });
    (component.find('Form').instance() as Form).onBlur('city');
    (component.find('Form').instance() as Form).detectScroll();
    (component.find('Form').instance() as Form).componentWillReceiveProps(
      copyComponentProps
    );
    (component.find('Form').instance() as Form).checkIfFieldExists(
      'streetNumber'
    );
    (component.find('Form').instance() as Form).componentWillUnmount();
  });
});
