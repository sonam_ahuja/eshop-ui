import {
  getLabelValueForFormFields,
  isFormFieldValidAsPerType,
  isInputAllowed,
  readyToProceed
} from '@checkout/CheckoutSteps/Shipping/utils/formFields';
import { getAddresses } from '@src/common/routes/checkout/store/personalInfo/services';
import React, { Component, ReactNode } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import { SHIPPING_CONSTANTS } from '@src/common/routes/checkout/store/shipping/enums';
import { Autocomplete, FloatLabelInput, Utils } from 'dt-components';
import { isMobile } from '@src/common/utils';
import { shippingFormState } from '@checkout/CheckoutSteps/Shipping/DeliverToAddress/shippingFormGrid';
import {
  IAddressResponse,
  IPersonalInfoFormFields
} from '@src/common/routes/checkout/store/personalInfo/types';
import {
  IFormFieldValue,
  IListItem,
  IShippingFormField
} from '@src/common/routes/checkout/store/shipping/types';
import {
  charEPresent,
  ignoreSpaces
} from '@checkout/CheckoutSteps/PersonalInfo/utils';
import CONSTANTS from '@checkout/CheckoutSteps/PersonalInfo/constants';
import { FormWrapper } from '@src/common/routes/checkout/Common/FormWrapper/styles';
import { checkIfValidateField } from '@checkout/CheckoutSteps/utils';
import store from '@src/common/store';
import { fetchGeoMetadata } from '@src/common/routes/checkout/utils/geoDataConverter';
import { SHIPPING_FEE_DEBOUNCE_TIME } from '@checkout/CheckoutSteps/constants';
import { sendContentFilledOutEvent } from '@events/checkout';
import { FORM_NAME } from '@events/constants/eventName';

import { IProps, IState } from './types';

class Form extends Component<IProps, IState> {
  filledOffSet: { name: string; offsetTop: number }[] = [];
  scrollPos = 0;

  constructor(props: IProps) {
    super(props);
    this.state = shippingFormState();
    this.createForm = this.createForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeydown = this.handleKeydown.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.checkForGoogleSuggestionFlag = this.checkForGoogleSuggestionFlag.bind(
      this
    );
    this.fetchShippingFee = Utils.debounce(
      this.fetchShippingFee.bind(this),
      SHIPPING_FEE_DEBOUNCE_TIME
    );
  }

  componentDidMount(): void {
    if (this.props.shippingInfo.sameAsPersonalInfo) {
      this.updateFromField(this.props.shippingInfo.dummyState, true, false);
    } else if (this.props.shippingInfo) {
      this.updateFromField(this.props.shippingInfo, true, false);
    }
    window.addEventListener('scroll', this.detectScroll, {
      passive: true
    });
  }

  componentWillUnmount(): void {
    window.removeEventListener('scroll', this.detectScroll);
  }

  detectScroll(): void {
    this.scrollPos = document.body.getBoundingClientRect().top;
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const { shippingInfo, personalInfo } = this.props;

    if (
      nextProps.shippingInfo.sameAsPersonalInfo !==
        shippingInfo.sameAsPersonalInfo &&
      nextProps.shippingInfo.sameAsPersonalInfo
    ) {
      this.updateFromField(personalInfo.formFields, true, false);
    } else if (
      nextProps.shippingInfo.sameAsPersonalInfo !==
        shippingInfo.sameAsPersonalInfo &&
      !nextProps.shippingInfo.sameAsPersonalInfo
    ) {
      this.updateFromField(shippingInfo, true, false);
      // validate if form fields are valid on toggle: false
    } else if (
      nextProps.shippingInfo.streetAddressId !== shippingInfo.streetAddressId
    ) {
      this.updateFromField(nextProps.shippingInfo, true, false);
    }
  }

  fetchShippingFee(): void {
    this.props.fetchShippingFee();
  }

  updateShippingAddress(
    field: string,
    inputValue: string,
    isValidState: boolean,
    validationMessage: string
  ): void {
    this.props.updateShippingAddress({
      key: field,
      value: inputValue,
      isValid: isValidState,
      validationMessage
    });
  }

  handleKeydown(
    field: string,
    event: React.KeyboardEvent<HTMLInputElement>
  ): void {
    if (ignoreSpaces(event)) {
      event.preventDefault();
    }
    if (
      this.props.shippingConfiguration.form.fields[field].inputType ===
        CONSTANTS.NUMBER &&
      charEPresent(event)
    ) {
      event.preventDefault();

      return;
    }
  }

  handleChange(
    field: string,
    event?: React.ChangeEvent<HTMLInputElement>,
    value?: string
  ): void {
    const {
      fields: cmsFormConfiguration
    } = this.props.shippingConfiguration.form;
    let inputValue = '';

    if (event) {
      inputValue = (event.target as HTMLInputElement).value;
      event.persist();
    }
    if (value) {
      inputValue = value;
    }

    const specificConfiguration = cmsFormConfiguration[field];

    if (
      !isInputAllowed(
        specificConfiguration.inputType,
        inputValue,
        specificConfiguration.validation
      )
    ) {
      return;
    }

    const isValidValue = isFormFieldValidAsPerType(
      specificConfiguration.validation.value,
      inputValue,
      specificConfiguration.validation.type
    );

    this.setState(
      state => ({
        ...state,
        formFields: {
          ...state.formFields,
          [field]: {
            ...this.state.formFields[field],
            value: inputValue,
            isValid: isValidValue,
            validationMessage: specificConfiguration.validation.message
          }
        }
      }),
      () => {
        const status = readyToProceed(
          this.state.formFields,
          cmsFormConfiguration
        );

        if (!this.props.shippingInfo.sameAsPersonalInfo && isValidValue) {
          this.updateShippingAddress(
            field,
            inputValue,
            isValidValue,
            specificConfiguration.validation.message
          );
        }

        if (isValidValue && this.props.shippingInfo.sameAsPersonalInfo) {
          this.props.updateDummyState({
            [field]: {
              value: this.state.formFields[field].value
            }
          });
        }
        this.props.setProceedButton(status);

        if (
          field === 'postCode' &&
          inputValue.length >= Number(specificConfiguration.validation.value)
        ) {
          this.fetchShippingFee();
        }
      }
    );
  }

  // tslint:disable-next-line:cognitive-complexity
  setSearchAddress(searchAddressResponse: IAddressResponse): void {
    for (const field in searchAddressResponse) {
      if (field === SHIPPING_CONSTANTS.ADDRESS) {
        if (this.state.formFields[SHIPPING_CONSTANTS.STREET_ADDRESS]) {
          if (this.props.shippingInfo.sameAsPersonalInfo) {
            this.props.updateDummyState({
              [SHIPPING_CONSTANTS.STREET_ADDRESS as string]: {
                value: searchAddressResponse[field]
              }
            });
          } else {
            this.updateShippingAddress(
              SHIPPING_CONSTANTS.STREET_ADDRESS,
              searchAddressResponse[field] as string,
              true,
              this.props.shippingConfiguration.form.fields[
                SHIPPING_CONSTANTS.STREET_ADDRESS
              ].validation.message
            );
          }
        }
      } else {
        let fieldTemp = field;

        if (field === SHIPPING_CONSTANTS.ID) {
          fieldTemp = SHIPPING_CONSTANTS.STREET_ADDRESS_ID;
        }

        if (this.state.formFields[field]) {
          if (this.props.shippingInfo.sameAsPersonalInfo) {
            this.props.updateDummyState({
              [fieldTemp]: {
                value: searchAddressResponse[field]
              }
            });
          } else {
            this.updateShippingAddress(
              fieldTemp,
              searchAddressResponse[field] as string,
              true,
              this.props.shippingConfiguration.form.fields[field].validation
                .message
            );
          }
        }
      }
    }

    this.updateFromField(searchAddressResponse, true, true);
    this.fetchShippingFee();
  }

  checkIfFieldExists(field: string): boolean {
    return Object.keys(this.state.formFields).indexOf(field) > -1;
  }

  // tslint:disable-next-line:cognitive-complexity
  updateFromField(
    address: IAddressResponse | IShippingFormField | IPersonalInfoFormFields,
    isValid: boolean = false,
    isSearchData: boolean = false
  ): IShippingFormField {
    const newFormFields = shippingFormState().formFields;
    const {
      fields: cmsFormConfiguration
    } = this.props.shippingConfiguration.form;

    // tslint:disable:forin
    for (const field in newFormFields) {
      if (field === SHIPPING_CONSTANTS.STREET_ADDRESS) {
        if (address.hasOwnProperty(SHIPPING_CONSTANTS.ADDRESS)) {
          newFormFields[field as string].value =
            address[SHIPPING_CONSTANTS.ADDRESS];
          newFormFields[field as string].isValid = checkIfValidateField(
            field,
            address[SHIPPING_CONSTANTS.ADDRESS],
            isSearchData,
            isValid,
            cmsFormConfiguration
          );
        } else if (address.hasOwnProperty(SHIPPING_CONSTANTS.STREET_ADDRESS)) {
          newFormFields[field as string].value =
            address[SHIPPING_CONSTANTS.STREET_ADDRESS];
          newFormFields[field as string].isValid = checkIfValidateField(
            field,
            address[SHIPPING_CONSTANTS.STREET_ADDRESS],
            isSearchData,
            isValid,
            cmsFormConfiguration
          );
        }
      } else if (address.hasOwnProperty(field)) {
        newFormFields[field].value = address[field] ? address[field] : '';
        newFormFields[field].isValid = checkIfValidateField(
          field,
          newFormFields[field].value,
          isSearchData,
          isValid,
          cmsFormConfiguration
        );
      }
    }

    this.setState(
      state => ({
        ...state,
        formFields: newFormFields
      }),
      () => {
        const status = readyToProceed(
          this.state.formFields,
          cmsFormConfiguration
        );
        this.props.setProceedButton(status);
      }
    );

    return newFormFields;
  }

  onBlur(field: string): void {
    document.body.classList.remove('hideDrawer');
    this.props.updateFormChangeField(true);
    sendContentFilledOutEvent(
      FORM_NAME.SHIPPING_INFO,
      getLabelValueForFormFields(this.props, field)
    );
  }

  onFocus(field: string, event: React.FocusEvent<HTMLInputElement>): void {
    document.body.classList.add('hideDrawer');
    if (event) {
      this.filledOffSet.forEach(
        (element: { name: string; offsetTop: number }) => {
          if (
            element.name === field &&
            element.offsetTop > window.screen.height / 2
          ) {
            window.scrollTo(0, element.offsetTop / 1.5);
          }
        }
      );
    }
  }

  floatLabelReference(
    name: string,
    element: HTMLDivElement | null,
    index: number
  ): void {
    if (element) {
      this.filledOffSet[index] = { name, offsetTop: element.offsetTop };
    }
  }

  checkForGoogleSuggestionFlag(data: IListItem): void {
    const {
      enableGoogleAddressSuggestion
    } = store.getState().configuration.cms_configuration.global;

    if (enableGoogleAddressSuggestion) {
      fetchGeoMetadata(data.id as string, data.title).then(response => {
        this.setSearchAddress(response);
      });
    } else {
      this.setSearchAddress(data as IAddressResponse);
    }
  }

  createForm(): ReactNode | ReactNode[] {
    const { formFields } = this.state;
    const { sameAsPersonalInfo } = this.props.shippingInfo;
    const { loadingText } = this.props;
    const {
      fields: cmsFormConfiguration
    } = this.props.shippingConfiguration.form;

    return Object.keys(formFields).map((field, index) => {
      return (
        <>
          {cmsFormConfiguration[field] && cmsFormConfiguration[field].show && (
            <Column
              key={field}
              ref={element => {
                this.floatLabelReference(field, element, index);
              }}
              colMobile={formFields[field].colMobile}
              colTabletLandscape={formFields[field].colDesktop}
              colDesktop={formFields[field].colDesktop}
              orderMobile={cmsFormConfiguration[field].order}
              orderDesktop={cmsFormConfiguration[field].order}
              className={field}
            >
              {field === SHIPPING_CONSTANTS.STREET_ADDRESS ||
              field === SHIPPING_CONSTANTS.ADDRESS ? (
                <Autocomplete
                  key={field}
                  label={getLabelValueForFormFields(this.props, field)}
                  value={(formFields[field] as IFormFieldValue).value}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange(field, event);
                  }}
                  error={!(formFields[field] as IFormFieldValue).isValid}
                  errorMessage={
                    (formFields[field] as IFormFieldValue).validationMessage
                  }
                  tabIndex={cmsFormConfiguration[field].order}
                  disabled={
                    cmsFormConfiguration[field].readOnly && sameAsPersonalInfo
                  }
                  getItems={getAddresses}
                  onItemSelect={val =>
                    this.checkForGoogleSuggestionFlag(val as IListItem)
                  }
                  debounceInterval={300}
                  autoCompleteValuesOnly={false}
                  onBlur={() => {
                    this.onBlur(field);
                  }}
                  loadingText={loadingText}
                />
              ) : (
                <FloatLabelInput
                  key={field}
                  id={field}
                  onBlur={() => {
                    this.onBlur(field);
                  }}
                  onFocus={(event: React.FocusEvent<HTMLInputElement>) => {
                    this.onFocus(field, event);
                  }}
                  label={getLabelValueForFormFields(this.props, field)}
                  type={cmsFormConfiguration[field].inputType}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    this.handleChange(field, event);
                  }}
                  onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
                    this.handleKeydown(field, event);
                  }}
                  value={formFields[field].value}
                  error={!formFields[field].isValid}
                  errorMessage={formFields[field].validationMessage}
                  tabIndex={cmsFormConfiguration[field].order}
                  disabled={
                    cmsFormConfiguration[field].readOnly && sameAsPersonalInfo
                  }
                  isMobile={isMobile.phone}
                />
              )}
            </Column>
          )}
        </>
      );
    });
  }

  render(): ReactNode {
    return (
      <FormWrapper className='formWrapper'>
        <Row>{this.createForm()}</Row>
      </FormWrapper>
    );
  }
}
// tslint:disable-next-line:max-file-line-count
export default Form;
