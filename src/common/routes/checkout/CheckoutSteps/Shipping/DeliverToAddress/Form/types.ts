import {
  IShippingAddressState,
  IShippingFormField,
  IUpdateShippingAddress
} from '@src/common/routes/checkout/store/shipping/types';
import { IShippingInfoTranslation } from '@src/common/store/types/translation';
import { IShipping } from '@src/common/store/types/configuration';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
export interface IState {
  id: string;
  isDefault: boolean;
  isFormChanged: boolean;
  formFields: IShippingFormField;
}

export interface IProps {
  shippingConfiguration: IShipping;
  shippingTranslation: IShippingInfoTranslation;
  shippingInfo: IShippingAddressState;
  buttonEnable: boolean;
  loadingText: string;
  personalInfo: IPersonalInfoState;
  updateShippingAddress(data: IUpdateShippingAddress): void;
  setProceedButton(data: boolean): void;
  updateFormChangeField(data: boolean): void;
  updateDummyState(data: IShippingFormField): void;
  fetchShippingFee(): void;
}
