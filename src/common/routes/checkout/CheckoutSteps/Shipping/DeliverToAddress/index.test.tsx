import { RootState } from '@src/common/store/reducers';
import React from 'react';
import ShippingInformationShell from '@checkout/CheckoutSteps/Shipping/index.shell';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import DeliverToAddress, {
  IProps
} from '@checkout/CheckoutSteps/Shipping/DeliverToAddress';
import * as PickUpUtils from '@checkout/CheckoutSteps/Shipping/PickUp/utils';

import Form from './Form';

const mockPickUpUtilPath = '@checkout/CheckoutSteps/Shipping/PickUp/utils';

jest.mock(mockPickUpUtilPath, {
  ...PickUpUtils,
  getCurrentLocation: jest.fn()
});

describe('<DeliverToAddress />', () => {
  const initStateValue: RootState = appState();
  const props: IProps = {
    cartId: null,
    shippingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.shipping,
    shippingTranslation: appState().translation.cart.checkout.shippingInfo,
    buttonEnable: true,
    personalInfo: appState().checkout.personalInfo,
    shippingInfo: appState().checkout.shippingInfo,
    globalCurrency: appState().configuration.cms_configuration.global.currency,
    sendShippingAddress: jest.fn(),
    loadingText: '',
    updateShippingAddress: jest.fn(),
    setProceedButton: jest.fn(),
    updateFormChangeField: jest.fn(),
    updateSelectedDate: jest.fn(),
    fetchDeliveryDate: jest.fn(),
    setDeliveryOptions: jest.fn(),
    sameAsPersonalToggleSwitch: jest.fn(),
    updateDummyState: jest.fn(),
    fetchShippingFee: jest.fn(),
    evaluateCartSummary: jest.fn()
  };

  window.scrollTo = jest.fn();
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <DeliverToAddress {...newProps} />
            <Form {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = {};
    const component = mount(
      <ThemeProvider theme={{}}>
        <ShippingInformationShell {...appShellProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
