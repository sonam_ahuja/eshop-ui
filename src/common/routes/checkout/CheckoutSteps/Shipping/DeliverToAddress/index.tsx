import { Button, Section, Switch } from 'dt-components';
import { IShippingInfoTranslation } from '@common/store/types/translation';
import React, { FunctionComponent } from 'react';
import DeliveryOptions from '@checkout/CheckoutSteps/Shipping/DeliverToAddress/DeliveryOptions';
import Form from '@checkout/CheckoutSteps/Shipping/DeliverToAddress/Form';
import {
  ICurrencyConfiguration,
  IShipping
} from '@common/store/types/configuration';
import { shippingType } from '@checkout/store/shipping/types';
import {
  IShippingAddressState,
  IShippingFormField,
  IUpdateShippingAddress
} from '@src/common/routes/checkout/store/shipping/types';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import EVENT_NAME from '@events/constants/eventName';
import { ICartSummary } from '@src/common/routes/basket/store/types';

import { StyledDeliverToAddress, StyledUseSameAddress } from './styles';

export interface IProps {
  cartId: string | null;
  shippingConfiguration: IShipping;
  shippingTranslation: IShippingInfoTranslation;
  shippingInfo: IShippingAddressState;
  buttonEnable: boolean;
  loadingText: string;
  personalInfo: IPersonalInfoState;
  globalCurrency: ICurrencyConfiguration;
  updateShippingAddress(data: IUpdateShippingAddress): void;
  setProceedButton(data: boolean): void;
  updateFormChangeField(data: boolean): void;
  sendShippingAddress(): void;
  updateDummyState(data: IShippingFormField): void;
  setDeliveryOptions(type: shippingType): void;
  sameAsPersonalToggleSwitch(): void;
  updateSelectedDate(type: string): void;
  fetchDeliveryDate(type: string): void;
  fetchShippingFee(): void;
  evaluateCartSummary(cartSummary: ICartSummary[]): void;
}

const DeliverToAddress: FunctionComponent<IProps> = (props: IProps) => {
  const {
    setDeliveryOptions,
    shippingConfiguration,
    shippingTranslation,
    sameAsPersonalToggleSwitch,
    shippingInfo,
    buttonEnable,
    updateShippingAddress,
    setProceedButton,
    sendShippingAddress,
    personalInfo,
    updateFormChangeField,
    updateDummyState,
    updateSelectedDate,
    fetchDeliveryDate,
    globalCurrency,
    fetchShippingFee,
    evaluateCartSummary,
    loadingText
  } = props;
  const { sameAddressText, proceedToPayment } = props.shippingTranslation;

  const onProceedToPayment = () => {
    sendShippingAddress();
  };

  return (
    <StyledDeliverToAddress>
      <StyledUseSameAddress>
        <div className='textAndSwitchWrap'>
          <Section size='large'>{sameAddressText}</Section>
          <Switch
            disabled={false}
            on={shippingInfo.sameAsPersonalInfo}
            onToggle={sameAsPersonalToggleSwitch}
          />
        </div>
        {/* <Divider dashed /> */}
      </StyledUseSameAddress>
      <Form
        shippingConfiguration={shippingConfiguration}
        shippingTranslation={shippingTranslation}
        loadingText={loadingText}
        shippingInfo={shippingInfo}
        buttonEnable={buttonEnable}
        updateShippingAddress={updateShippingAddress}
        setProceedButton={setProceedButton}
        personalInfo={personalInfo}
        updateFormChangeField={updateFormChangeField}
        updateDummyState={updateDummyState}
        fetchShippingFee={fetchShippingFee}
      />
      <DeliveryOptions
        shippingTranslation={shippingTranslation}
        shippingConfiguration={shippingConfiguration}
        setDeliveryOptions={setDeliveryOptions}
        shippingInfo={shippingInfo}
        updateSelectedDate={updateSelectedDate}
        fetchDeliveryDate={fetchDeliveryDate}
        globalCurrency={globalCurrency}
        evaluateCartSummary={evaluateCartSummary}
      />
      <div className='buttonWrap'>
        <Button
          size='medium'
          loading={shippingInfo.loading}
          onClickHandler={onProceedToPayment}
          disabled={!buttonEnable}
          data-event-id={EVENT_NAME.CHECKOUT.EVENTS.PROCEED_TO_PAYMENT}
          data-event-message={proceedToPayment}
        >
          {proceedToPayment}
        </Button>
      </div>
    </StyledDeliverToAddress>
  );
};

export default DeliverToAddress;
