import { IState } from '@checkout/CheckoutSteps/Shipping/DeliverToAddress/Form/types';

export const shippingFormState = (): IState => {
  return {
    id: '',
    isDefault: true,
    isFormChanged: false,
    formFields: {
      streetAddress: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 12,
        colMobile: 12
      },
      streetNumber: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      flatNumber: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      city: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      postCode: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      unit: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 4,
        colMobile: 6
      },
      deliveryNote: {
        value: '',
        isValid: true,
        validationMessage: '',
        colDesktop: 12,
        colMobile: 12
      }
    }
  };
};

export const ShippingFormGrid = {
  streetAddress: {
    colDesktop: 8,
    colMobile: 12
  },
  streetNumber: {
    colDesktop: 4,
    colMobile: 12
  },
  city: {
    colDesktop: 4,
    colMobile: 12
  },
  postCode: {
    colDesktop: 4,
    colMobile: 12
  },
  unit: {
    colDesktop: 4,
    colMobile: 12
  },
  notes: {
    colDesktop: 12,
    colMobile: 12
  }
};
