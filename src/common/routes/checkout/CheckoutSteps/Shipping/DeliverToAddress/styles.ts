import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledUseSameAddress = styled.div`
  .dt_divider {
    color: ${colors.lightGray};
    margin-bottom: 1.9rem;
  }

  .textAndSwitchWrap {
    display: flex;
    justify-content: space-between;
    color: ${colors.darkGray};
    margin-bottom: 1rem;
    align-items: center;
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .textAndSwitchWrap {
      margin-bottom: 0.78125rem;
    }
    .dt_divider {
      margin-bottom: 2.28125rem;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .dt_divider {
      margin-top: 0.28125rem;
      margin-bottom: 2.28125rem;
    }
    .textAndSwitchWrap {
      margin-bottom: 0.875rem;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    flex-direction: column-reverse;

    .dt_divider {
      margin-top: 0.6rem;
      margin-bottom: 0.6rem;
    }

    .textAndSwitchWrap {
      margin-bottom: 3rem;

      .dt_section {
        font-size: 0.625rem;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.2;
        letter-spacing: normal;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .textAndSwitchWrap {
      margin-bottom: 3.2rem;
    }
  }
`;

// mainWrapper
export const StyledDeliverToAddress = styled.div`
  padding: 0rem 1.25rem;
  background: ${colors.coldGray};
  margin-top: 1.8rem;
  width: 100%;

  .formWrapper {
    padding: 0;

    .streetAddress {
      > span {
        width: 100%;
        > div {
          width: 100%;
          > div {
            width: 100%;
          }
        }
      }
    }
  }

  .buttonWrap {
    margin-top: 2.4rem;
    margin-bottom: 1.25rem;
    .dt_button {
      width: 100%;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 0.9375rem 2.25rem 1.5rem 2.25rem;
    margin-top: 1.125rem;
    .buttonWrap {
      margin-top: 2.8125rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 0.9375rem 3rem 1.5rem 3rem;
    margin-top: 2.5rem;
    .buttonWrap {
      margin-top: 2.6875rem;
      text-align: right;
      .dt_button {
        width: auto;
        height: 2.5rem;
        padding: 0 1.5rem;
        font-size: 0.875rem;
        line-height: 1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 1.085rem 2.25rem 1.25rem 2.25rem;
    margin-top: 1.8rem;
    .buttonWrap {
      margin-top: 3rem;
      .dt_button {
        height: 3rem;
        font-size: 1rem;
        line-height: 1.5rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 1.085rem 5.5rem 1.5rem 5.5rem;
    margin-top: 2.2rem;
    .buttonWrap {
      margin-top: 4.75rem;
    }
  }
`;
