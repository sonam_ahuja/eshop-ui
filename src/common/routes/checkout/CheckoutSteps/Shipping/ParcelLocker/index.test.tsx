import React from 'react';
import { mount, render } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import store from '@common/store';
import translation from '@src/common/store/states/translation';

import ParcelLocker from '.';

jest.mock('@src/common/utils', () => ({
  isMobile: {
    phone: true
  },
  hexToRgbA: jest.fn()
}));

describe('<Order Review Shipping />', () => {
  const state = store.getState().checkout;

  const props = {
    checkoutTranslation: translation().cart.checkout,
    shippingInfo: state.shippingInfo,
    markers: [],
    fetchPickupLocation: jest.fn(),
    setSelectedStore: jest.fn(),
    setOtherPersonPickup: jest.fn(),
    isUserPickedLocation: jest.fn(),
    sendShippingAddress: jest.fn(),

    getStores: jest.fn(),
    onEnterPressOnSearch: jest.fn(),
    fetchShippingFee: jest.fn()
  };

  test('should render properly', () => {
    const component = render(
      <ThemeProvider theme={{}}>
        <ParcelLocker {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    const component = mount<ParcelLocker>(
      <ThemeProvider theme={{}}>
        <ParcelLocker {...copyComponentProps} />
      </ThemeProvider>
    );
    (component.find('ParcelLocker').instance() as ParcelLocker).findOtherStore(
      true
    );
    (component
      .find('ParcelLocker')
      .instance() as ParcelLocker).fetchOtherStore();
  });
});
