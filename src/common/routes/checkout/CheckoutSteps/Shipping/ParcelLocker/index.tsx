// tslint:disable:no-any

import PickUpDetails from '@checkout/CheckoutSteps/Shipping/Common/PickUpDetails';
import React, { Component, ReactNode } from 'react';
import FindPickUpLocation from '@checkout/CheckoutSteps/Shipping/Common/FindPickUpLocation';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import {
  IPickUpLocationPayload,
  ISelectedStoreAddress,
  IShippingAddressState
} from '@checkout/store/shipping/types';
import StoreAddress from '@checkout/CheckoutSteps/Shipping/Common/StoreAddress';
import { isMobile } from '@src/common/utils';

import FindOtherStoreAction from '../Common/FindOtherStoreAction';
import { StyledCompleteMapWrap } from '../styles';

import { StyledParcelLocker } from './styles';

export interface IProps {
  checkoutTranslation: ICheckoutTranslation;
  shippingInfo: IShippingAddressState;
  markers: any[];
  fetchPickupLocation(data: IPickUpLocationPayload): void;
  setSelectedStore(data: ISelectedStoreAddress | null): void;
  setOtherPersonPickup(data: boolean): void;
  isUserPickedLocation(): void;
  sendShippingAddress(): void;
  getStores(data: any): void;
  onEnterPressOnSearch(value: string): void;
  fetchShippingFee(): void;
}

interface IState {
  isOnClickExecuted: boolean;
}

class ParcelLocker extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isOnClickExecuted: !isMobile.phone
    };

    this.findOtherStore = this.findOtherStore.bind(this);
    this.fetchOtherStore = this.fetchOtherStore.bind(this);
  }

  findOtherStore(data: boolean): void {
    this.setState({
      isOnClickExecuted: data
    });
  }

  fetchOtherStore(): void {
    const { fetchPickupLocation, shippingInfo } = this.props;
    if (shippingInfo.selectedStoreAddress) {
      fetchPickupLocation({
        shippingType: shippingInfo.shippingType,
        geoX: shippingInfo.selectedStoreAddress.geoX,
        geoY: shippingInfo.selectedStoreAddress.geoY
      });
    }
  }

  render(): ReactNode {
    const { isPickUpLocationSelected } = this.props.shippingInfo;
    const { shippingInfo, checkoutTranslation } = this.props;
    const { fetchShippingFee, isUserPickedLocation } = this.props;
    const { getStores, onEnterPressOnSearch } = this.props;

    return (
      <StyledParcelLocker>
        {isMobile.phone && !isPickUpLocationSelected ? (
          <>
            <StoreAddress
              isUserPickedLocation={isUserPickedLocation}
              shippingInfo={shippingInfo}
              checkoutTranslation={checkoutTranslation}
              onClickOnSelectedAddress={this.fetchOtherStore}
              onClickOnSelectAddress={fetchShippingFee}
            />

            <FindOtherStoreAction
              onClick={() => this.findOtherStore(true)}
              text={checkoutTranslation.shippingInfo.findOtherStore}
            />
          </>
        ) : (
          isMobile.phone && (
            <PickUpDetails
              onClickOnSelectedAddress={() => {
                this.fetchOtherStore();
                this.findOtherStore(true);
              }}
              {...this.props}
            />
          )
        )}

        {this.state.isOnClickExecuted && (
          <StyledCompleteMapWrap>
            {!isPickUpLocationSelected ? (
              <FindPickUpLocation
                closeModal={() => this.findOtherStore(false)}
                onClickOnSelectAddress={() => {
                  if (isMobile.phone) {
                    this.findOtherStore(false);
                  }
                  fetchShippingFee();
                }}
                onEnterPressOnSearch={onEnterPressOnSearch}
                onSelectItem={getStores}
                {...this.props}
              />
            ) : (
              !isMobile.phone && (
                <PickUpDetails
                  onClickOnSelectedAddress={() => {
                    this.fetchOtherStore();
                  }}
                  {...this.props}
                />
              )
            )}
          </StyledCompleteMapWrap>
        )}
      </StyledParcelLocker>
    );
  }
}

export default ParcelLocker;
