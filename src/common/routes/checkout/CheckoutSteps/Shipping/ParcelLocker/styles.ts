import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledParcelLocker = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  /* class exists in PickUpDetails */
  /* .personalInfoWrap {
    .header {
      .mainHeading {
        font-size: 0.625rem;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.2;
        letter-spacing: normal;
      }
    }
  } */
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    height: 100vh;
  }
`;
