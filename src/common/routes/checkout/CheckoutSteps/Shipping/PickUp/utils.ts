import { isBrowser } from '@src/common/utils';

// tslint:disable-next-line:no-any
export function getCurrentLocation(success: any): void {
  const options = {
    enableHighAccuracy: true,
    timeout: 50000,
    maximumAge: 1000
  };
  if (isBrowser) {
    navigator.geolocation.getCurrentPosition(success, error, options);
  }
}

// tslint:disable-next-line:no-any
export function error(err: any): void {
  // tslint:disable-next-line:no-console
  console.warn(`ERROR(${err.code}): ${err.message}`);
}
