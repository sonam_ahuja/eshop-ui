import React from 'react';
import { render } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import store from '@common/store';
import translation from '@src/common/store/states/translation';

import PickUpAtPoint from '.';
describe('<PickUpAtPoint />', () => {
  const state = store.getState().checkout;

  const props = {
    checkoutTranslation: translation().cart.checkout,
    shippingInfo: state.shippingInfo,
    markers: [],
    fetchPickupLocation: jest.fn(),
    setSelectedStore: jest.fn(),
    setOtherPersonPickup: jest.fn(),
    isUserPickedLocation: jest.fn(),
    sendShippingAddress: jest.fn(),

    getStores: jest.fn(),
    onEnterPressOnSearch: jest.fn(),
    fetchShippingFee: jest.fn()
  };

  test('should render properly', () => {
    const component = render(
      <ThemeProvider theme={{}}>
        <PickUpAtPoint {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
