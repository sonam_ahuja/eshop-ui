import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPickUpAtPoints = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    height: 100vh;
  }
`;
