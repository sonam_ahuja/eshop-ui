import React, { Component, ReactNode } from 'react';
import {
  ILatLong,
  ISelectedStoreAddress,
  IShippingAddressState
} from '@checkout/store/shipping/types';
import FindPickUpLocation from '@checkout/CheckoutSteps/Shipping/Common/FindPickUpLocation';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import PickUpDetails from '@checkout/CheckoutSteps/Shipping/Common/PickUpDetails';
import StoreAddress from '@checkout/CheckoutSteps/Shipping/Common/StoreAddress';
import { isMobile } from '@src/common/utils';
import { SHIPPING_TYPE } from '@checkout/store/enums';

import FindOtherStoreAction from '../Common/FindOtherStoreAction';
import { StyledCompleteMapWrap } from '../styles';

import { StyledPickUpAtStore } from './styles';

export interface IProps {
  checkoutTranslation: ICheckoutTranslation;
  shippingInfo: IShippingAddressState;
  // tslint:disable-next-line:no-any
  markers: any[];
  fetchPickupLocation(data: ILatLong): void;
  setSelectedStore(data: ISelectedStoreAddress | null): void;
  setOtherPersonPickup(data: boolean): void;
  isUserPickedLocation(): void;
  sendShippingAddress(): void;
  // tslint:disable-next-line:no-any
  getStores(data: any): void;
  onEnterPressOnSearch(value: string): void;
  fetchShippingFee(): void;
}

interface IState {
  isOnClickExecuted: boolean;
  isModalOpened: boolean;
}

class PickUpAtStore extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isOnClickExecuted: !isMobile.phone,
      isModalOpened: false
    };

    this.findOtherStore = this.findOtherStore.bind(this);
    this.fetchOtherStore = this.fetchOtherStore.bind(this);
  }

  findOtherStore(data: boolean): void {
    const { isPickUpLocationSelected } = this.props.shippingInfo;

    this.setState({
      isOnClickExecuted: data,
      isModalOpened: isPickUpLocationSelected ? data : this.state.isModalOpened
    });
  }

  fetchOtherStore(): void {
    const { fetchPickupLocation, shippingInfo } = this.props;
    if (shippingInfo.selectedStoreAddress) {
      fetchPickupLocation({
        geoX: shippingInfo.selectedStoreAddress.geoX,
        geoY: shippingInfo.selectedStoreAddress.geoY
      });
    }
  }

  isSelectedAddressIsStore(): boolean {
    const {
      selectedStoreAddress,
      savedDeliveryMethod
    } = this.props.shippingInfo;

    if (
      selectedStoreAddress &&
      savedDeliveryMethod === SHIPPING_TYPE.PICK_UP_STORE
    ) {
      return true;
    }

    return false;
  }

  render(): ReactNode {
    const { isPickUpLocationSelected } = this.props.shippingInfo;
    const { shippingInfo, checkoutTranslation } = this.props;
    const { fetchShippingFee, isUserPickedLocation } = this.props;
    const { getStores, onEnterPressOnSearch } = this.props;

    const { isOnClickExecuted } = this.state;

    return (
      <StyledPickUpAtStore>
        {isMobile.phone && !isPickUpLocationSelected ? (
          <>
            <StoreAddress
              isUserPickedLocation={isUserPickedLocation}
              shippingInfo={shippingInfo}
              checkoutTranslation={checkoutTranslation}
              onClickOnSelectedAddress={this.fetchOtherStore}
              onClickOnSelectAddress={fetchShippingFee}
              boxShadow={false}
            />
            <FindOtherStoreAction
              onClick={() => this.findOtherStore(true)}
              text={checkoutTranslation.shippingInfo.findOtherStore}
            />
          </>
        ) : (
          isMobile.phone && (
            <PickUpDetails
              onClickOnSelectedAddress={() => {
                this.fetchOtherStore();
                this.findOtherStore(true);
              }}
              {...this.props}
            />
          )
        )}

        {isOnClickExecuted && (
          <StyledCompleteMapWrap>
            {!isPickUpLocationSelected ? (
              <FindPickUpLocation
                closeModal={() => this.findOtherStore(false)}
                onClickOnSelectAddress={() => {
                  if (isMobile.phone) {
                    this.findOtherStore(false);
                  }
                  fetchShippingFee();
                }}
                onEnterPressOnSearch={onEnterPressOnSearch}
                onSelectItem={getStores}
                {...this.props}
              />
            ) : (
              !isMobile.phone && (
                <PickUpDetails
                  onClickOnSelectedAddress={() => {
                    this.fetchOtherStore();
                  }}
                  {...this.props}
                />
              )
            )}
          </StyledCompleteMapWrap>
        )}
      </StyledPickUpAtStore>
    );
  }
}

export default PickUpAtStore;
