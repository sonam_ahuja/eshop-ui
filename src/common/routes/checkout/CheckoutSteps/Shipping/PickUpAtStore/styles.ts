import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPickUpAtStore = styled.div`
  position: relative;
  height: 100%;
  flex-grow: 1;
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    height: 100vh;
  }
`;
