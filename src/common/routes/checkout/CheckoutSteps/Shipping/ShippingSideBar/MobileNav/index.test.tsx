import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import ShippingModeMobileNav, { getSVG, IProps } from '.';

describe('<ShippingModeMobileNav/>', () => {
  const props: IProps = {
    shippingTranslation: appState().translation.cart.checkout.shippingInfo,
    shippingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.shipping,
    shippingInfo: appState().checkout.shippingInfo,
    setShippingMode: jest.fn()
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <ThemeProvider theme={{}}>
        <ShippingModeMobileNav {...newProps} />
      </ThemeProvider>
    );
  };

  test('should render properly, when delivery option is standard', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when delivery option is exact day', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.EXACT_DAY;
    newProps.shippingConfiguration.shippingType.deliverToAddress.show = false;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when delivery option is same day', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.SAME_DAY;
    newProps.shippingConfiguration.shippingType.pickUpAtStore.show = false;

    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when delivery option is pick up point', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.PICK_UP_POINT;
    newProps.shippingConfiguration.shippingType.parcelLocker.show = false;

    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when delivery option is pick up store', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.PICK_UP_STORE;
    newProps.shippingConfiguration.shippingType.pickUpPoints.show = false;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when delivery option is parcel locker', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.PARCEL_LOCKER;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('getSVG :: ', () => {
    const exactDay = getSVG(SHIPPING_TYPE.EXACT_DAY);
    expect(getSVG(SHIPPING_TYPE.EXACT_DAY)).toEqual(exactDay);
    const sameDay = getSVG(SHIPPING_TYPE.SAME_DAY);
    expect(getSVG(SHIPPING_TYPE.EXACT_DAY)).toEqual(sameDay);
    const standerd = getSVG(SHIPPING_TYPE.STANDARD);
    expect(getSVG(SHIPPING_TYPE.EXACT_DAY)).toEqual(standerd);
  });

  test('handle trigger', () => {
    const initStateValue = appState();
    const mockStore = configureStore();
    const store = mockStore(initStateValue);
    const component = mount<ShippingModeMobileNav>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ShippingModeMobileNav {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).componentWillReceiveProps(props);
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).deliveryOptions(
      SHIPPING_TYPE.EXACT_DAY
    );
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).deliveryOptions(
      SHIPPING_TYPE.PARCEL_LOCKER
    );
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).deliveryOptions(
      SHIPPING_TYPE.PICK_UP_POINT
    );
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).deliveryOptions(
      SHIPPING_TYPE.PICK_UP_STORE
    );
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).deliveryOptions(
      SHIPPING_TYPE.SAME_DAY
    );
    (component
      .find('MobileNav')
      .instance() as ShippingModeMobileNav).deliveryOptions(
      SHIPPING_TYPE.STANDARD
    );
  });
});
