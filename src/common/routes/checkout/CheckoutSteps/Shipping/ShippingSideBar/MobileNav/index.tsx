import React, { Component, ReactNode } from 'react';
import { Select } from 'dt-components';
import { IShippingInfoTranslation } from '@src/common/store/types/translation';
import { IShipping } from '@src/common/store/types/configuration';
import {
  IFee,
  IListItem,
  IShippingAddressState,
  IShippingFee,
  shippingType
} from '@checkout/store/shipping/types';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import SvgDeliveryAddress from '@src/common/components/Svg/delivery-address';
import SelectWithIcon from '@src/common/components/Select/SelectWithIcon';
import SvgParcelLocker from '@src/common/components/Svg/parcel-locker';
import SvgPickUpPoint from '@src/common/components/Svg/pick-up-point';
import SvgPickUpStore from '@src/common/components/Svg/pick-up-store';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledMobileNav } from './styles';

export interface IProps {
  shippingTranslation: IShippingInfoTranslation;
  shippingConfiguration: IShipping;
  shippingInfo: IShippingAddressState;
  setShippingMode: setShippingModeType;
}

type setShippingModeType = (type: shippingType) => void;

export const getSVG = (selectedId: string) => {
  switch (selectedId) {
    case SHIPPING_TYPE.EXACT_DAY:
      return <SvgDeliveryAddress />;
    case SHIPPING_TYPE.PARCEL_LOCKER:
      return <SvgParcelLocker />;
    case SHIPPING_TYPE.PICK_UP_POINT:
      return <SvgPickUpPoint />;
    case SHIPPING_TYPE.PICK_UP_STORE:
      return <SvgPickUpStore />;
    case SHIPPING_TYPE.SAME_DAY:
      return <SvgDeliveryAddress />;
    case SHIPPING_TYPE.STANDARD: // deliver to address
      return <SvgDeliveryAddress />;
    default:
      return <SvgDeliveryAddress />; // deliver to address
  }
};

export interface IState {
  selectedDeliveryMethod: IListItem;
}
export class MobileNav extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      selectedDeliveryMethod: { id: '', title: '', value: '' }
    };

    this.getDeliverToAddressDeliveryMethod = this.getDeliverToAddressDeliveryMethod.bind(
      this
    );
    this.shippingMethod = this.shippingMethod.bind(this);
    this.deliveryOptions = this.deliveryOptions.bind(this);
    this.getSelectedDeliveryMethod = this.getSelectedDeliveryMethod.bind(this);
  }

  componentDidMount(): void {
    const { shippingInfo, shippingTranslation } = this.props;

    this.setState({
      selectedDeliveryMethod: this.getSelectedDeliveryMethod(
        shippingTranslation,
        shippingInfo
      )
    });
  }

  componentWillReceiveProps(nextProps: IProps): void {
    const { shippingInfo, shippingTranslation } = nextProps;
    if (
      this.props.shippingInfo.deliveryOptions !==
      nextProps.shippingInfo.deliveryOptions
    ) {
      this.setState({
        selectedDeliveryMethod: this.getSelectedDeliveryMethod(
          shippingTranslation,
          shippingInfo
        )
      });
    }
  }

  deliveryOptions(shippingtype: shippingType): void {
    this.props.setShippingMode(shippingtype);
  }

  getSelectedDeliveryMethod(
    shippingTranslation: IShippingInfoTranslation,
    shippingInfo: IShippingAddressState
  ): IListItem {
    const { deliverToAddress } = shippingTranslation.deliveryType;

    let obj: IListItem = {
      id: shippingInfo.shippingType,
      title: '',
      value: ''
    };

    if (
      shippingInfo.shippingType === SHIPPING_TYPE.STANDARD ||
      shippingInfo.shippingType === SHIPPING_TYPE.EXACT_DAY ||
      shippingInfo.shippingType === SHIPPING_TYPE.SAME_DAY
    ) {
      obj = {
        id: 'deliverAddress',
        title: deliverToAddress,
        value: deliverToAddress
      };

      return obj;
    }
    if (
      shippingInfo.shippingFee &&
      shippingInfo.shippingFee[shippingInfo.shippingType]
    ) {
      obj = {
        id: shippingInfo.shippingType,
        title: shippingTranslation.deliveryType[shippingInfo.shippingType],
        value: shippingInfo.shippingType
      };

      return obj;
    }

    return obj;
  }

  getDeliverToAddressDeliveryMethod(
    shippingFee: IShippingFee,
    deliveryMethod: SHIPPING_TYPE,
    firstEle: string[]
  ): SHIPPING_TYPE {
    firstEle.push(deliveryMethod);

    if ((shippingFee[deliveryMethod] as IFee).isDefault) {
      firstEle = [];
      firstEle.push(deliveryMethod);

      return deliveryMethod;
    }

    return firstEle[0] as SHIPPING_TYPE;
  }

  shippingMethod(props: IProps): IListItem[] {
    const shippingSideNav: IListItem[] = [];
    let shippingFlag = false;
    const { shippingInfo, shippingTranslation } = props;
    const firstEle: string[] = [];
    Object.keys(shippingInfo.shippingFee).forEach(deliveryMethod => {
      let obj: IListItem;
      if (
        deliveryMethod === 'standard' ||
        deliveryMethod === 'withinTime' ||
        deliveryMethod === 'pickADate'
      ) {
        if (!shippingFlag) {
          const delMethod = this.getDeliverToAddressDeliveryMethod(
            shippingInfo.shippingFee,
            deliveryMethod as SHIPPING_TYPE,
            firstEle
          );
          obj = {
            id: 'deliverAddress',
            title: shippingTranslation.deliveryType.deliverToAddress,
            value: delMethod
          };
          shippingSideNav.push(obj);
        }
        shippingFlag = true;
      } else {
        obj = {
          id: deliveryMethod,
          title: shippingTranslation.deliveryType[deliveryMethod],
          value: deliveryMethod
        };
        shippingSideNav.push(obj);
      }
    });

    return shippingSideNav;
  }

  render(): ReactNode {
    return (
      <StyledMobileNav>
        <Select
          useNativeDropdown={true}
          onItemSelect={item => {
            sendDropdownClickEvent(item.title);
            this.deliveryOptions(item.value);
          }}
          SelectedItemComponent={selectedItemProps => (
            <SelectWithIcon
              {...selectedItemProps}
              svgNode={getSVG(
                selectedItemProps.selectedItem
                  ? (selectedItemProps.selectedItem.id as string)
                  : ' 0'
              )}
              description={
                selectedItemProps.selectedItem
                  ? selectedItemProps.selectedItem.title
                  : ''
              }
            />
          )}
          items={this.shippingMethod(this.props)}
          selectedItem={this.state.selectedDeliveryMethod}
        />
      </StyledMobileNav>
    );
  }
}

export default MobileNav;
