import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledSelectWithIcon } from '@src/common/components/Select/SelectWithIcon/styles';

export const StyledMobileNav = styled.div`
  background: ${colors.white};
  padding: 0 1.25rem;
  /* border-bottom: 1px solid ${colors.fogGray}; */

  .description {
    color: ${colors.darkGray};
  }

  ${StyledSelectWithIcon} {
    .caret {
      font-size: 0.6875rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 0 2.25rem;
  }
`;
