import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { SHIPPING_TYPE } from '@checkout/store/enums';

import ShippingMode, { IProps as IShippingModeProps } from '.';

describe('<ShippingMode  />', () => {
  const props: IShippingModeProps = {
    shippingInfo: appState().checkout.shippingInfo,
    setShippingMode: jest.fn(),
    shippingTranslation: appState().translation.cart.checkout.shippingInfo
  };

  const componentWrapper = (newProps: IShippingModeProps) => {
    return mount(
      <ThemeProvider theme={{}}>
        <ShippingMode {...newProps} />
      </ThemeProvider>
    );
  };

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is exact day', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.EXACT_DAY;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is same day', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.SAME_DAY;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is parcel locker', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.PARCEL_LOCKER;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is pick point', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.PICK_UP_POINT;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is pick up store', () => {
    const newProps = { ...props };
    newProps.shippingInfo.shippingType = SHIPPING_TYPE.PICK_UP_STORE;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
