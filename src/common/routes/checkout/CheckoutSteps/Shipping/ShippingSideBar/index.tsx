import SvgDeliveryAddress from '@src/common/components/Svg/delivery-address';
import React, { Component, MouseEvent, ReactNode } from 'react';
import { IShippingInfoTranslation } from '@src/common/store/types/translation';
import { logError } from '@src/common/utils';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import {
  IFee,
  IShippingAddressState,
  IShippingFee,
  shippingType
} from '@checkout/store/shipping/types';
import SideNavTabs from '@checkout/Common/SideNavTabs';
import SvgPickUpStore from '@src/common/components/Svg/pick-up-store';
import SvgParcelLocker from '@src/common/components/Svg/parcel-locker';
import SvgPickUpPoint from '@src/common/components/Svg/pick-up-point';
import { sendCTAClicks } from '@events/index';

import { StyledShippingSideBar } from './styles';

export interface IProps {
  shippingTranslation: IShippingInfoTranslation;
  shippingInfo: IShippingAddressState;
  setShippingMode: setShippingModeType;
}

type setShippingModeType = (type: shippingType) => void;

export class ShippingSideBar extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.getDeliverToAddressDeliveryMethod = this.getDeliverToAddressDeliveryMethod.bind(
      this
    );
    this.shippingMethod = this.shippingMethod.bind(this);
    this.sideNavElement = this.sideNavElement.bind(this);
    this.deliveryOptions = this.deliveryOptions.bind(this);
    this.onKeyDownDeliveryOptions = this.onKeyDownDeliveryOptions.bind(this);
    this.getSvgElementByShippingType = this.getSvgElementByShippingType.bind(
      this
    );

    this.checkActiveCondition = this.checkActiveCondition.bind(this);
  }

  deliveryOptions(
    event: MouseEvent<HTMLAnchorElement>,
    shippingtype: shippingType,
    setShippingMode: setShippingModeType
  ): void {
    if (event) {
      setShippingMode(shippingtype);
      logError(event);
    }
  }

  getSvgElementByShippingType(shippingtype: shippingType): ReactNode {
    switch (shippingtype) {
      case SHIPPING_TYPE.EXACT_DAY:
        return <SvgDeliveryAddress />;
      case SHIPPING_TYPE.PARCEL_LOCKER:
        return <SvgParcelLocker />;
      case SHIPPING_TYPE.PICK_UP_POINT:
        return <SvgPickUpPoint />;
      case SHIPPING_TYPE.PICK_UP_STORE:
        return <SvgPickUpStore />;
      case SHIPPING_TYPE.SAME_DAY:
        return <SvgDeliveryAddress />;
      case SHIPPING_TYPE.STANDARD: // deliver to address
        return <SvgDeliveryAddress />;
      default:
        return null;
    }
  }

  onKeyDownDeliveryOptions(
    event: React.KeyboardEvent<Element>,
    shippingtype: shippingType,
    setShippingMode: setShippingModeType
  ): void {
    if (event.keyCode === 13) {
      setShippingMode(shippingtype);
      logError(event);
    }
  }

  checkActiveCondition(deliveryMethod: SHIPPING_TYPE[]): boolean {
    const { shippingType: shipmentType } = this.props.shippingInfo;

    if (
      deliveryMethod[0] === 'standard' ||
      deliveryMethod[0] === 'withinTime' ||
      deliveryMethod[0] === 'pickADate'
    ) {
      if (
        shipmentType === 'standard' ||
        shipmentType === 'withinTime' ||
        shipmentType === 'pickADate'
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return deliveryMethod[0] === shipmentType;
    }
  }

  sideNavElement(
    deliveryMethod: SHIPPING_TYPE[],
    setShippingMode: setShippingModeType,
    translatedValue: string,
    index: number
  ): ReactNode {
    return (
      <SideNavTabs
        tabIndex={index}
        onClick={(event: MouseEvent<HTMLAnchorElement>) => {
          sendCTAClicks(translatedValue, window.location.href);
          this.deliveryOptions(event, deliveryMethod[0], setShippingMode);
        }}
        onKeyDown={(event: React.KeyboardEvent<Element>) => {
          sendCTAClicks(translatedValue, window.location.href);
          this.onKeyDownDeliveryOptions(
            event,
            deliveryMethod[0],
            setShippingMode
          );
        }}
        isActive={this.checkActiveCondition(deliveryMethod)}
        translatedValue={translatedValue}
      />
    );
  }

  getDeliverToAddressDeliveryMethod(
    shippingFee: IShippingFee,
    deliveryMethod: SHIPPING_TYPE,
    firstEle: string[]
  ): SHIPPING_TYPE[] {
    firstEle.push(deliveryMethod);

    if ((shippingFee[deliveryMethod] as IFee).isDefault) {
      firstEle = [];
      firstEle.push(deliveryMethod);

      return [deliveryMethod];
    }

    return firstEle as SHIPPING_TYPE[];
  }

  shippingMethod(props: IProps): ReactNode[] {
    const shippingSideNav: ReactNode[] = [];
    let shippingFlag = false;
    const { setShippingMode, shippingInfo, shippingTranslation } = props;
    const firstEle: string[] = [];
    Object.keys(shippingInfo.shippingFee).forEach((deliveryMethod, index) => {
      if (
        deliveryMethod === 'standard' ||
        deliveryMethod === 'withinTime' ||
        deliveryMethod === 'pickADate'
      ) {
        if (!shippingFlag) {
          shippingSideNav.push(
            this.sideNavElement(
              this.getDeliverToAddressDeliveryMethod(
                shippingInfo.shippingFee,
                deliveryMethod as SHIPPING_TYPE,
                firstEle
              ),
              setShippingMode,
              shippingTranslation.deliveryType.deliverToAddress,
              index
            )
          );
        }
        shippingFlag = true;
      } else {
        shippingSideNav.push(
          this.sideNavElement(
            [deliveryMethod as SHIPPING_TYPE],
            setShippingMode,
            shippingTranslation.deliveryType[deliveryMethod],
            index
          )
        );
      }
    });

    return shippingSideNav;
  }

  render(): ReactNode {
    return (
      <StyledShippingSideBar>
        <div className='iconWrap'>
          {this.getSvgElementByShippingType(
            this.props.shippingInfo.shippingType
          )}
        </div>
        {this.shippingMethod(this.props)}
      </StyledShippingSideBar>
    );
  }
}

export default ShippingSideBar;
