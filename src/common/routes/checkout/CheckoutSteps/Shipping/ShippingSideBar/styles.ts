import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledShippingSideBar = styled.div`
  .iconWrap {
    font-size: 6.5rem;
    line-height: 0;
    text-align: center;
    margin-top: 0;
    margin-bottom: 1.5rem;
    svg {
      height: 1em;
      width: 1em;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .iconWrap {
      margin-top: 2.7rem;
      margin-bottom: 0.6rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .iconWrap {
      font-size: 12.75rem;
      margin-top: 0;
      text-align: left;
    }
  }
`;
