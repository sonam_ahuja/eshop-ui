import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';

const ShippingShellWrapper = styled.div`
  padding: 2rem 1rem;
  float: left;
  width: 100%;
  p {
    height: 0.5rem;
    background-color: #dadada;
    border-radius: 2rem;
    margin-top: 2.75rem;
    width: 50%;
    display: inline-block;
  }
  p:nth-child(2) {
    float: right;
    border-radius: 2rem;
    width: 2rem;
    height: 0.6rem;
    border-radius: 0.71rem;
  }

  hr {
    margin-top: 1.025rem;
    margin-bottom: 1.825rem;
    border-color: #dadada;
    border-style: dotted;
  }
  h3 {
    height: 0.65rem;
    margin-bottom: 2.05rem;
    border-radius: 2rem;
    background-color: #dadada;
    width: 70%;
  }
  h4 {
    height: 0.65rem;
    margin-bottom: 2.55rem;
    background-color: #dadada;
    border-radius: 2rem;
    width: 30%;
  }
  h1 {
    height: 0.65rem;
    margin-bottom: 1.6rem;
    background-color: #dadada;
    border-radius: 2rem;
    width: 26%;
  }
  button {
    width: 8.05rem;
    height: 2rem;
    border: none;
    margin-top: 2.15rem;
    background-color: #eee;
    float: right;
  }
`;

const DeliveryOptions = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0 auto;
  div {
    width: 33%;
    padding: 1em;
    border: 0.5px solid #eee;
    margin: 0.1em;
    height: 6.8rem;
    border-radius: 0.4rem;
    position: relative;
    background-color: #eee;
    p {
      height: 0.5rem;
      margin-top: 0;
      padding-top: 0.5rem;
      width: 40%;
      display: block;
    }
    p.para {
      height: 0.65rem;
      width: 70%;
      margin-top: 0.05rem;
      float: left;
    }
    p.dimension {
      position: absolute;
      bottom: 0.5rem;
      height: 0.6rem;
      width: 2.05rem;
      background-color: #dadada;
    }
  }
`;
const ShippingAppShell: FunctionComponent = () => {
  return (
    <StyledShell>
      <ShippingShellWrapper>
        <p />
        <p />
        <hr />
        <h3 />
        <h4 />
        <h1 />
        <DeliveryOptions>
          <div>
            <p />
            <p className='para' />
            <p className='dimension' />
          </div>

          <div>
            <p />
            <p className='para' />
            <p className='dimension' />
          </div>

          <div>
            <p />
            <p className='para' />
            <p className='dimension' />
          </div>
        </DeliveryOptions>
        <button />
      </ShippingShellWrapper>
    </StyledShell>
  );
};

export default ShippingAppShell;
