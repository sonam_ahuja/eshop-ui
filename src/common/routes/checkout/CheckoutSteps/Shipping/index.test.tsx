import React from 'react';
import { mount } from 'enzyme';
import ShippingInformationShell from '@checkout/CheckoutSteps/Shipping/index.shell';
import { ThemeProvider } from 'dt-components';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@src/common/store/reducers';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import {
  IFormFieldValue,
  IShippingAddressAndNote,
  IShippingFormField,
  IUpdateShippingAddress,
  shippingType
} from '@checkout/store/shipping/types';
import {
  IComponentProps,
  mapDispatchToProps,
  mapStateToProps,
  ShippingMap
} from '@checkout/CheckoutSteps/Shipping';
import * as PickUpUtils from '@checkout/CheckoutSteps/Shipping/PickUp/utils';

const mockPickUpUtilPath = '@checkout/CheckoutSteps/Shipping/PickUp/utils';

jest.mock(mockPickUpUtilPath, {
  ...PickUpUtils,
  getCurrentLocation: jest.fn()
});

describe('<ShippingMap />', () => {
  const initStateValue: RootState = appState();
  const props: IComponentProps = {
    cartId: null,
    isUserPickedLocation: jest.fn(),
    setShippingMode: jest.fn(),
    checkout: appState().checkout,
    shippingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.shipping,
    shippingTranslation: appState().translation.cart.checkout.shippingInfo,
    checkoutTranslation: appState().translation.cart.checkout,
    buttonEnable: true,
    mainAppShell: false,
    personalInfo: appState().checkout.personalInfo,
    shippingInfo: appState().checkout.shippingInfo,
    globalCurrency: appState().configuration.cms_configuration.global.currency,
    sendShippingAddress: jest.fn(),
    updateShippingAddress: jest.fn(),
    setProceedButton: jest.fn(),
    updateFormChangeField: jest.fn(),
    updateSelectedDate: jest.fn(),
    fetchDeliveryDate: jest.fn(),
    updateInputField: jest.fn(),
    setDeliveryOptions: jest.fn(),
    setPaymentNavigationButton: jest.fn(),
    editPaymentStepClick: jest.fn(),
    sameAsPersonalToggleSwitch: jest.fn(),
    updateNewAddressShipping: jest.fn(),
    updateDummyState: jest.fn(),
    fetchShippingFee: jest.fn(),
    fetchPickupLocation: jest.fn(),
    setSelectedStore: jest.fn(),
    setOtherPersonPickup: jest.fn(),
    evaluateCartSummary: jest.fn(),
    configuration: appState().configuration,
    translation: appState().translation,
    fetchCheckoutAddressLoading: jest.fn()
  };

  window.scrollTo = jest.fn();
  const mockStore = configureStore();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: IComponentProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ShippingMap {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = {};
    const component = mount(
      <ThemeProvider theme={{}}>
        <ShippingInformationShell {...appShellProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly properly with DEFAULT shipping type', () => {
    const newProps = { ...props };
    newProps.checkout.shippingInfo.shippingType = 'DEFAULT' as shippingType;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    const component = mount<ShippingMap>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ShippingMap {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('ShippingMap')
      .instance() as ShippingMap).componentWillUnmount();
    (component.find('ShippingMap').instance() as ShippingMap).getStores({
      geoX: '74.22222',
      geoY: '24.2323'
    });
    (component
      .find('ShippingMap')
      .instance() as ShippingMap).onEnterPressOnSearch('string');
    (component
      .find('ShippingMap')
      .instance() as ShippingMap).onEnterPressOnSearch('string');
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const setAddress: IShippingAddressAndNote = {
      streetNumber: '122',
      address: 'address',
      deliveryNote: 'Delivery'
    };
    const data: IUpdateShippingAddress = {
      key: 'firstName',
      value: 'wq',
      isValid: false,
      validationMessage: ''
    };
    const formField: IFormFieldValue = {
      value: 'wq',
      isValid: false,
      validationMessage: '',
      colDesktop: 12,
      colMobile: 12
    };
    const dumpState: IShippingFormField = {
      streetNumber: formField,
      streetAddress: formField,
      city: formField,
      postCode: formField,
      flatNumber: formField,
      deliveryNote: formField,
      unit: formField
    };

    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.sendShippingAddress();
    mapDispatch.updateInputField({ key: 'key', value: 'value' });
    mapDispatch.setPaymentNavigationButton({ key: 'key', value: 'value' });
    mapDispatch.setDeliveryOptions(SHIPPING_TYPE.STANDARD);
    mapDispatch.updateNewAddressShipping(setAddress);
    mapDispatch.sameAsPersonalToggleSwitch();
    mapDispatch.setProceedButton(true);
    mapDispatch.updateFormChangeField(true);
    mapDispatch.updateDummyState(dumpState);
    mapDispatch.updateShippingAddress(data);
    mapDispatch.fetchShippingFee();
    mapDispatch.fetchPickupLocation({
      geoX: '74.34521',
      geoY: '42.3444'
    });
    mapDispatch.setSelectedStore({
      streetNumber: 'streetNumber',
      address: 'address',
      flatNumber: 'flatNumber',
      postCode: 'postCode',
      city: 'city',
      state: 'state',
      country: 'country',
      distance: 'distance',
      distanceUnit: 'distanceUnit',
      name: 'name',
      geoX: '78.3421',
      geoY: '42.344421'
    });
    mapDispatch.setOtherPersonPickup(true);
    mapDispatch.setShippingMode(SHIPPING_TYPE.EXACT_DAY);
    mapDispatch.isUserPickedLocation();
    mapDispatch.updateSelectedDate('20-12-2019');
    mapDispatch.fetchDeliveryDate('20-12-2019');
    mapDispatch.editPaymentStepClick();
  });
});
