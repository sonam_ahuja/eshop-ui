// tslint:disable:max-file-line-count
import MobileNav from '@checkout/CheckoutSteps/Shipping/ShippingSideBar/MobileNav';
import { isMobile, logError } from '@src/common/utils';
import { connect } from 'react-redux';
import shippingActions from '@checkout/store/shipping/actions';
import {
  IConfigurationState,
  ICurrencyConfiguration,
  IShipping
} from '@common/store/types/configuration';
import {
  ICheckoutTranslation,
  IShippingInfoTranslation,
  ITranslationState
} from '@common/store/types/translation';
import React, { Component, ReactNode } from 'react';
import { RootState } from '@common/store/reducers';
import { Dispatch } from 'redux';
import {
  ILatLong,
  ISelectedStoreAddress,
  IShippingAddressAndNote,
  IShippingAddressState,
  IShippingFormField,
  shippingType
} from '@checkout/store/shipping/types';
import { IState as ICheckoutState } from '@checkout/store/types';
import {
  IInputKeyValue,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';
import { IUpdateShippingAddress } from '@src/common/routes/checkout/store/shipping/types';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import { INortheast } from '@src/common/types/common/googleMap';
import { getLocationOfAddress } from '@src/common/store/services/googleMap';
import StepNavigation from '@checkout/CheckoutSteps/StepNavigation';
import paymentActions from '@checkout/store/payment/actions';
import SideNavigation from '@checkout/SideNavigation';
import { sendProceedEvent } from '@events/checkout/index';
import { CHECKOUT_VIEW, PAGE_VIEW } from '@events/constants/eventName';
import { pageViewEvent } from '@events/index';
import checkoutActions from '@checkout/store/actions';
import { ICartSummary } from '@src/common/routes/basket/store/types';
import Footer from '@src/common/components/FooterWithTermsAndConditions';
import Loadable from 'react-loadable';

import DeliverToAddress from './DeliverToAddress';
import PickUpAtPoints from './PickUpAtPoints';
import PickUpAtStore from './PickUpAtStore';
import ParcelLocker from './ParcelLocker';
import ShippingSideBar from './ShippingSideBar';
import { StyledShipping } from './styles';

const CheckoutAppShellComponent = Loadable({
  loading: () => null,
  loader: () => import('@common/routes/checkout/CheckoutSteps/index.shell')
});

export interface IComponentStateProps {
  cartId: string | null;
  checkout: ICheckoutState;
  shippingConfiguration: IShipping;
  globalCurrency: ICurrencyConfiguration;
  shippingTranslation: IShippingInfoTranslation;
  buttonEnable: boolean;
  personalInfo: IPersonalInfoState;
  shippingInfo: IShippingAddressState;
  checkoutTranslation: ICheckoutTranslation;
  configuration: IConfigurationState;
  translation: ITranslationState;
  mainAppShell: boolean;
}

export interface ICompDispatchToProps {
  sendShippingAddress(): void;
  updateShippingAddress(data: IUpdateShippingAddress): void;
  setProceedButton(data: boolean): void;
  updateFormChangeField(data: boolean): void;
  updateInputField(data: IInputKeyValue): void;
  setDeliveryOptions(type: shippingType): void;
  setPaymentNavigationButton(data: IInputKeyValue): void;
  sameAsPersonalToggleSwitch(): void;
  updateNewAddressShipping(data: IShippingAddressAndNote): void;
  updateDummyState(data: IShippingFormField): void;
  fetchPickupLocation(data: ILatLong): void;
  setSelectedStore(data: ISelectedStoreAddress): void;
  setOtherPersonPickup(data: boolean): void;
  setShippingMode(shippingMode: shippingType): void;
  fetchShippingFee(data?: boolean): void;
  isUserPickedLocation(): void;
  updateSelectedDate(type: string): void;
  fetchDeliveryDate(type: string): void;
  editPaymentStepClick(): void;
  evaluateCartSummary(cartSummary: ICartSummary[]): void;
  fetchCheckoutAddressLoading(data: boolean): void;
}

export type IComponentProps = IComponentStateProps & ICompDispatchToProps;

export class ShippingMap extends Component<IComponentProps, {}> {
  constructor(props: IComponentProps) {
    super(props);
    this.renderPaymentStep = this.renderPaymentStep.bind(this);
    this.getStores = this.getStores.bind(this);
    this.onEnterPressOnSearch = this.onEnterPressOnSearch.bind(this);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.CHECKOUT_SHIPPING_INFO);
    window.scrollTo(0, 0);
    this.props.fetchCheckoutAddressLoading(true);
    this.props.fetchShippingFee(true);
    this.props.editPaymentStepClick();
    sendProceedEvent(CHECKOUT_VIEW.SHIPPING.STEP, CHECKOUT_VIEW.SHIPPING.VIEW);
  }

  componentWillUnmount(): void {
    const { shippingInfo, sameAsPersonalToggleSwitch } = this.props;
    const { sameAsPersonalInfo, streetAddressId, dummyState } = shippingInfo;

    if (streetAddressId !== dummyState.streetAddressId && sameAsPersonalInfo) {
      sameAsPersonalToggleSwitch();
    }
  }

  // tslint:disable-next-line:no-any
  getStores(data: any): void {
    this.props.fetchPickupLocation({
      geoX: data.geoX,
      geoY: data.geoY
    });
  }

  onEnterPressOnSearch(value: string): void {
    getLocationOfAddress(value)
      .then((response: INortheast) => {
        this.props.fetchPickupLocation({
          geoX: response.lat.toString(),
          geoY: response.lng.toString()
        });
      })
      // tslint:disable-next-line:no-any
      .catch((error: any) => {
        logError(error);
      });
  }

  renderPaymentStep(): ReactNode {
    const { cartId, shippingConfiguration, shippingTranslation } = this.props;
    const {
      personalInfo,
      buttonEnable,
      updateShippingAddress,
      evaluateCartSummary
    } = this.props;
    const { setProceedButton, sendShippingAddress } = this.props;
    const { updateDummyState, shippingInfo, isUserPickedLocation } = this.props;
    const {
      updateFormChangeField,
      setDeliveryOptions,
      updateSelectedDate
    } = this.props;
    const {
      checkoutTranslation,
      fetchShippingFee,
      fetchDeliveryDate
    } = this.props;
    const {
      fetchPickupLocation,
      setSelectedStore,
      globalCurrency
    } = this.props;
    const { sameAsPersonalToggleSwitch, setOtherPersonPickup } = this.props;

    switch (this.props.checkout.shippingInfo.shippingType) {
      case SHIPPING_TYPE.STANDARD:
      case SHIPPING_TYPE.SAME_DAY:
      case SHIPPING_TYPE.EXACT_DAY:
        return (
          <DeliverToAddress
            shippingInfo={shippingInfo}
            cartId={cartId}
            buttonEnable={buttonEnable}
            shippingConfiguration={shippingConfiguration}
            shippingTranslation={shippingTranslation}
            sameAsPersonalToggleSwitch={sameAsPersonalToggleSwitch}
            updateShippingAddress={updateShippingAddress}
            setProceedButton={setProceedButton}
            sendShippingAddress={sendShippingAddress}
            personalInfo={personalInfo}
            updateFormChangeField={updateFormChangeField}
            updateDummyState={updateDummyState}
            setDeliveryOptions={setDeliveryOptions}
            updateSelectedDate={updateSelectedDate}
            fetchDeliveryDate={fetchDeliveryDate}
            globalCurrency={globalCurrency}
            fetchShippingFee={fetchShippingFee}
            evaluateCartSummary={evaluateCartSummary}
            loadingText={checkoutTranslation.loadingText}
          />
        );

      case SHIPPING_TYPE.PICK_UP_STORE:
        return (
          <PickUpAtStore
            markers={
              shippingInfo.pickUpLocations ? shippingInfo.pickUpLocations : []
            }
            checkoutTranslation={checkoutTranslation}
            shippingInfo={shippingInfo}
            isUserPickedLocation={isUserPickedLocation}
            fetchPickupLocation={fetchPickupLocation}
            setSelectedStore={setSelectedStore}
            setOtherPersonPickup={setOtherPersonPickup}
            sendShippingAddress={sendShippingAddress}
            getStores={this.getStores}
            onEnterPressOnSearch={this.onEnterPressOnSearch}
            fetchShippingFee={fetchShippingFee}
          />
        );

      case SHIPPING_TYPE.PARCEL_LOCKER:
        return (
          <ParcelLocker
            markers={
              shippingInfo.pickUpLocations ? shippingInfo.pickUpLocations : []
            }
            checkoutTranslation={checkoutTranslation}
            shippingInfo={shippingInfo}
            isUserPickedLocation={isUserPickedLocation}
            fetchPickupLocation={fetchPickupLocation}
            setSelectedStore={setSelectedStore}
            setOtherPersonPickup={setOtherPersonPickup}
            sendShippingAddress={sendShippingAddress}
            getStores={this.getStores}
            onEnterPressOnSearch={this.onEnterPressOnSearch}
            fetchShippingFee={fetchShippingFee}
          />
        );
      case SHIPPING_TYPE.PICK_UP_POINT:
        return (
          <PickUpAtPoints
            markers={
              shippingInfo.pickUpLocations ? shippingInfo.pickUpLocations : []
            }
            checkoutTranslation={checkoutTranslation}
            shippingInfo={shippingInfo}
            isUserPickedLocation={isUserPickedLocation}
            fetchPickupLocation={fetchPickupLocation}
            setSelectedStore={setSelectedStore}
            setOtherPersonPickup={setOtherPersonPickup}
            sendShippingAddress={sendShippingAddress}
            getStores={this.getStores}
            onEnterPressOnSearch={this.onEnterPressOnSearch}
            fetchShippingFee={fetchShippingFee}
          />
        );
      default:
        return null;
    }
  }

  render(): ReactNode {
    const {
      checkoutTranslation,
      editPaymentStepClick,
      setShippingMode,
      shippingInfo,
      mainAppShell,
      fetchCheckoutAddressLoading
    } = this.props;

    return (
      <>
        {mainAppShell ? (
          <CheckoutAppShellComponent
            checkoutTranslation={checkoutTranslation}
            editPaymentStepClick={editPaymentStepClick}
            fetchCheckoutAddressLoading={fetchCheckoutAddressLoading}
          />
        ) : (
          <StyledShipping>
            {!isMobile.phone ? (
              <SideNavigation>
                <ShippingSideBar
                  shippingTranslation={checkoutTranslation.shippingInfo}
                  setShippingMode={setShippingMode}
                  shippingInfo={shippingInfo}
                />
              </SideNavigation>
            ) : null}

            <div className='mainContent'>
              <StepNavigation
                checkoutTranslation={checkoutTranslation}
                editPaymentStepClick={editPaymentStepClick}
              />
              <div className='shippingStepsWrap'>
                {isMobile.phone || isMobile.tablet ? (
                  <MobileNav
                    shippingInfo={shippingInfo}
                    shippingTranslation={this.props.shippingTranslation}
                    shippingConfiguration={this.props.shippingConfiguration}
                    setShippingMode={setShippingMode}
                  />
                ) : null}
                {this.renderPaymentStep()}
              </div>
              {isMobile.tablet ? (
                <Footer
                  className='footerCheckout'
                  termsAndConditionsUrl={
                    this.props.configuration.cms_configuration.global
                      .termsAndConditionsUrl
                  }
                  shouldTermsAndConditionsOpenInNewTab={
                    this.props.configuration.cms_configuration.global
                      .shouldTermsAndConditionsOpenInNewTab
                  }
                  globalTranslation={this.props.translation.cart.global}
                />
              ) : null}
            </div>
          </StyledShipping>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IComponentStateProps => ({
  cartId: state.basket && state.basket.cartId,
  checkout: state.checkout,
  shippingConfiguration:
    state.configuration.cms_configuration.modules.checkout.shipping,
  shippingTranslation: state.translation.cart.checkout.shippingInfo,
  globalCurrency: state.configuration.cms_configuration.global.currency,

  buttonEnable: state.checkout.shippingInfo.buttonEnable,
  shippingInfo: state.checkout.shippingInfo,
  personalInfo: state.checkout.personalInfo,
  checkoutTranslation: state.translation.cart.checkout,
  configuration: state.configuration,
  translation: state.translation,
  mainAppShell: state.checkout.checkout.mainAppShell
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): ICompDispatchToProps => ({
  sendShippingAddress(): void {
    dispatch(shippingActions.sendShippingAddress());
  },

  updateInputField(data: { key: string; value: string }): void {
    dispatch(shippingActions.updateInputField(data));
  },
  setPaymentNavigationButton(data: { key: string; value: string }): void {
    dispatch(shippingActions.setPaymentNavigationButton(data));
  },
  setDeliveryOptions(type: shippingType): void {
    dispatch(shippingActions.setDeliveryOptions(type));
  },
  updateNewAddressShipping(data: IShippingAddressAndNote): void {
    dispatch(shippingActions.updateNewAddressShipping(data));
  },
  sameAsPersonalToggleSwitch(): void {
    dispatch(shippingActions.sameAsPersonalSwitch());
  },
  setProceedButton(data: boolean): void {
    dispatch(shippingActions.setProceedButton(data));
  },
  updateFormChangeField(data: boolean): void {
    dispatch(shippingActions.updateFormChangeField(data));
  },

  updateDummyState(data: IShippingFormField): void {
    dispatch(shippingActions.updateDummyState(data));
  },
  updateShippingAddress(data: IUpdateShippingAddress): void {
    dispatch(shippingActions.updateShippingAddress(data));
  },
  fetchPickupLocation(data: ILatLong): void {
    dispatch(shippingActions.fetchPickupLocations(data));
  },
  setSelectedStore(data: ISelectedStoreAddress): void {
    dispatch(shippingActions.setSelectedStore(data));
  },
  setOtherPersonPickup(data: boolean): void {
    dispatch(shippingActions.setOtherPersonPickup(data));
  },
  setShippingMode(shippingMode: shippingType): void {
    dispatch(shippingActions.setShippingMode(shippingMode));
  },
  fetchShippingFee(data = false): void {
    dispatch(shippingActions.fetchShippingFee(data));
  },
  isUserPickedLocation(): void {
    dispatch(shippingActions.isUserPickedLocation());
  },
  updateSelectedDate(type: string): void {
    dispatch(shippingActions.updateSelectedDate(type));
  },
  fetchDeliveryDate(type: string): void {
    dispatch(shippingActions.fetchDeliveryDate(type));
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  },
  evaluateCartSummary(cartSummary: ICartSummary[]): void {
    dispatch(checkoutActions.evaluateCartSummaryPrice(cartSummary));
  },
  fetchCheckoutAddressLoading(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddressLoading(data));
  }
});

export default connect<
  IComponentStateProps,
  ICompDispatchToProps,
  void,
  RootState
>(
  mapStateToProps,
  mapDispatchToProps
)(ShippingMap);
