import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledMobileNav } from './ShippingSideBar/MobileNav/styles';

export const StyledCompleteMapWrap = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  z-index: 1000;
  background: ${colors.white};

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    position: static;
    display: flex;
    flex-direction: column;
    .mapHeader {
      margin: -2.25rem -2.25rem 0;
      padding: 2.25rem 2.25rem 0;
      min-height: 110px;

      .labelAndCloseIcon {
        display: none;
      }
    }

    .storeAddressWrap {
      margin: 0;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .storeAddressWrap {
      margin-top: 2.625rem;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .storeAddressWrap {
      margin-top: 0;
    }
  }
`;

export const StyledShipping = styled.div`
  display: flex;
  flex-direction: row;
  .mainContent {
    width: 100%;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    flex-direction: column;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    flex-direction: row;
    .shippingStepsWrap {
      flex-grow: 1;
      ${StyledMobileNav} {
        display: none;
      }
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .mainContent {
      display: flex;
      flex-direction: column;
      .shippingStepsWrap {
        display: flex;
        flex: 1;
      }
    }
  }
`;
