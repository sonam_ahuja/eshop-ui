import {
  getConvertedDate,
  getSelectedDate
} from '@checkout/CheckoutSteps/Shipping/utils/dateConvert';
import { IDateListItem } from '@checkout/store/shipping/types';

describe('<dateConvert />', () => {
  const data: IDateListItem[] = [
    {
      id: 'string',
      title: 'string',
      key: 'string'
    }
  ];
  test('getConvertedDate', () => {
    expect(typeof getConvertedDate(data)).toBe('object');
  });

  test('getSelectedDate', () => {
    expect(getSelectedDate(data, '')).toBe(null);
  });
});
