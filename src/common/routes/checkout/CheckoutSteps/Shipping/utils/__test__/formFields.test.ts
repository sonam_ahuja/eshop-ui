import {
  isFormFieldValidAsPerType,
  isInputAllowed
} from '@checkout/CheckoutSteps/Shipping/utils/formFields';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';

describe('<formField />', () => {
  test('isInputAllowed', () => {
    expect(
      isInputAllowed('number', 'type', {
        type: VALIDATION_TYPE.MAX,
        value: 'value',
        message: 'error'
      })
    ).toBeDefined();
  });

  test('isFormFieldValidAsPerType', () => {
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.MAX)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.MIN)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.BETWEEN)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType(
        'number',
        'value',
        VALIDATION_TYPE.MIN_LOWERCASE
      )
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType(
        'number',
        'value',
        VALIDATION_TYPE.MIN_UPPERCASE
      )
    ).toBe(false);

    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.MIN_DIGIT)
    ).toBe(false);
    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.REGEX)
    ).toBe(false);

    expect(
      isFormFieldValidAsPerType('number', 'value', VALIDATION_TYPE.NONE)
    ).toBe(true);
  });
});
