import { IDateListItem } from '@checkout/store/shipping/types';

// tslint:disable-next-line:no-any
export function getConvertedDate(dateList: any): IDateListItem[] {
  const convertedDateItems: IDateListItem[] = [];

  dateList.forEach((item: number, i: number) => {
    const dateItem: IDateListItem = { id: '', title: '' };
    const itemToDate = new Date(item * 1000);
    dateItem.id = i;
    dateItem.value = item;
    dateItem.title = `${itemToDate.toLocaleString('en-us', {
      weekday: 'short'
    })}, ${itemToDate.getDate()} ${itemToDate.toLocaleString('en-us', {
      month: 'short'
    })}`;

    convertedDateItems.push(dateItem);
  });

  return convertedDateItems;
}

export function getSelectedDate(
  dateList: IDateListItem[],
  selectedDate: string | number
): IDateListItem | null {
  let dateItem: IDateListItem = {
    id: 0,
    title: ''
  };
  dateList.forEach((item: IDateListItem) => {
    if (String(selectedDate).length > 10) {
      if (
        new Date(Number(item.value) * 1000).getDate() ===
        new Date(selectedDate).getDate()
      ) {
        dateItem = item;
      }
    } else {
      if (
        new Date(Number(item.value) * 1000).getDate() ===
        new Date(Number(selectedDate) * 1000).getDate()
      ) {
        dateItem = item;
      }
    }
  });

  if (dateItem.title !== '') {
    return dateItem;
  }

  return null;
}
