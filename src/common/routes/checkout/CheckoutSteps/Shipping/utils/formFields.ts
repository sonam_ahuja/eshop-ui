import {
  IFields,
  IFieldValdation,
  validationType
} from '@common/store/types/configuration';
import { IShippingFormField } from '@src/common/routes/checkout/store/shipping/types';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { IProps as IFormBodyProps } from '@checkout/CheckoutSteps/Shipping/DeliverToAddress/Form/types';
import PERSONAL_INFO_CONSTANTS from '@checkout/CheckoutSteps/PersonalInfo/constants';

export function getLabelValueForFormFields(
  props: IFormBodyProps,
  field: string
): string {
  const labelKey = props.shippingConfiguration.form.fields[field].labelKey;
  const isFieldMandatory =
    props.shippingConfiguration.form.fields[field].mandatory;
  let labelValue = props.shippingTranslation[labelKey];

  if (!isFieldMandatory && props.shippingTranslation.optionalText) {
    labelValue += ` ${props.shippingTranslation.optionalText}`;
  }

  return labelValue;
}

export function isInputAllowed(
  inputType: string,
  inputValue: string,
  validation: IFieldValdation
): boolean {
  // tslint:disable-next-line:no-small-switch
  switch (inputType) {
    case PERSONAL_INFO_CONSTANTS.NUMBER:
      if (checkCharE(inputValue)) {
        return false;
      }
      if (maxLengthReached(inputValue, validation)) {
        return false;
      }
      break;
    default:
      break;
  }

  return true;
}

export function readyToProceed(
  formFields: IShippingFormField,
  cmsFormConfiguration: IFields
): boolean {
  let isFormValid = true;

  Object.keys(formFields).forEach(field => {
    if (
      cmsFormConfiguration[field] &&
      cmsFormConfiguration[field].show &&
      cmsFormConfiguration[field].mandatory &&
      (!formFields[field].value || !formFields[field].isValid)
    ) {
      isFormValid = false;
    }
  });

  return isFormValid;
}

export function isFormFieldValidAsPerType(
  value: string | number,
  inputValue: string,
  type: validationType
): boolean {
  const {
    MAX,
    MIN,
    REGEX,
    BETWEEN,
    NONE,
    MIN_LOWERCASE,
    MIN_UPPERCASE,
    MIN_DIGIT,
    EXACT
  } = VALIDATION_TYPE;

  switch (type) {
    case MAX:
      return inputValue.length <= value;
    case MIN:
      return inputValue.length >= value;
    case EXACT:
      return String(inputValue.length) === String(value);
    case BETWEEN:
      const values = (value as string).split('-');

      return (
        inputValue.length <= parseInt(values[1], 0) &&
        inputValue.length >= parseInt(values[0], 0)
      );
    case REGEX:
      const newRegex = new RegExp(value as string);

      return newRegex.test(inputValue);
    case NONE:
      return true;
    case MIN_LOWERCASE:
      return value <= inputValue.replace(/[^a-z]/g, '').length;
    case MIN_UPPERCASE:
      return value <= inputValue.replace(/[^A-Z]/g, '').length;
    case MIN_DIGIT:
      return value <= inputValue.replace(/[^0-9]/g, '').length;
    default:
      return true;
  }
}

function checkCharE(value: string): boolean {
  return value === PERSONAL_INFO_CONSTANTS.CHAR_E;
}

function maxLengthReached(value: string, validation: IFieldValdation): boolean {
  return (
    validation.type === VALIDATION_TYPE.MAX &&
    value.length > parseInt(validation.value, 0)
  );
}
