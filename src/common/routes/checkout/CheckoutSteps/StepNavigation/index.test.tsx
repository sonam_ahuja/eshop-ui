import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import { StaticRouter } from 'react-router-dom';

import StepNavigation, { IProps as IStepNavigationProps } from '.';
describe('<Order Review Shipping />', () => {
  const props: IStepNavigationProps = {
    ...histroyParams,
    checkoutTranslation: appState().translation.cart.checkout,
    editPaymentStepClick: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <StepNavigation {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('handleChange test for input field', () => {
    const copyComponentProps = { ...props };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <StepNavigation {...copyComponentProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    const ClickHandlerDiv = component.find('div[onClick]');
    ClickHandlerDiv.at(0).simulate('click');
    ClickHandlerDiv.at(1).simulate('click');
    ClickHandlerDiv.at(2).simulate('click');
  });
});
