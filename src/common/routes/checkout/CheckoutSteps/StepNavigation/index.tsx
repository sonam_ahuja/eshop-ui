import React, { ReactNode } from 'react';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CHECKOUT_STEPS_ARRAY } from '@checkout/CheckoutSteps/constants';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';
import { sendCTAClicks } from '@events/index';

import { StyledStepNavigation } from './styles';

interface IRouterParams {
  history?: string;
  location?: string;
  martch?: string;
}
export interface IProps extends RouteComponentProps<IRouterParams> {
  checkoutTranslation: ICheckoutTranslation;
  editPaymentStepClick(): void;
}

export class StepNavigation extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);

    this.getActiveCheckoutStepIndex = this.getActiveCheckoutStepIndex.bind(
      this
    );
    this.getActiveStepName = this.getActiveStepName.bind(this);
    this.onPersonalInfoClick = this.onPersonalInfoClick.bind(this);
    this.onShippingInfoClick = this.onShippingInfoClick.bind(this);
    this.onPaymentAndBillingClick = this.onPaymentAndBillingClick.bind(this);
  }

  getActiveStepName(): string {
    const { pathname } = this.props.location;
    const pathNameSplitArray = pathname.split('/');

    return pathNameSplitArray[pathNameSplitArray.length - 1];
  }

  getActiveCheckoutStepIndex(): number {
    const activeStep = this.getActiveStepName();

    if (activeStep === CHECKOUT_SUB_ROUTE_TYPE.BILLING) {
      return CHECKOUT_STEPS_ARRAY.indexOf(CHECKOUT_SUB_ROUTE_TYPE.PAYMENT);
    }

    return CHECKOUT_STEPS_ARRAY.indexOf(activeStep);
  }

  onPersonalInfoClick(): void {
    if (this.getActiveStepName() !== CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO) {
      sendCTAClicks(
        CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO,
        window.location.href
      );
    }

    this.props.history.push(
      `/checkout/${CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO}`
    );
  }

  onShippingInfoClick(): void {
    if (
      CHECKOUT_STEPS_ARRAY.indexOf(CHECKOUT_SUB_ROUTE_TYPE.SHIPPING) <
      this.getActiveCheckoutStepIndex()
    ) {
      sendCTAClicks(CHECKOUT_SUB_ROUTE_TYPE.SHIPPING, window.location.href);
      this.props.history.push(`/checkout/${CHECKOUT_SUB_ROUTE_TYPE.SHIPPING}`);
      this.props.editPaymentStepClick();
    }
  }

  onPaymentAndBillingClick(): void {
    if (
      CHECKOUT_STEPS_ARRAY.indexOf(CHECKOUT_SUB_ROUTE_TYPE.PAYMENT) <
      this.getActiveCheckoutStepIndex()
    ) {
      sendCTAClicks(CHECKOUT_SUB_ROUTE_TYPE.PAYMENT, window.location.href);
      this.props.history.push(`/checkout/${CHECKOUT_SUB_ROUTE_TYPE.PAYMENT}`);
      this.props.editPaymentStepClick();
    }
  }

  render(): ReactNode {
    const { checkoutTranslation } = this.props;

    const activeCheckoutStepIndex = this.getActiveCheckoutStepIndex();

    return (
      <StyledStepNavigation
        steps={[
          {
            label: checkoutTranslation.personalInfo.personalInfoText,
            iconName: 'ec-user-account',
            activeIconName: 'ec-user-account',
            onClickHandler: this.onPersonalInfoClick
          },
          {
            label: checkoutTranslation.shippingInfo.shippingText,
            iconName: 'ec-transporter-right',
            activeIconName: 'ec-transporter-right',
            onClickHandler: this.onShippingInfoClick,
            lineStyle: {}
          },
          {
            label: checkoutTranslation.paymentInfo.paymentAndBilling,
            iconName: 'ec-credit-card',
            activeIconName: 'ec-credit-card',
            onClickHandler: this.onPaymentAndBillingClick
          }
        ]}
        activeStep={activeCheckoutStepIndex}
        completedStep={activeCheckoutStepIndex}
      />
    );
  }
}

export default withRouter(StepNavigation);
