import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { ProgressStepper } from 'dt-components';
import { IProps } from 'dt-components/lib/es/components/molecules/progress-stepper';
import { ComponentType } from 'enzyme';

export const StyledStepNavigation = styled(ProgressStepper)`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 100;

  &.dt_progressStepper {
    .dt_title {
      padding: 0 15px 0 8px;
    }
    .circleDiv.disabled {
      cursor: default;
    }
  }

  .dt_icon.lockIcon {
    display: none;
    margin-left: -1.3rem;
    font-size: 15px;
    margin-top: -2px;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    &.dt_progressStepper {
      position: absolute;
      height: auto;
      background: none;
      padding: 2.5rem 2.25rem 0 2.25rem;
      z-index: 2;
      .circleDiv.disabled {
        width: 40px;
        height: 40px;
        box-shadow: 0 0 0 2px ${colors.warmGray} inset;

        .dt_icon {
          font-size: 20px;
          path {
            fill: ${colors.mediumGray};
          }
        }
      }

      .dt_divider {
        color: ${colors.warmGray};
      }
      .dt_divider.disabled,
      .dt_divider.completed {
        width: 12px;
      }
      .dt_divider.completed {
        color: ${colors.magenta};
      }

      .dt_title {
        color: ${colors.darkGray};
        font-size: 1.25rem;
        line-height: 1.75rem;
        padding: 0 0.5rem;
      }

      .active .dt_icon path {
        fill: ${colors.magenta};
      }

      .lockIcon {
        display: block;
        margin-left: 0;
        font-size: 15px;
        position: relative;
        top: 2px;
        /* left: -10px; */
        svg {
          path {
            fill: ${colors.gray};
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    &.dt_progressStepper {
      position: relative;
      padding: 2.5rem 3rem 0 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    &.dt_progressStepper {
      padding: 2.1rem 2.25rem 0 2.25rem;
      .dt_divider.disabled,
      .dt_divider.completed {
        width: 20px;
      }
      .dt_title {
        font-size: 1.5rem;
        padding: 0 15px 0 14px;
      }
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    &.dt_progressStepper {
      padding: 2.1rem 5.5rem 0 5.5rem;
    }
  }
` as ComponentType<IProps>;
