// tslint:disable-next-line:no-commented-code
// const CHECKOUT_STEPS = {
//   PERSONAL_INFO: 0,
//   SHIPPING: 1,
//   PAYMENT: 2,
//   BILLING: 3,
//   ORDER_REVIEW: 4
// };

export const CONSTANTS = {
  PHONE_NUMBER: 'phoneNumber',
  NUMBER: 'number',
  ADDRESS: 'address',
  CHAR_E: 'e'
};

// tslint:disable-next-line:no-commented-code
// export default CHECKOUT_STEPS;

export const CHECKOUT_STEPS_ARRAY = [
  'personal-info',
  'shipping',
  'payment',
  'billing',
  'order-review'
];

export const SHIPPING_FEE_DEBOUNCE_TIME = 300;

export const SHIPPING_OPEN_HOUR_DAY_SLICE = 3;
