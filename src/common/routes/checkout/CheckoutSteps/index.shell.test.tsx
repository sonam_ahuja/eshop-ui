import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import { mount } from 'enzyme';
import React from 'react';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import CheckoutStepsAppShell, {
  IProps
} from '@checkout/CheckoutSteps/index.shell';

describe('<CheckoutStepsAppShell/>', () => {
  const props: IProps = {
    ...histroyParams,
    checkoutTranslation: appState().translation.cart.checkout,
    editPaymentStepClick: jest.fn(),
    fetchCheckoutAddressLoading: jest.fn(),
  };

  const componentWrapper = (newProps: IProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <CheckoutStepsAppShell {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
