import React, { ReactNode } from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints, colors } from '@src/common/variables';
import { isMobile } from '@src/common/utils';
import { Icon } from 'dt-components';
import { ICheckoutTranslation } from '@src/common/store/types/translation';
import { Column, Row } from '@src/common/components/Grid/styles';
import { FormWrapper } from '@checkout/Common/FormWrapper/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CHECKOUT_STEPS_ARRAY } from '@checkout/CheckoutSteps/constants';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';

import { StyledStepNavigation } from './StepNavigation/styles';

interface IRouterParams {
  history?: string;
  location?: string;
  martch?: string;
}

export interface IProps extends RouteComponentProps<IRouterParams> {
  checkoutTranslation: ICheckoutTranslation;
  editPaymentStepClick(): void;
  fetchCheckoutAddressLoading(data: boolean): void;
}

const StyledSelectWithIconWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  .thumbnail {
    margin-right: 1.25rem;
    height: 5rem;
    width: 5rem;
  }
  .dropdown {
    height: 1.5rem;
    width: 100%;
  }
`;

const StyledShellMobile = styled(StyledShell)`
  width: 100%;
  height: calc(100vh - 80px);
  background: ${colors.fogGray};

  .sectionTop {
    height: 6.625rem;
    padding: 0 1.25rem;
    display: flex;
    align-items: center;
    background: ${colors.white};
    .sideNav {
      padding: 0 1.25rem;
    }
  }

  .mainContent {
    padding: 3.25rem 1.25rem 1.25rem;

    .shine {
      height: 2.5rem;
      margin-bottom: 1.5rem;
    }

    .twoCol {
      display: flex;
      justify-content: space-between;
      .shine {
        width: 48%;
      }
    }
  }
`;

const StyledShellDesktop = styled(StyledShell)`
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
  .sideNav {
    height: 6.5rem;
    padding: 0 2.25rem;
    background: ${colors.white};
    display: flex;
    align-items: center;
    flex-shrink: 0;
    .logo,
    .line {
      display: none;
    }
  }
  .mainContentWrap {
    width: 100%;
  }
  .formWrapper {
    ${Column} {
      margin-bottom: 30px;
    }

    .shine {
      height: 50px;
      width: 100%;
    }
    .numberPortingBanner {
      height: 148px;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .formWrapper {
      padding: 3.25rem 2.25rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    flex-direction: row;
    .sideNav {
      position: sticky;
      top: 0;
      left: 0;
      width: 14.5rem;
      height: 100vh;
      flex-direction: column;
      align-items: flex-start;
      .logo {
        display: block;
        height: 7.25rem;
        font-size: 3.5rem;
        padding-top: 1.8rem;
        margin-bottom: 2.75rem;
        display: block;
        padding-left: 0.75rem;
      }

      .line {
        width: 100%;
        height: 8rem;
        display: block;
      }
      ${StyledSelectWithIconWrap} {
        display: none;
      }
    }
    .formWrapper {
      padding: 3.25rem 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .sideNav {
      width: 15.8rem;
      .logo {
        height: 5.75rem;
        padding-top: 1.3rem;
        padding-left: 2.65rem;
        margin-bottom: 1.9rem;
      }
    }
    .formWrapper {
      padding: 3.25rem 2.25rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .sideNav {
      width: 19.25rem;
      padding: 0 3.25rem;
      .logo {
        padding-left: 2.25rem;
      }
    }
    .formWrapper {
      padding: 3.5rem 5.5rem 2.25rem;
    }
  }
`;

class CheckoutStepsAppShell extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);

    this.getActiveCheckoutStepIndex = this.getActiveCheckoutStepIndex.bind(
      this
    );
    this.getActiveStepName = this.getActiveStepName.bind(this);
    this.onPersonalInfoClick = this.onPersonalInfoClick.bind(this);
    this.onShippingInfoClick = this.onShippingInfoClick.bind(this);
    this.onPaymentAndBillingClick = this.onPaymentAndBillingClick.bind(this);
  }

  getActiveStepName(): string {
    const { pathname } = this.props.location;
    const pathNameSplitArray = pathname.split('/');

    return pathNameSplitArray[pathNameSplitArray.length - 1];
  }

  getActiveCheckoutStepIndex(): number {
    const activeStep = this.getActiveStepName();

    if (activeStep === CHECKOUT_SUB_ROUTE_TYPE.BILLING) {
      return CHECKOUT_STEPS_ARRAY.indexOf(CHECKOUT_SUB_ROUTE_TYPE.PAYMENT);
    }

    return CHECKOUT_STEPS_ARRAY.indexOf(activeStep);
  }

  onPersonalInfoClick(): void {
    this.props.fetchCheckoutAddressLoading(false);
    this.props.history.push(
      `/checkout/${CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO}`
    );
  }

  onShippingInfoClick(): void {
    if (
      CHECKOUT_STEPS_ARRAY.indexOf(CHECKOUT_SUB_ROUTE_TYPE.SHIPPING) <
      this.getActiveCheckoutStepIndex()
    ) {
      this.props.fetchCheckoutAddressLoading(false);
      this.props.history.push(`/checkout/${CHECKOUT_SUB_ROUTE_TYPE.SHIPPING}`);
      this.props.editPaymentStepClick();
    }
  }

  onPaymentAndBillingClick(): void {
    if (
      CHECKOUT_STEPS_ARRAY.indexOf(CHECKOUT_SUB_ROUTE_TYPE.PAYMENT) <
      this.getActiveCheckoutStepIndex()
    ) {
      this.props.fetchCheckoutAddressLoading(false);
      this.props.history.push(`/checkout/${CHECKOUT_SUB_ROUTE_TYPE.PAYMENT}`);
      this.props.editPaymentStepClick();
    }
  }

  render(): ReactNode {
    const { checkoutTranslation } = this.props;

    const activeCheckoutStepIndex = this.getActiveCheckoutStepIndex();

    return (
      <>
        {isMobile.phone ? (
          <StyledShellMobile className='primary'>
            <StyledStepNavigation
              steps={[
                {
                  label: checkoutTranslation.personalInfo.personalInfoText,
                  // tslint:disable-next-line:no-duplicate-string
                  iconName: 'ec-user-account',
                  activeIconName: 'ec-user-account',
                  onClickHandler: this.onPersonalInfoClick
                },
                {
                  label: checkoutTranslation.shippingInfo.shippingText,
                  // tslint:disable-next-line:no-duplicate-string
                  iconName: 'ec-transporter-right',
                  activeIconName: 'ec-transporter-right',
                  onClickHandler: this.onShippingInfoClick,
                },
                {
                  label: checkoutTranslation.paymentInfo.paymentAndBilling,
                  // tslint:disable-next-line:no-duplicate-string
                  iconName: 'ec-credit-card',
                  activeIconName: 'ec-credit-card',
                  onClickHandler: this.onPaymentAndBillingClick
                }
              ]}
              activeStep={activeCheckoutStepIndex}
              completedStep={
                activeCheckoutStepIndex > 0 ? activeCheckoutStepIndex - 1 : 0
              }
            />
            <div className='sectionTop'>
              <StyledSelectWithIconWrap className='selectWithIcon'>
                <div className='shine thumbnail' />
                <div className='shine dropdown' />
              </StyledSelectWithIconWrap>
            </div>
            <div className='mainContent'>
              <div className='twoCol'>
                <div className='shine' />
                <div className='shine' />
              </div>
              <div className='shine' />
              <div className='shine' />
              <div className='shine' />
              <div className='shine' />
            </div>
          </StyledShellMobile>
        ) : (
            <StyledShellDesktop className='primary'>
              <div className='sideNav'>
                <Icon className='logo' name='ec-dt-logo' size='large' />
                <div className='shine line' />
                <StyledSelectWithIconWrap className='selectWithIcon'>
                  <div className='shine thumbnail' />
                  <div className='shine dropdown' />
                </StyledSelectWithIconWrap>
              </div>
              <div className='mainContentWrap'>
                <StyledStepNavigation
                  steps={[
                    {
                      label: checkoutTranslation.personalInfo.personalInfoText,
                      iconName: 'ec-user-account',
                      activeIconName: 'ec-user-account',
                      onClickHandler: this.onPersonalInfoClick
                    },
                    {
                      label: checkoutTranslation.shippingInfo.shippingText,
                      iconName: 'ec-transporter-right',
                      activeIconName: 'ec-transporter-right',
                      onClickHandler: this.onShippingInfoClick,

                    },
                    {
                      label: checkoutTranslation.paymentInfo.paymentAndBilling,
                      iconName: 'ec-credit-card',
                      activeIconName: 'ec-credit-card',
                      onClickHandler: this.onPaymentAndBillingClick
                    }
                  ]}
                  activeStep={activeCheckoutStepIndex}
                  completedStep={
                    activeCheckoutStepIndex > 0 ? activeCheckoutStepIndex - 1 : 0
                  }
                />
                <FormWrapper className='formWrapper'>
                  <Row>
                    <Column colMobile={9}>
                      <div className='shine' />
                    </Column>
                    <Column colMobile={3}>
                      <div className='shine' />
                    </Column>
                  </Row>
                  <Row>
                    <Column colMobile={9}>
                      <div className='shine' />
                    </Column>
                    <Column colMobile={3}>
                      <div className='shine' />
                    </Column>
                  </Row>
                  <Row>
                    <Column colMobile={12}>
                      <div className='shine' />
                    </Column>
                  </Row>
                  <Row>
                    <Column colMobile={4}>
                      <div className='shine' />
                    </Column>
                    <Column colMobile={4}>
                      <div className='shine' />
                    </Column>
                    <Column colMobile={4}>
                      <div className='shine' />
                    </Column>
                  </Row>
                  <Row>
                    <Column colMobile={12}>
                      <div className='shine numberPortingBanner' />
                    </Column>
                  </Row>
                </FormWrapper>
              </div>
            </StyledShellDesktop>
          )}
      </>
    );
  }
}

// tslint:disable-next-line:max-file-line-count
export default withRouter(CheckoutStepsAppShell);
