import {
  fieldValidation,
  getActiveCheckoutStep
} from '@checkout/CheckoutSteps/utils';
import { billingForm, formFields } from '@src/common/mock-api/checkout/billing';

import { readyToProceed } from './Billing/utils/formFields';
describe('<utils />', () => {
  test('fieldValidation test', () => {
    expect(fieldValidation('value', 'value', 'max')).toBeDefined();
  });

  test('fieldValidation should work properly', () => {
    expect(fieldValidation('10', '6', 'max')).toBeDefined();
    expect(fieldValidation('6', '10', 'max')).toBeDefined();
    expect(fieldValidation('10', '6', 'min')).toBeDefined();
    expect(fieldValidation('6', '10', 'min')).toBeDefined();
    expect(fieldValidation('//', '10', 'regex')).toBeDefined();
    expect(fieldValidation('6', '10', 'none')).toBeDefined();
    // tslint:disable-next-line:no-any
    expect(fieldValidation('6', '10', 'adf' as any)).toBeDefined();
    expect(fieldValidation('10', '10', 'between')).toBeDefined();
    expect(fieldValidation('1-2', '10', 'between')).toBeDefined();
    expect(fieldValidation('10', '10', 'regex')).toBeDefined();
    expect(fieldValidation('10', '10', 'min-digit')).toBeDefined();
  });

  test('billing ready to proceed', () => {
    expect(readyToProceed(billingForm, formFields)).toBeDefined();
  });

  test('show return CheckoutStep', () => {
    expect(
      getActiveCheckoutStep(
        // tslint:disable-next-line:no-any
        { pathname: 'https://checkout/personalInfo' } as any
      )
    ).toBeDefined();
  });
});
