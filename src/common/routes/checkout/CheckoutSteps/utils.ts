import { IFields, validationType } from '@common/store/types/configuration';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import * as H from 'history';
import { IDateListItem } from '@checkout/store/shipping/types';
import { isFormFieldValidAsPerType } from '@checkout/CheckoutSteps/Shipping/utils/formFields';

export function getConvertedDate(dateList: number[]): IDateListItem[] {
  const convertedDateItems: IDateListItem[] = [];

  dateList.forEach((item: number, index: number) => {
    const dateItem: IDateListItem = { id: '', title: '' };
    const itemToDate = new Date(item * 1000);
    dateItem.id = index;
    dateItem.value = item;
    dateItem.title = `${itemToDate.toLocaleString('en-us', {
      day: '2-digit'
    })} ${itemToDate.toLocaleString('en-us', {
      month: 'long'
    })}  ${itemToDate.toLocaleString('en-us', {
      year: 'numeric'
    })}`;

    convertedDateItems.push(dateItem);
  });

  return convertedDateItems;
}

export const fieldValidation = (
  value: string,
  inputValue: string,
  type: validationType
): boolean => {
  const { MAX, MIN, REGEX, BETWEEN, NONE } = VALIDATION_TYPE;

  if (type === MAX) {
    return inputValue.length <= parseInt(value, 0);
  } else if (type === MIN) {
    return inputValue.length >= parseInt(value, 0);
  } else if (type === BETWEEN) {
    const values = value.split('-');

    return (
      inputValue.length <= parseInt(values[1], 0) &&
      inputValue.length >= parseInt(values[0], 0)
    );
  } else if (type === REGEX) {
    return !!inputValue.match(value);
  } else if (type === NONE) {
    return true;
  }

  return true;
};

export const getActiveCheckoutStep = (location: H.Location): string => {
  const { pathname } = location;

  const pathNameSplitArray = pathname.split('/');

  return pathNameSplitArray[pathNameSplitArray.length - 1];
};

export const getFieldValidation = (
  field: string,
  value: string,
  config: IFields
): boolean => {
  const specificConfiguration = config[field];

  return isFormFieldValidAsPerType(
    specificConfiguration.validation.value,
    value,
    specificConfiguration.validation.type
  );
};

export const checkIfValidateField = (
  field: string,
  value: string,
  isSearchData: boolean,
  isValid: boolean,
  config: IFields
) => {
  return config[field] && !config[field].mandatory && value.length === 0
    ? true
    : isSearchData
    ? getFieldValidation(field, value, config)
    : isValid;
};
