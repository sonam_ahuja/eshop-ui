import { breakpoints } from '@src/common/variables';
import styled from 'styled-components';
import { Column, Row } from '@common/components/Grid/styles';

export const FormWrapper = styled.form`
  padding: 3.25rem 1.25rem;

  ${Row} {
    margin: 0 -0.5rem;
  }
  ${Column} {
    margin-bottom: 0.4rem;
    padding: 0 0.5rem;

    input {
      font-size: inherit;
    }
  }

  .firstName > div {
    /* margin-right: -1rem; */
  }

  .address {
    width: 100%;

    > span {
      display: block;
      width: 100%;
    }
    > span > div {
      display: block;
      width: 100%;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    ${Row} {
      ${Column} {
        margin-bottom: 0.8125rem;
        .dt_floatLabelInput {
          padding: 0.8125rem 0;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    ${Row} {
      ${Column} {
        margin-bottom: 0.75rem;
        .dt_floatLabelInput {
          padding: 0.75rem 0;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 3.5rem 2.25rem 2.25rem;
    ${Row} {
      ${Column} {
        margin-bottom: 0.8rem;
        .dt_floatLabelInput {
          padding: 0.8rem 0;
        }
      }
    }
  }
`;
