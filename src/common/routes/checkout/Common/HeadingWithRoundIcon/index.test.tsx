import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';

import SectionHeading, { IProps as ISectionHeadingProps } from '.';

describe('<HeadingWithRoundIcon/>', () => {
  const props: ISectionHeadingProps = {
    iconName: 'ec-user-account',
    title: 'title',
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    editKey: CHECKOUT_SUB_ROUTE_TYPE.BILLING,
    editCheckoutStep: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SectionHeading {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('on click should execute navigation', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SectionHeading {...props} />
      </ThemeProvider>
    );
    component.find('div[onClick]').simulate('click');
    component
      .find('div[onKeyDown]')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .simulate('keydown', { keyCode: 13 } as React.KeyboardEvent<Element>);
    component
      .find('div[onKeyDown]')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .simulate('keydown', { keyCode: 12 } as React.KeyboardEvent<Element>);
  });
});
