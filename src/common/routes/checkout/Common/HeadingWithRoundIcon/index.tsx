import React from 'react';
import { Divider, Icon, Paragraph, Section } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';
import { sendEditDetailsEvent } from '@events/checkout/index';

import { LinkWithIcon, RoundIcon, StyledHeadingWithRoundIcon } from './styles';

export interface IProps {
  iconName: string;
  title: string;
  orderReviewTranslation: IOrderReviewTranslation;
  // @TODO: HERE_CHECKOUT_ADDRESSED
  editKey: CHECKOUT_SUB_ROUTE_TYPE;
  translationPath?: string;
  editCheckoutStep(route: CHECKOUT_SUB_ROUTE_TYPE): void;
}

const HeadingWithRoundIcon = (props: IProps) => {
  const {
    orderReviewTranslation,
    editCheckoutStep,
    title,
    iconName,
    editKey,
    translationPath = ''
  } = props;

  const onClickHandler = () => {
    // @TODO: HERE_CHECKOUT_ADDRESSED
    sendEditDetailsEvent(translationPath);
    editCheckoutStep(editKey);
  };
  const onKeyDownEnter = (event: React.KeyboardEvent<Element>): void => {
    if (event.keyCode === 13) {
      onClickHandler();
    }
  };

  return (
    <StyledHeadingWithRoundIcon>
      <div className='textWrap'>
        <RoundIcon name={iconName as iconNamesType} />
        <Paragraph size='large' weight='bold'>
          {title}
        </Paragraph>

        <LinkWithIcon
          onClick={onClickHandler}
          onKeyDown={onKeyDownEnter}
          tabIndex={0}
        >
          <Section size='small' weight='normal'>
            {orderReviewTranslation.edit}
          </Section>
          <Icon color='currentColor' name='ec-edit' />
        </LinkWithIcon>
      </div>

      <Divider dashed />
    </StyledHeadingWithRoundIcon>
  );
};

export default HeadingWithRoundIcon;
