import styled from 'styled-components';
import { Icon } from 'dt-components';
import { breakpoints, colors } from '@common/variables';

export const RoundIcon = styled(Icon)`
  height: 40px;
  width: 40px;
  border-radius: 50%;
  background: ${colors.magenta};
  margin-right: 0.75rem;
  justify-content: center;
  align-items: center;
  font-size: 20px !important;

  path {
    fill: ${colors.white};
  }
`;

export const LinkWithIcon = styled.div`
  display: flex;
  align-items: center;
  color: ${colors.magenta};
  cursor: pointer;
  padding-top: 0.25rem;

  .dt_icon {
    margin-left: 0.5rem;
    font-size: 0.75rem;
  }
`;

export const StyledHeadingWithRoundIcon = styled.div`
  display: flex;
  flex-direction: column;

  .textWrap {
    display: flex;
    align-items: center;
    color: ${colors.ironGray};
  }

  ${LinkWithIcon} {
    justify-self: flex-end;
    margin-left: auto;
  }

  .dt_divider.horizontal {
    margin: 0;
    margin-top: 0.7rem;
    margin-left: auto;
    min-height: 3px;
    color: ${colors.lightGray};
    width: calc(100% - 3.5rem);
    justify-self: flex-end;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .dt_divider.horizontal {
      width: 100%;
    }
  }
`;
