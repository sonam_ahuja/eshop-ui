import React from 'react';
import { Icon } from 'dt-components';

import { StyledInformation } from './styles';

interface IProps {
  informationText?: string;
}

const Information = (props: IProps) => {
  const { informationText } = props;

  return (
    <StyledInformation>
      <div className='iconsInfoWrap'>
        <Icon className='icons' color='currentColor' name='ec-information' />
        <p className='infoText'>{informationText}</p>
      </div>
    </StyledInformation>
  );
};

export default Information;
