import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledInformation = styled.div`
  font-size: 1.25rem;
  line-height: 1.5rem;
  font-weight: bold;
  color: ${colors.ironGray};
  margin-bottom: 3rem;
  .iconsInfoWrap {
    .icons {
      font-size: 1.25rem;
      color: ${colors.mediumGray};
      margin-right: 0.5rem;
      vertical-align: top;
      margin-top: 0.1875rem;
    }
  }
  .infoText {
    display: inline;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .iconsInfoWrap {
      .icons {
        margin-right: 0.25rem;
        font-size: 1rem;
      }
    }
  }
`;
