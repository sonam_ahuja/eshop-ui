import styled from 'styled-components';
import { Column, Row } from '@src/common/components/Grid/styles';
import { breakpoints } from '@src/common/variables';

export const ReadOnlyForm = styled.div`
  ${Row} {
    padding: 0;
  }

  ${Column} {
    position: relative;
    /* margin-bottom: 1.5rem; */
    margin-bottom: 1.75rem;
    word-break: break-word;
    overflow: hidden;
    display: block;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${Column} {
      margin-bottom: 1.85rem;
    }
  }
`;
