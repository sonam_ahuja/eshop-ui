import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SideNavTabs, { IProps } from '@checkout/Common/SideNavTabs';

describe('<SideNavTabs/>', () => {
  const props: IProps = {
    href: '/basket',
    isActive: true,
    translatedValue: '',
    tabIndex: 1,
    onClick: jest.fn(),
    onKeyDown: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <SideNavTabs {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const newProps: IProps = { ...props };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isActive is false', () => {
    const newProps: IProps = { ...props, isActive: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('Simulate native event on side nav navigation', () => {
    const newProps: IProps = { ...props, isActive: false };
    const component = componentWrapper(newProps);
    const clickEventAnchorEl = component.find('a[onClick]');
    const onKeyDownAnchorEl = component.find('a[onKeyDown]');
    clickEventAnchorEl.at(0).simulate('click');
    onKeyDownAnchorEl.at(0).simulate('keyDown');
  });
});
