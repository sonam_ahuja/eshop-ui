import React, { KeyboardEvent, MouseEvent, ReactNode } from 'react';
import { Paragraph } from 'dt-components';

import { StyledTabs } from './styles';

export interface IProps {
  href?: string;
  isActive: boolean;
  translatedValue: string;
  tabIndex: number;
  onClick(event: MouseEvent<HTMLAnchorElement>): void;
  onKeyDown(event: KeyboardEvent<Element>): void;
}

class SideNavTabs extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
    this.onKeyDownHandler = this.onKeyDownHandler.bind(this);
  }

  onClickHandler(event: MouseEvent<HTMLAnchorElement>): void {
    const { onClick } = this.props;
    event.preventDefault();
    onClick(event);
  }

  onKeyDownHandler(event: KeyboardEvent<Element>): void {
    const { onKeyDown } = this.props;
    event.preventDefault();
    onKeyDown(event);
  }

  render(): ReactNode {
    const { tabIndex, isActive, translatedValue, href } = this.props;

    return (
      <StyledTabs
        href={href ? href : ''}
        tabIndex={tabIndex}
        onClick={this.onClickHandler}
        onKeyDown={this.onKeyDownHandler}
        className={isActive ? 'active' : ''}
      >
        <Paragraph size='medium'>{translatedValue}</Paragraph>
      </StyledTabs>
    );
  }
}

export default SideNavTabs;
