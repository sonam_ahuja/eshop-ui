import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const StyledTabs = styled.a`
  border: 1px solid transparent;
  padding: 0.25rem 1rem;
  color: ${colors.ironGray};
  background: ${hexToRgbA(colors.coldGray, 0.6)};
  border-radius: 10px;
  margin-bottom: 0.25rem;
  cursor: pointer;
  text-decoration: none;
  display: block;

  &.active {
    color: ${colors.magenta};
    border-color: ${colors.magenta};
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0.22rem 1rem;
    margin-bottom: 0.2rem;
  }
`;
