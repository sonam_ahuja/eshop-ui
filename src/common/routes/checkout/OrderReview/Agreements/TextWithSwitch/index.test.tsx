import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import TextWithSwitch, { IProps } from '.';

describe('<OrderReview TextWithSwitch/>', () => {
  const props: IProps = {
    titleName: '',
    isChecked: true,
    onToggle: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <TextWithSwitch {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('execute action', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <TextWithSwitch {...props} />
      </ThemeProvider>
    );
    component.find('span[onClick]').simulate('click');
  });

  test('should render properly, without onToggle', () => {
    const newProps: IProps = { ...props, onToggle: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <TextWithSwitch {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
