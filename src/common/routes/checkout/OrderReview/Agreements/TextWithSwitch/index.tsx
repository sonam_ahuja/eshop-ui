import React from 'react';
import { Switch, Title } from 'dt-components';

import { StyledTextWithSwitch } from './styles';

export interface IProps {
  titleName: string;
  isChecked: boolean;
  onToggle?(): void;
}

const TextWithSwitch = (props: IProps) => {
  const onClickHandler = () => {
    if (props.onToggle) {
      props.onToggle();
    }
  };

  return (
    <StyledTextWithSwitch>
      <Title size='xsmall' weight='bold'>
        {/* Receive professional product and service information from Telekom. */}
        {props.titleName}
      </Title>
      <Switch on={props.isChecked} disabled={false} onToggle={onClickHandler} />
    </StyledTextWithSwitch>
  );
};

export default TextWithSwitch;
