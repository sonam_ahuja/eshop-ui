import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledTextWithSwitch = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 2.5rem;
  align-items: flex-start;

  .dt_title {
    padding-right: 2.25rem;
  }
  .dt_switch {
    margin-top: 3px;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: auto;
    .dt_title {
      padding-right: 2rem;
    }
  }
`;
