import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Agreements, { IProps } from '@checkout/OrderReview/Agreements';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {
  AGREEMENT_PRIVACY_GROUP,
  IAgreementResponse,
  IOrderReviewState
} from '@checkout/store/orderReview/types';
import { StaticRouter } from 'react-router-dom';

describe('<Agreements/>', () => {
  const props: IProps = {
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    orderReview: appState().checkout.orderReview,
    orderReviewConfiguration: appState().configuration.cms_configuration.modules
      .checkout.orderReview,
    innerAgreementToggle: jest.fn(),
    mainAgreementToggle: jest.fn(),
    setTelekomService: jest.fn(),
    showTermAndConditions: jest.fn(),
    downloadPdf: jest.fn(),
    agreementShouldTermsAndConditionsOpenInNewTab: true,
    agreementTermsAndConditionsUrl: 'string',
    mainCheckboxClick: jest.fn(),
    fetchDiscount: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Agreements {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, agreement response', () => {
    const data: IAgreementResponse[] = [
      {
        description: '',
        version: '1',
        documentationUrls: [
          {
            name: 'string',
            relativeUrl: 'string',
            absoluteUrl: 'string'
          }
        ],
        id: '1',
        name: 'name',
        privacyGroup: AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
        partyPrivacyProfileTypeCharacteristics: [
          {
            id: '2',
            name: 'name',
            description: 'description',
            documentationUrl: '/url',
            privacyUsagePurpose: 'privacy',
            privacyType: 'privacyType',
            isAuthorized: true
          }
        ],
        subConsents: [
          {
            description: '',
            version: '1',
            documentationUrls: [
              {
                name: 'string',
                relativeUrl: 'string',
                absoluteUrl: 'string'
              }
            ],
            id: '1',
            name: 'name',
            privacyGroup: AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
            partyPrivacyProfileTypeCharacteristics: [
              {
                id: '2',
                name: 'name',
                description: 'description',
                documentationUrl: '/url',
                privacyUsagePurpose: 'privacy',
                privacyType: 'privacyType',
                isAuthorized: true
              }
            ]
          }
        ]
      },
      {
        description: '',
        version: '1',
        documentationUrls: [
          {
            name: 'string',
            relativeUrl: 'string',
            absoluteUrl: 'string'
          }
        ],
        id: '2',
        name: 'name',
        privacyGroup: AGREEMENT_PRIVACY_GROUP.TERMS_OF_SERVICE,
        partyPrivacyProfileTypeCharacteristics: [
          {
            id: '2',
            name: 'name',
            description: 'description',
            documentationUrl: '/url',
            privacyUsagePurpose: 'privacy',
            privacyType: 'privacyType',
            isAuthorized: true
          }
        ],
        subConsents: [
          {
            description: '',
            version: '1',
            documentationUrls: [
              {
                name: 'string',
                relativeUrl: '',
                absoluteUrl: 'string'
              }
            ],
            id: '1',
            name: 'name',
            privacyGroup: AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
            partyPrivacyProfileTypeCharacteristics: [
              {
                id: '2',
                name: 'name',
                description: 'description',
                documentationUrl: '/url',
                privacyUsagePurpose: 'privacy',
                privacyType: 'privacyType',
                isAuthorized: true
              }
            ]
          }
        ]
      }
    ];
    const orderReview: IOrderReviewState = {
      ...appState().checkout.orderReview,
      agreementResponse: data
    };
    const newProps: IProps = { ...props };
    newProps.orderReview = orderReview;
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Agreements {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();

    (component.find('Agreements').instance() as Agreements).changeState();
    component.update();
    component.setProps(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should update state properly', () => {
    const initStateValue = appState();
    const mockStore = configureStore();
    const store = mockStore(initStateValue);
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Agreements {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('Agreements').instance() as Agreements).handleToggle(
      AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
      'selectedId'
    )();
    (component.find('Agreements').instance() as Agreements).handleInnerToggle(
      1,
      2
    )();
    (component.find('Agreements').instance() as Agreements).relativePdfUrlClick(
      'string'
    )();
    (component
      .find('Agreements')
      .instance() as Agreements).handleMainCheckboxClick()();
    (component.find('Agreements').instance() as Agreements).changeState();
  });
});
