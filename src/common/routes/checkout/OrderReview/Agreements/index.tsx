import React, { Component, ReactNode } from 'react';
import { Accordion, Anchor, Checkbox, Icon, Title } from 'dt-components';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { StyledAccordionWithCheckbox } from '@src/common/components/Accordion/AccordionWithCheckbox/styles';
import {
  AGREEMENT_PRIVACY_GROUP,
  IAgreementResponse,
  IOrderReviewState,
  ISetTelekomType
} from '@checkout/store/orderReview/types';
import EVENT_NAME from '@events/constants/eventName';
import { IOrderReview } from '@store/types/configuration';
import appConstants from '@src/common/constants/appConstants';

import {
  StyledAgreements,
  StyledHeadingWithCheckbox,
  StyledSpan,
  TncWrap
} from './styles';

export interface IProps {
  orderReviewTranslation: IOrderReviewTranslation;
  orderReview: IOrderReviewState;
  agreementShouldTermsAndConditionsOpenInNewTab: boolean;
  orderReviewConfiguration: IOrderReview;
  agreementTermsAndConditionsUrl: string;
  setTelekomService(data: ISetTelekomType): void;
  innerAgreementToggle(outerIndex: number, innerIndex: number): void;
  mainAgreementToggle(data: number): void;
  showTermAndConditions(): void;
  mainCheckboxClick(): void;
  downloadPdf(url: string): void;
  fetchDiscount(): void;
}

export interface IState {
  isActive: boolean;
}
class Agreements extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isActive: false
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleInnerToggle = this.handleInnerToggle.bind(this);
    this.handleOuterToggle = this.handleOuterToggle.bind(this);
    this.relativePdfUrlClick = this.relativePdfUrlClick.bind(this);
    this.handleMainCheckboxClick = this.handleMainCheckboxClick.bind(this);
    this.changeState = this.changeState.bind(this);
  }

  componentDidMount(): void {
    const { orderReviewConfiguration } = this.props;

    if (orderReviewConfiguration.hideAllConsentBtn) {
      this.setState({
        isActive: true
      });
    } else {
      this.setState({
        isActive: orderReviewConfiguration.expandConsentByDefault
      });
    }
  }

  handleToggle(type: AGREEMENT_PRIVACY_GROUP, selectedId: string): () => void {
    return () => {
      this.props.setTelekomService({
        type,
        id: selectedId
      });
    };
  }

  handleInnerToggle(consentId: number, selectedId: number): () => void {
    return () => {
      this.props.innerAgreementToggle(consentId, selectedId);
    };
  }

  handleOuterToggle(
    consentId: number,
    agreement: IAgreementResponse
  ): () => void {
    return () => {
      this.props.mainAgreementToggle(consentId);
      if (agreement.discountApplicable) {
        this.props.fetchDiscount();
      }
    };
  }

  relativePdfUrlClick(url: string): () => void {
    return () => {
      this.props.downloadPdf(url);
    };
  }

  handleMainCheckboxClick(): () => void {
    return () => {
      this.props.mainCheckboxClick();

      if (this.isDiscountApplicable()) {
        this.props.fetchDiscount();
      }
    };
  }

  isDiscountApplicable = (): boolean => {
    const { agreementResponse } = this.props.orderReview;
    if (agreementResponse.length > 0) {
      return (
        agreementResponse.filter(agreement => {
          return !!agreement.discountApplicable;
        }).length > 0
      );
    }

    return false;
  }

  changeState(): void {
    this.setState({
      isActive: !this.state.isActive
    });
  }

  // tslint:disable-next-line: cognitive-complexity
  render(): ReactNode {
    const {
      showTermAndConditions,
      orderReview: {
        agreeOnTermsAndCondition,
        agreementResponse,
        mainMarketingCheckbox
      },
      orderReviewTranslation: { marketingAgreements, agree, termsAndCondition },
      orderReviewConfiguration
    } = this.props;

    const { isActive } = this.state;

    return (
      <StyledAgreements>
        {!orderReviewConfiguration.hideAllConsentBtn ? (
          <StyledHeadingWithCheckbox
            onClick={this.changeState}
            className={isActive ? 'active' : ''}
          >
            <Checkbox
              checked={mainMarketingCheckbox}
              disabled={false}
              name='checkbox'
              focused={false}
              onClick={this.handleMainCheckboxClick()}
              size='large'
            />
            <div className='iconWrap'>
              <Icon
                color='currentColor'
                size='inherit'
                name='ec-plus'
                className='iconPlus'
              />
              <Icon
                color='currentColor'
                size='inherit'
                name='ec-minus'
                className='iconMinus'
              />
            </div>
            <Title size='xsmall' weight='bold'>
              {marketingAgreements}
            </Title>
          </StyledHeadingWithCheckbox>
        ) : null}
        {isActive &&
          agreementResponse.map((agreement: IAgreementResponse, i: number) => {
            return (
              <StyledAccordionWithCheckbox key={i}>
                <Accordion
                  isOpen={orderReviewConfiguration.expandConsentByDefault}
                  headerTitle={
                    <StyledSpan
                      data-event-id={EVENT_NAME.CHECKOUT.EVENTS.AGREEMENT}
                      data-event-message={agreement.description}
                    >
                      <Checkbox
                        checked={
                          agreement.partyPrivacyProfileTypeCharacteristics[0]
                            .isAuthorized
                        }
                        disabled={false}
                        name='checkbox'
                        focused={false}
                        onClick={this.handleOuterToggle(i, agreement)}
                        size='large'
                      />
                      <div key={agreement.id} className='text'>
                        {agreement.description}
                      </div>
                    </StyledSpan>
                  }
                >
                  {agreement.subConsents && (
                    <div className='accordionBody'>
                      {agreement.subConsents.map(
                        (subConsentItem: IAgreementResponse, index: number) => {
                          return (
                            <div
                              key={subConsentItem.id}
                              className='textWrapper'
                            >
                              <Checkbox
                                checked={
                                  subConsentItem
                                    .partyPrivacyProfileTypeCharacteristics[0]
                                    .isAuthorized
                                }
                                disabled={false}
                                name='checkbox'
                                focused={false}
                                onClick={this.handleInnerToggle(i, index)}
                                size='large'
                              />
                              <div className='text'>
                                {subConsentItem.description}{' '}
                                {subConsentItem.documentationUrls &&
                                subConsentItem.documentationUrls[0] &&
                                subConsentItem.documentationUrls[0]
                                  .relativeUrl ? (
                                  <a
                                    className='link'
                                    onClick={this.relativePdfUrlClick(
                                      subConsentItem.documentationUrls[0]
                                        .relativeUrl
                                    )}
                                  >
                                    {subConsentItem.documentationUrls &&
                                      subConsentItem.documentationUrls[0] &&
                                      subConsentItem.documentationUrls[0].name}
                                  </a>
                                ) : (
                                  <a
                                    className='link'
                                    href={
                                      subConsentItem.documentationUrls &&
                                      subConsentItem.documentationUrls[0]
                                        ? subConsentItem.documentationUrls[0]
                                            .absoluteUrl
                                        : ''
                                    }
                                  >
                                    {subConsentItem.documentationUrls &&
                                      subConsentItem.documentationUrls[0] &&
                                      subConsentItem.documentationUrls[0].name}
                                  </a>
                                )}
                              </div>
                            </div>
                          );
                        }
                      )}
                    </div>
                  )}
                </Accordion>
              </StyledAccordionWithCheckbox>
            );
          })}

        {
          <TncWrap key={'1'}>
            <Checkbox
              checked={agreeOnTermsAndCondition}
              disabled={false}
              name='checkbox'
              focused={false}
              onClick={this.handleToggle(
                AGREEMENT_PRIVACY_GROUP.TERMS_OF_SERVICE,
                '1'
              )}
              size='large'
              className=''
            />
            <Title size='xsmall' weight='bold'>
              {agree}{' '}
              {orderReviewConfiguration.isTermsAndConditionCMSDriven &&
              orderReviewConfiguration.shouldTermsAndConditionsOpenInNewTab ? (
                <Anchor
                  size='medium'
                  hreflang={appConstants.LANGUAGE_CODE}
                  title={termsAndCondition}
                  target={
                    orderReviewConfiguration.shouldTermsAndConditionsOpenInNewTab
                      ? '_blank'
                      : '_self'
                  }
                  href={orderReviewConfiguration.termsAndConditionsUrl}
                  data-event-id={EVENT_NAME.CHECKOUT.EVENTS.TERMS_CONDITION}
                  data-event-message={termsAndCondition}
                  data-event-path={
                    'cart.checkout.orderReview.termsAndCondition'
                  }
                >
                  {termsAndCondition}
                </Anchor>
              ) : (
                <Anchor
                  size='medium'
                  hreflang={appConstants.LANGUAGE_CODE}
                  title={termsAndCondition}
                  onClickHandler={() => showTermAndConditions()}
                  data-event-id={EVENT_NAME.CHECKOUT.EVENTS.TERMS_CONDITION}
                  data-event-message={termsAndCondition}
                  data-event-path={
                    'cart.checkout.orderReview.termsAndCondition'
                  }
                >
                  {termsAndCondition}
                </Anchor>
              )}
            </Title>
          </TncWrap>
        }
      </StyledAgreements>
    );
  }
}

// tslint:disable-next-line: max-file-line-count
export default Agreements;
