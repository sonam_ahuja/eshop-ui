import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const TncWrap = styled.div`
  display: flex;
  align-items: center;
  padding-top: 1.25rem;
  margin-top: -1px;
  border-top: 1px solid ${colors.lightGray};

  .dt_title {
    a {
      display: block;
      font-size: inherit;
    }
  }
  .dt_checkbox {
    margin-right: 1.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .dt_title {
      a {
        display: inline-flex;
      }
    }
  }
`;

export const StyledHeadingWithCheckbox = styled.div`
  display: flex;
  .dt_checkbox {
    margin-right: 1.25rem;
  }
  .dt_title {
    align-self: center;
  }
  padding-bottom: 1.25rem;
  position: relative;

  &:after {
    content: '';
    position: absolute;
    width: calc(100% - 40px - 1.25rem);
    height: 1px;
    bottom: 0;
    right: 0;
    background: ${colors.lightGray};
  }

  .iconWrap {
    position: absolute;
    right: 0;
    top: 0.75rem;
    font-size: 20px;

    .iconPlus {
      display: block;
    }
    .iconMinus {
      display: none;
    }
  }
  &.active {
    margin-bottom: 0.5rem;
    .iconWrap {
      .iconPlus {
        display: none;
      }
      .iconMinus {
        display: block;
      }
    }
  }
`;

export const StyledAgreements = styled.div`
  color: ${colors.mediumGray};
  margin-top: 3rem;

  .heading {
    margin-bottom: 1.25rem;
    color: ${colors.ironGray};
  }

  @media (min-width: ${breakpoints.desktop}px) {
  }
`;
export const StyledSpan = styled.div`
  display: flex;
`;
