import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import Header, { IProps } from '.';

describe('<Order Review Shipping />', () => {
  const props: IProps = {
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Header {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
