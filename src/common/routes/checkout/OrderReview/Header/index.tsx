import React, { FunctionComponent } from 'react';
import { Anchor, Icon, Title } from 'dt-components';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import appConstants from '@src/common/constants/appConstants';

import { StyledHeader } from './styles';

export interface IProps {
  orderReviewTranslation: IOrderReviewTranslation;
}

export const Header: FunctionComponent<IProps> = (props: IProps) => {
  return (
    <StyledHeader>
      <Anchor hreflang={appConstants.LANGUAGE_CODE} href='/' underline={false}>
        <Icon className='logo' name='ec-dt-logo' size='large' />
      </Anchor>
      <Title size='xsmall' weight='bold'>
        {props.orderReviewTranslation.orderReviewText}
      </Title>
    </StyledHeader>
  );
};

export default Header;
