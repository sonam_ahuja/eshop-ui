import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${colors.black};
  height: 5.5rem;
  padding: 1.25rem 1.25rem 1.25rem 1.625rem;

  .dt_icon {
    font-size: 3.5rem;
  }

  .dt_title {
    color: ${colors.white};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    background: ${colors.white};
    height: 7.25rem;
    padding: 0 2.25rem 0 3rem;

    .dt_title {
      color: ${colors.darkGray};
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-left: 3rem;
    padding-right: 3rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    height: 5.75rem;
    padding: 0 4.9rem;

    .dt_icon {
      font-size: 3.5rem;
    }

    .dt_title {
      font-size: 1.5rem;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.17;
      letter-spacing: normal;
      color: ${colors.darkGray};
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 0 5.5rem;
  }
`;
