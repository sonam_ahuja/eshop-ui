import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import IState from '@basket/store/state';
import { StaticRouter } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import { Provider } from 'react-redux';
import appState from '@store/states/app';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

import Order, { IProps as IOrderProps } from '.';

describe('<Order />', () => {
  const newBasket = { ...IState() };
  newBasket.cartId = '12345';
  newBasket.showAppShell = true;
  const mockStore = configureStore();

  const componentProps: IOrderProps = {
    basketItems: BASKET_ALL_ITEM.cartItems,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    showMore: true,
    switchViewMore: jest.fn()
  };
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Order {...componentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });
});
