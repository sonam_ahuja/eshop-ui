import React, { FunctionComponent } from 'react';
import { Paragraph } from 'dt-components';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import BasketWrap from '@basket/BasketWrap';
import { IBasketItem } from '@basket/store/types';

import { StyledOrder } from './styles';

export interface IProps {
  orderReviewTranslation: IOrderReviewTranslation;
  showMore: boolean;
  basketItems: IBasketItem[];
  switchViewMore(): void;
}
const Order: FunctionComponent<IProps> = (props: IProps) => {
  const { order, viewLess, viewMore } = props.orderReviewTranslation;
  const { showMore, switchViewMore, basketItems } = props;

  return (
    <StyledOrder>
      <Paragraph className='orderHeading' size='large' weight='bold'>
        {order}
      </Paragraph>
      <BasketWrap isStepperDisable={true} showMoreFlag={showMore} />
      {basketItems && basketItems.length > 1 ? (
        <div className='viewAllBtn' onClick={switchViewMore}>
          {showMore ? viewLess : viewMore}
        </div>
      ) : null}
    </StyledOrder>
  );
};

export default Order;
