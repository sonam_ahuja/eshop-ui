import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledOrder = styled.div`
  padding-top: 1.75rem;

  .orderHeading {
    margin-bottom: 2rem;
    color: ${colors.ironGray};
  }

  .viewAllBtn {
    font-size: 0.875rem;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;

    height: 2.25rem;
    border-radius: 0.5rem;
    background: ${colors.cloudGray};
    color: ${colors.mediumGray};
    display: flex;
    align-items: center;
    justify-content: center;

    cursor: pointer;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-top: 1.5rem;
    .orderHeading {
      color: ${colors.darkGray};
    }
  }
`;
