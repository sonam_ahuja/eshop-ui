import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import BillingForm, { IProps as IBillingFormProps } from '.';
import BillingFormBackup from './backup';

describe('< BillingForm />', () => {
  const props: IBillingFormProps = {
    billingInfo: appState().checkout.billing,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  test('should render properly with default address', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <BillingForm {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with default address for backup file', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <BillingFormBackup {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
