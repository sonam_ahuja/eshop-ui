import React, { FunctionComponent } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { Section } from 'dt-components';
import KeyValue from '@src/common/components/KeyValue';

import { IBillingState } from '../../../store/billing/types';

import { StyledBilling } from './styles';

export interface IProps {
  billingInfo: IBillingState;
  orderReviewTranslation: IOrderReviewTranslation;
}

const BillingForm: FunctionComponent<IProps> = (props: IProps) => {
  const {
    billing,
    // address,
    billingOption,
    address
  } = props.orderReviewTranslation.paymentBilling;

  const {
    typeOfBill,
    streetAddress,
    flatNumber,
    city,
    postCode,
    streetNumber
  } = props.billingInfo;

  return (
    <StyledBilling>
      <Row>
        <Column className='subHeading' colMobile={8} colTabletLandscape={4}>
          <Section size='small' weight='normal' transform='uppercase'>
            {billing}
          </Section>
        </Column>
        <Column className='address' colMobile={12} colTabletLandscape={6}>
          <KeyValue
            labelName={address}
            labelValue={`${flatNumber} ${streetAddress} ${streetNumber} ${city} ${postCode}`}
          />
        </Column>
        <Column className='billingOption' colMobile={12} colTabletLandscape={2}>
          <KeyValue labelName={billingOption} labelValue={typeOfBill} />
        </Column>
      </Row>
    </StyledBilling>
  );
};

export default BillingForm;
