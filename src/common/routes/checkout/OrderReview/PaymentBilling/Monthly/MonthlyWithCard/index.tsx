import React, { FunctionComponent } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import { ICardDetails } from '@checkout/store/payment/types';
import KeyValue from '@src/common/components/KeyValue';
import { Section } from 'dt-components';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';

import { StyledMonthly } from './styles';

export interface IProps {
  monthlyCardDetail: ICardDetails;
  orderReviewTranslation: IOrderReviewTranslation;
}

const MonthlyWithCard: FunctionComponent<IProps> = (props: IProps) => {
  const {
    monthly: monthlyTxt,
    cardNumber,
    nameOnCard,
    expirationDate
  } = props.orderReviewTranslation.paymentBilling;
  const { monthlyCardDetail } = props;

  return (
    <StyledMonthly>
      <Row>
        <Column
          className='subHeading'
          colMobile={12}
          colTabletLandscape={4}
          orderDesktop={0}
        >
          <Section size='small' weight='normal' transform='uppercase'>
            {monthlyTxt}
          </Section>
        </Column>
        <Column
          className='cardNumber'
          colMobile={7}
          colTabletPortrait={5}
          colTabletLandscape={3}
          orderDesktop={1}
        >
          <KeyValue
            labelName={cardNumber}
            labelValue={monthlyCardDetail.cardNumber}
          />
        </Column>
        <Column
          className='expirationDate'
          colMobile={5}
          colTabletPortrait={7}
          colTabletLandscape={2}
          orderDesktop={3}
        >
          <KeyValue
            labelName={expirationDate}
            labelValue={monthlyCardDetail.expiryDate}
          />
        </Column>
        <Column
          className='nameOnCard'
          colMobile={12}
          colTabletLandscape={3}
          orderDesktop={2}
        >
          <KeyValue
            labelName={nameOnCard}
            labelValue={monthlyCardDetail.nameOnCard}
          />
        </Column>
      </Row>
    </StyledMonthly>
  );
};

export default MonthlyWithCard;
