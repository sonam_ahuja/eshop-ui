import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

import MonthlyWithoutCard, { IProps } from '.';

describe('<Order Review UpFront />', () => {
  const props: IProps = {
    paymentTypeSelected: appState().checkout.payment.monthly
      .paymentTypeSelected,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  test('should render properly with default card info', () => {
    const newProps = { ...props };
    newProps.paymentTypeSelected = PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    const component = mount(
      <ThemeProvider theme={{}}>
        <MonthlyWithoutCard {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('show render proporly with flag true', () => {
    const newProps = { ...props };
    newProps.paymentTypeSelected = PAYMENT_TYPE.MANUAL_PAYMENTS;
    const component = mount(
      <ThemeProvider theme={{}}>
        <MonthlyWithoutCard {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
