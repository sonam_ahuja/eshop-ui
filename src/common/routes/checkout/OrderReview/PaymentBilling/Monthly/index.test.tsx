import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

import Monthly, { IProps as IMonthlyProps } from '.';

describe('<Order Review Monthly />', () => {
  const card = {
    cardNumber: '',
    isDefault: false,
    cardType: '',
    expiryDate: '',
    securityCode: '',
    nameOnCard: '',
    id: '0',
    isCardSaved: false,
    lastFourDigits: '',
    brand: ''
  };
  const props: IMonthlyProps = {
    monthlyCardDetails: appState().checkout.payment.monthly,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  test('should render properly with default card info', () => {
    const newProps = { ...props };
    newProps.monthlyCardDetails.cardDetails = [card];
    const component = mount(
      <ThemeProvider theme={{}}>
        <Monthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without default card info', () => {
    const newProps = { ...props };
    newProps.monthlyCardDetails.cardDetails = [card];
    newProps.monthlyCardDetails.cardDetails[0].isDefault = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Monthly {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('show render properly with payment different', () => {
    const newProps = { ...props };
    newProps.monthlyCardDetails.paymentTypeSelected = PAYMENT_TYPE.PAY_BY_LINK;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Monthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
