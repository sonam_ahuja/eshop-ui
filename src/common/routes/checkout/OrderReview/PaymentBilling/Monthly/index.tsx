import React, { FunctionComponent } from 'react';
import { ICardDetails, IMonthly } from '@checkout/store/payment/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { PAYMENT_TYPE } from '@checkout/store/enums';

import MonthlyWithoutCard from './MonthlyWithoutCard';
import MonthlyWithCard from './MonthlyWithCard';

export interface IProps {
  monthlyCardDetails: IMonthly;
  orderReviewTranslation: IOrderReviewTranslation;
}

const Monthly: FunctionComponent<IProps> = (props: IProps) => {
  const { monthlyCardDetails } = props;

  if (
    monthlyCardDetails.paymentTypeSelected !== PAYMENT_TYPE.CREDIT_DEBIT_CARD
  ) {
    return (
      <MonthlyWithoutCard
        paymentTypeSelected={monthlyCardDetails.paymentTypeSelected}
        orderReviewTranslation={props.orderReviewTranslation}
      />
    );
  } else {
    return (
      <>
        {monthlyCardDetails.cardDetails.map((card: ICardDetails) => {
          if (card.isDefault) {
            return (
              <MonthlyWithCard
                monthlyCardDetail={card}
                orderReviewTranslation={props.orderReviewTranslation}
              />
            );
          }

          return null;
        })}
      </>
    );
  }
};

export default Monthly;
