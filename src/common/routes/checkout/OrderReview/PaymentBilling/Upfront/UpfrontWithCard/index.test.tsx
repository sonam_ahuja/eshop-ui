import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import UpfrontWithCard, { IProps } from '.';

describe('<Order Review UpFront />', () => {
  const card = {
    cardNumber: '2121',
    isDefault: false,
    cardType: '',
    expiryDate: '',
    securityCode: '',
    nameOnCard: '',
    id: '0',
    isCardSaved: false,
    lastFourDigits: '',
    brand: ''
  };
  const props: IProps = {
    upfrontCardDetail: card,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  test('should render properly with default card info', () => {
    const newProps = { ...props };
    newProps.upfrontCardDetail = card;
    const component = mount(
      <ThemeProvider theme={{}}>
        <UpfrontWithCard {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('show render proporly with flag true', () => {
    const newProps = { ...props };
    newProps.upfrontCardDetail = card;
    newProps.upfrontCardDetail.isDefault = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <UpfrontWithCard {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
