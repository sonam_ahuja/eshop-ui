import React, { FunctionComponent } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import { ICardDetails } from '@checkout/store/payment/types';
import KeyValue from '@src/common/components/KeyValue';
import { Section } from 'dt-components';
import { IOrderReviewTranslation } from '@store/types/translation';

import { StyledUpfront } from './styles';

export interface IProps {
  upfrontCardDetail: ICardDetails;
  orderReviewTranslation: IOrderReviewTranslation;
}

const UpfrontWithCard: FunctionComponent<IProps> = (props: IProps) => {
  const {
    upfront: upFrontText,
    cardNumber,
    nameOnCard,
    expirationDate
  } = props.orderReviewTranslation.paymentBilling;
  const { upfrontCardDetail } = props;

  return (
    <StyledUpfront>
      <Row>
        <Column
          className='subHeading'
          colMobile={12}
          colTabletLandscape={4}
          orderDesktop={0}
        >
          <Section size='small' weight='normal' transform='uppercase'>
            {upFrontText}
          </Section>
        </Column>
        <Column
          className='cardNumber'
          colMobile={7}
          colTabletPortrait={5}
          colTabletLandscape={3}
          orderDesktop={1}
        >
          <KeyValue
            labelName={cardNumber}
            labelValue={upfrontCardDetail.cardNumber}
          />
        </Column>
        <Column
          className='expirationDate'
          colMobile={5}
          colTabletPortrait={5}
          colTabletLandscape={2}
          orderDesktop={3}
        >
          <KeyValue
            labelName={expirationDate}
            labelValue={upfrontCardDetail.expiryDate}
          />
        </Column>
        <Column
          className='nameOnCard'
          colMobile={12}
          colTabletLandscape={3}
          orderDesktop={2}
        >
          <KeyValue
            labelName={nameOnCard}
            labelValue={upfrontCardDetail.nameOnCard}
          />
        </Column>
      </Row>
    </StyledUpfront>
  );
};

export default UpfrontWithCard;
