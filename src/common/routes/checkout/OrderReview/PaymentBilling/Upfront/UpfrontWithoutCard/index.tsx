import React, { FunctionComponent } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import KeyValue from '@src/common/components/KeyValue';
import { Divider, Section } from 'dt-components';
import {
  IBillingInfoTranslation,
  IOrderReviewTranslation
} from '@src/common/store/types/translation';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';
import LinkWithIcon from '@src/common/components/LinkWithIcon';

import { StyledUpfront } from './styles';

export interface IProps {
  paymentTypeSelected: PAYMENT_TYPE;
  orderReviewTranslation: IOrderReviewTranslation;
  isEditNeeded?: boolean;
  isBillingSection?: boolean;
  title?: string;
  paymentType?: string;
  billingTranslation?: IBillingInfoTranslation;
  onClickHandler?(data: string): void;
}

const UpfrontWithoutCard: FunctionComponent<IProps> = (props: IProps) => {
  const {
    upfront: upFrontText,
    paymentModeSelected,
    paymentModeText
  } = props.orderReviewTranslation.paymentBilling;
  const {
    paymentTypeSelected,
    isEditNeeded,
    isBillingSection,
    title,
    onClickHandler,
    paymentType
  } = props;
  let cardDetail = {
    cardNumber: '',
    nameOnCard: '',
    expirationDate: '',
    edit: ''
  };

  if (props.billingTranslation) {
    cardDetail = props.billingTranslation.cardDetail;
  }

  const onClick = (): void => {
    if (onClickHandler) {
      onClickHandler(paymentType as string);
    }
  };

  return (
    <StyledUpfront>
      {isBillingSection && (
        <div className='heading'>
          <div className='textWrap'>
            <Section size='small' transform='uppercase'>
              {title}
            </Section>
            {isEditNeeded && (
              <Section size='small'>
                <LinkWithIcon text={cardDetail.edit} onClick={onClick} />
              </Section>
            )}
          </div>
          <Divider dashed orientation='horizontal' />
        </div>
      )}
      <Row>
        <Column
          className='subHeading'
          colMobile={12}
          colTabletLandscape={4}
          orderDesktop={0}
        >
          <Section size='small' weight='normal' transform='uppercase'>
            {upFrontText}
          </Section>
        </Column>
        <Column
          className='cardNumber'
          colMobile={7}
          colTabletPortrait={5}
          colTabletLandscape={3}
          orderDesktop={1}
        >
          <KeyValue
            labelName={paymentModeSelected}
            labelValue={paymentModeText[paymentTypeSelected]}
          />
        </Column>
      </Row>
    </StyledUpfront>
  );
};

export default UpfrontWithoutCard;
