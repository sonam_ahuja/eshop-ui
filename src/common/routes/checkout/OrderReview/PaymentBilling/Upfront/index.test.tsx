import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

import UpFront, { IProps } from '.';

describe('<Order Review UpFront />', () => {
  const card = {
    cardNumber: '',
    isDefault: false,
    cardType: '',
    expiryDate: '',
    securityCode: '',
    nameOnCard: '',
    id: '0',
    isCardSaved: false,
    lastFourDigits: '',
    brand: ''
  };
  const props: IProps = {
    upFrontCardDetails: appState().checkout.payment.upFront,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  test('should render properly with default card info', () => {
    const newProps = { ...props };
    newProps.upFrontCardDetails.cardDetails = [card];
    const component = mount(
      <ThemeProvider theme={{}}>
        <UpFront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('show render properly with flag true', () => {
    const newProps = { ...props };
    newProps.upFrontCardDetails.cardDetails = [card];
    newProps.upFrontCardDetails.cardDetails[0].isDefault = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <UpFront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('show render properly with payment different', () => {
    const newProps = { ...props };
    newProps.upFrontCardDetails.paymentTypeSelected = PAYMENT_TYPE.PAY_BY_LINK;
    const component = mount(
      <ThemeProvider theme={{}}>
        <UpFront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
