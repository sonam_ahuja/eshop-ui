import React, { FunctionComponent } from 'react';
import { ICardDetails, IUpFront } from '@checkout/store/payment/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { PAYMENT_TYPE } from '@checkout/store/enums';

import UpfrontWithoutCard from './UpfrontWithoutCard';
import UpfrontWithCard from './UpfrontWithCard';

export interface IProps {
  upFrontCardDetails: IUpFront;
  orderReviewTranslation: IOrderReviewTranslation;
}

const UpFront: FunctionComponent<IProps> = (props: IProps) => {
  const { upFrontCardDetails } = props;

  if (
    upFrontCardDetails.paymentTypeSelected !== PAYMENT_TYPE.CREDIT_DEBIT_CARD
  ) {
    return (
      <UpfrontWithoutCard
        paymentTypeSelected={upFrontCardDetails.paymentTypeSelected}
        orderReviewTranslation={props.orderReviewTranslation}
      />
    );
  } else {
    return (
      <>
        {upFrontCardDetails.cardDetails.map((card: ICardDetails) => {
          if (card.isDefault) {
            return (
              <UpfrontWithCard
                upfrontCardDetail={card}
                orderReviewTranslation={props.orderReviewTranslation}
              />
            );
          }

          return null;
        })}
      </>
    );
  }
};

export default UpFront;
