import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import PaymentBilling, { IProps as IPaymentBillingProps } from '.';

describe('<Order Review Order />', () => {
  const props: IPaymentBillingProps = {
    billingInfo: appState().checkout.billing,
    paymentInfo: appState().checkout.payment,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    editCheckoutStep: jest.fn(),
    isMonthlyPrice: false,
    isUpfrontPrice: true,
    billingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.billingInfo,
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentBilling {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
