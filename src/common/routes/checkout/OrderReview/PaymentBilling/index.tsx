import Billing from '@checkout/OrderReview/PaymentBilling/Billing';
import { IPaymentState } from '@checkout/store/payment/types';
import { Divider } from 'dt-components';
import Upfront from '@checkout/OrderReview/PaymentBilling/Upfront';
import Monthly from '@checkout/OrderReview/PaymentBilling/Monthly';
import React, { FunctionComponent } from 'react';
import { IBillingState } from '@checkout/store/billing/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import HeadingWithRoundIcon from '@checkout/Common/HeadingWithRoundIcon';
import { ReadOnlyForm } from '@checkout/Common/ReadOnlyForm/styles';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';
import { IBillingConfiguration } from '@src/common/store/types/configuration';

import { StyledPaymentBilling } from './styles';

export interface IProps {
  billingInfo: IBillingState;
  paymentInfo: IPaymentState;
  orderReviewTranslation: IOrderReviewTranslation;
  isMonthlyPrice: boolean;
  isUpfrontPrice: boolean;
  billingConfiguration: IBillingConfiguration;
  editCheckoutStep(route: CHECKOUT_SUB_ROUTE_TYPE): void;
}

const PaymentBilling: FunctionComponent<IProps> = (props: IProps) => {
  const {
    orderReviewTranslation,
    editCheckoutStep,
    isMonthlyPrice,
    isUpfrontPrice,
    billingConfiguration
  } = props;
  const { upFront, monthly } = props.paymentInfo;
  const { paymentBillingTxt } = props.orderReviewTranslation.paymentBilling;

  return (
    <StyledPaymentBilling>
      <HeadingWithRoundIcon
        iconName='ec-credit-card'
        title={paymentBillingTxt}
        // @TODO: HERE_CHECKOUT_ADDRESSED
        editKey={CHECKOUT_SUB_ROUTE_TYPE.PAYMENT}
        editCheckoutStep={editCheckoutStep}
        orderReviewTranslation={orderReviewTranslation}
        translationPath={
          'cart.checkout.orderReview.paymentBilling.paymentBillingTxt'
        }
      />

      <ReadOnlyForm className='readOnlyForm'>
        {isUpfrontPrice && (
          <>
            <Upfront
              upFrontCardDetails={upFront}
              orderReviewTranslation={props.orderReviewTranslation}
            />
            <Divider dashed />
          </>
        )}
        {isMonthlyPrice && (
          <>
            <Monthly
              monthlyCardDetails={monthly}
              orderReviewTranslation={props.orderReviewTranslation}
            />
            {!billingConfiguration.skipBillingStep && <Divider dashed />}
          </>
        )}
        {!billingConfiguration.skipBillingStep && (
          <Billing
            orderReviewTranslation={props.orderReviewTranslation}
            billingInfo={props.billingInfo}
          />
        )}
      </ReadOnlyForm>
    </StyledPaymentBilling>
  );
};

export default PaymentBilling;
