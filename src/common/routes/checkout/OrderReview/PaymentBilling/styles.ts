import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledPaymentBilling = styled.div`
  div.readOnlyForm {
    padding-top: 0.78123rem;
    .subHeading {
      color: ${colors.mediumGray};
      /* text-transform: uppercase; */
      margin-bottom: 1.5rem;
    }

    .dt_divider.horizontal {
      margin: 0;
      min-height: 3px;
      margin-bottom: 0.78125rem;
      color: ${colors.lightGray};

      &:before {
        transform: scale(0.15);
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    div.readOnlyForm {
      padding-top: 1.75rem;
      .subHeading {
        align-self: flex-start;
        padding-top: 1.4rem;
      }

      .dt_divider.horizontal {
        margin-bottom: 1.75rem;
      }
    }
  }
`;
