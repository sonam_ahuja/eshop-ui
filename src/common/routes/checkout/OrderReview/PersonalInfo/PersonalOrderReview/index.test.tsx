import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import PersonalOrderReview, { IProps as IPersonalOrderReviewProps } from '.';

describe('<Order Review Order />', () => {
  const props: IPersonalOrderReviewProps = {
    personalInfo: appState().checkout.personalInfo,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    formConfiguration: appState().configuration.cms_configuration.modules
      .checkout.form
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PersonalOrderReview {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
