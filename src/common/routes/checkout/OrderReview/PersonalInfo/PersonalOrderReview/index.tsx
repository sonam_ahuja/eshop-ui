import React, { FunctionComponent } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import { ReadOnlyForm } from '@checkout/Common/ReadOnlyForm/styles';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import KeyValue from '@src/common/components/KeyValue';
import { IForm } from '@src/common/store/types/configuration';

import { StyledEmptyValue } from './styles';

export interface IProps {
  personalInfo: IPersonalInfoState;
  orderReviewTranslation: IOrderReviewTranslation;
  formConfiguration: IForm;
}

const PersonalOrderReview: FunctionComponent<IProps> = (props: IProps) => {
  const { personalInfo, formConfiguration } = props;
  const {
    personalInformation: personalInformationTranslation
  } = props.orderReviewTranslation;

  const checkForEmpty = (value: string) => {
    if (!value || value === '') {
      return <StyledEmptyValue />;
    }

    return value;
  };

  const formFields = formConfiguration.fields;

  const sortedFormFields = Object.keys(formFields).map(key => ({
    ...formFields[key],
    key
  }));

  sortedFormFields.sort((a, b) => (a.order < b.order ? -1 : 1));

  return (
    <ReadOnlyForm className='readOnlyForm'>
      <Row>
        {sortedFormFields.map((field, index) => {
          if (field.show) {
            if (field.labelKey === 'dob') {
              return (
                <Column
                  key={index}
                  className={field.labelKey}
                  colMobile={5}
                  colTabletPortrait={7}
                  colTabletLandscape={3}
                  colDesktop={2}
                >
                  <KeyValue
                    labelName={personalInformationTranslation.birthDate}
                    labelValue={checkForEmpty(
                      personalInfo.formFields[field.labelKey]
                    )}
                  />
                </Column>
              );
            } else if (field.labelKey === 'streetAddress') {
              return (
                <Column
                  key={index}
                  className={field.labelKey}
                  colMobile={5}
                  colTabletPortrait={7}
                  colTabletLandscape={3}
                  colDesktop={2}
                >
                  <KeyValue
                    labelName={personalInformationTranslation.streetAddress}
                    labelValue={checkForEmpty(personalInfo.formFields
                      .address as string)}
                  />
                </Column>
              );
            } else {
              return (
                <Column
                  key={index}
                  className={field.labelKey}
                  colMobile={5}
                  colTabletPortrait={7}
                  colTabletLandscape={3}
                  colDesktop={2}
                >
                  <KeyValue
                    labelName={personalInformationTranslation[field.labelKey]}
                    labelValue={checkForEmpty(
                      personalInfo.formFields[field.labelKey]
                    )}
                  />
                </Column>
              );
            }
          }

          return null;
        })}
      </Row>
    </ReadOnlyForm>
  );
};

export default PersonalOrderReview;
