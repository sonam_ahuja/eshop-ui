import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledEmptyValue = styled.div`
  height: 20px;
  width: 10px;
  position: relative;
  &:after {
    content: '';
    position: absolute;
    height: 1px;
    width: 100%;
    left: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    background: ${colors.darkGray};
  }
`;
