import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import PersonalInfo, { IProps as IPersonalInfoProps } from '.';

describe('<Order Review PersonalInfo />', () => {
  const props: IPersonalInfoProps = {
    personalInfo: appState().checkout.personalInfo,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    editCheckoutStep: jest.fn(),
    configuration: appState().configuration
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PersonalInfo {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
