import React, { FunctionComponent } from 'react';
import PersonalOrderReview from '@checkout/OrderReview/PersonalInfo/PersonalOrderReview';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import HeadingWithRoundIcon from '@checkout/Common/HeadingWithRoundIcon';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';
import { IConfigurationState } from '@src/common/store/types/configuration';

import { StyledPersonalInfo } from './styles';

export interface IProps {
  personalInfo: IPersonalInfoState;
  orderReviewTranslation: IOrderReviewTranslation;
  configuration: IConfigurationState;
  // @TODO: HERE_CHECKOUT_ADDRESSED
  editCheckoutStep(route: CHECKOUT_SUB_ROUTE_TYPE): void;
}

const PersonalInfo: FunctionComponent<IProps> = (props: IProps) => {
  const {
    personalInfo,
    orderReviewTranslation,
    editCheckoutStep,
    configuration
  } = props;
  const { personalInfoTxt } = props.orderReviewTranslation.personalInformation;

  return (
    <StyledPersonalInfo>
      <HeadingWithRoundIcon
        iconName='ec-user-account'
        title={personalInfoTxt}
        // @TODO: HERE_CHECKOUT_ADDRESSED
        editKey={CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO}
        orderReviewTranslation={orderReviewTranslation}
        editCheckoutStep={editCheckoutStep}
        translationPath={
          'cart.checkout.orderReview.personalInformation.personalInfoTxt'
        }
      />
      <PersonalOrderReview
        personalInfo={personalInfo}
        orderReviewTranslation={orderReviewTranslation}
        formConfiguration={
          configuration.cms_configuration.modules.checkout.form
        }
      />
    </StyledPersonalInfo>
  );
};

export default PersonalInfo;
