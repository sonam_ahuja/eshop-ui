import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import { colors } from '@src/common/variables';
import EVENT_NAME from '@events/constants/eventName';

export interface IProps {
  orderReviewTranslation: IOrderReviewTranslation;
  isOpen: boolean;
  firstName: string;
  chooseOtherMethod(): void;
  closeModal(): void;
}

export class InsufficientFunds extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { isOpen, chooseOtherMethod, closeModal, firstName } = this.props;
    const { placeOrderModals } = this.props.orderReviewTranslation;
    const insufficientFundsTitle = placeOrderModals.insufficientFunds.replace(
      '{0}',
      firstName
    );

    return (
      <DialogBoxApp
        isOpen={isOpen}
        closeOnBackdropClick={true}
        closeOnEscape={true}
        onEscape={closeModal}
        onBackdropClick={closeModal}
        backgroundScroll={false}
        type='flexibleHeight'
        onClose={closeModal}
        // onKeyDown={onKeyPressClose}
      >
        <div className='dialogBoxHeader'>
          <Icon name='ec-sad-face-round' size='xlarge' color={colors.magenta} />
        </div>

        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {insufficientFundsTitle}
          </Title>
        </div>

        <div className='dialogBoxFooter'>
          <Button
            type='secondary'
            size='medium'
            onClickHandler={chooseOtherMethod}
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_OTHER_METHOD}
            data-event-message={placeOrderModals.chooseOtherMethod}
          >
            {placeOrderModals.chooseOtherMethod}
          </Button>
        </div>
      </DialogBoxApp>
    );
  }
}

export default InsufficientFunds;
