import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translationState from '@store/states/translation';

import SelectOtherDevice, { IProps as ISelectOtherDeviceProps } from '.';

describe('<CreditCheck SelectOtherDevice />', () => {
  const props: ISelectOtherDeviceProps = {
    orderReviewTranslation: translationState().cart.checkout.orderReview,
    isOpen: false,
    chooseOtherMethod: jest.fn(),
    closeModal: jest.fn(),
    tryAgain: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SelectOtherDevice {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
