import React, { Component, ReactNode } from 'react';
import { Button, Icon, Title } from 'dt-components';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import { colors } from '@src/common/variables';
import EVENT_NAME from '@events/constants/eventName';

export interface IProps {
  orderReviewTranslation: IOrderReviewTranslation;
  isOpen: boolean;
  tryAgain(): void;
  chooseOtherMethod(): void;
  closeModal(): void;
}

export class SomethingWrong extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const { isOpen, chooseOtherMethod, tryAgain, closeModal } = this.props;
    const { placeOrderModals } = this.props.orderReviewTranslation;

    return (
      <>
        <DialogBoxApp
          isOpen={isOpen}
          closeOnBackdropClick={true}
          closeOnEscape={true}
          onEscape={closeModal}
          onBackdropClick={closeModal}
          backgroundScroll={false}
          type='flexibleHeight'
          onClose={closeModal}
          // onKeyDown={onKeyPressClose}
        >
          <div className='dialogBoxHeader'>
            <Icon
              name='ec-sad-face-round'
              size='xlarge'
              color={colors.magenta}
            />
          </div>

          <div className='dialogBoxBody'>
            <Title className='infoMessage' size='large' weight='ultra'>
              {placeOrderModals.somethingWrong}
            </Title>
          </div>

          <div className='dialogBoxFooter'>
            <Button
              type='primary'
              size='medium'
              onClickHandler={tryAgain}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.TRY_AGAIN}
              data-event-message={placeOrderModals.tryAgain}
            >
              {placeOrderModals.tryAgain}
            </Button>
            <Button
              type='secondary'
              size='medium'
              onClickHandler={chooseOtherMethod}
              data-event-id={EVENT_NAME.CHECKOUT.EVENTS.CHOOSE_OTHER_METHOD}
              data-event-message={placeOrderModals.chooseOtherMethod}
            >
              {placeOrderModals.chooseOtherMethod}
            </Button>
          </div>
        </DialogBoxApp>
      </>
    );
  }
}

export default SomethingWrong;
