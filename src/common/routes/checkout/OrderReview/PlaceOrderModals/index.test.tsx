import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';
import translation from '@common/store/states/translation';

import Component from '.';

describe('<PlaceOrderModal />', () => {
  test('should render properly on ORDER_SUCCESS', () => {
    const props = {
      firstName: '',
      activeStep: PLACE_ORDER_CODE.ORDER_SUCCESS,
      orderReviewTranslation: translation().cart.checkout.orderReview,
      isOpen: false,
      closeModal: jest.fn(),
      tryAgain: jest.fn(),
      chooseOtherMethod: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly on PAYMENT_DECLINED', () => {
    const props = {
      firstName: '',
      activeStep: PLACE_ORDER_CODE.PAYMENT_DECLINED,
      orderReviewTranslation: translation().cart.checkout.orderReview,
      isOpen: false,
      closeModal: jest.fn(),
      tryAgain: jest.fn(),
      chooseOtherMethod: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly on INSUFFICIENT_FUNDS', () => {
    const props = {
      firstName: '',
      activeStep: PLACE_ORDER_CODE.INSUFFICIENT_FUNDS,
      orderReviewTranslation: translation().cart.checkout.orderReview,
      isOpen: false,
      closeModal: jest.fn(),
      tryAgain: jest.fn(),
      chooseOtherMethod: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly on SOMETHING_WRONG', () => {
    const props = {
      firstName: '',
      activeStep: PLACE_ORDER_CODE.SOMETHING_WRONG,
      orderReviewTranslation: translation().cart.checkout.orderReview,
      isOpen: false,
      closeModal: jest.fn(),
      tryAgain: jest.fn(),
      chooseOtherMethod: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly on SOMETHING_WRONG', () => {
    // tslint:disable-next-line:no-any
    const props: any = {
      firstName: '',
      activeStep: '',
      orderReviewTranslation: translation().cart.checkout.orderReview,
      isOpen: false,
      closeModal: jest.fn(),
      tryAgain: jest.fn(),
      chooseOtherMethod: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
