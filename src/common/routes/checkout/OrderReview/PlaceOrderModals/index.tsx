import React, { Component, ReactNode } from 'react';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';

import PaymentDeclined from './PaymentDeclined';
import InsufficientFunds from './InsufficientFunds';
import SomethingWrong from './SomethingWrong';

export interface IProps {
  firstName: string;
  activeStep: PLACE_ORDER_CODE;
  orderReviewTranslation: IOrderReviewTranslation;
  isOpen: boolean;
  closeModal(): void;
  tryAgain(): void;
  chooseOtherMethod(): void;
}

export class PlaceOrderModals extends Component<IProps, {}> {
  renderModals = () => {
    const {
      activeStep,
      orderReviewTranslation,
      isOpen,
      closeModal,
      tryAgain,
      chooseOtherMethod,
      firstName
    } = this.props;

    switch (activeStep) {
      case PLACE_ORDER_CODE.PAYMENT_DECLINED:
        return (
          <PaymentDeclined
            firstName={firstName}
            orderReviewTranslation={orderReviewTranslation}
            isOpen={isOpen}
            closeModal={closeModal}
            tryAgain={tryAgain}
            chooseOtherMethod={chooseOtherMethod}
          />
        );

      case PLACE_ORDER_CODE.INSUFFICIENT_FUNDS:
        return (
          <InsufficientFunds
            firstName={firstName}
            orderReviewTranslation={orderReviewTranslation}
            isOpen={isOpen}
            closeModal={closeModal}
            chooseOtherMethod={chooseOtherMethod}
          />
        );

      case PLACE_ORDER_CODE.SOMETHING_WRONG:
        return (
          <SomethingWrong
            orderReviewTranslation={orderReviewTranslation}
            isOpen={isOpen}
            tryAgain={tryAgain}
            chooseOtherMethod={chooseOtherMethod}
            closeModal={closeModal}
          />
        );

      default:
        return null;
    }
  }

  render(): ReactNode {
    return <>{this.renderModals()}</>;
  }
}

export default PlaceOrderModals;
