import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { SHIPPING_TYPE } from '@checkout/store/enums';

import ShippingOrderReview, { IProps as IShippingOrderReviewProps } from '.';

describe('<Order Review PersonalInfoHeader />', () => {
  const props: IShippingOrderReviewProps = {
    shippingInfo: appState().checkout.shippingInfo,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview
  };

  const componentWrapper = (newProps: IShippingOrderReviewProps) => mount(
    <ThemeProvider theme={{}}>
      <ShippingOrderReview {...newProps} />
    </ThemeProvider>
  );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is exact day', () => {
    const newShippingInfo = { ...appState().checkout.shippingInfo };
    newShippingInfo.shippingType = SHIPPING_TYPE.EXACT_DAY;
    const newProps: IShippingOrderReviewProps = {
      shippingInfo: newShippingInfo,
      orderReviewTranslation: appState().translation.cart.checkout.orderReview
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is same day', () => {
    const newShippingInfo = { ...appState().checkout.shippingInfo };
    newShippingInfo.shippingType = SHIPPING_TYPE.SAME_DAY;
    const newProps: IShippingOrderReviewProps = {
      shippingInfo: newShippingInfo,
      orderReviewTranslation: appState().translation.cart.checkout.orderReview
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when shipping type is pick up point', () => {
    const newShippingInfo = {
      ...appState().checkout.shippingInfo,
      selectedStoreAddress: {
        id: '1',
        streetNumber: 'string',
        address: 'string',
        flatNumber: 'string',
        postCode: 'string',
        city: 'string',
        state: 'string',
        country: 'string',
        distance: 'string',
        distanceUnit: 'string',
        name: 'string',
        geoX: 'string',
        geoY: 'string'
      }
    };
    newShippingInfo.shippingType = SHIPPING_TYPE.PICK_UP_POINT;
    const newProps: IShippingOrderReviewProps = {
      shippingInfo: newShippingInfo,
      orderReviewTranslation: appState().translation.cart.checkout.orderReview
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
