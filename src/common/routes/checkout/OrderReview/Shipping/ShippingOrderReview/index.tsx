import React, { FunctionComponent } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import {
  ISelectedStoreAddress,
  IShippingAddressState
} from '@checkout/store/shipping/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import { ReadOnlyForm } from '@checkout/Common/ReadOnlyForm/styles';
import { Section } from 'dt-components';
import KeyValue from '@src/common/components/KeyValue';
import { SHIPPING_TYPE } from '@checkout/store/enums';

interface IAddressObj {
  streetAddress: string;
  flatNumber: string;
  city: string;
  postCode: string;
  streetNumber: string;
}

export interface IProps {
  shippingInfo: IShippingAddressState;
  orderReviewTranslation: IOrderReviewTranslation;
}
const ShippingOrderReview: FunctionComponent<IProps> = (props: IProps) => {
  const { shippingType } = props.shippingInfo;

  let obj: IAddressObj;

  let shippingTypeKey = shippingType as string;

  if (
    shippingType === SHIPPING_TYPE.STANDARD ||
    shippingType === SHIPPING_TYPE.EXACT_DAY ||
    shippingType === SHIPPING_TYPE.SAME_DAY
  ) {
    shippingTypeKey = 'deliverToAddress';
    const {
      streetAddress,
      flatNumber,
      city,
      postCode,
      streetNumber
    } = props.shippingInfo;

    obj = {
      streetAddress,
      flatNumber,
      city,
      postCode,
      streetNumber
    };
  } else {
    const {
      address: streetAddress,
      flatNumber,
      city,
      postCode,
      streetNumber
    } = props.shippingInfo.selectedStoreAddress as ISelectedStoreAddress;

    obj = {
      streetAddress,
      flatNumber,
      city,
      postCode,
      streetNumber
    };
  }

  return (
    <ReadOnlyForm className='readOnlyForm'>
      <Row>
        <Column
          className='subHeading'
          colMobile={12}
          colTabletLandscape={4}
          orderDesktop={1}
          orderMobile={1}
        >
          <Section size='small' weight='normal' transform='uppercase'>
            {props.orderReviewTranslation[shippingTypeKey]}
          </Section>
        </Column>
        <Column
          className='storename'
          colMobile={12}
          colTabletLandscape={2}
          colDesktop={4}
          orderDesktop={1}
          orderMobile={1}
        >
          <KeyValue
            labelName={props.orderReviewTranslation.shipping.addressLabel}
            labelValue={`${obj.flatNumber || ''} ${obj.streetAddress ||
              ''} ${obj.streetNumber || ''} ${obj.city || ''} ${obj.postCode ||
              ''}`}
          />
        </Column>

        <Column
          className='storeaddress'
          colMobile={12}
          colTabletLandscape={6}
          colDesktop={4}
          orderDesktop={2}
          orderMobile={2}
        >
          {/* {selectedStoreAddress &&
            selectedStoreAddress.pickUpAddress.map(
              (pickUpAddress: IPickUpAddress) => {
                if (pickUpAddress.isDefault)
                  return (
                    <KeyValue
                      labelName={storeAddress}
                      labelValue={pickUpAddress.address}
                    />
                  );

                return null;
              }
            )} */}
        </Column>
      </Row>
    </ReadOnlyForm>
  );
};

export default ShippingOrderReview;
