import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import Shipping, { IProps as IShippingProps } from '.';

describe('<Order Review Shipping />', () => {
  const props: IShippingProps = {
    shippingInfo: appState().checkout.shippingInfo,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    editCheckoutStep: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Shipping {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
