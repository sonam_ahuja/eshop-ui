import React, { FunctionComponent } from 'react';
import ShippingOrderReview from '@checkout/OrderReview/Shipping/ShippingOrderReview';
import { IShippingAddressState } from '@checkout/store/shipping/types';
import { IOrderReviewTranslation } from '@src/common/store/types/translation';
import HeadingWithRoundIcon from '@checkout/Common/HeadingWithRoundIcon';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';

import { StyledShipping } from './styles';

export interface IProps {
  shippingInfo: IShippingAddressState;
  orderReviewTranslation: IOrderReviewTranslation;
  editCheckoutStep(route: CHECKOUT_SUB_ROUTE_TYPE): void;
}

const Shipping: FunctionComponent<IProps> = (props: IProps) => {
  const { orderReviewTranslation, editCheckoutStep } = props;
  const { shippingTxt } = props.orderReviewTranslation.shipping;

  return (
    <StyledShipping>
      <HeadingWithRoundIcon
        iconName='ec-transporter-right'
        title={shippingTxt}
        // @TODO: HERE_CHECKOUT_ADDRESSED
        editKey={CHECKOUT_SUB_ROUTE_TYPE.SHIPPING}
        editCheckoutStep={editCheckoutStep}
        orderReviewTranslation={orderReviewTranslation}
        translationPath={'cart.checkout.orderReview.shipping.shippingTxt'}
      />
      <ShippingOrderReview {...props} />
    </StyledShipping>
  );
};

export default Shipping;
