import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledShipping = styled.div`
  margin-top: 1rem;

  div.readOnlyForm {
    padding-top: 0.78123rem;
    .subHeading {
      color: ${colors.mediumGray};
      /* text-transform: uppercase; */
      margin-bottom: 1.5rem;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    div.readOnlyForm {
      padding-top: 1.75rem;

      .subHeading {
        align-self: flex-start;
        padding-top: 1.4rem;
      }
    }
  }
`;
