import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints } from '@src/common/variables';

const OrderReviewShellWrapper = styled.div`
  padding: 2rem 1rem;
  width: 100%;

  p {
    height: 0.5rem;
    background-color: #dadada;
    border-radius: 2rem;
    margin-top: 0rem;
    width: 20%;
    display: inline-block;
    margin-right: 1.5rem;
    margin-bottom: 1.85rem;
    margin-left: 2.75rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    p {
      margin-left: 0rem;
    }
  }
`;

const OrderReviewAppShell: FunctionComponent = () => {
  return (
    <StyledShell>
      <OrderReviewShellWrapper>
        <p />
        <p />
        <p />
        <p />
        <p />
        <p />
        <p />
        <p />
        <p />
        <p />
        <p />
      </OrderReviewShellWrapper>
    </StyledShell>
  );
};

export default OrderReviewAppShell;
