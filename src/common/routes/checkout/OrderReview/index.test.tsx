import IState from '@basket/store/state';
import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import constants from '@checkout/store/orderReview/constants';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { StaticRouter } from 'react-router-dom';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';
import { AGREEMENT_PRIVACY_GROUP } from '@checkout/store/orderReview/types.ts';
import appState from '@store/states/app';
import { ISetTelekomType } from '@checkout/store/orderReview/types';
import OrderReviewAppShell from '@checkout/OrderReview/index.shell';
import { histroyParams } from '@mocks/common/histroy';

import {
  IComponentProps as IOrderReviewProps,
  mapDispatchToProps,
  mapStateToProps,
  OrderReview
} from '.';

describe('<Order review />', () => {
  const initStateValue = appState();
  const newBasket = { ...IState() };
  newBasket.cartSummary = BASKET_ALL_ITEM.cartSummary;
  newBasket.basketItems = BASKET_ALL_ITEM.cartItems;

  newBasket.showAppShell = false;
  window.scrollTo = jest.fn();
  const props: IOrderReviewProps = {
    ...histroyParams,
    orderReviewConfiguration: appState().configuration.cms_configuration.modules
      .checkout.orderReview,
    orderReviewTranslation: appState().translation.cart.checkout.orderReview,
    checkoutTranslation: appState().translation.cart.checkout,
    globalTranslation: appState().translation.cart.global,
    checkout: appState().checkout,
    termsAndConditionsUrl: 'string',
    basketItems: BASKET_ALL_ITEM.cartItems,
    shouldTermsAndConditionsOpenInNewTab: true,
    agreementShouldTermsAndConditionsOpenInNewTab: true,
    agreementTermsAndConditionsUrl: 'string',
    billingConfiguration: appState().configuration.cms_configuration.modules
      .checkout.billingInfo,
      configuration: appState().configuration,
    fetchAgreement: jest.fn(),
    setTelekomService: jest.fn(),
    editCheckoutStep: jest.fn(),
    tryAgain: jest.fn(),
    closeModal: jest.fn(),
    mainCheckboxClick: jest.fn(),
    chooseOtherMethod: jest.fn(),
    innerAgreementToggle: jest.fn(),
    mainAgreementToggle: jest.fn(),
    showTermAndConditions: jest.fn(),
    downloadPdf: jest.fn(),
    fetchDiscount: jest.fn(),
    editPaymentStepClick: jest.fn(),
    fetchCheckoutAddressLoading: jest.fn()
  };
  window.scrollTo = jest.fn();
  const mockStore = configureStore();
  const store = mockStore(initStateValue);

  const componentWrapper = (newProps: IOrderReviewProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <OrderReview {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('OrderReviewAppShell loading', () => {
    const wapper = mount(<OrderReviewAppShell />);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when module App progress Modal ', () => {
    const newProps = { ...props };
    newProps.checkout.orderReview.placeOrderLoading = true;
    const wapper = componentWrapper(newProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly when payment is failed ', () => {
    const newProps = { ...props };
    newProps.checkout.orderReview.openPaymentFailureModal = true;
    const wapper = componentWrapper(newProps);
    expect(wapper).toMatchSnapshot();
  });

  test('should render properly, when cart summary and basket item is empty', () => {
    const newBasketWithoutCartSummary = { ...newBasket };
    newBasketWithoutCartSummary.cartSummary = [];
    newBasketWithoutCartSummary.basketItems = [];
    newBasketWithoutCartSummary.showAppShell = true;
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const orderReviewExpected: { type: string; payload: ISetTelekomType } = {
      type: constants.SET_TELEKOM_SERVICE,
      payload: {
        id: '1',
        type: AGREEMENT_PRIVACY_GROUP.NOTIFICATION
      }
    };

    mapDispatchToProps(dispatch).setTelekomService({
      id: '1',
      type: AGREEMENT_PRIVACY_GROUP.NOTIFICATION
    });
    mapDispatchToProps(dispatch).editCheckoutStep(
      CHECKOUT_SUB_ROUTE_TYPE.BILLING
    );
    mapDispatchToProps(dispatch).closeModal();
    mapDispatchToProps(dispatch).tryAgain();

    mapDispatchToProps(dispatch).chooseOtherMethod();
    mapDispatchToProps(dispatch).fetchAgreement();
    mapDispatchToProps(dispatch).innerAgreementToggle(2, 4);
    mapDispatchToProps(dispatch).mainAgreementToggle(3);
    mapDispatchToProps(dispatch).showTermAndConditions();
    mapDispatchToProps(dispatch).downloadPdf('string');
    mapDispatchToProps(dispatch).mainCheckboxClick();
    mapDispatchToProps(dispatch).fetchDiscount();
    expect(dispatch.mock.calls[0][0]).toEqual(orderReviewExpected);
  });

  test('handle trigger', () => {
    const component = mount<OrderReview>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <OrderReview {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('OrderReview').instance() as OrderReview).switchViewMore();
  });
});
