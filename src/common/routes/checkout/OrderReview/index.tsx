import FooterWithTermsAndConditions from '@common/components/FooterWithTermsAndConditions';
import PaymentBilling from '@checkout/OrderReview/PaymentBilling';
import {
  IBillingConfiguration,
  IConfigurationState,
  IOrderReview
} from '@store/types/configuration';
import { ISetTelekomType } from '@checkout/store/orderReview/types';
import Header from '@checkout/OrderReview/Header';
import PersonalInfo from '@checkout/OrderReview/PersonalInfo';
import Shipping from '@checkout/OrderReview/Shipping';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import actions from '@checkout/store/orderReview/actions';
import React, { Component } from 'react';
import Order from '@checkout/OrderReview/Order';
import { RootState } from '@common/store/reducers';
import Agreements from '@checkout/OrderReview/Agreements';
import {
  ICheckoutTranslation,
  IGlobalTranslation,
  IOrderReviewTranslation
} from '@store/types/translation';
import { IState as ICheckoutState } from '@checkout/store/types';
import paymentActions from '@checkout/store/payment/actions';
import checkoutActions from '@checkout/store/actions';
import store from '@src/common/store';
import { isMobile } from '@src/common/utils';
import PlaceOrderModals from '@checkout/OrderReview/PlaceOrderModals';
import { AppProgressModal } from '@src/common/components/ProgressModal/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import history from '@src/client/history';
import { CHECKOUT_SUB_ROUTE_TYPE } from '@checkout/store/enums';
import { IBasketItem } from '@basket/store/types';
import { Paragraph, StripMessage } from 'dt-components';
import { pageViewEvent, sendCTAClicks } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import Loadable from 'react-loadable';

import {
  ReviewOrderWrap,
  StyledFooter,
  StyledStripMessageWrap
} from './styles';

const CheckoutAppShellComponent = Loadable({
  loading: () => null,
  loader: () =>
    import(
      /* webpackChunkName: 'CheckoutAppShellComponent' */ '@common/routes/checkout/CheckoutSteps/index.shell'
    )
});

interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export interface IComponentStateProps {
  orderReviewTranslation: IOrderReviewTranslation;
  globalTranslation: IGlobalTranslation;
  checkout: ICheckoutState;
  checkoutTranslation: ICheckoutTranslation;
  termsAndConditionsUrl: string;
  shouldTermsAndConditionsOpenInNewTab: boolean;
  agreementShouldTermsAndConditionsOpenInNewTab: boolean;
  agreementTermsAndConditionsUrl: string;
  basketItems: IBasketItem[];
  orderReviewConfiguration: IOrderReview;
  billingConfiguration: IBillingConfiguration;
  configuration: IConfigurationState;
}

export interface ICompDispatchToProps {
  setTelekomService(data: ISetTelekomType): void;
  closeModal(): void;
  tryAgain(): void;
  chooseOtherMethod(): void;
  fetchAgreement(): void;
  editCheckoutStep(route: CHECKOUT_SUB_ROUTE_TYPE): void;
  innerAgreementToggle(outerIndex: number, innerIndex: number): void;
  mainAgreementToggle(data: number): void;
  showTermAndConditions(): void;
  mainCheckboxClick(): void;
  downloadPdf(url: string): void;
  fetchDiscount(): void;
  fetchCheckoutAddressLoading(data: boolean): void;
  editPaymentStepClick(): void;
}

export type IComponentProps = IComponentStateProps &
  ICompDispatchToProps &
  RouteComponentProps<IRouterParams>;

export interface IState {
  viewMoreBoolean: boolean;
}

export class OrderReview extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);

    this.state = {
      viewMoreBoolean: true
    };

    this.switchViewMore = this.switchViewMore.bind(this);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.CHECKOUT_ORDER_REVIEW_INFO);
    window.scrollTo(0, 0);
    this.props.fetchAgreement();
  }

  switchViewMore(): void {
    this.setState(
      {
        viewMoreBoolean: !this.state.viewMoreBoolean
      },
      () => {
        sendCTAClicks(
          this.state.viewMoreBoolean
            ? this.props.orderReviewTranslation.viewMore
            : this.props.orderReviewTranslation.viewLess,
          window.location.href
        );
      }
    );
  }

  render(): JSX.Element {
    const {
      checkout,
      orderReviewTranslation,
      globalTranslation,
      termsAndConditionsUrl,
      shouldTermsAndConditionsOpenInNewTab,
      closeModal,
      tryAgain,
      chooseOtherMethod,
      editCheckoutStep,
      checkoutTranslation,
      basketItems,
      orderReviewConfiguration,
      billingConfiguration,
      editPaymentStepClick,
      fetchCheckoutAddressLoading,
      configuration
    } = this.props;

    const { placeOrderModals } = orderReviewTranslation;

    const firstName = checkout.personalInfo.formFields.firstName;
    const placeOrderTitle = placeOrderModals.placeOrderInProcess.replace(
      '{0}',
      firstName
    );

    return (
      <>
        {checkout.orderReview.placeOrderLoading ? (
          <AppProgressModal
            isOpen={checkout.orderReview.placeOrderLoading}
            title={placeOrderTitle}
          />
        ) : null}

        {checkout.orderReview.openPaymentFailureModal ? (
          <PlaceOrderModals
            firstName={firstName}
            activeStep={checkout.orderReview.activeStep}
            orderReviewTranslation={orderReviewTranslation}
            isOpen={checkout.orderReview.openPaymentFailureModal}
            tryAgain={tryAgain}
            chooseOtherMethod={chooseOtherMethod}
            closeModal={closeModal}
          />
        ) : null}

        {checkout.checkout.mainAppShell ? (
          <CheckoutAppShellComponent
            checkoutTranslation={checkoutTranslation}
            editPaymentStepClick={editPaymentStepClick}
            fetchCheckoutAddressLoading={fetchCheckoutAddressLoading}
          />
        ) : (
          <ReviewOrderWrap>
            <Header orderReviewTranslation={orderReviewTranslation} />

            {orderReviewConfiguration.enablePaymentProcess ? (
              <StyledStripMessageWrap>
                <StripMessage
                  isOpen={true}
                  closeOnDismiss={true}
                  icon='ec-information'
                >
                  <Paragraph size='small'>
                    {orderReviewTranslation.paymentProcessMessage}
                  </Paragraph>
                </StripMessage>
              </StyledStripMessageWrap>
            ) : null}

            <div className='whiteBg'>
              <PersonalInfo
                orderReviewTranslation={orderReviewTranslation}
                personalInfo={checkout.personalInfo}
                editCheckoutStep={editCheckoutStep}
                configuration={configuration}
              />
              <Shipping
                shippingInfo={checkout.shippingInfo}
                orderReviewTranslation={orderReviewTranslation}
                editCheckoutStep={editCheckoutStep}
              />
            </div>
            <div className='grayBg'>
              <PaymentBilling
                orderReviewTranslation={orderReviewTranslation}
                paymentInfo={checkout.payment}
                billingInfo={checkout.billing}
                editCheckoutStep={editCheckoutStep}
                isUpfrontPrice={checkout.checkout.isUpfrontPrice}
                isMonthlyPrice={checkout.checkout.isMonthlyPrice}
                billingConfiguration={billingConfiguration}
              />
              <Order
                orderReviewTranslation={orderReviewTranslation}
                showMore={this.state.viewMoreBoolean}
                switchViewMore={this.switchViewMore}
                basketItems={basketItems}
              />
              <Agreements orderReview={checkout.orderReview} {...this.props} />
            </div>

            {!isMobile.phone && (
              <StyledFooter>
                <FooterWithTermsAndConditions
                  className='footer'
                  termsAndConditionsUrl={termsAndConditionsUrl}
                  shouldTermsAndConditionsOpenInNewTab={
                    shouldTermsAndConditionsOpenInNewTab
                  }
                  globalTranslation={globalTranslation}
                />
              </StyledFooter>
            )}
          </ReviewOrderWrap>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IComponentStateProps => ({
  checkout: state.checkout,
  globalTranslation: state.translation.cart.global,
  orderReviewTranslation: state.translation.cart.checkout.orderReview,
  termsAndConditionsUrl:
    state.configuration.cms_configuration.global.termsAndConditionsUrl,
  shouldTermsAndConditionsOpenInNewTab:
    state.configuration.cms_configuration.global
      .shouldTermsAndConditionsOpenInNewTab,
  agreementTermsAndConditionsUrl:
    state.configuration.cms_configuration.modules.checkout.orderReview
      .termsAndConditionsUrl,
  agreementShouldTermsAndConditionsOpenInNewTab:
    state.configuration.cms_configuration.modules.checkout.orderReview
      .shouldTermsAndConditionsOpenInNewTab,
  basketItems: state.basket.basketItems,
  orderReviewConfiguration:
    state.configuration.cms_configuration.modules.checkout.orderReview,
  billingConfiguration:
    state.configuration.cms_configuration.modules.checkout.billingInfo,
  checkoutTranslation: state.translation.cart.checkout,
  configuration: state.configuration
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  setTelekomService(data: ISetTelekomType): void {
    dispatch(actions.setTelekomService(data));
  },
  // @TODO: HERE_CHECKOUT_ADDRESSED
  editCheckoutStep(route: CHECKOUT_SUB_ROUTE_TYPE): void {
    const isUpfrontPrice = store.getState().checkout.checkout.isUpfrontPrice;
    dispatch(paymentActions.billingToPaymentStepUpdate(isUpfrontPrice)); // to update upfront data
    // @TODO: HERE_CHECKOUT_ADDRESSED
    if (history) {
      history.push(`/checkout/${route}`);
    }
  },
  closeModal(): void {
    dispatch(actions.closeModal());
  },
  tryAgain(): void {
    dispatch(actions.tryAgain());
  },
  chooseOtherMethod(): void {
    dispatch(actions.chooseOtherMethod());
  },
  fetchAgreement(): void {
    dispatch(actions.fetchAgreement());
  },
  innerAgreementToggle(outerIndex: number, innerIndex: number): void {
    dispatch(actions.innerAgreementToggle({ outerIndex, innerIndex }));
  },
  mainAgreementToggle(data: number): void {
    dispatch(actions.mainAgreementToggle(data));
  },
  showTermAndConditions(): void {
    dispatch(checkoutActions.showTermsAndConditions());
  },
  downloadPdf(url: string): void {
    dispatch(actions.downloadPdf(url));
  },
  mainCheckboxClick(): void {
    dispatch(actions.mainCheckboxClick());
  },
  fetchDiscount(): void {
    dispatch(actions.fetchDiscount());
  },
  fetchCheckoutAddressLoading(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddressLoading(data));
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  }
});

export default withRouter(
  connect<IComponentStateProps, ICompDispatchToProps, void, RootState>(
    mapStateToProps,
    mapDispatchToProps
  )(OrderReview)
  // tslint:disable-next-line:max-file-line-count
);
