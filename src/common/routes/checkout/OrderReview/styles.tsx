import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledFooter = styled.div`
  .footer {
    width: 100%;
    background: ${colors.black};
    padding: 0 2.25rem;

    > div {
      height: 2.5rem;
      padding-top: 1rem;
      font-size: 0.75rem;
      line-height: 1.33;
      letter-spacing: 0.24px;
      display: flex;
      align-items: center;
      justify-content: space-between;
      color: ${colors.mediumGray};
    }
    > div a {
      font-size: inherit !important;
    }
    > div a div {
      font-size: inherit !important;
      letter-spacing: inherit;
      color: ${colors.ironGray};
    }

    @media (min-width: ${breakpoints.tabletLandscape}px) {
      background: none;
      width: auto;
      display: inline-block;
      padding-left: 3rem;
      padding-right: 3rem;
      padding-bottom: 2.25rem;
      padding-top: 1rem;
      > div {
        height: auto;
        display: block;
        a {
          display: block;
        }
      }
    }

    @media (min-width: ${breakpoints.desktop}px) {
      padding: 0 4.9rem;
      margin-top: 0.5rem;
      > div {
        height: 3.25rem;
        font-size: 0.625rem;
        padding-top: 0;
        align-items: center;
        display: inline-flex;
        a {
          display: inline-block;
        }
      }
    }

    @media (min-width: ${breakpoints.desktopLarge}px) {
      padding: 0 5.5rem;
      margin-top: 0.5rem;
    }
  }
`;

// NEW STYLES
export const ReviewOrderWrap = styled.div`
  margin-top: -5rem;
  display: flex;
  flex-direction: column;
  width: 100%;

  /* common form wrapper start */
  .readOnlyForm {
    padding-top: 1.75rem;
    padding-left: 3.5rem;
  }
  /* common form wrapper end */

  .whiteBg,
  .grayBg {
    padding: 2rem 1.25rem 1.75rem;
  }

  .whiteBg {
    background-color: ${colors.white};
  }
  .grayBg {
    background-color: ${colors.coldGray};
    padding-top: 3rem;
    padding-bottom: 2.5rem;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    margin-top: -7rem;
    .whiteBg,
    .grayBg {
      padding-left: 2.25rem;
      padding-right: 2.25rem;
    }
    .whiteBg {
      padding-top: 0.75rem;
    }
    .grayBg {
      padding-bottom: 3rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    margin-top: 0;
    /* common form wrapper start */
    .readOnlyForm {
      padding-left: 0;
    }
    /* common form wrapper end */

    .whiteBg,
    .grayBg {
      padding-left: 3rem;
      padding-right: 3rem;
    }
    .grayBg {
      padding-top: 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .whiteBg,
    .grayBg {
      padding-left: 4.9rem;
      padding-right: 4.9rem;
    }
    .whiteBg {
      padding-top: 2rem;
    }
    .grayBg {
      padding-top: 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .whiteBg,
    .grayBg {
      padding-left: 5.5rem;
      padding-right: 5.5rem;
    }
  }
`;

export const StyledStripMessageWrap = styled.div`
  padding: 1.25rem;
  padding-bottom: 0;
  background: ${colors.white};

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-left: 2.25rem;
    padding-right: 2.25rem;
    & + .whiteBg {
      padding-top: 2rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-left: 3rem;
    padding-right: 3rem;
    & + .whiteBg {
      padding-top: 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-top: 0;
    padding-left: 4.9rem;
    padding-right: 4.9rem;
    & + .whiteBg {
      padding-top: 2rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-left: 5.5rem;
    padding-right: 5.5rem;
  }
`;
