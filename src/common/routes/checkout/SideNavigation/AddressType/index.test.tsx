import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import AddressType, { IProps as IAddressTypeProps } from '.';

describe('<Payment AddressType />', () => {
  const props: IAddressTypeProps = {
    billing: appState().checkout.billing,
    billingTranslation: appState().translation.cart.checkout.billingInfo,
    setAddressBillingType: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <AddressType {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly sameAsPersonalInfo is true', () => {
    const newProps = { ...props };
    newProps.billing.sameAsPersonalInfo = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <AddressType {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly sameAsShippingInfo is true', () => {
    const newProps = { ...props };
    newProps.billing.sameAsShippingInfo = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <AddressType {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('on changing billing type', () => {
    const newProps = { ...props };
    newProps.billing.sameAsShippingInfo = true;
    const component = mount(
      <ThemeProvider theme={{}}>
        <AddressType {...newProps} />
      </ThemeProvider>
    );
    component
      .find('div[onClick]')
      .at(2)
      .simulate('click');
  });
});
