import React, { Fragment, FunctionComponent } from 'react';
import {
  billingAddressType,
  IBillingState
} from '@checkout/store/billing/types';
import { BILLING_ADDRESS_TYPE } from '@checkout/store/enums';
import { Paragraph } from 'dt-components';
import { IBillingInfoTranslation } from '@src/common/store/types/translation';
import styled from 'styled-components';

export interface IProps {
  billing: IBillingState;
  billingTranslation: IBillingInfoTranslation;
  setAddressBillingType(type: billingAddressType): void;
}

const setBillingType = (
  adressType: billingAddressType,
  fn: (type: billingAddressType) => void
) => {
  return () => {
    fn(adressType);
  };
};

const AddressWrap = styled.div`
  cursor: pointer;
`;

const AddressType: FunctionComponent<IProps> = (props: IProps) => {
  const { setAddressBillingType, billing, billingTranslation } = props;

  return (
    <Fragment>
      <AddressWrap
        onClick={setBillingType(
          BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS,
          setAddressBillingType
        )}
      >
        <Paragraph
          size='medium'
          weight='normal'
          className={
            !billing.sameAsPersonalInfo && !billing.sameAsShippingInfo
              ? // tslint:disable-next-line:no-duplicate-string
                'paragraph active'
              : 'paragraph'
          }
        >
          {billingTranslation.addressType.differentAddress}
        </Paragraph>
      </AddressWrap>

      <AddressWrap
        onClick={setBillingType(
          BILLING_ADDRESS_TYPE.PERSONAL_INFO,
          setAddressBillingType
        )}
      >
        <Paragraph
          size='medium'
          weight='normal'
          className={
            billing.sameAsPersonalInfo ? 'paragraph active' : 'paragraph'
          }
        >
          {billingTranslation.addressType.sameAsPersonalInfo}
        </Paragraph>
      </AddressWrap>

      <AddressWrap
        onClick={setBillingType(
          BILLING_ADDRESS_TYPE.SHIPPING_INFO,
          setAddressBillingType
        )}
      >
        <Paragraph
          size='medium'
          weight='normal'
          className={
            billing.sameAsShippingInfo ? 'paragraph active' : 'paragraph'
          }
        >
          {billingTranslation.addressType.sameAsShippingInfo}
        </Paragraph>
      </AddressWrap>
    </Fragment>
  );
};

export default AddressType;
