import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SideBarDesktop from '@checkout/SideNavigation/SideBarDesktop';

describe('<ShippingModeMobileNav Shipping />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SideBarDesktop />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
