import styled from 'styled-components';
import React, { FunctionComponent, ReactNode } from 'react';
import { breakpoints } from '@src/common/variables';

// tslint:disable-next-line:no-commented-code
// export interface IComponentStateProps {
//   checkoutSteps: ICheckoutStepsState;
// }

const SideBarDesktopWrap = styled.div`
  @media (min-width: ${breakpoints.desktop}px) {
    max-width: 15.8rem;
    min-width: 15.8rem;
    padding: 2.25rem 2.25rem 1.25rem;
    height: 100vh;
    display: flex;
    flex-direction: column;
    color: #6c6c6c;
    overflow: hidden;
  }
`;

const SideBarDesktop: FunctionComponent<{}> = (props: {
  children?: ReactNode;
}) => {
  const { children } = props;

  return (
    <SideBarDesktopWrap className='sidebarWrap'>{children}</SideBarDesktopWrap>
  );
};

export default SideBarDesktop;
