import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SideBarMobile from '@checkout/SideNavigation/SideBarMobile';

describe('<ShippingModeMobileNav Shipping />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SideBarMobile />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
