import styled from 'styled-components';
import React, { FunctionComponent, ReactNode } from 'react';
// tslint:disable-next-line:no-commented-code
// import { ICheckoutStepsState } from '@checkout/store/checkoutSteps/types';

// tslint:disable-next-line:no-commented-code
// export interface IComponentStateProps {
//   checkoutSteps: ICheckoutStepsState;
// }

const SideBarMobileWrap = styled.div`
  max-width: 100%;
  min-width: 100%;
  padding: 1.25rem;
  display: flex;
  flex-direction: row;
  color: #6c6c6c;
  overflow: hidden;
  > div {
    background-color: #fff;
    padding-left: 1.25rem;
  }
`;

const SideBarMobile: FunctionComponent<{}> = (props: {
  children?: ReactNode;
}) => {
  const { children } = props;

  return <SideBarMobileWrap>{children}</SideBarMobileWrap>;
};

export default SideBarMobile;
