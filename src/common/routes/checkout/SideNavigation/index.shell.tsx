import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';

const StyledSideNavigationShell = styled.div`
  color: #dadada;
  max-width: 15.8rem;
  min-width: 15.8rem;
  padding: 2.25rem 2.25rem 1.25rem;
  height: 100vh;
  display: flex;
  flex-direction: column;
  position: fixed;

  overflow: hidden;

  .icon {
    height: 2rem;
    width: 5rem;
    background: currentColor;
    margin-bottom: 3.75rem;
  }

  .img {
    height: 8rem;
    background: currentColor;
    margin-bottom: 1.75rem;
  }

  .text p {
    height: 0.5rem;
    background: currentColor;
    margin-bottom: 0.25rem;
  }

  footer {
    height: 0.5rem;
    background: currentColor;
    margin-top: auto;
  }
`;

const SideNavigationAppShell = () => {
  return (
    <StyledShell>
      <StyledSideNavigationShell>
        <div className='shine icon' />
        <section>
          <div className='shine img' />
          <div className='text'>
            <p className='shine' />
            <p className='shine' />
            <p className='shine' />
          </div>
        </section>
        <footer className='shine' />
      </StyledSideNavigationShell>
    </StyledShell>
  );
};
export default SideNavigationAppShell;
