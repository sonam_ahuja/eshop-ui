import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import React from 'react';
import { StaticRouter } from 'react-router-dom';
import {
  IComponentProps,
  mapStateToProps,
  SideNavigation
} from '@checkout/SideNavigation';
import SideNavigationAppShell from '@checkout/SideNavigation/index.shell';
import appState from '@store/states/app';

describe('<SideNavigation />', () => {
  const initStateValue = appState();
  const props: IComponentProps = {
    translation: appState().translation,
    shouldTermsAndConditionsOpenInNewTab: true,
    termsAndConditionsUrl: 'string'
  };

  test('should render properly', () => {
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SideNavigation {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = {};

    const component = mount(
      <ThemeProvider theme={{}}>
        <SideNavigationAppShell {...appShellProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
});
