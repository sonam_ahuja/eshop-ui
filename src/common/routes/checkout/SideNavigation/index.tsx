import { Anchor, Icon } from 'dt-components';
import { connect } from 'react-redux';
import { StyledSideNavigation } from '@checkout/SideNavigation/styles';
import React, { Component, ReactNode } from 'react';
import FooterWithTermsAndConditions from '@common/components/FooterWithTermsAndConditions';
import { ITranslationState } from '@src/common/store/types/translation';
import { IMainState } from '@src/common/store/reducers/types';
import { RootState } from '@src/common/store/reducers';
import { isMobile } from '@src/common/utils';
import appConstants from '@src/common/constants/appConstants';

interface IProps {
  children?: React.ReactNode;
}

export interface IComponentStateProps {
  translation: ITranslationState;
  termsAndConditionsUrl: string;
  shouldTermsAndConditionsOpenInNewTab: boolean;
}

export type IComponentProps = IComponentStateProps & IProps;

export class SideNavigation extends Component<IComponentProps> {
  constructor(props: IComponentProps) {
    super(props);
  }

  render(): ReactNode {
    const desktopSideBar = (
      <>
        <Anchor
          hreflang={appConstants.LANGUAGE_CODE}
          href='/'
          underline={false}
          className='logoWrap'
        >
          <Icon className='logo' name='ec-dt-logo' size='large' />
        </Anchor>
        {this.props.children}

        <FooterWithTermsAndConditions
          className='sideNavigationFooter'
          termsAndConditionsUrl={this.props.termsAndConditionsUrl}
          shouldTermsAndConditionsOpenInNewTab={
            this.props.shouldTermsAndConditionsOpenInNewTab
          }
          globalTranslation={this.props.translation.cart.global}
        />
      </>
    );

    const mobileEl = this.props.children;

    return (
      <StyledSideNavigation>
        {isMobile.phone ? mobileEl : desktopSideBar}
      </StyledSideNavigation>
    );
  }
}

export const mapStateToProps = (state: IMainState): IComponentStateProps => ({
  translation: state.translation,

  termsAndConditionsUrl:
    state.configuration.cms_configuration.global.termsAndConditionsUrl,
  shouldTermsAndConditionsOpenInNewTab:
    state.configuration.cms_configuration.global
      .shouldTermsAndConditionsOpenInNewTab
});

export default connect<IComponentStateProps, void, void, RootState>(
  mapStateToProps,
  undefined
)(SideNavigation);
