import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledFooter } from '@src/common/components/FooterWithTermsAndConditions/styles';

export const StyledSideNavigation = styled.div`
  background: ${colors.white};
  padding: 1.25rem 1.25rem 1.5rem;

  ${StyledFooter} {
    margin-bottom: 2.25rem;
    display: flex;
    align-items: flex-start;
    width: 100%;
    flex-direction: column;
    font-size: 0.78125rem;
    line-height: 0.9375rem;
    letter-spacing: -0.2px;
    font-weight: normal;
    margin-top: auto;
    .copyText {
      font-size: inherit;
      letter-spacing: -0.2px;
      color: ${colors.mediumGray};
    }
    .dt_anchor {
      font-size: inherit;
      color: ${colors.ironGray};
      letter-spacing: 0px;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: none;
    .logoWrap,
    ${StyledFooter} {
      display: none;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    position: sticky;
    top: 0;
    left: 0;
    width: 14.5rem;
    height: 100vh;
    flex-shrink: 0;
    display: flex;
    flex-direction: column;
    padding: 0 2.25rem;

    .logo {
      height: 7.25rem;
      font-size: 3.5rem;
      padding-top: 0.7rem;
      padding-left: 0.75rem;
      margin-bottom: 2.75rem;
      align-items: center;
      display: flex;
    }

    .logoWrap,
    ${StyledFooter} {
      display: flex;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: 15.8rem;
    .logo {
      padding-left: 2.7rem;
      height: 5.75rem;
      margin-bottom: 1.9rem;
    }
    ${StyledFooter} {
      font-size: 0.625rem;
      line-height: 0.75rem;
      flex-direction: row;
      flex-wrap: wrap;
      margin-bottom: 1.75rem;
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    width: 19.25rem;
    padding-right: 2rem;
    .logo {
      padding-left: 3.25rem;
    }
  }
`;

export const NavigationWrap = styled.div``;
