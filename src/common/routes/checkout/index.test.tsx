import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { ThemeProvider } from 'dt-components';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { StaticRouter } from 'react-router';
import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { RootState } from '@src/common/store/reducers';
import {
  Checkout,
  IProps as ICheckoutProps,
  mapDispatchToProps,
  mapStateToProps
} from '@checkout/index';
import { RouteComponentProps } from 'react-router-dom';

import { CHECKOUT_SUB_ROUTE_TYPE } from './store/enums';
export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export type IRouteProps = RouteComponentProps<IRouterParams>;

// tslint:disable-next-line:no-big-function
describe('<Checkout/>', () => {
  const newbasket = { ...appState().basket };
  newbasket.basketItems = BASKET_ALL_ITEM.cartItems;
  newbasket.cartSummary = BASKET_ALL_ITEM.cartSummary;
  const history: IRouteProps = {
    ...histroyParams,
    location: {
      pathname: CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO,
      search: '',
      hash: '',
      key: 'hmi41z',
      state: ''
    }
  };

  const props: ICheckoutProps = {
    commonError: {
      code: '',
      retryable: false
    },
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    ...history,
    basket: appState().basket,
    personalInfo: appState().checkout.personalInfo,
    checkout: appState().checkout,
    translation: appState().translation,
    configuration: appState().configuration,
    isUserLoggedIn: true,
    setAddressBillingType: jest.fn(),
    setBillingType: jest.fn(),
    fetchBasket: jest.fn(),
    setBillingAddress: jest.fn(),
    setNewAddessShipping: jest.fn(),
    updateNewAddessShipping: jest.fn(),
    fetchCheckoutAddress: jest.fn(),
    setSmsNotification: jest.fn(),
    setProceedToShippingButton: jest.fn(),
    updateInputField: jest.fn(),
    editPaymentStepClick: jest.fn(),
    fetchPaymentMethods: jest.fn(),
    placeOrderButton: jest.fn(),
    setStickySummary: jest.fn(),
    downloadPdf: jest.fn(),
    openTnCModal: jest.fn(),
    fetchBasketAndContact: jest.fn(),
    fetchCheckoutAddressLoading: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const componentWrapper = (newProps: ICheckoutProps) =>
    mount<Checkout>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Checkout {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when billing is activated', () => {
    const newHistory: IRouteProps = {
      ...histroyParams,
      location: {
        pathname: CHECKOUT_SUB_ROUTE_TYPE.BILLING,
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      }
    };
    const newProps: ICheckoutProps = { ...props, ...newHistory };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when order review is activated', () => {
    const newHistory: IRouteProps = {
      ...histroyParams,
      location: {
        pathname: CHECKOUT_SUB_ROUTE_TYPE.ORDER_REVIEW,
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      }
    };
    const newProps: ICheckoutProps = { ...props, ...newHistory };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when payment is activated', () => {
    const newHistory: IRouteProps = {
      ...histroyParams,
      location: {
        pathname: CHECKOUT_SUB_ROUTE_TYPE.PAYMENT,
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      }
    };
    const newProps: ICheckoutProps = { ...props, ...newHistory };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when order shipping is activated', () => {
    const newHistory: IRouteProps = {
      ...histroyParams,
      location: {
        pathname: CHECKOUT_SUB_ROUTE_TYPE.SHIPPING,
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      }
    };
    const newProps: ICheckoutProps = { ...props, ...newHistory };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const component = mount<Checkout>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Checkout {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when unknown path seleted', () => {
    const newHistory: IRouteProps = {
      ...histroyParams,
      location: {
        pathname: '/unknown',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      }
    };
    const newProps: ICheckoutProps = { ...props, ...newHistory };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when unknown path seleted with error code', () => {
    const newProps: ICheckoutProps = { ...props };
    newProps.commonError = {
      httpStatusCode: 500,
      code: '',
      showFullPageError: true,
      retryable: false
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
    newProps.commonError.showFullPageError = false;
    const component1 = componentWrapper(newProps);
    expect(component1).toMatchSnapshot();
  });

  test('should render properly, when unknown path seleted with error code', () => {
    const newProps: ICheckoutProps = { ...props };
    newProps.checkout.checkout.mainAppShell = false;
    newProps.checkout.checkout.creditCheckLoading = true;
    newProps.checkout.checkout.openCreditModal = true;
    newProps.checkout.checkout.openTnCModal = true;
    newProps.basket.showAppShell = false;
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).fetchCheckoutAddress(true);
    mapDispatchToProps(dispatch).fetchPaymentMethods();
    mapDispatchToProps(dispatch).fetchBasket({});
    mapDispatchToProps(dispatch).setSmsNotification();
    mapDispatchToProps(dispatch).setProceedToShippingButton({
      key: 'key',
      value: 'value'
    });
    mapDispatchToProps(dispatch).updateInputField({
      key: 'key',
      value: 'value'
    });
    mapDispatchToProps(dispatch).editPaymentStepClick();
    mapDispatchToProps(dispatch).placeOrderButton();
    mapDispatchToProps(dispatch).setStickySummary(true);
    mapDispatchToProps(dispatch).openTnCModal(true);
    mapDispatchToProps(dispatch).downloadPdf('atring');
    mapDispatchToProps(dispatch).fetchBasketAndContact();
  });
});
