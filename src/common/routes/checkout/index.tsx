import { IState as ICheckoutState } from '@checkout/store/types';
import routes from '@common/routes';
import { RootState } from '@common/store/reducers';
import personalInfoActions from '@checkout/store/personalInfo/actions';
import orderReviewActions from '@checkout/store/orderReview/actions';
import basketActions from '@basket/store/actions';
import checkoutActions from '@checkout/store/actions';
import { IShippingAddressAndNote } from '@checkout/store/shipping/types';
import { ITranslationState } from '@common/store/types/translation';
import {
  IConfigurationState,
  ICurrencyConfiguration
} from '@common/store/types/configuration';
import {
  IInputKeyValue,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';
import React, { Component, ReactNode } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { renderRoutes } from 'react-router-config';
import { CheckoutBody, StyledCheckoutSteps } from '@checkout/styles';
import { IBasketState, IFetchBasketArgs } from '@basket/store/types';
import OrderSummary from '@src/common/components/Summary/desktop';
import Loadable from 'react-loadable';
import {
  billingAddressType,
  billingType,
  IBillingAddressParams
} from '@checkout/store/billing/types';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { isMobile, logInfo, noopFn } from '@src/common/utils';
import paymentActions from '@checkout/store/payment/actions';
import Helmet from 'react-helmet';
import { AppProgressModal } from '@src/common/components/ProgressModal/styles';
import CreditCheck from '@checkout/CheckoutSteps/CreditCheck';
import { getActiveCheckoutStep } from '@checkout/CheckoutSteps/utils';
import SideNavigationAppShell from '@checkout/SideNavigation/index.shell';
import TnC from '@src/common/components/TnC';
import Error from '@src/common/components/Error';
import { IError as IGenricError } from '@common/store/types/common';
import { loadGoogleMaps } from '@src/common/utils/scriptLoad';

import { CHECKOUT_SUB_ROUTE_TYPE } from './store/enums';
import { isCartItemsAndSummaryAvailable } from './utils';

export interface ISEOFields {
  metaTitle: string;
  metaDescription: string;
}

const OrderSummaryShellComponent = Loadable({
  loading: () => null,
  loader: () =>
    import(
      /* webpackChunkName: 'OrderSummaryShellComponent' */ '@common/components/Summary/index.shell'
    )
});

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export interface IProps extends RouteComponentProps<IRouterParams> {
  commonError: IGenricError;
  basket: IBasketState;
  personalInfo: IPersonalInfoState;
  checkout: ICheckoutState;
  translation: ITranslationState;
  configuration: IConfigurationState;
  isUserLoggedIn: boolean;
  currency: ICurrencyConfiguration;
  setAddressBillingType(type: billingAddressType): void;
  setBillingType(data: billingType): void;
  fetchBasket(data: IFetchBasketArgs): void;
  setBillingAddress(data: IBillingAddressParams): void;
  setNewAddessShipping(data: IShippingAddressAndNote): void;
  updateNewAddessShipping(data: IShippingAddressAndNote): void;
  fetchCheckoutAddress(data: boolean): void;
  setSmsNotification(): void;
  setProceedToShippingButton(data: IInputKeyValue): void;
  updateInputField(data: IInputKeyValue): void;
  editPaymentStepClick(): void;
  fetchPaymentMethods(): void;
  placeOrderButton(): void;
  setStickySummary(data: boolean): void;
  openTnCModal(data: boolean): void;
  downloadPdf(url: string): void;
  fetchBasketAndContact(): void;
  fetchCheckoutAddressLoading(data: boolean): void;
}

export class Checkout extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.evaluateSEO = this.evaluateSEO.bind(this);
  }

  componentDidMount(): void {
    document.body.classList.remove('hasBasketStrip');
    loadGoogleMaps(
      this.props.configuration.cms_configuration.global.googleMap.apiKey,
      () => {
        logInfo('Google map script loaded');
      }
    );
    // @fetchBasketAndContact  is used for calling basket and contact api synchrounously
    this.props.fetchBasketAndContact();
  }

  evaluateSEO(route: CHECKOUT_SUB_ROUTE_TYPE): ISEOFields {
    const metaObj = {
      metaTitle: '',
      metaDescription: ''
    };

    const {
      personalInfo,
      shipping,
      payment,
      billing,
      orderReview
    } = this.props.translation.cart.global.checkout;

    switch (route) {
      case CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO:
        metaObj.metaTitle = personalInfo.title;
        metaObj.metaDescription = personalInfo.description;
        break;

      case CHECKOUT_SUB_ROUTE_TYPE.SHIPPING:
        metaObj.metaTitle = shipping.title;
        metaObj.metaDescription = shipping.description;
        break;

      case CHECKOUT_SUB_ROUTE_TYPE.PAYMENT:
        metaObj.metaTitle = payment.title;
        metaObj.metaDescription = payment.description;
        break;

      case CHECKOUT_SUB_ROUTE_TYPE.BILLING:
        metaObj.metaTitle = billing.title;
        metaObj.metaDescription = billing.description;
        break;

      case CHECKOUT_SUB_ROUTE_TYPE.ORDER_REVIEW:
        metaObj.metaTitle = orderReview.title;
        metaObj.metaDescription = orderReview.description;
        break;

      default:
        break;
    }

    return metaObj;
  }

  // tslint:disable-next-line:cognitive-complexity
  render(): ReactNode {
    const {
      basket,
      checkout,
      configuration,
      translation,
      isUserLoggedIn,
      placeOrderButton,
      openTnCModal,
      currency,
      commonError
    } = this.props;

    const { creditCheck } = this.props.translation.cart.checkout;
    const creditLoadingTitle = creditCheck.creditCheckInProcess.replace(
      '{0}',
      checkout.personalInfo.formFields.firstName
    );

    const activeCheckoutStep = getActiveCheckoutStep(
      this.props.location
    ) as CHECKOUT_SUB_ROUTE_TYPE;

    const orderSummary = isCartItemsAndSummaryAvailable(basket) ? (
      <OrderSummary
        configuration={configuration}
        cartSummary={basket.cartSummary}
        translation={translation}
        currency={currency}
        isCheckoutEnable={false}
        proceedToCheckoutLoading={checkout.orderReview.placeOrderLoading}
        isTermsConditionChecked={checkout.orderReview.agreeOnTermsAndCondition}
        hideCheckoutButton={
          activeCheckoutStep !== CHECKOUT_SUB_ROUTE_TYPE.ORDER_REVIEW
        }
        goToLogin={noopFn}
        isUserLoggedIn={isUserLoggedIn}
        isCheckoutSummary={true}
        placeOrderButton={placeOrderButton}
        isSticky={true}
        stickySummary={basket.stickySummary}
      />
    ) : null;

    const { metaTitle, metaDescription } = this.evaluateSEO(activeCheckoutStep);

    return (
      <CheckoutBody>
        <Helmet>
          {<title>{metaTitle}</title>}
          {<meta name='Description' content={metaDescription} />}
        </Helmet>
        {commonError.httpStatusCode && commonError.showFullPageError ? (
          <Error />
        ) : (
          <>
            {commonError.httpStatusCode && <Error />}
            {getActiveCheckoutStep(this.props.location) !==
              CHECKOUT_SUB_ROUTE_TYPE.ORDER_REVIEW && (
              <>
                {!isMobile.phone && checkout.checkout.sideAppShell ? (
                  <SideNavigationAppShell />
                ) : null}
              </>
            )}
            <StyledCheckoutSteps>
              {renderRoutes(
                routes.filter(route => route.basePath === '/checkout')[0]
                  .childRoutes
              )}
            </StyledCheckoutSteps>

            {checkout.checkout.creditCheckLoading ? (
              <AppProgressModal isOpen={true} title={creditLoadingTitle} />
            ) : null}

            {checkout.checkout.openCreditModal ? <CreditCheck /> : null}
            {checkout.checkout.openTnCModal && (
              <TnC openTnCModal={openTnCModal} {...this.props} />
            )}

            {basket.showAppShell ? (
              <OrderSummaryShellComponent isButtonVisible={false} />
            ) : (
              orderSummary
            )}
          </>
        )}
      </CheckoutBody>
    );
  }
}

export const mapStateToProps = (state: RootState) => ({
  commonError: state.common.error,
  basket: state.basket,
  checkout: state.checkout,
  configuration: state.configuration,
  translation: state.translation,
  isUserLoggedIn: state.common.isLoggedIn,
  currency: state.configuration.cms_configuration.global.currency
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchCheckoutAddress(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddress(data));
  },
  fetchPaymentMethods(): void {
    dispatch(paymentActions.fetchPaymentInfoRequested(undefined));
  },
  fetchBasket(data: IFetchBasketArgs): void {
    dispatch(basketActions.fetchBasket(data));
  },
  setSmsNotification(): void {
    dispatch(personalInfoActions.setSMSNotification());
  },
  setProceedToShippingButton(data: { key: string; value: string }): void {
    dispatch(personalInfoActions.setProceedToShippingButton(data));
  },
  updateInputField(data: { key: string; value: string }): void {
    dispatch(personalInfoActions.updateInputField(data));
  },
  editPaymentStepClick(): void {
    dispatch(paymentActions.billingToPaymentStepUpdate(true)); // to update upfront data
  },
  placeOrderButton(): void {
    dispatch(orderReviewActions.placeOrder());
  },
  setStickySummary(data: boolean): void {
    dispatch(basketActions.setStickySummary(data));
  },
  openTnCModal(data: boolean): void {
    dispatch(checkoutActions.openTnCModal(data));
  },
  downloadPdf(url: string): void {
    dispatch(orderReviewActions.downloadPdf(url));
  },
  fetchBasketAndContact(): void {
    dispatch(checkoutActions.fetchBasketAndContact());
  },
  fetchCheckoutAddressLoading(data: boolean): void {
    dispatch(checkoutActions.fetchCheckoutAddressLoading(data));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
  // tslint:disable-next-line:max-file-line-count
)(withRouter(Checkout));
