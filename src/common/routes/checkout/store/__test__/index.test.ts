import checkoutReducer from '@checkout/store';
import appState from '@store/states/app';

describe('checkoutReducer', () => {
  it('checkoutReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(checkoutReducer(appState().checkout, definedAction)).toEqual(
      appState().checkout
    );
  });
});
