import checkout from '@checkout/store/reducers';
import checkoutState from '@checkout/store/state';
import { ICheckoutState } from '@checkout/store/types';
import actions from '@checkout/store/actions';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';

describe('Checkout Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ICheckoutState = checkout(
    checkoutState(),
    definedAction
  );

  beforeEach(() => {
    initialStateValue = checkoutState();
  });
  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('FETCH_CHECKOUT_ADDRESS_LOADING should mutate billing state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      loading: boolean;
      mainAppShell: boolean;
      sideAppShell: boolean;
    } = {
      loading: true,
      mainAppShell: true,
      sideAppShell: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.fetchCheckoutAddressLoading(true);
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);
  });

  it('CREDIT_SCORE_ERROR should mutate billing state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      loading: boolean;
      mainAppShell: boolean;
      sideAppShell: boolean;
    } = {
      loading: false,
      mainAppShell: true,
      sideAppShell: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.creditScoreError();
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);
  });

  it('CREDIT_QUALIFICATION_SUCCESS should mutate checkout state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      loading: boolean;
      mainAppShell: boolean;
      sideAppShell: boolean;
    } = {
      loading: false,
      mainAppShell: true,
      sideAppShell: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.creditQualificationSuccess({ status: false });
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);

    const newAction = actions.creditQualificationSuccess({ status: true });
    const oldNewState = checkout(initialStateValue, newAction);
    expect(oldNewState).toEqual(expectedState);
  });

  it('FETCH_CHECKOUT_ADDRESS_SUCCESS should mutate checkout state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      loading: boolean;
      mainAppShell: boolean;
      sideAppShell: boolean;
      isAddressFetched: boolean;
    } = {
      loading: false,
      mainAppShell: false,
      sideAppShell: false,
      isAddressFetched: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.fetchCheckoutAddressSuccess();
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);
  });

  it('EVALUATE_CART_SUMMARY_PRICE should mutate checkout state address params for personal info', () => {
    const action = actions.evaluateCartSummaryPrice(
      BASKET_ALL_ITEM.cartSummary
    );
    const newState = checkout(initialStateValue, action);
    expect(newState).toBeDefined();
  });

  it('FETCH_CREDIT_SCORE_LOADING should mutate checkout state address params for personal info', () => {
    const action = actions.fetchCreditLoading();
    const newState = checkout(initialStateValue, action);
    expect(newState).toBeDefined();
  });

  it('CREDIT_SCORE_SUCCESS should mutate checkout state address params for personal info', () => {
    const action = actions.creditScoreSuccess({
      code: CREDIT_SCORE_CODE.CREDIT_SUCCESS,
      status: '201'
    });
    const newState = checkout(initialStateValue, action);
    expect(newState).toBeDefined();
  });

  it('IS_CREDIT_CHECK_REQUIRED_SUCCESS should mutate checkout state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      isCreditCheckRequired: boolean;
    } = {
      isCreditCheckRequired: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.isCreditCheckRequiredSuccess({
      status: true
    });
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);
  });

  it('OPEN_TNC_MODAL should mutate checkout state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      openTnCModal: boolean;
    } = {
      openTnCModal: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.openTnCModal(true);
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);
  });

  it('TERMS_AND_CONDITION_SUCCESS should mutate checkout state address params for personal info', () => {
    const defaultState = checkoutState();
    const mutation: {
      termsAndConditionData: boolean;
    } = {
      termsAndConditionData: true
    };
    const expectedState = { ...defaultState, ...mutation };
    const action = actions.fetchTnCSuccess(true);
    const newState = checkout(initialStateValue, action);
    expect(newState).toEqual(expectedState);
  });
});
