import checkoutSaga, {
  checkCreditScore,
  creditCheckQualification,
  evaluateCartSummary,
  fetchCheckoutAddress,
  fetchDeliveryInfo,
  fetchNearByStores,
  showTermsAndConditions
} from '@checkout/store/sagas';

describe('checkout Saga', () => {
  it('checkout saga test', () => {
    const generator = checkoutSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('checkCreditScore saga test', () => {
    const generator = checkCreditScore({
      type: 'string',
      payload: 'string'
    });
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('creditCheckQualification saga test', () => {
    const generator = creditCheckQualification();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('evaluateCartSummary saga test', () => {
    const generator = evaluateCartSummary();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchCheckoutAddress saga test', () => {
    const generator = fetchCheckoutAddress({ type: 'string', payload: true });
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchDeliveryInfo saga test', () => {
    const generator = fetchDeliveryInfo();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchNearByStores saga test', () => {
    const generator = fetchNearByStores();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('showTermsAndConditions saga test', () => {
    const generator = showTermsAndConditions();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
