import {
  fetchCheckoutAddressService,
  fetchCreditCheckQualification,
  fetchCreditScore,
  fetchDeliveryInfoService,
  fetchNearByStoresService,
  fetchTnC
} from '@checkout/store/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { BILLING_TYPE } from '@checkout/store/enums';
import { logError } from '@src/common/utils';

describe('checkout Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchCheckoutAddressService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.GET_PERSONA_INFO_DATA;
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchCheckoutAddressService();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('fetchCreditCheckQualification success test', async () => {
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost('deliveryInfo').replyOnce(200, addressPayload);
    axios
      .post('deliveryInfo')
      .then(async () => {
        const result = await fetchCreditCheckQualification();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('fetchDeliveryInfoService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.BILLING.E_BILL;
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchDeliveryInfoService();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('fetchNearByStoresService success test', async () => {
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost('nearbyStores').replyOnce(200, addressPayload);
    axios
      .post('nearbyStores')
      .then(async () => {
        const result = await fetchNearByStoresService();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('fetchCreditScore success test', async () => {
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock
      .onPost(apiEndpoints.CHECKOUT.GET_CREDIT_QUALIFY.url)
      .replyOnce(200, addressPayload);
    axios
      .post(apiEndpoints.CHECKOUT.GET_CREDIT_QUALIFY.url)
      .then(async () => {
        const result = await fetchCreditScore();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('fetchTnC success test', async () => {
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock
      .onPost(apiEndpoints.CHECKOUT.GET_TERMS_AND_CONDITION.url)
      .replyOnce(200, addressPayload);
    axios
      .post(apiEndpoints.CHECKOUT.GET_TERMS_AND_CONDITION.url)
      .then(async () => {
        const result = await fetchTnC();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });
});
