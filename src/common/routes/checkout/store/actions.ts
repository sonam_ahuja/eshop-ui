import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/constants';
import { ICartSummary } from '@basket/store/types';
import {
  ICreditQualifyResponse,
  ICreditScoreResponse
} from '@checkout/store/types';

export default {
  evaluateCartSummaryPrice: actionCreator<ICartSummary[]>(
    CONSTANTS.EVALUATE_CART_SUMMARY_PRICE
  ),
  setLoadingState: actionCreator<undefined>(CONSTANTS.CHECKOUT_MODULE_LOADING),

  fetchCheckoutAddress: actionCreator<boolean>(
    CONSTANTS.FETCH_CHECKOUT_ADDRESS_REQUESTED
  ),
  fetchCheckoutAddressLoading: actionCreator<boolean>(
    CONSTANTS.FETCH_CHECKOUT_ADDRESS_LOADING
  ),
  fetchCheckoutAddressSuccess: actionCreator<undefined>(
    CONSTANTS.FETCH_CHECKOUT_ADDRESS_SUCCESS
  ),
  fetchCheckoutAddressError: actionCreator<undefined>(
    CONSTANTS.FETCH_CHECKOUT_ADDRESS_ERROR
  ),
  fetchDeliveryInfo: actionCreator<undefined>(
    CONSTANTS.FETCH_DELIVERY_INFO_REQUESTED
  ),
  fetchDeliveryInfoLoading: actionCreator<undefined>(
    CONSTANTS.FETCH_DELIVERY_INFO_LOADING
  ),
  fetchDeliveryInfoSuccess: actionCreator<undefined>(
    CONSTANTS.FETCH_DELIVERY_INFO_SUCCESS
  ),
  fetchDeliveryInfoError: actionCreator<Error>(
    CONSTANTS.FETCH_DELIVERY_INFO_ERROR
  ),

  fetchNearByStores: actionCreator<undefined>(
    CONSTANTS.FETCH_NEARBY_STORES_REQUESTED
  ),
  fetchNearByStoresLoading: actionCreator<undefined>(
    CONSTANTS.FETCH_NEARBY_STORES_LOADING
  ),
  fetchNearByStoresSuccess: actionCreator<undefined>(
    CONSTANTS.FETCH_NEARBY_STORES_SUCCESS
  ),
  fetchNearByStoresError: actionCreator<Error>(
    CONSTANTS.FETCH_NEARBY_STORES_ERROR
  ),

  fetchCreditLoading: actionCreator<undefined>(
    CONSTANTS.FETCH_CREDIT_SCORE_LOADING
  ),
  fetchCreditCheckQualification: actionCreator<undefined>(
    CONSTANTS.CREDIT_CHECK_QUALIFY_REQUESTED
  ),
  fetchCreditCheckScore: actionCreator<string | void>(
    CONSTANTS.CREDIT_CHECK_SCORE_REQUESTED
  ),
  creditScoreSuccess: actionCreator<ICreditScoreResponse>(
    CONSTANTS.CREDIT_SCORE_SUCCESS
  ),
  creditScoreError: actionCreator<undefined>(CONSTANTS.CREDIT_SCORE_ERROR),
  creditQualificationSuccess: actionCreator<ICreditQualifyResponse>(
    CONSTANTS.CREDIT_QUALIFICATION_SUCCESS
  ),
  creditQualificationError: actionCreator<undefined>(
    CONSTANTS.CREDIT_QUALIFICATION_ERROR
  ),
  isCreditCheckRequired: actionCreator<string | void>(
    CONSTANTS.IS_CREDIT_CHECK_REQUIRED
  ),
  isCreditCheckRequiredSuccess: actionCreator<ICreditQualifyResponse>(
    CONSTANTS.IS_CREDIT_CHECK_REQUIRED_SUCCESS
  ),
  showTermsAndConditions: actionCreator<void>(CONSTANTS.SHOW_TERMS_CONDTIONS),
  openTnCModal: actionCreator<boolean>(CONSTANTS.OPEN_TNC_MODAL),
  fetchTnCSuccess: actionCreator<boolean>(
    CONSTANTS.TERMS_AND_CONDITION_SUCCESS
  ),

  fetchBasketAndContact: actionCreator<void>(
    CONSTANTS.FETCH_BASKET_AND_CONTACTS
  )
};
