import actions from '@checkout/store/billing/actions';
import constants from '@checkout/store/billing/constants';
import { BILLING_ADDRESS_TYPE, BILLING_TYPE } from '@checkout/store/enums';
import { IUpdateBillingAddress } from '@checkout/store/billing/types';

describe('billing actions', () => {
  it('setBillingType action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SET_BILLING_TYPE,
      payload: {
        billType: BILLING_TYPE.E_BILL,
        showAppShell: true
      }
    };
    expect(
      actions.setBillingType({
        billType: BILLING_TYPE.E_BILL,
        showAppShell: true
      })
    ).toEqual(expectedAction);
  });

  it('setAddressBillingType action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SET_BILLING_ADDRESS_TYPE,
      payload: BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS
    };
    expect(
      actions.setBillingAddressType(BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS)
    ).toEqual(expectedAction);
  });

  it('setBillingAddress action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SEND_BILLING_ADDRESS
    };
    expect(actions.sendBillingAddress()).toEqual(expectedAction);
  });

  it('setBillingAddressError action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SEND_BILLING_ADDRESS_ERROR
    };
    expect(actions.sendBillingAddressError()).toEqual(expectedAction);
  });

  it('setBillingAddressLoading action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SEND_BILLING_ADDRESS_LOADING
    };
    expect(actions.sendBillingAddressLoading()).toEqual(expectedAction);
  });

  it('updateBillingAddressRequested action creator should return a object with expected value', () => {
    const payload = {
      key: '',
      value: '',
      isValid: true,
      validationMessage: ''
    };
    const expectedAction = {
      type: constants.UPDATE_BILLING_ADDRESS,
      payload
    };
    expect(
      actions.updateBillingAddress(payload as IUpdateBillingAddress)
    ).toEqual(expectedAction);
  });

  it('updateBillingAddressSuccess action creator should return a object with expected value', () => {
    const payload = {
      key: '',
      value: '',
      isValid: false,
      validationMessage: ''
    };
    const expectedAction = {
      type: constants.UPDATE_BILLING_ADDRESS,
      payload
    };
    expect(actions.updateBillingAddress(payload)).toEqual(expectedAction);
  });
  it('update personal info dummy sate action with valid data ', () => {
    const payload = {
      streetAddress: {
        colDesktop: 4,
        colMobile: 4,
        value: '',
        isValid: false,
        validationMessage: ''
      }
    };
    const expectedAction = {
      type: constants.UPDATE_PERSONAL_INFO_DUMMY_STATE,
      payload
    };
    expect(actions.updatePersonalInfoDummyState(payload)).toEqual(
      expectedAction
    );
  });
});
