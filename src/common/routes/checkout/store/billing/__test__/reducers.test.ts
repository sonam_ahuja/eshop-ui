import billing from '@checkout/store/billing/reducers';
import actions from '@checkout/store/billing/actions';
import shippingAddressState from '@checkout/store/shipping/state';
import { BILLING_TYPE } from '@checkout/store/enums';
import {
  IBillingAddressResponse,
  IBillingState,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import appState from '@store/states/app';

describe('Billing Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IBillingState = billing(undefined, definedAction);

  const initializeValue = () => {
    initialStateValue = billing(undefined, definedAction);
  };
  beforeEach(() => {
    initializeValue();
  });

  it('SEND_BILLING_ADDRESS_LOADING should mutate address field parameter in state', () => {
    const paperBillAction = actions.sendBillingAddressLoading();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SEND_BILLING_ADDRESS_ERROR should mutate address field parameter in state', () => {
    const paperBillAction = actions.sendBillingAddressError();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('ENABLE_PROCEED_BUTTON should mutate address field parameter in state', () => {
    const paperBillAction = actions.enableProceedButton(true);
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('UPDATE_FORM_CHANGE_FIELD should mutate address field parameter in state', () => {
    const paperBillAction = actions.updateFormChangeField();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SET_BILLING_PERSONAL_INFO_DUMMY_STATE should mutate address field parameter in state', () => {
    const paperBillAction = actions.setPersonalInfoDummyState(
      appState().checkout.personalInfo
    );
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('FETCH_BILLING_ADDRESS_ERROR should mutate address field parameter in state', () => {
    const paperBillAction = actions.fetchBillingAddressError();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SET_BILLING_SHIPPING_INFO_DUMMY_STATE should mutate address field parameter in state', () => {
    const paperBillAction = actions.setShippingInfoDummyState(
      shippingAddressState()
    );
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SET_SAME_AS_PERSONAL_ON_ERROR should mutate address field parameter in state', () => {
    const paperBillAction = actions.setSameAsPersonalOnError(
      appState().checkout.personalInfo
    );
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('UPDATE_BILLING_ADDRESS should mutate address field parameter in state', () => {
    const payload: IUpdateBillingAddress = {
      key: 'key',
      value: 'value',
      isValid: true,
      validationMessage: 'message'
    };
    const action = actions.updateBillingAddress(payload);
    const paperBillState = billing(initialStateValue, action);
    expect(paperBillState).toBeDefined();
  });

  it('SEND_BILLING_ADDRESS_SUCCESS should mutate address field parameter in state', () => {
    const payload: IBillingAddressResponse = {
      sameAsPersonalInfo: true,
      billingAddress: [],
      sameAsShippingInfo: false,
      typeOfBill: BILLING_TYPE.E_BILL
    };
    const paperBillAction = actions.sendBillingAddressSuccess(payload);
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('BILL_TYPE_SUCCESS should mutate address field parameter in state', () => {
    const paperBillAction = actions.billTypeSuccess({
      fee: 2,
      billApplicable: false,
      ebillApplicable: false,
      paperBillApplicable: false
    });
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });
});
