import billing from '@checkout/store/billing/reducers';
import actions from '@checkout/store/billing/actions';
import shippingAddressState from '@checkout/store/shipping/state';
import { BILLING_ADDRESS_TYPE, BILLING_TYPE } from '@checkout/store/enums';
import {
  IBillingAddressResponse,
  IBillingState,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import appState from '@store/states/app';
import { billingForm, billingInfoResponse } from '@mocks/checkout/billing';

describe('Billing Reducer 2', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IBillingState = billing(undefined, definedAction);

  const initializeValue = () => {
    initialStateValue = billing(undefined, definedAction);
  };
  beforeEach(() => {
    initializeValue();
  });

  it('SEND_BILLING_ADDRESS_LOADING should mutate address field parameter in state', () => {
    const paperBillAction = actions.sendBillingAddressLoading();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SEND_BILLING_ADDRESS_ERROR should mutate address field parameter in state', () => {
    const paperBillAction = actions.sendBillingAddressError();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('ENABLE_PROCEED_BUTTON should mutate address field parameter in state', () => {
    const paperBillAction = actions.enableProceedButton(true);
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('UPDATE_FORM_CHANGE_FIELD should mutate address field parameter in state', () => {
    const paperBillAction = actions.updateFormChangeField();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SET_BILLING_PERSONAL_INFO_DUMMY_STATE should mutate address field parameter in state', () => {
    const paperBillAction = actions.setPersonalInfoDummyState(
      appState().checkout.personalInfo
    );
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('FETCH_BILLING_ADDRESS_ERROR should mutate address field parameter in state', () => {
    const paperBillAction = actions.fetchBillingAddressError();
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SET_BILLING_SHIPPING_INFO_DUMMY_STATE should mutate address field parameter in state', () => {
    const paperBillAction = actions.setShippingInfoDummyState(
      shippingAddressState()
    );
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('SET_SAME_AS_PERSONAL_ON_ERROR should mutate address field parameter in state', () => {
    const paperBillAction = actions.setSameAsPersonalOnError(
      appState().checkout.personalInfo
    );
    const paperBillState = billing(initialStateValue, paperBillAction);
    expect(paperBillState).toBeDefined();
  });

  it('UPDATE_BILLING_ADDRESS should mutate address field parameter in state', () => {
    const payload: IUpdateBillingAddress = {
      key: 'key',
      value: 'value',
      isValid: true,
      validationMessage: 'message'
    };
    const action = actions.updateBillingAddress(payload);
    const state = billing(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SEND_BILLING_ADDRESS_SUCCESS should mutate address field parameter in state', () => {
    const payload: IBillingAddressResponse = {
      sameAsPersonalInfo: true,
      billingAddress: [],
      sameAsShippingInfo: false,
      typeOfBill: BILLING_TYPE.E_BILL
    };
    const action = actions.sendBillingAddressSuccess(payload);
    const state = billing(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('BILL_TYPE_SUCCESS should mutate address field parameter in state', () => {
    const action = actions.billTypeSuccess({
      fee: 2,
      billApplicable: false,
      ebillApplicable: false,
      paperBillApplicable: false
    });
    const state = billing(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_BILLING_ADDRESS_SUCCESS should mutate bill type field parameter in state', () => {
    const action = actions.fetchBillingAddressSuccess(billingInfoResponse);
    const state = billing(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SET_BILLING_TYPE should mutate bill type field parameter in state', () => {
    const action = actions.setBillingType({
      billType: BILLING_TYPE.E_BILL,
      showAppShell: true
    });
    const state = billing(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SET_BILLING_ADDRESS_TYPE should mutate address field parameter in state', () => {
    const action1 = actions.setBillingAddressType(
      BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS
    );

    const action2 = actions.setBillingAddressType(
      BILLING_ADDRESS_TYPE.PERSONAL_INFO
    );
    const action3 = actions.setBillingAddressType(
      BILLING_ADDRESS_TYPE.SHIPPING_INFO
    );
    const state1 = billing(initialStateValue, action1);
    expect(state1).toBeDefined();
    const state2 = billing(initialStateValue, action2);
    expect(state2).toBeDefined();
    const state3 = billing(initialStateValue, action3);
    expect(state3).toBeDefined();
  });

  it('UPDATE_PERSONAL_INFO_DUMMY_STATE should mutate bill type field parameter in state', () => {
    const action = actions.updatePersonalInfoDummyState(billingForm);
    const state = billing(initialStateValue, action);
    expect(state).toBeDefined();
  });
});
