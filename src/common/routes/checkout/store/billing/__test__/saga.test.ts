import billingSaga, {
  bill,
  editPaymentStep,
  fetchBillingError,
  sendBillingAddress,
  setBillingAddressType
} from '@checkout/store/billing/sagas';
import CONSTANTS from '@checkout/store/billing/constants';
import { BILLING_ADDRESS_TYPE, BILLING_TYPE } from '@checkout/store/enums';
import { billingAddressType, billingType } from '@checkout/store/billing/types';
describe('Billing Saga', () => {
  it('billingSaga saga test', () => {
    const generator = billingSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('editPaymentStep saga test', () => {
    const payload: { type: string; payload: string } = {
      type: CONSTANTS.EDIT_PAYMENT_STEP,
      payload: 'some string'
    };

    const generator = editPaymentStep(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchBillingError saga test', () => {
    const generator = fetchBillingError();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('billingSaga saga test', () => {
    const generator = sendBillingAddress();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('bill saga test', () => {
    const payload: {
      type: string;
      payload: { billType: billingType; showAppShell: boolean };
    } = {
      type: CONSTANTS.SET_BILLING_TYPE,
      payload: { billType: BILLING_TYPE.PAPER_BILL, showAppShell: true }
    };
    const generator = bill(payload);
    expect(generator.next().value).toBeUndefined();
    expect(generator.next().value).toBeUndefined();
    expect(generator.next().value).toBeUndefined();
    expect(generator.next().value).toBeUndefined();
  });

  // tslint:disable: no-identical-functions
  it('bill saga test with ebill', () => {
    const payload: {
      type: string;
      payload: { billType: billingType; showAppShell: boolean };
    } = {
      type: CONSTANTS.SET_BILLING_TYPE,
      payload: { billType: BILLING_TYPE.PAPER_BILL, showAppShell: true }
    };
    const generator = bill(payload);
    expect(generator.next().value).toBeUndefined();
    expect(generator.next().value).toBeUndefined();
    expect(generator.next().value).toBeUndefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('setBillingAddressType saga test', () => {
    const payload: { type: string; payload: billingAddressType } = {
      type: CONSTANTS.SET_BILLING_ADDRESS_TYPE,
      payload: BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS
    };
    const generator = setBillingAddressType(payload);
    expect(generator.next().value).toBeUndefined();
  });

  it('setBillingAddressType saga test with personal info', () => {
    const payload: { type: string; payload: billingAddressType } = {
      type: CONSTANTS.SET_BILLING_ADDRESS_TYPE,
      payload: BILLING_ADDRESS_TYPE.PERSONAL_INFO
    };
    const generator = setBillingAddressType(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('setBillingAddressType saga test, shipping', () => {
    const payload: { type: string; payload: billingAddressType } = {
      type: CONSTANTS.SET_BILLING_ADDRESS_TYPE,
      payload: BILLING_ADDRESS_TYPE.SHIPPING_INFO
    };
    const generator = setBillingAddressType(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
