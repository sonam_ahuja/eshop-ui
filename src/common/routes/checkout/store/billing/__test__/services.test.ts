import {
  addBillingAddressService,
  billService,
  updateBillingAddressService
} from '@checkout/store/billing/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { BILLING_TYPE } from '@checkout/store/enums';
import { logError } from '@src/common/utils';

describe('billing Service', () => {
  const mock = new MockAdapter(axios);
  it('addBillingAddressService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.BILLING.ADD_BILLING_ADDRESS;
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await addBillingAddressService({
          address: {
            streetNumber: 'streetNumber',
            address: 'ADDRESS'
          },
          typeOfBill: BILLING_TYPE.E_BILL
        });
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('updateBillingAddressService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.BILLING.UPDATE_BILLING_ADDRESS;
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await updateBillingAddressService({
          isChanged: true,
          address: {
            streetNumber: 'streetNumber',
            address: 'ADDRESS'
          },
          typeOfBill: BILLING_TYPE.E_BILL
        });
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('eBillService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.BILLING.E_BILL;
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await billService(BILLING_TYPE.E_BILL);
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('paperBillService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.BILLING.PAPER_BILL;
    const addressPayload = {
      address: {
        streetNumber: 'streetNumber',
        address: 'ADDRESS'
      },
      typeOfBill: BILLING_TYPE.E_BILL
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await billService(BILLING_TYPE.PAPER_BILL);
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });
});
