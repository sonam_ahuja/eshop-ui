import {
  patchBillingAdderessData,
  postBillingAdderessData
} from '@checkout/store/billing/state';

describe('billing selector', () => {
  it('mapBillingInfoFieldsToBilling  test', () => {
    expect(patchBillingAdderessData()).toBeDefined();
  });

  it('mapBillingIpostBillingAdderessDatanfoFieldsToBilling  test', () => {
    expect(postBillingAdderessData()).toBeDefined();
  });
});
