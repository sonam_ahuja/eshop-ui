import {
  payloadForPatchBilling,
  payloadForPatchMainStoreBilling,
  payloadForPostBilling,
  updateDummyStateAfterGetData,
  updateStateAfterSuccess
} from '@checkout/store/billing/transformer';
import { billingInfoResponse } from '@mocks/checkout/billing';
import billingState from '@checkout/store/billing/state';

describe('Transformer ', () => {
  it('payloadForPostBilling  test', () => {
    expect(payloadForPostBilling(billingState())).toBeDefined();
  });

  it('payloadForPatchBilling  test', () => {
    expect(payloadForPatchBilling(billingState())).toBeDefined();
  });

  it('payloadForPatchMainStoreBilling  test', () => {
    expect(payloadForPatchMainStoreBilling(billingState())).toBeDefined();
  });

  it('updateStateAfterSuccess test', () => {
    expect(updateStateAfterSuccess(billingState())).toBeUndefined();
  });

  it('updateDummyStateAfterGetData test', () => {
    expect(
      updateDummyStateAfterGetData(billingState(), billingInfoResponse)
    ).toBeUndefined();
  });
});
