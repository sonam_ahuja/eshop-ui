import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/billing/constants';
import {
  billingAddressType,
  billingType,
  IBillingAddressResponse,
  IBillingFormField,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import { IBillingInfoResponse } from '@checkout/store/types';

import { IPersonalInfoState } from '../personalInfo/types';
import { IShippingAddressState } from '../shipping/types';

export default {
  enableProceedButton: actionCreator<boolean>(CONSTANTS.ENABLE_PROCEED_BUTTON),
  fetchBillingAddressSuccess: actionCreator<IBillingInfoResponse>(
    CONSTANTS.FETCH_BILLING_ADDRESS_SUCCESS
  ),
  fetchBillingAddressError: actionCreator<void>(
    CONSTANTS.FETCH_BILLING_ADDRESS_ERROR
  ),
  updatePersonalInfoDummyState: actionCreator<IBillingFormField>(
    CONSTANTS.UPDATE_PERSONAL_INFO_DUMMY_STATE
  ),
  updateShippingInfoDummyState: actionCreator<IBillingFormField>(
    CONSTANTS.UPDATE_SHIPPING_INFO_DUMMY_STATE
  ),
  updateBillingAddress: actionCreator<IUpdateBillingAddress>(
    CONSTANTS.UPDATE_BILLING_ADDRESS
  ),
  sendBillingAddress: actionCreator(CONSTANTS.SEND_BILLING_ADDRESS),
  sendBillingAddressLoading: actionCreator(
    CONSTANTS.SEND_BILLING_ADDRESS_LOADING
  ),

  sendBillingAddressSuccess: actionCreator<IBillingAddressResponse>(
    CONSTANTS.SEND_BILLING_ADDRESS_SUCCESS
  ),
  sendBillingAddressError: actionCreator(CONSTANTS.SEND_BILLING_ADDRESS_ERROR),
  updateFormChangeField: actionCreator(CONSTANTS.UPDATE_FORM_CHANGE_FIELD),
  setBillingType: actionCreator<{
    billType: billingType;
    showAppShell: boolean;
  }>(CONSTANTS.SET_BILLING_TYPE),

  setBillingAddressType: actionCreator<billingAddressType>(
    CONSTANTS.SET_BILLING_ADDRESS_TYPE
  ),
  setPersonalInfoDummyState: actionCreator<IPersonalInfoState>(
    CONSTANTS.SET_BILLING_PERSONAL_INFO_DUMMY_STATE
  ),
  setSameAsPersonalOnError: actionCreator<IPersonalInfoState>(
    CONSTANTS.SET_SAME_AS_PERSONAL_ON_ERROR
  ),
  setShippingInfoDummyState: actionCreator<IShippingAddressState>(
    CONSTANTS.SET_BILLING_SHIPPING_INFO_DUMMY_STATE
  ),
  editPaymentStep: actionCreator<string>(CONSTANTS.EDIT_PAYMENT_STEP),
  showBillingLoader: actionCreator<boolean>(CONSTANTS.SHOW_BILLING_LOADER),
  billTypeSuccess: actionCreator<{
    fee: number;
    billApplicable: boolean;
    ebillApplicable: boolean;
    paperBillApplicable: boolean;
  }>(CONSTANTS.BILL_TYPE_SUCCESS)
};
