export enum BILLING_CONSTANTS {
  STREET_ADDRESS = 'streetAddress',
  STREET_NUMBER = 'streetNumber',
  FLAT_NUMBER = 'flatNumber',
  CITY = 'city',
  POST_CODE = 'postCode',
  DELIVERY_NOTE = 'deliveryNote',
  ADDRESS = 'address',
  NOTES = 'notes',
  ID = 'id',
  STREET_ADDRESS_ID = 'streetAddressId'
}
