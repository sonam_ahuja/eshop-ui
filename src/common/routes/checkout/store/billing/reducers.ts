import { AnyAction, Reducer } from 'redux';
import initialState from '@checkout/store/billing/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@checkout/store/billing/constants';
import {
  billingAddressType,
  billingType,
  IBillingAddressResponse,
  IBillingFormField,
  IBillingState,
  IUpdateBillingAddress
} from '@checkout/store/billing/types';
import {
  updateDummyStateAfterGetData,
  updateStateAfterSuccess
} from '@checkout/store/billing/transformer';
import { BILLING_ADDRESS_TYPE, BILLING_TYPE } from '@checkout/store/enums';
import { mapBillingInfoFieldsToBilling } from '@checkout/store/billing/selector';
import { IBillingInfoResponse } from '@checkout/store/types';

import { IPersonalInfoState } from '../personalInfo/types';
import { IShippingAddressState } from '../shipping/types';

const reducers = {
  [CONSTANTS.SET_SAME_AS_PERSONAL_ON_ERROR]: (
    state: IBillingState,
    payload: IPersonalInfoState
  ) => {
    const {
      postCode,
      city,
      address: streetAddress,
      streetNumber,
      addressId: id,
      flatNumber
    } = payload.formFields;

    const data = {
      postCode,
      city,
      streetAddress,
      streetNumber,
      id,
      flatNumber
    };

    const dummyData = {
      postCode,
      city,
      streetAddress,
      streetNumber,
      flatNumber,
      streetAddressId: id
    };
    state.isFormChanged = true;

    Object.assign(state, data);
    Object.assign(state.personalInfoDummyState, dummyData);
  },
  [CONSTANTS.FETCH_BILLING_ADDRESS_SUCCESS]: (
    state: IBillingState,
    payload: IBillingInfoResponse
  ) => {
    if (payload) {
      const billingAddress = payload.billingAddress[0];
      state.dataReceived = true;
      state.streetAddress = billingAddress.address || '';
      state.city = billingAddress.city || '';
      state.deliveryNote = billingAddress.deliveryNote || '';

      state.sameAsPersonalInfo = payload.sameAsPersonalInfo;
      state.sameAsShippingInfo = payload.sameAsShippingInfo;

      state.flatNumber = billingAddress.flatNumber || '';
      state.postCode = billingAddress.postCode || '';
      if (billingAddress.id) {
        state.id = billingAddress.id;
      }
      state.streetNumber = billingAddress.streetNumber || '';
      state.enableProceedButton = true;

      if (payload.sameAsPersonalInfo && payload.sameAsShippingInfo) {
        state.sameAsShippingInfo = false;
      }

      if (payload.sameAsPersonalInfo || payload.sameAsShippingInfo) {
        updateDummyStateAfterGetData(state, payload);
      }
    }
  },
  [CONSTANTS.UPDATE_PERSONAL_INFO_DUMMY_STATE]: (
    state: IBillingState,
    payload: IBillingFormField
  ) => {
    if (payload) {
      mapBillingInfoFieldsToBilling(state.personalInfoDummyState, payload);
    }
  },
  [CONSTANTS.UPDATE_SHIPPING_INFO_DUMMY_STATE]: (
    state: IBillingState,
    payload: IBillingFormField
    // tslint:disable-next-line:no-identical-functions
  ) => {
    if (payload) {
      mapBillingInfoFieldsToBilling(state.personalInfoDummyState, payload);
    }
  },
  [CONSTANTS.UPDATE_BILLING_ADDRESS]: (
    state: IBillingState,
    payload: IUpdateBillingAddress
  ) => {
    state[payload.key] = payload.value;
  },
  [CONSTANTS.SEND_BILLING_ADDRESS_SUCCESS]: (
    state: IBillingState,
    payload: IBillingAddressResponse
  ) => {
    state.showAppShell = true;
    state.loading = false;
    state.error = null;
    state.isFormChanged = false;
    if (state.sameAsPersonalInfo || state.sameAsShippingInfo) {
      updateStateAfterSuccess(state);
    }
    if (
      payload &&
      payload.billingAddress &&
      payload.billingAddress.length > 0
    ) {
      state.id = payload.billingAddress[0].id;
    }
  },
  [CONSTANTS.SEND_BILLING_ADDRESS_LOADING]: (state: IBillingState) => {
    state.showAppShell = false;
    state.loading = true;
  },
  [CONSTANTS.SEND_BILLING_ADDRESS_ERROR]: (state: IBillingState) => {
    state.showAppShell = false;
    state.error = true;
    state.loading = false;
  },
  [CONSTANTS.ENABLE_PROCEED_BUTTON]: (
    state: IBillingState,
    payload: boolean
  ) => {
    state.enableProceedButton = payload;
  },
  [CONSTANTS.SHOW_BILLING_LOADER]: (state: IBillingState, payload: boolean) => {
    state.billLoader = payload;
  },
  [CONSTANTS.UPDATE_FORM_CHANGE_FIELD]: (state: IBillingState) => {
    state.isFormChanged = true;
  },
  [CONSTANTS.SET_BILLING_TYPE]: (
    state: IBillingState,
    payload: {
      billType: billingType;
      showAppShell: boolean;
    }
  ) => {
    state.isFormChanged = true;
    switch (payload.billType) {
      case BILLING_TYPE.E_BILL:
        state.typeOfBill = BILLING_TYPE.E_BILL;
        break;
      case BILLING_TYPE.PAPER_BILL:
        state.typeOfBill = BILLING_TYPE.PAPER_BILL;
        break;
      default:
        state.typeOfBill = BILLING_TYPE.E_BILL;
    }
  },
  [CONSTANTS.BILL_TYPE_SUCCESS]: (
    state: IBillingState,
    payload: {
      fee: number;
      billApplicable: boolean;
      ebillApplicable: boolean;
      paperBillApplicable: boolean;
    }
  ) => {
    state.loading = false;
    state.billApplicable = payload.billApplicable;
    state.ebillApplicable = payload.ebillApplicable;
    state.paperBillApplicable = payload.paperBillApplicable;

    if (state.typeOfBill === BILLING_TYPE.E_BILL) {
      state.eBillAmount = payload.fee;
    } else {
      state.paperBillAmount = payload.fee;
    }
  },

  [CONSTANTS.SET_BILLING_ADDRESS_TYPE]: (
    state: IBillingState,
    payload: billingAddressType
  ) => {
    state.isFormChanged = true;

    switch (payload) {
      case BILLING_ADDRESS_TYPE.PERSONAL_INFO:
        state.sameAsPersonalInfo = true;
        state.sameAsShippingInfo = false;

        break;
      case BILLING_ADDRESS_TYPE.SHIPPING_INFO:
        state.sameAsShippingInfo = true;
        state.sameAsPersonalInfo = false;

        break;
      case BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS:
        state.sameAsPersonalInfo = false;
        state.sameAsShippingInfo = false;
        break;
      default:
        break;
    }
  },
  [CONSTANTS.SET_BILLING_PERSONAL_INFO_DUMMY_STATE]: (
    state: IBillingState,
    payload: IPersonalInfoState
  ) => {
    const {
      postCode,
      city,
      address: streetAddress,
      streetNumber,
      addressId: streetAddressId,
      flatNumber
    } = payload.formFields;
    const data = {
      postCode,
      city,
      streetAddress,
      streetNumber,
      streetAddressId,
      flatNumber
    };
    Object.assign(state.personalInfoDummyState, data);
  },
  [CONSTANTS.SET_BILLING_SHIPPING_INFO_DUMMY_STATE]: (
    state: IBillingState,
    payload: IShippingAddressState
  ) => {
    const {
      postCode,
      city,
      streetAddress,
      streetNumber,
      streetAddressId,
      flatNumber
    } = payload;
    const data = {
      postCode,
      city,
      streetAddress,
      streetNumber,
      streetAddressId,
      flatNumber
    };
    Object.assign(state.shippingInfoDummyState, data);
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IBillingState,
  AnyAction
  // tslint:disable-next-line:max-file-line-count
>;
