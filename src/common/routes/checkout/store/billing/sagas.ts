import { put, takeLatest } from 'redux-saga/effects';
import billingActions from '@checkout/store/billing/actions';
import paymentActions from '@checkout/store/payment/actions';
import checkoutActions from '@checkout/store/actions';
import basketActions from '@basket/store/actions';
import {
  payloadForPatchBilling,
  payloadForPatchMainStoreBilling,
  payloadForPostBilling
} from '@checkout/store/billing/transformer';
import store from '@common/store';
import {
  billingAddressType,
  billingType,
  IBillingAddressResponse
} from '@checkout/store/billing/types';
import { BILLING_ADDRESS_TYPE } from '@checkout/store/enums';
import history from '@src/client/history';

import {
  addBillingAddressService,
  billService,
  updateBillingAddressService
} from './services';
import CONSTANTS from './constants';

const CHECKOUT_PAYMENT = '/checkout/payment';
const CHECKOUT_ORDER_REVIEW = '/checkout/order-review';

export function* sendBillingAddress(): Generator {
  try {
    const billingState = store.getState().checkout.billing;

    const {
      dataReceived,
      sameAsPersonalInfo,
      sameAsShippingInfo,
      personalInfoDummyState,
      shippingInfoDummyState
    } = billingState;

    yield put(billingActions.sendBillingAddressLoading());

    let response = {};

    if (!dataReceived && !sameAsPersonalInfo && !sameAsShippingInfo) {
      response = yield addBillingAddressService(
        payloadForPostBilling(billingState)
      );
    } else if (sameAsPersonalInfo || sameAsShippingInfo) {
      if (
        personalInfoDummyState.streetAddressId !== '' ||
        shippingInfoDummyState.streetAddressId !== ''
      ) {
        response = yield updateBillingAddressService(
          payloadForPatchBilling(billingState)
        );
      } else {
        response = yield updateBillingAddressService(
          payloadForPatchMainStoreBilling(billingState)
        );
      }

      // tslint:disable-next-line:no-duplicated-branches
    } else {
      response = yield updateBillingAddressService(
        payloadForPatchMainStoreBilling(billingState)
      );
    }

    yield put(
      billingActions.sendBillingAddressSuccess(
        response as IBillingAddressResponse
      )
    );
    if (history) {
      history.push(CHECKOUT_ORDER_REVIEW);
    }
  } catch (error) {
    yield put(billingActions.sendBillingAddressError());
  }
}

export function* setBillingAddressType(action: {
  type: string;
  payload: billingAddressType;
}): Generator {
  const { personalInfo, shippingInfo } = store.getState().checkout;

  switch (action.payload) {
    case BILLING_ADDRESS_TYPE.PERSONAL_INFO:
      yield put(billingActions.setPersonalInfoDummyState(personalInfo));
      break;
    case BILLING_ADDRESS_TYPE.SHIPPING_INFO:
      yield put(billingActions.setShippingInfoDummyState(shippingInfo));
      break;

    default:
      break;
  }
}

export function* fetchBillingError(): Generator {
  const { personalInfo } = store.getState().checkout;

  yield put(billingActions.setSameAsPersonalOnError(personalInfo));
}

export function* editPaymentStep(action: {
  type: string;
  payload: string;
}): Generator {
  const { checkout } = store.getState().checkout;

  if (action.payload === 'monthly') {
    if (checkout.isMonthlyPrice) {
      yield put(paymentActions.billingToPaymentStepUpdate(false));
    } else {
      yield put(paymentActions.billingToPaymentStepUpdate(true));
    }
  } else {
    yield put(paymentActions.billingToPaymentStepUpdate(true));
  }

  if (history) {
    history.push(CHECKOUT_PAYMENT);
  }
}

export function* bill(action: {
  type: string;
  payload: { billType: billingType; showAppShell: boolean };
}): Generator {
  try {
    const personalInfoId = store.getState().checkout.personalInfo.id;

    if (personalInfoId) {
      window.scrollTo(0, 0);
      if (action.payload.showAppShell) {
        yield put(checkoutActions.fetchCheckoutAddressLoading(true));
      } else {
        yield put(billingActions.showBillingLoader(true));
      }
      const {
        cartSummary,
        fee,
        billApplicable,
        ebillApplicable,
        paperBillApplicable
      } = yield billService(action.payload.billType);
      if (cartSummary) {
        yield put(basketActions.setCartSummary(cartSummary));
      }
      yield put(
        billingActions.billTypeSuccess({
          fee,
          billApplicable,
          ebillApplicable,
          paperBillApplicable
        })
      );
      yield put(billingActions.showBillingLoader(false));
      yield put(checkoutActions.fetchCheckoutAddressLoading(false));
    }
  } catch (e) {
    yield put(billingActions.showBillingLoader(false));
    yield put(billingActions.sendBillingAddressError());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.SEND_BILLING_ADDRESS, sendBillingAddress);
  yield takeLatest(CONSTANTS.SET_BILLING_ADDRESS_TYPE, setBillingAddressType);
  yield takeLatest(CONSTANTS.FETCH_BILLING_ADDRESS_ERROR, fetchBillingError);
  yield takeLatest(CONSTANTS.EDIT_PAYMENT_STEP, editPaymentStep);
  yield takeLatest(CONSTANTS.SET_BILLING_TYPE, bill);
}
export default watcherSaga;
