import { IBillingFormField, IDummyState } from './types';

export const mapBillingInfoFieldsToBilling = (
  dummyState: IDummyState,
  localState: IBillingFormField
): void => {
  const obj = {};
  Object.keys(localState).forEach(key => {
    obj[key] = localState[key].value;
  });
  Object.assign(dummyState, obj);
};
