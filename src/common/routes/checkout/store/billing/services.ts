import apiCaller from '@common/utils/apiCaller';
import { IResponse } from '@basket/store/types';
import * as billingAddressApi from '@common/types/api/checkout/billing/billingAddress';
import * as billTypeApi from '@common/types/api/checkout/billing/billType';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import { BILLING_TYPE } from '@checkout/store/enums';

export const addBillingAddressService = async (
  payload: billingAddressApi.POST.IRequest
): Promise<IResponse | Error> => {
  const { url } = apiEndpoints.CHECKOUT.BILLING.ADD_BILLING_ADDRESS;
  try {
    return await apiCaller.post(url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const updateBillingAddressService = async (
  payload: billingAddressApi.PATCH.IRequest
): Promise<IResponse | Error> => {
  const { url } = apiEndpoints.CHECKOUT.BILLING.UPDATE_BILLING_ADDRESS;
  try {
    return await apiCaller.patch(url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const billService = async (
  type: BILLING_TYPE
): Promise<billTypeApi.GET.IResponse | Error> => {
  const { url } =
    type === BILLING_TYPE.PAPER_BILL
      ? apiEndpoints.CHECKOUT.BILLING.PAPER_BILL
      : apiEndpoints.CHECKOUT.BILLING.E_BILL;
  try {
    return await apiCaller.get(url);
  } catch (e) {
    logError(e);
    throw e;
  }
};
