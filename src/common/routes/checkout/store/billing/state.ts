import {
  IBillingState,
  IPatchBillingAddressPayload,
  IPostBillingAddressPayload
} from '@checkout/store/billing/types';

import { BILLING_TYPE } from '../enums';

export default (): IBillingState => ({
  id: '',
  flatNumber: '',
  deliveryNote: '',
  isDefault: true,
  isFormChanged: false,
  dataReceived: false,
  streetAddress: '',
  streetNumber: '',
  city: '',
  postCode: '',
  sameAsPersonalInfo: false,
  sameAsShippingInfo: false,
  enableProceedButton: false,
  loading: false,
  billLoader: false,
  error: null,
  showAppShell: false,
  personalInfoDummyState: {
    streetAddress: '',
    streetAddressId: '',
    streetNumber: '',
    flatNumber: '',
    city: '',
    postCode: '',
    unit: ''
  },
  shippingInfoDummyState: {
    streetAddress: '',
    streetAddressId: '',
    streetNumber: '',
    flatNumber: '',
    city: '',
    postCode: '',
    unit: ''
  },
  typeOfBill: BILLING_TYPE.E_BILL,
  eBillAmount: 0,
  paperBillAmount: 0,
  billApplicable: false,
  ebillApplicable: false,
  paperBillApplicable: false
});

export const patchBillingAdderessData = (): IPatchBillingAddressPayload => {
  return {
    isChanged: false,
    address: {
      id: '',
      streetNumber: '',
      address: '',
      city: '',
      postCode: '',
      flatNumber: '',
      deliveryNote: ''
    },
    typeOfBill: BILLING_TYPE.E_BILL
  };
};

export const postBillingAdderessData = (): IPostBillingAddressPayload => {
  return {
    address: {
      streetNumber: '',
      address: '',
      city: '',
      postCode: '',
      flatNumber: '',
      deliveryNote: ''
    },
    typeOfBill: BILLING_TYPE.E_BILL
  };
};
