import {
  IBillingState,
  // IPatchBillingPayload,
  IPatchBillingAddressPayload,
  IPostBillingAddressPayload
} from '@checkout/store/billing/types';
import {
  // patchBillingAdderessData,
  patchBillingAdderessData,
  postBillingAdderessData
} from '@checkout/store/billing/state';

import { IBillingInfoResponse } from '../types';

export const payloadForPostBilling = (
  data: IBillingState
): IPostBillingAddressPayload => {
  const postData = postBillingAdderessData();

  postData.typeOfBill = data.typeOfBill;
  const {
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber,
    deliveryNote
  } = data;
  postData.address = {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    deliveryNote
  };

  return postData;
};

export const payloadForPatchBilling = (
  data: IBillingState
): IPatchBillingAddressPayload => {
  const patchData = patchBillingAdderessData();
  patchData.isChanged = data.isFormChanged;
  patchData.typeOfBill = data.typeOfBill;

  if (data.sameAsPersonalInfo) {
    const {
      streetAddressId: id,
      streetNumber,
      streetAddress: address,
      city,
      postCode,
      flatNumber,
      deliveryNote
    } = data.personalInfoDummyState;

    patchData.address = {
      id,
      streetNumber,
      address,
      city,
      postCode,
      flatNumber,
      deliveryNote
    };
  } else {
    const {
      streetAddressId: id,
      streetNumber,
      streetAddress: address,
      city,
      postCode,
      flatNumber,
      deliveryNote
    } = data.shippingInfoDummyState;

    patchData.address = {
      id,
      streetNumber,
      address,
      city,
      postCode,
      flatNumber,
      deliveryNote
    };
  }

  return patchData;
};

export const payloadForPatchMainStoreBilling = (
  data: IBillingState
): IPatchBillingAddressPayload => {
  const patchData = patchBillingAdderessData();
  patchData.isChanged = data.isFormChanged;
  patchData.typeOfBill = data.typeOfBill;
  const {
    id,
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber,

    deliveryNote
  } = data;
  patchData.address = {
    id,
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    deliveryNote
  };

  return patchData;
};

export const updateStateAfterSuccess = (data: IBillingState) => {
  const dummyState = data.sameAsPersonalInfo
    ? data.personalInfoDummyState
    : data.shippingInfoDummyState;

  const {
    streetAddressId: id,
    streetNumber,
    streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote
  } = dummyState;

  const obj = {
    id,
    streetNumber,
    streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote
  };
  Object.assign(data, obj);
};

export const updateDummyStateAfterGetData = (
  state: IBillingState,
  payload: IBillingInfoResponse
) => {
  const {
    id,
    streetNumber,
    address: streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote
  } = payload.billingAddress[0];
  const data = {
    id,
    streetNumber,
    streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote
  };
  Object.assign(
    state.sameAsPersonalInfo
      ? state.personalInfoDummyState
      : state.shippingInfoDummyState,
    data
  );
};
