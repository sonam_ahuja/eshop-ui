import { BILLING_ADDRESS_TYPE, BILLING_TYPE } from '@checkout/store/enums';
import { ICartSummary } from '@basket/store/types';

export interface IBillingState {
  id: string;
  dataReceived: boolean;
  isDefault: boolean;
  isFormChanged: boolean;
  streetAddress: string;
  streetNumber: string;
  flatNumber: string;
  city: string;
  deliveryNote: string;
  postCode: string;
  sameAsPersonalInfo: boolean;
  sameAsShippingInfo: boolean;
  enableProceedButton: boolean;
  error: boolean | null;
  loading: boolean;
  billLoader: boolean;
  showAppShell: boolean;
  personalInfoDummyState: IDummyState;
  shippingInfoDummyState: IDummyState;
  typeOfBill: BILLING_TYPE.E_BILL | BILLING_TYPE.PAPER_BILL;
  eBillAmount: number;
  paperBillAmount: number;
  billApplicable: boolean;
  ebillApplicable: boolean;
  paperBillApplicable: boolean;
}

export interface IDummyState {
  streetAddress?: string;
  streetAddressId?: string;
  streetNumber?: string;
  flatNumber?: string;
  city?: string;
  postCode?: string;
  unit?: string;
  deliveryNote?: string;
}

export interface IFormFieldValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
  colDesktop: number;
  colMobile: number;
}

export interface IBillingFormField {
  streetAddress?: IFormFieldValue;
  streetNumber?: IFormFieldValue;
  flatNumber?: IFormFieldValue;
  city?: IFormFieldValue;
  postCode?: IFormFieldValue;
  unit?: IFormFieldValue;
  deliveryNote?: IFormFieldValue;
}

export interface IBillingAddress {
  address: string;
  id: number;
  streetNumber: string;
  city?: string;
  postCode?: string;
  isDefault: boolean;
}

export interface IBillingAddressParams {
  streetNumber: string;
  address: string;
}
export interface IBillingForm {
  id: number | null;
  isDefault: boolean;
  isFormChanged: boolean;
  streetAddress: IBillingInfoValue;
  streetNumber: IBillingInfoValue;
  city?: IBillingInfoValue;
  postCode?: IBillingInfoValue;
}

export interface IBillingInfoValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
  id?: string;
}

export type billingAddressType =
  | BILLING_ADDRESS_TYPE.PERSONAL_INFO
  | BILLING_ADDRESS_TYPE.SHIPPING_INFO
  | BILLING_ADDRESS_TYPE.DIFFERENT_ADDRESS;

export type billingType = BILLING_TYPE.E_BILL | BILLING_TYPE.PAPER_BILL;

export interface IBillingFormState {
  id: string | null;
  isDefault: boolean;
  isFormChanged: boolean;
  streetAddress: IBillingStreetAddress;
  streetNumber: IBillingInfoValue;
  city?: IBillingInfoValue;
  postCode?: IBillingInfoValue;
}

export interface IBillingStreetAddress {
  value: string;
  isValid: boolean;
  validationMessage: string;
}

export interface IAddBillingRequest {
  address: IAddress;
  typeOfBill: billingType;
}

export interface IUpdateBillingRequest {
  isChanged: boolean;
  address: IAddress;
  typeOfBill: billingType;
}

export interface IUpdateBillingAddress {
  key: string;
  value: string;
  isValid: boolean;
  validationMessage: string;
}
export interface IAddress {
  id?: string;
  streetNumber: string;
  address: string;
  city?: string;
  postCode?: string;
  flatNumber?: string;
  deliveryNote?: string;
}

export interface IBillTypeResponse {
  cartSummary: ICartSummary[];
  fee: number;
  billApplicable: boolean;
  ebillApplicable: boolean;
  paperBillApplicable: boolean;
}

/*** PSR ADDED HERE */

export interface IPatchBillingAddressPayload {
  isChanged: boolean;
  address: {
    id?: string;
    streetNumber?: string;
    address?: string;
    city?: string;
    postCode?: string;
    flatNumber?: string;
    deliveryNote?: string;
  };
  typeOfBill: billingType;
}

export interface IPostBillingAddressPayload {
  address: IAddress;
  typeOfBill: billingType;
}

export interface IAddressReponse {
  id: string;
  streetNumber: string;
  address: string;
  city?: string;
  postCode?: string;
  flatNumber?: string;
  deliveryNote?: string;
  isDefault: boolean;
}

export interface IBillingAddressResponse {
  sameAsPersonalInfo: boolean;
  billingAddress: IAddressReponse[];
  sameAsShippingInfo: boolean;
  typeOfBill: billingType;
}
