import creditCheckState from '@checkout/store/creditCheck/state';
import { creditCheckReducer } from '@checkout/store/creditCheck/index';

describe('creditCheckReducer', () => {
  it('creditCheckReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(creditCheckReducer(creditCheckState(), definedAction)).toEqual(
      creditCheckState()
    );
  });
});
