import creditCheck from '@checkout/store/creditCheck/reducers';
import { ICreditCheckState } from '@checkout/store/payment/types';
import actions from '@checkout/store/creditCheck/actions';

describe('creditCheck Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ICreditCheckState = creditCheck(
    undefined,
    definedAction
  );
  const expectedCreditCheckClose: ICreditCheckState = {
    ...initialStateValue,
    isOpen: false
  };
  const expectedCreditCheckOpen: ICreditCheckState = {
    ...initialStateValue,
    isOpen: true
  };

  const initializeValue = () => {
    initialStateValue = creditCheck(undefined, definedAction);
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('PAY_FULL_PRICE should mutate shipping isOpen parameter value', () => {
    const action = actions.payFullPrice();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });

  it('REVIEW_INFO should mutate shipping isOpen parameter value', () => {
    const action = actions.reviewInfo();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });

  it('CREDIT_SUCCESS should mutate shipping isOpen parameter value', () => {
    const action = actions.creditSuccess();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });

  it('REQUEST_SUPPORT_CALL_SUCCESS should mutate shipping isOpen parameter value', () => {
    const action = actions.requestSupportCallSuccess();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });

  it('REQUEST_SUPPORT_CALL_ERROR should mutate shipping isOpen parameter value', () => {
    const action = actions.requestSupportCallError();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });

  it('CREDIT_SCORE_SUCCESS should mutate shipping isOpen parameter value', () => {
    const action = actions.creditScoreSuccess();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckOpen);
  });

  it('SELECT_OTHER_DEVICE should mutate shipping isOpen parameter value', () => {
    const action = actions.selectOtherDevice();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });

  it('CLOSE_MODAL should mutate shipping isOpen parameter value', () => {
    const action = actions.closeModal();
    const state = creditCheck(initialStateValue, action);
    expect(state).toEqual(expectedCreditCheckClose);
  });
});
