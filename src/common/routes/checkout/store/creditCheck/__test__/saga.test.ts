import creditCheckSaga, {
  creditSuccess,
  payFullPrice,
  requestSupportCall,
  selectOtherDevice
} from '@checkout/store/creditCheck/sagas';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';
import CONSTANTS from '@checkout/store/creditCheck/constants';

describe('Credit Check Saga', () => {
  const mock = new MockAdapter(axios);

  it('creditCheckSaga saga test', () => {
    const logoutGenerator = creditCheckSaga();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });
  it('selectOtherDevice saga test', async () => {
    const logoutGenerator = selectOtherDevice();
    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('creditSuccess saga test', () => {
    const logoutGenerator = creditSuccess();
    expect(logoutGenerator.next().value).toBeUndefined();
    mock.onPost(apiEndpoints.BASKET.DELETE_BASKET.url).replyOnce(200, {
      cartItems: BASKET_ALL_ITEM.cartItems,
      cartSummary: BASKET_ALL_ITEM.cartSummary,
      totalItems: BASKET_ALL_ITEM.totalItems
    });
    axios.post(apiEndpoints.BASKET.DELETE_BASKET.url).then(async () => {
      await logoutGenerator.next().value;
    });

    expect(logoutGenerator.next().value).toBeUndefined();
  });

  it('payFullPrice saga test', () => {
    const logoutGenerator = payFullPrice();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
  });

  it('requestSupportCall saga test', () => {
    const action: { type: string; payload: string } = {
      type: CONSTANTS.REQUEST_SUPPORT_CALL,
      payload: 'someString'
    };
    const logoutGenerator = requestSupportCall(action);
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeDefined();
    expect(logoutGenerator.next().value).toBeUndefined();
  });
});
