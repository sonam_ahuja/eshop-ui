import {
  payFullPriceService,
  requestSupportCallService
} from '@checkout/store/creditCheck/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

describe('credit Service', () => {
  const mock = new MockAdapter(axios);
  it('requestSupportCallService success test', async () => {
    mock.onPost(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url).replyOnce(200, {});
    axios
      .post(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url)
      .then(async () => {
        const result = await requestSupportCallService('someString');
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });

  it('requestSupportCallService error test', async () => {
    mock.onPost(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url).replyOnce(404, {});
    axios
      .post(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url)
      .catch(async () => {
        const result = await requestSupportCallService('someString');
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });

  it('payFullPriceService success test', async () => {
    mock.onPost(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url).replyOnce(200, {});
    axios
      .post(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url)
      .then(async () => {
        const result = await payFullPriceService();
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });

  it('payFullPriceService error test', async () => {
    mock.onPost(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url).replyOnce(404, {});
    axios
      .post(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url)
      .catch(async () => {
        const result = await payFullPriceService();
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });
});
