import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/creditCheck/constants';

export default {
  selectOtherDevice: actionCreator<undefined>(CONSTANTS.SELECT_OTHER_DEVICE),
  requestSupportCall: actionCreator<string>(CONSTANTS.REQUEST_SUPPORT_CALL),
  payFullPrice: actionCreator<undefined>(CONSTANTS.PAY_FULL_PRICE),
  reviewInfo: actionCreator<undefined>(CONSTANTS.REVIEW_INFO),
  creditSuccess: actionCreator<undefined>(CONSTANTS.CREDIT_SUCCESS),
  requestSupportCallSuccess: actionCreator<undefined>(
    CONSTANTS.REQUEST_SUPPORT_CALL_SUCCESS
  ),
  requestSupportCallError: actionCreator<undefined>(
    CONSTANTS.REQUEST_SUPPORT_CALL_ERROR
  ),
  creditScoreSuccess: actionCreator<undefined>(CONSTANTS.CREDIT_SCORE_SUCCESS),
  closeModal: actionCreator<undefined>(CONSTANTS.CLOSE_MODAL)
};
