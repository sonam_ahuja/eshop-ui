import { AnyAction, Reducer } from 'redux';
import { ICreditCheckState } from '@checkout/store/payment/types';
import initialState from '@checkout/store/creditCheck/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@checkout/store/creditCheck/constants';

const reducers = {
  [CONSTANTS.PAY_FULL_PRICE]: (state: ICreditCheckState) => {
    state.isOpen = false;
  },
  [CONSTANTS.REVIEW_INFO]: (state: ICreditCheckState) => {
    state.isOpen = false;
  },
  [CONSTANTS.CREDIT_SUCCESS]: (state: ICreditCheckState) => {
    state.isOpen = false;
  },
  [CONSTANTS.REQUEST_SUPPORT_CALL_SUCCESS]: (state: ICreditCheckState) => {
    state.isOpen = false;
  },
  [CONSTANTS.REQUEST_SUPPORT_CALL_ERROR]: (state: ICreditCheckState) => {
    state.isOpen = false;
  },
  [CONSTANTS.CREDIT_SCORE_SUCCESS]: (state: ICreditCheckState) => {
    state.isOpen = true;
  },
  [CONSTANTS.SELECT_OTHER_DEVICE]: (state: ICreditCheckState) => {
    state.isOpen = false;
  },
  [CONSTANTS.CLOSE_MODAL]: (state: ICreditCheckState) => {
    state.isOpen = false;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  ICreditCheckState,
  AnyAction
>;
