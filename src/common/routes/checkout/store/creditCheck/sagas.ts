import { put, takeLatest } from 'redux-saga/effects';
import CONSTANTS from '@checkout/store/creditCheck/constants';
import actions from '@checkout/store/creditCheck/actions';
import history from '@src/client/history';
// tslint:disable-next-line:no-commented-code
// import { CHECKOUT_STEPS_TYPE } from '@checkout/store/enums';
import {
  payFullPriceService,
  requestSupportCallService
} from '@checkout/store/creditCheck/services';
import { logError } from '@src/common/utils';
import basketActions from '@basket/store/actions';
import checkoutActions from '@checkout/store/actions';

export function* selectOtherDevice(): Generator {
  try {
    if (history) {
      history.push('/Devices-Mobile');
    }
  } catch (error) {
    logError(error);
  }
}

// tslint:disable-next-line:no-identical-functions
export function* creditSuccess(): Generator {
  try {
    // @TODO: HERE_CHECKOUT_ADDRESSED
    // yield put(
    //   checkoutStepsActions.setCheckoutSteps(CHECKOUT_STEPS_TYPE.SHIPPING)
    // );
    if (history) {
      history.push('/checkout/shipping');
    }
  } catch (error) {
    logError(error);
  }
}

// tslint:disable-next-line:no-identical-functions
export function* payFullPrice(): Generator {
  try {
    yield put(checkoutActions.fetchCheckoutAddressLoading(false));
    const { cartItems, cartSummary, totalItems } = yield payFullPriceService();

    yield put(
      basketActions.setBasketData({ cartItems, cartSummary, totalItems })
    );

    yield put(checkoutActions.evaluateCartSummaryPrice(cartSummary));
    yield put(checkoutActions.fetchCheckoutAddressSuccess());
    // @TODO: HERE_CHECKOUT_ADDRESSED
    // yield put(
    //   checkoutStepsActions.setCheckoutSteps(CHECKOUT_STEPS_TYPE.SHIPPING)
    // );
    if (history) {
      history.push('/checkout/shipping');
    }
  } catch (error) {
    yield put(checkoutActions.fetchCheckoutAddressSuccess());
    logError(error);
  }
}

export function* requestSupportCall(action: {
  type: string;
  payload: string;
}): Generator {
  try {
    yield requestSupportCallService(action.payload);
    yield put(actions.requestSupportCallSuccess());
  } catch (error) {
    logError(error);
    yield put(actions.requestSupportCallError());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.SELECT_OTHER_DEVICE, selectOtherDevice);
  yield takeLatest(CONSTANTS.CREDIT_SUCCESS, creditSuccess);
  yield takeLatest(CONSTANTS.REQUEST_SUPPORT_CALL, requestSupportCall);
  yield takeLatest(CONSTANTS.PAY_FULL_PRICE, payFullPrice);
}
export default watcherSaga;
