import apiCaller from '@common/utils/apiCaller';
import { IResponse } from '@checkout/store/types';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import { IResponse as IBasketResponse } from '@basket/store/types';

export const requestSupportCallService = async (
  payload: string
): Promise<IResponse | Error> => {
  try {
    return await apiCaller.post(apiEndpoints.CHECKOUT.POST_SUPPORT_CALL.url, {
      number: payload
    });
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const payFullPriceService = async (): Promise<
  IBasketResponse | Error
> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.POST_PAY_FULL_PRICE.method.toLowerCase()
    ](apiEndpoints.CHECKOUT.POST_PAY_FULL_PRICE.url);
  } catch (error) {
    logError(error);
    throw error;
  }
};
