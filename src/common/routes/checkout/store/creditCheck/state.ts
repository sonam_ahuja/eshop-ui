import { ICreditCheckState } from '@checkout/store/payment/types';

export default (): ICreditCheckState => ({
  loading: false,
  error: null,
  data: null,
  isOpen: false
});
