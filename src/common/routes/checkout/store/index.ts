import { combineReducers } from 'redux';
import { billingReducer as billing } from '@checkout/store/billing';
import { default as checkout } from '@checkout/store/reducers';
import { creditCheckReducer as creditCheck } from '@checkout/store/creditCheck';
import { orderSummaryReducer as orderSummary } from '@checkout/store/orderSummary';
import { paymentReducer as payment } from '@checkout/store/payment';
import { personalInfoReducer as personalInfo } from '@checkout/store/personalInfo';
import { shippingReducer as shippingInfo } from '@checkout/store/shipping';
import { orderReviewReducer as orderReview } from '@checkout/store/orderReview';
import { numberPortingReducer as numberPorting } from '@checkout/store/numberPorting';

import { IState } from './types';

const checkoutRootReducer = combineReducers<IState>({
  billing,
  checkout,
  creditCheck,
  orderSummary,
  payment,
  personalInfo,
  shippingInfo,
  orderReview,
  numberPorting
});

export default checkoutRootReducer;

export { default as checkoutSaga } from './sagas';
export { default as personalInfoSaga } from './personalInfo/sagas';
export { default as shippingSaga } from './shipping/sagas';
export { default as paymentInfoSaga } from './payment/sagas';
export { default as creditCheckSaga } from './creditCheck/sagas';
export { default as orderReviewSaga } from './orderReview/sagas';
export { default as billingSaga } from './billing/sagas';
export { default as numberPortingSaga } from './numberPorting/sagas';
