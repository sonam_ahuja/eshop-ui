import actions from '@checkout/store/numberPorting/actions';
import constants from '@checkout/store/numberPorting/constants';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';
import {
  IResourceNumber,
  ISetMigrationType
} from '@checkout/store/numberPorting/types';

describe('Number porting actions', () => {
  it('proceedToNextScreen action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.PROCEED_TO_NEXT_SCREEN,
      payload: undefined
    };
    expect(actions.proceedToNextScreen()).toEqual(expectedAction);
  });

  it('setActiveStep action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SET_ACTIVE_STEP,
      payload: NUMBER_PORTING_STEPS.ENTER_OTP
    };
    expect(actions.setActiveStep(NUMBER_PORTING_STEPS.ENTER_OTP)).toEqual(
      expectedAction
    );
  });

  it('setPhoneNumber action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SET_PHONE_NUMBER,
      payload: '+919998075168'
    };
    expect(actions.setPhoneNumber('+919998075168')).toEqual(expectedAction);
  });

  it('setSelectedPhoneNumber action creator should return a object with expected value', () => {
    const data: IResourceNumber = {
      id: '123',
      displayId: '123',
      selected: true
    };
    const expectedAction: { type: string; payload: IResourceNumber } = {
      type: constants.SET_SELECTED_PHONE_NUMBER,
      payload: data
    };
    expect(actions.setSelectedPhoneNumber(data)).toEqual(expectedAction);
  });

  it('confirmSelectedPhoneNumber action creator should return a object with expected value', () => {
    const data: IResourceNumber = {
      id: '123',
      displayId: '123',
      selected: true
    };
    const expectedAction: { type: string; payload: IResourceNumber } = {
      type: constants.CONFIRM_SELECTED_PHONE_NUMBER,
      payload: data
    };
    expect(actions.confirmSelectedPhoneNumber(data)).toEqual(expectedAction);
  });

  it('setMigrationType action creator should return a object with expected value', () => {
    const data: ISetMigrationType = {
      migrationType: MIGRATION_TYPE.DESIRED_DATE,
      value: 'values'
    };
    const expectedAction: { type: string; payload: ISetMigrationType } = {
      type: constants.SET_MIGRATION_TYPE,
      payload: data
    };
    expect(actions.setMigrationType(data)).toEqual(expectedAction);
  });

  it('setFlowType action creator should return a object with expected value', () => {
    const expectedAction: { type: string; payload: NUMBER_PORTING_TYPES } = {
      type: constants.SET_NUMBER_PORTING_TYPE,
      payload: NUMBER_PORTING_TYPES.CHANGE_NUMBER
    };
    expect(actions.setFlowType(NUMBER_PORTING_TYPES.CHANGE_NUMBER)).toEqual(
      expectedAction
    );
  });

  it('setPlanType action creator should return a object with expected value', () => {
    const expectedAction: { type: string; payload: PLAN_TYPE } = {
      type: constants.SET_PLAN_TYPE,
      payload: PLAN_TYPE.HYBRID
    };
    expect(actions.setPlanType(PLAN_TYPE.HYBRID)).toEqual(expectedAction);
  });
});
