import numberPortingState from '@checkout/store/numberPorting/state';
import { numberPortingReducer } from '@checkout/store/numberPorting';

describe('Number Porting main reducer', () => {
  it('numberPortingState test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(numberPortingReducer(numberPortingState(), definedAction)).toEqual(
      numberPortingState()
    );
  });
});
