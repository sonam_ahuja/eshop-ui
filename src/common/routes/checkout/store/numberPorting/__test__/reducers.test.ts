import numberPorting from '@checkout/store/numberPorting/reducers';
import numberPortingState from '@checkout/store/numberPorting/state';
import {
  IAddDeleteCartRequest,
  INumberPortingState,
  IResourceNumber,
  IValidateNumberRequest
} from '@checkout/store/numberPorting/types';
import actions from '@checkout/store/numberPorting/actions';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  OPERATION_TYPE,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';

// tslint:disable-next-line:no-big-function
describe('Order Review Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: INumberPortingState = numberPorting(
    undefined,
    definedAction
  );
  let expectedState: INumberPortingState = numberPortingState();
  const initializeValue = () => {
    initialStateValue = numberPorting(undefined, definedAction);
    expectedState = numberPortingState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('PROCEED_TO_NEXT_SCREEN should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const expected: INumberPortingState = {
      ...expectedState,
      activeStep: NUMBER_PORTING_STEPS.ENTER_OTP
    };
    const action = actions.proceedToNextScreen();
    const store = numberPorting(initialStateValue, action);
    expect(store).toEqual(expected);
  });

  it('SET_ACTIVE_STEP should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const expected: INumberPortingState = {
      ...expectedState,
      activeStep: NUMBER_PORTING_STEPS.ENTER_OTP
    };
    const action = actions.setActiveStep(NUMBER_PORTING_STEPS.ENTER_OTP);
    const store = numberPorting(initialStateValue, action);
    expect(store).toEqual(expected);
  });

  it('SET_NUMBER_PORTING_TYPE should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.setFlowType(NUMBER_PORTING_TYPES.CHANGE_NUMBER);
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('CONFIRM_SELECTED_PHONE_NUMBER should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const data: IResourceNumber = {
      id: '123',
      displayId: '123',
      selected: true
    };
    const action = actions.confirmSelectedPhoneNumber(data);
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('SET_PLAN_TYPE should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.setPlanType(PLAN_TYPE.HYBRID);
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('SET_ERROR should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.setError({
      message: 'message'
    });
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_MSISDN_OTP should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const payload: IValidateNumberRequest = {
      otpContext: 'otp',
      action: OPERATION_TYPE.SEND,
      mobileNumber: '+91998075168',
      nonce: 'nonce',
      otp: 'otp'
    };
    const action = actions.requestOtpForMsisdnNumberPort(payload);
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_MSISDN_OTP_ERROR should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestOtpForMsisdnNumberPortError();
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('SHOW_ALL_NUMBERS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.showAllNumber(true);
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_PORTING_NUMBER_ERROR should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestForPortingNumberError();
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_PORTING_NUMBER should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const payload: IAddDeleteCartRequest = {
      operationType: OPERATION_TYPE.MNP,
      consent: null,
      cartItems: []
    };
    const action = actions.requestForPortingNumber(payload);
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_VALIDATE_MSISDN_OTP_SUCCESS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestOtpValidationForMsisdnNumberPortSuccess();
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_VALIDATE_MSISDN_OTP_ERROR should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestOtpValidationForMsisdnNumberPortError();
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_MSISDN_OTP_SUCCESS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestOtpForMsisdnNumberPortSuccess({
      success: true,
      nonce: 'nonce'
    });
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('SET_SELECTED_PHONE_NUMBER should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.setSelectedPhoneNumber({
      id: '2',
      displayId: '22',
      selected: true
    });
    const stateValue = { ...initialStateValue };
    stateValue.availableNumber = [
      {
        id: '2',
        displayId: '22',
        selected: false
      },
      {
        id: '4',
        displayId: '44',
        selected: true
      },
      {
        id: '5',
        displayId: '55',
        selected: false
      }
    ];
    const store = numberPorting(stateValue, action);
    expect(store).toBeDefined();
  });

  it('SET_PHONE_NUMBER should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.setPhoneNumber('+9199980976545');
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('SET_MIGRATION_TYPE should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.setMigrationType({
      migrationType: MIGRATION_TYPE.DESIRED_DATE,
      value: '20-12-2019'
    });
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_REOURCE_POOL_SUCCESS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestReourcePoolSuccess({
      productOfferingId: '12',
      resourceType: 'STIRN',
      cartItemId: '1',
      resources: [],
      imageUrl: '',
      show: true,
      mnpSelectedResource: {
        selectedNumber: '122',
        numberType: PLAN_TYPE.HYBRID,
        migrationType: MIGRATION_TYPE.WITHDRAWAL,
        transferDate: '22/06/2018'
      }
    });
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_PORTING_NUMBER_SUCCESS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestForPortingNumberSuccess({
      success: true,
      cartItemId: '1',
      phoneNumber: {
        id: '1',
        displayId: '2',
        selected: true
      }
    });
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });

  it('REQUEST_TERM_AND_CONDITIONS_SUCCESS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const action = actions.requestTermAndConditionsSuccess({
      id: '12',
      partyPrivacyProfileTypeCharacteristics: {
        id: '12',
        name: 'name',
        description: 'des',
        documentationUrl: '/ddfdfdf',
        isAuthorized: 'auth'
      }
    });
    const store = numberPorting(initialStateValue, action);
    expect(store).toBeDefined();
  });
});
