import numberPortingSaga, {
  cancelNumberPortingRequest,
  requestForPortingNumber,
  requestForTermAndConditions,
  requestForValidateOtp,
  requestValidatePhoneNumber
} from '@checkout/store/numberPorting/sagas';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import Constant from '@checkout/store/numberPorting/constants';
import {
  IAddDeleteCartRequest,
  IValidateNumberRequest
} from '@checkout/store/numberPorting/types';
import { OPERATION_TYPE } from '@checkout/store/numberPorting/enum';

describe('Number Porting Saga', () => {
  it('orderReviewSaga saga test', () => {
    const generator = numberPortingSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('requestForPortingNumber saga test', () => {
    const payload: IAddDeleteCartRequest = {
      operationType: OPERATION_TYPE.MNP,
      consent: null,
      cartItems: []
    };
    const action: {
      type: string;
      payload: numberPortTypesApi.PATCH.IRequest;
    } = {
      type: Constant.REQUEST_MSISDN_OTP,
      payload
    };

    const generator = requestForPortingNumber(action);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('requestForValidateOtp saga test', () => {
    const payload: IValidateNumberRequest = {
      otpContext: 'otp',
      action: OPERATION_TYPE.SEND,
      mobileNumber: '+91998075168',
      nonce: 'nonce',
      otp: 'otp'
    };
    const action: {
      type: string;
      payload: numberPortTypesApi.PHONE_POST.IRequest;
    } = {
      type: Constant.REQUEST_MSISDN_OTP,
      payload
    };

    const generator = requestForValidateOtp(action);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('requestForPortingNumber saga test', () => {
    const payload: IValidateNumberRequest = {
      otpContext: 'otp',
      action: OPERATION_TYPE.SEND,
      mobileNumber: '+91998075168',
      nonce: 'nonce',
      otp: 'otp'
    };
    const action: {
      type: string;
      payload: numberPortTypesApi.PHONE_POST.IRequest;
    } = {
      type: Constant.REQUEST_MSISDN_OTP,
      payload
    };

    const generator = requestValidatePhoneNumber(action);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('requestForTermAndConditions saga test', () => {
    const generator = requestForTermAndConditions();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('cancelNumberPortingRequest saga test', () => {
    const generator = cancelNumberPortingRequest({
      type: 'string',
      payload: {
        operationType: OPERATION_TYPE.MNS,
        consent: null,
        cartItems: []
      }
    });
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
