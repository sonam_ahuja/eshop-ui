import {
  fetchReourcePoolService,
  portingNumberService,
  termAndConditionService,
  validatePhoneNumberService
} from '@checkout/store/numberPorting/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import {
  IAddDeleteCartRequest,
  IValidateNumberRequest
} from '@checkout/store/numberPorting/types';
import { OPERATION_TYPE } from '@checkout/store/numberPorting/enum';

describe('porting Number Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchReourcePoolService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.NUMBER_PORTING.VALIDATE_PHONE_NUMBER;
    const addressPayload = {};
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchReourcePoolService('1234');
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('portingNumberService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.NUMBER_PORTING.VALIDATE_PHONE_NUMBER;
    const payload: IAddDeleteCartRequest = {
      operationType: OPERATION_TYPE.MNP,
      consent: null,
      cartItems: []
    };
    const addressPayload = {};
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await portingNumberService(payload);
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('validatePhoneNumberService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.NUMBER_PORTING.VALIDATE_PHONE_NUMBER;
    const addressPayload = {};
    const payload: IValidateNumberRequest = {
      otpContext: 'otp',
      action: OPERATION_TYPE.SEND,
      mobileNumber: '+91998075168',
      nonce: 'nonce',
      otp: 'otp'
    };
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await validatePhoneNumberService(payload);
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('termAndConditionService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.NUMBER_PORTING.VALIDATE_PHONE_NUMBER;
    const addressPayload = {};
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await termAndConditionService();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });
});
