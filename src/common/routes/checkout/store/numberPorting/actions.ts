import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/numberPorting/constants';
import {
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';
import * as numberPortTypesApi from '@common/types/api/checkout/numberPorting/index';

import { IError, IResourceNumber, ISetMigrationType } from './types';

export default {
  proceedToNextScreen: actionCreator<void>(CONSTANTS.PROCEED_TO_NEXT_SCREEN),
  setActiveStep: actionCreator<NUMBER_PORTING_STEPS>(CONSTANTS.SET_ACTIVE_STEP),
  setPhoneNumber: actionCreator<string>(CONSTANTS.SET_PHONE_NUMBER),
  setSelectedPhoneNumber: actionCreator<IResourceNumber>(
    CONSTANTS.SET_SELECTED_PHONE_NUMBER
  ),
  confirmSelectedPhoneNumber: actionCreator<IResourceNumber>(
    CONSTANTS.CONFIRM_SELECTED_PHONE_NUMBER
  ),

  setMigrationType: actionCreator<ISetMigrationType>(
    CONSTANTS.SET_MIGRATION_TYPE
  ),
  setFlowType: actionCreator<NUMBER_PORTING_TYPES>(
    CONSTANTS.SET_NUMBER_PORTING_TYPE
  ),
  setPlanType: actionCreator<PLAN_TYPE>(CONSTANTS.SET_PLAN_TYPE),
  setError: actionCreator<IError>(CONSTANTS.SET_ERROR),

  requestReourcePool: actionCreator<
    numberPortTypesApi.RESOURCE_POOL_GET.IRequest
  >(CONSTANTS.REQUEST_REOURCE_POOL),
  requestReourcePoolSuccess: actionCreator<
    numberPortTypesApi.RESOURCE_POOL_GET.IResponse
  >(CONSTANTS.REQUEST_REOURCE_POOL_SUCCESS),
  requestReourcePoolError: actionCreator<void>(
    CONSTANTS.REQUEST_REOURCE_POOL_ERROR
  ),

  requestTermAndConditions: actionCreator<void>(
    CONSTANTS.REQUEST_TERM_AND_CONDITIONS
  ),
  requestTermAndConditionsSuccess: actionCreator<
    numberPortTypesApi.TERM_CONDITION_GET.IResponse
  >(CONSTANTS.REQUEST_TERM_AND_CONDITIONS_SUCCESS),
  requestTermAndConditionsError: actionCreator<void>(
    CONSTANTS.REQUEST_TERM_AND_CONDITIONS_ERROR
  ),

  requestOtpForMsisdnNumberPort: actionCreator<
    numberPortTypesApi.PHONE_POST.IRequest
  >(CONSTANTS.REQUEST_MSISDN_OTP),
  requestOtpForMsisdnNumberPortError: actionCreator<void>(
    CONSTANTS.REQUEST_MSISDN_OTP_ERROR
  ),
  requestOtpForMsisdnNumberPortSuccess: actionCreator<
    numberPortTypesApi.PHONE_POST.IResponse
  >(CONSTANTS.REQUEST_MSISDN_OTP_SUCCESS),

  requestOtpValidationForMsisdnNumberPort: actionCreator<
    numberPortTypesApi.PHONE_POST.IRequest
  >(CONSTANTS.REQUEST_VALIDATE_MSISDN_OTP),
  requestOtpValidationForMsisdnNumberPortError: actionCreator<void>(
    CONSTANTS.REQUEST_VALIDATE_MSISDN_OTP_ERROR
  ),
  requestOtpValidationForMsisdnNumberPortSuccess: actionCreator<void>(
    CONSTANTS.REQUEST_VALIDATE_MSISDN_OTP_SUCCESS
  ),

  requestForPortingNumber: actionCreator<numberPortTypesApi.PATCH.IRequest>(
    CONSTANTS.REQUEST_PORTING_NUMBER
  ),
  requestForCancelPortingNumber: actionCreator<
    numberPortTypesApi.PATCH.IRequest
  >(CONSTANTS.REQUEST_CANCEL_PORTING_NUMBER),

  requestForPortingNumberError: actionCreator<void>(
    CONSTANTS.REQUEST_PORTING_NUMBER_ERROR
  ),
  requestForPortingNumberSuccess: actionCreator<
    numberPortTypesApi.PATCH.IResponse
  >(CONSTANTS.REQUEST_PORTING_NUMBER_SUCCESS),
  showAllNumber: actionCreator<boolean>(CONSTANTS.SHOW_ALL_NUMBERS)
};
