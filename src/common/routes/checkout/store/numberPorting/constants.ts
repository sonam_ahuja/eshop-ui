const namespace = 'CHECKOUT/NUMBER_PORTING';

export default {
  REQUEST_REOURCE_POOL: `${namespace}_REQUEST_REOURCE_POOL`,
  REQUEST_REOURCE_POOL_SUCCESS: `${namespace}_REQUEST_REOURCE_POOL_SUCCESS`,
  REQUEST_REOURCE_POOL_ERROR: `${namespace}_REQUEST_REOURCE_POOL_ERROR`,

  REQUEST_TERM_AND_CONDITIONS: `${namespace}_REQUEST_TERM_AND_CONDITIONS`,
  REQUEST_TERM_AND_CONDITIONS_SUCCESS: `${namespace}_REQUEST_TERM_AND_CONDITIONS_SUCCESS`,
  REQUEST_TERM_AND_CONDITIONS_ERROR: `${namespace}_REQUEST_TERM_AND_CONDITIONS_ERROR`,

  REQUEST_MSISDN_OTP: `${namespace}_REQUEST_MSISDN_OTP`,
  REQUEST_MSISDN_OTP_SUCCESS: `${namespace}_REQUEST_MSISDN_OTP_SUCCESS`,
  REQUEST_MSISDN_OTP_ERROR: `${namespace}_REQUEST_MSISDN_OTP_ERROR`,

  REQUEST_VALIDATE_MSISDN_OTP: `${namespace}_REQUEST_VALIDATE_MSISDN_OTP`,
  REQUEST_VALIDATE_MSISDN_OTP_SUCCESS: `${namespace}_REQUEST_VALIDATE_MSISDN_OTP_SUCCESS`,
  REQUEST_VALIDATE_MSISDN_OTP_ERROR: `${namespace}_REQUEST_VALIDATE_MSISDN_OTP_ERROR`,

  REQUEST_PORTING_NUMBER: `${namespace}_REQUEST_PORTING_NUMBER`,
  REQUEST_CANCEL_PORTING_NUMBER: `${namespace}_REQUEST_CANCEL_PORTING_NUMBER`,
  REQUEST_PORTING_NUMBER_SUCCESS: `${namespace}_REQUEST_PORTING_NUMBER_SUCCESS`,
  REQUEST_PORTING_NUMBER_ERROR: `${namespace}_REQUEST_PORTING_NUMBER_ERROR`,

  SET_ACTIVE_STEP: `${namespace}_SET_ACTIVE_STEP`,
  SET_NUMBER_PORTING_TYPE: `${namespace}_SET_NUMBER_PORTING_TYPE`,

  SET_PROGRESS_LOADING_TEXT: `${namespace}_SET_PROGRESS_LOADING_TEXT`,
  SET_HEADER_TEXT: `${namespace}_SET_PROGRESS_LOADING_TEXT`,
  SET_PHONE_NUMBER: `${namespace}_SET_PHONE_NUMBER`,
  SET_SELECTED_PHONE_NUMBER: `${namespace}_SET_SELECTED_PHONE_NUMBER`,
  SET_OTP: `${namespace}_SET_OTP`,
  SET_SELECTED_NUMBER: `${namespace}_SET_SELECTED_NUMBER`,
  CONFIRM_SELECTED_PHONE_NUMBER: `${namespace}_CONFIRM_SELECTED_PHONE_NUMBER`,
  SET_ERROR: `${namespace}_SET_ERROR`,
  SET_NONCE_FOR_MSISDN: `${namespace}_SET_NONCE_FOR_MSISDN`,
  SET_MIGRATION_TYPE: `${namespace}_SET_MIGRATION_TYPE`,
  SET_PLAN_TYPE: `${namespace}_SET_PLAN_TYPE`,

  SHOW_ALL_NUMBERS: `${namespace}_SHOW_ALL_NUMBERS`,
  PROCEED_TO_NEXT_SCREEN: `${namespace}_PROCEED_TO_NEXT_SCREEN`
};
