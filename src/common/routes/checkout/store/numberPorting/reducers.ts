import { AnyAction, Reducer } from 'redux';
import moment from 'moment';
import {
  IError,
  INumberPortingState,
  IResourceNumber,
  ISetMigrationType
} from '@checkout/store/numberPorting/types';
import initialState from '@checkout/store/numberPorting/state';
import { getConvertedDate } from '@checkout/CheckoutSteps/utils';
import withProduce from '@utils/withProduce';
import { numberPortingFlow } from '@checkout/CheckoutSteps/NumberPorting/utils/index';
import * as numberPortTypesApi from '@common/types/api/checkout/numberPorting/index';

import CONSTANTS from './constants';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  PLAN_TYPE
} from './enum';

const reducers = {
  [CONSTANTS.PROCEED_TO_NEXT_SCREEN]: (state: INumberPortingState) => {
    state.activeStep =
      state.error.message && state.error.message.length
        ? state.activeStep
        : numberPortingFlow[state.flowType][state.activeStep].success;
  },

  [CONSTANTS.SET_ACTIVE_STEP]: (
    state: INumberPortingState,
    payload: NUMBER_PORTING_STEPS
  ) => {
    state.activeStep = payload;
  },
  [CONSTANTS.SET_NUMBER_PORTING_TYPE]: (
    state: INumberPortingState,
    payload: NUMBER_PORTING_TYPES
  ) => {
    state.flowType = payload;
    state.activeStep =
      payload === NUMBER_PORTING_TYPES.CHANGE_NUMBER
        ? NUMBER_PORTING_STEPS.SELECT_NUMBER
        : NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER;
  },
  [CONSTANTS.CONFIRM_SELECTED_PHONE_NUMBER]: (
    state: INumberPortingState,
    payload: IResourceNumber
  ) => {
    state.selectedNumber = payload;
  },
  [CONSTANTS.SET_SELECTED_PHONE_NUMBER]: (
    state: INumberPortingState,
    payload: IResourceNumber
  ) => {
    state.availableNumber = state.availableNumber.map(phone => {
      if (phone.id === payload.id) {
        return { ...phone, selected: true };
      }

      return { ...phone, selected: false };
    });
  },
  [CONSTANTS.SET_PHONE_NUMBER]: (
    state: INumberPortingState,
    payload: string
  ) => {
    state.phoneNumber = payload;
  },
  [CONSTANTS.SET_MIGRATION_TYPE]: (
    state: INumberPortingState,
    payload: ISetMigrationType
  ) => {
    state.migrationType = payload.migrationType;
    state.date =
      payload.migrationType === MIGRATION_TYPE.DESIRED_DATE
        ? payload.value
          ? payload.value
          : state.date
        : payload.value;
  },
  [CONSTANTS.SET_PLAN_TYPE]: (
    state: INumberPortingState,
    payload: PLAN_TYPE
  ) => {
    state.currentPlan = payload;
  },
  [CONSTANTS.SET_ERROR]: (state: INumberPortingState, error: IError) => {
    state.loading = false;
    state.error = {
      message: error.message ? error.message : '',
      code: error.code ? error.code : 0
    };
  },
  [CONSTANTS.REQUEST_REOURCE_POOL_SUCCESS]: (
    state: INumberPortingState,
    payload: numberPortTypesApi.RESOURCE_POOL_GET.IResponse
  ) => {
    state.showBanner = payload.show;
    state.bannerImageUrl = payload.imageUrl || '';
    state.resourceType =
      payload.resourceType || payload.selectedResourceType || '';
    state.numberPortingCartItemId = payload.cartItemId || '';
    state.availableNumber = payload.resources || [];
    state.selectedNumber = payload.resources
      ? payload.resources[0]
      : { id: '', displayId: '' };
    state.mnpEligibleDates =
      payload.mnpEligibleDates && getConvertedDate(payload.mnpEligibleDates);
    if (payload.mnpSelectedResource) {
      const prevMnpSlection = payload.mnpSelectedResource;
      state.phoneNumber = prevMnpSlection.selectedNumber || '';
      state.currentPlan = prevMnpSlection.numberType as PLAN_TYPE;
      state.migrationType = prevMnpSlection.migrationType as MIGRATION_TYPE;

      state.date = prevMnpSlection.transferDate
        ? moment(prevMnpSlection.transferDate, 'DD-MM-YYYY').format(
            'DD MMMM YYYY'
          )
        : '';
    }
  },
  [CONSTANTS.REQUEST_TERM_AND_CONDITIONS_SUCCESS]: (
    state: INumberPortingState,
    payload: numberPortTypesApi.TERM_CONDITION_GET.IResponse
  ) => {
    state.termConditionId = payload.id;
    state.termConditionCharecterstics = {
      ...payload.partyPrivacyProfileTypeCharacteristics
    };
  },
  [CONSTANTS.REQUEST_MSISDN_OTP]: (state: INumberPortingState) => {
    state.loading = true;
  },
  [CONSTANTS.REQUEST_MSISDN_OTP_ERROR]: (state: INumberPortingState) => {
    state.loading = false;
  },
  [CONSTANTS.REQUEST_MSISDN_OTP_SUCCESS]: (
    state: INumberPortingState,
    payload: numberPortTypesApi.PHONE_POST.IResponse
  ) => {
    state.loading = false;
    state.nonce = payload.nonce;
  },
  [CONSTANTS.REQUEST_VALIDATE_MSISDN_OTP_ERROR]: (
    state: INumberPortingState
  ) => {
    state.loading = false;
  },
  [CONSTANTS.REQUEST_VALIDATE_MSISDN_OTP_SUCCESS]: (
    state: INumberPortingState
  ) => {
    state.loading = false;
  },
  [CONSTANTS.REQUEST_PORTING_NUMBER]: (state: INumberPortingState) => {
    state.loading = true;
  },
  [CONSTANTS.REQUEST_PORTING_NUMBER_ERROR]: (state: INumberPortingState) => {
    state.loading = false;
    state.selectedNumber = { displayId: '', id: '' };
  },
  [CONSTANTS.REQUEST_PORTING_NUMBER_SUCCESS]: (
    state: INumberPortingState,
    payload: numberPortTypesApi.PATCH.IResponse
  ) => {
    state.numberPortingCartItemId = payload.cartItemId;
    state.selectedNumber = payload.phoneNumber;
    state.loading = false;
  },
  [CONSTANTS.SHOW_ALL_NUMBERS]: (
    state: INumberPortingState,
    payload: boolean
  ) => {
    state.showAllNumber = payload;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  INumberPortingState,
  AnyAction
>;
