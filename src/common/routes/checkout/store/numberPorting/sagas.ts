import { put, takeLatest } from 'redux-saga/effects';
import store from '@common/store';
import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import { logError } from '@common/utils';
import getErrorMessage from '@common/utils/errorHandler';
import actions from '@checkout/store/numberPorting/actions';
import personalInfoActions from '@checkout/store/personalInfo/actions';
import basketAction from '@basket/store/actions';
import getSelectedNumber from '@checkout/CheckoutSteps/NumberPorting/utils/getSelectedNumber';

import CONSTANTS from './constants';
import {
  fetchReourcePoolService,
  portingNumberService,
  termAndConditionService,
  validatePhoneNumberService
} from './services';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  OPERATION_TYPE
} from './enum';

export function* requestValidatePhoneNumber({
  payload
}: {
  type: string;
  payload: numberPortTypesApi.PHONE_POST.IRequest;
}): Generator {
  try {
    const result = yield validatePhoneNumberService(payload);
    if (result.success === false) {
      const message = getErrorMessage(result.code);
      yield put(actions.setError({ code: result.code, message }));
    } else {
      yield put(actions.requestOtpForMsisdnNumberPortSuccess(result));

      yield put(actions.proceedToNextScreen());
    }
  } catch (error) {
    logError(error);
    yield put(actions.requestOtpForMsisdnNumberPortError());
  }
}

export function* requestForValidateOtp({
  payload
}: {
  type: string;
  payload: numberPortTypesApi.PHONE_POST.IRequest;
}): Generator {
  try {
    const result = yield yield validatePhoneNumberService(payload);
    if (result.success === false) {
      const message = getErrorMessage(result.code);
      yield put(actions.setError({ code: result.code, message }));
    } else {
      yield put(actions.requestOtpForMsisdnNumberPortSuccess(result));
      yield put(actions.proceedToNextScreen());
    }
  } catch (error) {
    logError(error);
    yield put(actions.requestOtpForMsisdnNumberPortError());
  }
}

export function* requestResourcePool({
  payload
}: {
  type: string;
  payload: numberPortTypesApi.RESOURCE_POOL_GET.IRequest;
}): Generator {
  try {
    const {
      mnp
    } = store.getState().configuration.cms_configuration.modules.checkout;
    const result = yield fetchReourcePoolService(payload);
    if (result && result.selectedResourceType === OPERATION_TYPE.MNP) {
      yield put(personalInfoActions.setNumberPortingStatus(true));
      if (mnp.verificationMethods.MSISDNOTP) {
        yield put(actions.setFlowType(NUMBER_PORTING_TYPES.MSISDN_WITH_OTP));
      } else {
        yield put(actions.setFlowType(NUMBER_PORTING_TYPES.MSISDN_WITHOUT_OTP));
      }
      yield put(
        actions.setActiveStep(NUMBER_PORTING_STEPS.SELECT_MIGRATION_PLAN)
      );
    }
    yield put(actions.requestReourcePoolSuccess(result));
  } catch (error) {
    yield put(actions.requestReourcePoolError(error));
  }
}
export function* cancelNumberPortingRequest({
  payload
}: {
  type: string;
  payload: numberPortTypesApi.PATCH.IRequest;
}): Generator {
  try {
    const result = yield portingNumberService(payload);
    if (result.success === false) {
      const message = getErrorMessage(result.code);
      yield put(actions.setError({ code: result.code, message }));
    } else {
      yield put(personalInfoActions.showNumberPortingCancelModal(false));
      yield put(personalInfoActions.showNumberPortModal(false));
      yield put(personalInfoActions.setNumberPortingStatus(false));
      yield put(
        actions.setMigrationType({ migrationType: MIGRATION_TYPE.EMPTY })
      );
    }
    yield put(basketAction.fetchBasket({ isCalledFromCheckout: true }));
  } catch (error) {
    logError(error);
    yield put(actions.requestForPortingNumberError());
  }
}
export function* requestForPortingNumber({
  payload
}: {
  type: string;
  payload: numberPortTypesApi.PATCH.IRequest;
}): Generator {
  const state = store.getState().checkout.numberPorting;

  try {
    const result = yield portingNumberService(payload);
    if (result.success === false) {
      const message = getErrorMessage(result.code);
      yield put(actions.setError({ code: result.code, message }));
    } else {
      yield put(personalInfoActions.showNumberPortModal(false));
      yield put(actions.proceedToNextScreen());
      if (payload.operationType === OPERATION_TYPE.MNP) {
        yield put(personalInfoActions.setNumberPortingStatus(true));
      }
    }
    yield put(basketAction.fetchBasket({}));
    const phoneNumber = getSelectedNumber(state.availableNumber);
    const successPayload = { phoneNumber, ...result };
    yield put(actions.requestForPortingNumberSuccess(successPayload));
  } catch (error) {
    logError(error);
    yield put(actions.requestForPortingNumberError());
  }
}
export function* requestForTermAndConditions(): Generator {
  try {
    const result = yield termAndConditionService();
    yield put(actions.requestTermAndConditionsSuccess(result));
  } catch (error) {
    logError(error);
    yield put(actions.requestTermAndConditionsError());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.REQUEST_PORTING_NUMBER, requestForPortingNumber);
  yield takeLatest(CONSTANTS.REQUEST_MSISDN_OTP, requestValidatePhoneNumber);
  yield takeLatest(
    CONSTANTS.REQUEST_CANCEL_PORTING_NUMBER,
    cancelNumberPortingRequest
  );
  yield takeLatest(
    CONSTANTS.REQUEST_VALIDATE_MSISDN_OTP,
    requestForValidateOtp
  );
  yield takeLatest(
    CONSTANTS.REQUEST_TERM_AND_CONDITIONS,
    requestForTermAndConditions
  );
  yield takeLatest(CONSTANTS.REQUEST_REOURCE_POOL, requestResourcePool);
}

export default watcherSaga;
