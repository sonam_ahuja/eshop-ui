import * as numberPortTypesApi from '@src/common/types/api/checkout/numberPorting';
import { apiEndpoints } from '@common/constants';
import apiCaller from '@common/utils/apiCaller';
import { logError } from '@src/common/utils';

export const validatePhoneNumberService = async (
  payload: numberPortTypesApi.PHONE_POST.IRequest
): Promise<numberPortTypesApi.PHONE_POST.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.NUMBER_PORTING.VALIDATE_PHONE_NUMBER.method.toLowerCase()
    ](apiEndpoints.CHECKOUT.NUMBER_PORTING.VALIDATE_PHONE_NUMBER.url, payload);
  } catch (error) {
    logError(error);
    throw error;
  }
};
export const termAndConditionService = async (): Promise<
  numberPortTypesApi.TERM_CONDITION_GET.IResponse | Error
> => {
  try {
    const { method, url } = apiEndpoints.CHECKOUT.NUMBER_PORTING.TERM_CONDITION;

    return await apiCaller[method.toLowerCase()](url);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const portingNumberService = async (
  payload: numberPortTypesApi.PATCH.IRequest
): Promise<numberPortTypesApi.PATCH.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.NUMBER_PORTING.UPDATE_CART.method.toLowerCase()
    ](apiEndpoints.CHECKOUT.NUMBER_PORTING.UPDATE_CART.url, payload);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchReourcePoolService = async (
  payload: numberPortTypesApi.RESOURCE_POOL_GET.IRequest
): Promise<numberPortTypesApi.RESOURCE_POOL_GET.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.NUMBER_PORTING.GET_RESOURCE_POOL.method.toLowerCase()
    ](apiEndpoints.CHECKOUT.NUMBER_PORTING.GET_RESOURCE_POOL.url(payload));
  } catch (error) {
    logError(error);
    throw error;
  }
};
