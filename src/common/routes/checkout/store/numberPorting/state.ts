import { INumberPortingState } from '@checkout/store/numberPorting/types';
import {
  MIGRATION_TYPE,
  NUMBER_PORTING_STEPS,
  NUMBER_PORTING_TYPES,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';

export default (): INumberPortingState => ({
  currentPlan: PLAN_TYPE.PREPAID,
  migrationType: MIGRATION_TYPE.EMPTY,
  activeStep: NUMBER_PORTING_STEPS.ENTER_PHONE_NUMBER,
  flowType: NUMBER_PORTING_TYPES.MSISDN_WITH_OTP,
  bannerImageUrl: '',
  numberPortingCartItemId: '',
  productOfferingId: '',
  resourceType: '',
  availableNumber: [],
  mnpEligibleDates: [],
  isButtonEnable: false,
  loading: false,
  showAllNumber: false,
  showBanner: false,
  otp: '',
  operator: '',
  date: '',
  nonce: '',
  token: '',
  phoneNumber: '',
  termConditionId: '',
  termConditionCharecterstics: {
    id: '',
    name: '',
    description: '',
    documentationUrl: '',
    isAuthorized: ''
  },
  error: {
    code: 0,
    message: ''
  },
  selectedNumber: {
    id: '',
    displayId: '',
    selected: false
  }
});
