import {
  MIGRATION_TYPE,
  NUMBER_PORTING_TYPES,
  OPERATION_TYPE,
  PLAN_TYPE
} from '@checkout/store/numberPorting/enum';
import { IDateListItem } from '@checkout/store/shipping/types';

export interface INumberPortingState {
  termConditionId: string;
  bannerImageUrl: string;
  phoneNumber: string;
  migrationType: MIGRATION_TYPE;
  currentPlan: PLAN_TYPE;
  availableNumber: IResourceNumber[];
  flowType: NUMBER_PORTING_TYPES;
  termConditionCharecterstics: ITermConditionCharecterstics;
  error: IError;
  isButtonEnable: boolean;
  loading: boolean;
  showAllNumber: boolean;
  showBanner: boolean;
  selectedNumber: IResourceNumber;
  otp: string;
  activeStep: string;
  operator: string;
  numberPortingCartItemId: string;
  productOfferingId: string;
  resourceType: string;
  date?: string;
  nonce?: string;
  token?: string;
  mnpEligibleDates?: IDateListItem[];
}
export interface IResourcePoolRequest {
  resourceType: string;
}

export interface IResourcePoolResponse {
  show: boolean;
  imageUrl?: string;
  productOfferingId?: string;
  mnpEligibleDates?: number[];
  resourceType?: string;
  cartItemId?: string;
  resources?: IResourceNumber[] | [];
  selectedResourceType?: OPERATION_TYPE.MNP | OPERATION_TYPE.MNS;
  mnpSelectedResource?: IMNPSelectedResource;
}

export interface IMNPSelectedResource {
  selectedNumber?: string;
  numberType?: PLAN_TYPE.HYBRID | PLAN_TYPE.POSTPAID | PLAN_TYPE.PREPAID;
  migrationType?:
    | MIGRATION_TYPE.END_OF_PROMOTIOM
    | MIGRATION_TYPE.DESIRED_DATE
    | MIGRATION_TYPE.WITHDRAWAL;
  transferDate?: string; // dd-mm-yyyy
}
export interface ITermConditionCharecterstics {
  id: string;
  name: string;
  description: string;
  documentationUrl: string;
  isAuthorized: string;
}
export interface ITermConditionResponse {
  id: string;
  partyPrivacyProfileTypeCharacteristics: ITermConditionCharecterstics;
}
export interface IResourceNumber {
  id: string;
  displayId: string;
  selected?: boolean;
}

export interface IError {
  code?: number;
  message?: string;
}

export interface IAddDeleteCartRequest {
  operationType: OPERATION_TYPE;
  consent: boolean | null;
  cartItems:
    | ICartItemAdd[]
    | ICartItemDelete[]
    | [ICartItemDelete, ICartItemAdd];
}

export interface ICartItemDelete {
  actions: string;
  cartItemId: string;
  product?: IProduct;
}
export interface ICartItemAdd {
  action: string;
  cartItemId?: string;
  product: IProduct;
}
export interface IProduct {
  id: string;
  characteristics?: IMNSCharacteristics | IMNPCharacteristics;
}

export interface IMNSCharacteristics {
  selectedNumber: string;
}
export interface IMNPCharacteristics {
  operator?: string;
  numberType: PLAN_TYPE;
  otpNonce: string;
  transferDate?: string;
  migrationType: string;
}

export interface IAddDeleteCartResponse {
  success: boolean;
  cartItemId: string;
  phoneNumber: IResourceNumber;
}

export interface IValidateNumberResponse {
  success: boolean;
  nonce: string;
}
export interface IValidateNumberRequest {
  otpContext: string;
  action: OPERATION_TYPE.SEND | OPERATION_TYPE.VALIDATE;
  mobileNumber: string;
  nonce?: string;
  otp?: string;
}

export interface IVerifyOTPRequest {
  action: string;
  mobileNumber: string;
  otp: string; // encrypted
  nonce: string;
}

export interface IVerifyOTPResponse {
  success: boolean;
  token?: string;
}

export interface IPhoneNumberValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
}
export interface ISetMigrationType {
  migrationType: MIGRATION_TYPE;
  value?: string;
}

export interface ICharecterstics {
  selectedNumber: string;
  operator?: string;
  numberType?: string;
  otpNonce?: string;
  transferDate?: string;
  migrationType?: string;
}
