import actions from '@checkout/store/orderReview/actions';
import constants from '@checkout/store/orderReview/constants';
import {
  AGREEMENT_PRIVACY_GROUP,
  ISetTelekomType
} from '@checkout/store/orderReview/types';

describe('orderReview actions', () => {
  it('setTelekomService action creator should return a object with expected value', () => {
    const payload: ISetTelekomType = {
      id: '12',
      type: AGREEMENT_PRIVACY_GROUP.NOTIFICATION
    };
    const expectedAction = {
      type: constants.SET_TELEKOM_SERVICE,
      payload
    };
    expect(actions.setTelekomService(payload)).toEqual(expectedAction);
  });
});
