import orderReviewState from '@checkout/store/orderReview/state';
import { orderReviewReducer } from '@checkout/store/orderReview';

describe('Order review main reducer', () => {
  it('orderReviewReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(orderReviewReducer(orderReviewState(), definedAction)).toEqual(
      orderReviewState()
    );
  });
});
