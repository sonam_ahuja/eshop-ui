import orderReview from '@checkout/store/orderReview/reducers';
import orderReviewState from '@checkout/store/orderReview/state';
import {
  AGREEMENT_PRIVACY_GROUP,
  IOrderReviewState
} from '@checkout/store/orderReview/types';
import actions from '@checkout/store/orderReview/actions';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';

// tslint:disable-next-line: no-big-function
describe('Order Review Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IOrderReviewState = orderReview(
    undefined,
    definedAction
  );
  let expectedState: IOrderReviewState = orderReviewState();

  const initializeValue = () => {
    initialStateValue = orderReview(undefined, definedAction);
    expectedState = orderReviewState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('SET_TELEKOM_SERVICE should mutate state, when agreement privacy group is notification type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      telekomProductServiceInfo: false
    };
    const action = actions.setTelekomService({
      id: '12',
      type: AGREEMENT_PRIVACY_GROUP.NOTIFICATION
    });
    const telekomServiceStore = orderReview(initialStateValue, action);
    expect(telekomServiceStore).toEqual(expected);
  });

  it('SET_TELEKOM_SERVICE should mutate state, when agreement privacy group is terms of services type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      agreeOnTermsAndCondition: true
    };
    const action = actions.setTelekomService({
      id: '12',
      type: AGREEMENT_PRIVACY_GROUP.TERMS_OF_SERVICE
    });
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('CLOSE_MODAL should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      openPaymentFailureModal: false
    };
    const action = actions.closeModal();
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('CHOOSE_OTHER_METHOD should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      openPaymentFailureModal: false
    };
    const action = actions.chooseOtherMethod();
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('TRY_AGAIN should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      openPaymentFailureModal: false
    };
    const action = actions.tryAgain();
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('PLACE_ORDER_LOADING should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      placeOrderLoading: true
    };
    const action = actions.placeOrderLoading(true);
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('PLACE_ORDER_SUCCESS should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      openPaymentFailureModal: true,
      activeStep: PLACE_ORDER_CODE.PAYMENT_DECLINED
    };
    const action = actions.placeOrderSuccess(
      PLACE_ORDER_CODE.INSUFFICIENT_FUNDS
    );
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('PLACE_ORDER_ERROR should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      openPaymentFailureModal: true
    };
    const action = actions.placeOrderError(PLACE_ORDER_CODE.PAYMENT_DECLINED);
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('FETCH_AGREEMENT_LOADING should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState,
      loading: true
    };
    const action = actions.fetchAgreementLoading(true);
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('FETCH_AGREEMENT_ERROR should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState
    };
    const action = actions.fetchAgreementError();
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('FETCH_AGREEMENT_SUCCESS should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const expected: IOrderReviewState = {
      ...expectedState
    };
    const action = actions.fetchAgreementSuccess([]);
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toEqual(expected);
  });

  it('INNER_AGREEMENT_TOGGLE should mutate state when AGREE_TERMS_CONDITION type dispatch', () => {
    const agreementResponse = [
      {
        subConsents: [
          {
            description: '',
            version: '1',
            documentationUrls: [
              {
                name: 'string',
                relativeUrl: 'string',
                absoluteUrl: 'string'
              }
            ],
            id: '1',
            name: 'name',
            privacyGroup: AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
            partyPrivacyProfileTypeCharacteristics: [
              {
                id: '2',
                name: 'name',
                description: 'description',
                documentationUrl: '/url',
                privacyUsagePurpose: 'privacy',
                privacyType: 'privacyType',
                isAuthorized: true
              }
            ]
          }
        ],
        description: '',
        version: '1',
        documentationUrls: [
          {
            name: 'string',
            relativeUrl: 'string',
            absoluteUrl: 'string'
          }
        ],
        id: '1',
        name: 'name',
        privacyGroup: AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
        partyPrivacyProfileTypeCharacteristics: [
          {
            id: '2',
            name: 'name',
            description: 'description',
            documentationUrl: '/url',
            privacyUsagePurpose: 'privacy',
            privacyType: 'privacyType',
            isAuthorized: true
          }
        ]
      },
      {
        description: '',
        version: '1',
        documentationUrls: [
          {
            name: 'string',
            relativeUrl: 'string',
            absoluteUrl: 'string'
          }
        ],
        id: '2',
        name: 'name',
        privacyGroup: AGREEMENT_PRIVACY_GROUP.TERMS_OF_SERVICE,
        partyPrivacyProfileTypeCharacteristics: [
          {
            id: '2',
            name: 'name',
            description: 'description',
            documentationUrl: '/url',
            privacyUsagePurpose: 'privacy',
            privacyType: 'privacyType',
            isAuthorized: true
          }
        ]
      }
    ];
    const action = actions.innerAgreementToggle({
      outerIndex: 0,
      innerIndex: 0
    });
    initialStateValue.agreementResponse = agreementResponse;
    const serviceStore = orderReview(initialStateValue, action);
    expect(serviceStore).toBeDefined();
  });
});
