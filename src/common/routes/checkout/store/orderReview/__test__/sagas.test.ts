import orderReviewSaga, {
  chooseOtherMethod,
  downloadPdf,
  fetchAgreements,
  placeOrderFunction,
  tryAgainFunction
} from '@checkout/store/orderReview/sagas';

describe('Order Review Saga', () => {
  it('orderReviewSaga saga test', () => {
    const generator = orderReviewSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  it('chooseOtherMethod saga test', () => {
    const generator = chooseOtherMethod();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('placeOrderFunction saga test', () => {
    const generator = placeOrderFunction();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('tryAgainFunction saga test', () => {
    const generator = tryAgainFunction();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchAgreements saga test', () => {
    const generator = fetchAgreements();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  it('downloadPdf saga test', () => {
    const generator = downloadPdf({
      type: 'string',
      payload: 'string'
    });
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
