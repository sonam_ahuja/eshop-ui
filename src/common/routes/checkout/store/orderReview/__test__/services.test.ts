import {
  downloadPdfService,
  fetchAgreementService,
  placeOrderService
} from '@checkout/store/orderReview/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

describe('order Review Service', () => {
  const mock = new MockAdapter(axios);
  it('placeOrderService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.PLACE_ORDER;
    const addressPayload = {};
    mock.onPost(url).replyOnce(200, addressPayload);
    const data = {
      termsAndConditions: false,
      marketingConsents: []
    };
    axios
      .post(url)
      .then(async () => {
        const result = await placeOrderService(data);
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('fetchAgreementService success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.PLACE_ORDER;
    const addressPayload = {};
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchAgreementService();
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });

  it('downloadPdfService success test', async () => {
    const url = apiEndpoints.CHECKOUT.GET_PDF.url('/');
    const addressPayload = {};
    mock.onGet(url).replyOnce(200, addressPayload);
    axios
      .get(url)
      .then(async () => {
        const result = await downloadPdfService('/base');
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });
});
