import {
  changeAuthorizedState,
  marketingAgreementMainChange
} from '@checkout/store/orderReview/transformer';
import orderReviewState from '@checkout/store/orderReview/state';
import {
  AGREEMENT_PRIVACY_GROUP,
  IAgreementResponse,
  IOrderReviewState
} from '@checkout/store/orderReview/types';

describe('Transformer', () => {
  const data: IAgreementResponse[] = [
    {
      id: '1',
      name: 'name',
      description: '',
      version: '',
      documentationUrls: [
        {
          name: 'string',
          relativeUrl: 'string',
          absoluteUrl: 'string'
        }
      ],
      privacyGroup: AGREEMENT_PRIVACY_GROUP.NOTIFICATION,
      partyPrivacyProfileTypeCharacteristics: [
        {
          id: '2',
          name: 'name',
          description: 'description',
          documentationUrl: '/url',
          privacyUsagePurpose: 'privacy',
          privacyType: 'privacyType',
          isAuthorized: true
        }
      ]
    }
  ];
  it('fetchAgreements saga test', () => {
    const orderReview: IOrderReviewState = {
      ...orderReviewState(),
      agreementResponse: data
    };

    expect(
      changeAuthorizedState(
        orderReview,
        '12',
        AGREEMENT_PRIVACY_GROUP.NOTIFICATION
      )
    ).toBeUndefined();
  });
  it('fetchAgreements saga test', () => {
    expect(marketingAgreementMainChange(data, true)).toBeUndefined();
  });
});
