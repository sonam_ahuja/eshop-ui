import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/orderReview/constants';
import {
  IAgreementResponse,
  ISetTelekomType
} from '@checkout/store/orderReview/types';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';

export default {
  setTelekomService: actionCreator<ISetTelekomType>(
    CONSTANTS.SET_TELEKOM_SERVICE
  ),
  placeOrder: actionCreator<undefined>(CONSTANTS.PLACE_ORDER),
  placeOrderLoading: actionCreator<boolean>(CONSTANTS.PLACE_ORDER_LOADING),
  placeOrderSuccess: actionCreator<PLACE_ORDER_CODE>(
    CONSTANTS.PLACE_ORDER_SUCCESS
  ),
  placeOrderError: actionCreator<PLACE_ORDER_CODE>(CONSTANTS.PLACE_ORDER_ERROR),
  closeModal: actionCreator<undefined>(CONSTANTS.CLOSE_MODAL),
  chooseOtherMethod: actionCreator<undefined>(CONSTANTS.CHOOSE_OTHER_METHOD),
  tryAgain: actionCreator<undefined>(CONSTANTS.TRY_AGAIN),
  fetchAgreementLoading: actionCreator<boolean>(
    CONSTANTS.FETCH_AGREEMENT_LOADING
  ),
  fetchAgreementSuccess: actionCreator<IAgreementResponse[]>(
    CONSTANTS.FETCH_AGREEMENT_SUCCESS
  ),
  fetchAgreementError: actionCreator<void>(CONSTANTS.FETCH_AGREEMENT_ERROR),
  fetchAgreement: actionCreator<void>(CONSTANTS.FETCH_AGREEMENT),
  downloadPdf: actionCreator<string>(CONSTANTS.DOWNLOAD_PDF),
  performPlaceOrder: actionCreator<void>(CONSTANTS.PERFORM_PLACE_ORDER),
  mainCheckboxClick: actionCreator(CONSTANTS.MAIN_CHECKBOX_CLICK),
  mainAgreementToggle: actionCreator<number>(CONSTANTS.MAIN_AGREEMENT_TOGGLE),
  fetchDiscount: actionCreator<void>(CONSTANTS.FETCH_DISCOUNT),
  fetchDiscountError: actionCreator<void>(CONSTANTS.FETCH_DISCOUNT_ERROR),
  fetchDiscountSuccess: actionCreator<void>(CONSTANTS.FETCH_DISCOUNT_SUCCESS),
  checkForActiveRoute: actionCreator<void>(CONSTANTS.CHECK_FOR_ACTIVE_ROUTE),
  updateStateAfterPlaceOrder: actionCreator<void>(
    CONSTANTS.UPDATE_STATE_AFTER_PLACE_ORDER
  ),
  innerAgreementToggle: actionCreator<{
    outerIndex: number;
    innerIndex: number;
  }>(CONSTANTS.INNER_AGREEMENT_TOGGLE)
};
