import { AnyAction, Reducer } from 'redux';
import {
  AGREEMENT_PRIVACY_GROUP,
  IAgreementResponse,
  IOrderReviewState,
  ISetTelekomType
} from '@checkout/store/orderReview/types';
import initialState from '@checkout/store/orderReview/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@checkout/store/orderReview/constants';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';
import { sendTermsToggleEvent } from '@src/common/events/orderSummary';

import {
  changeAuthorizedState,
  defaultAgreementCheck,
  marketingAgreementMainChange,
  syncAllLevelCheckBoxes
} from './transformer';

const reducers = {
  [CONSTANTS.SET_TELEKOM_SERVICE]: (
    state: IOrderReviewState,
    payload: ISetTelekomType
  ) => {
    switch (payload.type) {
      case AGREEMENT_PRIVACY_GROUP.TERMS_OF_SERVICE:
        state.agreeOnTermsAndCondition = !state.agreeOnTermsAndCondition;
        changeAuthorizedState(state, payload.id, payload.type);
        sendTermsToggleEvent(state.agreeOnTermsAndCondition);
        break;
      case AGREEMENT_PRIVACY_GROUP.NOTIFICATION:
        changeAuthorizedState(state, payload.id, payload.type);
        break;

      default:
        break;
    }
  },
  [CONSTANTS.CLOSE_MODAL]: (state: IOrderReviewState) => {
    state.openPaymentFailureModal = false;
  },
  [CONSTANTS.CHOOSE_OTHER_METHOD]: (state: IOrderReviewState) => {
    state.openPaymentFailureModal = false;
  },
  [CONSTANTS.TRY_AGAIN]: (state: IOrderReviewState) => {
    state.openPaymentFailureModal = false;
  },
  [CONSTANTS.PLACE_ORDER_LOADING]: (
    state: IOrderReviewState,
    payload: boolean
  ) => {
    state.placeOrderLoading = payload;
  },
  [CONSTANTS.PLACE_ORDER_SUCCESS]: (state: IOrderReviewState) => {
    state.openPaymentFailureModal = true;
  },
  [CONSTANTS.PLACE_ORDER_ERROR]: (
    state: IOrderReviewState,
    payload: PLACE_ORDER_CODE
  ) => {
    state.activeStep = payload;
    state.openPaymentFailureModal = true;
  },
  [CONSTANTS.FETCH_AGREEMENT_LOADING]: (
    state: IOrderReviewState,
    payload: boolean
  ) => {
    state.loading = payload;
  },
  [CONSTANTS.FETCH_AGREEMENT_ERROR]: (state: IOrderReviewState) => {
    state.loading = false;
  },
  [CONSTANTS.FETCH_AGREEMENT_SUCCESS]: (
    state: IOrderReviewState,
    payload: IAgreementResponse[]
  ) => {
    state.loading = false;
    state.agreementResponse = payload;
  },
  [CONSTANTS.MAIN_AGREEMENT_TOGGLE]: (
    state: IOrderReviewState,
    payload: number
  ) => {
    state.agreementResponse[
      payload
    ].partyPrivacyProfileTypeCharacteristics[0].isAuthorized = !state
      .agreementResponse[payload].partyPrivacyProfileTypeCharacteristics[0]
      .isAuthorized;

    marketingAgreementMainChange(
      state.agreementResponse[payload].subConsents as IAgreementResponse[],
      state.agreementResponse[payload].partyPrivacyProfileTypeCharacteristics[0]
        .isAuthorized
    );

    syncAllLevelCheckBoxes(state);
  },
  [CONSTANTS.INNER_AGREEMENT_TOGGLE]: (
    state: IOrderReviewState,
    payload: { outerIndex: number; innerIndex: number }
  ) => {
    const subConsents = state.agreementResponse[payload.outerIndex].subConsents;
    if (subConsents) {
      subConsents[
        payload.innerIndex
      ].partyPrivacyProfileTypeCharacteristics[0].isAuthorized = !subConsents[
        payload.innerIndex
      ].partyPrivacyProfileTypeCharacteristics[0].isAuthorized;

      syncAllLevelCheckBoxes(state);
    }
  },
  [CONSTANTS.MAIN_CHECKBOX_CLICK]: (state: IOrderReviewState) => {
    state.mainMarketingCheckbox = !state.mainMarketingCheckbox;
    defaultAgreementCheck(state.mainMarketingCheckbox, state.agreementResponse);
  },
  [CONSTANTS.UPDATE_STATE_AFTER_PLACE_ORDER]: (state: IOrderReviewState) => {
    state.agreeOnTermsAndCondition = false;
  }
};
export default withProduce(initialState, reducers) as Reducer<
  IOrderReviewState,
  AnyAction
>;
