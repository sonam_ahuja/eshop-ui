import basketActions from '@basket/store/actions';
import history from '@src/client/history';
import CONSTANTS from '@checkout/store/orderReview/constants';
import actions from '@checkout/store/orderReview/actions';
import paymentActions from '@checkout/store/payment/actions';
import { PLACE_ORDER_CONSTANT } from '@checkout/store/enums';
import {
  discountConsentService,
  downloadPdfService,
  fetchAgreementService,
  placeOrderService
} from '@checkout/store/orderReview/services';
import { put, select, takeLatest } from 'redux-saga/effects';
import { isBrowser, logError } from '@src/common/utils';
import store from '@src/common/store';
import { downloadPdfFile } from '@src/common/utils/downloadBlob';
import { checkMonthlyCondition } from '@checkout/store/personalInfo/selector';
import checkoutActions from '@checkout/store/actions';
import { sendOrderPlacedEvent } from '@src/common/events/checkout';
import createMarketDiscountPayload from '@checkout/utils/marketDiscountPayload';
import { getOrderReviewState } from '@checkout/utils';
import { fetchBasketService } from '@basket/store/services';
import ROUTE_CONSTANTS from '@common/constants/routes';
import { getBasketTranslation } from '@src/common/store/common';
import { IBasket as IBasketTranslation } from '@src/common/store/types/translation';

import { IOrderReviewState } from './types';
import { checkForActiveState } from './transformer';

const CHECKOUT_ORDER_REVIEW = '/checkout/order-review';

export function* chooseOtherMethod(): Generator {
  try {
    yield put(actions.closeModal());
    const checkoutState = store.getState().checkout.checkout;

    yield put(paymentActions.checkForIsUpfront(checkoutState));
    if (history) {
      history.push('/checkout/payment');
    }
  } catch (error) {
    logError(error);
  }
}

export function* tryAgainFunction(): Generator {
  try {
    yield put(actions.closeModal());
    yield put(actions.placeOrder());
  } catch (error) {
    logError(error);
    yield put(actions.closeModal());
  }
}

export function* fetchAgreements(): Generator {
  try {
    yield put(checkoutActions.fetchCheckoutAddressLoading(true));
    yield put(actions.fetchAgreementLoading(true));

    const payload = {
      isCalledFromCheckout: true,
      showFullPageError: false
    };
    const basketResponse = yield fetchBasketService({
      showFullPageError: payload.showFullPageError
    });

    if (basketResponse.cartItems) {
      const { cartItems, cartSummary, totalItems } = basketResponse;

      yield put(
        basketActions.setBasketData({
          cartItems,
          cartSummary,
          totalItems
        })
      );

      yield put(basketActions.setCheckoutButtonStatus(cartItems));
      yield put(checkoutActions.evaluateCartSummaryPrice(cartSummary));
    }
    const response = yield fetchAgreementService();
    yield put(actions.fetchAgreementSuccess(response.consents));
    yield put(checkoutActions.fetchCheckoutAddressLoading(false));
  } catch (error) {
    logError(error);
    yield put(actions.fetchAgreementError());
  }
}

export function* checkForActiveRoute(): Generator {
  try {
    const { personalInfo, shippingInfo, billing } = store.getState().checkout;
    const {
      skipBillingStep
    } = store.getState().configuration.cms_configuration.modules.checkout.billingInfo;

    const activeStep = checkForActiveState(
      personalInfo,
      shippingInfo,
      billing,
      skipBillingStep
    );

    if (activeStep !== CHECKOUT_ORDER_REVIEW && history) {
      history.push(activeStep);
    }
  } catch (error) {
    logError(error);
  }
}

export function* placeOrderFunction(): Generator {
  try {
    yield put(actions.placeOrderLoading(true));
    const {
      creditCheckRequired
    } = store.getState().configuration.cms_configuration.global;

    if (creditCheckRequired && checkMonthlyCondition()) {
      yield put(checkoutActions.isCreditCheckRequired(PLACE_ORDER_CONSTANT));
    } else {
      yield put(actions.performPlaceOrder());
    }
  } catch (error) {
    yield put(actions.placeOrderLoading(false));
  }
}

export function* downloadPdf(action: {
  type: string;
  payload: string;
}): Generator {
  try {
    const requestPayload = action.payload.split('/documents/');
    const response = yield downloadPdfService(requestPayload[1]);

    if (isBrowser) {
      const blob = new Blob([response], { type: 'application/pdf' });

      const url = window.URL.createObjectURL(blob);
      downloadPdfFile(url);
    }
  } catch (error) {
    logError(error);
    yield put(actions.fetchAgreementError());
  }
}

export function* performPlaceOrder(): Generator {
  try {
    const {
      agreementResponse,
      agreeOnTermsAndCondition
    } = store.getState().checkout.orderReview;
    const basketTranslation: IBasketTranslation = yield select(
      getBasketTranslation
    );
    const priceChangedMessage: string = basketTranslation.priceChanged;

    const orderPayload = {
      termsAndConditions: agreeOnTermsAndCondition,
      marketingConsents: agreementResponse
    };
    const response = yield placeOrderService(orderPayload);
    if (history) {
      if (response.priceChanged) {
        yield put(
          basketActions.setPriceNotificationStrip({
            show: response.priceChanged,
            notification: priceChangedMessage
          })
        );
        history.push(ROUTE_CONSTANTS.BASKET);
        yield put(actions.placeOrderLoading(false));
      } else {
        sendOrderPlacedEvent(response);
        yield put(actions.placeOrderLoading(false));
        yield put(basketActions.resetBasket());

        history.push(`/order-confirmation?id=${response.orderId}`);
      }
      yield put(actions.updateStateAfterPlaceOrder());
    }
  } catch (error) {
    logError(error);
    yield put(actions.placeOrderLoading(false));
    if (error && error.orderStatus) {
      yield put(actions.placeOrderError(error.orderStatus.status));
    }
  }
}

export function* fetchMarketDiscount(): Generator {
  try {
    const orderReviewState: IOrderReviewState = yield select(
      getOrderReviewState
    );
    const discountConsentRequest = createMarketDiscountPayload(
      orderReviewState
    );

    const response = yield discountConsentService(discountConsentRequest);
    if (response && response.cartSummary) {
      yield put(actions.fetchDiscountSuccess());
      const { cartSummary } = response;
      yield put(basketActions.setCartSummary(cartSummary));
    } else {
      yield put(actions.fetchDiscountError());
    }
  } catch (error) {
    logError(error);
    yield put(actions.fetchDiscountError());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.TRY_AGAIN, tryAgainFunction);
  yield takeLatest(CONSTANTS.CHOOSE_OTHER_METHOD, chooseOtherMethod);
  yield takeLatest(CONSTANTS.PLACE_ORDER, placeOrderFunction);
  yield takeLatest(CONSTANTS.FETCH_AGREEMENT, fetchAgreements);
  yield takeLatest(CONSTANTS.DOWNLOAD_PDF, downloadPdf);
  yield takeLatest(CONSTANTS.PERFORM_PLACE_ORDER, performPlaceOrder);
  yield takeLatest(CONSTANTS.FETCH_DISCOUNT, fetchMarketDiscount);
  yield takeLatest(CONSTANTS.CHECK_FOR_ACTIVE_ROUTE, checkForActiveRoute);
}
export default watcherSaga;
