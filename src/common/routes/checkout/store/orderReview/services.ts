import apiCaller from '@common/utils/apiCaller';
import * as orderReviewApi from '@common/types/api/checkout/orderReview/orderReview';
import {
  IAgreementResponse,
  IOrderTrackingError,
  IOrderTrackingResponse
} from '@checkout/store/orderReview/types';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

export const placeOrderService = async (payload: {
  termsAndConditions: boolean;
  marketingConsents: IAgreementResponse[];
}): Promise<IOrderTrackingResponse | IOrderTrackingError> => {
  try {
    return await apiCaller.post(apiEndpoints.CHECKOUT.PLACE_ORDER.url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const fetchAgreementService = async (): Promise<
  IAgreementResponse[] | Error
> => {
  try {
    return await apiCaller.get(apiEndpoints.CHECKOUT.GET_AGREEMENTS.url);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const downloadPdfService = async (url: string): Promise<{} | Error> => {
  try {
    return await apiCaller.get(apiEndpoints.CHECKOUT.GET_PDF.url(url), {
      responseType: 'blob'
    });
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const discountConsentService = async (
  payload: orderReviewApi.CONSENT_POST.IRequest
): Promise<orderReviewApi.CONSENT_POST.IResponse | Error> => {
  try {
    return await apiCaller.post(
      apiEndpoints.CHECKOUT.POST_MARKETING_DISCOUNT_CONSENT.url,
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};
