import { IOrderReviewState } from '@checkout/store/orderReview/types';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';

export default (): IOrderReviewState => ({
  telekomProductServiceInfo: false,
  loading: false,
  thirdPartyProductServiceInfo: false,
  agreeOnTermsAndCondition: false,
  placeOrderLoading: false,
  openPaymentFailureModal: false,
  activeStep: PLACE_ORDER_CODE.PAYMENT_DECLINED,
  thirdPartyProductServiceDesc: '',
  telekomProductServiceInfoDesc: '',
  termsAndConditionDesc: '',
  agreementResponse: [],
  mainMarketingCheckbox: false
});
