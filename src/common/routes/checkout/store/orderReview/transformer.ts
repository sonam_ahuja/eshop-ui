import {
  AGREEMENT_PRIVACY_GROUP,
  IAgreementResponse,
  IOrderReviewState
} from '@checkout/store/orderReview/types';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import { IShippingAddressState } from '@checkout/store/shipping/types';
import { IBillingState } from '@checkout/store/billing/types';

const CHECKOUT_ORDER_REVIEW = '/checkout/order-review';
const CHECKOUT_PERSONAL_INFO = '/checkout/personal-info';
const CHECKOUT_SHIPPING = '/checkout/shipping';

export const changeAuthorizedState = (
  data: IOrderReviewState,
  id: string,
  type: AGREEMENT_PRIVACY_GROUP
) => {
  data.agreementResponse.forEach(item => {
    if (item.privacyGroup === type) {
      item.partyPrivacyProfileTypeCharacteristics.forEach(characteristic => {
        if (characteristic.id === id) {
          characteristic.isAuthorized = !characteristic.isAuthorized;
        }
      });
    }
  });
};

export const marketingAgreementMainChange = (
  stateSubConsentData: IAgreementResponse[],
  authorized: boolean
) => {
  stateSubConsentData.forEach(item => {
    item.partyPrivacyProfileTypeCharacteristics[0].isAuthorized = authorized;
  });
};

export const defaultAgreementCheck = (
  mainValue: boolean,
  stateConsentData: IAgreementResponse[]
) => {
  stateConsentData.forEach(item => {
    item.partyPrivacyProfileTypeCharacteristics[0].isAuthorized = mainValue;
    if (item.subConsents && item.subConsents.length > 0) {
      const subConsentData = item.subConsents;
      subConsentData.forEach(subData => {
        subData.partyPrivacyProfileTypeCharacteristics[0].isAuthorized = mainValue;
      });
    }
  });
};

export const syncAllLevelCheckBoxes = (orderReviewState: IOrderReviewState) => {
  const { agreementResponse } = orderReviewState;

  agreementResponse.forEach(item => {
    let checkOfInnerFlag = true;
    if (item.subConsents && item.subConsents.length > 0) {
      const subConsentData = item.subConsents;
      subConsentData.forEach(subData => {
        if (!subData.partyPrivacyProfileTypeCharacteristics[0].isAuthorized) {
          checkOfInnerFlag = false;

          return;
        }
      });
    }
    item.partyPrivacyProfileTypeCharacteristics[0].isAuthorized = checkOfInnerFlag;
  });

  let mainCheckFlag = true;

  agreementResponse.forEach(item => {
    if (!item.partyPrivacyProfileTypeCharacteristics[0].isAuthorized) {
      mainCheckFlag = false;

      return;
    }
  });

  orderReviewState.mainMarketingCheckbox = mainCheckFlag;
};

export const isActiveStepRequired = (
  personalInfo: IPersonalInfoState,
  shippingInfo: IShippingAddressState,
  billing: IBillingState,
  skipBillingStep: boolean
) => {
  let isRequired = false;

  if (
    !personalInfo.id ||
    !shippingInfo.streetAddressId ||
    (!skipBillingStep && !billing.id)
  ) {
    isRequired = true;
  }

  return isRequired;
};

export const checkForActiveState = (
  personalInfo: IPersonalInfoState,
  shippingInfo: IShippingAddressState,
  billing: IBillingState,
  skipBillingStep: boolean
) => {
  let activeStep = CHECKOUT_ORDER_REVIEW;

  if (
    isActiveStepRequired(personalInfo, shippingInfo, billing, skipBillingStep)
  ) {
    if (!skipBillingStep && !billing.id) {
      activeStep = CHECKOUT_SHIPPING;
    }

    if (!shippingInfo.streetAddressId) {
      activeStep = CHECKOUT_PERSONAL_INFO;
    }

    if (!personalInfo.id) {
      activeStep = CHECKOUT_PERSONAL_INFO;
    }
  }

  return activeStep;
};
