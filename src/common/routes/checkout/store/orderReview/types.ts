import {
  ORDER_REVIEW_AGREEMENT,
  ORDER_SUCCESS_CODE,
  PLACE_ORDER_CODE
} from '@checkout/store/enums';
import { IPersonalInfoResponse } from '@checkout/store/types';
import { IShippingAddressResponse } from '@checkout/store/shipping/types';
import { IPaymentInfoResponse } from '@checkout/store/payment/types';
import { IBillingAddressResponse } from '@checkout/store/billing/types';
import { deliveryOptionType } from '@src/common/routes/checkout/store/shipping/types';
import { IProduct } from '@src/common/routes/basket/store/types';

export interface IOrderReviewState {
  telekomProductServiceInfo: boolean;
  thirdPartyProductServiceInfo: boolean;
  agreeOnTermsAndCondition: boolean;
  placeOrderLoading: boolean;
  openPaymentFailureModal: boolean;
  activeStep: PLACE_ORDER_CODE;
  loading: boolean;
  thirdPartyProductServiceDesc: string;
  telekomProductServiceInfoDesc: string;
  termsAndConditionDesc: string;
  mainMarketingCheckbox: boolean;
  agreementResponse: IAgreementResponse[];
}

export type telekomServicType =
  | ORDER_REVIEW_AGREEMENT.TELEKOM_PRODUCT_SERVICE
  | ORDER_REVIEW_AGREEMENT.THIRD_PARTY_PRODUCT_SERVICE
  | ORDER_REVIEW_AGREEMENT.AGREE_TERMS_CONDITION;

export interface ISetTelekomType {
  type: AGREEMENT_PRIVACY_GROUP;
  id: string;
}

export interface IOrderReviewResponse {
  personalInfo: IPersonalInfoResponse;
  shippinginfo: IShippingAddressResponse;
  paymentInfo: IPaymentInfoResponse;
  billingInfo: IBillingAddressResponse;
}

export type placeOrderResponseType =
  | PLACE_ORDER_CODE.ORDER_SUCCESS
  | PLACE_ORDER_CODE.SOMETHING_WRONG
  | PLACE_ORDER_CODE.PAYMENT_DECLINED
  | PLACE_ORDER_CODE.INSUFFICIENT_FUNDS;

export interface IOrderTrackingCartItem {
  id: string;
  quantity: number;
  group: string;
  product: IProduct;
}

export interface IOrderTrackingResponse {
  priceChanged: boolean;
  orderStatus: {
    status: PLACE_ORDER_CODE;
    subStatus: ORDER_SUCCESS_CODE;
  };
  orderId: string;
  personalInfo: {
    id: string;
    name: string;
    email: string;
    contact: string;
  };
  cartItems: [
    {
      id: string;
      quantity: number;
      group: string;
      product: IProduct;
      cartItems: IOrderTrackingCartItem[];
    }
  ];

  shippingDetails: {
    id: string;
    estimatedDelivery: string;
    deliveryType: deliveryOptionType;
    shippingTo: string;
  };
}

export interface IOrderTrackingError {
  status: PLACE_ORDER_CODE;
  message: string;
}

export enum AGREEMENT_PRIVACY_GROUP {
  NOTIFICATION = 'notifications',
  TERMS_OF_SERVICE = 'termsOfService'
}

export interface IPrivacyProfileCharacteristic {
  id: string;
  name: string;
  description: string;
  documentationUrl: string;
  privacyUsagePurpose: string;
  privacyType: string;
  isAuthorized: boolean;
}

export interface IAgreementResponse {
  id: string;
  name: string;
  description: string;
  version: string;
  discountApplicable?: boolean;
  documentationUrls: IDocumentationUrl[];
  privacyGroup: AGREEMENT_PRIVACY_GROUP;
  partyPrivacyProfileTypeCharacteristics: IPrivacyProfileCharacteristic[];
  subConsents?: IAgreementResponse[];
}

export interface IDocumentationUrl {
  name: string;
  relativeUrl: string;
  absoluteUrl: string;
}

export interface IConsentPayload {
  id: string;
  selected: boolean;
}

export interface IMarketDiscountRequest {
  consents: IConsentPayload[];
}
