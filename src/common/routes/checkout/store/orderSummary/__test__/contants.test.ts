import orderSummaryConstant from '@checkout/store/orderSummary/constants';

describe('Constant', () => {
  it('orderSummaryReducer test', () => {
    expect(orderSummaryConstant).toBeDefined();
  });
});
