import orderSummaryState from '@checkout/store/orderSummary/state';
import { orderSummaryReducer } from '@checkout/store/orderSummary';

describe('orderSummaryReducer', () => {
  it('orderSummaryReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(orderSummaryReducer(orderSummaryState(), definedAction)).toEqual(
      orderSummaryState()
    );
  });
});
