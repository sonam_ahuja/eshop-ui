import orderSummary from '@checkout/store/orderSummary/reducers';
import orderSummaryState from '@checkout/store/orderSummary/state';
import { IOrderSummaryState } from '@checkout/store/types';

describe('Order summary reducer', () => {
  it('initial state', () => {
    const definedAction: { type: string } = { type: '' };
    const initialStateValue: IOrderSummaryState = orderSummary(
      orderSummaryState(),
      definedAction
    );
    expect(initialStateValue).toMatchSnapshot();
  });
});
