const namespace = 'CHECKOUT/CHECKOUT';

export default {
  CHECKOUT_MODULE_LOADING: `${namespace}_CHECKOUT__MODULE_LOADING`
};
