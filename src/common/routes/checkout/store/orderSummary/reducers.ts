import { AnyAction, Reducer } from 'redux';
import { IOrderSummaryState } from '@checkout/store/types';
import initialState from '@checkout/store/orderSummary/state';
import withProduce from '@utils/withProduce';

const reducers = {};

export default withProduce(initialState, reducers) as Reducer<
  IOrderSummaryState,
  AnyAction
>;
