import { IOrderSummaryState } from '@checkout/store/types';

export default (): IOrderSummaryState => ({
  orderConfirmation: {
    orderDetail: {
      emailId: '',
      orderId: '',
      contactNumber: ''
    },
    shippingDetail: {
      estimateDelivery: '',
      shippingType: '',
      address: ''
    }
  },
  orderTracker: {
    orderId: '',
    shippingAddress: '',
    trackingDetails: []
  }
});
