import actions from '@checkout/store/payment/actions';
import constants from '@checkout/store/payment/constants';

describe('payment actions', () => {
  it('fetchPaymentInfoRequested action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.FETCH_PAYMENT_INFO_REQUESTED,
      payload: true
    };
    expect(actions.fetchPaymentInfoRequested(true)).toEqual(expectedAction);
  });
  it('fetchPaymentInfoLoading action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.FETCH_PAYMENT_INFO_LOADING,
      payload: undefined
    };
    expect(actions.fetchPaymentInfoLoading()).toEqual(expectedAction);
  });

  it('fetchPaymentInfoError action creator should return a object with expected value', () => {
    const error: Error = {
      message: 'Some error',
      name: 'error',
      stack: 'stack name'
    };
    const expectedAction = {
      type: constants.FETCH_PAYMENT_INFO_ERROR,
      payload: error
    };
    expect(actions.fetchPaymentInfoError(error)).toEqual(expectedAction);
  });
  it('changeActiveCardField action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.CHANGE_CARD_FIELD,
      payload: 'number'
    };
    expect(actions.changeActiveCardField('number')).toEqual(expectedAction);
  });

  it('setSaveThisCard action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SAVE_THIS_CARD,
      payload: undefined
    };
    expect(actions.setSaveThisCard()).toEqual(expectedAction);
  });
});
