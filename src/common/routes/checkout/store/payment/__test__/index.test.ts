import paymentState from '@checkout/store/payment/state';
import { paymentReducer } from '@checkout/store/payment';

describe('Payment Reducer', () => {
  it('paymentReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(paymentReducer(paymentState(), definedAction)).toEqual(
      paymentState()
    );
  });
});
