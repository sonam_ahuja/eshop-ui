import payment from '@checkout/store/payment/reducers';
import paymentState from '@checkout/store/payment/state';
import checkoutState from '@checkout/store/state';
import {
  ICardDetails,
  IPaymentInfoResponse,
  IPaymentState
} from '@checkout/store/payment/types';
import actions from '@checkout/store/payment/actions';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import {
  cardDetails,
  cardDetailsState,
  PaymentResponseData
} from '@mocks/checkout/payment';

// tslint:disable-next-line:no-big-function
describe('Payment Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IPaymentState = payment(undefined, definedAction);
  let expectedState: IPaymentState = paymentState();

  const initializeValue = () => {
    initialStateValue = payment(undefined, definedAction);
    expectedState = paymentState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('CHANGE_CARD_FIELD should mutate shipping isOpen parameter value', () => {
    const action = actions.changeActiveCardField('number');
    const state = payment(initialStateValue, action);
    expect(state).toEqual(expectedState);
  });

  it('SET_PAYMENT_STEPS should mutate shipping state parameter value', () => {
    let action = actions.setPaymentStepChange(PAYMENT_TYPE.CREDIT_DEBIT_CARD);
    let state = payment(initialStateValue, action);
    expectedState.monthly.addingNewCard = true;
    state.monthly.addingNewCard = true;
    expectedState.upFront.addingNewCard = true;
    expect(state).toEqual(expectedState);

    const stateValue = { ...initialStateValue };
    stateValue.paymentTypeSelected = PAYMENT_TYPE.BANK_ACCOUNT;
    action = actions.setPaymentStepChange(PAYMENT_TYPE.BANK_ACCOUNT);
    state = payment(stateValue, action);
    expect(state).not.toEqual(expectedState);
  });

  it('SET_PAYMENT_STEPS should mutate shipping state parameter value when upfront is false', () => {
    initialStateValue.isUpfront = false;
    let action = actions.setPaymentStepChange(PAYMENT_TYPE.CREDIT_DEBIT_CARD);
    expectedState.isUpfront = false;
    expectedState.monthly.addingNewCard = true;

    let state = payment(initialStateValue, action);
    expect(state).toEqual(expectedState);

    const stateValue = { ...initialStateValue };
    stateValue.paymentTypeSelected = PAYMENT_TYPE.BANK_ACCOUNT;
    action = actions.setPaymentStepChange(PAYMENT_TYPE.BANK_ACCOUNT);
    state = payment(stateValue, action);
    expect(state).not.toEqual(expectedState);
  });

  it('SAVE_THIS_CARD should mutate shipping isOpen parameter value', () => {
    expectedState.upFront.saveNewCard = true;
    expectedState.addNewCard.isCardSaved = true;
    const action = actions.setSaveThisCard();
    const state = payment(initialStateValue, action);
    expect(state).toEqual(expectedState);
  });

  it('SAVE_THIS_CARD should mutate shipping parameter value when upFront is not present', () => {
    initialStateValue.isUpfront = false;
    expectedState.isUpfront = false;
    expectedState.monthly.saveNewCard = true;
    expectedState.addNewCard.isCardSaved = true;
    const action = actions.setSaveThisCard();
    const state = payment(initialStateValue, action);
    expect(state).toEqual(expectedState);
  });

  it('SAVE_CARD_DETAILS should mutate payment parameter value when isUpfront is true', () => {
    initialStateValue.isUpfront = true;
    let action = actions.setCardDetails('set');
    let state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    initialStateValue.isUpfront = false;
    action = actions.setCardDetails('set');
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_CARD_DETAILS should mutate payment parameter value when isUpfront is false', () => {
    initialStateValue.isUpfront = false;
    const action = actions.resetCardDetails(cardDetailsState);
    const stateWithUpFront = payment(initialStateValue, action);
    expect(stateWithUpFront).toBeDefined();
  });

  it('UPDATE_CARD_DETAILS should mutate payment parameter value when isUpfront is true', () => {
    initialStateValue.isUpfront = true;
    const action = actions.resetCardDetails(cardDetailsState);
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('PROCEED_TO_MONTHLY_SUCCESS should mutate should mutate payment state when upFront addingNewCard is true', () => {
    initialStateValue.upFront.addingNewCard = true;
    expectedState.upFront.addingNewCard = true;
    expectedState.isUpfront = false;
    expectedState.upFront.addingNewCard = true;
    expectedState.upFront.cardDetails = [
      {
        brand: 'dummy',
        cardNumber: '',
        cardType: 'credit',
        expiryDate: '',
        id: '0',
        isCardSaved: false,
        isDefault: true,
        lastFourDigits: 'xxxxxxxxxxxx',
        nameOnCard: '',
        securityCode: ''
      }
    ];
    const action = actions.savePaymentMethod();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('PROCEED_TO_MONTHLY_ERROR should mutate payment state when upFront addingNewCard is false', () => {
    const action = actions.paymentLoadingError();
    const state = payment(initialStateValue, action);
    expect(state).toEqual(expectedState);
  });

  it('PROCEED_TO_BILLING_ERROR should mutate payment state when upFront addingNewCard is false', () => {
    const action = actions.proceedToBillingError();
    const state = payment(initialStateValue, action);
    expect(state).toEqual(expectedState);
  });

  it('SET_UPFRONT_PAYMENT_DETAILS should mutate payment state when upFront addingNewCard is false', () => {
    const action = actions.savePaymentMethod();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('PAYMENT_LOADING should mutate loading payment state', () => {
    const action = actions.paymentLoading();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('BILLING_TO_PAYMENT_UPDATE should mutate isUpfront payment state', () => {
    const action = actions.billingToPaymentStepUpdate(true);
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('DISABLE_PROCEED_TO_BILLING should mutate isUpfront payment state', () => {
    const action = actions.disableProceedToBillingButton(true);
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('PROCEED_TO_MONTHLY_SUCCESS should mutate isUpfront payment state', () => {
    const action = actions.proceedToMonthlySuccess(PaymentResponseData);
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('PROCEED_TO_BILLING_SUCCESS should mutate isUpfront payment state', () => {
    const action = actions.proceedToBillingSuccess(PaymentResponseData);
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_UPFRONT_PAYMENT_SUCCESS should mutate isUpfront payment state', () => {
    const action = actions.updateUpfrontPaymentSuccess();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_MONTHLY_PAYMENT_SUCCESS should mutate isUpfront payment state', () => {
    const action = actions.updateMonthlyPaymentSuccess();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('CHECK_FOR_IS_UPFRONT should mutate isUpfront payment state', () => {
    const action = actions.checkForIsUpfront(checkoutState());
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('USE_SAME_CARD should mutate isUpfront payment state', () => {
    const action = actions.useSameCard();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('EDIT_UPFRONT should mutate isUpfront payment state', () => {
    initialStateValue.upFront.cardDetails = [cardDetails];
    const action = actions.editUpFront();
    const state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });
  it('FECTH PAYMENT INFO SUCEESS ', () => {
    const cardDetail = [
      {
        cardNumber: '1122',
        isDefault: false,
        lastFourDigits: '1122',
        cardType: 'visa',
        expiryDate: '2-12',
        securityCode: '11',
        nameOnCard: 'Batra ji',
        id: '12321',
        isCardSaved: false,
        brand: 'brand'
      }
    ];
    const payload: IPaymentInfoResponse = {
      upfront: {
        tokenizedCard: {
          cardDetails: cardDetail,
          id: ''
        },
        bankAccounts: [],
        payByLink: {},
        payOnDelivery: {},
        paymentTypeSelected: PAYMENT_TYPE.BANK_ACCOUNT
      },
      monthly: {
        sameAsUpfront: false,
        tokenizedCard: {
          cardDetails: cardDetail,
          id: ''
        },
        bankAccounts: [],
        payByLink: {},
        payOnDelivery: {},
        paymentTypeSelected: PAYMENT_TYPE.BANK_ACCOUNT
      }
    };

    let action = actions.fetchPaymentInfoSuccess(payload);
    let state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    let newPayload = { ...payload };
    newPayload.upfront.tokenizedCard = {
      cardDetails: [],
      id: ''
    };
    action = actions.fetchPaymentInfoSuccess(newPayload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    payload.upfront.paymentTypeSelected = PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    payload.upfront.tokenizedCard.cardDetails = cardDetail;

    action = actions.fetchPaymentInfoSuccess(payload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    payload.upfront.tokenizedCard.cardDetails = [];
    action = actions.fetchPaymentInfoSuccess(payload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    newPayload = { ...payload };
    newPayload.monthly.tokenizedCard = {
      cardDetails: [],
      id: ''
    };
    action = actions.fetchPaymentInfoSuccess(newPayload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    payload.monthly.paymentTypeSelected = PAYMENT_TYPE.CREDIT_DEBIT_CARD;
    payload.monthly.tokenizedCard.cardDetails = cardDetail;

    action = actions.fetchPaymentInfoSuccess(payload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    const stateValue = { ...initialStateValue };
    stateValue.upFront.cardDetails = cardDetail;
    state = payment(stateValue, action);
    expect(state).toBeDefined();

    stateValue.upFront.cardDetails[0].isDefault = true;
    state = payment(stateValue, action);
    expect(state).toBeDefined();

    payload.monthly.tokenizedCard.cardDetails = [];
    action = actions.fetchPaymentInfoSuccess(payload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    action = actions.fetchPaymentInfoSuccess(payload);
    state = payment(initialStateValue, action);
    expect(state).toBeDefined();
  });
  it('CHANGE_SELECTED_CARD should mutate isUpfront payment state', () => {
    const cardDetail: ICardDetails = {
      cardNumber: '1122',
      isDefault: false,
      lastFourDigits: '1122',
      cardType: 'visa',
      expiryDate: '2-12',
      securityCode: '11',
      nameOnCard: 'Batra ji',
      id: '12321',
      isCardSaved: false,
      brand: 'brand'
    };
    const payload = { ...cardDetails };
    const stateValue = { ...initialStateValue };

    let action = actions.changeSelectedCard(cardDetail);
    let state = payment(initialStateValue, action);
    expect(state).toBeDefined();

    stateValue.upFront.cardDetails = [cardDetail];
    action = actions.changeSelectedCard(cardDetail);
    state = payment(stateValue, action);
    expect(state).toBeDefined();

    payload.isDefault = true;
    action = actions.changeSelectedCard(payload);
    state = payment(stateValue, action);
    expect(state).toBeDefined();

    stateValue.isUpfront = false;

    payload.brand = 'dummy';
    action = actions.changeSelectedCard(payload);
    state = payment(stateValue, action);
    expect(state).toBeDefined();

    stateValue.isUpfront = true;
    action = actions.changeSelectedCard(payload);
    state = payment(stateValue, action);
    expect(state).toBeDefined();
  });
  // tslint:disable-next-line:max-file-line-count
});
