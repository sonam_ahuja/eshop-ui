import paymentSaga, {
  checkMonthlyCondition,
  fetchPaymentInfo,
  postPaymentFunction,
  savePaymentMethod,
  setNewCard,
  updateSelectedCard
} from '@checkout/store/payment/sagas';
import CONSTANTS from '@checkout/store/payment/constants';
import {
  IPaymentInfoPatchReq,
  IPaymentInfoPostReq
} from '@checkout/store/payment/types';
import { PAYMENT_TYPE } from '@common/routes/checkout/store/enums';

describe('Payment Saga', () => {
  it('paymentSaga saga test', () => {
    const paymentSagaGenerator = paymentSaga();
    expect(paymentSagaGenerator.next().value).toBeDefined();
    expect(paymentSagaGenerator.next().value).toBeDefined();
    expect(paymentSagaGenerator.next().value).toBeDefined();
    expect(paymentSagaGenerator.next().value).toBeDefined();
    expect(paymentSagaGenerator.next().value).toBeDefined();
    expect(paymentSagaGenerator.next().value).toBeUndefined();
  });

  it('fetchPaymentInfo saga test', () => {
    const fetchPaymentInfoGenerator = fetchPaymentInfo({
      type: '',
      payload: true
    });
    expect(fetchPaymentInfoGenerator.next().value).toBeUndefined();
  });

  it('checkMonthlyCondition saga test', () => {
    const checkMonthlyConditionGenerator = checkMonthlyCondition();
    expect(checkMonthlyConditionGenerator.next().value).toBeDefined();
    expect(checkMonthlyConditionGenerator.next().value).toBeUndefined();
  });

  it('setNewCard saga test', () => {
    const payload: {
      type: string;
      payload: IPaymentInfoPostReq;
    } = {
      type: CONSTANTS.SET_NEW_CARD,
      payload: {}
    };
    const setNewCardGenerator = setNewCard(payload);
    expect(setNewCardGenerator.next().value).toBeDefined();
    expect(setNewCardGenerator.next().value).toBeDefined();
    expect(setNewCardGenerator.next().value).toBeDefined();
    expect(setNewCardGenerator.next().value).toBeUndefined();
  });

  it('updateSelectedCard saga test', () => {
    const payload: {
      type: string;
      payload: IPaymentInfoPatchReq;
    } = {
      type: CONSTANTS.SET_NEW_CARD,
      payload: {
        id: '',
        type: '',
        nonce: '',
        saveCard: true,
        selectedPaymentMethod: PAYMENT_TYPE.CREDIT_DEBIT_CARD
      }
    };
    const updateSelectedCardGenerator = updateSelectedCard(payload);
    expect(updateSelectedCardGenerator.next().value).toBeDefined();
    expect(updateSelectedCardGenerator.next().value).toBeDefined();
    expect(updateSelectedCardGenerator.next().value).toBeDefined();
    expect(updateSelectedCardGenerator.next().value).toBeDefined();
  });

  it('savePaymentMethod saga test', () => {
    const updateSelectedCardGenerator = savePaymentMethod();
    expect(updateSelectedCardGenerator.next().value).toBeDefined();
    expect(updateSelectedCardGenerator.next().value).toBeUndefined();
  });

  it('postPaymentFunctionGenerator saga test', () => {
    const postPaymentFunctionGenerator = postPaymentFunction(
      {
        cardId: 'string',
        cardNumber: {
          value: 'string',
          isValid: true,
          validationMessage: 'string'
        },
        isDefault: true,
        cardType: 'string',
        expiryDate: {
          value: 'string',
          isValid: true,
          validationMessage: 'string'
        },
        securityCode: {
          value: 'string',
          isValid: true,
          validationMessage: 'string'
        },
        nameOnCard: {
          value: 'string',
          isValid: true,
          validationMessage: 'string'
        },
        isCardSaved: true,
        lastFourDigits: 'string',
        brand: 'string',
        nonce: 'string'
      },
      'string',
      PAYMENT_TYPE.BANK_ACCOUNT
    );
    expect(postPaymentFunctionGenerator.next().value).toBeDefined();
    expect(postPaymentFunctionGenerator.next().value).toBeUndefined();
  });
});
