import {
  fetchPaymentInfoService,
  setPaymentService,
  updatePaymentService
} from '@checkout/store/payment/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import { PAYMENT_TYPE } from '@common/routes/checkout/store/enums';

describe('shipping Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchPaymentInfoService success test', async () => {
    mock
      .onGet(apiEndpoints.CHECKOUT.GET_PAYMENT_INFO_DATA.url)
      .replyOnce(200, {});
    axios
      .get(apiEndpoints.CHECKOUT.GET_PAYMENT_INFO_DATA.url)
      .then(async () => {
        const result = await fetchPaymentInfoService();
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });

  it('setPaymentService success test', async () => {
    mock
      .onPost(apiEndpoints.CHECKOUT.POST_PAYMENT_UPFRONT_INFO.url)
      .replyOnce(200, {});
    axios
      .post(apiEndpoints.CHECKOUT.POST_PAYMENT_UPFRONT_INFO.url)
      .then(async () => {
        const result = await setPaymentService({});
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });

  it('updatePaymentService success test', async () => {
    mock
      .onPost(apiEndpoints.CHECKOUT.PATCH_PAYMENT_UPFRONT_INFO.url)
      .replyOnce(200, {});
    axios
      .post(apiEndpoints.CHECKOUT.PATCH_PAYMENT_UPFRONT_INFO.url)
      .then(async () => {
        const result = await updatePaymentService({
          id: '',
          type: '',
          nonce: '',
          saveCard: true,
          selectedPaymentMethod: PAYMENT_TYPE.CREDIT_DEBIT_CARD
        });
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });
});
