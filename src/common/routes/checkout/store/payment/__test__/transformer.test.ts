import {
  checkUseSameCardValue,
  isDefaultCardSelected,
  maskCardDetails,
  showUseSameCardOption
} from '@checkout/store/payment/transformer';
import { cardDetails, PaymentResponseData } from '@mocks/checkout/payment';

describe('Transformer', () => {
  it('maskCardDetails test', () => {
    expect(maskCardDetails([cardDetails])).toMatchSnapshot();
  });

  it('isDefaultCardSelected test', () => {
    expect(isDefaultCardSelected([cardDetails])).toBe(false);
  });

  it('showUseSameCardOption test', () => {
    expect(showUseSameCardOption('123', [cardDetails])).toBe(false);
  });

  it('checkUseSameCardValue test', () => {
    expect(checkUseSameCardValue(PaymentResponseData)).toBe(true);
  });
});
