import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/payment/constants';
import {
  activeFieldTypes,
  ICardDetails,
  IPaymentInfoPatchReq,
  IPaymentInfoPostReq,
  IPaymentInfoResponse,
  paymentTypes
} from '@checkout/store/payment/types';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/types';

import { ICheckoutState } from '../types';

export default {
  fetchPaymentInfoRequested: actionCreator<boolean | undefined>(
    CONSTANTS.FETCH_PAYMENT_INFO_REQUESTED
  ),
  fetchPaymentInfoLoading: actionCreator<undefined>(
    CONSTANTS.FETCH_PAYMENT_INFO_LOADING
  ),
  fetchPaymentInfoSuccess: actionCreator<IPaymentInfoResponse>(
    CONSTANTS.FETCH_PAYMENT_INFO_SUCCESS
  ),
  fetchPaymentInfoError: actionCreator<Error>(
    CONSTANTS.FETCH_PAYMENT_INFO_ERROR
  ),
  changeActiveCardField: actionCreator<activeFieldTypes>(
    CONSTANTS.CHANGE_CARD_FIELD
  ),
  setSaveThisCard: actionCreator<undefined>(CONSTANTS.SAVE_THIS_CARD),
  setCardDetails: actionCreator<string>(CONSTANTS.SAVE_CARD_DETAILS),
  resetCardDetails: actionCreator<ICardState>(CONSTANTS.UPDATE_CARD_DETAILS),

  proceedToMonthlySuccess: actionCreator<IPaymentInfoResponse>(
    CONSTANTS.PROCEED_TO_MONTHLY_SUCCESS
  ),
  paymentLoadingError: actionCreator<undefined>(
    CONSTANTS.PAYMENT_LOADING_ERROR
  ),
  proceedToBillingSuccess: actionCreator<IPaymentInfoResponse>(
    CONSTANTS.PROCEED_TO_BILLING_SUCCESS
  ),
  proceedToBillingError: actionCreator<undefined>(
    CONSTANTS.PROCEED_TO_BILLING_ERROR
  ),
  useSameCard: actionCreator<undefined>(CONSTANTS.USE_SAME_CARD),
  editUpFront: actionCreator<undefined>(CONSTANTS.EDIT_UPFRONT),
  changeSelectedCard: actionCreator<ICardDetails>(
    CONSTANTS.CHANGE_SELECTED_CARD
  ),
  billingToPaymentStepUpdate: actionCreator<boolean>(
    CONSTANTS.BILLING_TO_PAYMENT_UPDATE
  ),

  setPaymentStepChange: actionCreator<paymentTypes>(
    CONSTANTS.SET_PAYMENT_STEPS
  ),
  disableProceedToBillingButton: actionCreator<boolean>(
    CONSTANTS.DISABLE_PROCEED_TO_BILLING
  ),
  savePaymentMethod: actionCreator<undefined>(CONSTANTS.SAVE_PAYMENT_METHOD),
  paymentLoading: actionCreator<undefined>(CONSTANTS.PAYMENT_LOADING),
  updatePaymentSuccess: actionCreator<undefined>(
    CONSTANTS.UPDATE_PAYMENT_SUCCESS
  ),
  setNewCard: actionCreator<IPaymentInfoPostReq>(CONSTANTS.SET_NEW_CARD),
  updateSelectedCard: actionCreator<IPaymentInfoPatchReq>(
    CONSTANTS.UPDATE_SELECTED_CARD
  ),
  updateUpfrontPaymentSuccess: actionCreator<undefined>(
    CONSTANTS.UPDATE_UPFRONT_PAYMENT_SUCCESS
  ),
  updateMonthlyPaymentSuccess: actionCreator<undefined>(
    CONSTANTS.UPDATE_MONTHLY_PAYMENT_SUCCESS
  ),
  checkForIsUpfront: actionCreator<ICheckoutState>(
    CONSTANTS.CHECK_FOR_IS_UPFRONT
  )
};
