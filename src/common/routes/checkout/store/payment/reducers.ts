import { savedCardIndex } from '@checkout/CheckoutSteps/Payment/utils/checkCardValidation';
import { AnyAction, Reducer } from 'redux';
import initialState from '@checkout/store/payment/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@checkout/store/payment/constants';
import {
  activeFieldTypes,
  ICardDetails,
  IPaymentInfoResponse,
  IPaymentState
} from '@checkout/store/payment/types';
import {
  changeDefaultCard,
  getDefaultCard,
  mainToLocalConversion
} from '@checkout/CheckoutSteps/Payment/utils/convertData';
import { ICardState } from '@checkout/CheckoutSteps/Payment/Common/CardDetailForm/types';
import { PAYMENT_OPTION, PAYMENT_TYPE } from '@checkout/store/enums';
import {
  checkUseSameCardValue,
  enablePaymentMethod,
  isDefaultCardSelected,
  maskCardDetails,
  showUseSameCardOption
} from '@checkout/store/payment/transformer';

import { ICheckoutState } from '../types';

const reducers = {
  [CONSTANTS.CHECK_FOR_IS_UPFRONT]: (
    state: IPaymentState,
    payload: ICheckoutState
  ) => {
    if (payload) {
      state.isUpfront = payload.isUpfrontPrice;
    }
  },
  // tslint:disable-next-line:cyclomatic-complexity
  [CONSTANTS.FETCH_PAYMENT_INFO_SUCCESS]: (
    state: IPaymentState,
    payload: IPaymentInfoResponse
  ) => {
    if (payload && payload.upfront) {
      state.upFront.payByLinkId =
        payload.upfront.payByLink && payload.upfront.payByLink.id
          ? payload.upfront.payByLink.id
          : '';

      state.upFront.payOnDeliveryId =
        payload.upfront.payOnDelivery && payload.upfront.payOnDelivery.id
          ? payload.upfront.payOnDelivery.id
          : '';

      state.upFront.id =
        payload.upfront.tokenizedCard && payload.upfront.tokenizedCard.id
          ? payload.upfront.tokenizedCard.id
          : '';

      state.upFront.enabledPaymentMethods = enablePaymentMethod(
        payload,
        PAYMENT_OPTION.UPFRONT
      );
    }

    if (payload && payload.monthly) {
      state.monthly.payByLinkId =
        payload.monthly.payByLink && payload.monthly.payByLink.id
          ? payload.monthly.payByLink.id
          : '';

      state.monthly.payOnDeliveryId =
        payload.monthly.payOnDelivery && payload.monthly.payOnDelivery.id
          ? payload.monthly.payOnDelivery.id
          : '';

      state.monthly.id =
        payload.monthly.tokenizedCard && payload.monthly.tokenizedCard.id
          ? payload.monthly.tokenizedCard.id
          : '';

      state.monthly.enabledPaymentMethods = enablePaymentMethod(
        payload,
        PAYMENT_OPTION.MONTHLY
      );
    }

    if (
      payload &&
      payload.upfront &&
      payload.upfront.paymentTypeSelected !== PAYMENT_TYPE.CREDIT_DEBIT_CARD
    ) {
      state.upFront.addingNewCard = false;
      state.upFront.proceedDisabled = false;
      if (
        payload.upfront.tokenizedCard &&
        payload.upfront.tokenizedCard.cardDetails &&
        payload.upfront.tokenizedCard.cardDetails.length
      ) {
        state.upFront.cardDetails = maskCardDetails(
          payload.upfront.tokenizedCard.cardDetails
        );
        state.upFront.cardDetails.push(initialState().dummyCard);
      }
    } else {
      if (
        payload &&
        payload.upfront &&
        payload.upfront.tokenizedCard &&
        payload.upfront.tokenizedCard.cardDetails &&
        payload.upfront.tokenizedCard.cardDetails.length
      ) {
        state.upFront.cardDetails = maskCardDetails(
          payload.upfront.tokenizedCard.cardDetails
        );
        state.upFront.addingNewCard = false;
        state.upFront.proceedDisabled = isDefaultCardSelected(
          state.upFront.cardDetails
        );
        state.upFront.cardDetails.push(initialState().dummyCard);
      } else {
        state.upFront.addingNewCard =
          state.upFront.enabledPaymentMethods.tokenizedCard;
        state.upFront.proceedDisabled =
          state.upFront.enabledPaymentMethods.tokenizedCard;
      }
    }

    if (
      payload &&
      payload.monthly &&
      payload.monthly.paymentTypeSelected !== PAYMENT_TYPE.CREDIT_DEBIT_CARD
    ) {
      state.monthly.addingNewCard = false;
      state.monthly.proceedDisabled = false;
      if (
        payload.monthly.tokenizedCard &&
        payload.monthly.tokenizedCard.cardDetails &&
        payload.monthly.tokenizedCard.cardDetails.length
      ) {
        state.monthly.cardDetails = maskCardDetails(
          payload.monthly.tokenizedCard.cardDetails
        );
        state.monthly.cardDetails.push(initialState().dummyCard);
      }
    } else {
      if (
        payload.monthly &&
        payload.monthly.tokenizedCard &&
        payload.monthly.tokenizedCard.cardDetails &&
        payload.monthly.tokenizedCard.cardDetails.length
      ) {
        state.monthly.cardDetails = maskCardDetails(
          payload.monthly.tokenizedCard.cardDetails
        );
        state.monthly.addingNewCard = false;
        state.monthly.proceedDisabled = isDefaultCardSelected(
          state.monthly.cardDetails
        );
        state.monthly.cardDetails.push(initialState().dummyCard);

        if (state.upFront.cardDetails && state.upFront.cardDetails.length) {
          const upfrontDefaultCard = getDefaultCard(state.upFront.cardDetails);

          if (upfrontDefaultCard) {
            state.showUseSameCard = showUseSameCardOption(
              upfrontDefaultCard.id,
              state.monthly.cardDetails
            );
          }
        }
      } else {
        state.monthly.addingNewCard =
          state.monthly.enabledPaymentMethods.tokenizedCard;
        state.monthly.proceedDisabled =
          state.monthly.enabledPaymentMethods.tokenizedCard;
      }
    }

    state.upFront.paymentTypeSelected = payload.upfront
      .paymentTypeSelected as PAYMENT_TYPE;

    state.monthly.paymentTypeSelected = payload.monthly
      .paymentTypeSelected as PAYMENT_TYPE;

    state.monthly.useSameCard = checkUseSameCardValue(payload);
  },
  [CONSTANTS.DISABLE_PROCEED_TO_BILLING]: (
    state: IPaymentState,
    payload: boolean
  ) => {
    state.monthly.proceedDisabled = payload;
  },

  [CONSTANTS.SAVE_THIS_CARD]: (state: IPaymentState) => {
    if (state.isUpfront) {
      state.upFront.saveNewCard = !state.upFront.saveNewCard;
      state.addNewCard.isCardSaved = state.upFront.saveNewCard;
    } else {
      state.monthly.saveNewCard = !state.monthly.saveNewCard;
      state.addNewCard.isCardSaved = state.monthly.saveNewCard;
    }
  },

  [CONSTANTS.SAVE_CARD_DETAILS]: (state: IPaymentState, payload: string) => {
    state.addNewCard.nonce = payload;

    state.isUpfront
      ? (state.upFront.proceedDisabled = false)
      : (state.monthly.proceedDisabled = false);
  },

  [CONSTANTS.UPDATE_CARD_DETAILS]: (
    state: IPaymentState,
    payload: ICardState
  ) => {
    Object.assign(state.addNewCard, payload);

    state.isUpfront
      ? (state.upFront.proceedDisabled = true)
      : (state.monthly.proceedDisabled = true);
  },

  [CONSTANTS.PROCEED_TO_MONTHLY_SUCCESS]: (
    state: IPaymentState,
    payload: IPaymentInfoResponse
  ) => {
    state.isUpfront = false;
    state.loading = false;

    if (payload) {
      state.upFront.cardDetails = maskCardDetails(
        payload.upfront.tokenizedCard.cardDetails
      );
      state.addNewCard = initialState().addNewCard;
    }

    if (state.monthly.cardDetails && state.monthly.cardDetails.length) {
      const upfrontDefaultCard = getDefaultCard(state.upFront.cardDetails);

      if (upfrontDefaultCard) {
        state.showUseSameCard = showUseSameCardOption(
          upfrontDefaultCard.id,
          state.monthly.cardDetails
        );
      }
    }
  },
  [CONSTANTS.PAYMENT_LOADING_ERROR]: (state: IPaymentState) => {
    state.loading = false;
  },
  [CONSTANTS.PROCEED_TO_BILLING_ERROR]: (state: IPaymentState) => {
    state.loading = false;
  },

  [CONSTANTS.PROCEED_TO_BILLING_SUCCESS]: (
    state: IPaymentState,
    payload: IPaymentInfoResponse
  ) => {
    state.loading = false;
    if (payload) {
      state.monthly.addingNewCard = false;
      state.monthly.cardDetails = maskCardDetails(
        payload.monthly.tokenizedCard.cardDetails
      );
      state.addNewCard = initialState().addNewCard;
    }
  },

  [CONSTANTS.USE_SAME_CARD]: (state: IPaymentState) => {
    state.monthly.useSameCard = !state.monthly.useSameCard;

    if (state.monthly.useSameCard) {
      state.monthly.addingNewCard = false;
      state.monthly.proceedDisabled = false;

      const upfrontCardDetails = getDefaultCard(state.upFront.cardDetails);
      if (upfrontCardDetails) {
        if (state.monthly.cardDetails && state.monthly.cardDetails.length) {
          changeDefaultCard(state.monthly.cardDetails, upfrontCardDetails);
        } else {
          state.addNewCard = mainToLocalConversion(
            state.addNewCard,
            upfrontCardDetails
          );
        }
      }
    } else {
      state.addNewCard = initialState().addNewCard;
    }
  },

  [CONSTANTS.EDIT_UPFRONT]: (state: IPaymentState) => {
    state.isUpfront = true;

    const cardIndex = savedCardIndex(state.upFront.cardDetails);
    const cardDetails = state.upFront.cardDetails[cardIndex];

    state.addNewCard = mainToLocalConversion(state.addNewCard, cardDetails);
  },

  [CONSTANTS.CHANGE_SELECTED_CARD]: (
    state: IPaymentState,
    payload: ICardDetails
  ) => {
    const paymentType = state.isUpfront ? 'upFront' : 'monthly';
    if (payload.brand === 'dummy') {
      state[paymentType].addingNewCard = true;
      state[paymentType].proceedDisabled = true;
      changeDefaultCard(state[paymentType].cardDetails, payload);
      if (paymentType === 'monthly') {
        state[paymentType].useSameCard = false;
      }
    } else {
      state[paymentType].addingNewCard = false;
      state[paymentType].proceedDisabled = false;
      changeDefaultCard(state[paymentType].cardDetails, payload);

      if (state.upFront.cardDetails && state.upFront.cardDetails.length) {
        const upfrontSelectedCard = getDefaultCard(state.upFront.cardDetails);
        if (upfrontSelectedCard) {
          state.monthly.useSameCard = upfrontSelectedCard.id === payload.id;
        }
      }
    }
  },
  [CONSTANTS.SET_PAYMENT_STEPS]: (
    state: IPaymentState,
    payload: PAYMENT_TYPE
  ) => {
    if (state.isUpfront) {
      state.upFront.paymentTypeSelected = payload;
      if (
        state.upFront.paymentTypeSelected !== PAYMENT_TYPE.CREDIT_DEBIT_CARD
      ) {
        state.upFront.proceedDisabled = false;
      } else {
        state.upFront.addingNewCard = !state.upFront.cardDetails.length;
      }
    } else {
      state.monthly.paymentTypeSelected = payload;
      if (
        state.monthly.paymentTypeSelected !== PAYMENT_TYPE.CREDIT_DEBIT_CARD
      ) {
        state.monthly.proceedDisabled = false;
      } else {
        state.monthly.addingNewCard = !state.monthly.cardDetails.length;
      }
    }
  },
  [CONSTANTS.CHANGE_CARD_FIELD]: (
    state: IPaymentState,
    payload: activeFieldTypes
  ) => {
    state.activeCardField = payload;
  },

  [CONSTANTS.PAYMENT_LOADING]: (state: IPaymentState) => {
    state.loading = true;
  },
  [CONSTANTS.BILLING_TO_PAYMENT_UPDATE]: (
    state: IPaymentState,
    payload: boolean
  ) => {
    state.isUpfront = payload;
  },
  [CONSTANTS.UPDATE_UPFRONT_PAYMENT_SUCCESS]: (state: IPaymentState) => {
    state.isUpfront = false;
    state.loading = false;

    if (state.monthly.cardDetails && state.monthly.cardDetails.length) {
      const upfrontDefaultCard = getDefaultCard(state.upFront.cardDetails);
      const monthlySelectedCard = getDefaultCard(state.monthly.cardDetails);

      if (upfrontDefaultCard) {
        state.showUseSameCard = showUseSameCardOption(
          upfrontDefaultCard.id,
          state.monthly.cardDetails
        );

        if (monthlySelectedCard) {
          state.monthly.useSameCard =
            monthlySelectedCard.id === upfrontDefaultCard.id;
        }
      }
    }
  },
  [CONSTANTS.UPDATE_MONTHLY_PAYMENT_SUCCESS]: (state: IPaymentState) => {
    state.loading = false;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IPaymentState,
  AnyAction
  // tslint:disable-next-line:max-file-line-count
>;
