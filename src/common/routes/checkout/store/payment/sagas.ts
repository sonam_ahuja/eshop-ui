import {
  patchPayloadForOtherPayment,
  patchPayloadForPayment,
  postPayloadForPayment
} from '@checkout/CheckoutSteps/Payment/utils/convertData';
import store from '@common/store';
import paymentActions from '@checkout/store/payment/actions';
import {
  fetchPaymentInfoService,
  setPaymentService,
  updatePaymentService
} from '@checkout/store/payment/services';
import {
  IAddNewCard,
  IPaymentInfoPatchReq,
  IPaymentInfoPostReq,
  IPaymentInfoResponse
} from '@checkout/store/payment/types';
import CONSTANTS from '@checkout/store/payment/constants';
import { call, put, takeLatest } from 'redux-saga/effects';
import { PAYMENT_TYPES } from '@src/common/types/modules/common';
import history from '@src/client/history';
import { SagaIterator } from '@authentication/store/types';
import { getConfiguration } from '@src/common/store/common';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import {
  otherPaymentMethodId,
  skipMonthlyStep
} from '@checkout/store/payment/transformer';
import checkoutActions from '@checkout/store/actions';

const CHECKOUT_BILLING = '/checkout/billing';
const CHECKOUT_ORDER_REVIEW = '/checkout/order-review';
const SELECTED_PAYMENT_TYPE = 'paymentTypeSelected';

// tslint:disable-next-line:cognitive-complexity
export function* fetchPaymentInfo(action: {
  type: string;
  payload: boolean;
}): Generator {
  try {
    const isPartyIdExist = store.getState().checkout.personalInfo.formFields
      .addressId;

    if (isPartyIdExist) {
      if (action.payload) {
        yield put(checkoutActions.fetchCheckoutAddressLoading(true));
      }
      const response: IPaymentInfoResponse = yield fetchPaymentInfoService();
      yield put(paymentActions.fetchPaymentInfoLoading());

      const {
        monthly: monthlyConfig,
        upfront: upfrontConfig
      } = getConfiguration().cms_configuration.global.paymentMethods;

      if (response && (response.upfront || response.monthly)) {
        if (response && response.upfront) {
          // tslint:disable:no-string-literal
          response.upfront[SELECTED_PAYMENT_TYPE] = response.upfront
            .paymentTypeSelected
            ? response.upfront.paymentTypeSelected
            : upfrontConfig.defaultPaymentMethod;
        }

        if (response && response.monthly) {
          response.monthly[SELECTED_PAYMENT_TYPE] = response.monthly
            .paymentTypeSelected
            ? response.monthly.paymentTypeSelected
            : monthlyConfig.defaultPaymentMethod;
        }
        yield put(paymentActions.fetchPaymentInfoSuccess(response));
      }

      if (action.payload) {
        yield put(checkoutActions.fetchCheckoutAddressLoading(false));
      }
    }
  } catch (error) {
    yield put(paymentActions.fetchPaymentInfoError(error));
  }
}

export function* setNewCard(action: {
  type: string;
  payload: IPaymentInfoPostReq;
}): Generator {
  try {
    const isMonthlyPrice = store.getState().checkout.checkout.isMonthlyPrice;
    const isUpfront = store.getState().checkout.payment.isUpfront;

    yield put(paymentActions.paymentLoading());
    const response = yield setPaymentService(action.payload);

    if (isUpfront) {
      yield put(paymentActions.proceedToMonthlySuccess(response));
    } else {
      yield put(paymentActions.proceedToBillingSuccess(response));
      if (history) {
        history.push(CHECKOUT_BILLING);
      }
    }

    if (!isMonthlyPrice && history) {
      history.push(CHECKOUT_BILLING);
    }
  } catch (error) {
    yield put(paymentActions.paymentLoadingError(error));
  }
}

// tslint:disable-next-line:cognitive-complexity
export function* updateSelectedCard(action: {
  type: string;
  payload: IPaymentInfoPatchReq;
}): Generator {
  try {
    const isMonthlyPrice = store.getState().checkout.checkout.isMonthlyPrice;
    const isUpfront = store.getState().checkout.payment.isUpfront;
    const paymentMethod = store.getState().checkout.payment;
    const canSkipMonthlyStep = skipMonthlyStep(
      store.getState().checkout.payment.monthly.enabledPaymentMethods
    );
    const canSkipBillingStep = store.getState().configuration.cms_configuration
      .modules.checkout.billingInfo.skipBillingStep;

    yield put(paymentActions.paymentLoading());
    const patchResponse = yield updatePaymentService(action.payload);

    if (patchResponse) {
      yield put(paymentActions.fetchPaymentInfoRequested(undefined));
    }

    if (isUpfront) {
      // condition to skip monthly step start
      if (canSkipMonthlyStep) {
        const monthlyPaymentPayload = patchPayloadForOtherPayment(
          otherPaymentMethodId(
            paymentMethod.monthly,
            PAYMENT_TYPE.PAY_ON_DELIVERY
          ),
          PAYMENT_TYPES.MONTHLY,
          PAYMENT_TYPE.PAY_ON_DELIVERY
        );
        yield updatePaymentService(monthlyPaymentPayload);
        yield put(paymentActions.updateUpfrontPaymentSuccess());
        yield put(paymentActions.updateMonthlyPaymentSuccess());
        if (history) {
          canSkipBillingStep
            ? history.push(CHECKOUT_ORDER_REVIEW)
            : history.push(CHECKOUT_BILLING);
        }
      } else {
        yield put(paymentActions.updateUpfrontPaymentSuccess());
      }

      // condition to skip monthly step end
    } else {
      yield put(paymentActions.updateMonthlyPaymentSuccess());
      if (history) {
        canSkipBillingStep
          ? history.push(CHECKOUT_ORDER_REVIEW)
          : history.push(CHECKOUT_BILLING);
      }
    }

    if (!isMonthlyPrice && history) {
      canSkipBillingStep
        ? history.push(CHECKOUT_ORDER_REVIEW)
        : history.push(CHECKOUT_BILLING);
    }
  } catch (error) {
    yield put(paymentActions.paymentLoadingError(error));
  }
}

export function* checkMonthlyCondition(): Generator {
  const { isMonthlyPrice, isUpfrontPrice } = store.getState().checkout.checkout;
  const canSkipMonthlyStep = skipMonthlyStep(
    store.getState().checkout.payment.monthly.enabledPaymentMethods
  );
  const canSkipBillingStep = store.getState().configuration.cms_configuration
    .modules.checkout.billingInfo.skipBillingStep;

  if (!isMonthlyPrice && !isUpfrontPrice && canSkipMonthlyStep && history) {
    canSkipBillingStep
      ? history.push(CHECKOUT_ORDER_REVIEW)
      : history.push(CHECKOUT_BILLING);
  }

  if (!isMonthlyPrice) {
    yield put(paymentActions.disableProceedToBillingButton(true));
  }
}

export function* postPaymentFunction(
  cardDetail: IAddNewCard,
  paymentType: string,
  optionType: PAYMENT_TYPE
): SagaIterator {
  yield put(
    paymentActions.updateSelectedCard(
      postPayloadForPayment(cardDetail, paymentType, optionType)
    )
  );
}

// tslint:disable-next-line:cognitive-complexity
export function* savePaymentMethod(): Generator {
  const paymentInfo = store.getState().checkout.payment;
  try {
    if (paymentInfo.isUpfront) {
      if (
        paymentInfo.upFront.paymentTypeSelected !==
        PAYMENT_TYPE.CREDIT_DEBIT_CARD
      ) {
        yield put(
          paymentActions.updateSelectedCard(
            patchPayloadForOtherPayment(
              otherPaymentMethodId(
                paymentInfo.upFront,
                paymentInfo.upFront.paymentTypeSelected
              ),
              PAYMENT_TYPES.UPFRONT,
              paymentInfo.upFront.paymentTypeSelected
            )
          )
        );
      } else {
        if (paymentInfo.upFront.addingNewCard) {
          yield call(
            postPaymentFunction,
            paymentInfo.addNewCard,
            PAYMENT_TYPES.UPFRONT,
            paymentInfo.upFront.paymentTypeSelected
          );
        } else {
          yield put(
            paymentActions.updateSelectedCard(
              patchPayloadForPayment(
                paymentInfo.upFront.cardDetails,
                PAYMENT_TYPES.UPFRONT,
                paymentInfo.upFront.paymentTypeSelected
              )
            )
          );
        }
      }
    } else {
      if (
        paymentInfo.monthly.paymentTypeSelected !==
        PAYMENT_TYPE.CREDIT_DEBIT_CARD
      ) {
        yield put(
          paymentActions.updateSelectedCard(
            patchPayloadForOtherPayment(
              otherPaymentMethodId(
                paymentInfo.monthly,
                paymentInfo.monthly.paymentTypeSelected
              ),
              PAYMENT_TYPES.MONTHLY,
              paymentInfo.monthly.paymentTypeSelected
            )
          )
        );
      } else {
        if (paymentInfo.monthly.addingNewCard) {
          yield call(
            postPaymentFunction,
            paymentInfo.addNewCard,
            PAYMENT_TYPES.MONTHLY,
            paymentInfo.monthly.paymentTypeSelected
          );
        } else {
          yield put(
            paymentActions.updateSelectedCard(
              patchPayloadForPayment(
                paymentInfo.monthly.cardDetails,
                PAYMENT_TYPES.MONTHLY,
                paymentInfo.monthly.paymentTypeSelected
              )
            )
          );
        }
      }
    }
  } catch (error) {
    yield put(paymentActions.paymentLoadingError(error));
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.FETCH_PAYMENT_INFO_REQUESTED, fetchPaymentInfo);
  yield takeLatest(CONSTANTS.FETCH_PAYMENT_INFO_SUCCESS, checkMonthlyCondition);
  yield takeLatest(CONSTANTS.SAVE_PAYMENT_METHOD, savePaymentMethod);
  yield takeLatest(CONSTANTS.SET_NEW_CARD, setNewCard);
  yield takeLatest(CONSTANTS.UPDATE_SELECTED_CARD, updateSelectedCard);
}
// tslint:disable:max-file-line-count

export default watcherSaga;
