import apiCaller from '@common/utils/apiCaller';
import {
  IPaymentInfoPatchReq,
  IPaymentInfoPostReq,
  IPaymentInfoResponse
} from '@checkout/store/payment/types';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

export const fetchPaymentInfoService = async (): Promise<
  IPaymentInfoResponse | Error
> => {
  try {
    return await apiCaller.get(apiEndpoints.CHECKOUT.GET_PAYMENT_INFO_DATA.url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const setPaymentService = async (
  payload: IPaymentInfoPostReq
): Promise<{} | Error> => {
  try {
    return await apiCaller.post(
      apiEndpoints.CHECKOUT.POST_PAYMENT_UPFRONT_INFO.url,
      payload
    );
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const updatePaymentService = async (
  payload: IPaymentInfoPatchReq
): Promise<{} | Error> => {
  try {
    return await apiCaller.patch(
      apiEndpoints.CHECKOUT.PATCH_PAYMENT_UPFRONT_INFO.url,
      payload
    );
  } catch (e) {
    logError(e);
    throw e;
  }
};
