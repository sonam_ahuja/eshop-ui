import { IPaymentState } from '@checkout/store/payment/types';
import { PAYMENT_TYPE } from '@checkout/store/enums';

export default (): IPaymentState => ({
  upFront: {
    cardDetails: [],
    saveNewCard: false,
    showEditIcon: false,
    proceedDisabled: true,
    addingNewCard: false,
    id: '',
    paymentTypeSelected: PAYMENT_TYPE.CREDIT_DEBIT_CARD,
    enabledPaymentMethods: {
      tokenizedCard: false,
      payByLink: false,
      payOnDelivery: false,
      bankAccount: false,
      manualPayment: false,
    }

  },
  addNewCard: {
    cardNumber: {
      value: '',
      isValid: false,
      validationMessage: ''
    },
    expiryDate: {
      value: '',
      isValid: false,
      validationMessage: ''
    },
    nameOnCard: {
      value: '',
      isValid: false,
      validationMessage: ''
    },
    securityCode: {
      value: '',
      isValid: false,
      validationMessage: ''
    },
    isDefault: true,
    cardType: 'credit',
    brand: 'dummy',
    isCardSaved: false,
    lastFourDigits: 'xxxxxxxxxxxx3434',
    nonce: ''
  },
  monthly: {
    id: '',
    cardDetails: [],
    saveNewCard: false,
    showEditIcon: false,
    useSameCard: false,
    proceedDisabled: true,
    addingNewCard: false,
    paymentTypeSelected: PAYMENT_TYPE.CREDIT_DEBIT_CARD,
    enabledPaymentMethods: {
      tokenizedCard: false,
      payByLink: false,
      payOnDelivery: false,
      bankAccount: false,
      manualPayment: false,
    }
  },
  isUpfront: true,
  showUseSameCard: false,
  activeCardField: 'number',
  bankDetails: [],
  paymentTypeSelected: PAYMENT_TYPE.CREDIT_DEBIT_CARD,
  creditCheck: {
    loading: false,
    error: null,
    data: null,
    isOpen: false
  },
  dummyCard: {
    cardNumber: '',
    isDefault: false,
    cardType: 'credit',
    expiryDate: '',
    securityCode: '',
    nameOnCard: '',
    id: '0',
    isCardSaved: false,
    lastFourDigits: '',
    brand: 'dummy'
  },
  loading: false
});
