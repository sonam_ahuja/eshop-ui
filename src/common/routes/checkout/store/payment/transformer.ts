import {
  ICardDetails,
  IEnabledPaymentMethods,
  IMonthly,
  IPaymentInfoResponse,
  IUpFront
} from '@checkout/store/payment/types';
import { getDefaultCard } from '@checkout/CheckoutSteps/Payment/utils/convertData';
import { PAYMENT_TYPE } from '@checkout/store/enums';
import {
  IMonthlyPayment,
  IUpfrontPayment
} from '@src/common/store/types/configuration';

export const maskCardDetails = (data: ICardDetails[]): ICardDetails[] => {
  const modifyData: ICardDetails[] = [];
  data.forEach((item: ICardDetails) => {
    if (item.lastFourDigits) {
      item.cardNumber = `****  ****  ****  ${item.lastFourDigits}`;
      modifyData.push(item);
    }
  });

  return modifyData;
};

export function isDefaultCardSelected(cardList: ICardDetails[]): boolean {
  let isDefault = true;

  cardList.forEach((item: ICardDetails) => {
    if (item.isDefault === true) {
      isDefault = false;
    }
  });

  return isDefault;
}

export function showUseSameCardOption(
  selectedId: string,
  cardList: ICardDetails[]
): boolean {
  let showOption = false;

  cardList.forEach((item: ICardDetails) => {
    if (item.id === selectedId) {
      showOption = true;
    }
  });

  return showOption;
}

export function checkUseSameCardValue(payload: IPaymentInfoResponse): boolean {
  let showOption = false;

  if (
    payload.upfront &&
    payload.upfront.tokenizedCard &&
    payload.monthly &&
    payload.monthly.tokenizedCard &&
    payload.upfront.tokenizedCard.cardDetails &&
    payload.upfront.tokenizedCard.cardDetails.length > 0 &&
    payload.monthly.tokenizedCard.cardDetails &&
    payload.monthly.tokenizedCard.cardDetails.length > 0
  ) {
    const upfrontDefaultCard = getDefaultCard(
      payload.upfront.tokenizedCard.cardDetails
    );
    const monthlyDefaultCard = getDefaultCard(
      payload.upfront.tokenizedCard.cardDetails
    );

    if (
      upfrontDefaultCard &&
      monthlyDefaultCard &&
      monthlyDefaultCard.id === upfrontDefaultCard.id
    ) {
      showOption = true;
    }
  }

  return showOption;
}

export function enablePaymentMethod(
  payload: IPaymentInfoResponse,
  type: string
): IEnabledPaymentMethods {
  const enablePaymentOptions: IEnabledPaymentMethods = {
    tokenizedCard: false,
    payByLink: false,
    payOnDelivery: false,
    bankAccount: false,
    manualPayment: false
  };
  const paymentType = payload[type];

  Object.keys(paymentType).forEach(keys => {
    if (enablePaymentOptions[keys] !== undefined) {
      enablePaymentOptions[keys] = true;
    }
  });

  return enablePaymentOptions;
}

export const skipMonthlyStep = (
  enableMethods: IEnabledPaymentMethods
): boolean => {
  let canSkipMonthlyStep = true;

  Object.keys(enableMethods).forEach(keys => {
    if (enableMethods[keys] === PAYMENT_TYPE.PAY_ON_DELIVERY) {
      canSkipMonthlyStep = enableMethods[keys];
    }
  });

  Object.keys(enableMethods).forEach(keys => {
    if (keys !== PAYMENT_TYPE.PAY_ON_DELIVERY && enableMethods[keys]) {
      canSkipMonthlyStep = false;
    }
  });

  return canSkipMonthlyStep;
};

export const cmsPaymentMethodDisabled = (
  paymentMethods: IUpfrontPayment | IMonthlyPayment
): boolean => {
  let canSkipMonthlyStep = true;

  Object.keys(paymentMethods).forEach(key => {
    if (
      key !== 'defaultPaymentMethod' &&
      paymentMethods[key] &&
      paymentMethods[key].show
    ) {
      canSkipMonthlyStep = false;
    }
  });

  return canSkipMonthlyStep;
};

export const otherPaymentMethodId = (
  paymentMethod: IUpFront | IMonthly,
  paymentType: PAYMENT_TYPE
): string => {
  let paymentMethodId = '';

  if (paymentType === PAYMENT_TYPE.PAY_BY_LINK) {
    paymentMethodId = paymentMethod.payByLinkId as string;
  } else if (paymentType === PAYMENT_TYPE.PAY_ON_DELIVERY) {
    paymentMethodId = paymentMethod.payOnDeliveryId as string;
  }

  return paymentMethodId;
};
