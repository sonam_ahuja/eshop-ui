import { CREDIT_CHECK_OPTIONS, PAYMENT_TYPE } from '@checkout/store/enums';

export type paymentTypes =
  | PAYMENT_TYPE.CREDIT_DEBIT_CARD
  | PAYMENT_TYPE.PAY_BY_LINK
  | PAYMENT_TYPE.PAY_ON_DELIVERY
  | PAYMENT_TYPE.BANK_ACCOUNT
  | PAYMENT_TYPE.MANUAL_PAYMENTS;

export type activeFieldTypes =
  | 'number'
  | 'cvv'
  | 'nameOnCard'
  | 'expirationDate'
  | '';

export interface IPaymentGenerateType {
  tokenizedCard?: IPaymentMethodResponseTypes;
  payOnDelivery?: IPaymentMethodResponseTypes;
  payByLink?: IPaymentMethodResponseTypes;
  bankAccount?: IPaymentMethodResponseTypes;
  manualPayments?: IPaymentMethodResponseTypes;
  labelKey?: string;
  link?: string;
}

export interface IPaymentMethodResponseTypes {
  show?: boolean;
  link?: string;
  labelKey?: string;
}

export interface IPaymentState {
  upFront: IUpFront;
  monthly: IMonthly;
  isUpfront: boolean;
  showUseSameCard: boolean;
  bankDetails: {
    name: string;
    id: number;
  }[];
  addNewCard: IAddNewCard;
  paymentTypeSelected: PAYMENT_TYPE;
  creditCheck: ICreditCheckState;
  dummyCard: IDummyCard;
  activeCardField: activeFieldTypes;
  loading: boolean;
}
export interface IDummyCard {
  cardNumber: string;
  isDefault: boolean;
  cardType: string;
  expiryDate: string;
  securityCode: string;
  nameOnCard: string;
  id: string;
  isCardSaved: boolean;
  lastFourDigits: string;
  brand: string;
}

export interface ICardDetail {
  upFront: IUpFront;
  monthly: IMonthly;
}

export interface IAddNewCard {
  cardId?: string;
  cardNumber: INewCardValue;
  isDefault: boolean;
  cardType: string;
  expiryDate: INewCardValue;
  securityCode: INewCardValue;
  nameOnCard: INewCardValue;
  isCardSaved: boolean;
  lastFourDigits: string;
  brand: string;
  nonce: string;
}

export interface INewCardValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
}

export interface IUpFront {
  cardDetails: ICardDetails[];
  saveNewCard: boolean;
  showEditIcon: boolean;
  proceedDisabled: boolean;
  addingNewCard: boolean;
  id: string | null;
  paymentTypeSelected: PAYMENT_TYPE;
  enabledPaymentMethods: IEnabledPaymentMethods;
  payByLinkId?: string;
  payOnDeliveryId?: string;
}

export interface IEnabledPaymentMethods {
  tokenizedCard: boolean;
  payByLink: boolean;
  payOnDelivery: boolean;
  bankAccount: boolean;
  manualPayment: boolean;
}

export interface IMonthly {
  cardDetails: ICardDetails[];
  saveNewCard: boolean;
  showEditIcon: boolean;
  useSameCard: boolean;
  proceedDisabled: boolean;
  addingNewCard: boolean;
  id: string | null;
  paymentTypeSelected: PAYMENT_TYPE;
  enabledPaymentMethods: IEnabledPaymentMethods;
  payByLinkId?: string;
  payOnDeliveryId?: string;
}

export interface ICardDetails {
  cardNumber: string;
  isDefault: boolean;
  cardType: string;
  expiryDate: string;
  securityCode: string;
  nameOnCard: string;
  id: string;
  isCardSaved: boolean;
  lastFourDigits: string;
  brand: string;
}

export interface IOwnerAddress {
  firstName: string;
  lastName: string;
  OIB: number | null;
  street: string;
}

export interface ICreditCheckState {
  loading: boolean;
  error: string | null;
  data: // tslint:disable-next-line:max-union-size
    | CREDIT_CHECK_OPTIONS.ID_NOT_MATCH
    | CREDIT_CHECK_OPTIONS.PAY_FULL_AMOUNT
    | CREDIT_CHECK_OPTIONS.PAY_FULL_OTHER_DEVICE
    | CREDIT_CHECK_OPTIONS.REQUEST_SUPPORT_CALL
    | CREDIT_CHECK_OPTIONS.SELECT_OTHER_DEVICE
    | CREDIT_CHECK_OPTIONS.SUCCESS
    | null;
  isOpen: boolean;
}

export interface IPaymentInfoPatchReq {
  id?: string;
  type?: string;
  nonce?: string;
  saveCard?: boolean;
  selectedPaymentMethod: PAYMENT_TYPE;
}

export interface IPaymentInfoPostReq {
  cardNumber?: string;
  expiryDate?: string;
  nameOnCard?: string;
  cardType?: string;
  securityCode?: string;
  saveCard?: boolean;
  type?: string;
  brand?: string;
}

export interface ICardDetailResponse {
  cardNumber: string;
  isDefault: boolean;
  lastFourDigits: string;
  cardType: string;
  expiryDate: string;
  securityCode: string;
  nameOnCard: string;
  id: string;
  isCardSaved: boolean;
  brand: string;
}

export interface IPaymentInfoResponse {
  upfront: {
    tokenizedCard: { cardDetails: ICardDetailResponse[]; id: string };
    bankAccounts: [];
    payByLink: { id?: string };
    payOnDelivery: { id?: string };
    paymentTypeSelected?: PAYMENT_TYPE;
  };
  monthly: {
    sameAsUpfront: boolean;
    tokenizedCard: { cardDetails: ICardDetailResponse[]; id: string };
    bankAccounts: [];
    payByLink: { id?: string };
    payOnDelivery: { id?: string };
    paymentTypeSelected?: PAYMENT_TYPE;
  };
}
