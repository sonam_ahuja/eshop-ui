import actions from '@checkout/store/billing/actions';
import constants from '@checkout/store/billing/constants';
import {
  // BILLING_ADDRESS_TYPE,
  BILLING_TYPE
} from '@checkout/store/enums';

describe('personalInfo actions', () => {
  it('setBillingType action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.SET_BILLING_TYPE,
      payload: {
        billType: BILLING_TYPE.E_BILL,
        showAppShell: true
      }
    };
    expect(
      actions.setBillingType({
        billType: BILLING_TYPE.E_BILL,
        showAppShell: true
      })
    ).toEqual(expectedAction);
  });

  it('fetchBillingAddressSuccess action creator should return a object with expected value', () => {
    const billingInfo = {
      billingAddress: [
        {
          streetNumber: '',
          city: '',
          postCode: '',
          flatNumber: '',
          address: '',
          deliveryNote: '',
          unit: ''
        }
      ],
      sameAsPersonalInfo: true,
      sameAsShippingInfo: true,
      typeOfBill: BILLING_TYPE.E_BILL
    };
    const expectedAction = {
      type: constants.FETCH_BILLING_ADDRESS_SUCCESS,
      payload: billingInfo
    };

    expect(actions.fetchBillingAddressSuccess(billingInfo)).toEqual(
      expectedAction
    );
  });
});
