import billingState from '@checkout/store/billing/state';
import { billingReducer } from '@checkout/store/billing';

describe('billingReducer', () => {
  it('billingReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(billingReducer(billingState(), definedAction)).toEqual(
      billingState()
    );
  });
});
