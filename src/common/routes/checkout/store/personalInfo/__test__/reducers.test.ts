import personalInfo from '@checkout/store/personalInfo/reducers';
import personalInfoState from '@checkout/store/personalInfo/state';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import actions from '@checkout/store/personalInfo/actions';
import {
  localFormStateWithStatus,
  personalInfoConverterResponse
} from '@mocks/checkout/personalInfo';

// tslint:disable-next-line: no-big-function
describe('personalInfo Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IPersonalInfoState = personalInfo(
    undefined,
    definedAction
  );
  let expectedState: IPersonalInfoState = personalInfoState();

  const initializeValue = () => {
    initialStateValue = personalInfo(undefined, definedAction);
    expectedState = personalInfoState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('UPDATE_PERSONAL_INFO_REQUESTED should mutate parameter in state', () => {
    const expected: IPersonalInfoState = {
      ...expectedState,
      proceedLoader: true
    };
    const action = actions.updatePersonalInfoRequested({});
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('UPDATE_PERSONAL_INFO_REQUESTED should mutate parameter in state', () => {
    const data = {
      email: {
        value: ''
      },
      phoneNumber: {
        value: ''
      },
      mainAddress: [{}],
      isSmsEnabled: true
    };
    const expected: IPersonalInfoState = {
      ...expectedState,
      proceedLoader: true
    };
    const action = actions.setPersonalInfoRequested(data);
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SET_PERSONAL_INFO_LOADER_FALSE should mutate parameter in state', () => {
    const expected: IPersonalInfoState = {
      ...expectedState,
      proceedLoader: false
    };
    const action = actions.setPersonalInfoLoaderFalse();
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SET_SMS_NOTIFICATION should mutate parameter in state with enable sms notification', () => {
    const expected: IPersonalInfoState = {
      ...expectedState,
      isSmsEnabled: true,
      isFormChanged: true
    };
    const action = actions.setSMSNotification();
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('SYNC_DATA_IN_STORE should mutate parameter in state', () => {
    const expected = { ...initialStateValue };
    expected.formFields.address = 'address';
    expected.formFields.streetNumber = 'streetNumber';
    expected.formFields.flatNumber = 'flatNumber';
    expected.formFields.city = 'city';
    expected.formFields.postCode = 'postCode';
    expected.enableProceedButton = true;
    const action = actions.syncDataInStore(localFormStateWithStatus);
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  // @
  // it('UPDATE_PERSONAL_INFO_STORE should mutate parameter in state email', () => {
  //   const expectedData = {
  //     id: '',
  //     isDefault: false,
  //     isSmsEnabled: false,
  //     formFields: {
  //       ...initialStateValue.formFields,
  //       email: null,
  //       phoneNumber: null
  //     }
  //   };
  //   const expected = { ...initialStateValue, ...expectedData };
  //   const action = actions.fetchPersonalInfoSuccess(
  //     personalInfoConverterResponse
  //   );
  //   const state = personalInfo(initialStateValue, action);
  //   expect(state).toEqual(expected);
  // });
  //
  // test('UPDATE_PERSONAL_INFO_STORE should mutate parameter in state', () => {
  //   const data = { ...personalInfoConverterResponse, isDefault: true };
  //   const expectedData = {
  //     id: '',
  //     isDefault: false,
  //     isSmsEnabled: false,
  //     formFields: {
  //       ...initialStateValue.formFields,
  //       email: null,
  //       phoneNumber: null
  //     }
  //   };
  //   const expected = { ...initialStateValue, ...expectedData };
  //   const action = actions.fetchPersonalInfoSuccess(data);
  //   const state = personalInfo(initialStateValue, action);
  //   expect(state).toEqual(expected);
  // });

  test('UPDATE_PERSONAL_INFO_STORE should mutate parameter in state with perosnal state', () => {
    const data = { ...personalInfoConverterResponse, isDefault: true };
    const action = actions.fetchPersonalInfoSuccess(data);
    const state = personalInfo(initialStateValue, action);
    expect(state).toBeDefined();
  });

  test('ENABLE_PROCEED_TO_SHIPPING_BUATTON should mutate parameter in state ', () => {
    const data = {
      enableProceedButton: true
    };
    const expected = { ...initialStateValue, ...data };
    const action = actions.enableProceedToShippingButton(true);
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  test('SET_SMS_NOTIFICATION should mutate parameter in state ', () => {
    const data = {
      enableProceedButton: false,
      isSmsEnabled: true
    };
    const expected = { ...initialStateValue, ...data };
    expected.isFormChanged = true;
    const action = actions.setSMSNotification();
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  test('SHOW_NUMBER_PORT_MODAL should mutate parameter in state ', () => {
    const data = {
      isSmsEnabled: false,
      isNumberPortingModalOpen: true
    };
    const expected = { ...initialStateValue, ...data };
    const action = actions.showNumberPortModal(true);
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  test('SHOW_NUMBER_PORTING_CANCEL_MODAL should mutate parameter in state ', () => {
    const data = {
      isSmsEnabled: false,
      isNumberPortingCancelModal: true
    };
    const expected = { ...initialStateValue, ...data };
    const action = actions.showNumberPortingCancelModal(true);
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  test('SET_NUMBER_PORTING_STATUS should mutate parameter in state ', () => {
    const data = {
      enableProceedButton: false,
      isNumberPorting: true
    };
    const expected = { ...initialStateValue, ...data };
    const action = actions.setNumberPortingStatus(true);
    const state = personalInfo(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  // tslint:disable-next-line:no-commented-code
  // test('SET_PROCEED_TO_SHIPPING_BUTTON should mutate parameter in state ', () => {
  //   const data = {
  //     ...personalInfoConverterResponse,
  //     formFields: {
  //       ...personalInfoConverterResponse.formFields,
  //       streetNumber: '213',
  //       email: true,
  //       phoneNumber: true
  //     },
  //     enableProceedButton: false,
  //     isSmsEnabled: false,
  //     isFormChanged: true
  //   };
  //   const expected = { ...initialStateValue, ...data };
  //   const action = actions.setProceedToShippingButton({
  //     key: 'streetNumber',
  //     value: '213'
  //   });
  //   const state = personalInfo(initialStateValue, action);
  //   expect(state).toEqual(expected);
  // });
});
