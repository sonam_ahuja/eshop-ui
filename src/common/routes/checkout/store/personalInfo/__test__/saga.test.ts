import personalInfoSaga, {
  enableProceedButton,
  setPersonalInfo,
  updatePersonalInfo
} from '@checkout/store/personalInfo/sagas';
import { IPersonalInfoParams } from '@checkout/store/personalInfo/types';
import CONSTANTS from '@checkout/store/personalInfo/constants';

describe('Personal Info Saga', () => {
  const payload: {
    type: string;
    payload: IPersonalInfoParams;
  } = {
    type: CONSTANTS.SET_PERSONAL_INFO_REQUESTED,
    payload: {
      cartId: '12345',
      enableProceedButton: true,
      isFormChanged: true,
      isPersonalInfoPrefilled: true,
      proceedLoader: true,
      isDefault: true,
      isSmsEnabled: true,
      id: '12222',
      formFields: {
        pesel: '2',
        firstName: 'firstName',
        lastName: 'lastName',
        dob: 'dob',
        idNumber: 'idNumber',
        oibNumber: 'oibNumber',
        company: 'company',
        emailId: 'emailId',
        email: 'email',
        phoneNumberId: 'phoneNumberId',
        phoneNumber: 'phoneNumber',
      },
      isNumberPortingCancelModal: false,
      isNumberPortingModalOpen: false,
      isNumberPorting: false,
      smsNotificationName: '',
      smsNotificationDesc: ''
    }
  };
  it('personalInfoSaga saga test', () => {
    const generator = personalInfoSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('setPersonalInfo saga test', () => {
    const generator = setPersonalInfo(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('enableProceedButton saga test', () => {
    const generator = enableProceedButton();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('updatePersonalInfo saga test', () => {
    const newPayload = {
      ...payload,
      type: CONSTANTS.UPDATE_PERSONAL_INFO_REQUESTED
    };
    const generator = updatePersonalInfo(newPayload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  // tslint:disable-next-line:no-identical-functions
  it('enableProceedButton saga test', () => {
    const generator = enableProceedButton();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
