import { checkMonthlyCondition } from '@checkout/store/personalInfo/selector';

describe('personal info selector', () => {
  it('checkMonthlyCondition test', () => {
    expect(checkMonthlyCondition()).toBeFalsy();
  });
});
