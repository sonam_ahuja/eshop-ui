import {
  getAddresses,
  setPersonalInfoService,
  updatePersonalInfoService
} from '@checkout/store/personalInfo/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import apiEndPoints from '@common/constants/apiEndpoints';
import appState from '@store/states/app';
import { logError } from '@src/common/utils';

describe('personal info Service', () => {
  const mock = new MockAdapter(axios);
  it('getAddresses success test', async () => {
    const response = [
      {
        id: '',
        streetNumber: '',
        city: '',
        postCode: '',
        flatNumber: '',
        address: '',
        deliveryNote: ''
      }
    ];
    mock
      .onGet(apiEndPoints.CHECKOUT.GET_ADDRESSES.url('', 10))
      .replyOnce(200, response);
    axios
      .get(apiEndPoints.CHECKOUT.GET_ADDRESSES.url('', 10))
      .then(async () => {
        const result = await getAddresses('address');
        expect(result).toEqual(response);
      })
      .catch(error => logError(error));
  });

  it('updatePersonalInfoService success test', async () => {
    mock
      .onPost(apiEndPoints.CHECKOUT.UPDATE_PERSONAL_INFO_DATA.url)
      .replyOnce(200, {});
    axios
      .post(apiEndPoints.CHECKOUT.UPDATE_PERSONAL_INFO_DATA.url)
      .then(async () => {
        const result = await updatePersonalInfoService(
          appState().checkout.personalInfo
        );
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });

  it('setPersonalInfoService success test', async () => {
    mock
      .onPost(apiEndPoints.CHECKOUT.SET_PERSONAL_INFO_DATA.url)
      .replyOnce(200, {});
    axios
      .post(apiEndPoints.CHECKOUT.SET_PERSONAL_INFO_DATA.url)
      .then(async () => {
        const result = await setPersonalInfoService(
          appState().checkout.personalInfo
        );
        expect(result).toEqual({});
      })
      .catch(error => logError(error));
  });
});
