import { actionCreator } from '@common/utils/actionCreator';
import CONSTANTS from '@checkout/store/personalInfo/constants';
import * as personalInfoTypes from '@common/types/api/checkout/personalInfo';
import {
  IInputKeyValue,
  IPersonalInfoPostResponse
} from '@checkout/store/personalInfo/types';
import { ILocalFormState } from '@checkout/CheckoutSteps/PersonalInfo/formGrid';

export interface ILocalFormStateWithStatus extends ILocalFormState {
  status: boolean;
}

export default {
  fetchPersonalInfoSuccess: actionCreator<personalInfoTypes.GET.IResponse>(
    CONSTANTS.FETCH_PERSONAL_INFO_SUCCESS
  ),
  setPersonalInfoLoaderFalse: actionCreator<undefined>(
    CONSTANTS.SET_PERSONAL_INFO_LOADER_FALSE
  ),
  setPersonalInfoRequested: actionCreator<personalInfoTypes.POST.IRequest>(
    CONSTANTS.SET_PERSONAL_INFO_REQUESTED
  ),
  setPersonalInfoSuccess: actionCreator<IPersonalInfoPostResponse>(
    CONSTANTS.SET_PERSONAL_INFO_SUCCESS
  ),
  setPersonalInfoError: actionCreator<Error>(CONSTANTS.SET_PERSONAL_INFO_ERROR),
  setPersonalInfoLoading: actionCreator<undefined>(
    CONSTANTS.SET_PERSONAL_INFO_LOADING
  ),
  setSelectedEmail: actionCreator<string>(
    CONSTANTS.SET_SELECTED_EMAIL
  ),
  setSMSNotification: actionCreator<void>(CONSTANTS.SET_SMS_NOTIFICATION),
  setProceedToShippingButton: actionCreator<IInputKeyValue>(
    CONSTANTS.SET_PROCEED_TO_SHIPPING_BUTTON
  ),
  updatePersonalInfoRequested: actionCreator<personalInfoTypes.PATCH.IRequest>(
    CONSTANTS.UPDATE_PERSONAL_INFO_REQUESTED
  ),
  updatePersonalInfoSuccess: actionCreator<{}>(
    CONSTANTS.UPDATE_PERSONAL_INFO_SUCCESS
  ),
  updatePersonalInfoError: actionCreator<Error>(
    CONSTANTS.UPDATE_PERSONAL_INFO_ERROR
  ),
  updatePersonalInfoLoading: actionCreator<undefined>(
    CONSTANTS.UPDATE_PERSONAL_INFO_LOADING
  ),
  syncDataInStore: actionCreator<ILocalFormStateWithStatus>(
    CONSTANTS.SYNC_DATA_IN_STORE
  ),
  updateInputField: actionCreator<IInputKeyValue>(
    CONSTANTS.UPDATE_PERSONAL_INFO_FIELD
  ),
  enableProceedToShippingButton: actionCreator<boolean>(
    CONSTANTS.ENABLE_PROCEED_TO_SHIPPING_BUATTON
  ),
  enableProceedButton: actionCreator<undefined>(
    CONSTANTS.ENABLE_PROCEED_BUTTON
  ),
  showNumberPortModal: actionCreator<boolean>(CONSTANTS.SHOW_NUMBER_PORT_MODAL),
  showNumberPortingCancelModal: actionCreator<boolean>(
    CONSTANTS.SHOW_NUMBER_PORTING_CANCEL_MODAL
  ),
  setNumberPortingStatus: actionCreator<boolean>(
    CONSTANTS.SET_NUMBER_PORTING_STATUS
  )
};
