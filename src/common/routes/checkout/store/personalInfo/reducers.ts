import { AnyAction, Reducer } from 'redux';
import {
  IInputKeyValue,
  IPersonalInfoConverterResponse,
  IPersonalInfoPostResponse,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';
import initialState from '@checkout/store/personalInfo/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@checkout/store/personalInfo/constants';
import PERSONAL_INFO_CONSTANTS from '@checkout/CheckoutSteps/PersonalInfo/constants';
import APP_CONSTANTS from '@common/constants/appConstants';

import { ILocalFormStateWithStatus } from './actions';

const { ADDRESS } = PERSONAL_INFO_CONSTANTS;
const reducers = {
  [CONSTANTS.UPDATE_PERSONAL_INFO_REQUESTED]: (state: IPersonalInfoState) => {
    state.proceedLoader = true;
  },
  [CONSTANTS.SET_PERSONAL_INFO_REQUESTED]: (state: IPersonalInfoState) => {
    state.proceedLoader = true;
  },
  [CONSTANTS.SET_PERSONAL_INFO_LOADER_FALSE]: (state: IPersonalInfoState) => {
    state.proceedLoader = false;
  },
  [CONSTANTS.SET_SELECTED_EMAIL]: (
    state: IPersonalInfoState,
    payload: string
  ) => {
    state.formFields.email = payload;
  },
  [CONSTANTS.FETCH_PERSONAL_INFO_SUCCESS]: (
    state: IPersonalInfoState,
    payload: IPersonalInfoConverterResponse
  ) => {
    if (payload.id) {
      state.enableProceedButton = true;
      state.isPersonalInfoPrefilled = true;
      state.formFields = payload.formFields;
      Object.assign(state, payload);
    } else {
      Object.assign(state, payload);
      state.formFields[APP_CONSTANTS.EMAIL] = localStorage.getItem(
        APP_CONSTANTS.EMAIL
      );
      state.formFields[APP_CONSTANTS.PHONE_NUMBER] = localStorage.getItem(
        APP_CONSTANTS.PHONE_NUMBER
      );
    }
  },

  [CONSTANTS.SET_PERSONAL_INFO_SUCCESS]: (
    state: IPersonalInfoState,
    payload: IPersonalInfoPostResponse
  ) => {
    if (payload && payload.mainAddress && payload.mainAddress.length > 0) {
      state.formFields.addressId = payload.mainAddress[0].id;
      state.formFields.phoneNumberId = payload.phoneNumber.id as string;
      state.formFields.emailId = payload.email.id as string;
      state.isPersonalInfoPrefilled = true;
    }
  },

  [CONSTANTS.SET_SMS_NOTIFICATION]: (state: IPersonalInfoState) => {
    state.isSmsEnabled = !state.isSmsEnabled;
    state.isFormChanged = true;
  },
  [CONSTANTS.ENABLE_PROCEED_TO_SHIPPING_BUATTON]: (
    state: IPersonalInfoState,
    payload: boolean
  ) => {
    state.enableProceedButton = payload;
  },
  [CONSTANTS.SET_PROCEED_TO_SHIPPING_BUTTON]: (
    state: IPersonalInfoState,
    payload: IInputKeyValue
  ) => {
    state.isFormChanged = true;
    state.formFields[payload.key] = payload.value;
  },
  [CONSTANTS.UPDATE_PERSONAL_INFO_FIELD]: (
    state: IPersonalInfoState,
    payload: IInputKeyValue
  ) => {
    if (payload && payload.key.toLowerCase() === ADDRESS.toLowerCase()) {
      state[payload.key].value[0].address = payload.value as string;
      state[payload.key].isValid = payload.isValid;
    } else {
      Object.assign(state[payload.key], payload);
    }
    state.isFormChanged = true;
  },
  [CONSTANTS.SYNC_DATA_IN_STORE]: (
    state: IPersonalInfoState,
    payload: ILocalFormStateWithStatus
  ) => {
    state.formFields.address = payload.address && payload.address.value;
    state.formFields.streetNumber =
      payload.streetNumber && payload.streetNumber.value;
    state.formFields.city = payload.city && payload.city.value;
    state.formFields.postCode = payload.postCode && payload.postCode.value;
    state.formFields.flatNumber =
      payload.flatNumber && payload.flatNumber.value;
    state.enableProceedButton = payload.status;
  },
  [CONSTANTS.SHOW_NUMBER_PORT_MODAL]: (
    state: IPersonalInfoState,
    payload: boolean
  ) => {
    state.isNumberPortingModalOpen = payload;
  },
  [CONSTANTS.SHOW_NUMBER_PORTING_CANCEL_MODAL]: (
    state: IPersonalInfoState,
    payload: boolean
  ) => {
    state.isNumberPortingCancelModal = payload;
  },
  [CONSTANTS.SET_NUMBER_PORTING_STATUS]: (
    state: IPersonalInfoState,
    payload: boolean
  ) => {
    state.isNumberPorting = payload;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IPersonalInfoState,
  AnyAction
>;
