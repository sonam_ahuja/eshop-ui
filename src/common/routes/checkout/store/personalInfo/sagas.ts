import {
  setPersonalInfoService,
  updatePersonalInfoService
} from '@checkout/store/personalInfo/services';
import { put, takeLatest } from 'redux-saga/effects';
import personalInfoActions from '@checkout/store/personalInfo/actions';
import checkoutActions from '@checkout/store/actions';
import { IPersonalInfoParams } from '@checkout/store/personalInfo/types';
import CONSTANTS from '@checkout/store/personalInfo/constants';
import store from '@common/store';
import {
  checkReadyToProceedEnablity,
  isFormFieldValidAsPerType
} from '@checkout/CheckoutSteps/PersonalInfo/utils';
import PERSONAL_INFO_CONSTANTS from '@checkout/CheckoutSteps/PersonalInfo/constants';
import { logError } from '@src/common/utils';

export function* setPersonalInfo(action: {
  type: string;
  payload: IPersonalInfoParams;
}): Generator {
  try {
    yield put(checkoutActions.setLoadingState());

    const response = yield setPersonalInfoService(action.payload);

    yield put(personalInfoActions.setPersonalInfoSuccess(response));

    yield put(checkoutActions.fetchCheckoutAddress(false));

  } catch (error) {
    logError(error);
  } finally {
    yield put(personalInfoActions.setPersonalInfoLoaderFalse());
  }
}

export function* updatePersonalInfo(action: {
  type: string;
  payload: IPersonalInfoParams;
}): Generator {
  try {
    yield put(checkoutActions.setLoadingState());

    yield updatePersonalInfoService(action.payload);
    yield put(checkoutActions.fetchCheckoutAddress(false));

  } catch (error) {
    logError(error);
  } finally {
    yield put(personalInfoActions.setPersonalInfoLoaderFalse());
  }
}
export function* enableProceedButton(): Generator {
  try {
    const state = store.getState();
    const { formFields, isSmsEnabled } = state.checkout.personalInfo;
    const cmsConfiguration =
      state.configuration.cms_configuration.modules.checkout.form.fields;
    const isReadyToProceedEnable = checkReadyToProceedEnablity(
      formFields,
      cmsConfiguration
    );

    const isPhoneNumberValid = isFormFieldValidAsPerType(
      cmsConfiguration[PERSONAL_INFO_CONSTANTS.PHONE_NUMBER].validation.value,
      formFields.phoneNumber,
      cmsConfiguration[PERSONAL_INFO_CONSTANTS.PHONE_NUMBER].validation.type
    );

    if (isSmsEnabled) {
      if (!isPhoneNumberValid) {
        yield put(personalInfoActions.enableProceedToShippingButton(false));
      } else {
        yield put(
          personalInfoActions.enableProceedToShippingButton(
            isReadyToProceedEnable
          )
        );
      }
    } else {
      yield put(
        personalInfoActions.enableProceedToShippingButton(
          isReadyToProceedEnable
        )
      );
    }
  } catch (error) {
    logError(error);
    yield put(personalInfoActions.setPersonalInfoLoaderFalse());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.SET_PERSONAL_INFO_REQUESTED, setPersonalInfo);
  yield takeLatest(
    CONSTANTS.UPDATE_PERSONAL_INFO_REQUESTED,
    updatePersonalInfo
  );
  yield takeLatest(CONSTANTS.SET_SMS_NOTIFICATION, enableProceedButton);
  yield takeLatest(CONSTANTS.ENABLE_PROCEED_BUTTON, enableProceedButton);
  yield takeLatest(
    CONSTANTS.SET_PROCEED_TO_SHIPPING_BUTTON,
    enableProceedButton
  );
  yield takeLatest(CONSTANTS.SYNC_DATA_IN_STORE, enableProceedButton);
}
export default watcherSaga;
