import store from '@common/store';

export const checkMonthlyCondition = (): boolean => {
  return store.getState().checkout.checkout.isMonthlyPrice;
};
