import apiCaller from '@common/utils/apiCaller';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import apiEndPoints from '@common/constants/apiEndpoints';
import { IListItem } from 'dt-components/lib/es/components/molecules/autocomplete/list';
import { ISearchAddressesResponse } from '@checkout/store/shipping/types';
import { logError } from '@src/common/utils';
import store from '@src/common/store';
import { IGeoPrediction } from '@src/common/types/common/googleMap';

export const setPersonalInfoService = async (
  payload: IPersonalInfoState
): Promise<{} | Error> => {
  try {
    return await apiCaller[
      apiEndPoints.CHECKOUT.SET_PERSONAL_INFO_DATA.method.toLowerCase()
    ](`${apiEndPoints.CHECKOUT.SET_PERSONAL_INFO_DATA.url}`, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const updatePersonalInfoService = async (
  payload: IPersonalInfoState
): Promise<{} | Error> => {
  try {
    return await apiCaller[
      apiEndPoints.CHECKOUT.UPDATE_PERSONAL_INFO_DATA.method.toLowerCase()
    ](`${apiEndPoints.CHECKOUT.UPDATE_PERSONAL_INFO_DATA.url}`, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const getAddresses = async (
  searchTerm: string
): Promise<IListItem[]> => {
  try {
    const searchSize = store.getState().configuration.cms_configuration.modules
      .checkout.searchSize;
    const {
      enableGoogleAddressSuggestion
    } = store.getState().configuration.cms_configuration.global;

    if (!enableGoogleAddressSuggestion) {
      const response: ISearchAddressesResponse[] = await apiCaller[
        apiEndPoints.CHECKOUT.GET_ADDRESSES.method.toLowerCase()
      ](`${apiEndPoints.CHECKOUT.GET_ADDRESSES.url(searchTerm, searchSize)}`);

      if (response && Array.isArray(response) && response.length) {
        return response.map(address => {
          return {
            title: address.autoSuggestAddress,
            id: address.id,
            address: address.address,
            ...address
          };
        });
      }
    } else {
      return getAddressViaGoogle(searchTerm);
    }

    return [];
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const getAddressViaGoogle = async (
  searchTerm: string
): Promise<IListItem[]> => {
  return new Promise((resolve, reject) => {
    const serviceApi = new google.maps.places.AutocompleteService();
    const {
      geoCountryCode
    } = store.getState().configuration.cms_configuration.global;

    const listArray: IListItem[] = [];
    serviceApi.getPlacePredictions(
      {
        input: searchTerm,
        componentRestrictions: {
          country: geoCountryCode
        }
      },
      (predictions: IGeoPrediction[]) => {
        if (predictions && predictions.length) {
          predictions.forEach((location: IGeoPrediction) => {
            listArray.push({
              title: location.description ? location.description : '',
              id: location.place_id
            });
            resolve(listArray);
          });
        } else {
          reject([]);
        }
      }
    );
  });
};
