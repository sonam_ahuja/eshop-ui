import { IPersonalInfoState } from '@checkout/store/personalInfo/types';

export default (): IPersonalInfoState => ({
  id: '',
  formFields: {
    pesel: '',
    firstName: '',
    lastName: '',
    dob: '',
    streetNumber: '',
    city: '',
    flatNumber: '',
    postCode: '',
    idNumber: '',
    oibNumber: '',
    company: '',
    email: '',
    address: '',
    phoneNumber: '',
    emailId: '',
    addressId: '',
    phoneNumberId: ''
  },
  isDefault: false,
  isSmsEnabled: false,
  enableProceedButton: false,
  isFormChanged: false,
  isPersonalInfoPrefilled: false,
  proceedLoader: false,

  isNumberPortingCancelModal: false,
  isNumberPortingModalOpen: false,
  isNumberPorting: false,
  smsNotificationName: '',
  smsNotificationDesc: ''
});
