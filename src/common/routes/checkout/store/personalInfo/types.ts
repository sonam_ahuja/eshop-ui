import { ICheckout } from '@src/common/store/types/configuration';
import { IProps as IFormBodyProps } from '@checkout/CheckoutSteps/PersonalInfo/PersonalInfoContent';

export interface IPersonalInfoValidation {
  [key: string]: {
    valid: boolean;
    message: string;
  };
}
export interface IAddressResponse {
  id?: string;
  streetNumber?: string;
  address?: string;
  flatNumber?: string;
  city?: string;
  postCode?: string;
  deliveryNote?: string;
}

export interface IPersonalInfoConverterResponse {
  id: string;
  formFields: IPersonalInfoFormFields;
  isDefault: boolean;
  isSmsEnabled: boolean;
  smsNotificationName: string;
  smsNotificationDesc: string;
}

export interface IPersonalInfoState extends IPersonalInfoConverterResponse {
  enableProceedButton: boolean;
  isFormChanged: boolean;
  isPersonalInfoPrefilled: boolean;
  proceedLoader: boolean;
  isNumberPortingCancelModal: boolean;
  isNumberPortingModalOpen: boolean;
  isNumberPorting: boolean;
}
export interface IPhoneNumber {
  id: string;
  displayId: string;
  selected?: boolean;
}
export interface IPersonalInfoFormFields {
  firstName: string;
  lastName: string;
  dob: string;
  pesel: string;
  streetNumber?: string;
  city?: string;
  flatNumber?: string;
  postCode?: string;
  idNumber: string;
  oibNumber: string;
  company: string;
  addressId?: string;
  address?: string;
  emailId: string;
  email: string;
  deliveryNote?: string;
  phoneNumberId: string;
  phoneNumber: string;
}

export interface IPersonalInfoParams extends IPersonalInfoState {
  cartId: string;
}

export interface IInputKeyValue {
  key: string;
  value: string | IAddress[];
  isValid?: boolean;
  validationMessage?: string;
  status?: boolean;
}

export interface IPersonalInfoValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
  id?: string;
}

export type inputType =
  | 'number'
  | 'text'
  | 'email'
  | 'password'
  | 'url'
  | 'search';

export interface IPersonalInfoPatchRequest {
  email?: IRequestEmail;
  phoneNumber?: IRequestPhoneNumber;
  mainAddress?: IAddress[];
  isSmsEnabled?: boolean;
  isChanged?: boolean;
  company?: string;
  firstName?: string;
  lastName?: string;
  idNumber?: string;
  oibNumber?: string;
  dob?: string;
  pesel?: string;
}

export interface IPersonalInfoPostResponse {
  email: IRequestEmail;
  phoneNumber: IRequestPhoneNumber;
  mainAddress: IAddress[];
  oibNumber?: string;
  lastName?: string;
  firstName?: string;
  idNumber?: string;
  id?: string;
  dob?: string;
  company?: string;
  isSmsEnabled?: boolean;
  pesel?: string;
}

export interface IPersonalInfoPostRequest {
  email: {
    value: string;
  };
  phoneNumber: {
    value: string;
  };
  mainAddress: IAddress[];
  isSmsEnabled: boolean;
  company?: string;
  firstName?: string;
  lastName?: string;
  idNumber?: string;
  oibNumber?: string;
  dob?: string;
  pesel?: string;
}
export interface IAddress {
  id?: string;
  streetNumber?: string;
  address?: string;
  flatNumber?: string;
  city?: string;
  deliveryNote?: string;
  postCode?: string;
  notes?: string;
  isDefault?: boolean;
}

export interface IStateAddress {
  value: IAddress[];
  isValid: boolean;
  validationMessage: string;
  id?: string;
}

export interface IRequestEmail {
  id?: string;
  value: string;
}
export interface IRequestPhoneNumber {
  id?: string;
  value: string;
}

export interface IValidateInput {
  key: string;
  inputValue: string;
  checkoutConfiguration: ICheckout;
  states?: IPersonalInfoState;
  props?: IFormBodyProps;
}
