import { AnyAction, Reducer } from 'redux';
import withProduce from '@utils/withProduce';
import {
  ICheckoutState,
  ICreditQualifyResponse,
  ICreditScoreResponse,
  ITermsAndConditionResponse
} from '@checkout/store/types';
import initialState from '@checkout/store/state';
import CONSTANTS from '@checkout/store/constants';
import { ICartSummary } from '@basket/store/types';
import { ITEM_PRICE_TYPE } from '@basket/store/enums';

const reducers = {
  [CONSTANTS.FETCH_CHECKOUT_ADDRESS_LOADING]: (
    state: ICheckoutState,
    payload: boolean
  ) => {
    state.loading = true;
    state.mainAppShell = payload;
    state.sideAppShell = payload;
    state.isAddressFetched = false;
  },
  [CONSTANTS.FETCH_CHECKOUT_ADDRESS_SUCCESS]: (state: ICheckoutState) => {
    state.loading = false;
    state.mainAppShell = false;
    state.sideAppShell = false;
    state.isAddressFetched = true;
  },
  // tslint:disable-next-line: no-identical-functions
  [CONSTANTS.FETCH_CHECKOUT_ADDRESS_ERROR]: (state: ICheckoutState) => {
    state.loading = false;
    state.mainAppShell = false;
    state.sideAppShell = false;
    state.isAddressFetched = true;
  },
  [CONSTANTS.EVALUATE_CART_SUMMARY_PRICE]: (
    state: ICheckoutState,
    payload: ICartSummary[]
  ) => {
    if (payload) {
      payload.forEach(summaryType => {
        if (summaryType.priceType === ITEM_PRICE_TYPE.UPFRONT) {
          state.isUpfrontPrice = !!summaryType.totalPrice;
        }
        if (summaryType.priceType === ITEM_PRICE_TYPE.RECURRING_FEE) {
          state.isMonthlyPrice = !!summaryType.totalPrice;
        }
      });
    }
  },

  [CONSTANTS.CREDIT_QUALIFICATION_SUCCESS]: (
    state: ICheckoutState,
    payload: ICreditQualifyResponse
  ) => {
    if (!payload.status) {
      state.creditCheckLoading = false;
    }
  },
  [CONSTANTS.IS_CREDIT_CHECK_REQUIRED_SUCCESS]: (
    state: ICheckoutState,
    payload: ICreditQualifyResponse
  ) => {
    state.isCreditCheckRequired = payload.status;
  },

  [CONSTANTS.FETCH_CREDIT_SCORE_LOADING]: (state: ICheckoutState) => {
    state.creditCheckLoading = true;
  },
  [CONSTANTS.CREDIT_SCORE_SUCCESS]: (
    state: ICheckoutState,
    payload: ICreditScoreResponse
  ) => {
    state.creditCheckLoading = false;
    state.openCreditModal = true;
    state.creditScoreResponse = payload;
    state.activeCreditStep = payload.code;
  },
  [CONSTANTS.CREDIT_SCORE_ERROR]: (state: ICheckoutState) => {
    state.creditCheckLoading = false;
    state.openCreditModal = false;
  },
  [CONSTANTS.OPEN_TNC_MODAL]: (state: ICheckoutState, payload: boolean) => {
    state.openTnCModal = payload;
  },
  [CONSTANTS.TERMS_AND_CONDITION_SUCCESS]: (
    state: ICheckoutState,
    payload: ITermsAndConditionResponse
  ) => {
    state.termsAndConditionData = payload;
  }
  // [CONSTANTS.CHECKOUT_MODULE_LOADING]: (state: ICheckoutState) => {
  //   state.loading = true;
  //   state.openCreditModal = false;
  // },
};

export default withProduce(initialState, reducers) as Reducer<
  ICheckoutState,
  AnyAction
>;
