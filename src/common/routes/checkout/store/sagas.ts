import {
  BILLING_ADDRESS_TYPE,
  CHECKOUT_SUB_ROUTE_TYPE,
  CREDIT_SCORE_CODE,
  PLACE_ORDER_CONSTANT
} from '@checkout/store/enums';
import { put, takeLatest } from 'redux-saga/effects';
import updateReadOnlyFieldConfig from '@checkout/utils/updateReadOnlyFieldConfig';
import billingInfoActions from '@checkout/store/billing/actions';
import actions from '@checkout/store/actions';
import personalInfoActions from '@checkout/store/personalInfo/actions';
import creditCheckActions from '@checkout/store/creditCheck/actions';
import shippingInfoActions from '@checkout/store/shipping/actions';
import basketActions from '@basket/store/actions';
import orderReviewActions from '@checkout/store/orderReview/actions';
import CONSTANTS from '@checkout/store/constants';
import APP_CONSTANTS from '@common/constants/appConstants';
import {
  fetchCheckoutAddressService,
  fetchCreditCheckQualification,
  fetchCreditScore,
  fetchDeliveryInfoService,
  fetchNearByStoresService,
  fetchTnC
} from '@checkout/store/services';
import { convertPersonalInfo } from '@checkout/utils/checkoutConverter';
import { IAddressResponse } from '@checkout/store/types';
import paymentActions from '@checkout/store/payment/actions';
import store from '@src/common/store';
import numberPortingActions from '@checkout/store/numberPorting/actions';
import history from '@src/client/history';
import { getActiveCheckoutStep } from '@checkout/CheckoutSteps/utils';
import { isBrowser, logError } from '@src/common/utils';
import { getValueFromLocalStorage } from '@src/common/utils/localStorage';
import routeConstants from '@common/constants/routes';
import { fetchBasketService } from '@basket/store/services';
import mainActions from '@common/store/actions/configuration';
import { checkMonthlyCondition } from '@checkout/store/personalInfo/selector';

const CHECKOUT_SHIPPING = '/checkout/shipping';
const CHECKOUT_PERSONAL_INFO = '/checkout/personal-info';

// tslint:disable-next-line:cognitive-complexity
export function* fetchCheckoutAddress(action: {
  type: string;
  payload: boolean;
}): Generator {
  try {
    yield put(actions.fetchCheckoutAddressLoading(action.payload));

    const {
      mnp,
      mns
    } = store.getState().configuration.cms_configuration.modules.checkout;
    const isMnsEnabled = mns.show || mnp.show;
    try {
      // @TODO: HERE_CHECKOUT_ADDRESSED
      if (
        history &&
        getActiveCheckoutStep(history.location) ===
          CHECKOUT_SUB_ROUTE_TYPE.PERSONAL_INFO &&
        isMnsEnabled
      ) {
        yield put(numberPortingActions.requestReourcePool('MSISDN'));
      }
    } catch (error) {
      yield put(numberPortingActions.requestReourcePoolError(error));
    }
    const response: IAddressResponse = yield fetchCheckoutAddressService();

    if (response) {
      const { shippingInfo, billingInfo } = response;

      const updatedConfig = updateReadOnlyFieldConfig(response);
      yield put(mainActions.setCMSConfiguration(updatedConfig));

      const personalInfo = convertPersonalInfo(response.personalInfo);

      yield put(personalInfoActions.fetchPersonalInfoSuccess(personalInfo));

      if (
        shippingInfo &&
        shippingInfo.shippingAddress &&
        shippingInfo.shippingAddress[0]
      ) {
        yield put(shippingInfoActions.fetchShippingInfoSuccess(shippingInfo));

        if (
          shippingInfo.deliveryMethod &&
          shippingInfo.deliveryMethod.shippingType
        ) {
          yield put(
            shippingInfoActions.setSavedShippingType(
              shippingInfo.deliveryMethod.shippingType
            )
          );
        }
      } else {
        if (
          personalInfo &&
          personalInfo.formFields &&
          personalInfo.formFields.addressId
        ) {
          yield put(shippingInfoActions.sameAsPersonalSwitchTrue(true));
        }
      }

      if (billingInfo) {
        yield put(billingInfoActions.fetchBillingAddressSuccess(billingInfo));
      } else {
        if (
          personalInfo &&
          personalInfo.formFields &&
          personalInfo.formFields.addressId
        ) {
          yield put(
            billingInfoActions.setBillingAddressType(
              BILLING_ADDRESS_TYPE.PERSONAL_INFO
            )
          );
        }
      }
      yield put(actions.fetchCheckoutAddressSuccess());

      // check for route reload refresh start)
      if (history && personalInfo.id) {
        const CHECKOUT_ACTIVE_STEP = getActiveCheckoutStep(history.location);

        switch (CHECKOUT_ACTIVE_STEP) {
          case CHECKOUT_SUB_ROUTE_TYPE.SHIPPING:
            yield put(shippingInfoActions.fetchShippingFee(true));
            break;
          case CHECKOUT_SUB_ROUTE_TYPE.PAYMENT:
            yield put(paymentActions.fetchPaymentInfoRequested(true));
            break;
          case CHECKOUT_SUB_ROUTE_TYPE.BILLING:
            yield put(paymentActions.fetchPaymentInfoRequested(true));
            break;
          case CHECKOUT_SUB_ROUTE_TYPE.ORDER_REVIEW:
            yield put(paymentActions.fetchPaymentInfoRequested(true));
            yield put(orderReviewActions.checkForActiveRoute());
            break;
          default:
            break;
        }
      }
      // check for route reload end

      if (!action.payload) {
        const {
          creditCheckRequired
        } = store.getState().configuration.cms_configuration.global;

        yield put(personalInfoActions.setPersonalInfoLoaderFalse());

        if (checkMonthlyCondition() && creditCheckRequired) {
          yield put(actions.fetchCreditCheckQualification());
        } else {
          if (history) {
            history.push(CHECKOUT_SHIPPING);
          }
        }
      } else {
        if (!personalInfo.id && history) {
          history.push(CHECKOUT_PERSONAL_INFO);
        }
      }
    }
  } catch (error) {
    yield put(actions.fetchCheckoutAddressError());
  }
}

export function* fetchDeliveryInfo(): Generator {
  try {
    yield put(actions.fetchDeliveryInfoLoading());
    yield fetchDeliveryInfoService();

    yield put(actions.fetchDeliveryInfoSuccess());
  } catch (e) {
    yield put(actions.fetchDeliveryInfoError(e));
  }
}

export function* fetchNearByStores(): Generator {
  try {
    yield put(actions.fetchNearByStoresLoading());
    yield fetchNearByStoresService();

    yield put(actions.fetchNearByStoresSuccess());
  } catch (e) {
    yield put(actions.fetchNearByStoresError(e));
  }
}

export function* creditCheckQualification(): Generator {
  try {
    yield put(actions.setLoadingState());

    const isCreditCheck = store.getState().checkout.checkout
      .isCreditCheckRequired;

    const response = {
      status: isCreditCheck
    };
    yield put(actions.creditQualificationSuccess(response));
    if (isCreditCheck) {
      yield put(actions.fetchCreditCheckScore());
    } else {
      yield put(personalInfoActions.setPersonalInfoLoaderFalse());

      if (history) {
        history.push('/checkout/shipping');
      }
    }
  } catch (e) {
    yield put(personalInfoActions.setPersonalInfoLoaderFalse());
    yield put(actions.creditQualificationError());
  }
}

export function* isCreditCheckRequired(action: {
  type: string;
  payload: string;
}): Generator {
  try {
    const {
      creditCheckRequired
    } = store.getState().configuration.cms_configuration.global;

    if (creditCheckRequired) {
      const response = yield fetchCreditCheckQualification();

      yield put(actions.isCreditCheckRequiredSuccess(response));
      if (
        action.payload &&
        action.payload === PLACE_ORDER_CONSTANT &&
        response.status
      ) {
        yield put(actions.fetchCreditCheckScore(action.payload));
      } else if (
        action.payload &&
        action.payload === PLACE_ORDER_CONSTANT &&
        !response.status
      ) {
        yield put(orderReviewActions.performPlaceOrder());
      }
    }
  } catch (e) {
    yield put(orderReviewActions.placeOrderLoading(false));
    yield put(actions.creditQualificationError());
  }
}

export function* checkCreditScore(action: {
  type: string;
  payload: string;
}): Generator {
  try {
    if (!(action.payload && action.payload === PLACE_ORDER_CONSTANT)) {
      yield put(actions.fetchCreditLoading());
    }
    const response = yield fetchCreditScore();
    if (
      response &&
      response.code &&
      response.code === CREDIT_SCORE_CODE.CREDIT_SUCCESS &&
      action.payload &&
      action.payload === PLACE_ORDER_CONSTANT
    ) {
      yield put(orderReviewActions.performPlaceOrder());
    } else if (
      response &&
      response.code &&
      action.payload &&
      action.payload === PLACE_ORDER_CONSTANT
    ) {
      yield put(orderReviewActions.placeOrderLoading(false));
      yield put(actions.creditScoreSuccess(response));
      yield put(creditCheckActions.creditScoreSuccess());
    } else {
      yield put(actions.creditScoreSuccess(response));
      yield put(personalInfoActions.setPersonalInfoLoaderFalse());
      yield put(creditCheckActions.creditScoreSuccess());
    }
  } catch (e) {
    yield put(orderReviewActions.placeOrderLoading(false));
    yield put(personalInfoActions.setPersonalInfoLoaderFalse());
    yield put(actions.creditScoreError());
  }
}

export function* evaluateCartSummary(): Generator {
  try {
    const checkoutState = store.getState().checkout.checkout;

    yield put(paymentActions.checkForIsUpfront(checkoutState));
  } catch (e) {
    yield put(personalInfoActions.setPersonalInfoLoaderFalse());
  }
}

export function* showTermsAndConditions(): Generator {
  const {
    isTermsAndConditionCMSDriven
  } = store.getState().configuration.cms_configuration.modules.checkout.orderReview;
  try {
    if (!isTermsAndConditionCMSDriven) {
      const response = yield fetchTnC();
      yield put(actions.fetchTnCSuccess(response));
    }
    yield put(actions.openTnCModal(true));
  } catch (e) {
    yield put(actions.openTnCModal(false));
  }
}

export function* fetchBasketAndContact(): Generator {
  try {
    yield put(actions.fetchCheckoutAddressLoading(true));

    const payload = {
      isCalledFromCheckout: true,
      showFullPageError: false
    };
    const response = yield fetchBasketService({
      showFullPageError: payload.showFullPageError
    });

    const isUserLoggedIn = getValueFromLocalStorage(
      APP_CONSTANTS.IS_USER_LOGGED_IN
    );

    if (response.cartItems) {
      const { cartItems, cartSummary, totalItems } = response;

      yield put(
        basketActions.setBasketData({
          cartItems,
          cartSummary,
          totalItems
        })
      );

      if (payload.isCalledFromCheckout && isBrowser && isUserLoggedIn) {
        yield put(actions.isCreditCheckRequired());
      }
      yield put(actions.evaluateCartSummaryPrice(cartSummary));
      yield put(actions.fetchCheckoutAddress(true));
    } else {
      yield put(actions.fetchCheckoutAddressLoading(false));
      if (payload.isCalledFromCheckout && history) {
        history.push(routeConstants.BASKET);
      }
      yield put(basketActions.resetBasket());
    }

    yield put(basketActions.resetUndoAbleItems());
    yield put(basketActions.fetchBasketSuccess());
  } catch (error) {
    logError(error);
  }
}
function* watcherSaga(): Generator {
  yield takeLatest(
    CONSTANTS.FETCH_CHECKOUT_ADDRESS_REQUESTED,
    fetchCheckoutAddress
  );

  yield takeLatest(CONSTANTS.FETCH_DELIVERY_INFO_REQUESTED, fetchDeliveryInfo);

  yield takeLatest(CONSTANTS.FETCH_NEARBY_STORES_REQUESTED, fetchNearByStores);
  yield takeLatest(
    CONSTANTS.CREDIT_CHECK_QUALIFY_REQUESTED,
    creditCheckQualification
  );
  yield takeLatest(CONSTANTS.CREDIT_CHECK_SCORE_REQUESTED, checkCreditScore);
  yield takeLatest(CONSTANTS.EVALUATE_CART_SUMMARY_PRICE, evaluateCartSummary);
  yield takeLatest(CONSTANTS.IS_CREDIT_CHECK_REQUIRED, isCreditCheckRequired);
  yield takeLatest(CONSTANTS.SHOW_TERMS_CONDTIONS, showTermsAndConditions);
  yield takeLatest(CONSTANTS.FETCH_BASKET_AND_CONTACTS, fetchBasketAndContact);
}
// tslint:disable-next-line:max-file-line-count
export default watcherSaga;
