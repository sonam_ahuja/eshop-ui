import { CREDIT_CHECK_MODALS, CREDIT_SCORE_CODE } from '@checkout/store/enums';
import {
  IPersonalInfoPatchRequest,
  IPersonalInfoPostRequest
} from '@checkout/store/personalInfo/types';
import { IProps as IFormFooterProps } from '@checkout/CheckoutSteps/PersonalInfo/PersonalInfoActions';

export const creditCheckFlow = {
  [CREDIT_SCORE_CODE.CREDIT_SUCCESS]: {
    modal: CREDIT_CHECK_MODALS.CREDIT_SUCCESS,
    redirectTo: null
  },
  [CREDIT_SCORE_CODE.REQUEST_SUPPORT_CALL]: {
    modal: CREDIT_CHECK_MODALS.REQUEST_SUPPORT_CALL,
    redirectTo: null
  },
  [CREDIT_SCORE_CODE.SELECT_OTHER_DEVICE]: {
    modal: CREDIT_CHECK_MODALS.SELECT_OTHER_DEVICE,
    redirectTo: null
  },
  [CREDIT_SCORE_CODE.PAY_FULL]: {
    modal: CREDIT_CHECK_MODALS.PAY_FULL,
    redirectTo: null
  },
  [CREDIT_SCORE_CODE.PAY_FULL_PLUS_OTHER_DEVICE]: {
    modal: CREDIT_CHECK_MODALS.PAY_FULL_PLUS_OTHER_DEVICE,
    redirectTo: null
  }
};

export function composeUpdateApiBody(
  props: IFormFooterProps
): IPersonalInfoPatchRequest {
  const {
    email,
    phoneNumber,
    address,
    streetNumber,
    flatNumber,
    emailId,
    addressId,
    postCode,
    city,
    phoneNumberId,
    company,
    firstName,
    lastName,
    idNumber,
    oibNumber,
    dob,
    pesel
  } = props.personalInfo.formFields;
  const { isSmsEnabled, isFormChanged } = props.personalInfo;

  return {
    firstName,
    lastName,
    idNumber,
    oibNumber,
    pesel,
    dob,
    email: {
      id: emailId,
      value: email
    },
    phoneNumber: {
      id: phoneNumberId,
      value: phoneNumber
    },
    company,
    mainAddress: [
      {
        id: addressId,
        address,
        streetNumber,
        flatNumber,
        postCode,
        city,
        isDefault: true
      }
    ],
    isSmsEnabled,
    isChanged: isFormChanged
  };
}

export function composeSetApiBody(
  props: IFormFooterProps
): IPersonalInfoPostRequest {
  const {
    firstName,
    lastName,
    email,
    phoneNumber,
    address,
    idNumber,
    oibNumber,
    streetNumber,
    flatNumber,
    postCode,
    city,
    dob,
    company,
    pesel
  } = props.personalInfo.formFields;
  const { isSmsEnabled } = props.personalInfo;

  const data = {
    firstName,
    lastName,
    email: {
      value: email
    },
    phoneNumber: {
      value: phoneNumber
    },
    mainAddress: [
      {
        address,
        streetNumber,
        flatNumber,
        postCode,
        city,
        isDefault: true
      }
    ],
    company,
    isSmsEnabled,
    idNumber,
    oibNumber,
    dob,
    pesel
  };

  return data as IPersonalInfoPostRequest;
}
