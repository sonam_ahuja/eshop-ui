import apiCaller from '@common/utils/apiCaller';
import { IAddressResponse, IResponse } from '@checkout/store/types';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

export const fetchCheckoutAddressService = async (): Promise<
  IAddressResponse | Error
> => {
  try {
    return await apiCaller.get(apiEndpoints.CHECKOUT.GET_PERSONA_INFO_DATA.url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const fetchDeliveryInfoService = async (): Promise<
  IResponse | Error
> => {
  const url = 'deliveryInfo';
  try {
    return await apiCaller.get(url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

// tslint:disable-next-line:no-identical-functions
export const fetchNearByStoresService = async (): Promise<
  IResponse | Error
> => {
  try {
    return await apiCaller.get('nearbyStores');
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const fetchCreditCheckQualification = async (): Promise<{} | Error> => {
  try {
    return await apiCaller.get(apiEndpoints.CHECKOUT.GET_CREDIT_QUALIFY.url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const fetchCreditScore = async (): Promise<{} | Error> => {
  try {
    return await apiCaller.get(apiEndpoints.CHECKOUT.GET_CREDIT_SCORE.url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const fetchTnC = async (): Promise<{} | Error> => {
  try {
    return await apiCaller.get(
      apiEndpoints.CHECKOUT.GET_TERMS_AND_CONDITION.url
    );
  } catch (e) {
    logError(e);
    throw e;
  }
};
