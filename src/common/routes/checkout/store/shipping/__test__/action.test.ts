import actions from '@checkout/store/shipping/actions';
import constants from '@checkout/store/shipping/constants';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import {
  IShippingAddressAndNote,
  shippingType
} from '@checkout/store/shipping/types';

describe('shipping actions', () => {
  // tslint:disable-next-line:no-any
  it('setShippingMode action creator should return dispatchable function for corresponding', () => {
    const expectedAction = {
      type: constants.SET_SHIPPING_MODE,
      payload: SHIPPING_TYPE.EXACT_DAY
    };
    expect(
      actions.setShippingMode(SHIPPING_TYPE.EXACT_DAY as shippingType)
    ).toEqual(expectedAction);
  });

  it('updateNewAddressShipping action creator should return dispatchable function for corresponding', () => {
    const setNewAddressParams: IShippingAddressAndNote = {
      streetNumber: '1233',
      address: 'address',
      deliveryNote: 'notes'
    };
    const expectedAction = {
      type: constants.UPDATE_NEW_SHIPPING_ADDRESS,
      payload: setNewAddressParams
    };
    expect(actions.updateNewAddressShipping(setNewAddressParams)).toEqual(
      expectedAction
    );
  });

  it('setShippingAddressError action creator should return dispatchable function for corresponding', () => {
    const errorPayload: Error = {
      stack: 'stack',
      message: 'message',
      name: 'error'
    };
    const expectedAction = {
      type: constants.SET_SHIPPING_ADDRESS_ERROR,
      payload: errorPayload
    };
    expect(actions.setShippingAddressError(errorPayload)).toEqual(
      expectedAction
    );
  });

  // tslint:disable-next-line:no-identical-functions
  it('setShippingAddressError action creator should return dispatchable function for corresponding', () => {
    const errorPayload: Error = {
      stack: 'stack',
      message: 'message',
      name: 'error'
    };
    const expectedAction = {
      type: constants.SET_SHIPPING_ADDRESS_ERROR,
      payload: errorPayload
    };
    expect(actions.setShippingAddressError(errorPayload)).toEqual(
      expectedAction
    );
  });

  it('setShippingAddressLoading action creator should return dispatchable function for corresponding', () => {
    const expectedAction = {
      type: constants.SET_SHIPPING_ADDRESS_LOADING,
      payload: undefined
    };
    expect(actions.setShippingAddressLoading()).toEqual(expectedAction);
  });
  it('updateShippingAddressError action creator should return dispatchable function for corresponding', () => {
    const errorPayload: Error = {
      stack: 'stack',
      message: 'message',
      name: 'error'
    };
    const expectedAction = {
      type: constants.UPDATE_SHIPPING_ADDRESS_ERROR,
      payload: errorPayload
    };
    expect(actions.updateShippingAddressError(errorPayload)).toEqual(
      expectedAction
    );
  });

  it('updateShippingAddressLoading action creator should return dispatchable function for corresponding', () => {
    const expectedAction = {
      type: constants.UPDATE_SHIPPING_ADDRESS_LOADING
    };
    expect(actions.updateShippingAddressLoading()).toEqual(expectedAction);
  });
});
