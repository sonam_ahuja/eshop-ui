import { SHIPPING_CONSTANTS } from '@checkout/store/shipping/enums';

describe('enums', () => {
  it('SHIPPING_CONSTANTS test', () => {
    expect(SHIPPING_CONSTANTS).toBeDefined();
  });
});
