import shippingState from '@checkout/store/shipping/state';
import { shippingReducer } from '@checkout/store/shipping';

describe('billingReducer', () => {
  it('billingReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(shippingReducer(shippingState(), definedAction)).toEqual(
      shippingState()
    );
  });
});
