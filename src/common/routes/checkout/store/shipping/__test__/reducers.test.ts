import shipping from '@checkout/store/shipping/reducers';
import shippingInfoState from '@checkout/store/shipping/state';
import personalInfoState from '@checkout/store/personalInfo/state';
import {
  IShippingAddressState,
  IShippingFeeResponse,
  IUpdateShippingAddress,
  shippingType
} from '@checkout/store/shipping/types';
import actions from '@checkout/store/shipping/actions';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import { ShippingAddressResponse } from '@mocks/checkout/shipping';

describe('Shipping Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IShippingAddressState = shipping(
    undefined,
    definedAction
  );
  let expectedState: IShippingAddressState = shippingInfoState();

  const initializeValue = () => {
    initialStateValue = shipping(undefined, definedAction);
    expectedState = shippingInfoState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('SET_SHIPPING_TYPE should mutate shipping state with EXACT_DAY', () => {
    expectedState.shippingType = SHIPPING_TYPE.EXACT_DAY as shippingType;
    const action = actions.setShippingMode(
      SHIPPING_TYPE.EXACT_DAY as shippingType
    );
    const state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('SET_SHIPPING_TYPE should mutate shipping state with PARCEL_LOCKER', () => {
    expectedState.shippingType = SHIPPING_TYPE.PARCEL_LOCKER;
    const action = actions.setShippingMode(SHIPPING_TYPE.PARCEL_LOCKER);
    const state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('SET_SHIPPING_TYPE should mutate shipping state with PICK_UP_AT_STORE', () => {
    expectedState.shippingType = SHIPPING_TYPE.PICK_UP_STORE;
    const action = actions.setShippingMode(SHIPPING_TYPE.PICK_UP_STORE);
    const state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('SET_SHIPPING_TYPE should mutate shipping state with PICK_UP_POINTS', () => {
    expectedState.shippingType = SHIPPING_TYPE.PICK_UP_POINT;
    const action = actions.setShippingMode(SHIPPING_TYPE.PICK_UP_POINT);
    const state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('SET_DELIVERY_OPTION should mutate shipping state', () => {
    expectedState.shippingType = SHIPPING_TYPE.STANDARD;
    let action = actions.setDeliveryOptions(SHIPPING_TYPE.STANDARD);
    let state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
    expectedState.shippingType = SHIPPING_TYPE.PICK_UP_POINT;
    action = actions.setDeliveryOptions(SHIPPING_TYPE.PICK_UP_POINT);
    state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
    expectedState.shippingType = SHIPPING_TYPE.PICK_UP_STORE;
    action = actions.setDeliveryOptions(SHIPPING_TYPE.PICK_UP_STORE);
    state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
    expectedState.shippingType = SHIPPING_TYPE.PARCEL_LOCKER;
    action = actions.setDeliveryOptions(SHIPPING_TYPE.PARCEL_LOCKER);
    state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('SET_PROCEED_BUTTON should mutate shipping state', () => {
    expectedState.buttonEnable = true;
    const action = actions.setProceedButton(true);
    const state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('UPDATE_FORM_CHANGE_FIELD should mutate shipping state', () => {
    const action = actions.setProceedButton(true);
    const state = shipping(initialStateValue, action);
    expect(state).toEqual(state);
  });

  it('SEND_SHIPPING_ADDRESS should mutate shipping state', () => {
    const action = actions.sendShippingAddress();
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SAME_AS_PERSONAL_SWITCH should mutate shipping state', () => {
    const action = actions.sameAsPersonalSwitch();
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_FORM_CHANGE_FIELD should mutate shipping state', () => {
    const action = actions.updateFormChangeField(true);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SEND_SHIPPING_ADDRESS_ERROR should mutate shipping state', () => {
    const action = actions.sendShippingAddressError();
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SET_PERSONAL_INFO_DUMMY_STATE should mutate shipping state', () => {
    const action = actions.setPersonalInfoDummyState(personalInfoState());
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_DUMMY_STATE should mutate shipping state', () => {
    const action = actions.updateDummyState({});
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_SHIPPING_ADDRESS_SUCCESS should mutate shipping state', () => {
    const action = actions.fetchShippingInfoSuccess(ShippingAddressResponse);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SAME_AS_PERSONAL_SWITCH_TRUE should mutate shipping state', () => {
    const action = actions.sameAsPersonalSwitchTrue(true);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_SHIPPING_ADDRESS should mutate shipping state', () => {
    const payload: IUpdateShippingAddress = {
      key: 'key',
      value: 'value',
      isValid: true,
      validationMessage: 'message'
    };
    const action = actions.updateShippingAddress(payload);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_SHIPPING_FEE_SUCCESS should mutate shipping ', () => {
    const payload: IShippingFeeResponse = {};
    const action = actions.fetchShippingFeeSuccess(payload);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SET_SELECTED_STORE should mutate shipping state', () => {
    const payload = {
      id: 'id',
      streetNumber: 'streetNumber',
      address: 'address',
      city: 'city',
      postCode: 'postCode',
      flatNumber: 'flatNumber',
      deliveryNote: 'deliveryNote',
      name: 'name',
      geoX: 'geoX',
      geoY: 'geoY',
      state: 'state',
      country: 'country',
      distance: '',
      distanceUnit: ''
    };
    const action = actions.setSelectedStore(payload);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });
});
