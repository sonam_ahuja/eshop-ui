import shipping from '@checkout/store/shipping/reducers';
import { IShippingAddressState } from '@checkout/store/shipping/types';
import actions from '@checkout/store/shipping/actions';
import { SHIPPING_TYPE } from '@checkout/store/enums';

describe('Shipping Reducer2', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IShippingAddressState = shipping(
    undefined,
    definedAction
  );

  const initializeValue = () => {
    initialStateValue = shipping(undefined, definedAction);
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('SET_IF_USER_SELECTED_PICK_UP_LOCATION should mutate shipping state', () => {
    const action = actions.isUserPickedLocation();
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SET_IF_USER_SELECTED_PICK_UP_LOCATION should when pick is change mutate shipping state', () => {
    const newInitialStateValue = {
      ...initialStateValue,
      isPickUpLocationSelected: false
    };
    const action = actions.isUserPickedLocation();
    const state = shipping(newInitialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_SHIPPING_FEE_SUCCESS should mutate shipping state', () => {
    const action = actions.fetchPickupLocationsSuccess([]);
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_DELIVERY_DATE should mutate shipping state', () => {
    const action = actions.fetchDeliveryDate('12/12/2018');
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_DELIVERY_DATE_SUCCESS should mutate shipping state', () => {
    const action = actions.fetchDeliveryDateSuccess();
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('UPDATE_SELECTED_DATE should mutate shipping state', () => {
    const action = actions.updateSelectedDate('23');
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('FETCH_SHIPPING_ADDRESS_ERROR should mutate shipping state', () => {
    const action = actions.fetchShippingAddressError();
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });

  it('SEND_SHIPPING_ADDRESS_SUCCESS should mutate shipping state', () => {
    const action = actions.sendShippingAddressSuccess({
      sameAsPersonalInfo: true,
      shippingAddress: [
        {
          streetNumber: 'string',
          address: 'string',
          id: 'string',
          city: 'string',
          postCode: 'string',
          unit: 'string',
          flatNumber: 'string',
          deliveryNote: 'string',
          state: 'string',
          country: 'string',
          name: 'string',
          distance: 'string',
          distanceUnit: 'string',
          geoX: 'string',
          geoY: 'string'
        }
      ],
      deliveryMethod: {
        id: 'string',
        fee: 3,
        shippingType: SHIPPING_TYPE.EXACT_DAY,
        characteristics: [{ name: 'string', value: 2 }]
      }
    });
    const state = shipping(initialStateValue, action);
    expect(state).toBeDefined();
  });
});
