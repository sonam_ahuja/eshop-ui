import shippingSaga, {
  deliveryOptionChange,
  fetchDeliveryDates,
  fetchShippingFee,
  getPickUpLocations,
  sameAsPersonalSwitch,
  sameAsPersonalSwitchWithPayload,
  sendShippingAddress,
  setShippingMode
} from '@checkout/store/shipping/sagas';
import { SHIPPING_TYPE } from '@checkout/store/enums';

describe('shipping Info Saga', () => {
  it('shippingSaga saga test', () => {
    const shippingSagaGenerator = shippingSaga();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeDefined();
    expect(shippingSagaGenerator.next().value).toBeUndefined();
  });

  it('sameAsPersonalSwitch saga test', () => {
    const sameAsPersonalSwitchGenerator = sameAsPersonalSwitch();
    expect(sameAsPersonalSwitchGenerator.next().value).toBeUndefined();
  });

  it('sameAsPersonalSwitchWithPayload saga test', () => {
    const sameAsPersonalSwitchPayload = sameAsPersonalSwitchWithPayload({
      type: '',
      payload: true
    });
    expect(sameAsPersonalSwitchPayload.next().value).toBeDefined();
    expect(sameAsPersonalSwitchPayload.next().value).toBeUndefined();
  });

  it('sendShippingAddress saga test', () => {
    const sendShippingAddressGenerator = sendShippingAddress();
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeUndefined();
  });

  it('fetchShippingFee saga test', () => {
    const sendShippingAddressGenerator = fetchShippingFee({
      type: '',
      payload: false
    });
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeUndefined();
  });

  it('getPickUpLocations saga test', () => {
    const sendShippingAddressGenerator = getPickUpLocations({
      type: '',
      payload: {
        geoX: '23.55666',
        geoY: '71.233333'
      }
    });
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeUndefined();
  });

  it('fetchDeliveryDates saga test', () => {
    const sendShippingAddressGenerator = fetchDeliveryDates({
      type: '',
      payload: ''
    });
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeUndefined();
  });

  it('deliveryOptionChange saga test', () => {
    const sendShippingAddressGenerator = deliveryOptionChange({
      type: '',
      payload: SHIPPING_TYPE.EXACT_DAY
    });
    expect(sendShippingAddressGenerator.next().value).toBeUndefined();
  });

  it('setShippingMode saga test', () => {
    const sendShippingAddressGenerator = setShippingMode({
      type: '',
      payload: SHIPPING_TYPE.EXACT_DAY
    });
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeDefined();
    expect(sendShippingAddressGenerator.next().value).toBeUndefined();
  });
});
