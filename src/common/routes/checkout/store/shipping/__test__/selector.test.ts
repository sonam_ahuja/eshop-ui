import {
  dummyAddressToShippingAddress,
  getCartID,
  mapPersonalInfoToShipping,
  shippingAddressToDummyAddress
} from '@checkout/store/shipping/selector';
import appState from '@store/states/app';
import shippingAddressState from '@checkout/store/shipping/state';
import { ISelectedStoreAddress } from '@checkout/store/shipping/types';
describe('transformer', () => {
  it('getCartID ::', () => {
    expect(getCartID(appState().basket)).toBeDefined();
  });

  it('mapPersonalInfoToShipping ::', () => {
    expect(
      mapPersonalInfoToShipping(appState().checkout.shippingInfo.dummyState, {
        streetAddress: {
          value: '',
          isValid: true,
          validationMessage: '',
          colDesktop: 12,
          colMobile: 12
        }
      })
    ).toBeUndefined();
  });

  it('dummyAddressToShippingAddress ::', () => {
    expect(
      dummyAddressToShippingAddress(shippingAddressState().dummyState)
    ).toBeDefined();
  });

  it('shippingAddressToDummyAddress ::', () => {
    const payload: ISelectedStoreAddress = {
      id: 'id',
      streetNumber: 'streetNumber',
      address: 'address',
      city: 'city',
      postCode: 'postCode',
      flatNumber: 'flatNumber',
      name: 'name',
      geoX: 'geoX',
      geoY: 'geoY',
      state: 'state',
      country: 'country',
      distance: '',
      distanceUnit: ''
    };
    expect(shippingAddressToDummyAddress(payload)).toBeDefined();
  });
});
