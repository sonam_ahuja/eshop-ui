import {
  getDeliveryDates,
  getShippingFee,
  setShippingAddressService,
  updateShippingAddressService
} from '@checkout/store/shipping/services';
import shippingState from '@checkout/store/shipping/state';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {
  IPatchShippingAddressPayload,
  IPostShippingAddressPayload
} from '@checkout/store/shipping/types';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import { logError } from '@src/common/utils';

describe('shipping Service', () => {
  const mock = new MockAdapter(axios);
  const payload: IPostShippingAddressPayload = {
    address: {
      streetNumber: '',
      city: '',
      postCode: '',
      flatNumber: '',
      address: '',
      deliveryNote: '',
      unit: ''
    },
    deliveryMethod: {
      id: 'string',
      fee: 123,
      shippingType: SHIPPING_TYPE.STANDARD,
      characteristics: []
    }
  };
  const updatePayload: IPatchShippingAddressPayload = {
    ...payload,
    isChanged: true
  };
  it('setShippingAddressService success test', async () => {
    mock.onPost('shipping').replyOnce(200, payload);
    axios
      .post('/shipping')
      .then(async () => {
        const result = await setShippingAddressService(payload);
        expect(result).toEqual(payload);
      })
      .catch(error => logError(error));
  });

  it('updateShippingAddressService success test', async () => {
    mock.onPut('shipping').replyOnce(200, shippingState());
    axios
      .put('/shipping')
      .then(async () => {
        const result = await updateShippingAddressService(updatePayload);
        expect(result).toEqual(shippingState());
      })
      .catch(error => logError(error));
  });

  it('getShippingFee success test', async () => {
    mock.onPut('shipping').replyOnce(200, shippingState());
    axios
      .put('/shipping')
      .then(async () => {
        const result = await getShippingFee({
          address: payload.address,
          shippingTypes: [SHIPPING_TYPE.PARCEL_LOCKER]
        });
        expect(result).toEqual(shippingState());
      })
      .catch(error => logError(error));
  });

  it('getDeliveryDates success test', async () => {
    mock.onPut('shipping').replyOnce(200, shippingState());
    axios
      .put('/shipping')
      .then(async () => {
        const result = await getDeliveryDates('someString');
        expect(result).toEqual(shippingState());
      })
      .catch(error => logError(error));
  });
});
