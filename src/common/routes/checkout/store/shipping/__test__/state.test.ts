import {
  patchParcelLockerData,
  postParcelLockerData
} from '@checkout/store/shipping/state';

describe('shipping state', () => {
  it('patchParcelLockerData test', () => {
    expect(patchParcelLockerData()).toBeDefined();
  });

  it('postParcelLockerData test', () => {
    expect(postParcelLockerData()).toBeDefined();
  });
});
