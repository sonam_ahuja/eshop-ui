import {
  fetchShippingAddressTransform,
  payloadForFetchDummyShippingFee,
  payloadForFetchMainShippingFee,
  payloadForPatchMainStoreShipping,
  payloadForPatchShipping,
  payloadForPostShipping
} from '@checkout/store/shipping/transformer';
import { ShippingAddressResponse } from '@mocks/checkout/shipping';
import shippingInfoState from '@checkout/store/shipping/state';
import appState from '@store/states/app';

describe('transformer', () => {
  it('payloadForPatchMainStoreShipping test', () => {
    expect(payloadForPatchMainStoreShipping(shippingInfoState())).toBeDefined();
  });

  it('payloadForPatchShipping test', () => {
    expect(payloadForPatchShipping(shippingInfoState())).toBeDefined();
  });

  it('payloadForPostShipping test', () => {
    expect(payloadForPostShipping(shippingInfoState())).toBeDefined();
  });

  it('fetchShippingAddressTransform test', () => {
    expect(
      fetchShippingAddressTransform(
        shippingInfoState(),
        ShippingAddressResponse
      )
    ).toBeUndefined();
  });

  it('payloadForFetchDummyShippingFee test', () => {
    expect(
      payloadForFetchDummyShippingFee(
        shippingInfoState(),
        appState().configuration.cms_configuration.modules.checkout.shipping
          .shippingType
      )
    ).toBeDefined();
  });

  it('payloadForFetchMainShippingFee test', () => {
    expect(
      payloadForFetchMainShippingFee(
        shippingInfoState(),
        appState().configuration.cms_configuration.modules.checkout.shipping
          .shippingType
      )
    ).toBeDefined();
  });
});
