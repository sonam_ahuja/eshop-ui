import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@checkout/store/shipping/constants';
import {
  ILatLong,
  IPostShippingAddressResponse,
  ISelectedStoreAddress,
  IShippingAddressAndNote,
  IShippingAddressResponse,
  IShippingFeeResponse,
  IShippingFormField,
  IUpdateShippingAddress,
  shippingType
} from '@checkout/store/shipping/types';
import {
  IInputKeyValue,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';

export default {
  setShippingMode: actionCreator<shippingType>(CONSTANTS.SET_SHIPPING_MODE),
  sendShippingAddressSuccess: actionCreator<IPostShippingAddressResponse>(
    CONSTANTS.SEND_SHIPPING_ADDRESS_SUCCESS
  ),
  sendShippingAddressError: actionCreator<undefined>(
    CONSTANTS.SEND_SHIPPING_ADDRESS_ERROR
  ),
  sendShippingAddress: actionCreator<undefined>(
    CONSTANTS.SEND_SHIPPING_ADDRESS
  ),
  setProceedButton: actionCreator<boolean>(CONSTANTS.SET_PROCEED_BUTTON),
  updateFormChangeField: actionCreator<boolean>(
    CONSTANTS.UPDATE_FORM_CHANGE_FIELD
  ),
  updateDummyState: actionCreator<IShippingFormField>(
    CONSTANTS.UPDATE_DUMMY_STATE
  ),
  updateShippingAddress: actionCreator<IUpdateShippingAddress>(
    CONSTANTS.UPDATE_SHIPPING_ADDRESS
  ),
  updateInputField: actionCreator<IInputKeyValue>(
    CONSTANTS.UPDATE_SHIPPING_FIELD
  ),
  setDeliveryOptions: actionCreator<shippingType>(
    CONSTANTS.SET_DELIVERY_OPTION
  ),
  setSavedShippingType: actionCreator<shippingType>(
    CONSTANTS.SET_SAVED_SHIPPING_TYPE
  ),
  setPaymentNavigationButton: actionCreator<IInputKeyValue>(
    CONSTANTS.SET_PAYMENT_NAVIGATION_BUTTON
  ),

  sameAsPersonalSwitch: actionCreator<undefined>(
    CONSTANTS.SAME_AS_PERSONAL_SWITCH
  ),

  setPersonalInfoDummyState: actionCreator<IPersonalInfoState>(
    CONSTANTS.SET_PERSONAL_INFO_DUMMY_STATE
  ),

  sameAsPersonalSwitchTrue: actionCreator<boolean>(
    CONSTANTS.SAME_AS_PERSONAL_SWITCH_TRUE
  ),
  sameAsPersonalSwitchFalse: actionCreator<undefined>(
    CONSTANTS.SAME_AS_PERSONAL_SWITCH_FALSE
  ),

  updateNewAddressShipping: actionCreator<IShippingAddressAndNote>(
    CONSTANTS.UPDATE_NEW_SHIPPING_ADDRESS
  ),

  fetchShippingInfoSuccess: actionCreator<IShippingAddressResponse>(
    CONSTANTS.FETCH_SHIPPING_ADDRESS_SUCCESS
  ),
  fetchShippingAddressError: actionCreator<void>(
    CONSTANTS.FETCH_SHIPPING_ADDRESS_ERROR
  ),
  setShippingAddressError: actionCreator<Error>(
    CONSTANTS.SET_SHIPPING_ADDRESS_ERROR
  ),
  setShippingAddressLoading: actionCreator<undefined>(
    CONSTANTS.SET_SHIPPING_ADDRESS_LOADING
  ),

  updateShippingAddressError: actionCreator<Error>(
    CONSTANTS.UPDATE_SHIPPING_ADDRESS_ERROR
  ),
  updateShippingAddressLoading: actionCreator<undefined>(
    CONSTANTS.UPDATE_SHIPPING_ADDRESS_LOADING
  ),

  // fetch pick up locations
  fetchPickupLocations: actionCreator<ILatLong>(
    CONSTANTS.FETCH_PICKUP_LOCATIONS
  ),
  // tslint:disable-next-line:no-any
  fetchPickupLocationsError: actionCreator<any>(
    CONSTANTS.FETCH_PICKUP_LOCATIONS_ERROR
  ),
  fetchPickupLocationsSuccess: actionCreator<ISelectedStoreAddress[]>(
    CONSTANTS.FETCH_PICKUP_LOCATIONS_SUCCESS
  ),

  isUserPickedLocation: actionCreator(
    CONSTANTS.SET_IF_USER_SELECTED_PICK_UP_LOCATION
  ),
  setSelectedStore: actionCreator<ISelectedStoreAddress | null>(
    CONSTANTS.SET_SELECTED_STORE
  ),
  setOtherPersonPickup: actionCreator<boolean>(
    CONSTANTS.SET_OTHER_PERSON_PICKUP
  ),
  fetchShippingFee: actionCreator<boolean | undefined>(
    CONSTANTS.FETCH_SHIPPING_FEE
  ),
  fetchShippingFeeSuccess: actionCreator<IShippingFeeResponse>(
    CONSTANTS.FETCH_SHIPPING_FEE_SUCCESS
  ),
  fetchShippingFeeError: actionCreator<void>(
    CONSTANTS.FETCH_SHIPPING_FEE_ERROR
  ),
  fetchDeliveryDate: actionCreator<string>(CONSTANTS.FETCH_DELIVERY_DATE),
  fetchDeliveryDateSuccess: actionCreator<void>(
    CONSTANTS.FETCH_DELIVERY_DATE_SUCCESS
  ),
  updateSelectedDate: actionCreator<string>(CONSTANTS.UPDATE_SELECTED_DATE)
};
