import { AnyAction, Reducer } from 'redux';
import {
  IInputKeyValue,
  IPostShippingAddressResponse,
  ISelectedStoreAddress,
  IShippingAddressResponse,
  IShippingAddressState,
  IShippingFeeResponse,
  IShippingFormField,
  IShippingState,
  IUpdateShippingAddress,
  shippingType
} from '@checkout/store/shipping/types';
import initialState from '@checkout/store/shipping/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@checkout/store/shipping/constants';
import { mapPersonalInfoToShipping } from '@checkout/store/shipping/selector';
import {
  fetchShippingAddressTransform,
  updateDummyStateAfterGetData,
  updateSelectedStoreState,
  updateStateAfterSuccess
} from '@checkout/store/shipping/transformer';
import { DELIVERY_OPTIONS, SHIPPING_TYPE } from '@checkout/store/enums';
import { IPersonalInfoState } from '@checkout/store/personalInfo/types';

const reducers = {
  [CONSTANTS.SET_SHIPPING_MODE]: (
    state: IShippingAddressState,
    payload: shippingType
  ) => {
    state.shippingType = payload;
    if (
      payload === SHIPPING_TYPE.STANDARD ||
      payload === SHIPPING_TYPE.EXACT_DAY ||
      payload === SHIPPING_TYPE.SAME_DAY
    ) {
      state.deliveryOptions = DELIVERY_OPTIONS.DELIVER_TO_ADDRESS;
    } else {
      if (state.savedDeliveryMethod === payload) {
        state.isPickUpLocationSelected = true;
        if (state.selectedStoreAddress) {
          state.dummyState.streetAddressId = state.selectedStoreAddress.id;
          state.dummyState.streetAddress = state.selectedStoreAddress.address;
          state.dummyState.streetNumber =
            state.selectedStoreAddress.streetNumber;
          state.dummyState.flatNumber = state.selectedStoreAddress.flatNumber;
          state.dummyState.city = state.selectedStoreAddress.city;
          state.dummyState.postCode = state.selectedStoreAddress.postCode;
          state.dummyState.unit = '';
          state.dummyState.deliveryNote = '';
          state.dummyState.geoX = state.selectedStoreAddress.geoX;
          state.dummyState.geoY = state.selectedStoreAddress.geoY;
          state.dummyState.state = state.selectedStoreAddress.state;
          state.dummyState.country = state.selectedStoreAddress.country;
          state.dummyState.name = state.selectedStoreAddress.name;
          state.dummyState.distance = state.selectedStoreAddress.distance;
          state.dummyState.openHours = state.selectedStoreAddress.openHours;
          state.dummyState.distanceUnit =
            state.selectedStoreAddress.distanceUnit;
        }
      } else {
        state.isPickUpLocationSelected = false;
        state.pickUpLocations = null;
        state.dummyState = initialState().dummyState;
      }
    }
  },
  [CONSTANTS.SET_SAVED_SHIPPING_TYPE]: (
    state: IShippingAddressState,
    payload: shippingType
  ) => {
    state.shippingType = payload;
    if (
      payload === SHIPPING_TYPE.STANDARD ||
      payload === SHIPPING_TYPE.EXACT_DAY ||
      payload === SHIPPING_TYPE.SAME_DAY
    ) {
      localStorage.setItem('deliverToAddress', payload);
      state.deliveryOptions = DELIVERY_OPTIONS.DELIVER_TO_ADDRESS;
    } else if (payload === SHIPPING_TYPE.PICK_UP_POINT) {
      state.deliveryOptions = DELIVERY_OPTIONS.PICK_UP_POINTS;
      localStorage.removeItem('deliverToAddress');
    } else if (payload === SHIPPING_TYPE.PICK_UP_STORE) {
      state.deliveryOptions = DELIVERY_OPTIONS.PICK_UP_AT_STORE;
      localStorage.removeItem('deliverToAddress');
    } else if (payload === SHIPPING_TYPE.PARCEL_LOCKER) {
      state.deliveryOptions = DELIVERY_OPTIONS.PARCEL_LOCKER;
      localStorage.removeItem('deliverToAddress');
    }
  },
  [CONSTANTS.SET_DELIVERY_OPTION]: (
    state: IShippingAddressState,
    payload: shippingType
    // tslint:disable: no-identical-functions
  ) => {
    state.shippingType = payload;
    if (
      payload === SHIPPING_TYPE.STANDARD ||
      payload === SHIPPING_TYPE.EXACT_DAY ||
      payload === SHIPPING_TYPE.SAME_DAY
    ) {
      localStorage.setItem('deliverToAddress', payload);
      state.deliveryOptions = DELIVERY_OPTIONS.DELIVER_TO_ADDRESS;
    } else if (payload === SHIPPING_TYPE.PICK_UP_POINT) {
      state.deliveryOptions = DELIVERY_OPTIONS.PICK_UP_POINTS;
      localStorage.removeItem('deliverToAddress');
    } else if (payload === SHIPPING_TYPE.PICK_UP_STORE) {
      state.deliveryOptions = DELIVERY_OPTIONS.PICK_UP_AT_STORE;
      localStorage.removeItem('deliverToAddress');
    } else if (payload === SHIPPING_TYPE.PARCEL_LOCKER) {
      state.deliveryOptions = DELIVERY_OPTIONS.PARCEL_LOCKER;
      localStorage.removeItem('deliverToAddress');
    }
  },

  [CONSTANTS.UPDATE_SHIPPING_FIELD]: (
    state: IShippingState,
    payload: IInputKeyValue
  ) => {
    payload.key === 'address'
      ? (state.shippingForm[
          payload.key
        ].value[0].address = payload.value as string)
      : Object.assign(state.shippingForm[payload.key], payload);

    state.shippingForm.isFormChanged = true;
  },
  [CONSTANTS.SET_PAYMENT_NAVIGATION_BUTTON]: (
    state: IShippingState,
    payload: IInputKeyValue
  ) => {
    if (state.shippingForm[payload.key]) {
      if (payload.key === 'address') {
        state.shippingForm[
          payload.key
        ].value[0].address = payload.value as string;
      } else {
        Object.assign(state.shippingForm[payload.key], payload);
      }
      state.shippingForm.enableProceedButton = !state.shippingForm
        .enableProceedButton;
    }
  },
  [CONSTANTS.FETCH_SHIPPING_ADDRESS_ERROR]: (state: IShippingAddressState) => {
    state.sameAsPersonalInfo = true; // for no shipping data in get api
  },
  [CONSTANTS.FETCH_SHIPPING_ADDRESS_SUCCESS]: (
    state: IShippingAddressState,
    payload: IShippingAddressResponse
  ) => {
    if (payload) {
      fetchShippingAddressTransform(state, payload);

      if (payload.sameAsPersonalInfo) {
        updateDummyStateAfterGetData(state, payload);
      }
    }
  },
  [CONSTANTS.SAME_AS_PERSONAL_SWITCH_TRUE]: (
    state: IShippingAddressState,
    payload: boolean
  ) => {
    state.sameAsPersonalInfo = payload;
    state.formChanged = true;
  },
  [CONSTANTS.SAME_AS_PERSONAL_SWITCH]: (state: IShippingAddressState) => {
    state.sameAsPersonalInfo = !state.sameAsPersonalInfo;
    state.formChanged = true;
  },
  [CONSTANTS.SET_PROCEED_BUTTON]: (
    state: IShippingAddressState,
    payload: boolean
  ) => {
    state.buttonEnable = payload;
  },
  [CONSTANTS.UPDATE_FORM_CHANGE_FIELD]: (
    state: IShippingAddressState,
    payload: boolean
  ) => {
    state.formChanged = payload;
  },
  [CONSTANTS.UPDATE_DUMMY_STATE]: (
    state: IShippingAddressState,
    payload: IShippingFormField
  ) => {
    if (payload) {
      mapPersonalInfoToShipping(state.dummyState, payload);
    }
  },
  [CONSTANTS.UPDATE_SHIPPING_ADDRESS]: (
    state: IShippingAddressState,
    payload: IUpdateShippingAddress
  ) => {
    state[payload.key] = payload.value;
  },
  [CONSTANTS.SEND_SHIPPING_ADDRESS]: (state: IShippingAddressState) => {
    state.loading = true;
  },
  [CONSTANTS.SEND_SHIPPING_ADDRESS_SUCCESS]: (
    state: IShippingAddressState,
    payload: IPostShippingAddressResponse
  ) => {
    state.loading = false;
    state.savedDeliveryMethod = payload.deliveryMethod.shippingType;
    state.sameAsPersonalInfo = payload.sameAsPersonalInfo;

    if (
      state.shippingType === SHIPPING_TYPE.STANDARD ||
      state.shippingType === SHIPPING_TYPE.EXACT_DAY ||
      state.shippingType === SHIPPING_TYPE.SAME_DAY
    ) {
      updateStateAfterSuccess(state, payload);
      state.selectedDeliveryDate =
        payload.deliveryMethod.characteristics[0].value;
    } else {
      updateSelectedStoreState(state, payload);
    }
  },
  [CONSTANTS.SET_PERSONAL_INFO_DUMMY_STATE]: (
    state: IShippingAddressState,
    payload: IPersonalInfoState
  ) => {
    const {
      postCode,
      city,
      address: streetAddress,
      streetNumber,
      addressId: streetAddressId,
      flatNumber
    } = payload.formFields;
    const data = {
      postCode,
      city,
      streetAddress,
      streetNumber,
      streetAddressId,
      flatNumber
    };
    Object.assign(state.dummyState, data);
  },
  [CONSTANTS.SEND_SHIPPING_ADDRESS_ERROR]: (state: IShippingAddressState) => {
    state.loading = false;
  },
  [CONSTANTS.SET_SELECTED_STORE]: (
    state: IShippingAddressState,
    payload: ISelectedStoreAddress | null
  ) => {
    if (state.isPickUpLocationSelected) {
      state.selectedStoreAddress = payload;
    } else {
      if (payload) {
        state.dummyState.streetAddressId = payload.id;
        state.dummyState.streetAddress = payload.address;
        state.dummyState.streetNumber = payload.streetNumber;
        state.dummyState.flatNumber = payload.flatNumber;
        state.dummyState.city = payload.city;
        state.dummyState.postCode = payload.postCode;
        state.dummyState.unit = '';
        state.dummyState.deliveryNote = '';
        state.dummyState.geoX = payload.geoX;
        state.dummyState.geoY = payload.geoY;
        state.dummyState.state = payload.state;
        state.dummyState.country = payload.country;
        state.dummyState.name = payload.name;
        state.dummyState.distance = payload.distance;
        state.dummyState.distanceUnit = payload.distanceUnit;
        state.dummyState.openHours = payload.openHours;
      }
    }
  },
  [CONSTANTS.FETCH_SHIPPING_FEE_SUCCESS]: (
    state: IShippingAddressState,
    payload: IShippingFeeResponse
  ) => {
    state.shippingFee = payload;
    const checkForDeliveryFlag = localStorage.getItem('deliverToAddress');

    for (const deliveryMethod in payload) {
      if (payload[deliveryMethod].isDefault) {
        state.shippingType = deliveryMethod as SHIPPING_TYPE;
        state.shippingFee[deliveryMethod].fee = payload[deliveryMethod].fee;

        if (
          deliveryMethod === SHIPPING_TYPE.STANDARD ||
          deliveryMethod === SHIPPING_TYPE.EXACT_DAY ||
          deliveryMethod === SHIPPING_TYPE.SAME_DAY
        ) {
          state.deliveryOptions = DELIVERY_OPTIONS.DELIVER_TO_ADDRESS;
          localStorage.setItem('deliverToAddress', deliveryMethod);
        } else if (checkForDeliveryFlag) {
          state.deliveryOptions = DELIVERY_OPTIONS.DELIVER_TO_ADDRESS;
          state.shippingType = localStorage.getItem(
            'deliverToAddress'
          ) as SHIPPING_TYPE;
        } else if (deliveryMethod === SHIPPING_TYPE.PICK_UP_POINT) {
          state.deliveryOptions = DELIVERY_OPTIONS.PICK_UP_POINTS;
          localStorage.removeItem('deliverToAddress');
        } else if (deliveryMethod === SHIPPING_TYPE.PICK_UP_STORE) {
          state.deliveryOptions = DELIVERY_OPTIONS.PICK_UP_AT_STORE;
          localStorage.removeItem('deliverToAddress');
        } else if (deliveryMethod === SHIPPING_TYPE.PARCEL_LOCKER) {
          state.deliveryOptions = DELIVERY_OPTIONS.PARCEL_LOCKER;
          localStorage.removeItem('deliverToAddress');
        }
      }
    }
  },
  [CONSTANTS.SET_IF_USER_SELECTED_PICK_UP_LOCATION]: (
    state: IShippingAddressState
  ) => {
    state.isPickUpLocationSelected = !state.isPickUpLocationSelected;
    if (state.isPickUpLocationSelected) {
      state.selectedStoreAddress = {
        address: state.dummyState.streetAddress as string,
        streetNumber: state.dummyState.streetNumber as string,
        flatNumber: state.dummyState.flatNumber as string,
        city: state.dummyState.city as string,
        postCode: state.dummyState.postCode as string,
        geoX: state.dummyState.geoX as string,
        geoY: state.dummyState.geoY as string,
        state: state.dummyState.state as string,
        country: state.dummyState.country as string,
        distance: state.dummyState.distance as string,
        distanceUnit: state.dummyState.distanceUnit as string,
        name: state.dummyState.name as string,
        id: state.dummyState.streetAddressId as string,
        openHours: state.dummyState.openHours
      };
    }
  },
  [CONSTANTS.FETCH_PICKUP_LOCATIONS_SUCCESS]: (
    state: IShippingAddressState,
    payload: ISelectedStoreAddress[]
  ) => {
    state.pickUpLocations = payload;
  },
  [CONSTANTS.UPDATE_SELECTED_DATE]: (
    state: IShippingAddressState,
    payload: string
  ) => {
    state.selectedDeliveryDate = new Date(Number(payload) * 1000).toISOString();
  },
  [CONSTANTS.FETCH_DELIVERY_DATE_SUCCESS]: (
    state: IShippingAddressState,
    payload: string[] | number[]
  ) => {
    state.pickADateList = payload;
    state.loading = false;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IShippingAddressState,
  AnyAction
  // tslint:disable-next-line:max-file-line-count
>;
