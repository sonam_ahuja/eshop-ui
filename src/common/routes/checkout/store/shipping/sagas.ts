import CONSTANTS from '@checkout/store/shipping/constants';
import {
  getDeliveryDates,
  getPickUpPoints,
  getShippingFee,
  setShippingAddressService,
  updateShippingAddressService
} from '@checkout/store/shipping/services';
import actions from '@checkout/store/shipping/actions';
import basketActions from '@basket/store/actions';
import store from '@common/store';
import { put, takeLatest } from 'redux-saga/effects';
import {
  payloadForFetchDummyShippingFee,
  payloadForFetchMainShippingFee,
  payloadForPatchMainStoreShipping,
  payloadForPatchParcelShipping,
  payloadForPatchShipping,
  payloadForPostParcelShipping,
  payloadForPostShipping
} from '@checkout/store/shipping/transformer';
import { SHIPPING_TYPE } from '@checkout/store/enums';
import {
  IFee,
  ILatLong,
  IPostShippingAddressResponse,
  IShippingFeeResponse,
  shippingType as shipmentType
} from '@checkout/store/shipping/types';
import history from '@src/client/history';
import { logError } from '@src/common/utils';
import checkoutActions from '@checkout/store/actions';

const CHECKOUT_PAYMENT = '/checkout/payment';

export function* sendShippingAddress(): Generator {
  try {
    const shippingState = store.getState().checkout.shippingInfo;

    const { dataReceived, sameAsPersonalInfo } = shippingState;

    let response = {};

    if (
      shippingState.shippingType === SHIPPING_TYPE.STANDARD ||
      shippingState.shippingType === SHIPPING_TYPE.EXACT_DAY ||
      shippingState.shippingType === SHIPPING_TYPE.SAME_DAY
    ) {
      if (!dataReceived && !sameAsPersonalInfo) {
        response = yield setShippingAddressService(
          payloadForPostShipping(shippingState)
        );
      } else if (sameAsPersonalInfo) {
        response = yield updateShippingAddressService(
          payloadForPatchShipping(shippingState)
        );
      } else {
        response = yield updateShippingAddressService(
          payloadForPatchMainStoreShipping(shippingState)
        );
      }
    } else {
      response = shippingState.dataReceived
        ? yield updateShippingAddressService(
            payloadForPatchParcelShipping(shippingState)
          )
        : yield setShippingAddressService(
            payloadForPostParcelShipping(shippingState)
          );
    }

    yield put(
      actions.sendShippingAddressSuccess(
        response as IPostShippingAddressResponse
      )
    );

    if (history) {
      history.push(CHECKOUT_PAYMENT);
    }
  } catch (e) {
    yield put(actions.sendShippingAddressError());
  }
}

export function* sameAsPersonalSwitch(): Generator {
  const { personalInfo, shippingInfo } = store.getState().checkout;

  if (shippingInfo.sameAsPersonalInfo) {
    yield put(actions.setPersonalInfoDummyState(personalInfo));
  }
}

export function* sameAsPersonalSwitchWithPayload(action: {
  type: string;
  payload: boolean;
}): Generator {
  const { personalInfo } = store.getState().checkout;

  if (action.payload) {
    yield put(actions.setPersonalInfoDummyState(personalInfo));
  }
}

// tslint:disable-next-line:cognitive-complexity
export function* fetchShippingFee(action: {
  type: string;
  payload: boolean;
}): Generator {
  try {
    if (action.payload) {
      yield put(checkoutActions.fetchCheckoutAddressLoading(true));
    }
    const shippingState = store.getState().checkout.shippingInfo;
    const cmsConfig = store.getState().configuration.cms_configuration.modules
      .checkout.shipping.shippingType;
    yield put(actions.setShippingAddressLoading());
    let response: IShippingFeeResponse;
    if (
      shippingState.streetAddressId !== '' ||
      shippingState.dummyState.streetAddressId !== ''
    ) {
      if (shippingState.sameAsPersonalInfo) {
        response = yield getShippingFee(
          payloadForFetchDummyShippingFee(shippingState, cmsConfig)
        );
      } else {
        response = yield getShippingFee(
          payloadForFetchMainShippingFee(shippingState, cmsConfig)
        );
      }

      let deliveryFlag = false;
      let defaultShippingType: SHIPPING_TYPE = shippingState.shippingType;

      if (response) {
        Object.keys(response).forEach(item => {
          if (response[item].isDefault) {
            defaultShippingType = item as SHIPPING_TYPE;
          }
          if (item === SHIPPING_TYPE.EXACT_DAY) {
            deliveryFlag = true;
          }
        });

        if (deliveryFlag) {
          yield put(
            actions.fetchDeliveryDate(
              (response[SHIPPING_TYPE.EXACT_DAY] as IFee).productId
            )
          );
        }

        yield put(actions.fetchShippingFeeSuccess(response));

        if ((response[defaultShippingType] as IFee).cartItems) {
          yield put(
            basketActions.setCheckoutButtonStatus(
              (response[defaultShippingType] as IFee).cartItems
            )
          );
        }

        if ((response[defaultShippingType] as IFee).cartSummary) {
          yield put(
            basketActions.setCartSummary(
              (response[defaultShippingType] as IFee).cartSummary
            )
          );
          yield put(
            checkoutActions.evaluateCartSummaryPrice(
              (response[defaultShippingType] as IFee).cartSummary
            )
          );
        }
      }
    }
    if (action.payload) {
      yield put(checkoutActions.fetchCheckoutAddressLoading(false));
    }
  } catch (e) {
    yield put(actions.fetchShippingFeeError(e));
  }
}

export function* getPickUpLocations(action: {
  type: string;
  payload: ILatLong;
}): Generator {
  try {
    const configuration = store.getState().configuration;
    const googleMapConfiguration =
      configuration.cms_configuration.global.googleMap;
    const shippingType = store.getState().checkout.shippingInfo.shippingType;

    const markers = yield getPickUpPoints({
      shippingType,
      ...action.payload,
      distance: googleMapConfiguration.rangeCover.toString(),
      distanceUnit: 'km',
      page: '0',
      size: '10'
    });

    if (Array.isArray(markers) && markers.length) {
      yield put(actions.fetchPickupLocationsSuccess(markers));
      yield put(actions.setSelectedStore(markers[0]));
    }
  } catch (error) {
    logError(error);
  }
}

export function* fetchDeliveryDates(action: {
  type: string;
  payload: string;
}): Generator {
  try {
    const shippingType = store.getState().checkout.shippingInfo.shippingType;
    const response = yield getDeliveryDates(action.payload);
    yield put(actions.fetchDeliveryDateSuccess(response));
    if (shippingType !== SHIPPING_TYPE.EXACT_DAY) {
      yield put(actions.updateSelectedDate(response[0]));
    }
  } catch (error) {
    logError(error);
  }
}

export function* setShippingMode(action: {
  type: string;
  payload: shipmentType;
}): Generator {
  try {
    if (
      action.payload === SHIPPING_TYPE.STANDARD ||
      action.payload === SHIPPING_TYPE.EXACT_DAY ||
      action.payload === SHIPPING_TYPE.SAME_DAY
    ) {
      const { personalInfo } = store.getState().checkout;
      yield put(actions.setPersonalInfoDummyState(personalInfo));
    }

    yield put(actions.setDeliveryOptions(action.payload));
  } catch (error) {
    logError(error);
  }
}

export function* deliveryOptionChange(action: {
  type: string;
  payload: shipmentType;
}): Generator {
  try {
    const shippingState = store.getState().checkout.shippingInfo;

    if (action.payload) {
      const cartSummaryData = (shippingState.shippingFee[
        action.payload
      ] as IFee).cartSummary;

      if (cartSummaryData && cartSummaryData.length > 0) {
        yield put(
          basketActions.setCartSummary(
            (shippingState.shippingFee[action.payload] as IFee).cartSummary
          )
        );
      }
    }
  } catch (error) {
    logError(error);
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.SEND_SHIPPING_ADDRESS, sendShippingAddress);
  yield takeLatest(CONSTANTS.SAME_AS_PERSONAL_SWITCH, sameAsPersonalSwitch);
  yield takeLatest(CONSTANTS.FETCH_SHIPPING_FEE, fetchShippingFee);
  yield takeLatest(
    CONSTANTS.SAME_AS_PERSONAL_SWITCH_TRUE,
    sameAsPersonalSwitchWithPayload
  );

  yield takeLatest(CONSTANTS.FETCH_PICKUP_LOCATIONS, getPickUpLocations);
  yield takeLatest(CONSTANTS.FETCH_DELIVERY_DATE, fetchDeliveryDates);
  yield takeLatest(CONSTANTS.SET_SHIPPING_MODE, setShippingMode);
  yield takeLatest(CONSTANTS.SET_DELIVERY_OPTION, deliveryOptionChange);
}
// tslint:disable-next-line:max-file-line-count
export default watcherSaga;
