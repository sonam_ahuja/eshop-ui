import { IBasketState } from '@common/routes/basket/store/types';
import {
  IDummyState,
  ISelectedStoreAddress,
  IShippingFormField
} from '@checkout/store/shipping/types';

export const getCartID = (basket: IBasketState) => {
  return basket.cartId;
};

export const mapPersonalInfoToShipping = (
  dummyState: IDummyState,
  localState: IShippingFormField
): void => {
  const obj = {};
  Object.keys(localState).forEach(key => {
    obj[key] = localState[key].value;
  });
  Object.assign(dummyState, obj);
};

export const dummyAddressToShippingAddress = (
  dummyState: IDummyState
): ISelectedStoreAddress => {
  return {
    address: dummyState.streetAddress ? dummyState.streetAddress : '',
    streetNumber: dummyState.streetNumber ? dummyState.streetNumber : '',
    flatNumber: dummyState.flatNumber ? dummyState.flatNumber : '',
    city: dummyState.city ? dummyState.city : '',
    postCode: dummyState.postCode ? dummyState.postCode : '',
    geoX: dummyState.geoX ? dummyState.geoX : '',
    geoY: dummyState.geoY ? dummyState.geoY : '',
    state: dummyState.state ? dummyState.state : '',
    country: dummyState.country ? dummyState.country : '',
    distance: dummyState.distance ? dummyState.distance : '',
    distanceUnit: dummyState.distanceUnit ? dummyState.distanceUnit : '',
    name: dummyState.name ? dummyState.name : ''
  };
};

export const shippingAddressToDummyAddress = (
  shippingAddress: ISelectedStoreAddress
): IDummyState => {
  return {
    streetAddressId: shippingAddress.id,
    streetAddress: shippingAddress.address,
    streetNumber: shippingAddress.streetNumber,
    flatNumber: shippingAddress.flatNumber,
    city: shippingAddress.city,
    postCode: shippingAddress.postCode,
    unit: '',
    deliveryNote: '',
    geoX: shippingAddress.geoX,
    geoY: shippingAddress.geoY,
    state: shippingAddress.state,
    country: shippingAddress.country,
    name: shippingAddress.name,
    distance: shippingAddress.distance,
    distanceUnit: shippingAddress.distanceUnit
  };
};
