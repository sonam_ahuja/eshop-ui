import apiCaller from '@common/utils/apiCaller';
import {
  IPatchShippingAddressPayload,
  IPostShippingAddressPayload
} from '@checkout/store/shipping/types';
import apiEndpoints from '@common/constants/apiEndpoints';
import { logError } from '@src/common/utils';
import * as shippingFeeApi from '@common/types/api/checkout/shipping/shippingFee';
import * as getPickUpPointsApi from '@common/types/api/checkout/shipping/getPickUpPoints';

export const setShippingAddressService = async (
  payload: IPostShippingAddressPayload
): Promise<{} | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.SHIPPING.SET_PERSONAL_SHIPPING_ADDRESS.method.toLowerCase()
    ](
      apiEndpoints.CHECKOUT.SHIPPING.SET_PERSONAL_SHIPPING_ADDRESS.url,
      payload
    );
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const updateShippingAddressService = async (
  payload: IPatchShippingAddressPayload
): Promise<{} | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.SHIPPING.UPDATE_PERSONAL_SHIPPING_ADDRESS.method.toLowerCase()
    ](
      apiEndpoints.CHECKOUT.SHIPPING.UPDATE_PERSONAL_SHIPPING_ADDRESS.url,
      payload
    );
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const getShippingFee = (
  payload: shippingFeeApi.POST.IRequest
): Promise<shippingFeeApi.POST.IResponse | Error> => {
  return apiCaller.post(
    apiEndpoints.CHECKOUT.SHIPPING.GET_SHIPPING_FEE.url,
    payload
  );
};

export const getPickUpPoints = (
  payload: getPickUpPointsApi.POST.IRequest
): Promise<getPickUpPointsApi.POST.IResponse | Error> => {
  return apiCaller.post(
    apiEndpoints.CHECKOUT.SHIPPING.GET_PICK_UP_POINTS.url,
    payload
  );
};

export const getDeliveryDates = async (
  payload: string
): Promise<{} | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.CHECKOUT.SHIPPING.GET_DELIVERY_DATE.method.toLowerCase()
    ](apiEndpoints.CHECKOUT.SHIPPING.GET_DELIVERY_DATE.url(payload));
  } catch (error) {
    logError(error);
    throw error;
  }
};
