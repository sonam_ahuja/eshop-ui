import {
  IPatchShippingAddressPayload,
  IPostShippingAddressPayload,
  IShippingAddressState,
  IShippingFeeRequest
  // IShippingState
} from '@checkout/store/shipping/types';
import {
  // DELIVERY_OPTIONS,
  DELIVERY_OPTIONS,
  SHIPPING_TYPE
} from '@checkout/store/enums';

export default (): IShippingAddressState => ({
  streetAddress: '',
  streetAddressId: '',
  streetNumber: '',
  flatNumber: '',
  city: '',
  postCode: '',
  unit: '',
  deliveryNote: '',
  sameAsPersonalInfo: false,
  shippingFee: {
    standard: {
      cartSummary: [],
      cartItems: [],
      id: '',
      fee: -1,
      deliveryDate: '',
      name: '',
      isDefault: false,
      productId: ''
    },
    withinTime: {
      cartSummary: [],
      cartItems: [],
      id: '',
      fee: -1,
      deliveryDate: '',
      name: '',
      isDefault: false,
      productId: ''
    },
    pickADate: {
      cartSummary: [],
      cartItems: [],
      id: '',
      fee: -1,
      deliveryDate: '',
      name: '',
      isDefault: false,
      productId: ''
    },
    pickUpStore: {
      cartSummary: [],
      cartItems: [],
      id: '',
      fee: -1,
      deliveryDate: '',
      name: '',
      isDefault: false,
      productId: ''
    },
    parcelLocker: {
      cartSummary: [],
      cartItems: [],
      id: '',
      fee: -1,
      deliveryDate: '',
      name: '',
      isDefault: false,
      productId: ''
    },
    pickUpPoints: {
      cartSummary: [],
      cartItems: [],
      id: '',
      fee: -1,
      deliveryDate: '',
      name: '',
      isDefault: false,
      productId: ''
    }
  },
  deliveryOptions: DELIVERY_OPTIONS.DELIVER_TO_ADDRESS,
  shippingType: SHIPPING_TYPE.STANDARD,
  savedDeliveryMethod: SHIPPING_TYPE.STANDARD,
  buttonEnable: false,
  formChanged: false,
  loading: false,
  dataReceived: false,
  dummyState: {
    streetAddress: '',
    streetAddressId: '',
    streetNumber: '',
    flatNumber: '',
    city: '',
    postCode: '',
    unit: '',
    state: '',
    country: '',
    name: '',
    geoX: '',
    geoY: '',
    distance: '',
    distanceUnit: ''
  },
  pickUpLocations: null,
  isPickUpLocationSelected: false,
  selectedStoreAddress: null,
  isOtherPersonPickup: true,
  pickUpPersonInfo: null,
  pickupLocationInfo: [],
  pickADateList: [],
  selectedDeliveryDate: ''
});

export const postShippingAdderessData = (): IPostShippingAddressPayload => ({
  address: {
    streetNumber: '',
    address: '',
    city: '',
    postCode: '',
    flatNumber: '',
    deliveryNote: '',
    unit: ''
  },
  deliveryMethod: {
    id: '',
    fee: 0,
    shippingType: SHIPPING_TYPE.STANDARD,
    characteristics: [
      {
        name: 'deliveryDate',
        // tslint:disable-next-line:no-duplicate-string
        value: '2019-01-02T10:00:00Z'
      }
    ]
  }
});

export const patchShippingAdderessData = (): IPatchShippingAddressPayload => ({
  isChanged: false,
  address: {
    streetNumber: '',
    address: '',
    id: '',
    city: '',
    postCode: '',
    flatNumber: '',
    deliveryNote: '',
    unit: ''
  },
  deliveryMethod: {
    id: '',
    fee: 0,
    shippingType: SHIPPING_TYPE.STANDARD,
    characteristics: [
      {
        name: 'deliveryDate',
        value: '2019-01-02T10:00:00Z'
      }
    ]
  }
});

export const payloadShippinFeeData = (): IShippingFeeRequest => ({
  address: {
    streetNumber: '',
    address: '',
    id: '',
    city: '',
    postCode: '',
    flatNumber: ''
  },
  shippingTypes: [
    SHIPPING_TYPE.STANDARD,
    SHIPPING_TYPE.SAME_DAY,
    SHIPPING_TYPE.EXACT_DAY
  ]
});

export const postParcelLockerData = (): IPostShippingAddressPayload => ({
  address: {
    streetNumber: '',
    address: '',
    city: '',
    postCode: '',
    flatNumber: '',
    state: '',
    country: '',
    name: '',
    distance: '',
    distanceUnit: '',
    unit: '',
    deliveryNote: ''
  },
  deliveryMethod: {
    id: '',
    fee: 0,
    shippingType: SHIPPING_TYPE.STANDARD,
    characteristics: [
      {
        name: 'deliveryDate',
        // tslint:disable-next-line:no-duplicate-string
        value: '2019-01-02T10:00:00Z'
      }
    ]
  }
});

export const patchParcelLockerData = (): IPatchShippingAddressPayload => ({
  isChanged: false,
  address: {
    streetNumber: '',
    address: '',
    id: '',
    city: '',
    postCode: '',
    flatNumber: '',
    state: '',
    country: '',
    name: '',
    distance: '',
    distanceUnit: ''
  },
  deliveryMethod: {
    id: '',
    fee: 0,
    shippingType: SHIPPING_TYPE.STANDARD,
    characteristics: [
      {
        name: 'deliveryDate',
        value: '2019-01-02T10:00:00Z'
      }
    ]
  }
});
