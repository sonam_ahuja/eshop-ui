import {
  IFee,
  IPatchShippingAddressPayload,
  IPostShippingAddressPayload,
  IPostShippingAddressResponse,
  ISelectedStoreAddress,
  IShippingAddressResponse,
  IShippingAddressState,
  IShippingFeeRequest,
  shippingType
} from '@checkout/store/shipping/types';
import {
  patchParcelLockerData,
  patchShippingAdderessData,
  payloadShippinFeeData,
  postParcelLockerData,
  postShippingAdderessData
} from '@checkout/store/shipping/state';
import { IShippingType } from '@src/common/store/types/configuration';

import { SHIPPING_TYPE } from '../enums';

export const fetchShippingAddressTransform = (
  data: IShippingAddressState,
  payload: IShippingAddressResponse
) => {
  const {
    id,
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    deliveryNote,
    unit,
    name,
    geoX,
    geoY,
    state,
    country
  } = payload.shippingAddress[0];

  if (
    payload.deliveryMethod.shippingType === SHIPPING_TYPE.STANDARD ||
    payload.deliveryMethod.shippingType === SHIPPING_TYPE.EXACT_DAY ||
    payload.deliveryMethod.shippingType === SHIPPING_TYPE.SAME_DAY
  ) {
    const obj = {
      streetAddressId: id || '',
      streetNumber: streetNumber || '',
      streetAddress: address || '',
      city: city || '',
      postCode: postCode || '',
      flatNumber: flatNumber || '',
      deliveryNote: deliveryNote || '',
      unit: unit || ''
    };
    data.sameAsPersonalInfo = payload.sameAsPersonalInfo;
    data.dataReceived = true;
    data.buttonEnable = true;
    data.shippingType = payload.deliveryMethod.shippingType;
    data.selectedDeliveryDate =
      payload.deliveryMethod.characteristics &&
      payload.deliveryMethod.characteristics[0].value;

    const shipmentType = payload.deliveryMethod.shippingType;

    const shippingFeeClone = data.shippingFee && data.shippingFee[shipmentType];
    if (shippingFeeClone) {
      shippingFeeClone.fee = payload.deliveryMethod.fee;
      shippingFeeClone.id = payload.deliveryMethod.id;
    }
    Object.assign(data, obj);
  } else {
    data.isPickUpLocationSelected = true;
    const obj = {
      id: id || '',
      streetNumber: streetNumber || '',
      address: address || '',
      city: city || '',
      postCode: postCode || '',
      flatNumber: flatNumber || '',
      deliveryNote: deliveryNote || '',
      name: name || '',
      geoX: geoX || '',
      geoY: geoY || '',
      state: state || '',
      country: country || '',
      distance: '',
      distanceUnit: ''
    };
    data.savedDeliveryMethod = payload.deliveryMethod.shippingType;
    data.sameAsPersonalInfo = payload.sameAsPersonalInfo;
    data.shippingType = payload.deliveryMethod.shippingType;
    data.dataReceived = true;
    data.buttonEnable = true;
    data.streetAddressId = obj.id;
    data.selectedStoreAddress = { ...obj };

    const shipmentType = payload.deliveryMethod.shippingType;

    const shippingFeeClone = data.shippingFee && data.shippingFee[shipmentType];
    if (shippingFeeClone) {
      shippingFeeClone.fee = payload.deliveryMethod.fee;
      shippingFeeClone.id = payload.deliveryMethod.id;
    }
  }
};

export const payloadForPostShipping = (
  data: IShippingAddressState
): IPostShippingAddressPayload => {
  const postData = postShippingAdderessData();

  const {
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber,
    unit,
    deliveryNote
  } = data;
  postData.address = {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    unit,
    deliveryNote
  };

  postData.deliveryMethod.id = (data.shippingFee[data.shippingType] as IFee).id;
  postData.deliveryMethod.fee = (data.shippingFee[
    data.shippingType
  ] as IFee).fee;
  postData.deliveryMethod.shippingType = data.shippingType;
  postData.deliveryMethod.characteristics = [
    {
      name: 'deliveryDate',
      value: data.selectedDeliveryDate
    }
  ];

  return postData;
};

export const payloadForPatchShipping = (
  data: IShippingAddressState
): IPatchShippingAddressPayload => {
  const patchData = patchShippingAdderessData();

  const {
    streetAddressId: id,
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber,
    unit,
    deliveryNote
  } = data.dummyState;
  patchData.address = {
    id,
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    unit,
    deliveryNote
  };

  patchData.isChanged =
    data.shippingType === SHIPPING_TYPE.STANDARD ||
    data.shippingType === SHIPPING_TYPE.EXACT_DAY ||
    data.shippingType === SHIPPING_TYPE.SAME_DAY
      ? data.formChanged
      : true;

  patchData.deliveryMethod.id = (data.shippingFee[
    data.shippingType
  ] as IFee).id;
  patchData.deliveryMethod.fee = (data.shippingFee[
    data.shippingType
  ] as IFee).fee;
  patchData.deliveryMethod.shippingType = data.shippingType;
  patchData.deliveryMethod.characteristics = [
    {
      name: 'deliveryDate',
      value: data.selectedDeliveryDate
    }
  ];

  return patchData;
};

export const payloadForPatchMainStoreShipping = (
  data: IShippingAddressState
): IPatchShippingAddressPayload => {
  const patchData = patchShippingAdderessData();

  const {
    streetAddressId: id,
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber,
    unit,
    deliveryNote
  } = data;
  patchData.address = {
    id,
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    unit,
    deliveryNote
  };

  patchData.isChanged =
    data.shippingType === SHIPPING_TYPE.STANDARD ||
    data.shippingType === SHIPPING_TYPE.EXACT_DAY ||
    data.shippingType === SHIPPING_TYPE.SAME_DAY
      ? data.formChanged
      : true;
  patchData.deliveryMethod.id = (data.shippingFee[
    data.shippingType
  ] as IFee).id;
  patchData.deliveryMethod.fee = (data.shippingFee[
    data.shippingType
  ] as IFee).fee;
  patchData.deliveryMethod.shippingType = data.shippingType;
  patchData.deliveryMethod.characteristics = [
    {
      name: 'deliveryDate',
      value: data.selectedDeliveryDate
    }
  ];

  return patchData;
};

export const payloadForFetchDummyShippingFee = (
  data: IShippingAddressState,
  cmsConfig: IShippingType
): IShippingFeeRequest => {
  const patchData = payloadShippinFeeData();

  const {
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber,
    streetAddressId
  } = data.dummyState;
  patchData.address = {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    id: streetAddressId
  };

  patchData.shippingTypes =
    data.shippingType === SHIPPING_TYPE.STANDARD ||
    data.shippingType === SHIPPING_TYPE.EXACT_DAY ||
    data.shippingType === SHIPPING_TYPE.SAME_DAY
      ? enableShippingType(cmsConfig)
      : [data.shippingType];

  return patchData;
};

export const payloadForFetchMainShippingFee = (
  data: IShippingAddressState,
  cmsConfig: IShippingType
): IShippingFeeRequest => {
  const patchData = payloadShippinFeeData();

  const {
    streetNumber,
    streetAddress: address,
    city,
    postCode,
    flatNumber
  } = data;
  patchData.address = {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber
  };

  patchData.shippingTypes =
    data.shippingType === SHIPPING_TYPE.STANDARD ||
    data.shippingType === SHIPPING_TYPE.EXACT_DAY ||
    data.shippingType === SHIPPING_TYPE.SAME_DAY
      ? enableShippingType(cmsConfig)
      : [data.shippingType];

  return patchData;
};

export const updateStateAfterSuccess = (
  data: IShippingAddressState,
  payload: IPostShippingAddressResponse
) => {
  const {
    id: streetAddressId,
    streetNumber,
    address: streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote
  } = payload.shippingAddress[0];

  const obj = {
    streetAddressId,
    streetNumber,
    streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote,
    dummyState: data.dummyState
  };

  data.selectedStoreAddress = null;
  data.isPickUpLocationSelected = false;
  Object.assign(data, obj);
};

export const updateSelectedStoreState = (
  data: IShippingAddressState,
  payload: IPostShippingAddressResponse
) => {
  const {
    id,
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    deliveryNote,
    state,
    country,
    geoX,
    geoY,
    name
  } = payload.shippingAddress[0];

  const obj = {
    id,
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    deliveryNote,
    state,
    country,
    geoX,
    geoY,
    name
  };

  data.isPickUpLocationSelected = true;
  Object.assign(data.selectedStoreAddress, obj);
};

export const updateDummyStateAfterGetData = (
  state: IShippingAddressState,
  payload: IShippingAddressResponse
) => {
  const {
    id,
    streetNumber,
    address: streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote,
    unit
  } = payload.shippingAddress[0];
  const data = {
    streetAddressId: id,
    streetNumber,
    streetAddress,
    city,
    postCode,
    flatNumber,
    deliveryNote,
    unit
  };

  Object.assign(state.dummyState, data);
};

export const payloadForPostParcelShipping = (
  data: IShippingAddressState
): IPostShippingAddressPayload => {
  const postData = postParcelLockerData();

  const {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    state,
    country,
    distance,
    distanceUnit,
    name,
    geoX,
    geoY
  } = data.selectedStoreAddress as ISelectedStoreAddress;
  postData.address = {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    state,
    country,
    distance,
    distanceUnit,
    name,
    geoX,
    geoY,
    unit: '',
    deliveryNote: ''
  };

  postData.deliveryMethod.id = (data.shippingFee[data.shippingType] as IFee).id;
  postData.deliveryMethod.fee = (data.shippingFee[
    data.shippingType
  ] as IFee).fee;
  postData.deliveryMethod.shippingType = data.shippingType;
  postData.deliveryMethod.characteristics = [
    {
      name: 'deliveryDate',
      value: data.selectedDeliveryDate
    }
  ];

  return postData;
};

export const payloadForPatchParcelShipping = (
  data: IShippingAddressState
): IPatchShippingAddressPayload => {
  const patchData = patchParcelLockerData();

  const {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    state,
    country,
    distance,
    distanceUnit,
    name,
    geoX,
    geoY,
    id
  } = data.selectedStoreAddress as ISelectedStoreAddress;
  patchData.address = {
    streetNumber,
    address,
    city,
    postCode,
    flatNumber,
    id,
    state,
    country,
    distance,
    distanceUnit,
    name,
    geoX,
    geoY,
    unit: '',
    deliveryNote: ''
  };
  patchData.isChanged =
    data.shippingType === SHIPPING_TYPE.STANDARD ||
    data.shippingType === SHIPPING_TYPE.EXACT_DAY ||
    data.shippingType === SHIPPING_TYPE.SAME_DAY
      ? data.formChanged
      : true;

  patchData.address.id = data.streetAddressId.trim().length
    ? data.streetAddressId
    : patchData.address.id;

  patchData.deliveryMethod.id = (data.shippingFee[
    data.shippingType
  ] as IFee).id;
  patchData.deliveryMethod.fee = (data.shippingFee[
    data.shippingType
  ] as IFee).fee;
  patchData.deliveryMethod.shippingType = data.shippingType;
  patchData.deliveryMethod.characteristics = [
    {
      name: 'deliveryDate',
      value: data.selectedDeliveryDate
    }
  ];

  return patchData;
};

export const enableShippingType = (data: IShippingType): shippingType[] => {
  const shippingEnabled: shippingType[] = [];

  Object.keys(data).forEach(item => {
    if (item === 'deliverToAddress') {
      const options = data[item].deliveryOptions;
      Object.keys(options).forEach(value => {
        if (options[value].show) {
          shippingEnabled.push(value as SHIPPING_TYPE);
        }
      });
    } else if (data[item].show) {
      shippingEnabled.push(item as SHIPPING_TYPE);
    }
  });

  return shippingEnabled;
  // tslint:disable-next-line:max-file-line-count
};
