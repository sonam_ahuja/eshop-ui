// tslint:disable:max-file-line-count

import { DELIVERY_OPTIONS, SHIPPING_TYPE } from '@checkout/store/enums';
import { IBasketItem, ICartSummary } from '@basket/store/types';

export type deliveryOptionType =
  | DELIVERY_OPTIONS.DELIVER_TO_ADDRESS
  | DELIVERY_OPTIONS.PICK_UP_AT_STORE
  | DELIVERY_OPTIONS.PARCEL_LOCKER
  | DELIVERY_OPTIONS.PICK_UP_POINTS;

export type shippingType =
  | SHIPPING_TYPE.STANDARD
  | SHIPPING_TYPE.EXACT_DAY
  | SHIPPING_TYPE.PARCEL_LOCKER
  | SHIPPING_TYPE.PICK_UP_POINT
  | SHIPPING_TYPE.PICK_UP_STORE
  | SHIPPING_TYPE.SAME_DAY;

export interface IShippingAddressParams {
  isChanged?: boolean;
  address: IShippingAddress;
  shippingType: deliveryOptionType;
}

export interface IShippingFeeRequest {
  address: IAddress;
  shippingTypes: shippingType[];
}

interface IAddress {
  id?: string;
  streetNumber?: string;
  address?: string;
  city?: string;
  postCode?: string;
  flatNumber?: string;
}
export interface IShippingFeeResponse {
  standard?: IFee;
  withinTime?: IFee;
  pickADate?: IFee;
  pickUpStore?: IFee;
  parcelLocker?: IFee;
  pickUpPoints?: IFee;
}
export interface IFee {
  cartSummary: ICartSummary[];
  cartItems: IBasketItem[];
  id: string;
  fee: number;
  deliveryDate?: string;
  name?: string;
  isDefault: boolean;
  productId: string;
}

export interface IShippingAddressResponse {
  sameAsPersonalInfo: boolean;
  shippingAddress: IShippingAddress[];
  deliveryMethod: IDeliveryMethod;
}

export interface IShippingState {
  shippingForm: IShippingForm;
  sameAsPersonalInfo: boolean;
  shippingAddress: IShippingAddress[];
  shippingFee: number | null;
  shippingType: shippingType;
  deliveryOptions: deliveryOptionType;
  autoCompleteLoader: boolean;
  pickupInfo: {
    selectedStoreAddress: ISelectedStoreAddress | null;
    pickUpPersonInfo: IPickupPersonInfo | null;
  };
}
export interface IPickUpAddress {
  id: string;
  streetNumber: string;
  address: string;
  deliverNote: string;
  isDefault: boolean;
}

export interface INearbyStore {
  id?: string;
  name: string;
  address: string;
  distance: number;
  lat: number;
  long: number;
}

export interface IPickupPersonInfo {
  samePerson: boolean;
  otherUser: {
    firstName: string;
    lastName: string;
    IdNumber: number;
    phoneNumber: IPhoneNumber;
  };
}

export interface IPhoneNumber {
  id: string;
  value: string;
}
export interface ILocationSuggestions {
  name: string;
  lat: string;
  lng: string;
}

export interface IShippingAddress {
  id?: string;
  streetNumber: string;
  city: string;
  postCode: string;
  flatNumber: string;
  address: string;
  deliveryNote: string;
  unit: string;
  state?: string;
  country?: string;
  name?: string;
  geoX?: string;
  geoY?: string;
}

export interface ISearchAddressesResponse {
  id: string;
  streetNumber: string;
  city: string;
  postCode: string;
  flatNumber: string;
  address: string;
  deliveryNote: string;
  autoSuggestAddress: string;
  geoX?: string;
  geoY?: string;
  title?: string;
}

export interface IShippingAddressAndNote {
  streetNumber: string;
  address: string;
  deliveryNote?: string;
}

export interface IAddressState {
  value: IShippingAddress[];
  isValid: boolean;
  validationMessage: string;
  id?: string;
}
export interface IShippingForm {
  id?: string;
  streetAddress: IPersonalInfoValue;
  city: IPersonalInfoValue;
  notes: IPersonalInfoValue;
  postCode: IPersonalInfoValue;
  streetNumber: IPersonalInfoValue;
  unit: IPersonalInfoValue;
  isDefault: boolean;
  enableProceedButton: boolean;
  isFormChanged: boolean;
  isPersonalInfoPrefilled: boolean;
}

export interface IInputKeyValue {
  key: string;
  value: string | IShippingAddress[];
  isValid?: boolean;
  validationMessage?: string;
}

export interface IPersonalInfoValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
  id?: string;
}

export interface IShippingFormState {
  id: string;
  isDefault: boolean;
  isFormChanged: boolean;
  streetAddress: IValue;
  streetNumber: IValue;
  city: IValue;
  postCode: IValue;
  unit: IValue;
  notes: IValue;
}

export interface IValue {
  value: string;
  isValid: boolean;
  validationMessage: string;
}

export interface IShippingFee {
  standard?: IFee;
  withinTime?: IFee;
  pickADate?: IFee;
  pickUpStore?: IFee;
  parcelLocker?: IFee;
  pickUpPoints?: IFee;
}

export interface IShippingAddressState {
  // tslint:disable-next-line:no-any
  [key: string]: any;
  streetAddress: string;
  streetAddressId: string;
  streetNumber: string;
  flatNumber: string;
  city: string;
  postCode: string;
  unit: string;
  deliveryNote: string;
  sameAsPersonalInfo: boolean;
  shippingType: shippingType;
  shippingFee: IShippingFee;
  buttonEnable: boolean;
  formChanged: boolean;
  loading: boolean;
  dataReceived: boolean;
  deliveryOptions: deliveryOptionType;
  savedDeliveryMethod: shippingType;
  dummyState: IDummyState;

  // below are added for other delivery addresses
  pickUpLocations: ISelectedStoreAddress[] | null;
  isPickUpLocationSelected: boolean;
  selectedStoreAddress: ISelectedStoreAddress | null;
  isOtherPersonPickup: boolean;
  pickUpPersonInfo: IPickupPersonInfo | null;
  pickupLocationInfo: ISelectedStoreAddress | [];
  pickADateList: string[] | number[];
  selectedDeliveryDate: string | number;
}

export interface ISelectedStoreAddress {
  id?: string;
  streetNumber: string;
  address: string;
  flatNumber: string;
  postCode: string;
  city: string;
  state: string;
  country: string;
  distance: string;
  distanceUnit: string;
  name: string;
  geoX: string;
  geoY: string;
  openHours?: IOpenHours[];
}

export interface IOpenHours {
  day: string[];
  hourPeriod: IHourPeriod[];
}

export interface IHourPeriod {
  startHour: string;
  endHour: string;
}

export interface IPickupPersonInfo {
  samePerson: boolean;
  otherUser: {
    firstName: string;
    lastName: string;
    IdNumber: number;
    phoneNumber: IPhoneNumber;
  };
}

export interface IDummyState {
  streetAddress?: string;
  streetAddressId?: string;
  streetNumber?: string;
  flatNumber?: string;
  city?: string;
  postCode?: string;
  unit?: string;
  deliveryNote?: string;
  state?: string;
  country?: string;
  distance?: string;
  distanceUnit?: string;
  name?: string;
  geoX?: string;
  geoY?: string;
  openHours?: IOpenHours[];
}

export interface IFormFieldValue {
  [key: string]: string | boolean | number;
  value: string;
  isValid: boolean;
  validationMessage: string;
  colDesktop: number;
  colMobile: number;
}

export interface IShippingFormField {
  streetAddress?: IFormFieldValue;
  streetNumber?: IFormFieldValue;
  flatNumber?: IFormFieldValue;
  city?: IFormFieldValue;
  postCode?: IFormFieldValue;
  unit?: IFormFieldValue;
  deliveryNote?: IFormFieldValue;
  id?: IFormFieldValue;
}

export interface IUpdateShippingAddress {
  key: string;
  value: string;
  isValid: boolean;
  validationMessage: string;
}

export interface IPostShippingAddressPayload {
  address: IPostAddress;
  deliveryMethod: IDeliveryMethod;
}

export interface IPostAddress {
  streetNumber: string;
  address: string;
  city: string;
  unit: string;
  postCode: string;
  flatNumber: string;
  deliveryNote: string;
  state?: string;
  country?: string;
  name?: string;
  distance?: string;
  distanceUnit?: string;
  geoX?: string;
  geoY?: string;
}

export interface IPatchShippingAddressPayload {
  isChanged: boolean;
  address: IPatchAddress;
  deliveryMethod: IDeliveryMethod;
}
export interface IDeliveryMethod {
  id: string;
  fee: number;
  shippingType: shippingType;
  characteristics: IShippingCharacteristics[];
}

export interface IShippingCharacteristics {
  name: string;
  value: string | number;
}

export interface IPatchAddress {
  streetNumber?: string;
  address?: string;
  id?: string;
  city?: string;
  postCode?: string;
  unit?: string;
  flatNumber?: string;
  deliveryNote?: string;
  state?: string;
  country?: string;
  name?: string;
  distance?: string;
  distanceUnit?: string;
  geoX?: string;
  geoY?: string;
}

export interface IAddressResponse {
  streetNumber?: string;
  address?: string;
  id: string;
  city?: string;
  postCode?: string;
  unit?: string;
  flatNumber?: string;
  deliveryNote?: string;
  state?: string;
  country?: string;
  name?: string;
  distance?: string;
  distanceUnit?: string;
  geoX?: string;
  geoY?: string;
  openHours?: IOpenHours[];
}

export interface IPostShippingAddressResponse {
  sameAsPersonalInfo: boolean;
  shippingAddress: IAddressResponse[];
  deliveryMethod: IDeliveryMethod;
}

export interface ILatLong {
  geoX: string;
  geoY: string;
}
// API types for get store/ get pick up points /  pick up at store
export interface IPickUpLocationPayload extends ILatLong {
  shippingType: shippingType;
}
export interface IPickUpPointsRequest extends IPickUpLocationPayload {
  distance: string;
  distanceUnit: string;
  page: string;
  size: string;
}

export interface IDateListItem {
  id: string | number;
  title: string;
  [key: string]: string | number;
}

export interface IListItem {
  id: string | number;
  title: string;
  [key: string]: string | number;
}
