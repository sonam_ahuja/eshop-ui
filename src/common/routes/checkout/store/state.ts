import { ICheckoutState } from '@checkout/store/types';
import { CREDIT_SCORE_CODE } from '@checkout/store/enums';
export default (): ICheckoutState => ({
  loading: false,
  isAddressFetched: false,
  error: null,
  activeCheckoutStep: 0,
  completedCheckoutStep: 0,
  sendSMSNotification: false,
  creditCheckRequired: false,
  isUpfrontPrice: false,
  isMonthlyPrice: false,
  cartId: '',
  sideAppShell: true,
  mainAppShell: true,
  activeCreditStep: CREDIT_SCORE_CODE.REQUEST_SUPPORT_CALL,
  openCreditModal: false,
  creditScoreResponse: {
    code: CREDIT_SCORE_CODE.REQUEST_SUPPORT_CALL,
    status: 'request support call'
  },
  creditCheckLoading: false,
  isCreditCheckRequired: false,
  openTnCModal: false,
  termsAndConditionData: { termsAndConditions: [], links: [] }
});
