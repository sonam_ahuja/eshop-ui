import { IPersonalInfoState } from '@checkout/store/personalInfo/types';
import {
  IShippingAddressResponse,
  IShippingAddressState
} from '@checkout/store/shipping/types';
import { IBillingState } from '@checkout/store/billing/types';
import { IOrderReviewState } from '@checkout/store/orderReview/types';
import {
  ICreditCheckState,
  IPaymentState
} from '@checkout/store/payment/types';
import { BILLING_TYPE, CREDIT_SCORE_CODE } from '@checkout/store/enums';
import { INumberPortingState } from '@checkout/store/numberPorting/types';

export interface IResponse {
  id: number;
}

export interface IState {
  creditCheck: ICreditCheckState;
  shippingInfo: IShippingAddressState;
  payment: IPaymentState;
  billing: IBillingState;
  personalInfo: IPersonalInfoState;
  checkout: ICheckoutState;
  orderSummary: IOrderSummaryState;
  orderReview: IOrderReviewState;
  numberPorting: INumberPortingState;
}

export interface ICheckoutState {
  loading: boolean;
  isAddressFetched: boolean;
  error: Error | null;
  sendSMSNotification: boolean;
  creditCheckRequired: boolean;
  completedCheckoutStep: number;
  activeCheckoutStep: number;
  isUpfrontPrice: boolean;
  isMonthlyPrice: boolean;
  cartId: string;
  sideAppShell: boolean;
  mainAppShell: boolean;
  activeCreditStep: CREDIT_SCORE_CODE;
  creditScoreResponse: ICreditScoreResponse;
  openCreditModal: boolean;
  creditCheckLoading: boolean;
  isCreditCheckRequired: boolean;
  openTnCModal: boolean;
  termsAndConditionData: ITermsAndConditionResponse;
}

export interface ICreditScoreResponse {
  code: CREDIT_SCORE_CODE;
  status: string;
}
export interface ICreditQualifyResponse {
  status: boolean;
}

export interface IOrderSummaryState {
  orderConfirmation: IOrderConfirmation;
  orderTracker: IOrderTracker;
}

export interface IOrderDetail {
  emailId: string;
  orderId: string;
  contactNumber: string;
}

export interface IOrderShippingDetail {
  estimateDelivery: string;
  shippingType: string;
  address: string;
}

export interface IOrderConfirmation {
  orderDetail: IOrderDetail;
  shippingDetail: IOrderShippingDetail;
}

export interface ITrackingDetails {
  time: string;
  message: string;
  active: boolean;
}

export interface IOrderTracker {
  orderId: string;
  shippingAddress: string;
  trackingDetails: ITrackingDetails[];
}

export interface IAddressResponse {
  personalInfo?: IPersonalInfoResponse;
  billingInfo?: IBillingInfoResponse;
  shippingInfo?: IShippingAddressResponse;
}

export interface IPersonalInfoResponse {
  firstName: string;
  id: string;
  pesel: string;
  lastName: string;
  dob: string;
  email: IPersonalInfoValue;
  phoneNumber: IPersonalInfoValue;
  mainAddress: IMainAddress[];
  idNumber: string;
  oibNumber: string;
  company: string;
  isSmsEnabled: boolean;
  smsNotificationName: string;
  smsNotificationDesc: string;
}

export interface IMainAddress {
  id: string;
  streetNumber: string;
  address: string;
  flatNumber: string;
  city: string;
  postCode: string;
  deliveryNote: string;
  isDefault: boolean;
}

export interface IBillingInfoResponse {
  billingAddress: IBillingAddress[];
  sameAsPersonalInfo: boolean;
  sameAsShippingInfo: boolean;
  typeOfBill: BILLING_TYPE;
}

export interface IBillingAddress {
  id?: string;
  streetNumber: string;
  city: string;
  postCode: string;
  flatNumber: string;
  address: string;
  deliveryNote: string;
  unit: string;
}

export interface IShippingInfoResponse {
  sameAsPersonalInfo: boolean;
  correspondenceAddress: ICorrespondenceAddress[];
  shippingFee: string;
  shippingType: string;
  pickUpInfo: {
    selectedStoreAddress?: ISelectedStoreAddress;
    pickUpPersonInfo: {
      samePerson: boolean;
      otherUser?: IOtherUser;
    };
  };
}

export interface ICorrespondenceAddress {
  streetAddress: string;
  deliveryNote: string;
  id: number;
}

export interface ISelectedStoreAddress {
  lat: number;
  lon: number;
  name: string;
  id: number;
  streetAddress: string;
}

export interface IOtherUser {
  firstName: string;
  lastName: string;
  idNumber: string;
  phoneNumber: string;
}

export interface IPersonalInfoValue {
  id: string;
  value: string;
}

export interface ITermsAndConditionResponse {
  termsAndConditions: ITnC[];
  links: ILink[];
}

export interface ITnC {
  id: string;
  name: string;
  description: string;
  version: string;
}

export interface ILink {
  name: string;
  relativeUrl?: string;
  absoluteUrl?: string;
}
