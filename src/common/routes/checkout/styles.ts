import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledFooter } from '@src/common/components/FooterWithTermsAndConditions/styles';

export const CheckoutBody = styled.div<{}>`
  display: flex;
  flex: 1;
  flex-direction: column;
  .styledPaymentMethods {
    bottom: 55px !important;
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    flex-direction: row;
  }
`;

export const StyledCheckoutSteps = styled.div`
  /* height of the progressStepper */
  padding-top: 80px;
  width: 100%;
  background: ${colors.coldGray};

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    position: relative;
    padding-top: 7rem;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-top: 0;
  }

  ${StyledFooter} {
    &.footerCheckout {
      background: ${colors.charcoalGray};
      font-size: 0.75rem;
      line-height: 1rem;
      justify-content: space-between;
      align-items: center;
      .copyText {
        letter-spacing: 0.24px;
      }

      @media (min-width: ${breakpoints.tabletPortrait}px) {
        padding: 1rem 2.25rem;
      }

      @media (min-width: ${breakpoints.tabletLandscape}px) {
        &.footerCheckout {
          display: none;
        }
      }
    }
  }
`;
