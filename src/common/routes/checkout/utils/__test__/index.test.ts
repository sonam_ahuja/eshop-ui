import {
  getConfigState,
  getOrderReviewState,
  isCartItemsAndSummaryAvailable
} from '@checkout/utils';
import updateReadOnlyConfig from '@checkout/utils/updateReadOnlyFieldConfig';
import store from '@src/common/store';

describe('<utils />', () => {
  test('getConfigState test', () => {
    expect(getConfigState()).toBeDefined();
  });

  test('getOrderReviewState test', () => {
    const state = store.getState();
    expect(getOrderReviewState(state)).toBeDefined();
  });
  test('isCartItemsAndSummaryAvailable test', () => {
    const state = store.getState().basket;
    expect(isCartItemsAndSummaryAvailable(state)).toBeDefined();
  });
  test('isCartItemsAndSummaryAvailable test', () => {
    const contact = {};
    expect(updateReadOnlyConfig(contact)).toBeDefined();
  });
});
