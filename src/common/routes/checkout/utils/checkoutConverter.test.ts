import { convertPersonalInfo, mainAddressToAddress } from './checkoutConverter';

describe('<utils />', () => {
  test('mainAddressToAddress test', () => {
    const mainAddress = [
      {
        id: '187040114',
        streetNumber: '1',
        // tslint:disable-next-line:no-duplicate-string
        address: 'Tim Tim Streetadsfsdafwqwqw',
        flatNumber: '1232',
        city: 'Bonn',
        postCode: '53113',
        deliveryNote: '',
        isDefault: true
      }
    ];

    const personalInfo = {
      id: '5fba0646-f499-46b9-862e-6a09f2e39b',
      firstName: 'guest_bhar',
      lastName: 'lastname',
      dob: '1991-08-23',
      pesel: '22',
      idNumber: '123',
      oibNumber: '123',
      company: '1234',
      email: {
        id: '185226679',
        value: 'mukul@telelkom.com'
      },
      phoneNumber: {
        id: '137737631',
        value: '168'
      },
      mainAddress: [
        {
          id: '187040114',
          streetNumber: '1',
          // tslint:disable-next-line:no-duplicate-string
          address: 'Tim Tim Streetadsfsdafwqwqw',
          flatNumber: '1232',
          city: 'Bonn',
          postCode: '53113',
          deliveryNote: '',
          isDefault: true
        }
      ],
      isSmsEnabled: false,
      smsNotificationName: '',
      smsNotificationDesc: ''
    };

    const personalInfoWithoutAddress = {
      id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      firstName: 'guest_bhar',
      lastName: 'lastname',
      dob: '1991-08-23',
      pesel: '2',
      idNumber: '123',
      oibNumber: '123',
      company: '1234',
      email: {
        id: '185226679',
        value: 'mukul@telelkom.com'
      },
      phoneNumber: {
        id: '137737631',
        value: '168'
      },
      mainAddress,
      isSmsEnabled: false,
      smsNotificationName: '',
      smsNotificationDesc: ''
    };

    const emptyData = {
      id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      firstName: '',
      lastName: '',
      dob: '',
      pesel: '',
      idNumber: '',
      oibNumber: '',
      company: '',
      email: {
        id: '',
        value: ''
      },
      phoneNumber: {
        id: '',
        value: ''
      },
      mainAddress: [
        {
          id: '',
          streetNumber: '',
          address: '',
          flatNumber: '',
          city: '',
          postCode: '',
          deliveryNote: '',
          isDefault: true
        }
      ],
      isSmsEnabled: false,
      smsNotificationName: '',
      smsNotificationDesc: ''
    };
    expect(mainAddressToAddress(personalInfo.mainAddress)).toBeDefined();
    expect(convertPersonalInfo(personalInfo)).toBeDefined();
    expect(convertPersonalInfo(emptyData)).toBeDefined();
    expect(convertPersonalInfo(personalInfoWithoutAddress)).toBeDefined();
    expect(
      convertPersonalInfo({
        ...personalInfoWithoutAddress,
        // tslint:disable-next-line:no-any
        mainAddress: '' as any
      })
    ).toBeDefined();

    // tslint:disable-next-line:no-any
    expect(convertPersonalInfo({} as any)).toBeDefined();
  });
});
