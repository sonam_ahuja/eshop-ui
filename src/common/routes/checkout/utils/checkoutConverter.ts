import { IMainAddress, IPersonalInfoResponse } from '@checkout/store/types';
import { IPersonalInfoConverterResponse } from '@checkout/store/personalInfo/types';
import initialPersonalInfoState from '@checkout/store/personalInfo/state';
import { isObj } from '@src/common/utils';

export function mainAddressToAddress(
  mainAddress: IMainAddress[]
): IMainAddress {
  const {
    id,
    streetNumber,
    address,
    flatNumber,
    city,
    postCode,
    deliveryNote,
    isDefault
  } = mainAddress[0];

  return {
    id,
    streetNumber,
    address,
    flatNumber,
    city,
    postCode,
    deliveryNote,
    isDefault
  };
}

export const convertPersonalInfo = (
  personalInfoResponse?: IPersonalInfoResponse
): IPersonalInfoConverterResponse => {
  if (personalInfoResponse && Object.keys(personalInfoResponse).length) {
    const mainAddress = personalInfoResponse.mainAddress;

    const obj = {
      id: personalInfoResponse.id,
      formFields: {
        firstName: personalInfoResponse.firstName || '',
        pesel: personalInfoResponse.pesel || '',
        lastName: personalInfoResponse.lastName || '',
        dob: personalInfoResponse.dob || '',
        idNumber: personalInfoResponse.idNumber || '',
        oibNumber: personalInfoResponse.oibNumber || '',
        company: personalInfoResponse.company || '',
        emailId:
          (personalInfoResponse.email && personalInfoResponse.email.id) || '',
        email:
          (personalInfoResponse.email && personalInfoResponse.email.value) ||
          '',
        phoneNumberId:
          (personalInfoResponse.phoneNumber &&
            personalInfoResponse.phoneNumber.id) ||
          '',
        phoneNumber:
          (personalInfoResponse.phoneNumber &&
            personalInfoResponse.phoneNumber.value) ||
          ''
      },
      isDefault: isObj(mainAddress) && mainAddress[0].isDefault,
      isSmsEnabled: personalInfoResponse.isSmsEnabled,
      smsNotificationName: personalInfoResponse.smsNotificationName || '',
      smsNotificationDesc: personalInfoResponse.smsNotificationDesc || ''
    };

    if (isObj(mainAddress)) {
      return {
        ...obj,
        formFields: {
          ...obj.formFields,
          streetNumber: mainAddress[0].streetNumber || '',
          city: mainAddress[0].city || '',
          flatNumber: mainAddress[0].flatNumber || '',
          postCode: mainAddress[0].postCode || '',
          addressId: mainAddress[0].id || '',
          deliveryNote: mainAddress[0].deliveryNote || '',
          address: mainAddress[0].address || ''
        }
      };
    }

    return obj;
  }

  return initialPersonalInfoState();
};
