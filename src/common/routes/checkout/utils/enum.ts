export enum ADDRESS_GEO_TYPE {
  LOCALITY = 'locality',
  ADMIN_AREA_LEVEL_1 = 'administrative_area_level_1',
  ADMIN_AREA_LEVEL_2 = 'administrative_area_level_2',
  COUNTRY = 'country',
  SUB_LOCALITY = 'sublocality',
  STREET_NUMBER = 'street_number',
  POSTAL_CODE = 'postal_code'
}
