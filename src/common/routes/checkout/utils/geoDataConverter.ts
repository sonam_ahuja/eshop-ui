import {
  IGeocoderAddressComponent,
  IGeocoderResult
} from '@src/common/types/common/googleMap';
import { ISearchAddressesResponse } from '@checkout/store/shipping/types';

import { ADDRESS_GEO_TYPE } from './enum';

export const fetchGeoMetadata = (
  placeId: string,
  address: string
): Promise<ISearchAddressesResponse> => {
  const geocoder = new google.maps.Geocoder();

  return new Promise(resolve => {
    geocoder.geocode(
      {
        placeId
      },
      (geoResult: IGeocoderResult[]) => {
        let transformedObj: ISearchAddressesResponse;
        if (geoResult && geoResult.length) {
          transformedObj = transformGeoData(geoResult[0]);
          transformedObj.address = address;
          resolve(transformedObj);
        }
      }
    );
  });
};

export const transformGeoData = (
  geoData: IGeocoderResult
): ISearchAddressesResponse => {
  const geoTransformedObj: ISearchAddressesResponse = {
    id: '',
    streetNumber: '',
    city: '',
    postCode: '',
    flatNumber: '',
    address: '',
    deliveryNote: '',
    autoSuggestAddress: '',
    geoX: '',
    geoY: ''
  };

  if (geoData.address_components && geoData.address_components.length) {
    geoData.address_components.forEach(
      (addressItem: IGeocoderAddressComponent) => {
        if (addressItem && addressItem.types && addressItem.types.length) {
          addressItem.types.forEach((itemType: string) => {
            switch (itemType) {
              case ADDRESS_GEO_TYPE.STREET_NUMBER:
                geoTransformedObj.streetNumber = addressItem.long_name;
                break;
              case ADDRESS_GEO_TYPE.POSTAL_CODE:
                geoTransformedObj.postCode = addressItem.long_name;
                break;
              case ADDRESS_GEO_TYPE.LOCALITY:
                geoTransformedObj.city = addressItem.long_name;
                break;
              default:
                break;
            }
          });
        }
      }
    );
  }

  if (geoData.geometry && geoData.geometry.location) {
    geoTransformedObj.geoX = geoData.geometry.location.lat();
    geoTransformedObj.geoY = geoData.geometry.location.lng();
  }

  return geoTransformedObj;
};
