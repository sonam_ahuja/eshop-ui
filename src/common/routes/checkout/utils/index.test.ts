import { getGlobalConfig } from '@checkout/utils';

describe('<utils />', () => {
  test('getGlobalConfig test', () => {
    expect(getGlobalConfig()).toBeDefined();
  });
});
