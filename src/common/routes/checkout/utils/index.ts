import { IBasketState } from '@basket/store/types';
import { RootState } from '@src/common/store/reducers';
import store from '@src/common/store';

export const isCartItemsAndSummaryAvailable = (
  basket: IBasketState
): boolean => {
  return (
    basket.basketItems &&
    basket.basketItems.length > 0 &&
    Array.isArray(basket.cartSummary) &&
    basket.cartSummary.length > 0
  );
};

export const getGlobalConfig = () => {
  const state: RootState = store.getState();

  return state.configuration.cms_configuration.global;
};

export const getOrderReviewState = (state: RootState) => {
  return state.checkout.orderReview;
};

export const getConfigState = () => {
  const state: RootState = store.getState();

  return state.configuration;
};
