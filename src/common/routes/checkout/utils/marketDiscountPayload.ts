import {
  IMarketDiscountRequest,
  IOrderReviewState
} from '@checkout/store/orderReview/types';

export default function createMarketDiscountPayload(
  orderReviewState: IOrderReviewState
): IMarketDiscountRequest {
  const { agreementResponse } = orderReviewState;

  const discountConsent = agreementResponse.map(agreement => {
    return {
      id: agreement.id,
      selected: agreement.partyPrivacyProfileTypeCharacteristics[0].isAuthorized
    };
  });

  return {
    consents: discountConsent
  };
}
