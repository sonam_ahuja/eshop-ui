import {
  IConfigurationResponse,
  IFields
} from '@src/common/store/types/configuration';
import { IAddressResponse, IBillingAddress } from '@checkout/store/types';
import { convertPersonalInfo } from '@checkout/utils/checkoutConverter';
import { logError } from '@src/common/utils';

import { IPersonalInfoFormFields } from '../store/personalInfo/types';
import { IShippingForm } from '../store/shipping/types';

import { getConfigState } from '.';

export default function updateReadOnlyConfig(
  contacts: IAddressResponse
): IConfigurationResponse {
  const {
    personalInfo: personalInfoApiResponse,
    shippingInfo,
    billingInfo
  } = contacts;

  const config = getConfigState().cms_configuration;

  try {
    if (!contacts || (typeof contacts === 'object' && Object.keys(contacts).length === 0)) {
      return config;
    }
    const checkoutConfig = config.modules.checkout;
    const personalInfoFormConfig = checkoutConfig.form.fields;
    const shippingInfoFormConfig = checkoutConfig.shipping.form.fields;
    const billingInfoFormConfig = checkoutConfig.billingInfo.form.fields;

    const personalInfo = convertPersonalInfo(personalInfoApiResponse);

    if (personalInfo) {
      config.modules.checkout.form.fields = validateReadOnly(
        personalInfoFormConfig,
        personalInfo.formFields
      );
    }

    if (shippingInfo && shippingInfo.shippingAddress[0]) {
      config.modules.checkout.shipping.form.fields = validateReadOnly(
        shippingInfoFormConfig,
        shippingInfo.shippingAddress[0]
      );
    }

    if (billingInfo && billingInfo.billingAddress[0]) {
      config.modules.checkout.billingInfo.form.fields = validateReadOnly(
        billingInfoFormConfig,
        billingInfo.billingAddress[0]
      );
    }

    return config;
  } catch (error) {
    logError(error);

    return config;
  }
}
// tslint:disable: no-ignored-return
export function validateReadOnly(
  config: IFields,
  responseData: IPersonalInfoFormFields | IBillingAddress | IShippingForm
): IFields {
  Object.keys(responseData).map(key => {
    if (config[key] && config[key].readOnly && !responseData[key]) {
      config[key].readOnly = false;
    }
  });

  return config;
}
