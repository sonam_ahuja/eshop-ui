import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Back from '.';

describe('<Back />', () => {
  const props = {};

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Back {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
