import React from 'react';
import { Anchor, Icon } from 'dt-components';
import appConstants from '@src/common/constants/appConstants';

import { StyledBackIconWrap } from './style';

export interface IProps {
  onBackClick?(): void;
}

const Back = (props: IProps) => {
  return (
    <StyledBackIconWrap className='backArrow'>
      <Anchor
        hreflang={appConstants.LANGUAGE_CODE}
        onClickHandler={props.onBackClick}
      >
        <Icon size='inherit' name='ec-arrow-left' />
      </Anchor>
    </StyledBackIconWrap>
  );
};

export default Back;
