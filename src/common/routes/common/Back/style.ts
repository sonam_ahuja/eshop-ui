import styled from 'styled-components';

export const StyledBackIconWrap = styled.div`
  transition: opacity 0.2s linear 0s;

  a {
    font-size: 3.75rem;
  }

  &:hover {
    opacity: 0.8;
  }
`;
