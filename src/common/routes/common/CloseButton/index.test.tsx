import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import CloseButton from '.';

describe('<CloseButton />', () => {
  const props = {};

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <CloseButton {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
