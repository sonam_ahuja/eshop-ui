import styled from 'styled-components';

export const StyledFullScreenModal = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  min-height: 100%;

  .backArrow {
    position: absolute;
    left: 2.5rem;
    top: 2.5rem;
  }
  .closeIconWrap {
    position: absolute;
    right: 2.5rem;
    top: 2.5rem;
  }
`;
