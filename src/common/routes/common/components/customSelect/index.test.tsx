import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Component from '.';

describe('<CustomSelect />', () => {
  test('should render properly', () => {
    const props = {
      // tslint:disable-next-line:no-any
      type: 'simpleSelect' as any,
      isOpen: false,
      selectedItem: null,
      onClick: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
