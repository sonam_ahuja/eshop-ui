import React, { FunctionComponent } from 'react';

import { StyledAppSelect } from './styles';
interface IListItem {
  id: string | number;
  title: string;
}
interface IProps {
  type: 'simpleSelect';
  isOpen: boolean;
  selectedItem: IListItem | null;
  onClick?(event: React.MouseEvent<HTMLInputElement, MouseEvent>): void;
}
const Plans: FunctionComponent<IProps> = (props: IProps) => {
  const { onClick, selectedItem } = props;

  return (
    <StyledAppSelect onClick={onClick}>
      <div>{selectedItem}</div>
    </StyledAppSelect>
  );
};

export default Plans;
