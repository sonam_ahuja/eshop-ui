import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import StripPanel, { IProps } from '@src/common/routes/common/strip';
import { StaticRouter } from 'react-router-dom';

describe('<StripPanel />', () => {
  const props: IProps = {
    initialMessage: 'string',
    basketImages: [],
    basketItems: 4
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <StripPanel {...props} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should update state properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <StripPanel {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('StripPanel').instance() as StripPanel).onCloseDrawer();
    (component.find('StripPanel').instance() as StripPanel).onStripClick();
    (component
      .find('StripPanel')
      .instance() as StripPanel).renderImageonStrip();
    (component.find('StripPanel').instance() as StripPanel).renderDrawer();
  });

  test('should render properly basketstrip ', () => {
    const newProps = { ...props, initialMessage: '', basketItems: 0 };
    newProps.basketImages = ['asdda', 'sadaae'];
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <StripPanel {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly basketstrip withiout image', () => {
    const newProps = { ...props, initialMessage: '', basketItems: 0 };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <StripPanel {...newProps} />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
