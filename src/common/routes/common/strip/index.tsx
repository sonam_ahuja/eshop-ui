import React, { ReactNode } from 'react';
import { AppFlowPanel } from '@src/common/components/FlowPanel/styles';
import BasketWrap from '@basket/BasketWrap/index';

import { StyledStrip } from './styles';

export interface IProps {
  initialMessage?: string;
  basketImages?: string[];
  basketItems?: number;
}

export interface IState {
  open: boolean;
}

class StripPanel extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      open: false
    };
  }
  onCloseDrawer = (): void => {
    this.setState({ open: false });
  }

  onStripClick = (): void => {
    this.setState({ open: true });
  }

  renderImageonStrip = (): ReactNode => {
    const { basketImages } = this.props;

    return (
      basketImages &&
      basketImages.map((imageUrl: string) => {
        return <img key={imageUrl} src={imageUrl} />;
      })
    );
  }
  renderStrip = (): ReactNode => {
    const { initialMessage, basketImages } = this.props;
    if (this.state.open) {
      return this.renderDrawer();
    }

    return (
      <StyledStrip onClick={() => this.onStripClick()}>
        {basketImages && basketImages.length ? (
          this.renderImageonStrip()
        ) : (
          <span className='text-wrap'>
            {initialMessage || 'Start Shopping'}
          </span>
        )}
      </StyledStrip>
    );
  }

  renderDrawer = (): ReactNode => {
    return (
      <AppFlowPanel
        isOpen={this.state.open}
        onClose={() => this.onCloseDrawer()}
      >
        {this.renderBasket()}
      </AppFlowPanel>
    );
  }

  renderBasket = (): ReactNode => {
    const { basketItems } = this.props;

    return basketItems ? <BasketWrap showMoreFlag={true} /> : null;
    // <EmptyBasketComponent translation={translation} />
  }

  render(): React.ReactNode {
    return <>{this.renderStrip()}</>;
  }
}

export default StripPanel;
