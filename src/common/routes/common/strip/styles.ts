import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledStrip = styled.div`
  display: none;
  @media (min-width: ${breakpoints.desktop}px) {
    position: sticky;
    display: block;
    top: 0;
    width: 4.75rem;
    float: left;
    height: 100vh;
    display: flex;
    background: ${colors.cloudGray};
    padding: 1.25rem 1.5rem;
    letter-spacing: -1.2px;
    right: 0;
    .text-wrap {
      font-weight: 900;
      color: ${colors.mediumGray};
      writing-mode: tb-rl;
      align-items: center;
      justify-content: center;
      font-size: 2rem;
    }
  }
`;
