// tslint:disable-next-line:no-commented-code
// import styled from 'styled-components';
// import { colors } from '@src/common/variables';

// export const StyledHorizontalScrollRow = styled.div`
//   /* HORIZONTAL SCROLL START */
//   display: flex;
//   flex-wrap: nowrap;
//   /* Make this scrollable when needed */
//   overflow-x: auto;
//   /* We don't want vertical scrolling */
//   overflow-y: hidden;
//   /* Make an auto-hiding scroller for the 3 people using a IE */
//   -ms-overflow-style: -ms-autohiding-scrollbar;
//   /* For WebKit implementations, provide inertia scrolling */
//   -webkit-overflow-scrolling: touch;
//   /* We don't want internal inline elements to wrap */
//   /* white-space: nowrap; */
//   /* Remove the default scrollbar for WebKit implementations */

//   &::-webkit-scrollbar {
//     display: none;
//   }
//   /* HORIZONTAL SCROLL END */

//   .horizontalScrollRowInner {
//     display: flex;
//   }
// `;

// export const StyledVariantsWrap = styled.div`
//   /* > div = all variants */
//   > div {
//     width: 1.6rem;
//     height: 1.6rem;

//     &:after {
//       top: -9px;
//       right: -9px;
//       bottom: -9px;
//       left: -9px;
//     }
//   }
// `;

// export const StyledSelect = styled.div`
//   /* display: inline-flex; */
//   /* > div {
//     font-size: 2.25rem;
//     font-weight: 500;
//     line-height: 1.11;
//     color: ${colors.darkGray};

//     .caret {
//       margin-left: 13px;
//     }

//     .optionsList {
//       width: 320px;
//       height: 244px;
//       border-radius: 8px;
//       box-shadow: 0 8px 40px 0 rgba(0, 0, 0, 0.2);
//       background-color: #fff;
//       padding: 12px;
//     }
//   } */
// `;

// export const StyledBreadcrumbWrap = styled.div`
//   padding: 2rem 0 2.5rem;

// //   .breadcrumb {
// //     li {
// //       color: ${colors.mediumGray};
// //       &.active {
// //         color: ${colors.darkGray};
// //       }
// //     }
// //     li > div {
// //       display: flex;
// //       align-items: center;
// //     }
// //     i {
// //       margin: 0 1rem;
// //     }
// //   }
// // `;
