import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import ErrorPage from '.';

describe('<ErrorPage />', () => {
  test('should render properly', () => {
    const props = {
      title: 'title'
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ErrorPage {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with error', () => {
    const props = {
      title: 'title',
      error: {
        stack: 'Some error',
        name: 'error name',
        message: 'error message'
      }
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ErrorPage {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
