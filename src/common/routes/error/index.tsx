/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { ReactNode } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { IError } from '@server/types';

declare var __DEV__: boolean;

const ErrorH1 = styled.h1`
  font-weight: 400;
  color: #555;
`;

const ErrorPre = styled.pre`
  white-space: pre-wrap;
  text-align: left;
`;

interface IProps {
  error?: IError;
}
export default class ErrorPage extends React.Component<IProps> {
  static propTypes = {
    error: PropTypes.shape({
      name: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      stack: PropTypes.string.isRequired
    })
  };

  static defaultProps = {
    error: null
  };

  render(): ReactNode {
    if (__DEV__ && this.props.error) {
      return (
        <div>
          <ErrorH1>{this.props.error.name}</ErrorH1>
          <p>{this.props.error.message}</p>
          <ErrorPre>{this.props.error.stack}</ErrorPre>
        </div>
      );
    }

    return (
      <div>
        <ErrorH1>Error</ErrorH1>
        <ErrorPre>Sorry, a critical error occurred on this page.</ErrorPre>
      </div>
    );
  }
}
