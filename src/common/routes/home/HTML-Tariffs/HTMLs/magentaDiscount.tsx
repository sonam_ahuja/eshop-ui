// tslint:disable:max-line-length
// tslint:disable-next-line:no-big-function
export default function magentaDiscount(): string {
  return `
  <style>.clearfix:after {
    content: ' ';
    /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .hidden-lg {
    display: none;
  }
  .visible-lg {
    display: block;
  }
  .plans-container {
    background: #000;
    padding: 12rem 12.8rem;
  }
  .plans-container .plans-col {
    width: calc(50% - 2.5px);
    padding: 0 15px;
    float: left;
    background: #e20074;
    border-radius: 10px;
    height: 24rem;
    padding: 4rem 0 0 3rem;
    margin-right: 5px;
    display: flex;
  }
  .plans-container .plans-col .info-wrap {
    width: 60%;
    float: left;
    padding-right: 4rem;
  }
  .plans-container .plans-col .info-wrap .title {
    font-size: 1.5rem;
    color: #fff;
    line-height: 2rem;
    margin-bottom: 2rem;
  }
  .plans-container .plans-col .info-wrap p {
    font-size: 3rem;
    line-height: 3.5rem;
    color: #fff;
    font-weight: 600;
  }
  .plans-container .plans-col .img-wrap {
    width: 40%;
    float: right;
    height: 100%;
  }
  .plans-container .plans-col .img-wrap .img-fluid {
    width: 100%;
    height: 100%;
    border-bottom-right-radius: 10px;
  }
  .plans-container .plans-col:last-child {
    margin-right: 0;
    width: calc(50% - 2.5px);
  }
  .accordion-container {
    padding: 12rem 16rem;
  }
  .accordion-container.benefits-wrap {
    background: #e6e6e6;
  }
  .accordion-container.benefits-wrap .header {
    width: 80%;
    display: flex;
    flex-wrap: wrap;
  }
  .accordion-container.benefits-wrap .header .text {
    font-size: 5.5rem;
    line-height: 6rem;
    color: #e20074;
    padding-right: 2rem;
    font-weight: 600;
  }
  .accordion-container.benefits-wrap .header .text.dark {
    color: #000;
  }
  .accordion-container.benefits-wrap .header .icon {
    font-size: 5.5rem;
    line-height: 6rem;
    color: #000;
    padding-right: 1rem;
    font-weight: 600;
  }
  .accordion-container.benefits-wrap .inner-wrap {
    margin-top: 12rem;
  }
  .accordion-container.benefits-wrap .inner-wrap .cols-md-5 {
    width: 45%;
    float: left;
    padding-right: 13rem;
  }
  .accordion-container.benefits-wrap .inner-wrap .cols-md-7 {
    width: 55%;
    float: left;
  }
  .accordion-container.benefits-wrap .inner-wrap .list-box {
    display: flex;
    flex-wrap: wrap;
    flex-direction: inherit;
  }
  .accordion-container.benefits-wrap .inner-wrap .list-box .list {
    float: left;
    width: 50%;
    margin-bottom: 4rem;
  }
  .accordion-container.benefits-wrap
    .inner-wrap
    .list-box
    .list:nth-child(odd) {
    padding-right: 5rem;
  }
  .accordion-container.benefits-wrap .inner-wrap .list-box .list:last-child {
    margin-bottom: 0;
  }
  .accordion-container.benefits-wrap .inner-wrap .list-box .list .mid-wrap p {
    font-size: 2.25rem;
    color: #383838;
    line-height: 2.5rem;
  }
  .accordion-container.benefits-wrap .inner-wrap .top-wrap h4 {
    font-size: 3.75rem;
    line-height: 4rem;
    color: #7c7c7c;
    margin-bottom: 6rem;
  }
  .accordion-container.benefits-wrap .inner-wrap .top-wrap h5 {
    font-size: 2rem;
    line-height: 3rem;
    color: #383838;
    margin-bottom: 0.9rem;
    font-weight: 600;
  }
  .accordion-container.benefits-wrap .inner-wrap .mid-wrap p {
    font-size: 1.75rem;
    color: #4b4b4b;
    line-height: 2.5rem;
  }
  .accordion-container.discounts-wrap {
    background: #e20074;
  }
  .accordion-container.discounts-wrap .header {
    width: 80%;
    display: flex;
    flex-wrap: wrap;
  }
  .accordion-container.discounts-wrap .header .text {
    font-size: 5.5rem;
    line-height: 6rem;
    color: #fff;
    padding-right: 2rem;
    font-weight: 600;
  }
  .accordion-container.discounts-wrap .header .text.dark {
    color: #000;
  }
  .accordion-container.discounts-wrap .header .icon {
    font-size: 5.5rem;
    line-height: 6rem;
    color: #000;
    padding-right: 1rem;
    font-weight: 600;
  }
  .accordion-container.discounts-wrap .inner-wrap {
    margin-top: 12rem;
  }
  .accordion-container.discounts-wrap .inner-wrap .cols-md-5 {
    width: 45%;
    float: left;
    padding-right: 13rem;
  }
  .accordion-container.discounts-wrap .inner-wrap .cols-md-7 {
    width: 55%;
    float: left;
  }
  .accordion-container.discounts-wrap .inner-wrap .list-box {
    display: flex;
    flex-wrap: wrap;
    flex-direction: inherit;
  }
  .accordion-container.discounts-wrap .inner-wrap .list-box .list {
    float: left;
    width: 50%;
    margin-bottom: 4rem;
  }
  .accordion-container.discounts-wrap
    .inner-wrap
    .list-box
    .list:nth-child(odd) {
    padding-right: 5rem;
  }
  .accordion-container.discounts-wrap .inner-wrap .list-box .list:last-child {
    margin-bottom: 0;
  }
  .accordion-container.discounts-wrap .inner-wrap .list-box .list .mid-wrap p {
    font-size: 2.25rem;
    color: #fff;
    line-height: 2.5rem;
  }
  .accordion-container.discounts-wrap .inner-wrap .top-wrap h4 {
    font-size: 3.75rem;
    line-height: 4rem;
    color: #000;
    margin-bottom: 6rem;
  }
  .accordion-container.discounts-wrap .inner-wrap .top-wrap h5 {
    font-size: 2rem;
    line-height: 3rem;
    color: #fff;
    margin-bottom: 0.9rem;
    font-weight: 600;
  }
  .accordion-container.discounts-wrap .inner-wrap .mid-wrap p {
    font-size: 1.75rem;
    color: #fff;
    line-height: 2.5rem;
  }
  @media (max-width: 767px) {
    .hidden-xs {
      display: none;
    }
    .visible-xs {
      display: block;
    }
    .plans-container {
      padding: 5rem 2.5rem;
    }
    .plans-container .plans-col {
      width: 100%;
      margin-right: 0;
    }
    .plans-container .plans-col:last-child {
      margin-top: 5px;
      width: 100%;
    }
    .plans-container .plans-col .info-wrap p {
      font-size: 2rem;
    }
    .accordion-container {
      padding: 6.8rem 2.5rem 6.8rem;
    }
    .accordion-container.benefits-wrap .header,
    .accordion-container.discounts-wrap .header {
      width: 100%;
    }
    .accordion-container.benefits-wrap .header .text,
    .accordion-container.discounts-wrap .header .text {
      font-size: 5rem;
      line-height: 5.5rem;
      padding-right: 1rem;
    }
    .accordion-container.benefits-wrap .header .text:last-child,
    .accordion-container.discounts-wrap .header .text:last-child {
      padding-right: 0;
    }
    .accordion-container.benefits-wrap .header .icon,
    .accordion-container.discounts-wrap .header .icon {
      font-size: 5rem;
      line-height: 5.5rem;
      padding-right: 0.5rem;
    }
    .accordion-container.benefits-wrap .inner-wrap,
    .accordion-container.discounts-wrap .inner-wrap {
      margin-top: 6.4rem;
    }
    .accordion-container.benefits-wrap .inner-wrap .cols-md-5,
    .accordion-container.discounts-wrap .inner-wrap .cols-md-5 {
      width: 100%;
      padding-right: 6rem;
    }
    .accordion-container.benefits-wrap .inner-wrap .cols-md-7,
    .accordion-container.discounts-wrap .inner-wrap .cols-md-7 {
      width: 100%;
      padding-right: 6rem;
    }
    .accordion-container.benefits-wrap .inner-wrap .list-box,
    .accordion-container.discounts-wrap .inner-wrap .list-box {
      margin-bottom: 6.8rem;
    }
    .accordion-container.benefits-wrap .inner-wrap .list-box .list,
    .accordion-container.discounts-wrap .inner-wrap .list-box .list {
      width: 100%;
      padding-right: 0;
      margin-bottom: 3.2rem;
    }
    .accordion-container.benefits-wrap
      .inner-wrap
      .list-box
      .list:nth-child(odd),
    .accordion-container.discounts-wrap
      .inner-wrap
      .list-box
      .list:nth-child(odd) {
      padding-right: 0;
    }
  }
  .pos-abs{
    position: absolute;
  }
  </style>

  <div class='pos-abs'>
      <section class='accordion-container clearfix discounts-wrap'>
        <div class='header'>
          <span class='text'>Mobile Services</span>
          <span class='icon'>
            <i class='icon sc-bdVaJa bNjGGm'>
              <svg
                width='1em'
                height='1em'
                viewBox='0 0 64 64'
                color='currentColor'
              >
                <path
                  fill='currentColor'
                  d='M45 4a45.12 45.12 0 0 0-13-2 44.1 44.1 0 0 0-13 2c-3 1-3 4.5-3 4.5v47.3a4.47 4.47 0 0 0 3 4.1 43.12 43.12 0 0 0 13 2 44.1 44.1 0 0 0 13-2 4.47 4.47 0 0 0 3-4.1V8.5S48 5 45 4zm0 51.9a1.41 1.41 0 0 1-.9 1.3 41.37 41.37 0 0 1-12 1.8A43.68 43.68 0 0 1 20 57.2a1.67 1.67 0 0 1-1-1.3V51h26v4.9zm0-6.9H19V12h26v37zm0-39H19V8.5s.1-1.4 1-1.7A41.38 41.38 0 0 1 32 5a44.28 44.28 0 0 1 12.1 1.8c.8.3.9 1.7.9 1.7V10zM31 57h2a1.08 1.08 0 0 0 1-1v-2a1.08 1.08 0 0 0-1-1h-2a1.08 1.08 0 0 0-1 1v2a1.08 1.08 0 0 0 1 1z'
                />
              </svg>
            </i>
          </span>
          <span class='text'>+</span>
          <span class='text'>Telekom TV</span>
          <span class='icon'>
            <i class='icon sc-bdVaJa bNjGGm'>
              <svg
                width='1em'
                height='1em'
                viewBox='0 0 64 64'
                color='currentColor'
              >
                <path
                  fill='currentColor'
                  d='M60.001 9h-56c-1.1 0-2 .9-2 2v35c0 1.1.9 2 2 2h24v2h9v-2h23c1.1 0 2-.9 2-2V11c0-1.1-.9-2-2-2zm-1 36h-54V12h54v33zm-14 7h-26c-.55 0-1 .45-1 1v2h28v-2c0-.55-.45-1-1-1z'
                />
              </svg>
            </i>
          </span>
          <span class='text'>+</span>
          <span class='text'>Home Internet</span>
          <span class='icon'>
            <i class='icon sc-bdVaJa bNjGGm'>
              <svg
                width='1em'
                height='1em'
                viewBox='0 0 64 64'
                color='currentColor'
              >
                <path
                  fill='currentColor'
                  d='M53.095 27.278L32 6.182 10.853 27.33l.056.056v30.978h42.182V27.386a1.3 1.3 0 0 1 .004-.108zm2.632 2.632v29.772c0 .728-.59 1.318-1.318 1.318H9.591c-.728 0-1.318-.59-1.318-1.318V29.91L5.25 32.932a1.318 1.318 0 0 1-1.864-1.864L31.068 3.386a1.318 1.318 0 0 1 1.864 0l27.682 27.682a1.318 1.318 0 0 1-1.864 1.864l-3.023-3.022zM29.364 47.818h5.272v5.273h-5.272v-5.273zm-4.99-4.648A10.515 10.515 0 0 1 32 39.91c2.999 0 5.705 1.25 7.626 3.26l-1.829 1.905A7.888 7.888 0 0 0 32 42.545c-2.29 0-4.353.974-5.797 2.53l-1.829-1.905zm-3.653-3.806A15.77 15.77 0 0 1 32 34.636c4.416 0 8.41 1.81 11.28 4.728l-1.827 1.903A13.143 13.143 0 0 0 32 37.273c-3.708 0-7.058 1.53-9.453 3.994l-1.826-1.903zm-3.653-3.805A21.025 21.025 0 0 1 32 29.364a21.025 21.025 0 0 1 14.932 6.195l-1.826 1.903A18.398 18.398 0 0 0 32 32a18.398 18.398 0 0 0-13.106 5.462l-1.826-1.903z'
                />
              </svg>
            </i>
          </span>
          <span class='text'>+</span>
          <span class='text'>Home Phone</span>
          <span class='icon'>
            <i class='icon sc-bdVaJa bNjGGm'>
              <svg
                width='1em'
                height='1em'
                viewBox='0 0 64 64'
                color='currentColor'
              >
                <path
                  fill='currentColor'
                  d='M50.415 36.463v22.646a1.975 1.975 0 0 1-1.969 1.969H14.97a1.974 1.974 0 0 1-1.97-1.97V36.464c0-1.084.885-1.97 1.97-1.97h2.953v2.954h-1.97v20.677h31.509V37.447h-1.97v-2.954h2.954c1.083 0 1.97.886 1.97 1.97zM43.523 3.969V53.2c0 1.083-.885 1.97-1.97 1.97h-19.69a1.975 1.975 0 0 1-1.97-1.97V3.97c0-1.084.886-1.97 1.97-1.97h19.692c1.084 0 1.97.886 1.97 1.97zm-20.677.985v2.954H40.57V4.954H22.846zm0 4.923V25.63H40.57V9.877H22.846zM40.57 52.215V27.6H22.846v24.615H40.57zm-7.384-4.923H30.23a.494.494 0 0 0-.493.493v1.969c0 .27.223.492.493.492h2.954c.27 0 .492-.221.492-.492v-1.97a.494.494 0 0 0-.492-.492zm1.5-16.738h-5.908v3.94h5.908v-3.94zm-6.423 6.892h-2.954a.494.494 0 0 0-.493.492v1.97c0 .27.223.492.493.492h2.954c.27 0 .492-.222.492-.492v-1.97a.494.494 0 0 0-.492-.492zm0 4.923h-2.954a.494.494 0 0 0-.493.493v1.969c0 .27.223.492.493.492h2.954c.27 0 .492-.221.492-.492v-1.97a.494.494 0 0 0-.492-.492zm0 4.923h-2.954a.494.494 0 0 0-.493.493v1.969c0 .27.223.492.493.492h2.954c.27 0 .492-.221.492-.492v-1.97a.494.494 0 0 0-.492-.492zm4.923-9.846H30.23a.494.494 0 0 0-.493.492v1.97c0 .27.223.492.493.492h2.954c.27 0 .492-.222.492-.492v-1.97a.494.494 0 0 0-.492-.492zm4.923 4.923h-2.954a.494.494 0 0 0-.492.493v1.969c0 .27.222.492.492.492h2.954c.27 0 .492-.221.492-.492v-1.97a.494.494 0 0 0-.492-.492zm-4.923 0H30.23a.494.494 0 0 0-.493.493v1.969c0 .27.223.492.493.492h2.954c.27 0 .492-.221.492-.492v-1.97a.494.494 0 0 0-.492-.492zm4.923-4.923h-2.954a.494.494 0 0 0-.492.492v1.97c0 .27.222.492.492.492h2.954c.27 0 .492-.222.492-.492v-1.97a.494.494 0 0 0-.492-.492zm0 9.846h-2.954a.494.494 0 0 0-.492.493v1.969c0 .27.222.492.492.492h2.954c.27 0 .492-.221.492-.492v-1.97a.494.494 0 0 0-.492-.492z'
                />
              </svg>
            </i>
          </span>
          <span class='text dark'>=</span>
          <span class='text dark'>Magenta Discount</span>
        </div>
        <div class='inner-wrap clearfix'>
          <div class='cols-md-5'>
            <div class='top-wrap'>
              <h4>If you have at least 2 services enjoy all the benefits:</h4>
            </div>
            <div class='mid-wrap visible-lg hidden-xs'>
              <p>
                The following conditions must be met to enforce the discount:
                There is no charge on the current account of home and mobile
                services, the person of the subscriber of the home and mobile
                services is the same, so that the two current accounts can be
                collected during the administration, which allows the discount
                to be validated.
              </p>
            </div>
          </div>
          <div class='cols-md-7'>
            <ul class='list-box clearfix'>
              <li class='list'>
                <div class='top-wrap'>
                  <h5>Free Family Calls</h5>
                </div>
                <div class='mid-wrap'>
                  <p>
                    Talk within the family unlimited, including up to 5 mobile
                    numbers, 0 Ft per month!
                  </p>
                </div>
              </li>
              <li class='list'>
                <div class='top-wrap'>
                  <h5>500MB bonus</h5>
                </div>
                <div class='mid-wrap'>
                  <p>
                    Every time you recharge a minumum of 4 000 Ft on a prepaid
                    you get 500 MB upload bonus.
                  </p>
                </div>
              </li>
              <li class='list'>
                <div class='top-wrap'>
                  <h5>Extra discount</h5>
                </div>
                <div class='mid-wrap'>
                  <p>
                    You will receive an extra 30,000 Ft discount on any device.
                  </p>
                </div>
              </li>
              <li class='list'>
                <div class='top-wrap'>
                  <h5>Unlimited TV</h5>
                </div>
                <div class='mid-wrap'>
                  <p>
                    With TV GO App , you can watch TV on the mobile phone free
                    of charge, with unlimited domestic data traffic.
                  </p>
                </div>
              </li>
              <li class='list'>
                <div class='top-wrap'>
                  <h5>2X Data</h5>
                </div>
                <div class='mid-wrap'>
                  <p>
                    Duplicate GB, minute and SMS. The more the tariff the more
                    the discount.
                  </p>
                </div>
              </li>
            </ul>
            <div class='mid-wrap visible-xs hidden-lg'>
              <p>
                The following conditions must be met to enforce the discount:
                There is no charge on the current account of home and mobile
                services, the person of the subscriber of the home and mobile
                services is the same, so that the two current accounts can be
                collected during the administration, which allows the discount
                to be validated.
              </p>
            </div>
          </div>
        </div>
      </section>
  </div>
  `;
  // tslint:disable-next-line:max-file-line-count
}
