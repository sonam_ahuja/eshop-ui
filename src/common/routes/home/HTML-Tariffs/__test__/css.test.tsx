import FaimlyBenifits from '@src/common/routes/home/HTML-Tariffs/HTMLs/faimlyBenifits';
import HTMLs from '@src/common/routes/home/HTML-Tariffs/HTMLs';
import MagentaDiscount from '@src/common/routes/home/HTML-Tariffs/HTMLs/magentaDiscount';

describe('<css/>', () => {
  test('css function', () => {
    expect(typeof FaimlyBenifits()).toBe('string');
    expect(typeof HTMLs()).toBe('string');
    expect(typeof MagentaDiscount()).toBe('string');
  });
});
