import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import DoublePlanComponent from '@src/common/routes/home/HTML-Tariffs/double-plans';
import FaqsComponent from '@src/common/routes/home/HTML-Tariffs/faqs';
import FourPlanComponent from '@src/common/routes/home/HTML-Tariffs/four-plans';
import PlansHtmlComponents from '@src/common/routes/home/HTML-Tariffs/index';
import SinglePlanComponent from '@src/common/routes/home/HTML-Tariffs/single-plans';
import ThreePlanComponent from '@src/common/routes/home/HTML-Tariffs/three-plans';
import { StaticRouter } from 'react-router';

describe('<MultiplePlans />', () => {
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <div>
            <DoublePlanComponent />
            <FaqsComponent />
            <FourPlanComponent />
            <PlansHtmlComponents />
            <SinglePlanComponent />
            <ThreePlanComponent />
          </div>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
