import React from 'react';
import styled from 'styled-components';
import { BreadcrumbItem, Icon } from 'dt-components';
import { colors } from '@src/common/variables';

const Breadcrumb = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  padding: 0 0 2.5rem;
  li {
    color: ${colors.mediumGray};
    list-style: none;
    font-size: 0.75rem;
    line-height: 1rem;
    float: left;
    font-weight: 600;
    display: flex;
    align-items: center;
    &.active {
      color: ${colors.darkGray};
    }
  }
  li > div {
    display: flex;
    align-items: center;
  }
  i {
    margin: 0 0.5rem;
  }
`;

// tslint:disable-next-line:no-big-function
const BreadcrumbComponent = () => {
  return (
    <Breadcrumb className='breadcrumb clearfix'>
      {' '}
      <BreadcrumbItem>
        {' '}
        <a href='#'>Home</a>{' '}
        <Icon size='inherit' color='currentColor' name='ec-continue' />{' '}
      </BreadcrumbItem>{' '}
      <BreadcrumbItem active> Postpaid Plans </BreadcrumbItem>{' '}
    </Breadcrumb>
  );
};

// tslint:disable-next-line:max-file-line-count
export default BreadcrumbComponent;
