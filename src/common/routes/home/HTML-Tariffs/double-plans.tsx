import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { colors } from '@src/common/variables';
import { Button, Icon, Select } from 'dt-components';

import BreadcrumbComponent from './breadcrumb';

const DoublePlansWrap = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .main-plans-wrapper {
    padding: 2rem 6.4rem 3rem;
    background: #f6f6f6;
    .header-panel {
      .left-panel {
        float: left;
        width: 35%;
        text-align: left;
        h1 {
          font-size: 2.5rem;
          line-height: 2.75rem;
          color: ${colors.darkGray};
          letter-spacing: -0.25px;
          font-weight: 600;
          margin-bottom: 0.5rem;
        }
        p {
          font-size: 0.875rem;
          line-height: 1.25rem;
          color: ${colors.mediumGray};
          max-width: 17.75rem;
        }
      }
      .right-panel {
        float: right;
        width: 65%;
        text-align: right;
        .list-inline {
          float: right;
          li {
            float: left;
            margin-right: 0.25rem;
            line-height: 1.25rem;
            &:last-child {
              margin-right: 0;
            }
            &.dropdown-wrap {
              width: 8.45rem;
              background: ${colors.white};
              padding: 0.625rem 0rem;
              font-size: 0.75rem;
              line-height: 1rem;
              border-radius: 6px;
              border: 1px solid ${colors.warmGray};
              margin-top: 0.5rem;
              .styledSelectWrap {
                padding: 0rem 0.75rem;
                .styledSelect {
                  color: ${colors.darkGray};
                  text-overflow: ellipsis;
                  white-space: nowrap;
                  overflow: hidden;
                }
              }
              .optionsList {
                margin-top: -2rem;
                div {
                  color: ${colors.darkGray};
                  padding: 0.875rem 0.75rem;
                  font-size: 0.875rem;
                  line-height: 1.25rem;
                  font-weight: bold;
                }
              }
            }
          }
        }
        .linker {
          font-size: 0.875rem;
          line-height: 1.25rem;
          color: #e20074;
          margin-top: 1.75rem;
          float: right;
          width: 100%;
        }
      }
    }
    .panel-wrap {
      padding: 3rem 0 1.5rem;
      .list-inline {
        li {
          float: left;
          margin-right: 0.25rem;
          line-height: 1.25rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
      .multiple-plans {
        .pricing-plans-wrapper {
          .aside-panel {
            width: 7.25rem;
            float: left;
            .plans-header {
              height: 7.5rem;
              display: flex;
              align-items: center;
              justify-content: center;
              padding: 0;
              background: transparent;
              border-bottom: 1px solid ${colors.silverGray};
              .title-wrap {
                float: none;
                width: 100%;
                text-align: center;
                h3 {
                  color: ${colors.darkGray};
                  font-size: 0.75rem;
                  line-height: 1rem;
                  text-align: left;
                }
              }
            }
            .plans-body-wrap {
              background: transparent;
              padding: 0;
              border-radius: 0px;
              .plans-box {
                .lists {
                  border-bottom: 1px solid ${colors.silverGray};
                  padding: 1.225rem 0;
                  display: flex;
                  align-items: center;
                  justify-content: flex-start;
                }
              }
            }
            .btn-wrapper {
              margin-top: 3rem;
            }
          }
          .flex-container {
            width: calc(100% - 7.25rem);
            float: left;
            display: flex;
            padding-left: 0.5rem;
            .flex-panel {
              flex: 1 1 auto;
              margin-right: 0.5rem;
              text-align: center;
              border-radius: 10px;
              border: 1px solid transparent;
              transition: all 0.5s ease;
              cursor: default;
              &:hover,
              &:focus,
              &:active,
              &.active {
                border: 1px solid ${colors.magenta};
              }
              &:last-child {
                margin-right: 0;
              }
            }
          }
        }
        .plans-body-wrap {
          background: ${colors.white};
          padding: 0rem 1rem 0.75rem;
          border-bottom-left-radius: 10px;
          border-bottom-right-radius: 10px;
          .plans-box {
            .lists {
              width: 100%;
              margin: 0;
              border-bottom: 1px solid ${colors.silverGray};
              padding: 1.1rem 0;
              max-height: 3.75rem;
              height: 3.75rem;
              display: flex;
              align-items: center;
              justify-content: center;
              &:last-child {
                border-bottom: 1px solid transparent;
              }
            }
            .check-icon {
              font-size: 1rem;
              color: ${colors.black};
            }
            .titles {
              color: ${colors.darkGray};
              font-size: 0.75rem;
              line-height: 1rem;
              text-align: left;
            }
            .data-text {
              font-size: 1rem;
              line-height: 1.5rem;
              color: ${colors.mediumGray};
              font-weight: 500;
              display: block;
              strong {
                color: ${colors.black};
              }
            }
            .price-text {
              font-size: 1.75rem;
              line-height: 2rem;
              color: ${colors.magenta};
              font-weight: 900;
              sup {
                vertical-align: super;
                font-size: 0.625rem;
                line-height: 0.75rem;
              }
            }
            .circle-list {
              display: flex;
              align-items: center;
              justify-content: center;
              li {
                border-radius: 50px;
                height: 1.75rem;
                width: 1.75rem;
                border: 1px solid transparent;
                display: flex;
                align-items: center;
                justify-content: center;
                transition: all 1s ease;
                cursor: pointer;
                i {
                  font-size: 0.75rem;
                  color: #a4a4a4;
                }
                &:hover,
                &:active,
                &:focus,
                &.active {
                  border-color: ${colors.magenta};
                  i {
                    color: ${colors.black};
                  }
                }
              }
            }
          }
        }
        .plans-header {
          height: 7.5rem;
          display: flex;
          align-items: center;
          justify-content: center;
          padding: 0;
          background: ${colors.cloudGray};
          border-top-left-radius: 10px;
          border-top-right-radius: 10px;
          .title-wrap {
            float: none;
            width: 100%;
            text-align: center;
            h3 {
              color: ${colors.darkGray};
              font-size: 0.75rem;
              line-height: 1rem;
            }
          }
        }
      }
      header {
        display: flex;
        align-items: center;
        padding: 0 0.5rem 0.6rem 1.5rem;
        .title-wrap {
          width: calc(100% - 5rem);
          float: left;
          h3 {
            font-size: 1.125rem;
            line-height: 1.25rem;
            color: ${colors.ironGray};
          }
        }
        .price-text {
          font-size: 1.875rem;
          line-height: 2.5rem;
          color: ${colors.magenta};
          font-weight: 900;
          sup {
            vertical-align: super;
            font-size: 0.625rem;
            line-height: 0.75rem;
          }
        }
        .titles {
          font-size: 1rem;
          line-height: 1.5rem;
          color: ${colors.ironGray};
          display: block;
        }
        .icon-wrap {
          width: 5rem;
          float: right;
          text-align: right;
          i {
            font-size: 1.75rem;
          }
        }
      }
    }
    .btn-wrapper {
      display: flex;
      align-items: center;
      width: 100%;
      flex-wrap: wrap;
      .btn-primary {
        font-size: 0.875rem;
        line-height: 1.25rem;
        padding: 0.65rem 1.5rem;
        min-width: 8.75rem;
        text-align: center;
        width: 100%;
        min-height: 2.5rem;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .btn-line {
        font-size: 0.875rem;
        line-height: 1.25rem;
        padding: 0.65rem 1.5rem;
        min-width: 8.75rem;
        width: 100%;
        text-align: center;
        background: transparent;
        color: ${colors.magenta};
        min-height: 2.5rem;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .btn + .btn {
        margin-top: 0.25rem;
      }
      .left-panel {
        float: left;
        width: 35%;
        text-align: left;
      }
      .right-panel {
        float: right;
        width: 65%;
        text-align: right;
        margin-left: 35%;
        margin-top: 1.5rem;
        .list-inline {
          float: right;
          li {
            float: left;
            margin-right: 0.25rem;
            line-height: 1.25rem;
            &:last-child {
              margin-right: 0;
            }
          }
        }
      }
    }
    .view-button {
      font-size: 0.875rem;
      line-height: 1.25rem;
      color: ${colors.magenta};
      font-weight: 500;
      padding: 0.65rem 0rem;
      width: 100%;
    }
    .linker {
      font-size: 0.875rem;
      line-height: 1.25rem;
      color: #e20074;
      margin-top: 0;
      float: right;
      width: 100%;
    }
  }
`;

// tslint:disable-next-line:no-big-function
const DoublePlanComponent = () => {
  return (
    <DoublePlansWrap>
      <div className='main-plans-wrapper'>
        <BreadcrumbComponent />
        <div className='header-panel clearfix'>
          <div className='left-panel'>
            <h1>Get the best rates!</h1>
            <p>
              All plans include Best network with LTE Max, Hotspot flat rate,
              Unlimited SMS, EU roaming.
            </p>
          </div>
          <div className='right-panel'>
            <ul className='list-inline clearfix'>
              <li className='dropdown-wrap'>
                <Select
                  placeholder='Please Select'
                  items={[
                    { id: 1, title: '24 months loyalty' },
                    { id: 2, title: '12 months loyalty' },
                    { id: 3, title: 'No loyalty' }
                  ]}
                >
                  {' '}
                </Select>
              </li>
              <li className='dropdown-wrap'>
                <Select
                  placeholder='Please Select'
                  items={[
                    { id: 1, title: 'unfolded plans' },
                    { id: 2, title: 'magenta discounts' },
                    { id: 3, title: 'family discounts' }
                  ]}
                >
                  {' '}
                </Select>
              </li>
            </ul>
            <Link to='#' className='linker'>
              <span>About Discounts</span>
            </Link>
          </div>
        </div>
        <div className='panel-wrap'>
          <div className='multiple-plans'>
            <div className='pricing-plans-wrapper clearfix'>
              <div className='aside-panel'>
                <header className='plans-header clearfix'>
                  <div className='title-wrap'>
                    <h3>Price /mo</h3>
                  </div>
                </header>
                <div className='plans-body-wrap clearfix'>
                  <ul className='plans-box list-inline clearfix'>
                    <li className='lists'>
                      <span className='titles'>Data</span>
                    </li>
                    <li className='lists'>
                      <span className='titles'>Free App</span>
                    </li>
                    <li className='lists'>
                      <span className='titles'>Minutes</span>
                    </li>
                    <li className='lists'>
                      <span className='titles'>
                        Unlimited Calls and SMS Local
                      </span>
                    </li>
                    <li className='lists'>
                      <span className='titles'>MAXtv to GO Free</span>
                    </li>
                  </ul>
                  <div className='btn-wrapper clearfix'>
                    <Link to='#' className='view-button'>
                      View Less
                    </Link>
                  </div>
                </div>
              </div>
              <div className='flex-container'>
                <div className='flex-panel'>
                  <header className='plans-header clearfix'>
                    <div className='title-wrap'>
                      <span className='price-text'>
                        3 500<sup>Ft</sup>
                      </span>
                      <span className='titles'>Best S</span>
                    </div>
                  </header>
                  <div className='plans-body-wrap clearfix'>
                    <ul className='plans-box list-inline clearfix'>
                      <li className='lists'>
                        <span className='data-text'>
                          <strong>750</strong> MB
                        </span>
                      </li>
                      <li className='lists'>
                        <span className='data-text'>
                          <ul className='ordered-list list-inline circle-list clearfix'>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-facebook-solid'
                              />
                            </li>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-instagram-solid'
                              />
                            </li>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-snapchat-solid'
                              />
                            </li>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-whatsapp-solid'
                              />
                            </li>
                          </ul>
                        </span>
                      </li>
                      <li className='lists'>
                        <span className='data-text'>
                          <strong>500</strong>
                        </span>
                      </li>
                      <li className='lists'>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-confirm'
                          className='check-icon'
                        />
                      </li>
                      <li className='lists'>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-confirm'
                          className='check-icon'
                        />
                      </li>
                    </ul>
                    <div className='btn-wrapper'>
                      <Button className='btn btn-primary'>Add a Phone</Button>
                      <Button className='btn btn-line'>Buy Plan Only</Button>
                    </div>
                  </div>
                </div>
                <div className='flex-panel'>
                  <header className='plans-header clearfix'>
                    <div className='title-wrap'>
                      <span className='price-text'>
                        5 500<sup>Ft</sup>
                      </span>
                      <span className='titles'>Best M</span>
                    </div>
                  </header>
                  <div className='plans-body-wrap clearfix'>
                    <ul className='plans-box list-inline clearfix'>
                      <li className='lists'>
                        <span className='data-text'>
                          <strong>750</strong> MB
                        </span>
                      </li>
                      <li className='lists'>
                        <span className='data-text'>
                          <ul className='ordered-list list-inline circle-list clearfix'>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-facebook-solid'
                              />
                            </li>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-instagram-solid'
                              />
                            </li>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-snapchat-solid'
                              />
                            </li>
                            <li>
                              <Icon
                                size='inherit'
                                color='currentColor'
                                name='ec-whatsapp-solid'
                              />
                            </li>
                          </ul>
                        </span>
                      </li>
                      <li className='lists'>
                        <span className='data-text'>
                          <strong>500</strong>
                        </span>
                      </li>
                      <li className='lists'>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-confirm'
                          className='check-icon'
                        />
                      </li>
                      <li className='lists'>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-confirm'
                          className='check-icon'
                        />
                      </li>
                    </ul>
                    <div className='btn-wrapper'>
                      <Button className='btn btn-primary'>Add a Phone</Button>
                      <Button className='btn btn-line'>Buy Plan Only</Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='btn-wrapper clearfix'>
          <div className='right-panel'>
            <Link to='#' className='linker'>
              <span>Terms &amp; Conditions</span>
            </Link>
          </div>
        </div>
      </div>
    </DoublePlansWrap>
  );
};

// tslint:disable-next-line:max-file-line-count
export default DoublePlanComponent;
