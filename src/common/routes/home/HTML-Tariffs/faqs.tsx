import React from 'react';
import styled from 'styled-components';

const FaqsWrap = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
`;

// tslint:disable-next-line:no-big-function
const FaqsComponent = () => {
  return (
    <FaqsWrap>
      <div className='faqs-wrapper'>
        <div className='header'>
          <h2>Frequently Asked Questions</h2>
        </div>
      </div>
    </FaqsWrap>
  );
};

// tslint:disable-next-line:max-file-line-count
export default FaqsComponent;
