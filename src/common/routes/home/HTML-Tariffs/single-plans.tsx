import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { colors } from '@src/common/variables';
import { Button, Icon, Select } from 'dt-components';

import BreadcrumbComponent from './breadcrumb';

const SinglePlanWrap = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .main-plans-wrapper {
    padding: 2rem 6.4rem 3rem;
    background: #f6f6f6;
    .header-panel {
      .left-panel {
        float: left;
        width: 35%;
        text-align: left;
        h1 {
          font-size: 2.5rem;
          line-height: 2.75rem;
          color: ${colors.darkGray};
          letter-spacing: -0.25px;
          font-weight: 600;
          margin-bottom: 0.5rem;
        }
        p {
          font-size: 0.875rem;
          line-height: 1.25rem;
          color: ${colors.mediumGray};
          max-width: 17.75rem;
        }
      }
      .right-panel {
        float: right;
        width: 65%;
        text-align: right;
        .list-inline {
          float: right;
          li {
            float: left;
            margin-right: 0.25rem;
            line-height: 1.25rem;
            &:last-child {
              margin-right: 0;
            }
            &.dropdown-wrap {
              width: 8.45rem;
              background: ${colors.white};
              padding: 0.625rem 0rem;
              font-size: 0.75rem;
              line-height: 1rem;
              border-radius: 6px;
              border: 1px solid ${colors.warmGray};
              margin-top: 0.5rem;
              .styledSelectWrap {
                padding: 0rem 0.75rem;
                .styledSelect {
                  color: ${colors.darkGray};
                  text-overflow: ellipsis;
                  white-space: nowrap;
                  overflow: hidden;
                }
              }
              .optionsList {
                margin-top: -2rem;
                div {
                  color: ${colors.darkGray};
                  padding: 0.875rem 0.75rem;
                  font-size: 0.875rem;
                  line-height: 1.25rem;
                  font-weight: bold;
                }
              }
            }
          }
        }
        .linker {
          font-size: 0.875rem;
          line-height: 1.25rem;
          color: #e20074;
          margin-top: 1.75rem;
          float: right;
          width: 100%;
        }
      }
    }
    .panel-wrap {
      padding: 3rem 0 1.5rem;
      .list-inline {
        li {
          float: left;
          margin-right: 0.25rem;
          line-height: 1.25rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
      .single-wrap {
        border-radius: 10px;
        border: 1px solid transparent;
        padding: 2rem 0.5rem 0.5rem 0.5rem;
        background: ${colors.cloudGray};
        transition: all 0.5s ease;
        &:hover {
          border-color: ${colors.magenta};
        }
      }
      header {
        display: flex;
        align-items: center;
        padding: 0 0.5rem 0.6rem 1.5rem;
        .title-wrap {
          width: calc(100% - 5rem);
          float: left;
          h3 {
            font-size: 1.125rem;
            line-height: 1.25rem;
            color: ${colors.ironGray};
          }
        }
        .icon-wrap {
          width: 5rem;
          float: right;
          text-align: right;
          i {
            font-size: 1.75rem;
          }
        }
      }
      .plans-body-wrap {
        background: ${colors.white};
        padding: 1rem 1.5rem;
        border-radius: 5px;
        .plans-box {
          .lists {
            width: 25%;
            margin: 0;
            border-right: 1px solid ${colors.silverGray};
            padding-left: 1.5rem;
            &:last-child {
              border-right: 0;
            }
            &:first-child {
              padding-left: 0;
            }
          }
          .titles {
            font-size: 0.875rem;
            line-height: 1.25rem;
            color: ${colors.mediumGray};
            margin-bottom: 0.75rem;
            display: block;
          }
          .data-text {
            font-size: 1.75rem;
            line-height: 2rem;
            color: ${colors.mediumGray};
            font-weight: bold;
            display: block;
            strong {
              color: ${colors.darkGray};
            }
          }
          .price-text {
            font-size: 1.75rem;
            line-height: 2rem;
            color: ${colors.magenta};
            font-weight: 600;
            sup {
              vertical-align: super;
              font-size: 0.625rem;
              line-height: 0.75rem;
            }
          }
          .circle-list {
            margin-left: 0;
            li {
              border-radius: 50px;
              height: 2rem;
              width: 2rem;
              border: 1px solid transparent;
              display: flex;
              align-items: center;
              justify-content: center;
              transition: all 1s ease;
              cursor: pointer;
              i {
                font-size: 0.75rem;
                color: #a4a4a4;
              }
              &:hover,
              &:active,
              &:focus,
              &.active {
                border-color: ${colors.magenta};
                i {
                  color: ${colors.black};
                }
              }
            }
          }
        }
      }
    }
    .btn-wrapper {
      display: flex;
      align-items: center;
      .left-panel {
        float: left;
        width: 35%;
        text-align: left;
      }
      .right-panel {
        float: right;
        width: 65%;
        text-align: right;
        .list-inline {
          float: right;
          li {
            float: left;
            margin-right: 0.25rem;
            line-height: 1.25rem;
            &:last-child {
              margin-right: 0;
            }
          }
        }
      }
      ul {
        li {
          margin-right: 0.75rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
      .linker {
        font-size: 0.875rem;
        line-height: 1.25rem;
        color: #e20074;
        margin-top: 0;
        float: right;
        width: 100%;
      }
      .btn-primary {
        font-size: 0.875rem;
        line-height: 0.125rem;
        padding: 0.65rem 1.5rem;
        min-width: 8.75rem;
        text-align: center;

        min-height: 2.5rem;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .btn-line {
        font-size: 0.875rem;
        line-height: 0.125rem;
        padding: 0.65rem 1.5rem;
        min-width: 8.75rem;
        text-align: center;
        background: transparent;
        color: ${colors.magenta};
        min-height: 2.5rem;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    }
  }
  @media (max-width: 767px) {
    .main-plans-wrapper {
      display: none;
    }
  }
`;

// tslint:disable-next-line:no-big-function
const SinglePlanComponent = () => {
  return (
    <SinglePlanWrap>
      <div className='main-plans-wrapper'>
        <BreadcrumbComponent />
        <div className='header-panel clearfix'>
          <div className='left-panel'>
            <h1>Get the best rates!</h1>
            <p>
              All plans include Best network with LTE Max, Hotspot flat rate,
              Unlimited SMS, EU roaming.
            </p>
          </div>
          <div className='right-panel'>
            <ul className='list-inline clearfix'>
              <li className='dropdown-wrap'>
                <Select
                  placeholder='Please Select'
                  items={[
                    { id: 1, title: '24 months loyalty' },
                    { id: 2, title: '12 months loyalty' },
                    { id: 3, title: 'No loyalty' }
                  ]}
                >
                  {' '}
                </Select>
              </li>
              <li className='dropdown-wrap'>
                <Select
                  placeholder='Please Select'
                  items={[
                    { id: 1, title: 'unfolded plans' },
                    { id: 2, title: 'magenta discounts' },
                    { id: 3, title: 'family discounts' }
                  ]}
                >
                  {' '}
                </Select>
              </li>
            </ul>
            <Link to='#' className='linker'>
              <span>About Discounts</span>
            </Link>
          </div>
        </div>
        <div className='panel-wrap'>
          <div className='single-wrap'>
            <header className='clearfix'>
              <div className='title-wrap'>
                <h3>Best L</h3>
              </div>
              <div className='icon-wrap'>
                <Icon size='inherit' name='ec-special-offer' />
              </div>
            </header>
            <div className='plans-body-wrap clearfix'>
              <ul className='plans-box list-inline'>
                <li className='lists'>
                  <span className='titles'>Data</span>
                  <span className='data-text'>
                    <strong>5</strong> MB
                  </span>
                </li>
                <li className='lists'>
                  <span className='titles'>Data</span>
                  <span className='data-text'>
                    <strong>Unlimited</strong>
                  </span>
                </li>
                <li className='lists'>
                  <span className='titles'>Data</span>
                  <span className='data-text'>
                    <ul className='ordered-list list-inline circle-list clearfix'>
                      <li>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-facebook-solid'
                        />
                      </li>
                      <li>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-instagram-solid'
                        />
                      </li>
                      <li>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-snapchat-solid'
                        />
                      </li>
                      <li>
                        <Icon
                          size='inherit'
                          color='currentColor'
                          name='ec-whatsapp-solid'
                        />
                      </li>
                    </ul>
                  </span>
                </li>
                <li className='lists'>
                  <span className='titles'>&nbsp;</span>
                  <span className='price-text'>
                    7 500<sup>Ft</sup>
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className='btn-wrapper clearfix'>
          <div className='left-panel'>
            <Link to='#' className='linker'>
              <span>Terms &amp; Conditions</span>
            </Link>
          </div>
          <div className='right-panel'>
            <ul className='list-inline'>
              <li>
                <Button className='btn btn-line'>Buy Plan Only</Button>
              </li>
              <li>
                <Button className='btn btn-primary'>Add a Phone</Button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </SinglePlanWrap>
  );
};

// tslint:disable-next-line:max-file-line-count
export default SinglePlanComponent;
