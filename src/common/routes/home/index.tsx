import { ITranslationState } from '@src/common/store/types/translation';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { RootState } from '@common/store/reducers';
import Header from '@common/components/Header';
import Footer from '@common/components/Footer';
import { DEVICE_LIST, PAGE_VIEW } from '@events/constants/eventName';
import Error from '@src/common/components/Error';
import { IError as IGenricError } from '@src/common/store/types/common';
import { pageViewEvent, sendCTAClicks } from '@events/index';
import { Dispatch } from 'redux';
import React from 'react';
import { LandingPage } from 'dt-components';
import { openTargetLink } from '@src/common/utils/openTargetLink';
import { Helmet } from 'react-helmet';
import actions from '@home/store/actions';
import { IDataSection, ILandingPageState, IMetaData } from '@home/store/types';
import { isCategoryLandingPageRequired } from '@common/store/common/index';
import { IConfigurationState } from '@src/common/store/types/configuration';
import DownloadOneApp from '@src/common/components/DownloadOneApp';
import BasketStripWrapper from '@src/common/components/BasketStripWrapper';
import { getMobileOperatingSystem } from '@src/common/utils/getOperatingSystem';
import {
  sendLandingPagePromotionClickEvent,
  sendLandingPagePromotionViewEvent
} from '@events/home/index';
import { SECTION_TYPE } from '@home/store/enums';
import { StyledContentWrapper, StyledLandingWrapper } from '@home/styles';
import {
  IError,
  IGetNotificationRequestTemplatePayload,
  INotificationData,
  IVariant
} from '@productList/store/types';
import CommonModal from '@productList/Modals/CommonModal';
import ProductCard from '@productList/common/ProductCard';
import {
  getCTAText,
  getImage,
  getOutOfStock,
  getPrice,
  getSpecialOffer
} from '@productList/utils/getProductDetail';
import htmlKeys from '@common/constants/appkeys';
import {
  MODAL_TYPE,
  NOTIFICATION_MEDIUM,
  PRICES
} from '@productList/store/enum';
import { sendClickedProductEvent } from '@events/DeviceList/index';
import commonActions from '@common/store/actions/common';
import { isPreOrder } from '@home/store/selector';
import productListActions from '@productList/store/actions';
import { ILoginFormRules } from '@common/store/types/configuration';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

const Wrap = styled.div`
  color: blue;
  font-size: 1.5rem;
`;

export interface IProps extends RouteComponentProps<IRouterParams> {
  commonError: IGenricError;
  translation: ITranslationState;
  configuration: IConfigurationState;
  landingPageState: ILandingPageState;
  showModal: boolean;
  notificationLoading: boolean;
  notificationData: INotificationData;
  error: IError | null;
  notificationModalType: MODAL_TYPE;
  formConfiguration: ILoginFormRules;
  checkDynamicData(): void;
  getDynamicContent(data: string): void;
  setDeviceDetailedList(list: string): void;
  setNotificationData(data: INotificationData): void;
  openModal(isModalOpen: boolean): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
}

export class Home extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.onBannerClick = this.onBannerClick.bind(this);
    this.onCarouselClick = this.onCarouselClick.bind(this);
    this.onIconClick = this.onIconClick.bind(this);
    this.generateDynamicContent = this.generateDynamicContent.bind(this);
    this.getProductDetailUrl = this.getProductDetailUrl.bind(this);
    this.openNotificationModal = this.openNotificationModal.bind(this);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.HOME);
    this.props.checkDynamicData();
    sendLandingPagePromotionViewEvent();
  }

  componentWillUnmount(): void {
    this.props.openModal(false);
  }

  getCtaName = (data: IMetaData) => {
    return data.ctaTitle || data.name || data.title || '';
  }

  onBannerClick(data: IMetaData): void {
    sendLandingPagePromotionClickEvent(data);
    openTargetLink(data.ctaDeeplink);
  }

  onCarouselClick(data: IMetaData): void {
    sendCTAClicks(this.getCtaName(data), window.location.href);
    openTargetLink(data.ctaDeeplink);
  }

  onIconClick(data: IMetaData): void {
    sendCTAClicks(this.getCtaName(data), window.location.href);
    openTargetLink(data.ctaDeeplink);
  }

  dynamicContentHandler = (data: string) => {
    // tslint:disable: no-object-literal-type-assertion
    sendCTAClicks(
      this.getCtaName({ ctaTitle: data } as IMetaData),
      window.location.href
    );

    const {
      landingPageState: { staticContent },
      getDynamicContent
    } = this.props;

    let deviceMetaData: IMetaData[] = [];
    let isIdDifferent = true;
    if (staticContent && staticContent.data && staticContent.data.length) {
      staticContent.data.forEach((item: IDataSection) => {
        if (item.type === SECTION_TYPE.DEVICES) {
          deviceMetaData = item.metaData;
        }
      });

      if (deviceMetaData && deviceMetaData.length) {
        deviceMetaData.forEach((metaData: IMetaData) => {
          if (metaData.isDefault && metaData.id === data) {
            isIdDifferent = false;
          }
        });
      }
    }

    if (isIdDifferent) {
      getDynamicContent(data);
    }
  }

  openNotificationModal(
    event: React.MouseEvent,
    variantId: string,
    deviceName: string
  ): void {
    event.stopPropagation();
    this.props.setNotificationData({
      variantId,
      deviceName,
      mediumValue: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    });
    this.props.openModal(true);
  }

  getProductDetailUrl(
    id: string,
    productName: string,
    productId: string
  ): string {
    let productDetailUrl = '';
    const {
      landingPageState: { dynamicContent }
    } = this.props;

    if (dynamicContent && dynamicContent.length) {
      productDetailUrl = `/${
        dynamicContent[0].categorySlug
      }/${productName}/${productId}/${id}}`;

      return productDetailUrl;
    }

    return productDetailUrl;
  }

  generateDynamicContent(popularDevices: IVariant[]): React.ReactNode {
    const {
      translation,
      configuration,
      setDeviceDetailedList,
      landingPageState: { dynamicContent }
    } = this.props;

    const {
      catalog: catalogConfiguration
    } = configuration.cms_configuration.modules;

    const { currency } = configuration.cms_configuration.global;
    const { productList: productListTranslation } = translation.cart;

    return (
      <>
        {popularDevices.map((productItem, index) => (
          <ProductCard
            key={index}
            heading={productItem.productName}
            alt={productItem.name}
            productId={productItem.id}
            frontImageUrl={getImage(
              productItem.attachments[htmlKeys.THUMBNAIL_IMAGE],
              htmlKeys.PRODUCT_IMAGE_FRONT
            )}
            frontAndBackImageUrl={getImage(
              productItem.attachments[htmlKeys.THUMBNAIL_IMAGE],
              htmlKeys.PRODUCT_IMAGE_FRONT_AND_BACK
            )}
            onLinkClick={() => {
              setDeviceDetailedList(DEVICE_LIST.POPULAR_DEVICE);
              sendClickedProductEvent(
                popularDevices,
                productItem.productId,
                dynamicContent[0].categorySlug,
                DEVICE_LIST.POPULAR_DEVICE
              );
            }}
            monthlyAmount={getPrice(
              productItem.prices,
              PRICES.MONTHLY,
              currency
            )}
            baseAmount={getPrice(
              productItem.prices,
              PRICES.BASE_PRICE,
              currency
            )}
            upfrontAmount={getPrice(
              productItem.prices,
              PRICES.UPFRONT,
              currency
            )}
            isPreOrder={isPreOrder(
              productItem.unavailabilityReasonCodes,
              catalogConfiguration.preorder.considerInventory
            )}
            openNotificationModal={this.openNotificationModal}
            headingIconName='ec-solid-gift'
            specialOffer={getSpecialOffer(productItem.characteristics)}
            productDetailUrl={this.getProductDetailUrl(
              productItem.id,
              productItem.name,
              productItem.productId
            )}
            outOfStock={getOutOfStock(
              productItem.unavailabilityReasonCodes,
              catalogConfiguration.preorder.considerInventory
            )}
            translation={productListTranslation}
            buttonText={getCTAText(
              productItem.unavailabilityReasonCodes,
              catalogConfiguration.preorder.considerInventory,
              productListTranslation
            )}
          />
        ))}
      </>
    );
  }

  // tslint:disable:cognitive-complexity
  render(): React.ReactNode {
    const { httpStatusCode } = this.props.commonError;
    const {
      staticContent,
      loading,
      dynamicLoading,
      filteredDevice
    } = this.props.landingPageState;
    const {
      translation,
      configuration,
      showModal,
      notificationData,
      openModal,
      error,
      sendStockNotification,
      setError,
      notificationLoading,
      notificationModalType,
      formConfiguration
    } = this.props;
    const { landingPage } = translation.cart;
    const {
      showMessageStrip,
      downloadAndroidLink,
      downloadIOSLink
    } = configuration.cms_configuration.modules.landingPage;

    const getOS = getMobileOperatingSystem();

    return (
      <>
        <Helmet
          title={translation.cart.global.landingPageTitle}
          meta={[
            {
              name: 'description',
              content: translation.cart.global.landingPageDescription
            }
          ]}
        />
        {!configuration.cms_configuration.global.showLandingPage ? (
          <Header />
        ) : staticContent && staticContent.includeHeader ? (
          <Header />
        ) : null}
        {httpStatusCode ? (
          <Error />
        ) : !configuration.cms_configuration.global.showLandingPage ? (
          <Wrap>
            <Link to='/basket'>View basket</Link>
            <br />
            <br />
            <Link to='/checkout/personal-info'>View checkout</Link>
            {isCategoryLandingPageRequired() ? (
              <>
                <br />
                <br />
                <Link to='/category/Devices'>View category</Link>
              </>
            ) : null}
            <br />
            <br />
            <Link to='/tariff/tariff-mobile-postpaid'>View tariff</Link>
          </Wrap>
        ) : (
          <StyledLandingWrapper>
            <CommonModal
              isOpen={showModal}
              deviceName={notificationData.deviceName}
              variantId={notificationData.variantId}
              errorMessage={error && error.message}
              sendStockNotification={sendStockNotification}
              setError={setError}
              notificationLoading={notificationLoading}
              modalType={notificationModalType}
              translation={translation.cart.productList}
              notificationMediumType={notificationData.type}
              notificationMediumValue={notificationData.mediumValue}
              openModal={openModal}
              formConfiguration={formConfiguration}
            />
            <StyledContentWrapper>
              <LandingPage
                showAppShell={loading}
                showDynamicAppShell={dynamicLoading}
                sectionItems={staticContent.data}
                onTabsClickHandler={this.dynamicContentHandler}
                onBannerClickHandler={this.onBannerClick}
                onCarouselClickHandler={this.onCarouselClick}
                onIconClickHandler={this.onIconClick}
                dynamicReactNode={this.generateDynamicContent(filteredDevice)}
              />
              {showMessageStrip && (
                <DownloadOneApp
                  heading={landingPage.heading}
                  downloadText={landingPage.downloadText}
                  downloadLink={
                    getOS === 'iOS' ? downloadIOSLink : downloadAndroidLink
                  }
                  subHeading={landingPage.subHeading}
                />
              )}
            </StyledContentWrapper>
            <BasketStripWrapper />
          </StyledLandingWrapper>
        )}
        {staticContent && staticContent.includeFooter && (
          <Footer pageName={PAGE_VIEW.HOME} />
        )}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState) => ({
  commonError: state.common.error,
  landingPageState: state.landingPage,
  translation: state.translation,
  configuration: state.configuration,
  showModal: state.productList.openModal,
  notificationData: state.productList.notificationData,
  error: state.productList.error,
  notificationLoading: state.productList.notificationLoading,
  notificationModalType: state.productList.notificationModal,
  formConfiguration: state.configuration.cms_configuration.global.loginFormRules
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  checkDynamicData(): void {
    dispatch(actions.checkDynamicData());
  },
  getDynamicContent(data: string): void {
    dispatch(actions.fetchDynamicContent(data));
  },
  setDeviceDetailedList(list: string): void {
    dispatch(commonActions.setDeviceDetailedList(list));
  },
  setNotificationData(data: INotificationData): void {
    dispatch(productListActions.setNotificationData(data));
  },
  openModal(isModalOpen: boolean): void {
    dispatch(productListActions.openModal(isModalOpen));
  },
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void {
    dispatch(productListActions.sendStockNotification(payload));
  },
  setError(error: IError): void {
    dispatch(productListActions.setError(error));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
  // tslint:disable-next-line: max-file-line-count
)(Home);
