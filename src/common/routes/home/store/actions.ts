import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@home/store/constants';
import { IDynamicContent, IStaticContent } from '@home/store/types';
import { IVariant } from '@productList/store/types';

export default {
  landingPageLoading: actionCreator<boolean>(CONSTANTS.LANDING_PAGE_LOADER),
  dynamicContentLoading: actionCreator<boolean>(
    CONSTANTS.DYNAMIC_CONTENT_LOADER
  ),
  fetchStaticContent: actionCreator<void>(CONSTANTS.GET_STATIC_CONTENT),
  fetchStaticContentSuccess: actionCreator<IStaticContent>(
    CONSTANTS.GET_STATIC_CONTENT_SUCCESS
  ),
  fetchStaticContentError: actionCreator<void>(
    CONSTANTS.GET_STATIC_CONTENT_ERROR
  ),
  fetchDynamicContent: actionCreator<string>(CONSTANTS.GET_DYNAMIC_CONTENT),
  fetchDynamicContentSuccess: actionCreator<IDynamicContent[]>(
    CONSTANTS.GET_DYNAMIC_CONTENT_SUCCESS
  ),
  fetchDynamicContentError: actionCreator<void>(
    CONSTANTS.GET_DYNAMIC_CONTENT_ERROR
  ),
  updateStaticTabData: actionCreator<string>(CONSTANTS.UPDATE_STATIC_TAB_DATA),
  checkDynamicData: actionCreator<void>(CONSTANTS.CHECK_DYNAMIC_DATA),
  setFilteredDevice: actionCreator<IVariant[]>(CONSTANTS.SET_FILTERED_DEVICE)
};
