export enum SECTION_TYPE {
  CAROUSEL = 'CAROUSEL',
  BANNER = 'BANNER',
  HTML = 'HTML',
  DEVICES = 'DEVICES',
  TARIFFS_AND_PLANS = 'TARIFFS_AND_PLANS',
  PRODUCTS = 'PRODUCTS',
  ICONS = 'ICONS'
}

export enum DEVICE_LIST_IMAGE_TYPE {
  FRONT = 'front'
}
