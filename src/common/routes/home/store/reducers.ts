import CONSTANTS from '@home/store/constants';
import initialState from '@home/store/state';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import { StateType } from 'typesafe-actions';
import {
  IDynamicContent,
  ILandingPageState,
  IStaticContentResponse
} from '@home/store/types';
import { updateStaticSelectedTab } from '@home/store/selector';
import { IVariant } from '@productList/store/types';

const reducers = {
  [CONSTANTS.LANDING_PAGE_LOADER]: (
    state: ILandingPageState,
    payload: boolean
  ) => {
    state.loading = payload;
  },
  [CONSTANTS.DYNAMIC_CONTENT_LOADER]: (
    state: ILandingPageState,
    payload: boolean
  ) => {
    state.dynamicLoading = payload;
  },
  [CONSTANTS.GET_STATIC_CONTENT]: (state: ILandingPageState) => {
    state.loading = true;
  },
  [CONSTANTS.GET_STATIC_CONTENT_ERROR]: (state: ILandingPageState) => {
    state.loading = false;
  },
  [CONSTANTS.GET_STATIC_CONTENT_SUCCESS]: (
    state: ILandingPageState,
    payload: IStaticContentResponse
  ) => {
    state.staticContent = payload;
  },
  [CONSTANTS.GET_DYNAMIC_CONTENT]: (state: ILandingPageState) => {
    state.dynamicLoading = true;
  },
  [CONSTANTS.GET_DYNAMIC_CONTENT_ERROR]: (state: ILandingPageState) => {
    state.dynamicLoading = false;
  },
  [CONSTANTS.GET_DYNAMIC_CONTENT_SUCCESS]: (
    state: ILandingPageState,
    payload: IDynamicContent[]
  ) => {
    state.dynamicContent = payload;
  },
  [CONSTANTS.UPDATE_STATIC_TAB_DATA]: (
    state: ILandingPageState,
    payload: string
  ) => {
    updateStaticSelectedTab(state.staticContent.data, payload);
  },
  [CONSTANTS.SET_FILTERED_DEVICE]: (
    state: ILandingPageState,
    payload: IVariant[]
  ) => {
    state.filteredDevice = payload;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  ILandingPageState,
  AnyAction
>;

export type LandingPageState = StateType<typeof withProduce>;
