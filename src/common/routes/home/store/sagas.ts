import actions from '@home/store/actions';
import { Effect } from 'redux-saga';
import constants from '@home/store/constants';
import { call, put, takeLatest } from 'redux-saga/effects';
import {
  fetchDynamicContentService,
  fetchStaticContentService
} from '@home/store/services';
import store from '@common/store';
import { IStaticContentResponse } from '@home/store/types';
import * as productListApi from '@common/types/api/productList';
import {
  checkForDynamicData,
  dynamicSortString,
  transformDynamicResponse
} from '@home/store/selector';
import { sendLandingPageProductImpressionEvent } from '@events/home/index';
import { getPopularDevice } from '@productList/store/transformer';

export type SagaIterator = IterableIterator<
  Effect | Effect[] | Promise<IStaticContentResponse | Error>
>;

export function* getStaticContentRequested(action: {
  type: string;
  payload: { oneApp: boolean };
}): Generator {
  try {
    if (!action.payload.oneApp) {
      const response = yield fetchStaticContentService();

      yield put(actions.fetchStaticContentSuccess(response));
      yield put(actions.landingPageLoading(false));
    }
  } catch (error) {
    yield put(actions.fetchStaticContentError());
  }
}

export function* checkDynamicData(): Generator {
  try {
    const staticContent = store.getState().landingPage.staticContent.data;

    const isDynamicAvailable = checkForDynamicData(staticContent);
    if (isDynamicAvailable) {
      const sortString = dynamicSortString(staticContent);
      yield put(actions.fetchDynamicContent(sortString));
    }
  } catch (error) {
    yield put(actions.fetchStaticContentError());
  }
}

export function* getDynamicContentRequested(action: {
  type: string;
  payload: string;
}): Generator {
  try {
    const { upFront, monthly } = store.getState().translation.cart.productList;
    const {
      itemsPerPage,
      categoryId
    } = store.getState().configuration.cms_configuration.modules.landingPage;
    const apiParams = {
      filters: [],
      currentPage: 0,
      itemsPerPage,
      categoryId,
      sortBy: action.payload,
      tariffId: null,
      loyalty: null,
      discountId: null,
      showFullPageError: false
    };
    yield put(actions.dynamicContentLoading(true));

    const responseData: productListApi.POST.IResponse = yield call<
      typeof apiParams
    >(fetchDynamicContentService, apiParams);
    const transformedResponseData = transformDynamicResponse(
      responseData,
      upFront,
      monthly
    );
    const popularDevices = getPopularDevice(responseData);

    yield put(actions.updateStaticTabData(action.payload));
    yield put(actions.fetchDynamicContentSuccess(transformedResponseData));
    yield put(actions.setFilteredDevice(popularDevices));
    sendLandingPageProductImpressionEvent(responseData, categoryId);
    yield put(actions.dynamicContentLoading(false));
  } catch (error) {
    yield put(actions.fetchDynamicContentError());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(constants.GET_STATIC_CONTENT, getStaticContentRequested);
  yield takeLatest(constants.GET_DYNAMIC_CONTENT, getDynamicContentRequested);
  yield takeLatest(constants.CHECK_DYNAMIC_DATA, checkDynamicData);
}
export default watcherSaga;
