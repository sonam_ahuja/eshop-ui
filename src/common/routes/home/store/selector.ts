import * as productListApi from '@common/types/api/productList';
import { IDataSection, IDynamicContent, IMetaData } from '@home/store/types';
import {
  eligibilityUnavailabilityReasonType,
  IAttachments,
  IImageURL,
  IProductListItem
} from '@productList/store/types';
import { DEVICE_LIST_IMAGE_TYPE, SECTION_TYPE } from '@home/store/enums';
import { IPrices } from '@tariff/store/types';
import { PRICE_TYPE } from '@tariff/store/enum';
import { ITEM_STATUS } from '@src/common/store/enums';

export const transformDynamicResponse = (
  productItems: productListApi.POST.IResponse,
  upfrontTranslation: string,
  monthlyTranslation: string
): IDynamicContent[] => {
  const dynamicDataArr: IDynamicContent[] = [];
  let categorySlug = '';

  if (
    productItems &&
    productItems.characteristics &&
    productItems.characteristics.length
  ) {
    const productCharacteristics = productItems.characteristics;
    productCharacteristics.forEach(item => {
      if (item.name === 'slug') {
        categorySlug = item.values && item.values[0] && item.values[0].value;
      }
    });
  }

  if (productItems && productItems.data && productItems.data.length) {
    productItems.data.forEach((item: IProductListItem) => {
      if (item.variant) {
        const variantData = item.variant;
        const imageData = fetchImageData(variantData.attachments);
        const monthlyData = fetchPriceData(
          variantData.prices,
          PRICE_TYPE.RECURRING
        );
        const upfrontData = fetchPriceData(
          variantData.prices,
          PRICE_TYPE.UPFRONT
        );
        const basePriceData = fetchPriceData(
          variantData.prices,
          PRICE_TYPE.BASE
        );
        const dynamicObj: IDynamicContent = {
          imageUrl: imageData.url,
          monthlyLabel: monthlyTranslation,
          monthlyValue: String(monthlyData.discountedValue) || '',
          upfrontLabel: upfrontTranslation,
          upfrontValue:
            upfrontData.discountedValue === 0
              ? String(basePriceData.discountedValue)
              : String(upfrontData.discountedValue),
          title: variantData.productName,
          id: variantData.id,
          name: variantData.name,
          productId: variantData.productId,
          categorySlug,
          link: `/${categorySlug}/${variantData.name}/${
            variantData.productId
          }/${variantData.id}`
        };

        dynamicDataArr.push(dynamicObj);
      }
    });
  }

  return dynamicDataArr;
};

export const fetchImageData = (attachments: IAttachments): IImageURL => {
  let imageUrl = { name: '', url: '' };

  if (attachments && attachments.thumbnail && attachments.thumbnail.length) {
    const thumbnail = attachments.thumbnail;
    imageUrl =
      thumbnail.find(item => item.name === DEVICE_LIST_IMAGE_TYPE.FRONT) ||
      imageUrl;
  }

  return imageUrl;
};

export const fetchPriceData = (
  variantPrices: IPrices[],
  priceType: PRICE_TYPE
): IPrices => {
  let priceData = {
    priceType: '',
    actualValue: 0,
    discounts: [
      {
        discount: 0,
        name: '',
        label: '',
        dutyFreeValue: 0,
        taxRate: 0,
        taxPercentage: 0
      }
    ],
    discountedValue: 0
  };

  if (variantPrices && variantPrices.length) {
    priceData =
      variantPrices.find(item => item.priceType === priceType) || priceData;
  }

  return priceData;
};

export const checkForDynamicData = (dataSection: IDataSection[]): boolean => {
  let isDynamicAvailable = false;

  if (dataSection && dataSection.length) {
    dataSection.forEach((item: IDataSection) => {
      if (item.type === SECTION_TYPE.DEVICES) {
        isDynamicAvailable = true;
      }
    });
  }

  return isDynamicAvailable;
};

export const dynamicSortString = (dataSection: IDataSection[]): string => {
  let sortByString = '';

  if (dataSection && dataSection.length) {
    dataSection.forEach((item: IDataSection) => {
      if (item.type === SECTION_TYPE.DEVICES) {
        item.metaData.forEach((metaItem: IMetaData) => {
          if (metaItem.isDefault) {
            sortByString = metaItem.id ? metaItem.id : '';
          }
        });
      }
    });
  }

  return sortByString;
};

export const updateStaticSelectedTab = (
  data: IDataSection[],
  sortString: string
) => {
  if (data && data.length) {
    data.forEach((item: IDataSection) => {
      if (item.type === SECTION_TYPE.DEVICES) {
        item.metaData.forEach((metaItem: IMetaData) => {
          metaItem.isDefault = metaItem.id === sortString;
        });
      }
    });
  }
};

export const isPreOrder = (
  eligibilityUnavailabilityReason: eligibilityUnavailabilityReasonType[],
  considerInventory: boolean
): boolean => {
  if (!eligibilityUnavailabilityReason.includes(ITEM_STATUS.PRE_ORDER)) {
    return false;
  }

  if (
    eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK) &&
    considerInventory
  ) {
    return false;
  }

  return true;
};
