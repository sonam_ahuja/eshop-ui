import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import {
  IStaticContentResponse,
} from '@home/store/types';
import { logError } from '@src/common/utils';
import { IError } from '@src/common/store/types/common';
import { CancelTokenSource } from 'axios';
import * as productListApi from '@common/types/api/productList';
import {
  getProductListingRequestTemplate
} from '@productList/utils/requestTemplate';

export interface IFiltersRequest {
  name: string;
  characteristicValues: string[];
  provideQuantity: boolean;
}

export interface IPayload {
  filters: IFiltersRequest[];
  currentPage: number;
  itemsPerPage: number;
  categoryId: string;
  sortBy: string;
  tariffId: string | null;
  loyalty: string | null;
  discountId: string | null;
  showFullPageError?: boolean;
  signal?: CancelTokenSource;
}

export const fetchStaticContentService = async (): Promise<IStaticContentResponse | IError> => {
  try {
    return await apiCaller[
      apiEndpoints.LANDING_PAGE.GET_STATIC_CONTENT.method.toLowerCase()
    ](
      apiEndpoints.LANDING_PAGE.GET_STATIC_CONTENT.url
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchDynamicContentService = async (
  payload: IPayload
): Promise<productListApi.POST.IResponse> => {
  const {
    currentPage,
    itemsPerPage,
    categoryId,
    sortBy,
    filters,
    tariffId,
    loyalty,
    discountId,
    showFullPageError,
    signal
  } = payload;
  const requestBody = getProductListingRequestTemplate(
    currentPage,
    itemsPerPage,
    categoryId,
    filters,
    tariffId,
    loyalty,
    discountId
  );

  const apiUrl = sortBy
    ? `${apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url}?sortBy=${sortBy}`
    : apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url;

  try {
    return await apiCaller.post<productListApi.POST.IResponse>(
      apiUrl,
      requestBody,
      {
        showFullPageError: !!showFullPageError,
        cancelToken: signal && signal.token
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};
