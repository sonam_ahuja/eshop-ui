import { ILandingPageState } from '@home/store/types';

export default (): ILandingPageState => ({
  loading: true,
  dynamicLoading: true,
  staticContent: {
    data: [],
    visibility: [],
    status: '',
    startDate: 0,
    endDate: 0,
    includeFooter: true,
    includeHeader: true
  },
  dynamicContent: [],
  filteredDevice: []
});
