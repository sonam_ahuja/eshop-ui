import { SECTION_TYPE } from '@home/store/enums';
import { IVariant } from '@productList/store/types';

export interface IMetaData {
  title: string;
  description: string;
  ctaTitle: string;
  ctaDeeplink: string;
  imageUrl: string;
  value: string;
  textColor?: string;
  className?: string;
  id?: string;
  name?: string;
  isDefault?: boolean;
}

export interface IDataSection {
  type: SECTION_TYPE;
  id: string;
  order: number;
  metaData: IMetaData[];
}

export interface IStaticContentResponse {
  data: IDataSection[];
  visibility: string[];
  status: string;
  startDate: number;
  endDate: number;
  includeHeader: boolean;
  includeFooter: boolean;
}

export interface IStaticContent {
  data: IDataSection[];
  visibility: string[];
  status: string;
  startDate: number;
  endDate: number;
  includeHeader: boolean;
  includeFooter: boolean;
}

export interface ILandingPageState {
  loading: boolean;
  dynamicLoading: boolean;
  staticContent: IStaticContent;
  dynamicContent: IDynamicContent[];
  filteredDevice: IVariant[];
}

export interface IDynamicContent {
  imageUrl: string;
  title: string;
  monthlyLabel: string;
  monthlyValue: string;
  upfrontLabel: string;
  upfrontValue: string;
  id: string;
  variant?: IVariantResponse[];
  name: string;
  productId: string;
  categorySlug: string;
  link?: string;
}

export interface IVariantResponse {
  variantSize: string;
  value: string;
}
