import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

import { StyledCard } from '../productList/common/Card/styles';

export const StyledContentWrapper = styled.div`
  width: 100%;
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: calc(100% - 5.75rem);
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: calc(100% - 4.75rem);
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    /* width: calc(100% - 5.75rem); */
    width: 100%;
  }
`;

export const StyledLandingWrapper = styled.div`
  display: flex;
  flex: 1;
  overflow-x: hidden;
  .landingPageWrapper {
    min-height: 100vh;
    width: 100%;
  }
  /************* Product-card fixes **************/
  ${StyledCard} {
    width: 50%;
    flex-shrink: 0;
    padding-right: 0.25rem;
    margin-bottom: 0.25rem;
    position: relative;
    &:last-child {
      padding-right: 0;
      &:after {
        content: '';
        width: 1.25rem;
        height: 1px;
        position: absolute;
        right: -1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    ${StyledCard} {
      width: 25%;
      &:last-child {
        &:after {
          display: none;
        }
      }
    }
  }
  /************* Product-card fixes **************/

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    overflow-x: initial;
  }
`;
