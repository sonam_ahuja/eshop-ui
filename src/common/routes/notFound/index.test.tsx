import React from 'react';
import { mount } from 'enzyme';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { RootState } from '@common/store/reducers';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'dt-components';

import NotFound from '.';
import { IProps } from './types';
import { mapDispatchToProps, mapStateToProps } from './mapProps';

describe('<NotFound />', () => {
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const store = mockStore(initStateValue);
  const props: IProps = {
    common: appState().common,
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    location: {
      pathname: '/basket',
      search: 'code=URLUtils',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    },
    removeGenricError: jest.fn(),
    setGenricError: jest.fn()
  };

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NotFound {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const wrapper = componentWrapper(props);
    expect(wrapper).toMatchSnapshot();
  });

  test('renders without crashing', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <NotFound />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('dispatch action test', () => {
    const dispatch = jest.fn();
    const mapDispatch = mapDispatchToProps(dispatch);
    mapDispatch.removeGenricError();
    mapDispatch.setGenricError({ code: '', retryable: false });
  });
});
