/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { ReactNode } from 'react';
import { connect } from 'react-redux';
import ErrorPage from '@src/common/components/Error';
import Header from '@src/common/components/Header';
import Footer from '@src/common/components/Footer';
import styled from 'styled-components';
import { PAGE_VIEW } from '@src/common/events/constants/eventName';
import { pageViewEvent } from '@events/index';
import { RouteComponentProps, withRouter } from 'react-router';
import { RootState } from '@src/common/store/reducers';

import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IMapDispatchToProps, IMapStateToProps, IProps } from './types';

export const StyledNotFoundPage = styled.div`
  z-index: 999;
`;

class NotFound extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.NOT_FOUND);
  }

  render(): ReactNode {
    return (
      <StyledNotFoundPage>
        <Header />
        <ErrorPage />
        <Footer pageName={PAGE_VIEW.NOT_FOUND} />
      </StyledNotFoundPage>
    );
  }
}

export default withRouter(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(NotFound)
);
