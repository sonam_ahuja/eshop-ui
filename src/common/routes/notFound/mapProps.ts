import commonAction from '@common/store/actions/common';
import { Dispatch } from 'redux';
import { IError as IGenricError } from '@common/store/types/common';
import { RootState } from '@src/common/store/reducers';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  common: state.common
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  removeGenricError(): void {
    dispatch(commonAction.removeGenricError());
  },
  setGenricError(payload: IGenricError): void {
    dispatch(commonAction.showGenricError(payload));
  }
});
