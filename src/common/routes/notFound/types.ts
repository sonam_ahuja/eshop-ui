import { RouteComponentProps } from 'react-router';
import {
  ICommonState,
  IError as IGenricError
} from '@common/store/types/common';

export interface IMapDispatchToProps {
  removeGenricError(): void;
  setGenricError(payload: IGenricError): void;
}

export interface IMapStateToProps {
  common: ICommonState;
}

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export type IProps = RouteComponentProps<IRouterParams> &
  IMapStateToProps &
  IMapDispatchToProps;
