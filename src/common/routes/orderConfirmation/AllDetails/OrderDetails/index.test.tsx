import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import OrderDetails, {
  IProps
} from '@orderConfirmation/AllDetails/OrderDetails';
import translationInitialState from '@src/common/store/states/translation';

describe('<OrderDetails />', () => {
  test('should render properly', () => {
    const props: IProps = {
      orderId: '123',
      email: 'someemail@gmail.com',
      contact: '+199998XXXXXX',
      orderConfirmationTranslation: translationInitialState().cart.checkout
        .orderConfirmation,
      cartItems: []
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <OrderDetails {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
