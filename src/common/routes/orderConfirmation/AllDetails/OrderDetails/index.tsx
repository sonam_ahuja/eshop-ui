import React, { ReactNode } from 'react';
import { Paragraph, Section } from 'dt-components';
import ImageBox from '@orderConfirmation/common/ImageBox';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';
import { IBasketItem, IProduct } from '@src/common/routes/basket/store/types';
import { convertCartItems } from '@orderConfirmation/utils';

import { StyledOrderDetails } from './styles';

export interface IProps {
  orderId: string;
  email: string;
  contact: string;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
  cartItems: IBasketItem[];
}

const renderProductImages = (productList: IProduct[]) => {
  const productArr: ReactNode[] = [];

  productList.forEach((item: IProduct) => {
    productArr.push(
      <ImageBox className='imageBox' leftImageUrl={item.imageUrl || ''} />
    );
  });

  return productArr;
};

const OrderDetails = (props: IProps) => {
  const {
    orderId,
    email,
    contact,
    orderConfirmationTranslation,
    cartItems
  } = props;

  return (
    <StyledOrderDetails>
      <Paragraph className='title' size='small' weight='bold'>
        {orderConfirmationTranslation.orderDetails}
      </Paragraph>

      <div className='description'>
        <Section size='large'>
          <div>
            <span>{orderConfirmationTranslation.orderId}:</span>{' '}
            <span className='orderId'>{orderId}</span>
          </div>
          <div>
            <span>{orderConfirmationTranslation.email}:</span>{' '}
            <span>{email}</span>
          </div>
          <div>
            <span>{orderConfirmationTranslation.contact}:</span>{' '}
            <span>{contact}</span>
          </div>
        </Section>
        {/* <Anchor className='link' size='small'>
          {orderConfirmationTranslation.viewDetails}
        </Anchor> */}
      </div>
      <div className='boxWrap'>
        <div className='hScroll'>
          <div className='horizontalScrollRowInner'>
            {renderProductImages(convertCartItems(cartItems))}
          </div>
        </div>
      </div>
    </StyledOrderDetails>
  );
};

export default OrderDetails;
