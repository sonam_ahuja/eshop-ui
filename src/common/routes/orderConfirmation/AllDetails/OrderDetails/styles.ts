import styled from 'styled-components';
import { colors } from '@src/common/variables';

import { StyledDetailCard } from '../styles';

export const StyledOrderDetails = styled(StyledDetailCard)`
  background: ${colors.fogGray};

  .boxWrap {
    display: flex;
    .imageBox {
      margin: 0 0.125rem;
      flex: 1;
      min-width: calc(50% - 0.25rem);
    }
  }

  .orderId {
    word-break: break-word;
  }

  .hScroll {
    /* HORIZONTAL SCROLL START */
    display: flex;
    flex-wrap: nowrap;
    /* Make this scrollable when needed */
    overflow-x: auto;
    /* We don't want vertical scrolling */
    overflow-y: hidden;
    /* Make an auto-hiding scroller for the 3 people using a IE */
    -ms-overflow-style: -ms-autohiding-scrollbar;
    scroll-behavior: smooth;
    /* For WebKit implementations, provide inertia scrolling */
    -webkit-overflow-scrolling: touch;
    /* We don't want internal inline elements to wrap */
    /* white-space: nowrap; */
    /* Remove the default scrollbar for WebKit implementations */

    width: 100%;

    &::-webkit-scrollbar {
      display: none;
      width: 0 !important ;
      scrollbar-width: none;
      overflow: -moz-scrollbars-none;
    }
    &,
    * {
      scrollbar-width: none;
      overflow: -moz-scrollbars-none;
    }
    /* HORIZONTAL SCROLL END */

    .horizontalScrollRowInner {
      display: flex;
      width: 100%;
    }
  }
`;
