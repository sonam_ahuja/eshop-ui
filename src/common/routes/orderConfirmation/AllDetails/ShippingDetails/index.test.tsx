import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import ShippingDetails, {
  IProps
} from '@orderConfirmation/AllDetails/ShippingDetails';
import translationInitialState from '@src/common/store/states/translation';

describe('<ShippingDetails />', () => {
  test('should render properly', () => {
    const props: IProps = {
      estimatedDelivery: '02/02/2019',
      shippingType: 'standard',
      shippingAddress: 'some address',
      orderTrackingUrl: '',
      showTrackingLink: false,
      orderConfirmationTranslation: translationInitialState().cart.checkout
        .orderConfirmation
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ShippingDetails {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
