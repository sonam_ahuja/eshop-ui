import React from 'react';
import { Paragraph, Section, ShapeLink } from 'dt-components';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import { sendTrackOrderEvent } from '@src/common/events/checkout';

import { StyledShippingDetails } from './styles';

export interface IProps {
  estimatedDelivery: string;
  shippingType: string;
  shippingAddress: string;
  orderTrackingUrl: string;
  showTrackingLink: boolean;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
}

const ShippingDetails = (props: IProps) => {
  const {
    shippingType,
    shippingAddress,
    orderConfirmationTranslation,
    orderTrackingUrl,
    showTrackingLink
  } = props;

  return (
    <StyledShippingDetails>
      <Paragraph className='title' size='small' weight='bold'>
        {orderConfirmationTranslation.shippingDetails}
      </Paragraph>

      <div className='description'>
        <Section size='large'>
          <div>
            <span>{orderConfirmationTranslation.estimateDelivery}: </span>{' '}
            <span>{orderConfirmationTranslation.estimateDeliveryValue} </span>
          </div>
          <div>
            <span>
              {orderConfirmationTranslation.deliveryType[shippingType]}
            </span>
          </div>
          <div>
            <span>{orderConfirmationTranslation.shippingTo}: </span>
            <span>{shippingAddress}</span>
          </div>
        </Section>
      </div>
      {showTrackingLink && (
        <div className='boxWrap' onClick={() => sendTrackOrderEvent()}>
          <ShapeLink
            label={orderConfirmationTranslation.trackYourOrder}
            data-event-id={EVENT_NAME.CHECKOUT.EVENTS.TRACK_ORDER}
            data-event-message={orderConfirmationTranslation.trackYourOrder}
            iconName='ec-transporter-right-solid'
            href={orderTrackingUrl}
          />
          {/* <NavLink label='Add to Calendar' iconName='ec-solid-calendar' /> */}
        </div>
      )}
    </StyledShippingDetails>
  );
};

export default ShippingDetails;
