import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledDetailCard } from '../styles';

export const StyledShippingDetails = styled(StyledDetailCard)`
  background: ${colors.coldGray};

  .boxWrap {
    .dt_shapeLink {
      height: 3rem;
      padding-left: 0.875rem;
      padding-right: 0.875rem;
      margin-top: 0.25rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .boxWrap {
      .dt_shapeLink {
        padding: 1rem;
        .dt_paragraph {
          font-size: 0.75rem;
          font-weight: bold;
          font-style: normal;
          font-stretch: normal;
          line-height: 1.33;
          letter-spacing: normal;
        }
      }
    }
  }
`;
