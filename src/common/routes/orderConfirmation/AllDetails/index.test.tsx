import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import AllDetails, { IProps } from '@orderConfirmation/AllDetails';
import translationInitialState from '@src/common/store/states/translation';
import configurationState from '@src/common/store/states/configuration';
import initialOrderConfirmationState from '@routes/orderConfirmation/store/state';
const props: IProps = {
  className: 'someClassName',
  orderConfirmationState: initialOrderConfirmationState(),
  orderConfirmationTranslation: translationInitialState().cart.checkout
    .orderConfirmation,
  configuration: configurationState()
};
describe('<AllDetails />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <AllDetails {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
