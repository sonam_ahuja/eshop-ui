import React from 'react';
import { IOrderConfirmationState } from '@routes/orderConfirmation/store/types';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';

import { SHIPPING_TYPE } from '../../checkout/store/enums';

import { StyledAllDetails } from './styles';
import OrderDetails from './OrderDetails';
import ShippingDetails from './ShippingDetails';

export interface IProps {
  className?: string;
  orderConfirmationState: IOrderConfirmationState;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
  configuration: IConfigurationState;
}
const AllDetails = (props: IProps) => {
  const {
    className,
    orderConfirmationState,
    orderConfirmationTranslation,
    configuration
  } = props;
  const {
    orderId,
    personalInfo,
    shippingDetails,
    trackingLink
  } = orderConfirmationState;

  return (
    <StyledAllDetails className={className}>
      <OrderDetails
        orderId={orderId}
        email={personalInfo.email}
        contact={personalInfo.contact}
        orderConfirmationTranslation={orderConfirmationTranslation}
        cartItems={orderConfirmationState.cartItems}
      />
      {(shippingDetails.deliveryType === SHIPPING_TYPE.EXACT_DAY ||
        shippingDetails.deliveryType === SHIPPING_TYPE.STANDARD ||
        shippingDetails.deliveryType === SHIPPING_TYPE.SAME_DAY) && (
        <ShippingDetails
          estimatedDelivery={shippingDetails.estimatedDelivery}
          shippingType={shippingDetails.deliveryType}
          shippingAddress={shippingDetails.shippingTo}
          orderConfirmationTranslation={orderConfirmationTranslation}
          orderTrackingUrl={trackingLink}
          showTrackingLink={
            configuration.cms_configuration.modules.checkout.orderConfirmation
              .showTrackingLink
          }
        />
      )}
    </StyledAllDetails>
  );
};

export default AllDetails;
