import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledDetailCard = styled.div`
  padding: 2.5rem 2rem 1.5rem;

  .title {
    color: ${colors.darkGray};
  }
  .description {
    margin: 1rem 0 3.125rem;

    .link {
      margin-top: 4px;
    }
  }
  .boxWrap {
    margin: 0 -0.75rem;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    min-width: 50%;
    flex-shrink: 0;
    flex: 1;
    display: flex;
    flex-direction: column;
    .boxWrap {
      margin-top: auto;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding-bottom: 1.25rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 4.5rem 1.5rem 3rem;
    .boxWrap {
      margin-left: 0;
      margin-right: 0;
    }

    .title {
      font-size: 0.625rem;
      line-height: 1.2;
    }
    .description {
      > div {
        font-size: 0.625rem;
        line-height: 1.2;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 4rem 1.5rem 3.5rem;
  }
`;

export const StyledAllDetails = styled.div`
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    flex: 1;
    max-width: 100%;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    max-width: 66.67%;
  }
`;
