import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import GetMoreWithYourOrder, {
  IProps
} from '@orderConfirmation/GetMoreWithYourOrder';

describe('<GetMoreWithYourOrder />', () => {
  const props: IProps = {
    className: 'someClassName',
    titleText: 'title',
    descriptionText: 'descriptionText'
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <GetMoreWithYourOrder {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without className', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <GetMoreWithYourOrder {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
