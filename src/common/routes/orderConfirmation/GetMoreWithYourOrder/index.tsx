import React from 'react';
import { Paragraph } from 'dt-components';

// tslint:disable-next-line:no-commented-code
// import DiscountBox from '../common/DiscountBox';

import { StyledGetMore } from './styles';

export interface IProps {
  className?: string;
  titleText: string;
  descriptionText: string;
}

const GetMoreWithYourOrder = (props: IProps) => {
  const { className, titleText, descriptionText } = props;

  return (
    <StyledGetMore className={className}>
      <div className='textWrap'>
        <Paragraph className='title' size='small' weight='bold'>
          {titleText}
        </Paragraph>
        <Paragraph weight='bold' size='medium' className='description'>
          {descriptionText}
        </Paragraph>
      </div>

      {/* <div className='cardWrap'>
        <DiscountBox
          discount='20%'
          imageUrl='https://i.ibb.co/fnpy9RM/cards.png'
        />
      </div> */}

      {/* <VideoBox videoUrl='' />
      <ImageWithDescriptionBox
        imageUrl='https://i.ibb.co/fnpy9RM/cards.png'
        heading='samsung'
        subHeading='subheading'
        upfronAmount='4109211 ft'
      /> */}
    </StyledGetMore>
  );
};

export default GetMoreWithYourOrder;
