import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const StyledGetMore = styled.div`
  background: ${colors.magenta};
  padding: 2.5rem 1.25rem 1.5rem;

  .textWrap {
    .title {
      color: ${hexToRgbA(colors.darkGray, 0.4)};
      margin-bottom: 0.6875rem;
      margin-left: 0.75rem;
    }

    .description {
      color: ${colors.white};
      margin-bottom: 2.9375rem;
      margin-left: 0.75rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    align-items: center;
    padding-bottom: 2.5rem;

    .textWrap,
    .cardWrap {
      min-width: 50%;
      max-width: 50%;
    }

    .textWrap {
      .description {
        margin-bottom: 0;
      }
    }

    .cardWrap {
      padding-left: 1rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    min-height: 13.625rem;
    align-items: flex-start;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 4.5rem 1.5rem 3rem;

    min-width: 33.33%;
    max-width: 33.33%;

    flex-direction: column;
    justify-content: space-between;

    .textWrap,
    .cardWrap {
      min-width: 100%;
      max-width: 100%;
    }

    .cardWrap {
      padding-left: 0;
    }

    .textWrap {
      .title {
        padding-left: 0;
        font-size: 0.625rem;
        line-height: 1.2;
        margin-left: 0;
      }
      .description {
        padding-left: 0;
        margin-left: 0;
        font-size: 17.5px;
        line-height: 1.43;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 4rem 1.5rem 3.5rem;
  }
`;
