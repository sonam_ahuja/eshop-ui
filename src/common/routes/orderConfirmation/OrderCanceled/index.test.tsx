import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import OrderCanceled, { IProps } from '@orderConfirmation/OrderCanceled';
import appState from '@store/states/app';

describe('<OrderCanceled />', () => {
  const props: IProps = {
    className: 'string',
    orderNumber: 'string',
    userName: 'string',
    orderConfirmationTranslation: appState().translation.cart.checkout
      .orderConfirmation
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <OrderCanceled {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without className', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <OrderCanceled {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
