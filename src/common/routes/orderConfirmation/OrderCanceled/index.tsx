import React from 'react';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';

import {
  StyledDescription,
  StyledHeadingText,
  StyledOrderCanceled
} from './styles';

export interface IProps {
  className?: string;
  orderNumber: string;
  userName: string;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
}
const OrderCanceled = (props: IProps) => {
  const {
    className,
    orderNumber,
    orderConfirmationTranslation,
    userName
  } = props;
  const {
    orderCanceledText,
    nameText,
    cancelDescription
  } = orderConfirmationTranslation;

  return (
    <StyledOrderCanceled className={className}>
      <StyledHeadingText>
        {nameText.replace('{0}', userName)}
        <p className='orderNumber'>{orderNumber}</p>
        {orderCanceledText}
      </StyledHeadingText>

      <StyledDescription>{cancelDescription}</StyledDescription>
    </StyledOrderCanceled>
  );
};

export default OrderCanceled;
