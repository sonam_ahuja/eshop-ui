import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Section, Title } from 'dt-components';

export const StyledHeadingText = styled(Title).attrs({
  size: 'large',
  weight: 'ultra'
})`
  color: ${colors.darkGray};
  padding-right: 3.25rem;
  .orderNumber {
    color: ${colors.magenta};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-right: 0;
  }
`;

export const StyledDescription = styled(Section).attrs({
  size: 'large'
})`
  color: ${colors.ironGray};
  padding: 6.25rem 3.125rem 2rem 1.875rem;

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0;
    min-height: 6rem;
    padding-top: 0.15rem;
    padding-right: 1.45rem;

    /* font attr update */
    font-size: 0.625rem;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
  }
`;

export const StyledOrderCanceled = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;
`;
