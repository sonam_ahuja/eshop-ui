import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import OrderRecieved from '@orderConfirmation/OrderReceived';
import translationInitialState from '@src/common/store/states/translation';
import appState from '@store/states/app';

describe('<OrderRecieved />', () => {
  test('should render properly', () => {
    const initStateValue = appState();

    const component = mount(
      <ThemeProvider theme={{}}>
        <OrderRecieved
          status={'acknowledged'}
          orderConfirmationState={initStateValue.orderConfirmation}
          orderConfirmationTranslation={
            translationInitialState().cart.checkout.orderConfirmation
          }
        />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
