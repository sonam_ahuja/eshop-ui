import React from 'react';
import { Paragraph, Section } from 'dt-components';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';
import { ORDER_STATUS_TYPE } from '@routes/orderConfirmation/store/enum';

import { SHIPPING_TYPE } from '../../checkout/store/enums';
import { IOrderConfirmationState } from '../store/types';

import { AppProgressStepper, StyledOrderRecieved } from './styles';

export interface IProps {
  orderConfirmationTranslation: IOrderConfirmationTranslation;
  status: string;
  orderConfirmationState: IOrderConfirmationState;
}
const OrderRecieved = (props: IProps) => {
  const {
    orderConfirmationTranslation,
    status,
    orderConfirmationState
  } = props;
  const { shippingDetails } = orderConfirmationState;

  return (
    <StyledOrderRecieved>
      <div className='textWrap'>
        <Paragraph size='small' weight='bold' className='title'>
          {orderConfirmationTranslation.orderReceived}
        </Paragraph>
        <Section className='description' size='large'>
          {orderConfirmationTranslation.emailSent}
        </Section>
      </div>
      {(shippingDetails.deliveryType === SHIPPING_TYPE.EXACT_DAY ||
        shippingDetails.deliveryType === SHIPPING_TYPE.STANDARD ||
        shippingDetails.deliveryType === SHIPPING_TYPE.SAME_DAY) && (
          <AppProgressStepper
            steps={[
              {
                iconName: 'ec-confirm',
                activeIconName: 'ec-confirm'
              },
              {
                iconName: 'ec-transporter-right',
                activeIconName: 'ec-transporter-right'
              },
              {
                iconName: 'ec-home',
                activeIconName: 'ec-home'
              }
            ]}
            activeStep={ORDER_STATUS_TYPE[status]}
            completedStep={ORDER_STATUS_TYPE[status]}
          />
        )}
    </StyledOrderRecieved>
  );
};

export default OrderRecieved;
