import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { ProgressStepper } from 'dt-components';

export const AppProgressStepper = styled(ProgressStepper)`
  &.dt_progressStepper {
    position: relative;
    height: auto;
    background: none;
    z-index: 2;
    padding: 0;

    .dt_title,
    .lockIcon {
      display: none;
    }
    .dt_divider {
      color: ${colors.silverGray};
      height: 2px;
      min-height: 2px;
      &:before {
        border-width: 2px;
      }
    }

    .circleDiv.disabled {
      width: 40px;
      height: 40px;
      background: ${colors.silverGray};
      box-shadow: none;

      .dt_icon {
        font-size: 20px;
        path {
          fill: ${colors.steelGray};
        }
      }
    }

    .circleDiv.active {
      background: ${colors.magenta};
    }
    .active .dt_icon path {
      fill: ${colors.white};
    }

    @media (min-width: ${breakpoints.tabletPortrait}px) {
      margin-right: -2rem;
      padding-left: 0.75rem;
    }
    @media (min-width: ${breakpoints.tabletLandscape}px) {
      margin-right: 0;
      padding-left: 0;
    }
  }
`;

export const StyledOrderRecieved = styled.div`
  padding: 2.5rem 1.25rem 2rem 2rem;

  .textWrap {
    padding-right: 2rem;
    margin-bottom: 1.25rem;
    .title {
      color: ${colors.darkGray};
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-right: 2rem;
    display: flex;
    width: 100%;

    .textWrap {
      margin-bottom: 0;
      min-width: 50%;
      max-width: 50%;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: block;
    padding: 0;

    .textWrap {
      margin-bottom: 1.25rem;
      padding-right: 0;
      min-width: 100%;
      max-width: 100%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 0.5rem;

    .textWrap {
      margin-bottom: 1.1rem;

      .title {
        font-size: 0.75rem;
        line-height: 1.33;
      }
      .description {
        font-size: 0.625rem;
        line-height: 1.2;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .textWrap {
      margin-bottom: 1.5rem;
    }
  }
`;
