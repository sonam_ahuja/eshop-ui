import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import PaymentPending, { IProps } from '@orderConfirmation/PaymentPending';
import appState from '@store/states/app';

describe('<PaymentPending />', () => {
  const props: IProps = {
    className: 'string',
    showP24Button: true,
    p24Link: 'string',
    orderConfirmationTranslation: appState().translation.cart.checkout
      .orderConfirmation
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentPending {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without className', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentPending {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without p24Link', () => {
    const newProps: IProps = { ...props, p24Link: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentPending {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
