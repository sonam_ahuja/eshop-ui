import React from 'react';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';
import { Anchor } from 'dt-components';
import appConstants from '@src/common/constants/appConstants';

import {
  StyledActions,
  StyledDescription,
  StyledHeadingText,
  StyledPaymentPending
} from './styles';

export interface IProps {
  className?: string;
  showP24Button: boolean;
  p24Link?: string;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
}
const PaymentPending = (props: IProps) => {
  const {
    className,
    orderConfirmationTranslation,
    showP24Button,
    p24Link
  } = props;
  const {
    pleaseWaitText,
    paymentPendingText,
    p24ButtonText,
    makePaymentText,
    pendingDescription
  } = orderConfirmationTranslation;

  const leftButtonEl = (
    <Anchor
      size='medium'
      hreflang={appConstants.LANGUAGE_CODE}
      type='primary'
      href={p24Link}
    >
      {p24ButtonText}
    </Anchor>
  );

  return (
    <StyledPaymentPending className={className}>
      <StyledHeadingText>
        {!showP24Button ? (
          <>
            <div>{pleaseWaitText}</div>
            <div>{paymentPendingText}</div>{' '}
          </>
        ) : (
          <div>{makePaymentText}</div>
        )}
      </StyledHeadingText>

      {!showP24Button ? (
        <StyledDescription>{pendingDescription}</StyledDescription>
      ) : (
        <StyledActions>
          <div className='buttonWrap'>{leftButtonEl}</div>
        </StyledActions>
      )}
    </StyledPaymentPending>
  );
};

export default PaymentPending;
