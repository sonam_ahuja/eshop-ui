import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import PaymentUnsuccessful, {
  IProps
} from '@orderConfirmation/PaymentUnsuccessful';
import appState from '@store/states/app';

describe('<PaymentUnsuccessful />', () => {
  const props: IProps = {
    className: 'string',
    orderNumber: 'string',
    rightButtonText: 'string',
    orderConfirmationTranslation: appState().translation.cart.checkout
      .orderConfirmation
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentUnsuccessful {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without className', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentUnsuccessful {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without rightButtonText', () => {
    const newProps: IProps = { ...props, rightButtonText: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentUnsuccessful {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without orderNumber', () => {
    const newProps: IProps = { ...props, orderNumber: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <PaymentUnsuccessful {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
