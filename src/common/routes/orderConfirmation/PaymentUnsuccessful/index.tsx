import React from 'react';
import { Button } from 'dt-components';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';

import {
  StyledActions,
  StyledDescription,
  StyledHeadingText,
  StyledPaymentUnsuccessful
} from './styles';

export interface IProps {
  className?: string;
  orderNumber?: string;
  rightButtonText?: string;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
}
const PaymentUnsuccessful = (props: IProps) => {
  const { className, rightButtonText, orderConfirmationTranslation } = props;
  const {
    paymentFailText,
    tryAgain,
    sorryText,
    paymentUnsuccessText
  } = orderConfirmationTranslation;

  const leftButtonEl = (
    <Button size='medium' type='primary'>
      {tryAgain}
    </Button>
  );
  const rightButtonEl = rightButtonText ? (
    <Button size='medium' type='secondary'>
      {rightButtonText}
    </Button>
  ) : null;

  return (
    <StyledPaymentUnsuccessful className={className}>
      <StyledHeadingText>
        <div>{sorryText}</div>
        <div>{paymentUnsuccessText}</div>
      </StyledHeadingText>

      <StyledActions>
        <StyledDescription>{paymentFailText}</StyledDescription>
        <div className='buttonWrap'>
          {leftButtonEl}
          {rightButtonEl}
        </div>
      </StyledActions>
    </StyledPaymentUnsuccessful>
  );
};

export default PaymentUnsuccessful;
