import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Section, Title } from 'dt-components';

export const StyledHeadingText = styled(Title).attrs({
  size: 'large',
  weight: 'ultra'
})`
  color: ${colors.darkGray};
  padding-right: 3.25rem;

  @media (min-width: ${breakpoints.desktop}px) {
    padding-right: 0;
  }
`;

export const StyledDescription = styled(Section).attrs({ size: 'large' })`
  color: ${colors.ironGray};
  padding: 5rem 0 0 1.875rem;

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0;

    /* font attr update */
    font-size: 0.625rem;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
  }
`;

export const StyledActions = styled.div`
  padding-right: 1.25rem;
  padding-bottom: 2rem;
  .buttonWrap {
    padding-top: 1.25rem;
    button {
      margin-top: 0.75rem;
      width: 100%;
      &:first-child {
        margin-top: 0;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0;
    min-height: 6rem;
    padding-top: 0.15rem;

    /* font attr update */
    font-size: 0.78125rem;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;

    .buttonWrap {
      display: flex;
      margin: 0 -0.5rem;

      button {
        width: auto;
        margin: 0 0.5rem;
      }
    }
  }
`;

export const StyledPaymentUnsuccessful = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;

  @media (min-width: ${breakpoints.desktop}px) {
    margin-right: -1.35rem;
  }
`;
