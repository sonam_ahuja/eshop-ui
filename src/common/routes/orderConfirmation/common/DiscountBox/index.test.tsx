import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import DiscountBox, { IProps } from '@orderConfirmation/common/DiscountBox';

describe('<DiscountBox />', () => {
  const props: IProps = {
    discount: '5%',
    imageUrl: 'http://localhost:3001/demo.jpg',
    className: 'someClassName',
    children: <>children node</>
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <DiscountBox {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without image url', () => {
    const newProps: IProps = { ...props, imageUrl: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <DiscountBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without classname', () => {
    const newProps: IProps = { ...props, className: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <DiscountBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly without childern', () => {
    const newProps: IProps = { ...props, children: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <DiscountBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
