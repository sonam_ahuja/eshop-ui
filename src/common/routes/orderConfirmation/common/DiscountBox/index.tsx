import React from 'react';
import { PreloadImage, Title } from 'dt-components';

import { StyledDiscountBox } from './styles';

export interface IProps {
  discount: string;
  imageUrl?: string;
  className?: string;
  children?: React.ReactNode;
}

const DiscountBox = (props: IProps) => {
  const { className, discount, imageUrl } = props;

  const imageEl = imageUrl && (
    <div className='image'>
      <PreloadImage imageUrl={imageUrl} imageHeight={10} imageWidth={10} />
    </div>
  );

  return (
    <StyledDiscountBox className={className}>
      {imageEl}
      <Title className='text' size='normal' weight='bold'>
        {discount}
        <span> Discount</span>
      </Title>
    </StyledDiscountBox>
  );
};

export default DiscountBox;
