import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledDiscountBox = styled.div`
  background: rgba(0, 0, 0, 0.3);
  color: ${colors.magenta};
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  padding: 1rem;

  .image {
    width: 2.25rem;
    height: 2.25rem;
  }

  .text {
    margin-top: 0.75rem;
    /* useless bottom margin */
    margin-bottom: 1px;
  }
`;
