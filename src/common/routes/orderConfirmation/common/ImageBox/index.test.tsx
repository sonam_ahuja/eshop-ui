import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import ImageBox, { IProps } from '@orderConfirmation/common/ImageBox';

describe('<ImageBox />', () => {
  const props: IProps = {
    leftImageUrl: 'http://localhost:3001/demo.jpg',
    rightImageUrl: 'http://localhost:3001/demo.jpg',
    className: 'someclassName',
    children: <>children node</>
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageBox {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without right image', () => {
    const newProps: IProps = { ...props, rightImageUrl: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without classname', () => {
    const newProps: IProps = { ...props, className: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly without childern', () => {
    const newProps: IProps = { ...props, children: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
