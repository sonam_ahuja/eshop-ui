import React from 'react';
import { IMAGE_TYPE } from '@src/common/store/enums';
import PreloadImageWrapper from '@src/common/components/Preload';

import { StyledImageBox } from './styles';

export interface IProps {
  leftImageUrl: string;
  rightImageUrl?: string;
  className?: string;
  children?: React.ReactNode;
}

const ImageBox = (props: IProps) => {
  const { className, leftImageUrl, rightImageUrl } = props;

  const rightImageEl = rightImageUrl && (
    <div className='rightImage'>
      <PreloadImageWrapper
        loadDefaultImage={true}
        isObserveOnScroll={true}
        imageHeight={50}
        mobileWidth={50}
        width={50}
        type={IMAGE_TYPE.JPEG}
        imageWidth={50}
        imageUrl={rightImageUrl}
        intersectionObserverOption={{
          root: null,
          rootMargin: '60px',
          threshold: 0
        }}
      />
    </div>
  );

  return (
    <StyledImageBox className={className}>
      <div className='leftImage'>
        <PreloadImageWrapper
          loadDefaultImage={true}
          isObserveOnScroll={true}
          imageHeight={50}
          mobileWidth={50}
          width={50}
          type={IMAGE_TYPE.JPEG}
          imageWidth={50}
          imageUrl={leftImageUrl}
          intersectionObserverOption={{
            root: null,
            rootMargin: '60px',
            threshold: 0
          }}
        />
      </div>
      {rightImageEl}
    </StyledImageBox>
  );
};

export default ImageBox;
