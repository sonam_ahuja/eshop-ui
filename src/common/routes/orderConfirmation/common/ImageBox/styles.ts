import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledImageBox = styled.div`
  background: ${colors.silverGray};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  padding: 0.75rem;
  height: 6.5rem;

  .leftImage {
    width: 5rem;
    height: 5rem;
    overflow: hidden;
  }

  .rightImage {
    width: 4rem;
    height: 4rem;
    overflow: hidden;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-left: 0;
    padding-right: 0;
    height: 6.25rem;
  }
`;
