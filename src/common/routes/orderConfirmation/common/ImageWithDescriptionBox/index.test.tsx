import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import ImageWithDescriptionBox, {
  IProps
} from '@orderConfirmation/common/ImageWithDescriptionBox';

describe('<ImageBox />', () => {
  const props: IProps = {
    imageUrl: 'http://localhost:3001/demo.jpg',
    heading: 'Heading',
    subHeading: 'Sub Heading',
    upfronAmount: '220',
    className: 'someClassName',
    children: <>childern Node</>
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageWithDescriptionBox {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without sub heading', () => {
    const newProps: IProps = { ...props, subHeading: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageWithDescriptionBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without classname', () => {
    const newProps: IProps = { ...props, className: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageWithDescriptionBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly without childern', () => {
    const newProps: IProps = { ...props, children: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <ImageWithDescriptionBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
