import React from 'react';
import { Paragraph, PreloadImage, Section } from 'dt-components';

import { StyledImageWithDescriptionBox } from './styles';

export interface IProps {
  imageUrl: string;
  heading: string;
  subHeading?: string;
  upfronAmount: string;
  className?: string;
  children?: React.ReactNode;
}

const ImageWithDescriptionBox = (props: IProps) => {
  const { className, imageUrl, heading, subHeading, upfronAmount } = props;

  return (
    <StyledImageWithDescriptionBox className={className}>
      <div className='textWrap'>
        <Paragraph className='heading' size='small' weight='bold'>
          {heading}
          <div>{subHeading}</div>
        </Paragraph>
        <div className='upfront'>
          <Section className='label' size='large'>
            upfront
          </Section>
          <Section className='amount' size='large' weight='bold'>
            {upfronAmount}
          </Section>
        </div>
      </div>
      <div className='image'>
        <PreloadImage imageUrl={imageUrl} imageHeight={10} imageWidth={10} />
      </div>
    </StyledImageWithDescriptionBox>
  );
};

export default ImageWithDescriptionBox;
