import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledImageWithDescriptionBox = styled.div`
  background: ${colors.silverGray};
  display: flex;
  justify-content: space-between;
  border-radius: 10px;

  .textWrap {
    padding: 0.75rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .heading {
      color: ${colors.darkGray};
    }

    .upfront {
      margin-top: 0.5rem;

      .label {
        color: ${colors.ironGray};
      }
      .amount {
        color: ${colors.magenta};
      }
    }
  }

  .image {
    width: 7rem;
    height: 7rem;
    margin-right: 1rem;
    overflow: hidden;
  }
`;
