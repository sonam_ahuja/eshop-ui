import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import VideoBox, { IProps } from '@orderConfirmation/common/VideoBox';
import { StyledPaddingContainer } from '@orderConfirmation/common/styles';

describe('<VideoBox />', () => {
  const props: IProps = {
    videoUrl: 'http://localhost:3001/demo.mp4',
    className: 'someClassName',
    children: <>childern Node</>
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <VideoBox {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('StyledPaddingContainer should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StyledPaddingContainer {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without classname', () => {
    const newProps: IProps = { ...props, className: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <VideoBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly without childern', () => {
    const newProps: IProps = { ...props, children: undefined };

    const component = mount(
      <ThemeProvider theme={{}}>
        <VideoBox {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
