import React from 'react';
import { Icon } from 'dt-components';

import { StyledVideoBox } from './styles';

export interface IProps {
  videoUrl: string;
  className?: string;
  children?: React.ReactNode;
}

const VideoBox = (props: IProps) => {
  const { className } = props;

  return (
    <StyledVideoBox className={className}>
      <Icon className='icon' size='large' name='ec-solid-plus' />
    </StyledVideoBox>
  );
};

export default VideoBox;
