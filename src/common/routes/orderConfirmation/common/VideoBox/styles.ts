import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledVideoBox = styled.div`
  background: ${colors.fogGray};
  border-radius: 10px;
  padding: 0.75rem;
  position: relative;
  min-height: 7rem;

  .icon {
    position: absolute;
    color: ${colors.magenta};
    left: 1rem;
    top: 1rem;
  }
`;
