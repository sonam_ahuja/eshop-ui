import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPaddingContainer = styled.div`
  padding: 2.5rem 1.25rem 1.25rem 1.25rem;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 3.5rem 1.5rem 3.5rem 1.5rem;
  }
`;
