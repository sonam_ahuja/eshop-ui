import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { PLACE_ORDER_CODE } from '@checkout/store/enums';

import {
  IProps,
  mapDispatchToProps,
  mapStateToProps,
  OrderConfirmation
} from '.';

describe('<OrderConfirmation />', () => {
  const initStateValue = appState();
  const props: IProps = {
    ...histroyParams,
    commonError: {
      code: '',
      retryable: false
    },
    orderConfirmationState: appState().orderConfirmation,
    orderConfirmationTranslation: appState().translation.cart.checkout
      .orderConfirmation,
    configuration: appState().configuration,
    isUserLoggedIn: true,
    fetchOrderDetails: jest.fn(),
    translation: appState().translation
  };

  const mockStore = configureStore();
  const store = mockStore(initStateValue);

  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <OrderConfirmation {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
  };

  test('should render properly', () => {
    const wapper = componentWrapper(props);
    expect(wapper).toMatchSnapshot();
  });
  test('should render properly with error code', () => {
    const commonError = {
      code: '',
      retryable: false,
      httpStatusCode: 501
    };
    const newProps = { ...props, commonError };
    const wapper = componentWrapper(newProps);
    expect(wapper).toMatchSnapshot();
  });
  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    mapDispatchToProps(dispatch).fetchOrderDetails('1234');
  });

  test('handle trigger', () => {
    const newProps = { ...props };
    newProps.location.search = '?id=adkda&as=21';
    newProps.orderConfirmationState.dataReceived = true;
    const component = mount<OrderConfirmation>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <OrderConfirmation {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).componentWillReceiveProps(newProps);

    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).fetchOrderDetail();

    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).renderOrderStatus(
        PLACE_ORDER_CODE.ORDER_CONFIRMED
      );
    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).renderOrderStatus(
        PLACE_ORDER_CODE.PAYMENT_PENDING
      );
    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).renderOrderStatus(
        PLACE_ORDER_CODE.ORDER_CANCELLED
      );
    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).renderOrderStatus(
        PLACE_ORDER_CODE.PAYMENT_FAILED
      );
    (component
      .find('OrderConfirmation')
      .instance() as OrderConfirmation).renderOrderStatus(
        PLACE_ORDER_CODE.PAYMENT_PENDING_FROM_USER
      );
  });
});
