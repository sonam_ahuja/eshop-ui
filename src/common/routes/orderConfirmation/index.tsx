import Error from '@src/common/components/Error';
import actions from '@routes/orderConfirmation/store/actions';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { RootState } from '@src/common/store/reducers';
import { IConfigurationState } from '@src/common/store/types/configuration';
import { IOrderConfirmationTranslation } from '@src/common/store/types/translation';
import { Title } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import { IError } from '@src/common/store/types/common';
import { RouteComponentProps, withRouter } from 'react-router';
import { IOrderConfirmationState } from '@routes/orderConfirmation//store/types';
import Header from '@common/components/Header';
import BasketStripWrapper from '@common/components/BasketStripWrapper';
import { pageViewEvent } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import Footer from '@src/common/components/FooterWithTermsAndConditions';
import { ITranslationState } from '@common/store/types/translation';

import { PLACE_ORDER_CODE } from '../checkout/store/enums';

import {
  StyledContentWrapper,
  StyledMainContent,
  StyledOrderConfirmation
} from './styles';
import OrderReceived from './OrderReceived';
import AllDetails from './AllDetails';
import GetMoreWithYourOrder from './GetMoreWithYourOrder';
import OrderCanceled from './OrderCanceled';
import PaymentPending from './PaymentPending';
import PaymentUnsuccessful from './PaymentUnsuccessful';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}
export interface IMapStateToProps {
  commonError: IError;
  orderConfirmationState: IOrderConfirmationState;
  orderConfirmationTranslation: IOrderConfirmationTranslation;
  configuration: IConfigurationState;
  isUserLoggedIn: boolean;
  translation: ITranslationState;
}

export interface IMapDispatchToProps {
  fetchOrderDetails(id: string, showFullPageError?: boolean): void;
}

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export class OrderConfirmation extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.fetchOrderDetail = this.fetchOrderDetail.bind(this);
    this.renderOrderStatus = this.renderOrderStatus.bind(this);
  }

  componentWillReceiveProps(nextProps: IProps): void {
    if (nextProps.isUserLoggedIn !== this.props.isUserLoggedIn) {
      this.fetchOrderDetail();
    }
  }

  componentDidMount(): void {
    pageViewEvent(PAGE_VIEW.CHECKOUT_ORDER_CONFIRMATION);
    this.fetchOrderDetail();
  }

  fetchOrderDetail(): void {
    const params = new URLSearchParams(
      decodeURIComponent(this.props.location.search)
    );

    if (params.get('id')) {
      this.props.fetchOrderDetails(params.get('id') as string, true);
    }
  }

  renderOrderStatus(activeStep: PLACE_ORDER_CODE): ReactNode {
    const { orderConfirmationTranslation, orderConfirmationState } = this.props;

    const {
      personalInfo,
      dataReceived,
      p24Link
    } = this.props.orderConfirmationState;

    const { thankYouMsg } = orderConfirmationTranslation;

    switch (activeStep) {
      case PLACE_ORDER_CODE.ORDER_CONFIRMED:
        return (
          <>
            {dataReceived && (
              <Title className='mainTitle' size='large' weight='ultra'>
                {thankYouMsg.replace('{0}', personalInfo.name)}
              </Title>
            )}
            <OrderReceived
              orderConfirmationTranslation={orderConfirmationTranslation}
              status={orderConfirmationState.orderStatus.subStatus}
              orderConfirmationState={orderConfirmationState}
            />
          </>
        );

      case PLACE_ORDER_CODE.PAYMENT_PENDING:
        return (
          <>
            <PaymentPending
              orderConfirmationTranslation={orderConfirmationTranslation}
              showP24Button={false}
            />
          </>
        );

      case PLACE_ORDER_CODE.ORDER_CANCELLED:
        return (
          <>
            <OrderCanceled
              userName={personalInfo.name}
              orderConfirmationTranslation={orderConfirmationTranslation}
              orderNumber={orderConfirmationState.orderId}
            />
          </>
        );

      case PLACE_ORDER_CODE.PAYMENT_FAILED:
        return (
          <>
            <PaymentUnsuccessful
              orderConfirmationTranslation={orderConfirmationTranslation}
            />
          </>
        );

      case PLACE_ORDER_CODE.PAYMENT_PENDING_FROM_USER:
        return (
          <>
            <PaymentPending
              orderConfirmationTranslation={orderConfirmationTranslation}
              showP24Button={true}
              p24Link={p24Link}
            />
          </>
        );

      default:
        return null;
    }
  }

  render(): ReactNode {
    const {
      orderConfirmationTranslation,
      orderConfirmationState,
      configuration,
      commonError
    } = this.props;
    const {
      getMoreDescriptionText,
      getMoreTitleText
    } = orderConfirmationTranslation;

    return (
      <>
        <Header showBasketStrip={false} />
        {commonError.httpStatusCode ? (
          <Error />
        ) : (
          <>
            <StyledOrderConfirmation>
              <StyledContentWrapper>
                <StyledMainContent>
                  <div className='informationBlock'>
                    {this.renderOrderStatus(
                      orderConfirmationState.orderStatus.status
                    )}
                  </div>
                  <div className='AllDetailsAndGetMoreWrap'>
                    {
                      <AllDetails
                        orderConfirmationState={orderConfirmationState}
                        orderConfirmationTranslation={
                          orderConfirmationTranslation
                        }
                        configuration={configuration}
                      />
                    }
                    <GetMoreWithYourOrder
                      titleText={getMoreTitleText}
                      descriptionText={getMoreDescriptionText}
                    />
                  </div>
                </StyledMainContent>
                <Footer
                  className='darkBg footer'
                  termsAndConditionsUrl={
                    this.props.configuration.cms_configuration.global
                      .termsAndConditionsUrl
                  }
                  shouldTermsAndConditionsOpenInNewTab={
                    this.props.configuration.cms_configuration.global
                      .shouldTermsAndConditionsOpenInNewTab
                  }
                  globalTranslation={this.props.translation.cart.global}
                />
              </StyledContentWrapper>
              <BasketStripWrapper />
            </StyledOrderConfirmation>
          </>
        )}
      </>
    );
  }
}

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  commonError: state.common.error,
  orderConfirmationTranslation:
    state.translation.cart.checkout.orderConfirmation,
  orderConfirmationState: state.orderConfirmation,
  configuration: state.configuration,
  translation: state.translation,
  isUserLoggedIn: state.common.isLoggedIn
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  // tslint:disable-next-line:bool-param-default
  fetchOrderDetails(id: string, showFullPageError?: boolean): void {
    dispatch(actions.fetchOrderDetails({ id, showFullPageError }));
  }
});

export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(OrderConfirmation)
);
