import actions from '@orderConfirmation/store/actions';
import constants from '@orderConfirmation/store/constants';
import {} from '@orderConfirmation/store/enum';
import appState from '@store/states/app';

describe('Order Confirmation actions', () => {
  it('fetchOrderDetails action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.FETCH_ORDER_DETAILS_REQUESTED,
      payload: { id: 'orderDetail' }
    };
    expect(actions.fetchOrderDetails({ id: 'orderDetail' })).toEqual(
      expectedAction
    );
  });

  it('fetchOrderDetailsSuccess action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.FETCH_ORDER_DETAILS_SUCCESS,
      payload: appState().orderConfirmation
    };
    expect(
      actions.fetchOrderDetailsSuccess(appState().orderConfirmation)
    ).toEqual(expectedAction);
  });

  it('fetchOrderDetails action creator should return a object with expected value', () => {
    const expectedAction = {
      type: constants.FETCH_ORDER_DETAILS_ERROR,
      payload: undefined
    };
    expect(actions.fetchOrderDetailsError()).toEqual(expectedAction);
  });
});
