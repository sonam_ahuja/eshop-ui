import { ORDER_STATUS_TYPE } from '@orderConfirmation/store/enum';

describe('Order Confirmation actions', () => {
  it('ORDER_STATUS_TYPE test', () => {
    expect(ORDER_STATUS_TYPE).toBeDefined();
  });
});
