import orderConfirmationState from '@orderConfirmation/store/state';
import { orderConfirmationReducer } from '@orderConfirmation/store';

describe('Order review main reducer', () => {
  it('orderReviewReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(
      orderConfirmationReducer(orderConfirmationState(), definedAction)
    ).toEqual(orderConfirmationState());
  });
});
