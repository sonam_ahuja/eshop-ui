import orderConfirmation from '@orderConfirmation/store/reducers';
import orderConfirmationState from '@orderConfirmation/store/state';
import { IOrderConfirmationState } from '@orderConfirmation/store/types';
import actions from '@orderConfirmation/store/actions';
import {} from '@orderConfirmation/store/enum';
import appState from '@store/states/app';

describe('Order Confirmation Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IOrderConfirmationState = orderConfirmation(
    undefined,
    definedAction
  );
  let expectedState: IOrderConfirmationState = orderConfirmationState();

  const initializeValue = () => {
    initialStateValue = orderConfirmation(undefined, definedAction);
    expectedState = orderConfirmationState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('FETCH_ORDER_DETAILS_REQUESTED should mutate state while featching order details', () => {
    const expected: IOrderConfirmationState = {
      ...expectedState,
      loading: true
    };
    const action = actions.fetchOrderDetails({ id: 'featchDetails' });
    const telekomServiceStore = orderConfirmation(initialStateValue, action);
    expect(telekomServiceStore).toEqual(expected);
  });

  it('FETCH_ORDER_DETAILS_SUCCESS should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const expected: IOrderConfirmationState = {
      ...expectedState,
      dataReceived: true
    };
    const action = actions.fetchOrderDetailsSuccess(
      appState().orderConfirmation
    );
    const telekomServiceStore = orderConfirmation(initialStateValue, action);
    expect(telekomServiceStore).toEqual(expected);
  });

  it('FETCH_ORDER_DETAILS_ERROR should mutate state when TELEKOM_PRODUCT_SERVICE type dispatch', () => {
    const expected: IOrderConfirmationState = {
      ...expectedState
    };
    const action = actions.fetchOrderDetailsError();
    const telekomServiceStore = orderConfirmation(initialStateValue, action);
    expect(telekomServiceStore).toEqual(expected);
  });
});
