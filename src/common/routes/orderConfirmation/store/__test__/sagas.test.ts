import orderConfirmationSaga, {
  fetchOrderDetails,
  selectOtherDevice
} from '@orderConfirmation/store/sagas';
import { IFetchOrderDetails } from '@orderConfirmation/store/types';
import CONSTANT from '@orderConfirmation/store/constants';

describe('Order Confirmation Saga', () => {
  it('orderConfirmationSaga saga test', () => {
    const generator = orderConfirmationSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchOrderDetails saga test', () => {
    const action: {
      type: string;
      payload: IFetchOrderDetails;
    } = {
      type: CONSTANT.FETCH_ORDER_DETAILS_REQUESTED,
      payload: { id: 'detains' }
    };
    const generator = fetchOrderDetails(action);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('selectOtherDevice saga test', () => {
    const generator = selectOtherDevice();
    expect(generator.next().value).toBeUndefined();
  });
});
