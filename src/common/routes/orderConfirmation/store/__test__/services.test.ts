import { getOrderDetails } from '@orderConfirmation/store/services';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';

describe('Order Confirmation Service', () => {
  const mock = new MockAdapter(axios);
  it('getOrderDetails success test', async () => {
    const { url } = apiEndpoints.CHECKOUT.GET_ORDER_DETAILS;
    const addressPayload = {};
    mock.onPost(url).replyOnce(200, addressPayload);
    axios
      .post(url)
      .then(async () => {
        const result = await getOrderDetails('2135', false);
        expect(result).toEqual(addressPayload);
      })
      .catch(error => logError(error));
  });
});
