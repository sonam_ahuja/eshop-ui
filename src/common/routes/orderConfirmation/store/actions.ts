import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@routes/orderConfirmation/store/constants';
import {
  IFetchOrderDetails,
  IOrderConfirmationResponse
} from '@routes/orderConfirmation/store/types';

export default {
  fetchOrderDetails: actionCreator<IFetchOrderDetails>(
    CONSTANTS.FETCH_ORDER_DETAILS_REQUESTED
  ),
  fetchOrderDetailsSuccess: actionCreator<IOrderConfirmationResponse>(
    CONSTANTS.FETCH_ORDER_DETAILS_SUCCESS
  ),
  fetchOrderDetailsError: actionCreator<undefined>(
    CONSTANTS.FETCH_ORDER_DETAILS_ERROR
  )
};
