import { AnyAction, Reducer } from 'redux';
import {
  IOrderConfirmationResponse,
  IOrderConfirmationState
} from '@routes/orderConfirmation/store/types';
import initialState from '@routes/orderConfirmation/store/state';
import withProduce from '@utils/withProduce';
import CONSTANTS from '@routes/orderConfirmation/store/constants';
import { convertOrderConfirmationResponse } from '@routes/orderConfirmation/store/transformer';

const reducers = {
  [CONSTANTS.FETCH_ORDER_DETAILS_REQUESTED]: (
    state: IOrderConfirmationState
  ) => {
    state.loading = true;
  },
  [CONSTANTS.FETCH_ORDER_DETAILS_SUCCESS]: (
    state: IOrderConfirmationState,
    payload: IOrderConfirmationResponse
  ) => {
    convertOrderConfirmationResponse(state, payload);
    state.loading = false;
    state.dataReceived = true;
  },
  [CONSTANTS.FETCH_ORDER_DETAILS_ERROR]: (state: IOrderConfirmationState) => {
    state.loading = false;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IOrderConfirmationState,
  AnyAction
>;
