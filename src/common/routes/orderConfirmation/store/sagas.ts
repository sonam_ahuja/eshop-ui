import { put, takeLatest } from 'redux-saga/effects';
import CONSTANTS from '@routes/orderConfirmation/store/constants';
import actions from '@routes/orderConfirmation/store/actions';
import history from '@src/client/history';
import { getOrderDetails } from '@routes/orderConfirmation/store/services';
import { logError } from '@src/common/utils';
import { IFetchOrderDetails } from '@routes/orderConfirmation/store/types';

export function* selectOtherDevice(): Generator {
  try {
    if (history) {
      history.push('/basket');
    }
  } catch (error) {
    logError(error);
  }
}

export function* fetchOrderDetails(action: {
  type: string;
  payload: IFetchOrderDetails;
}): Generator {
  try {
    const { id, showFullPageError = false } = action.payload;
    const response = yield getOrderDetails(id, showFullPageError);
    if (response) {
      yield put(actions.fetchOrderDetailsSuccess(response));
    }
  } catch (error) {
    logError(error);
    yield put(actions.fetchOrderDetailsError());
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(CONSTANTS.FETCH_ORDER_DETAILS_REQUESTED, fetchOrderDetails);
}
export default watcherSaga;
