import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import { IOrderConfirmationResponse } from '@routes/orderConfirmation/store/types';

export const getOrderDetails = async (
  id: string,
  showFullPageError: boolean
): Promise<IOrderConfirmationResponse | Error> => {
  try {
    return await apiCaller.get(
      apiEndpoints.CHECKOUT.GET_ORDER_DETAILS.url + id,
      { showFullPageError }
    );
  } catch (e) {
    logError(e);
    throw e;
  }
};
