import { IOrderConfirmationState } from '@routes/orderConfirmation/store/types';
import {
  ORDER_SUCCESS_CODE,
  PAYMENT_TYPE,
  PLACE_ORDER_CODE,
  SHIPPING_TYPE
} from '@checkout/store/enums';

export default (): IOrderConfirmationState => ({
  loading: false,
  dataReceived: false,
  id: '',
  orderStatus: {
    status: PLACE_ORDER_CODE.ORDER_SUCCESS,
    subStatus: ORDER_SUCCESS_CODE.CONFIRMED
  },
  orderId: '****************************************=',
  cartItems: [],
  personalInfo: {
    id: '*****-****-****-****-************',
    name: '******',
    email: '****@*****.****',
    contact: '***********'
  },
  shippingDetails: {
    shippingTo: '* *** *** ***** **** **** ',
    deliveryType: SHIPPING_TYPE.STANDARD,
    id: '1',
    estimatedDelivery: '***'
  },
  paymentTypeSelected: PAYMENT_TYPE.PAY_BY_LINK,
  trackingLink: '',
  orderDateTime: '',
  p24Link: ''
});
