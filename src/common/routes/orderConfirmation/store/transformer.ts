import {
  IOrderConfirmationResponse,
  IOrderConfirmationState
} from '@routes/orderConfirmation/store/types';
import initialOrderConfirmationState from '@routes/orderConfirmation/store/state';

export const convertOrderConfirmationResponse = (
  state: IOrderConfirmationState,
  response: IOrderConfirmationResponse
): void => {
  let personalInfo = {};
  let shippingDetails = {};

  personalInfo = response.personalInfo
    ? response.personalInfo
    : initialOrderConfirmationState().personalInfo;
  shippingDetails = response.shippingDetails
    ? response.shippingDetails
    : initialOrderConfirmationState().shippingDetails;

  const {
    orderId,
    orderStatus,
    cartItems,
    id,
    paymentTypeSelected,
    trackingLink,
    orderDateTime,
    p24Link
  } = response;
  const obj = {
    personalInfo,
    shippingDetails,
    orderId,
    id,
    orderStatus,
    paymentTypeSelected,
    trackingLink,
    orderDateTime,
    cartItems,
    p24Link,
    loading: false,
    dataReceived: false
  };

  Object.assign(state, obj);
};
