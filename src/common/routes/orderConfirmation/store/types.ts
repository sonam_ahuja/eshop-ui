import {
  ORDER_SUCCESS_CODE,
  PAYMENT_TYPE,
  PLACE_ORDER_CODE
} from '@checkout/store/enums';
import { shippingType } from '@src/common/routes/checkout/store/shipping/types';
import { IBasketItem } from '@src/common/routes/basket/store/types';

export interface IOrderConfirmationState {
  loading: boolean;
  dataReceived: boolean;
  orderStatus: {
    status: PLACE_ORDER_CODE;
    subStatus: ORDER_SUCCESS_CODE;
  };
  orderId: string;
  id: string;
  personalInfo: {
    id: string;
    name: string;
    email: string;
    contact: string;
  };
  cartItems: IBasketItem[];

  shippingDetails: {
    id: string;
    estimatedDelivery: string;
    deliveryType: shippingType;
    shippingTo: string;
  };
  paymentTypeSelected: PAYMENT_TYPE;
  trackingLink: string;
  orderDateTime: string;
  p24Link?: string;
}

export interface IOrderConfirmationResponse {
  orderStatus: {
    status: PLACE_ORDER_CODE;
    subStatus: ORDER_SUCCESS_CODE;
  };
  id: string;
  trackingLink: string;
  orderDateTime: string;
  orderId: string;
  personalInfo: {
    id: string;
    name: string;
    email: string;
    contact: string;
  };
  cartItems: IBasketItem[];

  shippingDetails: {
    id: string;
    estimatedDelivery: string;
    deliveryType: shippingType;
    shippingTo: string;
  };
  paymentTypeSelected: PAYMENT_TYPE;
  p24Link?: string;
}

export interface IFetchOrderDetails {
  id: string;
  showFullPageError?: boolean;
}
