import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledBasketStrip } from '@src/common/components/BasketStripWrapper/BasketStrip/styles';

export const StyledOrderConfirmation = styled.div`
  position: relative;
  width: 100%;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    flex: 1;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    ${StyledBasketStrip} {
      height: calc(100vh - 80px);
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    ${StyledBasketStrip} {
      height: calc(100vh - 100px);
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    ${StyledBasketStrip} {
      height: calc(100vh - 120px);
    }
  }
`;

export const StyledContentWrapper = styled.div`
  width: 100%;
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    flex-direction: column;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    flex-direction: row;
  }
`;

export const StyledMainContent = styled.div`
  background: ${colors.cloudGray};
  color: ${colors.ironGray};
  padding: 2.25rem 0 2.5rem 1.25rem;
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 100%;
  height: 100%;
  .informationBlock {
    .mainTitle {
      margin-bottom: 2.5rem;
      color: ${colors.darkGray};
      padding-right: 1.25rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 2.25rem 2.25rem 2.5rem 2.25rem;
    .informationBlock {
      .mainTitle {
        padding-right: 0;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    & + .footer {
      display: none;
    }

    padding: 0;
    flex-direction: row;
    .informationBlock,
    .AllDetailsAndGetMoreWrap {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      flex-wrap: wrap;
      flex: 1;
    }

    .informationBlock {
      padding: 2.25rem 1.5rem 2rem 3rem;
      min-width: 36%;
      max-width: 36%;
    }

    .AllDetailsAndGetMoreWrap {
      min-width: 64%;
      max-width: 64%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .informationBlock {
      padding: 4rem 3.5rem 3rem 4.9rem;
      min-width: 36.75%;
      max-width: 36.75%;
    }

    .AllDetailsAndGetMoreWrap {
      flex-direction: row;
      min-width: 63.25%;
      max-width: 63.25%;

      .allDetails {
        min-width: 66.67%;
        max-width: 66.67%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .informationBlock {
      padding: 3.5rem 5.75rem 3.5rem 5.5rem;
      min-width: 34.5%;
      max-width: 34.5%;
    }

    .AllDetailsAndGetMoreWrap {
      min-width: 65.5%;
      max-width: 65.5%;
    }
  }
`;

// tslint:disable-next-line: no-commented-code
// export const StyledLandingStripWrap = styled.div`
//   flex: 1;
//   @media (min-width: ${breakpoints.desktop}px) {
//     display: flex;
//     height: calc(100vh - 125px);
//     ${StyledStrip} {
//       height: calc(100vh - 125px);
//     }
//   }
// `;
