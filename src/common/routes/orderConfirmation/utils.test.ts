import { BASKET_ALL_ITEM } from '@mocks/basket/basket.mock';

import { convertCartItems } from './utils';

describe('<utils />', () => {
  test('should render properly', () => {
    expect(convertCartItems(BASKET_ALL_ITEM.cartItems)).toMatchSnapshot();
  });

  test('should render properly, without error', () => {
    expect(convertCartItems(BASKET_ALL_ITEM.cartItems)).toMatchSnapshot();
  });
});
