import { IBasketItem, IProduct } from '@src/common/routes/basket/store/types';

export const convertCartItems = (cartItems?: IBasketItem[]): IProduct[] => {
  const productArr: IProduct[] = [];

  if (cartItems && cartItems.length) {
    cartItems.forEach(item => {
      productArr.push(item.product);
    });
  }

  return productArr;
};
