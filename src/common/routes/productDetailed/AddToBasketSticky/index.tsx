import React from 'react';
import { Button, Paragraph, Section } from 'dt-components';
import { IProductDetailedConfiguration } from '@src/common/store/types/configuration';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { IVariant, IVariantGroups } from '@productDetailed/store/types';
import {
  IMAGE_POSITION,
  IMAGE_TYPE,
  IMGAE_WIDTH_RESOLUTION,
  ITEM_STATUS
} from '@common/store/enums';
import PreloadImage from '@src/common/components/Preload';
import EVENT_NAME from '@events/constants/eventName';

import { getSelectedCharacterstic } from '../utils';

import { StyledStickyDiv } from './style';

interface IProps {
  productName: string;
  tariffName: string | null;
  variant?: IVariant;
  variantGroups: IVariantGroups;
  showNotifyMe: boolean;
  loading: boolean;
  productDetailedTranslation: IProductDetailedTranslation;
  productDetailedConfiguration: IProductDetailedConfiguration;
  notifyMe(): void;
  addToBasket(): void;
  setAddToBasketRef(ref: HTMLElement | null): void;
}

export default (props: IProps) => {
  const {
    variant,
    addToBasket,
    productDetailedTranslation,
    notifyMe,
    showNotifyMe,
    variantGroups,
    productDetailedConfiguration,
    setAddToBasketRef,
    productName,
    tariffName,
    loading
  } = props;

  if (!variant) {
    return null;
  }
  let variantImage = '';
  if (variant.attachments && variant.attachments.thumbnail) {
    const variantThumbanil = variant.attachments.thumbnail.find(thumbnail => {
      return thumbnail.name === IMAGE_POSITION.FRONT;
    });
    if (variantThumbanil) {
      variantImage = variantThumbanil.url;
    }
  }

  const {
    color: colorTranslatedKey,
    storage: storageTranslatedKey
  } = productDetailedConfiguration;
  const addToCartBtn =
    variant.unavailabilityReasonCodes &&
    variant.unavailabilityReasonCodes.length &&
    variant.unavailabilityReasonCodes.indexOf(ITEM_STATUS.OUT_OF_STOCK) !==
      -1 ? (
      showNotifyMe ? (
        <Button
          size='large'
          onClickHandler={notifyMe}
          data-event-id={EVENT_NAME.DEVICE_DETAIL.EVENTS.NOTIFY_ME}
          data-event-message={productDetailedTranslation.notifyMe}
          className='button'
          loading={loading}
        >
          {productDetailedTranslation.notifyMe}
        </Button>
      ) : null
    ) : (
      <Button
        size='large'
        onClickHandler={addToBasket}
        data-event-id={EVENT_NAME.DEVICE_DETAIL.EVENTS.ADD_TO_BASKET}
        data-event-message={productDetailedTranslation.addToBasket}
        className='button'
        loading={loading}
      >
        {productDetailedTranslation.addToBasket}
      </Button>
    );

  const selectedColor = getSelectedCharacterstic(
    variant.characteristics,
    variantGroups[colorTranslatedKey],
    colorTranslatedKey
  );
  const selectedStorage = getSelectedCharacterstic(
    variant.characteristics,
    variantGroups[storageTranslatedKey],
    storageTranslatedKey
  );

  return (
    <StyledStickyDiv ref={setAddToBasketRef}>
      <div className='container-wrap'>
        <div className='imageWrap'>
          {variantImage ? (
            <PreloadImage
              isObserveOnScroll={true}
              imageHeight={10}
              width={IMGAE_WIDTH_RESOLUTION.VERY_SMALL_DESKTOP_WIDTH}
              mobileWidth={IMGAE_WIDTH_RESOLUTION.VERY_SMALL_MOBILE_WIDTH}
              type={IMAGE_TYPE.WEBP}
              imageWidth={10}
              imageUrl={variantImage}
              alt={variant.description}
            />
          ) : null}
        </div>
        <div className='variantInfoWrap'>
          <Paragraph className='variantName' weight='bold'>
            {productName}
          </Paragraph>
          <Section className='selectedVariant' size='small'>
            <span>{selectedColor && selectedColor.label}</span>
            <span>{selectedStorage && selectedStorage.label}</span>
            {tariffName !== null ? <span>{tariffName}</span> : null}
          </Section>
        </div>
      </div>
      <div className='buttonWrap'>{addToCartBtn}</div>
    </StyledStickyDiv>
  );
};
