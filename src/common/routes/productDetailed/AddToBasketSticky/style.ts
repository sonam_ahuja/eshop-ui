import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledStickyDiv = styled.div`
  position: fixed;
  align-items: center;
  justify-content: center;
  z-index: 300;
  top: 0;
  transform: translateY(-102%);
  display: flex;
  opacity: 0;
  visibility: hidden;
  width: 100%;
  height: 4rem;
  background: ${colors.white};
  padding: 0rem;

  &.sticky {
    transition: transform 0.3s ease 0s, opacity 0.3s ease 0s;
    transform: translateY(0);
    opacity: 1;
    visibility: visible;
  }

  .imageWrap {
    min-width: 2.5rem;
    min-height: 2.5rem;
    width: 2.5rem;
    height: 2.5rem;
    overflow: hidden;
    img {
      margin: auto;
    }
  }

  .variantInfoWrap {
    margin: 0;
    min-width: calc(100% - 2.5rem);
    width: calc(100% - 2.5rem);
    padding: 0 0.5rem;
    .variantName {
      color: ${colors.darkGray};
      font-size: 1rem;
      line-height: 1.5rem;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      width: 100%;
    }

    .selectedVariant {
      color: ${colors.ironGray};
      span:after {
        content: '/';
        display: inline;
        padding: 0 0.15rem;
      }
      span:last-child:after {
        display: none;
      }
    }
  }
  .container-wrap {
    display: flex;
    height: 100%;
    width: 50%;
    align-items: center;
    padding-left: 1.25rem;
    flex-shrink: 0;
  }

  .buttonWrap {
    justify-self: flex-end;
    margin-left: auto;
    width: 50%;
    flex-shrink: 0;
    button {
      width: 100%;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    /* width: calc(100% - 5.75rem); */
  }

  @media (min-width: ${breakpoints.desktop}px) {
    /* width: calc(100% - 4.75rem); */
    padding-left: 4.9rem;
    .container-wrap {
      padding-left: 0;
      flex:1;

      .variantInfoWrap{
        padding-left: 2.1rem;
      }
    }
    .buttonWrap {
      width: 50%;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-left: 5.5rem;
    /* width: calc(100% - 5.75rem); */
  }

  /* @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 0;
    width: calc(100% - 5.75rem);

    .imageWrap {
      margin-right: 2.1rem;
    }
    .buttonWrap {
      width: 47.512%;
      flex-shrink: 0;
      height: 100%;
      .button {
        width: 100%;
      }
    }
  } */


`;
