import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Highlights from '@productDetailed/Highlights';

describe('<Highlights />', () => {
  test('should render properly', () => {
    const props = {
      highlightProductDetailedSection: ''
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Highlights {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
