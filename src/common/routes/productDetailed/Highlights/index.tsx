import React from 'react';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { noopFn } from '@src/common/utils';

export interface IProps {
  highlightProductDetailedSection: string;
}

const Highlights = (props: IProps) => {
  const { highlightProductDetailedSection } = props;

  return (
    <>
      {highlightProductDetailedSection && (
        <HTMLTemplate
          template={highlightProductDetailedSection}
          templateData={{}}
          onRouteChange={noopFn}
        />
      )}{' '}
    </>
  );
};

export default Highlights;
