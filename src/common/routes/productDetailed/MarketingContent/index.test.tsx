import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Component from '.';

describe('<Technical Specification />', () => {
  test('should render properly', () => {
    const props = {
      marketingContent: '',
      productExtraFeature: ''
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );

    expect(component).toMatchSnapshot();
  });
});
