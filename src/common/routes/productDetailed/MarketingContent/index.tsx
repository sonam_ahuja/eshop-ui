import React from 'react';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { noopFn } from '@src/common/utils';

export interface IProps {
  marketingContent: string;
  productExtraFeature: string;
}

const Highlights = (props: IProps) => {
  const { marketingContent, productExtraFeature } = props;

  return (
    <>
      {marketingContent && (
        <HTMLTemplate
          template={marketingContent}
          templateData={{}}
          onRouteChange={noopFn}
        />
      )}{' '}
      {productExtraFeature && (
        <HTMLTemplate
          template={productExtraFeature}
          templateData={{}}
          onRouteChange={noopFn}
        />
      )}{' '}
    </>
  );
};

export default Highlights;
