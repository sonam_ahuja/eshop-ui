import React, { FunctionComponent } from 'react';

import { StyledNotifyOnMobileModal } from './styles';

const NotifyOnMobileModal: FunctionComponent<{}> = () => {
  const titleEl = (
    <>
      Once the accessory is available you will be notified on
      <span className='mobileNumber'>0995556666</span>
    </>
  );

  return (
    <StyledNotifyOnMobileModal
      iconName='ec-confirm'
      isRightButton={false}
      isCloseIcon
      leftButtonType='complementary'
      leftButtonText='Ok, got it.'
      isOpen
      title={titleEl}
    />
  );
};

export default NotifyOnMobileModal;
