import styled from 'styled-components';
import { colors } from '@src/common/variables';

import { StyledRoundFlowModal } from '../styles';

export const StyledNotifyOnMobileModal = styled(StyledRoundFlowModal)`
  .mobileNumber {
    padding-left: 0.5rem;
    color: ${colors.mediumGray};
  }
`;
