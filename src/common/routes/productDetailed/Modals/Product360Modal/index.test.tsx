import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Component from '.';

describe('<Product360Degree />', () => {
  test('should render properly', () => {
    const props = {
      isOpen: false,
      onClose: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
