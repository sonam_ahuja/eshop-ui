import React from 'react';
import { Icon, PreloadImage, RoundButton, Section } from 'dt-components';

import { StyledProduct360Modal } from './styles';

interface IProps {
  isOpen: boolean;
  onClose(): void;
}

const Product360Modal = (props: IProps) => {
  const { isOpen, onClose } = props;

  return (
    <StyledProduct360Modal isOpen={isOpen}>
      <RoundButton
        onClick={onClose}
        className='closeIcon'
        shadow={true}
        iconName='ec-cancel'
      />
      <div className='imagesWrap'>
        <div className='mainImage'>
          <PreloadImage
            imageUrl='https://i.ibb.co/MDtHymh/mobile320x320.png'
            imageHeight={10}
            imageWidth={10}
          />
        </div>
        <div className='modalThumbnailsWrap'>
          <ul>
            <li className='active'>
              <Icon size='large' name='ec-360' />
              {/* <Icon size='large' name='ec-americanexpress' /> */}
            </li>
            <li>
              <img src='https://i.ibb.co/MDtHymh/mobile320x320.png' />
            </li>
            <li>
              <img src='https://i.ibb.co/MDtHymh/mobile320x320.png' />
            </li>
            <li>
              <img src='https://i.ibb.co/MDtHymh/mobile320x320.png' />
            </li>
            <li>
              <img src='https://i.ibb.co/MDtHymh/mobile320x320.png' />
            </li>
          </ul>
        </div>
        <div className='infoText'>
          <Section size='small'>Drag to explore the 360º view111</Section>
        </div>
      </div>
    </StyledProduct360Modal>
  );
};

export default Product360Modal;
