import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledRoundModal } from '../styles';

export const StyledProduct360Modal = styled(StyledRoundModal)`
  .contentWrap {
    overflow: hidden;

    .infoText {
      display: none;
    }

    .closeIcon {
      z-index: 1000;
    }

    .mainImage {
      max-width: 20rem;
      margin: auto;
      padding: 2.5rem 1.25rem;
    }

    .modalThumbnailsWrap {
      padding-bottom: 2.5rem;
      ul {
        display: flex;
        flex-direction: row-reverse;
        align-items: center;
        justify-content: center;

        li {
          width: 2.5rem;
          height: 2.5rem;
          margin: 0 0.625rem;
          display: inline-flex;
          align-items: center;
          justify-content: center;
          opacity: 0.5;
          transition: 0.2s linear 0s;

          &.active {
            opacity: 1;
          }

          img {
            display: block;
            max-width: 100%;
            max-height: 100%;
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .overlay,
    .outsideClick,
    .contentWrap {
      width: 100%;
      height: 100%;
    }

    .contentWrap {
      .closeIcon {
        top: 2rem !important;
        right: 2rem !important;
      }

      .imagesWrap {
        height: 100%;
        position: relative;
      }

      .imagesWrap .infoText {
        display: block;
        position: absolute;
        right: 2.5rem;
        top: 0;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        color: ${colors.mediumGray};
        writing-mode: vertical-lr;
      }

      .imagesWrap .mainImage {
        width: 40rem;
        max-width: 40rem;
        height: 100%;
      }

      .imagesWrap .modalThumbnailsWrap {
        position: absolute;
        left: 4.9rem;
        top: 0;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
      }

      .imagesWrap .modalThumbnailsWrap ul {
        flex-direction: column;
      }

      .imagesWrap .modalThumbnailsWrap ul li {
        margin: 0.75rem;
      }
    }
  }
`;
