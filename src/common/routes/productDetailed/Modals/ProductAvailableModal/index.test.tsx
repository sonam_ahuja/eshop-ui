import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import ProductAvailableModal from '.';

describe('<ProductAvailableModal />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <ProductAvailableModal />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
