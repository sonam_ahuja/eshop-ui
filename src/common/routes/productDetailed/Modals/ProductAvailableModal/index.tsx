import React, { FunctionComponent } from 'react';
import { FloatLabelInput } from 'dt-components';

import { StyledProductAvailableModal } from './styles';

const ProductAvailableModal: FunctionComponent<{}> = () => {
  return (
    <StyledProductAvailableModal
      iconName='ec-email'
      isRightButton={false}
      isCloseIcon
      isOpen
      title='We ll notify you once Apple iPhone XS Leather Case is available.'
      leftButtonText='Validate'
    >
      <FloatLabelInput className='inputWrap' label='Mobile number or Email' />
    </StyledProductAvailableModal>
  );
};

export default ProductAvailableModal;
