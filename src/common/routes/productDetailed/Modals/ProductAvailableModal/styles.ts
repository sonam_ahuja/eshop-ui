import styled from 'styled-components';

import { StyledRoundFlowModal } from '../styles';

export const StyledProductAvailableModal = styled(StyledRoundFlowModal)`
  .inputWrap {
    margin: 1.875rem 0;
  }

  .modalActions {
    padding-top: 0 !important;
  }
`;
