import { FlowModal, Modal } from 'dt-components';
import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledRoundFlowModal = styled(FlowModal)`
  .outsideClick,
  .contentWrap {
    width: 100%;
  }

  .overlay {
    align-items: flex-end;

    .contentWrap {
      border-radius: 0.5rem 0.5rem 0 0;
      background: ${colors.coldGray};

      .closeIcon {
        position: fixed;
        top: 1.25rem;
        right: 1.25rem;
      }

      button {
        width: 100%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .outsideClick,
    .contentWrap {
      width: 22.35rem;
    }

    .overlay {
      align-items: center;

      .contentWrap {
        border-radius: 0;

        .closeIcon {
          position: absolute;
          top: 1.5rem;
          right: 1.5rem;
        }

        button {
          width: auto;
        }
      }
    }
  }
`;

export const StyledRoundModal = styled(Modal)`
  .outsideClick,
  .contentWrap {
    width: 100%;
  }

  .overlay {
    align-items: flex-end;

    .contentWrap {
      border-radius: 0.5rem 0.5rem 0 0;
      background: ${colors.coldGray};

      .closeIcon {
        position: fixed;
        top: 1.25rem;
        right: 1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .outsideClick,
    .contentWrap {
      width: 22.35rem;
    }

    .overlay {
      align-items: center;

      .contentWrap {
        border-radius: 0;
      }
    }
  }
`;
