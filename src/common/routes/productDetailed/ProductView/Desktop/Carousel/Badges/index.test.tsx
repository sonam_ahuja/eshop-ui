import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { ITEM_STATUS } from '@src/common/store/enums';

import Badges, { IProps } from '.';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props: IProps = {
      showOnlineStockAvailability: true,
      variant: {
        id: '',
        quantity: 12,
        group: '',
        name: '',
        description: '',
        characteristics: [
          {
            name: '',
            values: [{ value: 'value1' }]
          }
        ],
        unavailabilityReasonCodes: [],
        prices: [
          {
            priceType: '',
            actualValue: 0,
            discountedValue: 0,
            recurringChargePeriod: '12',
            discounts: []
          }
        ],
        tags: []
      },
      deviceStock: ITEM_STATUS.IN_STOCK,
      showbadges: true,
      newProductBeforeLaunchDays: 34,
      productDetailedTranslation: appState().translation.cart.productDetailed
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Badges {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
