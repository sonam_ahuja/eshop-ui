import React from 'react';
import { Badge } from 'dt-components';
import { eligibilityUnavailabilityReasonType, IVariant } from '@productDetailed/store/types';
import { IProductDetailedTranslation } from '@common/store/types/translation';
import {
  getTagCharacterstics,
  getTags,
  getVariantLaunchDate
} from '@productDetailed/utils';

import { StyledHeader } from './styles';

export interface IProps {
  showbadges: boolean;
  variant: IVariant;
  showOnlineStockAvailability: boolean;
  newProductBeforeLaunchDays: number;
  productDetailedTranslation: IProductDetailedTranslation;
  deviceStock: eligibilityUnavailabilityReasonType;
}

const Header = (props: IProps) => {
  const {
    showbadges,
    variant,
    newProductBeforeLaunchDays,
    productDetailedTranslation,
    showOnlineStockAvailability,
    deviceStock
  } = props;

  const tagsCharacterstic = getTagCharacterstics(variant.characteristics);
  const launchDate = getVariantLaunchDate(variant.characteristics);
  const tags = getTags(
    tagsCharacterstic && tagsCharacterstic.values[0].label,
    launchDate,
    newProductBeforeLaunchDays,
    productDetailedTranslation,
    showOnlineStockAvailability,
    deviceStock
  );

  if (!showbadges) {
    return null;
  }

  return (
    <StyledHeader>
      <div className='badgesWrap'>
        {tags.map((tag, idx) => (
          <Badge key={idx} label={tag} />
        ))}
      </div>
    </StyledHeader>
  );
};

export default Header;
