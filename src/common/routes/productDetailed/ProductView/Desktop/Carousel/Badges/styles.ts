import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledHeader = styled.div`
  .badgesWrap {
    /* margin: 0 -0.25rem; */ /******** alignment fixes *******/
    margin: 0;
    > div {
      line-height: 0.8rem;
      padding: 0.2rem 0.6rem;
      margin-right: 0.25rem;
      cursor: inherit;
      user-select: none;
      margin-bottom: 0.25rem;
      background: ${colors.silverGray};
      color: ${colors.darkGray};
      font-weight: normal;
    }
  }
`;
