import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import appState from '@store/states/app';

import Component, {
  CountDownTimer as CountDownTimerComponent,
  IProps
} from '.';

describe('<CountDownTimer />', () => {
  const props: IProps = {
    variantId: '1',
    // tslint:disable-next-line:no-duplicate-string
    promotionEndDate: '12/12/2017',
    promotionStartDate: '12/12/2017',
    productDetailedTranslation: appState().translation.cart.productDetailed,
    refereshDetailedPage: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render with days as truthy value', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );

    component.instance().setState({
      days: 'dummy'
    });

    component.update();

    expect(component).toMatchSnapshot();
  });

  test('should render by calling handle change', () => {
    const newProps: IProps = {
      variantId: '1',
      promotionEndDate: '12/12/2017',
      promotionStartDate: '',
      productDetailedTranslation: appState().translation.cart.productDetailed,
      refereshDetailedPage: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...newProps} />
      </ThemeProvider>
    );

    component.instance().setState({
      days: 'dummy'
    });

    component.update();

    expect(component).toMatchSnapshot();
  });

  test('should render with days as truthy value', () => {
    const currentDate = new Date();

    currentDate.setDate(currentDate.getDate() + 1);

    const newProps: IProps = {
      variantId: '1',
      promotionEndDate: new Date(currentDate).toString(),
      promotionStartDate: '',
      productDetailedTranslation: appState().translation.cart.productDetailed,
      refereshDetailedPage: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...newProps} />
      </ThemeProvider>
    );

    component.instance().setState({
      days: 'dummy'
    });
    component.update();

    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<CountDownTimerComponent>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <CountDownTimerComponent {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('CountDownTimer')
      .instance() as CountDownTimerComponent).componentDidUpdate(props);
    (component
      .find('CountDownTimer')
      .instance() as CountDownTimerComponent).componentWillUnmount();
    (component
      .find('CountDownTimer')
      .instance() as CountDownTimerComponent).handleChange();
  });
});
