import React, { Component } from 'react';
import { Icon } from 'dt-components';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';

import { StyledCountDownTimer } from './styles';

export interface IProps {
  promotionEndDate: string;
  promotionStartDate?: string;
  productDetailedTranslation: IProductDetailedTranslation;
  variantId: string;
  refereshDetailedPage(): void;
}

export interface IState {
  days: string;
  seconds: string;
  hours: string;
  minutes: string;
  offerValid: boolean;
}

export class CountDownTimer extends Component<IProps, IState> {
  timer?: number = undefined;

  constructor(props: IProps) {
    super(props);
    this.state = {
      days: '',
      hours: '',
      minutes: '',
      seconds: '',
      offerValid: true
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount(): void {
    this.handleChange();
  }

  componentDidUpdate(preProps: IProps): void {
    if (preProps.variantId !== this.props.variantId) {
      if (this.timer) {
        clearInterval(this.timer);
      }
      this.setState({
        days: '',
        hours: '',
        minutes: '',
        seconds: '',
        offerValid: true
      });
      this.handleChange();
    }
  }

  componentWillUnmount(): void {
    this.setState({
      days: '',
      hours: '',
      minutes: '',
      seconds: '',
      offerValid: true
    });
    this.timer = undefined;
  }

  handleChange(): void {
    const { promotionEndDate, promotionStartDate } = this.props;
    const countDownDate = new Date(promotionEndDate).getTime();
    const countStartDate = promotionStartDate
      ? new Date(promotionStartDate).getTime()
      : 0;
    const time = new Date().getTime();
    const offerStart = time - countStartDate;
    let offerValid = false;

    const initialDiff = countDownDate - new Date().getTime();

    if (
      offerStart > 0 &&
      !isNaN(countStartDate) &&
      !isNaN(countDownDate) &&
      initialDiff > 0
    ) {
      offerValid = true;

      this.timer = setInterval(() => {
        const currentTime = new Date().getTime();
        const difference = countDownDate - currentTime;
        const days = Math.floor(difference / (1000 * 60 * 60 * 24));
        const updatedDays = `0${days}`.slice(-2);
        const hours = Math.floor(
          (difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const updatedHours = `0${hours}`.slice(-2);
        const minutes = Math.floor(
          (difference % (1000 * 60 * 60)) / (1000 * 60)
        );
        const updatedMinutes = `0${minutes}`.slice(-2);

        const seconds = Math.floor((difference % (1000 * 60)) / 1000);
        const updatedSeconds = `0${seconds}`.slice(-2);

        if (difference < 0) {
          this.props.refereshDetailedPage();
          offerValid = false;
          clearInterval(this.timer);
        }

        this.setState({
          hours: updatedHours,
          days: updatedDays,
          minutes: updatedMinutes,
          seconds: updatedSeconds,
          offerValid
        });
      }, 1000);
    } else {
      this.setState({
        offerValid: false
      });
    }
  }

  render(): React.ReactNode {
    const { offerValid, days, hours, minutes, seconds } = this.state;
    const { productDetailedTranslation } = this.props;
    if (!offerValid) {
      return null;
    }

    return (
      <StyledCountDownTimer>
        {offerValid ? (
          <div className='timer'>
            <>
              <div className='icon-wrap'>
                <Icon color='currentColor' size='inherit' name='ec-gift' />
              </div>
              <ul>
                <li>
                  <span className='titles'>
                    {productDetailedTranslation.days}
                  </span>
                  <span className='counters'>
                    {days === '' ? '\u00A0' : days}
                  </span>
                </li>
                <li>
                  <span className='titles'>
                    {productDetailedTranslation.hours}
                  </span>
                  <span className='counters'>{hours}</span>
                </li>
                <li>
                  <span className='titles'>
                    {productDetailedTranslation.minutes}
                  </span>
                  <span className='counters'>{minutes}</span>
                </li>
                <li>
                  <span className='titles'>
                    {productDetailedTranslation.seconds}
                  </span>
                  <span className='counters'>{seconds}</span>
                </li>
              </ul>
            </>
          </div>
        ) : null}
      </StyledCountDownTimer>
    );
  }
}

export default CountDownTimer;
