import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledCountDownTimer = styled.div`
  margin-left: auto;

  .timer {
    width: 13.5rem;
    height: 3rem;
    background-color: ${colors.charcoalGray};
    color: ${colors.white};
    display: flex;
    align-items: center;

    .icon-wrap {
      font-size: 2rem;
      margin-left: 1rem;
      flex-shrink: 0;
    }

    ul {
      padding-left: 2rem;
      display: flex;
      width: 100%;

      li {
        padding-right: 0.8rem;
        position: relative;

        &:nth-child(2):after,
        &:nth-child(3):after {
          content: ':';
          position: absolute;
          right: 0.3rem;
          bottom: 0;
        }

        .titles {
          font-size: 0.625rem;
          line-height: 0.7rem;
          display: block;
          color: ${colors.mediumGray};
        }
        .counters {
          font-size: 0.875rem;
          line-height: 0.9rem;
          color: ${colors.white};
          font-weight: bold;
        }
        &:last-child {
          padding-right: 0;
        }
      }
    }
  }
`;
