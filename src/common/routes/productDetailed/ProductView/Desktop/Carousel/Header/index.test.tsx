import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BrowserRouter as Router } from 'react-router-dom';
import translation from '@common/store/states/translation';

import Header from '.';

describe('<Header />', () => {
  const props = {
    productName: '',
    variantName: '',
    categoryName: 'catrogry',
    parentCategorySlug: '',
    categoryId: '1',
    lastListURL: '1',
    setDeviceList: jest.fn(),
    setLandingPageDeviceList: jest.fn(),
    globalTranslation: translation().cart.global
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Header {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
