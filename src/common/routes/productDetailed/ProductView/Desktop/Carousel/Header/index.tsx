import React from 'react';
import { IGlobalTranslation } from '@src/common/store/types/translation';
import CatalogBreadCrumb from '@common/components/CatalogBreadCrumb';
import { RouteComponentProps, withRouter } from 'react-router';
import { isCategoryLandingPageRequired } from '@common/store/common/index';

import { IList } from '../../../Mobile/Carousel/Header';

import { StyledHeader } from './styles';

export interface IProps extends RouteComponentProps {
  globalTranslation: IGlobalTranslation;
  productName: string;
  categoryId: string | null;
  categoryName: string;
  parentCategorySlug: string;
  lastListURL: string | null;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}

const onBreadcrumbItemClick = (
  setDeviceList: (list: string) => void,
  setLandingPageDeviceList: (list: string) => void
) => {
  return (item: IList) => {
    if (item && item.name === 'deviceList') {
      setDeviceList('DEVICE DETAILED');
    } else if (item && item.name === 'home') {
      setLandingPageDeviceList('DEVICE DETAILED');
    }
  };
};

const Header = (props: IProps) => {
  const categoryRoute = `/category/${props.parentCategorySlug}`;
  const deviceListingRoute =
    props.lastListURL === null
      ? `/${props.categoryId}`
      : `/${props.categoryId}${props.lastListURL}`;

  const categorylandingPage = isCategoryLandingPageRequired()
    ? [
        {
          title: props.globalTranslation.products,
          active: false,
          link: categoryRoute
        }
      ]
    : [];

  const breadCrumbItems = [
    {
      title: props.globalTranslation.home,
      active: false,
      link: '/',
      name: 'home'
    },
    ...categorylandingPage,
    {
      title: props.categoryName,
      active: false,
      link: deviceListingRoute,
      name: 'deviceList'
    },
    {
      title: props.productName,
      active: true
    }
  ];

  return (
    <StyledHeader>
      <CatalogBreadCrumb
        onBreadcrumbItemClick={onBreadcrumbItemClick(
          props.setDeviceList,
          props.setLandingPageDeviceList
        )}
        children={null}
        breadCrumbItems={breadCrumbItems}
      />
    </StyledHeader>
  );
};

export default withRouter(Header);
