import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { StyledBreadcrumbWrap } from '@src/common/components/CatalogBreadCrumb/styles';

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  ${StyledBreadcrumbWrap} {
    padding: 0;
  }

  .carouselDots {
    ul {
      font-size: 0;

      li {
        display: inline-block;
        height: 5px;
        width: 5px;
        margin-left: 5px;

        a {
          display: block;
          height: 100%;
          width: 100%;
          border-radius: 50%;
          overflow: hidden;
          text-decoration: none;
          background: ${colors.lightGray};
        }

        &.active {
          a {
            background: ${colors.black};
          }
        }
      }
    }
  }
  ${StyledBreadcrumbWrap} {
    /* padding-bottom: 2.25rem; Edit after review */
  }
`;
