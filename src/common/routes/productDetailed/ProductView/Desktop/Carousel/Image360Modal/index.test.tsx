import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Image360Modal, { IProps } from '.';

describe('<Image360Modal />', () => {
  const props: IProps = {
    isOpen: true,
    onClose: jest.fn()
  };

  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <Image360Modal {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when image is not open', () => {
    const newProps: IProps = { ...props, isOpen: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
