import React from 'react';
import Product360Modal from '@productDetailed/Modals/Product360Modal';

export interface IProps {
  isOpen: boolean;
  onClose(): void;
}

export default (props: IProps) => {
  const { isOpen, onClose } = props;

  return <Product360Modal isOpen={isOpen} onClose={onClose} />;
};
