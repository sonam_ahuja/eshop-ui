import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import ImageViewer, {
  IProps
} from '@productDetailed/ProductView/Desktop/Carousel/ImageViewer';
const mockStore = configureStore();

describe('<ImageViewer />', () => {
  const props: IProps = {
    variantId: 'string',
    outOfStock: true,
    alt: '',
    attachments: {
      thumbnail: [{ name: '', type: '', url: '' }]
    },
    productDetailedTranslation: appState().translation.cart.productDetailed
  };

  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <ImageViewer {...newProps} />
      </ThemeProvider>
    );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when product is outofStock', () => {
    const newProps: IProps = { ...props, outOfStock: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount<ImageViewer>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ImageViewer {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component
      .find('ImageViewer')
      .instance() as ImageViewer).componentDidUpdate(props);
    (component.find('ImageViewer').instance() as ImageViewer).selectImage(
      'string'
    );
    (component
      .find('ImageViewer')
      .instance() as ImageViewer).goToPreviousPage();
    (component.find('ImageViewer').instance() as ImageViewer).goToNextPage();
    (component
      .find('ImageViewer')
      .instance() as ImageViewer).set360ModalVisibility(true);
    (component
      .find('ImageViewer')
      .instance() as ImageViewer).set360ModalVisibility(false);
  });
});
