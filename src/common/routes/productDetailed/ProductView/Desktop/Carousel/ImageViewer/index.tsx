import React, { Component, ReactNode } from 'react';
import PreloadImage from '@src/common/components/Preload';
import { IAttachments } from '@productDetailed/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { IMAGE_TYPE, IMGAE_WIDTH_RESOLUTION } from '@common/store/enums';

import { StyledImageViewer } from './styles';

export interface IProps {
  variantId: string;
  attachments?: IAttachments;
  productDetailedTranslation: IProductDetailedTranslation;
  outOfStock: boolean;
  alt: string;
}

interface IState {
  selectedImageUrl: string;
  currentPage: number;
  modal360Open: boolean;
}

export default class ImageViewer extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      selectedImageUrl:
        this.props.attachments &&
        this.props.attachments.thumbnail &&
        this.props.attachments.thumbnail[0]
          ? this.props.attachments.thumbnail[0].url
          : '',
      currentPage: 0,
      modal360Open: false
    };
    this.goToPreviousPage = this.goToPreviousPage.bind(this);
    this.goToNextPage = this.goToNextPage.bind(this);
    this.selectImage = this.selectImage.bind(this);
    this.set360ModalVisibility = this.set360ModalVisibility.bind(this);
  }

  componentDidUpdate(prevProps: IProps): void {
    if (this.props.variantId !== prevProps.variantId) {
      this.setState({
        selectedImageUrl:
          this.props.attachments &&
          this.props.attachments.thumbnail &&
          this.props.attachments.thumbnail[0]
            ? this.props.attachments.thumbnail[0].url
            : '',
        currentPage: 0,
        modal360Open: false
      });
    }
  }

  get paginationItems(): ReactNode[] {
    if (this.props.attachments && this.props.attachments.thumbnail) {
      return this.props.attachments.thumbnail
        .slice(this.state.currentPage, this.state.currentPage + 4)
        .map((item, index) => (
          <li
            key={index}
            onClick={() => this.selectImage(item.url)}
            className={this.state.selectedImageUrl === item.url ? 'active' : ''}
          >
            <PreloadImage
              isObserveOnScroll={true}
              imageHeight={10}
              width={IMGAE_WIDTH_RESOLUTION.VERY_SMALL_MOBILE_WIDTH}
              mobileWidth={IMGAE_WIDTH_RESOLUTION.VERY_SMALL_MOBILE_WIDTH}
              type={IMAGE_TYPE.WEBP}
              imageWidth={10}
              alt={this.props.alt}
              imageUrl={item.url}
            />
          </li>
        ));
    } else {
      return [];
    }
  }

  selectImage(selectedImageUrl: string): void {
    this.setState({
      ...this.state,
      selectedImageUrl
    });
  }

  goToPreviousPage(): void {
    this.setState({
      ...this.state,
      currentPage: this.state.currentPage - 1
    });
  }

  goToNextPage(): void {
    this.setState({
      ...this.state,
      currentPage: this.state.currentPage + 1
    });
  }

  set360ModalVisibility(modal360Open: boolean): void {
    this.setState({
      ...this.state,
      modal360Open
    });
  }

  render(): ReactNode {
    // const totalImages =
    //   this.props.attachments && this.props.attachments.thumbnail
    //     ? this.props.attachments.thumbnail.length
    //     : 0;
    // const currentPage = this.state.currentPage;
    // const previousPageBtn = totalImages > 4 && currentPage !== 0 && (
    //   <div className='margin-none'>
    //     <RoundButton
    //       onClick={this.goToPreviousPage}
    //       className='prev-arrow'
    //       iconName='ec-expand'
    //     />
    //   </div>
    // );
    // const nextPageBtn = totalImages > 4 && currentPage + 4 !== totalImages && (
    //   <div className='margin-none'>
    //     <RoundButton
    //       onClick={this.goToNextPage}
    //       className='next-arrow'
    //       iconName='ec-expand'
    //     />
    //   </div>
    // );

    return (
      <StyledImageViewer outOfStock={this.props.outOfStock}>
        <div className='thumbnailsWrap'>
          {/* {this.state.selectedImageUrl && (
            <Icon
              className='top-span'
              color={colors.magenta}
              size='large'
              name='ec-360'
            />
          )} */}
          <ul>{this.paginationItems}</ul>
        </div>
        {/* {!this.state.selectedImageUrl && (
          <Paragraph>
            {this.props.productDetailedTranslation.noImagePreview}
          </Paragraph>
        )} */}
        <div className='imageBig'>
          <PreloadImage
            isObserveOnScroll={true}
            imageHeight={10}
            width={380}
            mobileWidth={IMGAE_WIDTH_RESOLUTION.MEDIUM_MOBILE_WIDTH}
            type={IMAGE_TYPE.WEBP}
            imageWidth={10}
            imageUrl={this.state.selectedImageUrl}
            alt={this.props.alt}
          />
        </div>
      </StyledImageViewer>
    );
  }
}
