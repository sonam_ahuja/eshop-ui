import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledImageViewer = styled.div<{ outOfStock: boolean }>`
  opacity: ${props => {
    return props.outOfStock ? '0.6' : '1';
  }};

  opacity: 1;
  margin: 0;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  padding-top: 0;
  align-items: center;
  .thumbnailsWrap {
    position: absolute;
    top: 6px;
    margin-right: 3rem;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    z-index: 2;
    ul {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      margin: 0;
      height: 100%;
      position: relative;
      li {
        width: 40px;
        height: 40px;
        margin-bottom: 1.5rem;
        display: inline-flex;
        align-items: center;
        opacity: 0.5;
        transition: all 0.5s ease;
        cursor: pointer;
        img {
          cursor: pointer;
        }

        &.active {
          opacity: 1;
          img {
            cursor: pointer;
          }
        }

        img {
          display: block;
          max-width: 100%;
          max-height: 100%;
          margin: auto;
        }
      }
    }
  }

  .imageBig {
    margin: 0rem auto;
    /* padding: 1rem 0.2rem; */
    /* padding: 1.15rem 0 3.75rem; */
    flex-shrink: 0;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    z-index: 1;
    div {
      height: 100%;
      width: 100%;
      padding-top: 0;
    }
    img {
      max-width: 100%;
      max-height: 100%;
      margin: auto;
      animation: fade 5s ease-in-out;
    }
  }
  .margin-none {
    margin: 0 !important;
  }
  .top-span {
    position: relative;
    top: 0px;
    cursor: pointer;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .imageBig {
      padding-top: 1.25rem;
      padding-bottom: 3.8rem;
    }
  }
`;
