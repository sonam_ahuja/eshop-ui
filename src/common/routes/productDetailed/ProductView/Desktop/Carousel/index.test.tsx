import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BrowserRouter as Router } from 'react-router-dom';
import translation from '@common/store/states/translation';
import configuration from '@common/store/states/configuration';
import { IWindow } from '@src/client/index';
import { ITEM_STATUS } from '@common/store/enums';

import Carousel, { IProps } from '.';

describe('<ProductImages />', () => {
  const newBinkies: IWindow['binkies'] = {
    onPageStyleSheetUrl: '/url',
    inModalStyleSheetUrl: '/url',
    contentIdentifier: '/url',
    // on3DNotSupported: jest.fn(),
    onLoadingProgress: jest.fn(),
    show: jest.fn(),
    hide: jest.fn()
  };
  (window as IWindow).binkies = newBinkies;
  test('should render properly', () => {
    const props: IProps = {
      productName: '',
      changeBinkiesFallbackVisible: jest.fn(),
      showOnlineStockAvailability: true,
      categoryName: 'catrogry',
      binkiesFallbackVisible: false,
      newProductBeforeLaunchDays: 12,
      showImageAppShell: false,
      lastListURL: '1',
      parentCategorySlug: '',
      variant: {
        id: '',
        name: '',
        group: '',
        description: '',
        characteristics: [],
        attachments: {
          thumbnail: [{ type: '', url: '', name: '' }]
        },
        prices: [],
        quantity: 34,
        tags: [],
        unavailabilityReasonCodes: []
      },
      categoryId: '1',
      globalTranslation: translation().cart.global,
      showbadges: configuration().cms_configuration.modules.productDetailed
        .productDetailedTemplate1.showbadges,
      productDetailedTranslation: translation().cart.productDetailed,
      refereshDetailedPage: jest.fn(),
      setImageAppShell: jest.fn(),
      setDeviceList: jest.fn(),
      setLandingPageDeviceList: jest.fn(),
      deviceStock: ITEM_STATUS.IN_STOCK
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Carousel {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
