import React from 'react';
import {
  eligibilityUnavailabilityReasonType,
  IVariant
} from '@productDetailed/store/types';
import { ITEM_STATUS } from '@src/common/store/enums';
import {
  IGlobalTranslation,
  IProductDetailedTranslation
} from '@src/common/store/types/translation';
import ProductDetailedImageShell from '@productDetailed/image.shell';
import { getBinkiesID, getPromotion } from '@productDetailed/utils';
import CountDownTimer from '@productDetailed/ProductView/Desktop/Carousel/CountDownTimer/index';
import cx from 'classnames';

import Badges from '../Carousel/Badges';

import Header from './Header';
import ImageViewer from './ImageViewer';
import { StyledBinkiesAndFallBackWrapper, StyledCarousel } from './styles';

export interface IProps {
  variant: IVariant;
  productName: string;
  categoryId: string | null;
  showImageAppShell: boolean;
  binkiesFallbackVisible: boolean;
  categoryName: string;
  parentCategorySlug: string;
  showbadges: boolean;
  lastListURL: string | null;
  newProductBeforeLaunchDays: number;
  productDetailedTranslation: IProductDetailedTranslation;
  showOnlineStockAvailability: boolean;
  globalTranslation: IGlobalTranslation;
  deviceStock: eligibilityUnavailabilityReasonType;
  changeBinkiesFallbackVisible(visible: boolean): void;
  refereshDetailedPage(): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
  setImageAppShell(showAppShell: boolean): void;
}

const Carousel = (props: IProps) => {
  const {
    variant,
    productDetailedTranslation,
    globalTranslation,
    categoryId,
    showImageAppShell,
    parentCategorySlug,
    categoryName,
    lastListURL,
    refereshDetailedPage,
    deviceStock,
    productName,
    binkiesFallbackVisible,
    setDeviceList,
    setLandingPageDeviceList
  } = props;

  const { promotionEndDate, promotionStartDate } = getPromotion(
    variant.characteristics
  );

  const binkiesId = getBinkiesID(variant.characteristics);

  const classes = cx('binkiesContainer', {
    visibilityHidden: binkiesFallbackVisible
  });

  const wrapperClass = cx('wrapper', {
    outOfStock: deviceStock === ITEM_STATUS.OUT_OF_STOCK
  });

  return (
    <>
      <StyledCarousel>
        <div className={wrapperClass}>
          <Header
            globalTranslation={globalTranslation}
            categoryId={categoryId}
            parentCategorySlug={parentCategorySlug}
            categoryName={categoryName}
            lastListURL={lastListURL}
            productName={productName}
            setDeviceList={setDeviceList}
            setLandingPageDeviceList={setLandingPageDeviceList}
          />
          {showImageAppShell && <ProductDetailedImageShell />}
          <StyledBinkiesAndFallBackWrapper>
            <div
              id='binkies-on-page'
              data-contentid={binkiesId}
              className={classes}
            />
            <ImageViewer
              productDetailedTranslation={productDetailedTranslation}
              variantId={variant.id}
              alt={variant.name}
              attachments={variant.attachments}
              outOfStock={deviceStock === ITEM_STATUS.OUT_OF_STOCK}
            />
          </StyledBinkiesAndFallBackWrapper>
          <div className='badgesAndTimer'>
            <Badges {...props} />
            {promotionEndDate && (
              <CountDownTimer
                variantId={variant.id}
                promotionEndDate={promotionEndDate}
                promotionStartDate={promotionStartDate}
                productDetailedTranslation={productDetailedTranslation}
                refereshDetailedPage={refereshDetailedPage}
              />
            )}
          </div>
        </div>
      </StyledCarousel>
    </>
  );
};

export default Carousel;
