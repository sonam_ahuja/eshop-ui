import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledImageViewer } from './ImageViewer/styles';

export const StyledBinkiesAndFallBackWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  position: relative;
  #binkies-on-page {
    position: relative;
    min-height: 2rem;
    z-index: 8;
  }
  ${StyledImageViewer},
  #binkies-on-page {
    flex: 1;
    display: flex;
    width: 100%;
    padding-right: 5.3rem;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    margin-top: 2.25rem;
    margin-bottom: 0.9rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 3rem;
    margin-bottom: 0rem;
  }
`;

export const StyledCarousel = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  position: relative;
  padding-top: 1rem;

  .productName {
    color: ${colors.darkGray};
  }

  .badgesAndTimer {
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 3rem;
    flex-shrink: 0;
    justify-self: flex-end;
    margin-top: auto;
  }
  .wrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .pos-fixed-bottom {
    position: absolute;
    bottom: 20px;
  }

  .visibilityHidden {
    display: none !important;
  }

  .outOfStock {
    opacity: 0.6;
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-top: 1.5rem;
  }
`;
