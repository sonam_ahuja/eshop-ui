import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@store/states/translation';

import Color from '.';
import { IProps } from './index';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props: IProps = {
      showDisabledAttributes: true,
      disabledAttributesClickable: false,
      variants: [
        {
          value: 'Blue',
          label: 'Colour',
          enabled: true,
          name: 'dummy'
        },
        {
          value: 'Red',
          label: 'Colour',
          enabled: true,
          name: 'dummy'
        }
      ],
      selected: {
        label: 'string',
        value: 'string',
        enabled: true,
        name: 'dummy'
      },
      colorTranslatedKey: 'Colour',
      productDetailedTranslation: translation().cart.productDetailed,
      onAttributeChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Color {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly with no variant', () => {
    const props: IProps = {
      variants: [],
      showDisabledAttributes: true,
      disabledAttributesClickable: true,
      selected: {
        label: 'string',
        enabled: true,
        value: 'string',
        name: 'dummy'
      },
      colorTranslatedKey: 'Colour',
      productDetailedTranslation: translation().cart.productDetailed,
      onAttributeChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Color {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with no variant, when attribute is disabled', () => {
    const props: IProps = {
      variants: [],
      showDisabledAttributes: false,
      disabledAttributesClickable: true,
      selected: {
        label: 'string',
        enabled: true,
        value: 'string',
        name: 'dummy'
      },
      colorTranslatedKey: 'Colour',
      productDetailedTranslation: translation().cart.productDetailed,
      onAttributeChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Color {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
