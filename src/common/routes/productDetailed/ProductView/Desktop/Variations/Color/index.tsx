import React from 'react';
import { Divider, Paragraph, Variant } from 'dt-components';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { colors } from '@common/variables';

import { StyledVariationsRow } from '../style';

import { StyledColor } from './styles';

export interface IProps {
  variants?: IVariantGroupsType[];
  selected?: IVariantGroupsType;
  colorTranslatedKey: string;
  productDetailedTranslation: IProductDetailedTranslation;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  onAttributeChange(atribute: IKeyValue): void;
}
const Color = (props: IProps) => {
  const {
    variants,
    selected,
    onAttributeChange,
    productDetailedTranslation,
    colorTranslatedKey
  } = props;
  if (!variants) {
    return null;
  }
  const selectedColor = selected && selected.value;
  const selectedColorLabel = selected && selected.label;
  const variantItems = variants
    .filter(variant => {
      if (props.showDisabledAttributes) {
        return true;
      }

      return variant.enabled;
    })
    .map(variant => (
      <Variant
        key={variant.value}
        name={variant.label}
        outOfStock={!variant.enabled}
        onClick={() => {
          if (
            (!props.disabledAttributesClickable && !variant.enabled) ||
            selectedColor === variant.value
          ) {
            return;
          }
          onAttributeChange({
            value: variant.value,
            name: colorTranslatedKey
          });
        }}
        variantSize='large'
        active={variant.value === selectedColor}
        value={variant.value}
        selectedBorderColor={colors.magenta}
      />
    ));

  return (
    <StyledColor>
      <StyledVariationsRow className='styledColorRow'>
        <ul>
          <li>
            <Paragraph
              firstLetterTransform='uppercase'
              size='medium'
              className='label'
            >
              {productDetailedTranslation.color}: (
              <span className='selectedColor'>{selectedColorLabel}</span>)
              {/* <span className='super'>tm</span> */}
            </Paragraph>
          </li>
          <li>
            <div className='variantsWrap'>{variantItems}</div>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRow>

      {/* <StyledVariationsRowSingle className='styledColorRowSingle'>
        <ul>
          <li>
            <Section size='large' className='label'>
              {productDetailedTranslation.color}:{' '}
              <span className='selectedColor'>{selectedColor}</span>
              <span> (out of stock)</span>
            </Section>
          </li>
          <li>
            <StyledVariantsWrap className='variantsWrap'>
              {variantItems}
            </StyledVariantsWrap>
          </li>
        </ul>
      </StyledVariationsRowSingle> */}
    </StyledColor>
  );
};

export default Color;
