import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledColor = styled.div`
  .styledColorRow {
    ul {
      padding-bottom: 0.7rem;
    }
    ul li:first-child {
      .label .selectedColor {
        /* text-transform: uppercase; */
      }
      .label .super {
        font-size: 0.65em;
        /* text-transform: uppercase; */
        transform: translateY(-50%);
        display: inline-block;
        padding-left: 0.25rem;
      }
    }

    ul li:last-child {
      min-width: 50%;
      max-width: 50%;
      padding-right: 0;
      .variantsWrap {
        margin-top: -0.5rem;
        line-height: 0;
      }
    }
  }

  .styledColorRowSingle {
    ul li:first-child {
      .label .selectedColor {
        /* text-transform: uppercase; */
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .styledColorRow {
      ul {
        padding-bottom: 0.7rem;
        padding-top: 0.25rem;
      }
      ul li:first-child .label {
        font-size: 0.75rem;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.33;
        letter-spacing: normal;
      }
      ul li:last-child {
        .variantsWrap {
          margin-top: -0.2rem;
        }
      }

      .variantsWrap > div {
        width: 1.25rem;
        height: 1.25rem;
        &:after {
          width: 0.75rem;
          height: 0.75rem;
        }
        &.outOfStock {
          &:before {
            height: 0.75rem;
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .styledColorRow ul li:first-child .label {
      font-size: 1rem;
      line-height: 1.5;
    }

    .styledColorRow {
      ul {
        padding-bottom: 1rem;
      }
    }

    .styledColorRow {
      ul li:last-child {
        .variantsWrap {
          margin-top: -0.2rem;
        }
      }

      .variantsWrap > div {
        width: 1.5rem;
        height: 1.5rem;
        &:after {
          width: 0.9rem;
          height: 0.9rem;
        }
        &.outOfStock {
          &:before {
            height: 0.9rem;
          }
        }
      }
    }
  }
`;
