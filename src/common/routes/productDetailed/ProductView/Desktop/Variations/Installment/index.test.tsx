import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { productDetailsSuccessResponse } from '@src/common/mock-api/productDetailed/details.mock';

import Installment, { IProps } from '.';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props: IProps = {
      productDetailedTranslation: appState().translation.cart.productDetailed,
      installmentGroup: productDetailsSuccessResponse.installmentGroup,
      showDisabledAttributes: false,
      disabledAttributesClickable: true,
      buyDeviceWithInstallments: true,
      onAttributeChange: jest.fn(),
      onInstallmentChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Installment {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
