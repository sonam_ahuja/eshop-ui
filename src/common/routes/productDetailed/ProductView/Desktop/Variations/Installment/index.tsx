import React, { ReactNode } from 'react';
import { Paragraph, Section, Select } from 'dt-components';
import SimpleSelect from '@common/components/Select/SimpleSelect';
import {
  IInstallmentData,
  IKeyValue
} from '@src/common/routes/productDetailed/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import cx from 'classnames';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledVariationsRow } from '../style';

import { StyledInstallment } from './styles';

export interface IProps {
  productDetailedTranslation: IProductDetailedTranslation;
  installmentGroup: IInstallmentData[];
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  buyDeviceWithInstallments: boolean;
  onAttributeChange(atribute: IKeyValue): void;
  onInstallmentChange(installmentValue: string | null): void;
}

interface IUpdatedInstallmentObj {
  id: number;
  title: string;
  rest: {
    amount: string | null;
    active: boolean;
    amountType: string;
    label: string;
    monthlySymbol: string | null;
    upfrontAmount: string;
    enabled: boolean;
  };
}

const Installment = (props: IProps) => {
  const {
    installmentGroup,
    onInstallmentChange,
    productDetailedTranslation,
    buyDeviceWithInstallments
  } = props;

  const updatedInstallmentArr: IUpdatedInstallmentObj[] = installmentGroup
    .filter(groupItem => {
      if (props.showDisabledAttributes) {
        return true;
      }

      return groupItem.enabled;
    })
    .map((item: IInstallmentData) => {
      return {
        id: item.id,
        title: item.title,
        rest: {
          amount: item.amount,
          active: item.active,
          amountType: item.amountType,
          label: item.label,
          monthlySymbol: item.monthlySymbol,
          upfrontAmount: item.upfrontAmount,
          enabled: item.enabled
        }
      };
    });

  const selectedInstallment = updatedInstallmentArr.find(item => {
    return item.rest.active === true;
  });
  const selectedInstallmentId = selectedInstallment
    ? selectedInstallment.id
    : null;

  return (
    <StyledInstallment>
      <StyledVariationsRow>
        <ul>
          <li>
            <Paragraph
              firstLetterTransform='uppercase'
              size='medium'
              className='label'
            >
              {buyDeviceWithInstallments
                ? productDetailedTranslation.installmentLabel
                : productDetailedTranslation.withoutInstallmentLabel}{' '}
              {/* <Icon color='currentColor' name='ec-information' /> */}
            </Paragraph>
          </li>
          <li>
            <div>
              <Select
                onItemSelect={item => {
                  if (
                    (!props.disabledAttributesClickable &&
                      !item.rest.enabled) ||
                    selectedInstallmentId === item.id
                  ) {
                    return;
                  }
                  sendDropdownClickEvent(item.title);
                  onInstallmentChange(String(item.id));
                }}
                className='select'
                selectedItem={selectedInstallment}
                SelectedItemComponent={selectedProps => {
                  return (
                    <>
                      <SimpleSelect {...selectedProps} />
                      <Section className='upfrontAmount ' size='small'>
                        {selectedProps.selectedItem &&
                          selectedProps.selectedItem.rest.upfrontAmount}
                      </Section>
                    </>
                  );
                }}
                items={updatedInstallmentArr}
                ListComponent={listProps => {
                  if (listProps.isOpen) {
                    const renderItems: ReactNode[] = [];

                    listProps.items.forEach(item => {
                      const classes = cx('singleOptionsList', {
                        active: item.rest.active
                      });

                      renderItems.push(
                        <div className=''>
                          <div
                            onClick={() => listProps.onItemSelect(item)}
                            className={classes}
                          >
                            <span className='labelTxt'>{item.rest.label}</span>
                            {item.id !== 0 && (
                              <span className='priceTxt'>
                                {item.rest.amount}{' '}
                                <span className='currencySymbol'>
                                  / {item.rest.monthlySymbol}
                                </span>
                              </span>
                            )}
                          </div>
                        </div>
                      );
                    });

                    return (
                      <StyledInstallment>
                        <div className='optionsList installmentsOptions'>
                          {renderItems}
                        </div>
                      </StyledInstallment>
                    );
                  }

                  return null;
                }}
              />
            </div>
          </li>
        </ul>
      </StyledVariationsRow>
    </StyledInstallment>
  );
};

export default Installment;
