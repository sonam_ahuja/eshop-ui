import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledInstallment = styled.div`
  .optionsListBelow {
    .installmentsOptions {
      width: 21.35rem;
      left: inherit;
      border-radius: 0.5rem;
      right: -0.75rem;
      margin-top: -1.25rem;
      .singleOptionsList {
        padding: 0.5rem 0.55rem 0.5rem 0.75rem;
        text-align: left;
        font-size: 0.875rem;
        line-height: 1rem;
        font-weight: bold;
        color: ${colors.darkGray};
        display: flex;
        justify-content: space-between;
        align-items: center;
        .lists {
          width: 100%;
          display: flex;
          justify-content: space-between;
        }
        .labelTxt {
          width: 55%;
          text-align: left;
        }
        .priceTxt {
          text-align: right;
          width: 45%;
          .currencySymbol {
            color: ${colors.mediumGray};
          }
        }
        &.active {
          color: ${colors.magenta};
        }
        &:hover {
          background: ${colors.fogGray};
        }
      }
    }
  }
  ul {
    li:first-child .label {
      i {
        vertical-align: text-top;
      }
    }

    li:last-child .upfrontAmount {
      color: ${colors.mediumGray};
      padding-right: 24px;
      font-size: 0.625rem;
      line-height: 0.75rem;
      position: absolute;
      right: 0;
      min-width: 10rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .optionsListBelow {
      .installmentsOptions {
        width: 21.35rem;
      }
    }

    ul li:first-child .label {
      padding-top: 0.25rem;
      font-size: 0.75rem;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.33;
      letter-spacing: normal;
    }
    ul li:last-child .upfrontAmount {
      font-size: 0.625rem;
      line-height: 1.33;
      margin-top: 0;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    ul li:first-child .label {
      font-size: 1rem;
      line-height: 1.5;
    }
    ul li:last-child .upfrontAmount {
      font-size: 0.75rem;
      line-height: 1.33;
      margin-top: 0.25rem;
    }
  }
`;
