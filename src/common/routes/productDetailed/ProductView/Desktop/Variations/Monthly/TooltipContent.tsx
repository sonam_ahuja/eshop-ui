import React, { ReactNode } from 'react';
import { Divider, Icon, Section } from 'dt-components';
import { IPrices } from '@tariff/store/types';
import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { formatCurrency } from '@src/common/utils/currency';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { getCurrencyCode } from '@store/common/index';

export interface IProps {
  price: IPrices;
  variantName: string;
  tariffName: string;
  productDetailedTranslation: IProductDetailedTranslation;
  onClose(): void;
}

const StyledTooltipContent = styled.div`
  color: ${colors.cloudGray};
  min-width: 10.5rem;

  .title {
    position: relative;
    padding-bottom: 0.5rem;
  }
  .iconClose {
    position: absolute;
    top: -0.5rem;
    right: -0.5rem;
  }

  .mainText {
    > p {
      display: flex;
      justify-content: space-between;
    }
  }

  .footerText {
    > p {
      display: flex;
      justify-content: space-between;
    }
  }

  .divider {
    min-height: 1px;
    color: ${colors.stoneGray};
    margin: 0.5rem 0;
  }
`;

export default (props: IProps) => {
  const {
    price: { discountedValue, actualValue, discounts },
    onClose,
    variantName,
    tariffName,
    productDetailedTranslation
  } = props;

  const discountRender = () => {
    const discountArr: ReactNode[] = [];

    discounts.forEach((item, idx) => {
      discountArr.push(
        <p key={idx}>
          <span className='label'>{item.label}</span>
          <span className='value'>
            {formatCurrency(item.discount, getCurrencyCode())}
          </span>
        </p>
      );
    });

    return discountArr;
  };

  return (
    <StyledTooltipContent>
      <Section className='title' size='large' weight='bold'>
        {variantName} {tariffName ? `+ ${tariffName}` : ''}
        <Icon
          className='iconClose'
          name='ec-cancel'
          color='currentColor'
          size='xsmall'
          onClick={onClose}
        />
      </Section>
      <Section className='mainText' size='large'>
        <p>
          <span className='label'>
            {productDetailedTranslation.fullDevicePrice}
          </span>
          <span className='value'>
            {formatCurrency(actualValue, getCurrencyCode())}
          </span>
        </p>
        {discounts && discounts.length > 0 ? discountRender() : null}
      </Section>
      <Divider className='divider' />
      <Section className='footerText' size='large'>
        <p>
          <span className='label'>
            {productDetailedTranslation.totalMonthly}
          </span>
          <span className='value'>
            {formatCurrency(
              discountedValue ? discountedValue : actualValue,
              getCurrencyCode()
            )}
          </span>
        </p>
      </Section>
    </StyledTooltipContent>
  );
};
