import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import translation from '@src/common/store/states/translation';

import Monthly, { IProps as IMonthlyProps } from '.';
import TooltipContent, {
  IProps as ITooltipContentProps
} from './TooltipContent';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props: IMonthlyProps = {
      variantName: 'variantName',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      tariffName: 'tariffName',
      recurringFee: {
        priceType: 'string',
        actualValue: 0,
        discounts: [
          {
            discount: 23,
            name: 'name',
            dutyFreeValue: 34,
            taxRate: 45,
            label: '',
            taxPercentage: 3
          }
        ],
        discountedValue: 0,
        recurringChargePeriod: 'string'
      },
      variantId: '123',
      productDetailedTranslation: appState().translation.cart.productDetailed
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Monthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly', () => {
    const props: ITooltipContentProps = {
      variantName: 'variantName',
      tariffName: 'tariffName',
      productDetailedTranslation: translation().cart.productDetailed,
      price: {
        priceType: 'string',
        actualValue: 0,
        discounts: [
          {
            discount: 23,
            name: 'name',
            dutyFreeValue: 34,
            taxRate: 45,
            label: '',
            taxPercentage: 3
          }
        ],
        discountedValue: 0,
        recurringChargePeriod: 'string'
      },
      onClose: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <TooltipContent {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('simulate events', () => {
    const props: IMonthlyProps = {
      variantName: 'variantName',
      tariffName: 'tariffName',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      recurringFee: {
        priceType: 'string',
        actualValue: 2,
        discountedValue: 1,
        discounts: [
          {
            discount: 23,
            name: 'name',
            dutyFreeValue: 34,
            taxRate: 45,
            label: '',
            taxPercentage: 3
          }
        ],
        recurringChargePeriod: 'string'
      },
      variantId: '123',
      productDetailedTranslation: appState().translation.cart.productDetailed
    };

    const component = mount<Monthly>(
      <ThemeProvider theme={{}}>
        <Monthly {...props} />
      </ThemeProvider>
    );

    (component.find('Monthly').instance() as Monthly).closeTooltip();
    (component.find('Monthly').instance() as Monthly).openTooltip();
  });
});
