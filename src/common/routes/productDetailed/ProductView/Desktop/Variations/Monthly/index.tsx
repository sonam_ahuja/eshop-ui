import React, { Component, ReactNode } from 'react';
import { Icon, Section, Title } from 'dt-components';
import { IPrices } from '@tariff/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { formatCurrency } from '@common/utils/currency';
import { AppTooltip } from '@src/common/components/Tooltip/styles';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';
import { sendOnDeviceDetailToolTipEvent } from '@events/DeviceDetail/index';

import { StyledMonthly } from './styles';
import TooltipContent from './TooltipContent';

export interface IProps {
  recurringFee?: IPrices;
  variantId: string;
  currency: ICurrencyConfiguration;
  productDetailedTranslation: IProductDetailedTranslation;
  variantName: string;
  tariffName: string;
}

interface IState {
  tooltipOpen: boolean;
}
export default class Monthly extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tooltipOpen: false
    };
    this.openTooltip = this.openTooltip.bind(this);
    this.closeTooltip = this.closeTooltip.bind(this);
  }

  openTooltip(): void {
    this.setState({
      tooltipOpen: true
    });
  }

  closeTooltip(): void {
    this.setState({
      tooltipOpen: false
    });
  }

  componentDidUpdate(prevProps: IProps): void {
    if (prevProps.variantId !== this.props.variantId) {
      this.closeTooltip();
    }
  }

  render(): ReactNode {
    const {
      recurringFee,
      productDetailedTranslation,
      variantName,
      tariffName,
      currency
    } = this.props;
    if (!recurringFee) {
      return null;
    }
    const isDiscounted =
      recurringFee.actualValue !== recurringFee.discountedValue;
    const tooltip = (
      <AppTooltip
        autoClose={true}
        destroyOnTriggerOut={true}
        position='topLeft'
        isOpen={this.state.tooltipOpen}
        onVisibilityChanges={tooltipOpen => {
          this.setState(
            {
              ...this.state,
              tooltipOpen
            },
            () => {
              if (this.state.tooltipOpen) {
                sendOnDeviceDetailToolTipEvent(
                  variantName,
                  recurringFee.discountedValue
                );
              }
            }
          );
        }}
        targetOffset={['-18', '-6']}
        tooltipContent={
          <TooltipContent
            price={recurringFee}
            tariffName={tariffName}
            variantName={variantName}
            onClose={this.closeTooltip}
            productDetailedTranslation={productDetailedTranslation}
          />
        }
      >
        <span>
          <Icon onClick={this.openTooltip} name='ec-information' />
        </span>
      </AppTooltip>
    );

    return (
      <StyledMonthly>
        <Section className='label' size='large'>
          {productDetailedTranslation.totalMonthly}{' '}
          {isDiscounted ? tooltip : null}
        </Section>
        <Title className='value' size='xsmall' weight='bold'>
          {formatCurrency(
            recurringFee.discountedValue
              ? recurringFee.discountedValue
              : recurringFee.actualValue,
            currency.locale
          )}
        </Title>
      </StyledMonthly>
    );
  }
}
