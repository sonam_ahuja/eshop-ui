import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledMonthly = styled.div`
  display: flex;
  justify-content: space-between;

  color: ${colors.magenta};

  .label {
    padding-top: 0.5rem;
    i {
      vertical-align: text-top;
    }
  }
  /** this is a hack solution because of safari issue: ECDEV-3489 ***/
  .dt_title::before {
    content: '';
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .label {
      padding-top: 0.3rem;
      font-size: 1rem;
      line-height: 1.5;
    }
    .value {
      font-size: 1.5rem;
      line-height: 1.17;
    }
  }
`;
