import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledPlans = styled.div`
  /* margin: 0.5rem -0.75rem 0; */
  margin-top: 0.7rem;

  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 0.5rem;
    margin-left: -0.75rem;
    margin-right: -0.75rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 1.1rem;
  }
`;
