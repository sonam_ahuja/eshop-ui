import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@store/states/translation';

import Storage, { IProps } from '.';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props: IProps = {
      showDisabledAttributes: false,
      disabledAttributesClickable: true,
      variants: [
        {
          value: 'Blue',
          label: 'Storage',
          enabled: true,
          name: 'dummy'
        },
        {
          value: 'Red',
          label: 'Storage',
          enabled: true,
          name: 'dummy'
        }
      ],
      selected: {
        label: 'string',
        value: 'string',
        enabled: true,
        name: 'dummy'
      },
      storageTranslatedKey: 'Storage',
      productDetailedTranslation: translation().cart.productDetailed,
      onAttributeChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Storage {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly with no variant', () => {
    const props: IProps = {
      showDisabledAttributes: true,
      disabledAttributesClickable: true,
      variants: [],
      selected: {
        label: 'string',
        enabled: true,
        value: 'string',
        name: 'dummy'
      },
      storageTranslatedKey: 'Storage',
      productDetailedTranslation: translation().cart.productDetailed,
      onAttributeChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Storage {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
