import React from 'react';
import { Divider, Paragraph, Select } from 'dt-components';
import SimpleSelect from '@common/components/Select/SimpleSelect';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledVariationsRow } from '../style';

import { StyledStorage } from './styles';

export interface IProps {
  variants?: IVariantGroupsType[];
  selected?: IVariantGroupsType;
  storageTranslatedKey: string;
  productDetailedTranslation: IProductDetailedTranslation;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  onAttributeChange(atribute: IKeyValue): void;
}

const Storage = (props: IProps) => {
  const {
    variants,
    selected,
    onAttributeChange,
    productDetailedTranslation,
    storageTranslatedKey,
    disabledAttributesClickable
  } = props;
  if (!variants) {
    return null;
  }
  const select = (
    <Select
      SelectedItemComponent={SimpleSelect}
      disableClickOnDisabledItem={!disabledAttributesClickable}
      onItemSelect={(item: IListItem) => {
        if (
          (!disabledAttributesClickable && item.disabled) ||
          (selected && selected.value === item.id)
        ) {
          return;
        }
        sendDropdownClickEvent(item.title);
        onAttributeChange({
          value: item.id as string,
          name: storageTranslatedKey
        });
      }}
      selectedItem={
        selected
          ? {
              id: selected.value,
              title: selected.label
            }
          : null
      }
      items={variants
        .filter(variant => {
          if (props.showDisabledAttributes) {
            return true;
          }

          return variant.enabled;
        })
        .map(variant => {
          return {
            id: variant.value,
            title: variant.label,
            disabled: !variant.enabled
          };
        })}
    />
  );

  return (
    <StyledStorage>
      <StyledVariationsRow>
        <ul>
          <li>
            <Paragraph
              firstLetterTransform='uppercase'
              size='medium'
              className='label'
            >
              {productDetailedTranslation.storage}
            </Paragraph>
          </li>
          <li>
            <div>{select}</div>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRow>

      {/* <StyledVariationsRowSingle>
        <ul>
          <li>
            <Section size='large' className='label'>
              Storage
            </Section>
          </li>
          <li>
            <div className='selectorWrap'>
              <Selector disabled type='main' label='test' />
              <Selector type='main' label='test' />
              <Selector active type='main' label='test' />
              <Selector type='main' label='test' />
              <Selector type='main' label='test' />
              <Selector type='main' label='test' />
              <Selector type='main' label='test' />
            </div>
          </li>
        </ul>
      </StyledVariationsRowSingle> */}
    </StyledStorage>
  );
};

export default Storage;
