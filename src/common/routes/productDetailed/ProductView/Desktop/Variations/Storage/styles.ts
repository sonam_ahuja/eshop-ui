import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledStorage = styled.div`
  .selectorWrap {
    margin: -0.125rem;

    > div {
      margin: 0.125rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ul li:first-child .label {
      padding-top: 0.25rem;
      font-size: 0.75rem;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.33;
      letter-spacing: normal;
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    ul li:first-child .label {
      font-size: 1rem;
      line-height: 1.5;
    }
  }
`;
