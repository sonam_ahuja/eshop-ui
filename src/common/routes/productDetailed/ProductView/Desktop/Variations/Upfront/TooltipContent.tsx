import React, { ReactNode } from 'react';
import { Divider, Icon, Section } from 'dt-components';
import { IPrices } from '@tariff/store/types';
import { formatCurrency } from '@common/utils/currency';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

export interface IProps {
  price: IPrices;
  currency: ICurrencyConfiguration;
  productDetailedTranslation: IProductDetailedTranslation;
  variantName: string;
  tariffName: string;
  onClose(): void;
}
export default (props: IProps) => {
  const {
    price: { discountedValue, actualValue, discounts },
    onClose,
    productDetailedTranslation,
    variantName,
    currency,
    tariffName
  } = props;

  const discountRender = () => {
    const discountArr: ReactNode[] = [];

    discounts.forEach((item, idx) => {
      discountArr.push(
        <ul key={idx}>
          <li> {item.label}</li>
          <li>{formatCurrency(item.discount, currency.locale)}</li>
        </ul>
      );
    });

    return discountArr;
  };

  return (
    <>
      <Section
        className='titleMain'
        size='large'
        weight='bold'
        transform={'capitalize'}
      >
        {variantName} {tariffName ? `+ ${tariffName}` : ''}
        <span onClick={onClose}>
          <Icon
            className='iconClose'
            name='ec-cancel'
            color='currentColor'
            size='xsmall'
          />
        </span>
      </Section>

      <div className='section'>
        <ul>
          <li> {productDetailedTranslation.fullDevicePrice} </li>
          <li>{formatCurrency(actualValue, currency.locale)}</li>
        </ul>
        {discounts && discounts.length > 0 ? discountRender() : null}
      </div>

      <Divider />

      <div className='section'>
        <ul>
          <li>{productDetailedTranslation.totalUpfront}</li>
          <li>
            {formatCurrency(
              discountedValue !== undefined ? discountedValue : actualValue,
              currency.locale
            )}
          </li>
        </ul>
      </div>
    </>
  );
};
