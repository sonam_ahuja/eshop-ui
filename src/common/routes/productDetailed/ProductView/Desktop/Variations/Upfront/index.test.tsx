import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@src/common/store/states/translation';

import TooltipContent, { IProps } from './TooltipContent';
import Upfront, { IProps as IUpfrontProps } from '.';

describe('<Upfront />', () => {
  const props: IUpfrontProps = {
    variantId: '1',
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    noInstallment: false,
    variantName: 'variantName',
    tariffName: 'tariffName',
    basePrice: {
      priceType: 'string',
      actualValue: 0,
      discounts: [
        {
          discount: 23,
          name: 'name',
          dutyFreeValue: 34,
          taxRate: 45,
          label: '',
          taxPercentage: 3
        }
      ],
      discountedValue: 0,
      recurringChargePeriod: 'string'
    },
    upfrontPrice: {
      priceType: 'string',
      actualValue: 0,
      discounts: [
        {
          discount: 23,
          name: 'name',
          dutyFreeValue: 34,
          taxRate: 45,
          label: '',
          taxPercentage: 3
        }
      ],
      discountedValue: 0,
      recurringChargePeriod: 'string'
    },
    buyDeviceOnly: false,
    productDetailedTranslation: translation().cart.productDetailed
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Upfront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('tooltipContent should render properly', () => {
    const newProps: IProps = {
      variantName: 'variantName',
      tariffName: 'tariffName',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      price: {
        priceType: 'string',
        actualValue: 0,
        discounts: [
          {
            discount: 23,
            name: 'name',
            dutyFreeValue: 34,
            taxRate: 45,
            label: '',
            taxPercentage: 3
          }
        ],
        discountedValue: 0,
        recurringChargePeriod: 'string'
      },
      onClose: jest.fn(),
      productDetailedTranslation: translation().cart.productDetailed
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <TooltipContent {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('simulate events', () => {
    const component = mount<Upfront>(
      <ThemeProvider theme={{}}>
        <Upfront {...props} />
      </ThemeProvider>
    );

    (component.find('Upfront').instance() as Upfront).closeTooltip();
    (component.find('Upfront').instance() as Upfront).openTooltip();
  });
});
