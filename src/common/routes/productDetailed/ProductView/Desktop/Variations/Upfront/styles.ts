import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledUpfront = styled.div<{ isDiscounted: boolean }>`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin-top: 2px;
  ${({ isDiscounted }) => (isDiscounted ? `color: ${colors.magenta}` : '')};

  .label {
    padding-top: 0.15rem;
    line-height: 0.75rem;
    align-self: center;

    ${({ isDiscounted }) => (isDiscounted ? '' : `color: ${colors.ironGray}`)};
    .dt_icon {
      font-size: 0.6rem;
    }
  }
  .value {
    ${({ isDiscounted }) =>
      isDiscounted ? '' : `color: ${colors.mediumGray}`};
    font-size: 0.875rem;
    line-height: 1.25rem;
    font-weight: bold;
  }
  .dt_icon svg path {
    ${({ isDiscounted }) => (isDiscounted ? '' : `fill: ${colors.ironGray}`)};
  }
  /** this is a hack solution because of safari issue: ECDEV-3489 ***/
  .dt_title::before {
    content: '';
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .label {
      padding-top: 0.3rem;
      font-size: 1rem;
      line-height: 1.5;
      .dt_icon {
        font-size: inherit;
        vertical-align: text-top;
      }
    }
    .value {
      font-size: 1.5rem;
      line-height: 1.17;
    }
  }
`;
