import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { tariff, tariffDetail, totalPrices } from '@mocks/tariff/tariff.mock';
import { productDetailsSuccessResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { ITEM_STATUS } from '@src/common/store/enums';
import { tariffCategoryMock } from '@src/common/mock-api/category/category.mock';

import { PRICE_TYPE } from '../../../store/enum';

import Variations, { IProps } from './index';

describe('<Variations />', () => {
  let props: IProps;
  beforeEach(() => {
    props = {
      addToBasketLoading: true,
      productName: '',
      buyDeviceWithInstallments: true,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      tariffCategory: tariffCategoryMock,
      installmentGroup: productDetailsSuccessResponse.installmentGroup,
      tariffGroup: tariffDetail,
      totalPrices,
      tariffs: tariff,
      addToBasket: jest.fn(),
      variantGroups: {
        Colour: [
          {
            value: 'value2',
            label: '',
            enabled: false,
            name: 'dummy'
          }
        ]
      },
      variant: {
        id: '',
        quantity: 12,
        group: '',
        unavailabilityReasonCodes: [ITEM_STATUS.OUT_OF_STOCK],
        name: '',
        description: '',
        characteristics: [
          {
            name: '',
            values: [{ value: 'value1' }]
          }
        ],
        prices: [
          {
            recurringChargePeriod: '',
            discounts: [
              {
                discount: 23,
                name: 'name',
                dutyFreeValue: 34,
                taxRate: 45,
                label: '',
                taxPercentage: 3
              }
            ],
            priceType: 'type',
            actualValue: 0,
            discountedValue: 0
          }
        ],
        tags: []
      },
      buyDeviceOnly: true,
      showVariantSelection: true,
      showDevicePrice: true,
      showTariffInfo: true,
      showNotifyMe: true,
      colorTranslatedKey: 'key',
      storageTranslatedKey: 'shifter',
      productDetailedTranslation: appState().translation.cart.productDetailed,
      showDisabledAttributes: true,
      disabledAttributesClickable: true,
      openNotificationModal: jest.fn(),
      onAttributeChange: jest.fn(),
      setAddToBasketBtnRef: jest.fn(),
      onInstallmentChange: jest.fn(),
      onPlanChange: jest.fn(),
      onAgreementChange: jest.fn(),
      youngTariffCheck: jest.fn(),
      showAllPlans: jest.fn(),
      considerInventory: true
    };
  });
  test('should render properly', () => {
    const newProps = { ...props };
    newProps.showDevicePrice = false;
    const component = mount(
      <ThemeProvider theme={{}}>
        <Variations {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with tariff detail id is -1', () => {
    const newProps = { ...props };
    newProps.tariffGroup[0].id = '-1';
    newProps.tariffGroup[0].active = true;
    newProps.installmentGroup[0].amountType = PRICE_TYPE.BASE_PRICE;
    newProps.showNotifyMe = false;
    newProps.showVariantSelection = false;

    const component = mount(
      <ThemeProvider theme={{}}>
        <Variations {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
