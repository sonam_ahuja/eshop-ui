import Storage from '@productDetailed/ProductView/Desktop/Variations/Storage';
import { IPrices } from '@tariff/store/types';
import Color from '@productDetailed/ProductView/Desktop/Variations/Color';
import { Button, Divider, Title } from 'dt-components';
import Plans from '@productDetailed/ProductView/Desktop/Variations/Plans';
import {
  IInstallmentData,
  IKeyValue,
  ITariff,
  ITariffDetail,
  IVariant,
  IVariantGroups
} from '@productDetailed/store/types';
import React from 'react';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import {
  getSelectedCharacterstic,
  getVariantPrices
} from '@productDetailed/utils';
import { ITEM_STATUS } from '@common/store/enums';
import { getOutOfStock } from '@productList/utils/getProductDetail';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';
import { ICategory } from '@category/store/types';

import { PRICE_TYPE } from '../../../store/enum';

import {
  StyledInnerScrollWrap,
  StyledMonthlyUpfrontWrap,
  StyledVariations
} from './style';
import Monthly from './Monthly';
import Upfront from './Upfront';
import Installment from './Installment';
// tslint:disable-next-line:no-commented-code
// import Description from './Description';
// import Feature from './Feature';

export interface IProps {
  installmentGroup: IInstallmentData[];
  tariffGroup: ITariffDetail[];
  currency: ICurrencyConfiguration;
  totalPrices: IPrices[];
  variant: IVariant;
  addToBasketLoading: boolean;
  productName: string;
  buyDeviceOnly: boolean;
  tariffCategory: ICategory;
  variantGroups: IVariantGroups;
  showVariantSelection: boolean;
  showDevicePrice: boolean;
  showTariffInfo: boolean;
  showNotifyMe: boolean;
  colorTranslatedKey: string;
  storageTranslatedKey: string;
  buyDeviceWithInstallments: boolean;
  productDetailedTranslation: IProductDetailedTranslation;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  tariffs: ITariff[];
  considerInventory: boolean;
  addToBasket(): void;
  openNotificationModal(): void;
  onAttributeChange(atribute: IKeyValue): void;
  setAddToBasketBtnRef(ref: HTMLElement | null): void;
  onInstallmentChange(installmentValue: string | null): void;
  onPlanChange(planId: string | null): void;
  onAgreementChange(agreementId: string | null): void;
  youngTariffCheck(planId: string): void;
  showAllPlans(): void;
}

const Variations = (props: IProps) => {
  const {
    variant,
    productName,
    variantGroups,
    openNotificationModal,
    onAttributeChange,
    showVariantSelection,
    showDevicePrice,
    showTariffInfo,
    addToBasket,
    showNotifyMe,
    productDetailedTranslation,
    colorTranslatedKey,
    storageTranslatedKey,
    setAddToBasketBtnRef,
    disabledAttributesClickable,
    installmentGroup,
    tariffGroup,
    totalPrices,
    onInstallmentChange,
    onPlanChange,
    onAgreementChange,
    youngTariffCheck,
    showAllPlans,
    currency,
    addToBasketLoading,
    buyDeviceWithInstallments,
    tariffCategory,
    considerInventory
  } = props;
  const { basePrice, upfrontPrice, recurringFee } = getVariantPrices(
    totalPrices
  );
  const selectedColor = getSelectedCharacterstic(
    variant.characteristics,
    variantGroups[colorTranslatedKey],
    colorTranslatedKey
  );
  const selectedStorage = getSelectedCharacterstic(
    variant.characteristics,
    variantGroups[storageTranslatedKey],
    storageTranslatedKey
  );

  const buyOnlyDeviceItem = tariffGroup.find(item => {
    return item.id === '-1';
  });

  const selectedTariff = tariffGroup.find(item => {
    return item.active === true && item.id !== '-1';
  });

  const noInstallmentItem = installmentGroup.find(item => {
    return item.amountType === PRICE_TYPE.BASE_PRICE;
  });

  const monthlyHide =
    buyOnlyDeviceItem &&
    buyOnlyDeviceItem.active &&
    noInstallmentItem &&
    noInstallmentItem.active;

  const addBtnText = (
    (variant && variant.unavailabilityReasonCodes) ||
    []
  ).includes(ITEM_STATUS.PRE_ORDER)
    ? productDetailedTranslation.preOrder
    : productDetailedTranslation.addToBasket;

  const addToCartBtn =
    variant.unavailabilityReasonCodes &&
    variant.unavailabilityReasonCodes.length &&
    getOutOfStock(variant.unavailabilityReasonCodes, considerInventory) ? (
      showNotifyMe ? (
        <Button
          size='medium'
          onClickHandler={openNotificationModal}
          loading={addToBasketLoading}
          data-event-id={EVENT_NAME.DEVICE_DETAIL.EVENTS.NOTIFY_ME}
          data-event-message={productDetailedTranslation.notifyMe}
          className='button'
        >
          {productDetailedTranslation.notifyMe}
        </Button>
      ) : null
    ) : (
      <Button
        size='medium'
        onClickHandler={addToBasket}
        className='button'
        loading={addToBasketLoading}
        data-event-id={EVENT_NAME.DEVICE_DETAIL.EVENTS.ADD_TO_BASKET}
        data-event-message={addBtnText}
      >
        {addBtnText}
      </Button>
    );

  return (
    <StyledVariations className='styledVariations wrapper-right'>
      <StyledInnerScrollWrap>
        <Title tag='h1' className='productName' size='large' weight='ultra'>
          {productName}
        </Title>
        {showVariantSelection ? (
          <Color
            productDetailedTranslation={productDetailedTranslation}
            variants={variantGroups[colorTranslatedKey]}
            selected={selectedColor}
            onAttributeChange={onAttributeChange}
            colorTranslatedKey={colorTranslatedKey}
            showDisabledAttributes={props.showDisabledAttributes}
            disabledAttributesClickable={disabledAttributesClickable}
          />
        ) : null}
        {showVariantSelection ? (
          <Storage
            productDetailedTranslation={productDetailedTranslation}
            variants={variantGroups[storageTranslatedKey]}
            selected={selectedStorage}
            onAttributeChange={onAttributeChange}
            storageTranslatedKey={storageTranslatedKey}
            showDisabledAttributes={props.showDisabledAttributes}
            disabledAttributesClickable={disabledAttributesClickable}
          />
        ) : null}
        <Installment
          productDetailedTranslation={productDetailedTranslation}
          installmentGroup={installmentGroup}
          onAttributeChange={onAttributeChange}
          showDisabledAttributes={props.showDisabledAttributes}
          disabledAttributesClickable={disabledAttributesClickable}
          onInstallmentChange={onInstallmentChange}
          buyDeviceWithInstallments={buyDeviceWithInstallments}
        />

        <Plans
          productDetailedTranslation={productDetailedTranslation}
          tariffGroup={tariffGroup}
          onAttributeChange={onAttributeChange}
          onPlanChange={onPlanChange}
          onAgreementChange={onAgreementChange}
          youngTariffCheck={youngTariffCheck}
          showAllPlans={showAllPlans}
          showTariffInfo={showTariffInfo}
          tariffCategory={tariffCategory}
        />

        <div className='bottomContentWrap'>
          {showDevicePrice ? (
            <StyledMonthlyUpfrontWrap>
              <Divider />
              {!monthlyHide ? (
                <Monthly
                  variantId={variant.id}
                  productDetailedTranslation={productDetailedTranslation}
                  recurringFee={recurringFee}
                  variantName={variant.name}
                  currency={currency}
                  tariffName={
                    (selectedTariff && selectedTariff.label) as string
                  }
                />
              ) : null}
              <Upfront
                variantId={variant.id}
                variantName={variant.name}
                currency={currency}
                noInstallment={
                  (noInstallmentItem && noInstallmentItem.active) as boolean
                }
                tariffName={(selectedTariff && selectedTariff.label) as string}
                buyDeviceOnly={
                  (buyOnlyDeviceItem && buyOnlyDeviceItem.active) as boolean
                }
                productDetailedTranslation={productDetailedTranslation}
                basePrice={basePrice}
                upfrontPrice={upfrontPrice}
              />
            </StyledMonthlyUpfrontWrap>
          ) : null}
        </div>
      </StyledInnerScrollWrap>
      <div className='buttonPalet'>
        {addToCartBtn ? (
          <div ref={setAddToBasketBtnRef} className='buttonWrap'>
            {addToCartBtn}
          </div>
        ) : null}
      </div>
    </StyledVariations>
  );
};

export default Variations;
