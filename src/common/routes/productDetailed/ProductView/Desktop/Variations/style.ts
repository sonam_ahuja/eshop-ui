import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledHorizontalScrollRow } from '@src/common/components/styles';

import { StyledMonthly } from './Monthly/styles';
import { StyledUpfront } from './Upfront/styles';

export const StyledInnerScrollWrap = styled.div`
  overflow-y: auto;
  padding: 2.25rem 3rem 1.25rem;
  height: 100%;
  display: flex;
  flex-direction: column;
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2.25rem 4.75rem 0;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 5.1rem 6rem 0 5rem;
  }
`;

export const StyledVariationsRow = styled.div`
  > ul {
    /* padding: 0.75rem 0 0.7rem; */
    padding: 1.15rem 0 1rem;
    display: flex;
    justify-content: space-between;

    > li:first-child {
      color: ${colors.ironGray};
    }

    > li:last-child {
      text-align: right;
      padding-right: 0.5rem;
    }
  }

  > .divider {
    color: ${colors.silverGray};
    margin: 0;
    min-height: 1px;
  }
  .optionsListBelow .optionsList {
    width: 5rem;
    font-size: 0.875rem;
    line-height: 1rem;
    margin-top: -1rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    > ul {
      padding-top: 0.75rem;
      padding-bottom: 0.75rem;

      > li:last-child {
        padding-right: 0rem;
      }
    }
    .optionsListBelow .optionsList {
      width: 4rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    > ul {
      padding-bottom: 0.9rem;

      > li:last-child {
        padding-top: 0.35rem;
      }
    }
  }
`;

export const StyledVariationsRowSingle = styled.div`
  > ul {
    padding: 1.25rem 0 3.25rem;

    > li:first-child {
      color: ${colors.mediumGray};
      margin-bottom: 0.5rem;
    }
  }

  > .divider {
    color: ${colors.silverGray};
    margin: 0;
    min-height: 1px;
  }
`;

export const StyledVariations = styled.div`
  width: 47.512%;
  flex-shrink: 0;
  background: ${colors.coldGray};
  display: flex;
  flex-direction: column;

  .bottomContentWrap {
    justify-content: flex-end;
  }
  .productName {
    padding-bottom: 1.5rem;
    width: 100%;
    color: ${colors.darkGray}; /* Bug #108 */
    /* white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis; */
  }

  ${StyledHorizontalScrollRow} {
    margin: 0 -1.25rem;
    .horizontalScrollRowInner {
      padding: 0 1.25rem;
    }
  }

  .bottomContentWrap {
    justify-self: flex-end;
    margin-top: auto;

    .dt_divider {
      min-height: 1px;
      height: 1px;
      margin-bottom: 1rem;
    }
  }

  .buttonPalet {
    position: sticky;
    bottom: 0;
    .buttonWrap {
      .button {
        width: 100%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    width: 50%;
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .productName {
      font-size: 2.5rem;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.1;
      letter-spacing: -0.25px;
      padding-bottom: 3.25rem;
    }

    .dt_select .styledSimpleSelect .dt_paragraph {
      font-size: 22.5px;
      font-weight: 500;
      line-height: 1.125rem;
      letter-spacing: normal;
    }
  }
`;

export const StyledMonthlyUpfrontWrap = styled.div`
  margin-top: 1.5rem;
  color: ${colors.silverGray};
  margin: 0;
  min-height: 1px;
  padding-bottom: 1rem;

  ${StyledMonthly} + ${StyledUpfront} {
    color: ${colors.ironGray};
    margin-top: 0.25rem;
    .value{
      font-size: 0.875rem;
      font-weight: bold;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${StyledMonthly} + ${StyledUpfront} {
      margin-top: 0.1rem;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    ${StyledMonthly} + ${StyledUpfront} {
      .value{
        font-size: 1rem;
        line-height: 1.5;
      }
    }
  }
`;
