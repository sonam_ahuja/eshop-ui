import React from 'react';
import Carousel from '@productDetailed/ProductView/Desktop/Carousel';
import Variations from '@productDetailed/ProductView/Desktop/Variations';
import {
  eligibilityUnavailabilityReasonType,
  IInstallmentData,
  IKeyValue,
  IProductDetailedData,
  ITariffDetail
} from '@productDetailed/store/types';
import { IPrices } from '@tariff/store/types';
import {
  IGlobalTranslation,
  IProductDetailedTranslation
} from '@common/store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductDetailedConfiguration
} from '@common/store/types/configuration';
import { ICategory } from '@category/store/types';

import { StyledProductViewDesktop } from './styles';

interface IProps {
  productDetailsData: IProductDetailedData;
  tariffCategory: ICategory;
  installmentDropdownData: IInstallmentData[];
  tariffDropdownData: ITariffDetail[];
  totalPrices: IPrices[];
  productDetailedConfiguration: IProductDetailedConfiguration;
  catalogConfiguration: ICatalogConfiguration;
  productDetailedTranslation: IProductDetailedTranslation;
  buyDeviceOnly: boolean;
  globalTranslation: IGlobalTranslation;
  categoryId: string | null;
  showImageAppShell: boolean;
  binkiesFallbackVisible: boolean;
  categoryName: string;
  parentCategorySlug: string;
  lastListURL: string | null;
  deviceStock: eligibilityUnavailabilityReasonType;
  addToBasketLoading: boolean;
  currency: ICurrencyConfiguration;
  addToBasket(): void;
  openNotificationModal(): void;
  refereshDetailedPage(): void;
  setVariantNameRef(ref: HTMLElement | null): void;
  setImageAppShell(showAppShell: boolean): void;
  onAttributeChange(atribute: IKeyValue): void;
  setAddToBasketBtnRef(ref: HTMLElement | null): void;
  changeBinkiesFallbackVisible(visible: boolean): void;
  onInstallmentChange(installmentValue: string | null): void;
  onPlanChange(planId: string | null): void;
  onAgreementChange(agreementId: string | null): void;
  youngTariffCheck(planId: string): void;
  showAllPlans(): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}

const ProductViewDesktop = (props: IProps) => {
  const {
    productDetailsData: { variants, groups, productName, tariffs },
    addToBasket,
    openNotificationModal,
    onAttributeChange,
    productDetailedConfiguration: { productDetailedTemplate1 },
    catalogConfiguration: {
      preorder: { considerInventory }
    },
    productDetailedTranslation,
    buyDeviceOnly,
    showImageAppShell,
    categoryId,
    globalTranslation,
    parentCategorySlug,
    categoryName,
    lastListURL,
    setImageAppShell,
    refereshDetailedPage,
    binkiesFallbackVisible,
    changeBinkiesFallbackVisible,
    installmentDropdownData,
    tariffDropdownData,
    totalPrices,
    onInstallmentChange,
    onPlanChange,
    onAgreementChange,
    youngTariffCheck,
    showAllPlans,
    currency,
    setDeviceList,
    setLandingPageDeviceList,
    tariffCategory,
    addToBasketLoading
  } = props;

  if (variants.length === 0) {
    return null;
  }

  return (
    <StyledProductViewDesktop>
      <Carousel
        showbadges={productDetailedTemplate1.showbadges}
        showOnlineStockAvailability={
          productDetailedTemplate1.showOnlineStockAvailability
        }
        binkiesFallbackVisible={binkiesFallbackVisible}
        productDetailedTranslation={productDetailedTranslation}
        globalTranslation={globalTranslation}
        variant={variants[0]}
        categoryId={categoryId}
        showImageAppShell={showImageAppShell}
        parentCategorySlug={parentCategorySlug}
        categoryName={categoryName}
        productName={productName}
        lastListURL={lastListURL}
        setImageAppShell={setImageAppShell}
        refereshDetailedPage={refereshDetailedPage}
        deviceStock={props.deviceStock}
        setDeviceList={setDeviceList}
        setLandingPageDeviceList={setLandingPageDeviceList}
        changeBinkiesFallbackVisible={changeBinkiesFallbackVisible}
        newProductBeforeLaunchDays={
          props.productDetailedConfiguration.newProductBeforeLaunchDays
        }
      />
      <Variations
        installmentGroup={installmentDropdownData}
        tariffGroup={tariffDropdownData}
        totalPrices={totalPrices}
        tariffCategory={tariffCategory}
        addToBasket={addToBasket}
        openNotificationModal={openNotificationModal}
        variant={variants[0]}
        productName={productName}
        currency={currency}
        buyDeviceOnly={buyDeviceOnly}
        variantGroups={groups}
        tariffs={tariffs}
        onAttributeChange={onAttributeChange}
        showVariantSelection={productDetailedTemplate1.showVariantSelection}
        showDevicePrice={productDetailedTemplate1.showDevicePrice}
        showTariffInfo={productDetailedTemplate1.showTariffInfo}
        showNotifyMe={productDetailedTemplate1.showNotifyMe}
        productDetailedTranslation={productDetailedTranslation}
        colorTranslatedKey={props.productDetailedConfiguration.color}
        buyDeviceWithInstallments={
          props.productDetailedConfiguration.buyDeviceWithInstallments
        }
        storageTranslatedKey={props.productDetailedConfiguration.storage}
        setAddToBasketBtnRef={props.setAddToBasketBtnRef}
        showDisabledAttributes={
          props.productDetailedConfiguration.showDisabledAttributes
        }
        disabledAttributesClickable={
          props.productDetailedConfiguration.disabledAttributesClickable
        }
        onInstallmentChange={onInstallmentChange}
        onPlanChange={onPlanChange}
        onAgreementChange={onAgreementChange}
        youngTariffCheck={youngTariffCheck}
        showAllPlans={showAllPlans}
        addToBasketLoading={addToBasketLoading}
        considerInventory={considerInventory}
      />
    </StyledProductViewDesktop>
  );
};

export default ProductViewDesktop;
