import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledProductViewDesktop = styled.div`
  display: flex;
  min-height: calc(100vh - 6.5625rem);
  max-height: calc(100vh - 6.5625rem);
  padding-left: 3rem;
  /* padding-left: 4.9rem; */

  @media (min-width: ${breakpoints.desktop}px) {
    min-height: calc(100vh - 6.25rem);
    padding-left: 4.9rem;
    max-height:calc(100vh - 6.25rem);
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-left: 5.5rem;
    min-height: calc(100vh - 7.25rem);
    max-height: calc(100vh - 7.25rem);
  }

  @media (max-width: ${breakpoints.desktop - 1}px) and (min-height: ${
  breakpoints.tabletPortrait
}px) {
    min-height: auto !important;

  }

  /* @media (min-width: ${
    breakpoints.desktopLarge
  }px) and (min-height: 1100px) {
    min-height: auto !important;
  } */
`;
