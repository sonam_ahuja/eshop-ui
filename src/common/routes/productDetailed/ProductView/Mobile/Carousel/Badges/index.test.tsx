import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Component from '.';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props = {
      tags: ['']
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
