import React from 'react';
import { Badge } from 'dt-components';

import { StyledHeader } from './styles';

interface IProps {
  tags: string[];
}
const Header = (props: IProps) => {
  const { tags } = props;
  const badges = tags.map(tag => <Badge key={tag} label={tag} />);

  return (
    <StyledHeader>
      <div className='badgesWrap'>{badges}</div>
    </StyledHeader>
  );
};

export default Header;
