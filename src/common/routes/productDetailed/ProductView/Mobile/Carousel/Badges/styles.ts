import styled from 'styled-components';

export const StyledHeader = styled.div`
  padding-bottom: 1.25rem;

  .badgesWrap {
    margin: 0 -0.25rem;
    > div {
      margin-right: 0.25rem;
      cursor: inherit;
      user-select: none;
      line-height: 1rem;
      margin-bottom: 0.25rem;
    }
  }
`;
