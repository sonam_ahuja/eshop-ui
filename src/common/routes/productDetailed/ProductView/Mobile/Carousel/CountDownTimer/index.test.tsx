import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';

import Component, { IProps } from '.';

describe('<CountDownTimer />', () => {
  const props: IProps = {
    variantId: '12',
    promotionEndDate: '12/12/2017',
    promotionStartDate: '12/12/2017',
    productDetailedTranslation: appState().translation.cart.productDetailed,
    refereshDetailedPage: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
    component.unmount();
  });
});
