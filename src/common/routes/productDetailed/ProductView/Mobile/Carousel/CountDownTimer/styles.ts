import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledCountDownTimer = styled.div`
  .timer {
    width: 100%;
    height: 3rem;
    background-color: ${colors.charcoalGray};
    color: ${colors.white};
    display: flex;
    align-items: center;

    .icon-wrap {
      font-size: 2rem;
      margin-left: 1rem;
      flex-shrink: 0;
    }

    ul {
      padding-left: 2rem;
      display: flex;
      justify-self: flex-end;
      margin-left: auto;
      padding-right: 2.0625rem;

      li {
        padding-right: 0.8125rem;
        position: relative;

        &:nth-child(1) {
          padding-right: 1rem;
        }
        &:nth-child(2):after,
        &:nth-child(3):after {
          content: ':';
          position: absolute;
          right: 0.3rem;
          bottom: auto;
        }
        &:last-child {
          padding-right: 0;
        }

        .titles {
          font-size: 0.625rem;
          line-height: 1.2;
          display: block;
          color: ${colors.mediumGray};
        }
        .counters {
          font-size: 0.875rem;
          /* line-height: 1.25rem; */
          line-height: 1;
          color: ${colors.white};
          font-weight: bold;
        }
      }
    }
  }
`;
