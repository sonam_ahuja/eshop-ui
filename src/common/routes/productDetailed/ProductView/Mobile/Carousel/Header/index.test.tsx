import React from 'react';
import { shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { withRouter } from 'react-router';

import Header from '.';

// tslint:disable-next-line:no-any
const HeaderWithRouter = withRouter(Header as any);

describe('<Header />', () => {
  test('should render properly', () => {
    const component = shallow(
      <ThemeProvider theme={{}}>
        <HeaderWithRouter />
      </ThemeProvider>
    );

    expect(component).toMatchSnapshot();
  });
});
