import React from 'react';
import { IGlobalTranslation } from '@src/common/store/types/translation';
import { RouteComponentProps, withRouter } from 'react-router';
import { IItemList } from 'dt-components/lib/es/components/atoms/breadcrumb';
import CatalogBreadCrumb from '@common/components/CatalogBreadCrumb';
import { isCategoryLandingPageRequired } from '@common/store/common/index';

import { StyledHeader } from './styles';

export interface IList extends IItemList {
  name?: string;
}

interface IProps extends RouteComponentProps {
  totalSlides: number;
  activeSlideIdx: number;
  globalTranslation: IGlobalTranslation;
  productName: string;
  categoryName: string;
  parentCategorySlug: string;
  categoryId: string | null;
  lastListURL: string | null;
  binkiesFallbackVisible: boolean;
  changeActiveSlide(idx: number): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}

const onBreadcrumbItemClick = (
  setDeviceList: (list: string) => void,
  setLandingPageDeviceList: (list: string) => void
) => {
  return (item: IList) => {
    if (item && item.name === 'deviceList') {
      setDeviceList('DEVICE DETAILED');
    } else if (item && item.name === 'home') {
      setLandingPageDeviceList('DEVICE DETAILED');
    }
  };
};
const Header = (props: IProps) => {
  const {
    globalTranslation,
    productName,
    categoryId,
    categoryName,
    lastListURL,
    parentCategorySlug,
    totalSlides,
    changeActiveSlide,
    activeSlideIdx,
    binkiesFallbackVisible,
    setDeviceList,
    setLandingPageDeviceList
  } = props;

  const dots = [];
  for (let i = 0; i < totalSlides; i++) {
    dots.push(
      <li key={i} className={i === activeSlideIdx ? 'active' : ''}>
        <a onClick={() => changeActiveSlide(i)} />
      </li>
    );
  }

  const categoryRoute = `/category/${parentCategorySlug}`;
  const deviceListingRoute =
    lastListURL === null ? `/${categoryId}` : `/${categoryId}${lastListURL}`;

  const categorylandingPage = isCategoryLandingPageRequired()
    ? [
        {
          id: categoryRoute,
          active: false,
          title: globalTranslation.products
        }
      ]
    : [];

  const breadCrumbItemList = [
    {
      id: '/',
      title: globalTranslation.home,
      name: 'home'
    },
    ...categorylandingPage,
    {
      id: deviceListingRoute,
      title: categoryName,
      name: 'deviceList'
    }
  ];

  const breadCrumbItems = [
    {
      title: globalTranslation.home,
      active: false,
      link: '/'
    },
    ...categorylandingPage,
    {
      title: categoryName,
      active: false,
      link: deviceListingRoute
    },
    {
      title: productName,
      active: true
    }
  ];

  return (
    <StyledHeader>
      <CatalogBreadCrumb
        className='breadcrumb'
        breadCrumbItemList={breadCrumbItemList}
        onBreadcrumbItemClick={onBreadcrumbItemClick(
          setDeviceList,
          setLandingPageDeviceList
        )}
        children={null}
        breadCrumbItems={breadCrumbItems}
      />
      {binkiesFallbackVisible && (
        <div className='carouselDots'>
          <ul>{dots}</ul>
        </div>
      )}
    </StyledHeader>
  );
};

export default withRouter(Header);
