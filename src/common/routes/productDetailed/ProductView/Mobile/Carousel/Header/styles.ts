import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledBreadcrumbWrap } from '@src/common/components/CatalogBreadCrumb/styles';

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative; /* fixes for dots alignment */

  ${StyledBreadcrumbWrap} {
    padding: 0 !important;
  }

  .carouselDots {
    position: absolute;
    top: 0;
    right: 0;
    /* background: #fff; */
    height: 1rem;
    display: flex;
    align-items: center;
    ul {
      font-size: 0;

      li {
        display: inline-block;
        height: 5px;
        width: 5px;
        margin-left: 5px;

        a {
          display: block;
          height: 100%;
          width: 100%;
          border-radius: 50%;
          overflow: hidden;
          text-decoration: none;
          background: ${colors.lightGray};
        }

        &.active {
          a {
            background: ${colors.black};
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .carouselDots {
      display: none;
    }
  }
`;
