import React from 'react';
import { shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Image360Modal, { IProps } from '.';

describe('<Image360Modal />', () => {
  const props: IProps = { isOpen: true, onClose: jest.fn() };
  test('should render properly', () => {
    const component = shallow(
      <ThemeProvider theme={{}}>
        <Image360Modal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when outofStock is true', () => {
    const newProps: IProps = { isOpen: true, onClose: jest.fn() };
    const component = shallow(
      <ThemeProvider theme={{}}>
        <Image360Modal {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
