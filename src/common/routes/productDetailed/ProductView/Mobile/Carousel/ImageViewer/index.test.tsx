import React from 'react';
import { shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { IGroup1 } from '@productDetailed/store/types';
import ImageViewer, {
  IProps
} from '@productDetailed/ProductView/Mobile/Carousel/ImageViewer';

describe('<ImageViewer />', () => {
  const group: IGroup1[] = [
    {
      type: '.jpg',
      name: 'name',
      url: 'url'
    }
  ];
  const props: IProps = {
    imageItems: group,
    outOfStock: false,
    productDetailedTranslation: appState().translation.cart.productDetailed,
    setSlideInstance: jest.fn(),
    onSlideChange: jest.fn()
  };
  test('should render properly', () => {
    const component = shallow(
      <ThemeProvider theme={{}}>
        <ImageViewer {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when outofStock is true', () => {
    const newProps: IProps = { ...props, outOfStock: true };
    const component = shallow(
      <ThemeProvider theme={{}}>
        <ImageViewer {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
