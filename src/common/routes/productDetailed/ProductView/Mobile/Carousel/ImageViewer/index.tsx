import React from 'react';
import { IGroup1 } from '@productDetailed/store/types';
import ReactSlick from 'react-slick';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import PreloadImage from '@src/common/components/Preload';
import { IMAGE_TYPE } from '@common/store/enums';

import { StyledImageViewer } from './styles';

export interface IProps {
  imageItems: IGroup1[];
  outOfStock: boolean;
  productDetailedTranslation: IProductDetailedTranslation;
  alt?: string;
  setSlideInstance(slider: ReactSlick | null): void;
  onSlideChange(idx: number): void;
  // open360Modal(openModal: boolean): void;
}
const ImageViewer = (props: IProps) => {
  const { onSlideChange, setSlideInstance, imageItems, alt } = props;
  const productImages = imageItems.map(item => (
    <PreloadImage
      key={item.url}
      isObserveOnScroll={false}
      imageHeight={10}
      width={240}
      mobileWidth={240}
      type={IMAGE_TYPE.WEBP}
      imageWidth={10}
      imageUrl={item.url}
      alt={alt}
    />
  ));

  return (
    <StyledImageViewer
      className='styledImageViewer'
      outOfStock={props.outOfStock}
    >
      {/* {productImages.length === 0 && (
        <Paragraph>{productDetailedTranslation.noImagePreview}</Paragraph>
      )} */}
      <ReactSlick
        afterChange={onSlideChange}
        ref={setSlideInstance}
        arrows={false}
        speed={500}
      >
        {productImages}
      </ReactSlick>
    </StyledImageViewer>
  );
};

export default ImageViewer;
