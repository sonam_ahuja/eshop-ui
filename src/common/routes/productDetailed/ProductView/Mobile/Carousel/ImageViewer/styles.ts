import styled from 'styled-components';
export const StyledImageViewer = styled.div<{ outOfStock: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  margin: auto;

  padding: 0.75rem 0 0.75rem;

  .outOfStock {
    opacity: 0.4;
  }

  opacity: ${props => {
    return props.outOfStock ? '0.4' : '1';
  }};

  .slick-slide > div > div {
    padding: 0;
  }

  .slick-slide img {
    display: block;
    margin: auto;
    max-width: 100%;
    max-height: 100%;
    opacity: 0;
    transition-property: opacity;
  }
  .slick-slide.slick-active img {
    opacity: 1;
  }
  .slick-slider,
  .slick-list,
  .slick-track,
  .slick-slide {
    height: 100%;
  }
  .slick-slide div {
    height: 100%;
  }
`;
