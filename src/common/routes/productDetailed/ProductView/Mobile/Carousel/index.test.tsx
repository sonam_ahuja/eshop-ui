import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { BrowserRouter as Router } from 'react-router-dom';
import translation from '@common/store/states/translation';
import { IWindow } from '@src/client/index';
import { ITEM_STATUS } from '@src/common/store/enums';

import Carousel, { IProps } from '.';

describe('<ProductImages />', () => {
  const newBinkies: IWindow['binkies'] = {
    onPageStyleSheetUrl: '/url',
    inModalStyleSheetUrl: '/url',
    contentIdentifier: '/url',
    // on3DNotSupported: jest.fn(),
    onLoadingProgress: jest.fn(),
    show: jest.fn(),
    hide: jest.fn()
  };
  (window as IWindow).binkies = newBinkies;
  const props: IProps = {
    productName: '',
    categoryName: 'catrogry',
    parentCategorySlug: '',
    changeBinkiesFallbackVisible: jest.fn(),
    showImageAppShell: true,
    lastListURL: '1',
    deviceStock: ITEM_STATUS.IN_STOCK,
    binkiesFallbackVisible: false,
    setImageAppShell: jest.fn(),
    setVariantNameRef: jest.fn(),
    variant: {
      id: '',
      name: '',
      group: '',
      description: '',
      characteristics: [
        {
          values: [{ value: 'value2' }],
          name: ''
        }
      ],
      attachments: {
        thumbnail: [{ type: '', url: '', name: '' }]
      },
      prices: [],
      tags: [],
      quantity: 34,
      unavailabilityReasonCodes: []
    },
    categoryId: '1',
    globalTranslation: translation().cart.global,
    productDetailedTranslation: translation().cart.productDetailed,
    refereshDetailedPage: jest.fn(),
    setDeviceList: jest.fn(),
    setLandingPageDeviceList: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Carousel {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('should render properly', () => {
    const newProps = { ...props };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Carousel {...newProps} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when device is tablet', () => {
    jest.mock('@src/common/utils', () => ({
      isMobile: {
        tablet: true
      }
    }));
    const wrapper = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Carousel {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(wrapper).toMatchSnapshot();
  });

  test('run onSlideChange', () => {
    const reactSlider = {
      slickNext: jest.fn(),
      slickPause: jest.fn(),
      slickPlay: jest.fn(),
      slickPrev: jest.fn(),
      slickGoTo: jest.fn()
    };
    const wrapper = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Carousel {...props} />
        </Router>
      </ThemeProvider>
    );
    (wrapper
      .find('Carousel')
      // tslint:disable-next-line: no-any
      .instance() as Carousel).slider = reactSlider as any;
    (wrapper.find('Carousel').instance() as Carousel).onSlideChange(123);
    (wrapper.find('Carousel').instance() as Carousel).changeActiveSlide(123);
    expect(wrapper).toMatchSnapshot();
  });
});
