import ProductDetailedImageShell from '@productDetailed/image.shell';
import React, { Component, ReactNode } from 'react';
import {
  eligibilityUnavailabilityReasonType,
  IVariant
} from '@productDetailed/store/types';
import ReactSlick from 'react-slick';
import {
  IGlobalTranslation,
  IProductDetailedTranslation
} from '@src/common/store/types/translation';
import { ITEM_STATUS } from '@src/common/store/enums';
import { Title } from 'dt-components';
import { getBinkiesID, getPromotion } from '@productDetailed/utils';
import CountDownTimer from '@productDetailed/ProductView/Mobile/Carousel/CountDownTimer';
import ImageViewerDesktop from '@productDetailed/ProductView/Desktop/Carousel/ImageViewer';
import cx from 'classnames';
import { isMobile } from '@src/common/utils';

import Header from './Header';
import ImageViewer from './ImageViewer';
import {
  StyledBinkiesAndFallBackWrapper,
  StyledBottomContent,
  StyledCarousel
} from './styles';

export interface IProps {
  variant: IVariant;
  productName: string;
  productDetailedTranslation: IProductDetailedTranslation;
  globalTranslation: IGlobalTranslation;
  categoryId: string | null;
  showImageAppShell: boolean;
  binkiesFallbackVisible: boolean;
  categoryName: string;
  parentCategorySlug: string;
  lastListURL: string | null;
  deviceStock: eligibilityUnavailabilityReasonType;
  changeBinkiesFallbackVisible(visible: boolean): void;
  setVariantNameRef(ref: HTMLElement | null): void;
  refereshDetailedPage(): void;
  setImageAppShell(showAppShell: boolean): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}

interface IState {
  activeSlideIndex: number;
}

export default class Carousel extends Component<IProps, IState> {
  slider: ReactSlick | null = null;

  constructor(props: IProps) {
    super(props);
    this.state = {
      activeSlideIndex: 0
    };
    this.setSlideInstance = this.setSlideInstance.bind(this);
    this.onSlideChange = this.onSlideChange.bind(this);
    this.changeActiveSlide = this.changeActiveSlide.bind(this);
  }

  setSlideInstance(slider: ReactSlick | null): void {
    this.slider = slider;
  }

  onSlideChange(idx: number): void {
    this.setState({
      activeSlideIndex: idx
    });
  }

  changeActiveSlide(idx: number): void {
    if (this.slider) {
      this.slider.slickGoTo(idx);
    }
  }

  render(): ReactNode {
    const {
      variant,
      productName,
      globalTranslation,
      categoryId,
      showImageAppShell,
      parentCategorySlug,
      categoryName,
      lastListURL,
      deviceStock,
      refereshDetailedPage,
      productDetailedTranslation,
      binkiesFallbackVisible,
      setDeviceList,
      setLandingPageDeviceList
    } = this.props;

    const binkiesId = getBinkiesID(variant.characteristics);
    const classes = cx('binkiesContainer', {
      visibilityHidden: binkiesFallbackVisible
    });

    const wrapperClass = cx({
      outOfStock: deviceStock === ITEM_STATUS.OUT_OF_STOCK
    });

    const { promotionEndDate, promotionStartDate } = getPromotion(
      variant.characteristics
    );

    const imageItems =
      variant.attachments && variant.attachments.thumbnail
        ? variant.attachments.thumbnail.slice(0, 4)
        : [];

    return (
      <>
        <StyledCarousel>
          <Header
            totalSlides={imageItems.length}
            setDeviceList={setDeviceList}
            setLandingPageDeviceList={setLandingPageDeviceList}
            activeSlideIdx={this.state.activeSlideIndex}
            changeActiveSlide={this.changeActiveSlide}
            globalTranslation={globalTranslation}
            productName={productName}
            categoryId={categoryId}
            parentCategorySlug={parentCategorySlug}
            categoryName={categoryName}
            lastListURL={lastListURL}
            binkiesFallbackVisible={binkiesFallbackVisible}
          />
          {showImageAppShell && <ProductDetailedImageShell />}
          <StyledBinkiesAndFallBackWrapper className={wrapperClass}>
            <div
              id='binkies-on-page'
              data-contentid={binkiesId}
              className={classes}
            />
            {isMobile.tablet ? (
              <ImageViewerDesktop
                productDetailedTranslation={productDetailedTranslation}
                variantId={variant.id}
                alt={variant.name}
                attachments={variant.attachments}
                outOfStock={deviceStock === ITEM_STATUS.OUT_OF_STOCK}
              />
            ) : (
              <ImageViewer
                outOfStock={deviceStock === ITEM_STATUS.OUT_OF_STOCK}
                imageItems={imageItems}
                setSlideInstance={this.setSlideInstance}
                onSlideChange={this.onSlideChange}
                productDetailedTranslation={productDetailedTranslation}
                alt={variant.description}
              />
            )}
          </StyledBinkiesAndFallBackWrapper>

          <StyledBottomContent>
            <Title tag='h1' className='productName' size='large' weight='ultra'>
              <span ref={this.props.setVariantNameRef}>{productName}</span>
            </Title>

            {promotionEndDate && (
              <CountDownTimer
                variantId={variant.id}
                promotionEndDate={promotionEndDate}
                productDetailedTranslation={productDetailedTranslation}
                promotionStartDate={promotionStartDate}
                refereshDetailedPage={refereshDetailedPage}
              />
            )}
          </StyledBottomContent>
        </StyledCarousel>
      </>
    );
  }
}
