import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

import { StyledCountDownTimer } from './CountDownTimer/styles';

export const StyledCarousel = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 1rem 1.25rem 2rem;

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-left: 2.25rem;
    padding-right: 2.25rem;
  }
`;

export const StyledBinkiesAndFallBackWrapper = styled.div`
  flex: 1;
  position: relative;
  width: 100%;
  height: 15rem;
  /* margin: 2.4rem 0 2rem;  */
  margin: 2.75rem 0 1.75rem;

  .outOfStock {
    opacity: 0.6;
  }

  .visibilityHidden {
    visibility: hidden !important;
  }
  /* .binkiesContainer {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
  } */
  #binkies-on-page {
    width: 100%;
    height: 100%;
    position: relative;
    padding-right: 0;
    z-index: 8;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    /* If maching size accordion to zepling spacing */
    /* height: 25rem;
    margin-top: 6.8125rem;
    margin-bottom: 5rem; */

    height: 27rem;
    margin-top: 7.3rem;
    margin-bottom: 2.5rem;
  }
`;

export const StyledBottomContent = styled.div`
  .productName {
    color: ${colors.darkGray};
    width: 100%;
    word-break: break-word;
  }

  ${StyledCountDownTimer} {
    margin-top: 2rem;
    margin-bottom: -2rem;
    margin-right: -2.25rem;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    display: flex;
    justify-content: space-between;

    .productName {
      margin-top: 0.5rem;
    }

    height: 3.75rem;
    margin-right: -2.25rem;
    margin-bottom: -2rem;

    ${StyledCountDownTimer} {
      margin: 0;
      min-width: 21.25rem;
      .timer {
        height: 3.75rem;
      }
    }
  }
`;
