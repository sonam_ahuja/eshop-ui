import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Storage from '@productDetailed/ProductView/Mobile/Variations/Storage';

describe('<Storage />', () => {
  test('should render properly', () => {
    // tslint:disable-next-line:no-any
    const props: any = {};

    const component = mount(
      <ThemeProvider theme={{}}>
        <Storage {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
