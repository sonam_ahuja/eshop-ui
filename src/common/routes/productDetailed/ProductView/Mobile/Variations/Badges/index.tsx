import React from 'react';
import { Badge } from 'dt-components';

import { StyledBadges } from './styles';

interface IProps {
  tags: string[];
}
const Badges = (props: IProps) => {
  const { tags } = props;

  return (
    <StyledBadges>
      <div className='badgesWrap'>
        {tags.map((tag, index) => (
          <Badge key={index} label={tag} />
        ))}
      </div>
    </StyledBadges>
  );
};

export default Badges;
