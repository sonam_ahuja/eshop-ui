import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledBadges = styled.div`
  padding-bottom: 1.75rem;

  .badgesWrap {
    margin: -0.125rem;
    > div {
      line-height: 1rem;
      padding: 0.25rem 0.75rem;
      margin: 0.125rem;
      user-select: none;
      cursor: default;
      background: ${colors.silverGray};
      color: ${colors.darkGray};
      font-weight: normal;
    }
  }
`;
