import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@common/store/states/translation';

import Color, { IProps } from '.';

describe('<Storage />', () => {
  const props: IProps = {
    showDisabledAttributes: true,
    disabledAttributesClickable: true,
    variants: [
      {
        value: '',
        label: '',
        enabled: false,
        name: 'dummy'
      }
    ],
    colorTranslatedKey: '',
    onAttributeChange: jest.fn(),
    productDetailedTranslation: translation().cart.productDetailed
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Color {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when variants are not there', () => {
    const newpProps: IProps = { ...props };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Color {...newpProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
