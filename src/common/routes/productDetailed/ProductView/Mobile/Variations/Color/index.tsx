import React from 'react';
import { Divider, Paragraph, Variant } from 'dt-components';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import { colors } from '@common/variables';
import { VariantsWrap } from '@common/components/Variant/styles';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';

import { StyledVariationsRow } from '../style';

import { StyledColor } from './styles';

export interface IProps {
  variants?: IVariantGroupsType[];
  selected?: IVariantGroupsType;
  colorTranslatedKey: string;
  productDetailedTranslation: IProductDetailedTranslation;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  onAttributeChange(atribute: IKeyValue): void;
}
const Color = (props: IProps) => {
  const {
    variants,
    selected,
    onAttributeChange,
    productDetailedTranslation,
    colorTranslatedKey,
    showDisabledAttributes,
    disabledAttributesClickable
  } = props;
  if (!variants) {
    return null;
  }
  const selectedColor = selected && selected.value;
  const selectedColorLabel = selected && selected.label;
  const variantItems = variants
    .filter(variant => {
      if (showDisabledAttributes) {
        return true;
      }

      return variant.enabled;
    })
    .map(variant => (
      <Variant
        outOfStock={!variant.enabled}
        name={variant.label}
        key={variant.value}
        onClick={() => {
          if (
            (!disabledAttributesClickable && !variant.enabled) ||
            (selected && variant.value === selected.value)
          ) {
            return;
          }
          onAttributeChange({
            value: variant.value,
            name: colorTranslatedKey
          });
        }}
        variantSize='large'
        active={variant.value === selectedColor}
        value={variant.value}
        selectedBorderColor={colors.magenta}
      />
    ));

  return (
    <StyledColor>
      <StyledVariationsRow className='styledColorRow'>
        <ul>
          <li>
            <Paragraph
              firstLetterTransform='uppercase'
              size='medium'
              className='label'
            >
              {productDetailedTranslation.color}: (
              <span className='selectedColor'>{selectedColorLabel}</span>)
              {/* <span className='super'>tm</span> */}
            </Paragraph>
          </li>
          <li>
            <VariantsWrap className='variantsWrap'>{variantItems}</VariantsWrap>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRow>

      {/* <StyledVariationsRowSingle className='styledColorRowSingle'>
        <ul>
          <li>
            <Paragraph className='label'>
              Color: <span className='selectedColor'>{selectedColor}</span>
              <span> (out of stock)</span>
            </Paragraph>
          </li>
          <li>
            <StyledHorizontalScrollRow>
              <StyledVariantsWrap className='variantsWrap horizontalScrollRowInner'>
                {variantItems}
              </StyledVariantsWrap>
            </StyledHorizontalScrollRow>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRowSingle> */}
    </StyledColor>
  );
};

export default Color;
