import styled from 'styled-components';

export const StyledColor = styled.div`
  .styledColorRow {
    ul {
      padding-bottom: 0.4rem;
    }
    ul li:first-child {
      .label .selectedColor {
        /* text-transform: uppercase; */
      }
      .label .super {
        font-size: 0.65em;
        /* text-transform: uppercase; */
        transform: translateY(-50%);
        display: inline-block;
        padding-left: 0.25rem;
      }
    }

    ul li:last-child {
      min-width: 40%;
      max-width: 40%;
      .variantsWrap {
        margin-top: -0.5rem;
        margin-right: -0.75rem;
      }
    }
  }

  .styledColorRowSingle {
    ul li:first-child {
      .label .selectedColor {
        /* text-transform: uppercase; */
      }
    }
  }
`;
