import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Description from '.';

describe('<Storage />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Description />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
