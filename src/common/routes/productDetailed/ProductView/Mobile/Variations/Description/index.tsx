import React, { FunctionComponent } from 'react';
import { Divider, Paragraph } from 'dt-components';

import { StyledVariationsRowSingle } from '../style';

import { StyledDescription } from './styles';

const Description: FunctionComponent<{}> = () => {
  return (
    <StyledDescription>
      <StyledVariationsRowSingle>
        <ul>
          <li>
            <Paragraph className='label'>Description</Paragraph>
          </li>
          <li>
            <Paragraph className='description' size='small'>
              Portable Bluetooth® handsfree kit that can be clipped to your
              car's sun visor, allows the driver to enjoy crystal clear phone
              calls.
            </Paragraph>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRowSingle>
    </StyledDescription>
  );
};

export default Description;
