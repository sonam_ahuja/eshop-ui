import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledDescription = styled.div`
  .description {
    color: ${colors.ironGray};
  }
`;
