import React, { FunctionComponent } from 'react';
import { Divider, List, ListItem, Paragraph } from 'dt-components';

import { StyledVariationsRowSingle } from '../style';

import { StyledFeature } from './styles';

const Feature: FunctionComponent<{}> = () => {
  return (
    <StyledFeature>
      <StyledVariationsRowSingle className='styledColorRowSingle'>
        <ul>
          <li>
            <Paragraph className='label'>Feature</Paragraph>
          </li>
          <li>
            <List>
              <ListItem description='Huawei Router is unlocked to all networks' />
              <ListItem description='Wi-Fi hotspot connecting up to 64 devices' />
              <ListItem description='4G Router supports a download speed of 300 Mbps' />
            </List>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRowSingle>
    </StyledFeature>
  );
};

export default Feature;
