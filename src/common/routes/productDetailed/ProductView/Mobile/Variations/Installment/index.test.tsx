import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { productDetailsSuccessResponse } from '@src/common/mock-api/productDetailed/details.mock';

import Component, { IProps } from '.';

describe('<VariationsFeature />', () => {
  const props: IProps = {
    productDetailedTranslation: appState().translation.cart.productDetailed,
    installmentGroup: productDetailsSuccessResponse.installmentGroup,
    showDisabledAttributes: false,
    disabledAttributesClickable: true,
    buyDeviceWithInstallments: true,
    onAttributeChange: jest.fn(),
    onInstallmentChange: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
