import React from 'react';
import { Paragraph, Section, Select } from 'dt-components';
import SimpleSelect from '@common/components/Select/SimpleSelect';
import {
  IInstallmentData,
  IKeyValue
} from '@src/common/routes/productDetailed/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledVariationsRow } from '../style';

import { StyledInstallment } from './styles';

export interface IProps {
  productDetailedTranslation: IProductDetailedTranslation;
  installmentGroup: IInstallmentData[];
  showDisabledAttributes: boolean;
  buyDeviceWithInstallments: boolean;
  disabledAttributesClickable: boolean;
  onAttributeChange(atribute: IKeyValue): void;
  onInstallmentChange(installmentValue: string | null): void;
}

const Installment = (props: IProps) => {
  const {
    installmentGroup,
    onInstallmentChange,
    buyDeviceWithInstallments,
    productDetailedTranslation
  } = props;

  const selectedInstallment = installmentGroup.find(item => {
    return item.active === true;
  });

  const selectedInstallmentId = selectedInstallment
    ? selectedInstallment.id
    : null;

  return (
    <StyledInstallment>
      <StyledVariationsRow>
        <ul>
          <li>
            <Paragraph
              firstLetterTransform='uppercase'
              size='medium'
              className='label'
            >
              {buyDeviceWithInstallments
                ? productDetailedTranslation.installmentLabel
                : productDetailedTranslation.withoutInstallmentLabel}{' '}
              {/* <Icon color='currentColor' name='ec-information' /> */}
            </Paragraph>
          </li>
          <li>
            <div>
              <Select
                onItemSelect={item => {
                  if (
                    (!props.disabledAttributesClickable && !item.enabled) ||
                    selectedInstallmentId === item.id
                  ) {
                    return;
                  }
                  sendDropdownClickEvent(item.title);
                  onInstallmentChange(String(item.id));
                }}
                // onItemSelect={item => {
                //   onInstallmentChange(String(item.id));
                // }}
                useNativeDropdown={true}
                className='select'
                selectedItem={selectedInstallment}
                SelectedItemComponent={selectedProps => {
                  return (
                    <>
                      <SimpleSelect {...selectedProps} />
                      <Section className='upfrontAmount' size='small'>
                        {selectedProps.selectedItem &&
                          selectedProps.selectedItem.upfrontAmount}
                      </Section>
                    </>
                  );
                }}
                items={installmentGroup}
              />
            </div>
          </li>
        </ul>
      </StyledVariationsRow>
    </StyledInstallment>
  );
};

export default Installment;
