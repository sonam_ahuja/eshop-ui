import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledInstallment = styled.div`
  ul {
    padding-bottom: 0;
    li:first-child .label {
      i {
        vertical-align: text-top;
      }
    }

    li:last-child .upfrontAmount {
      color: ${colors.mediumGray};
      padding-right: 24px;
      font-size: 0.625rem;
      line-height: 0.75rem;
      margin-top: 0.25rem;
    }
  }
  .styledSimpleSelect {
    color: ${colors.darkGray};
  }
`;
