import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@common/store/states/translation';
import TooltipContent, {
  IProps
} from '@productDetailed/ProductView/Mobile/Variations/Monthly/TooltipContent';
import { PRICE_TYPE, RECURRING_TYPE } from '@tariff/store/enum';

describe('<TooltipContent />', () => {
  test('should render properly', () => {
    const props: IProps = {
      variantName: 'variantName',
      tariffName: 'tariffName',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      price: {
        priceType: PRICE_TYPE.RECURRING,
        actualValue: 300,
        recurringChargePeriod: RECURRING_TYPE.MONTH,
        discounts: [],
        discountedValue: 200,
        recurringChargeOccurrence: 1,
        dutyFreeValue: 0,
        taxRate: 1,
        taxPercentage: 20
      },
      productDetailedTranslation: translation().cart.productDetailed,
      onClose: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <TooltipContent {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
