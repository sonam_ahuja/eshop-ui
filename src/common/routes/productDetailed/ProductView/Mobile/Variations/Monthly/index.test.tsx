import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@common/store/states/translation';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
const mockStore = configureStore();

import Monthly, { IProps } from '.';

describe('<Monthly />', () => {
  const props: IProps = {
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    variantName: 'variantName',
    tariffName: 'tariffName',
    recurringFee: {
      priceType: 'basePrice',
      actualValue: 1000,
      recurringChargePeriod: '12',
      discounts: [
        {
          discount: 23,
          name: 'name',
          dutyFreeValue: 34,
          label: '',
          taxRate: 45,
          taxPercentage: 3
        }
      ],
      discountedValue: 34
    },
    variantId: '',
    productDetailedTranslation: translation().cart.productDetailed
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Monthly {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const newProps = { ...props };
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount<Monthly>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Monthly {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('Monthly').instance() as Monthly).openTooltip();
    (component.find('Monthly').instance() as Monthly).closeTooltip();
    (component.find('Monthly').instance() as Monthly).componentDidUpdate(props);
  });
});
