import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledMonthly = styled.div`
  display: flex;
  justify-content: space-between;

  color: ${colors.magenta};

  .label {
    line-height: 0.75rem;
    padding-top: 0.5rem;
    i {
      vertical-align: text-top;
    }
  }
  /** this is a hack solution because of safari issue: ECDEV-3489 ***/
  .dt_title::before {
    content: '';
  }
`;
