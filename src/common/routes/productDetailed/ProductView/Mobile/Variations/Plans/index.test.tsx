import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import { tariffCategoryMock } from '@src/common/mock-api/category/category.mock';

import Plans from '.';
const mockStore = configureStore();

describe('<Size />', () => {
  // tslint:disable-next-line:no-any
  const props: any = {
    buyDeviceOnly: false,
    tariffCategory: tariffCategoryMock,
    productDetailedTranslation: {
      buyDeviceOnly: ''
    }
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Plans {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount<Plans>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Plans {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('Plans').instance() as Plans).onIsOpenChange(true);
    (component.find('Plans').instance() as Plans).onIsOpenChange(false);
  });
});
