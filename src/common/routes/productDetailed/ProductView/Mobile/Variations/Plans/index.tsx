import React, { Component, ReactNode } from 'react';
import { Select } from 'dt-components';
import PlanSelect from '@common/components/Select/PlanSelect';
import PlanSelectList from '@common/components/Select/PlanSelectList';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import {
  IKeyValue,
  ILoyaltyAgreement,
  ITariffDetail
} from '@src/common/routes/productDetailed/store/types';
import { sendDropdownClickEvent } from '@src/common/events/common';
import { ICategory } from '@src/common/routes/category/store/types';

import { StyledPlans } from './styles';

interface IProps {
  productDetailedTranslation: IProductDetailedTranslation;
  tariffGroup: ITariffDetail[];
  showTariffInfo?: boolean;
  tariffCategory: ICategory;
  onAttributeChange(atribute: IKeyValue): void;
  onClick?(event: React.MouseEvent<HTMLInputElement, MouseEvent>): void;
  onPlanChange(planId: string | null): void;
  onAgreementChange(agreementId: string | null): void;
  youngTariffCheck(planId: string): void;
  showAllPlans(): void;
}

interface IState {
  isOpen: boolean;
}
class Plans extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.onIsOpenChange = this.onIsOpenChange.bind(this);
  }

  onIsOpenChange(isOpen: boolean): void {
    this.setState({
      isOpen
    });
  }

  // tslint:disable-next-line: cognitive-complexity
  render(): ReactNode {
    const {
      onClick,
      productDetailedTranslation,
      tariffGroup,
      onAgreementChange,
      onPlanChange,
      tariffCategory,
      showAllPlans,
      showTariffInfo
    } = this.props;

    let planList = (tariffGroup || []).map(groupItem => {
      const { id, title, ...rest } = groupItem;

      return {
        id,
        title,
        rest
      };
    });

    if (!showTariffInfo) {
      planList = planList.filter(plan => plan.id === '-1');
    }
    const selectedPlanItem = planList.find(planListItem => {
      return planListItem.rest.active;
    });

    const deviceOnlyItem = planList.find(item => {
      return item.id === '-1';
    });

    return (
      <StyledPlans onClick={onClick}>
        <Select
          isOpen={this.state.isOpen}
          onOpenChange={this.onIsOpenChange}
          enableControlled={true}
          SelectedItemComponent={selectedItemProps => (
            <PlanSelect
              {...selectedItemProps}
              buyDeviceOnly={
                (deviceOnlyItem && deviceOnlyItem.rest.active) as boolean
              }
              deviceOnlyPrice={
                (deviceOnlyItem && deviceOnlyItem.rest.amount) as string
              }
              planList={planList}
              tariffCategory={tariffCategory}
              productDetailedTranslation={productDetailedTranslation}
              onAgreementChange={agreementId => {
                if (selectedItemProps.selectedItem) {
                  const selectedAgreement = selectedItemProps.selectedItem.rest.agreements.find(
                    (agreement: ILoyaltyAgreement) => agreement.active
                  );
                  if (
                    selectedAgreement &&
                    selectedAgreement.id !== undefined &&
                    selectedAgreement.id === agreementId
                  ) {
                    return;
                  }
                }
                onAgreementChange(agreementId);
              }}
            />
          )}
          ListComponent={listProps => (
            <PlanSelectList
              {...listProps}
              productDetailedTranslation={productDetailedTranslation}
              buyDeviceOnly={
                (deviceOnlyItem && deviceOnlyItem.rest.active) as boolean
              }
              deviceOnlyPrice={deviceOnlyItem && deviceOnlyItem.rest.amount}
              showTariffInfo={showTariffInfo}
              showAllPlans={() => {
                this.onIsOpenChange(false);
                showAllPlans();
              }}
            />
          )}
          selectedItem={selectedPlanItem}
          items={planList}
          onItemSelect={item => {
            if (selectedPlanItem && selectedPlanItem.id === item.id) {
              return;
            }
            sendDropdownClickEvent(item.title);
            onPlanChange(item.id.toString());
          }}
        />
      </StyledPlans>
    );
  }
}
export default Plans;
