import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import Size from '.';

describe('<Size />', () => {
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Size />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
