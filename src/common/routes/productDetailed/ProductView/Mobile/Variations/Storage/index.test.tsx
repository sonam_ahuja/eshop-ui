import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Storage, {
  IProps
} from '@productDetailed/ProductView/Mobile/Variations/Storage';
import translation from '@common/store/states/translation';

describe('<Storage />', () => {
  test('should render properly', () => {
    const props: IProps = {
      variants: [
        {
          value: '',
          label: '',
          enabled: false,
          name: 'dummy'
        }
      ],
      selected: {
        value: '',
        label: '',
        enabled: false,
        name: 'dummy'
      },
      storageTranslatedKey: 'sasdas',
      showDisabledAttributes: true,
      disabledAttributesClickable: true,
      productDetailedTranslation: translation().cart.productDetailed,
      onAttributeChange: jest.fn()
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Storage {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
