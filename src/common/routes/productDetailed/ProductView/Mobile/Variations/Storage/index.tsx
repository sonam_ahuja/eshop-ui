import React from 'react';
import { Divider, Paragraph, Select } from 'dt-components';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import { IKeyValue, IVariantGroupsType } from '@productDetailed/store/types';
import SimpleSelect from '@common/components/Select/SimpleSelect';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledVariationsRow } from '../style';

import { StyledStorage } from './styles';

export interface IProps {
  variants?: IVariantGroupsType[];
  selected?: IVariantGroupsType;
  storageTranslatedKey: string;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  productDetailedTranslation: IProductDetailedTranslation;
  onAttributeChange(atribute: IKeyValue): void;
}

const Storage = (props: IProps) => {
  const {
    variants,
    selected,
    onAttributeChange,
    productDetailedTranslation,
    storageTranslatedKey,
    showDisabledAttributes,
    disabledAttributesClickable
  } = props;
  if (!variants) {
    return null;
  }

  const outOfStockTranslation = productDetailedTranslation.disabledOutOfStock;

  const select = (
    <Select
      useNativeDropdown={true}
      enableControlled={true}
      SelectedItemComponent={SimpleSelect}
      disableClickOnDisabledItem={!disabledAttributesClickable}
      onItemSelect={(item: IListItem) => {
        if (
          (!disabledAttributesClickable && item.disabled) ||
          (selected && item.id === selected.value)
        ) {
          return;
        }
        sendDropdownClickEvent(item.title);
        onAttributeChange({
          value: item.id as string,
          name: storageTranslatedKey
        });
      }}
      selectedItem={
        selected
          ? {
              id: selected.value,
              title: selected.label
            }
          : null
      }
      items={variants
        .filter(variant => {
          if (showDisabledAttributes) {
            return true;
          }

          return variant.enabled;
        })
        .map(variant => {
          return {
            id: variant.value,
            title: variant.enabled
              ? variant.label
              : `${variant.label} (${outOfStockTranslation})`,
            disabled: !variant.enabled
          };
        })}
    />
  );

  return (
    <StyledStorage>
      <StyledVariationsRow>
        <ul>
          <li>
            <Paragraph firstLetterTransform='uppercase' size='medium'>
              {productDetailedTranslation.storage}
            </Paragraph>
          </li>
          <li>{select}</li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRow>

      {/* <StyledVariationsRowSingle>
        <ul>
          <li>
            <Paragraph className='label'>Storage</Paragraph>
          </li>
          <li>
            <StyledHorizontalScrollRow>
              <div className='selectorWrap horizontalScrollRowInner'>
                <Selector disabled type='main' label='test' />
                <Selector type='main' label='test' />
                <Selector active type='main' label='test' />
                <Selector type='main' label='test' />
                <Selector type='main' label='test' />
                <Selector type='main' label='test' />
                <Selector type='main' label='test' />
              </div>
            </StyledHorizontalScrollRow>
          </li>
        </ul>
        <Divider className='divider' />
      </StyledVariationsRowSingle> */}
    </StyledStorage>
  );
};

export default Storage;
