import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledStorage = styled.div`
  .selectorWrap {
    margin: 0 -0.125rem;

    > div {
      margin: 0 0.125rem;
    }
  }
  select {
    z-index: 0;
  }
  .styledSimpleSelect {
    color: ${colors.darkGray};
  }
`;
