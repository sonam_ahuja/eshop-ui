import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { PRICE_TYPE, RECURRING_TYPE } from '@tariff/store/enum';

import TooltipContent, { IProps } from './TooltipContent';

describe('<Upfront />', () => {
  const props: IProps = {
    variantName: 'variantName',
    tariffName: 'tariffName',
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    price: {
      priceType: PRICE_TYPE.RECURRING,
      actualValue: 300,
      recurringChargePeriod: RECURRING_TYPE.MONTH,
      discounts: [],
      discountedValue: 200,
      recurringChargeOccurrence: 1,
      dutyFreeValue: 0,
      taxRate: 1,
      taxPercentage: 20
    },
    productDetailedTranslation: appState().translation.cart.productDetailed,
    onClose: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <TooltipContent {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
