import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
const mockStore = configureStore();

import { StyledUpfront } from './styles';
import Upfront, { IProps } from '.';
describe('<Upfront />', () => {
  const props: IProps = {
    basePrice: undefined,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    upfrontPrice: undefined,
    noInstallment: false,
    buyDeviceOnly: true,
    variantId: '1',
    tariffName: 'string',
    variantName: 'string',
    productDetailedTranslation: appState().translation.cart.productDetailed
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Upfront {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('styled upfront should render properly', () => {
    const newProps: { buyDeviceOnly: boolean; isDiscounted: boolean } = {
      buyDeviceOnly: false,
      isDiscounted: false
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <StyledUpfront {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('styled upfront should render properly on buy device only true', () => {
    const newProps: { buyDeviceOnly: boolean; isDiscounted: boolean } = {
      buyDeviceOnly: true,
      isDiscounted: true
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <StyledUpfront {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount<Upfront>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Upfront {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('Upfront').instance() as Upfront).openTooltip();
    (component.find('Upfront').instance() as Upfront).closeTooltip();
    (component.find('Upfront').instance() as Upfront).componentDidUpdate(props);
  });
});
