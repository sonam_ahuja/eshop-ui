import React, { Component, ReactNode } from 'react';
import { Icon, Section, Title } from 'dt-components';
import { IPrices } from '@tariff/store/types';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { formatCurrency } from '@common/utils/currency';
import { AppTooltip } from '@src/common/components/Tooltip/styles';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';
import { sendOnDeviceDetailToolTipEvent } from '@events/DeviceDetail/index';

import TooltipContent from './TooltipContent';
import { StyledUpfront } from './styles';

export interface IProps {
  basePrice?: IPrices;
  upfrontPrice?: IPrices;
  currency: ICurrencyConfiguration;
  buyDeviceOnly: boolean;
  variantId: string;
  productDetailedTranslation: IProductDetailedTranslation;
  tariffName: string;
  variantName: string;
  noInstallment: boolean;
}

interface IState {
  tooltipOpen: boolean;
}
export default class Upfront extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tooltipOpen: false
    };
    this.openTooltip = this.openTooltip.bind(this);
    this.closeTooltip = this.closeTooltip.bind(this);
  }

  openTooltip(): void {
    this.setState({
      tooltipOpen: true
    });
  }

  closeTooltip(): void {
    this.setState({
      tooltipOpen: false
    });
  }

  componentDidUpdate(prevProps: IProps): void {
    if (prevProps.variantId !== this.props.variantId) {
      this.closeTooltip();
    }
  }

  render(): ReactNode {
    const {
      basePrice,
      upfrontPrice,
      productDetailedTranslation,
      noInstallment,
      currency,
      variantName,
      tariffName
    } = this.props;

    const price = noInstallment ? basePrice : upfrontPrice;

    if (!price) {
      return null;
    }
    const isDiscounted = price.actualValue !== price.discountedValue;
    const tooltip = (
      <AppTooltip
        autoPosition={false}
        tooltipStyles={{ zIndex: 20 }}
        trigger='click'
        destroyOnTriggerOut={true}
        position='topLeft'
        isOpen={this.state.tooltipOpen}
        onVisibilityChanges={tooltipOpen =>
          this.setState(
            {
              tooltipOpen
            },
            () => {
              if (this.state.tooltipOpen) {
                sendOnDeviceDetailToolTipEvent(
                  this.props.variantName,
                  price.discountedValue
                );
              }
            }
          )
        }
        targetOffset={['-18', '-6']}
        tooltipContent={
          <TooltipContent
            variantName={variantName}
            tariffName={tariffName}
            productDetailedTranslation={productDetailedTranslation}
            price={price}
            currency={currency}
            onClose={this.closeTooltip}
          />
        }
      >
        {' '}
        <span>
          <Icon
            color='currentColor'
            name='ec-information'
            onClick={this.openTooltip}
          />
        </span>
      </AppTooltip>
    );

    const amount =
      price.discountedValue !== undefined
        ? price.discountedValue
        : price.actualValue;

    return (
      <StyledUpfront isDiscounted={isDiscounted}>
        <Section className='label' size='large'>
          {isDiscounted
            ? productDetailedTranslation.totalUpfront
            : productDetailedTranslation.upfront}
          {isDiscounted ? tooltip : null}
        </Section>
        <Title className='value' size='xsmall' weight='bold'>
          {formatCurrency(amount, currency.locale)}{' '}
        </Title>
      </StyledUpfront>
    );
  }
}
