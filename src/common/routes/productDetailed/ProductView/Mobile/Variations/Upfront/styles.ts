import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledUpfront = styled.div<{ isDiscounted: boolean }>`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  ${({ isDiscounted }) => (isDiscounted ? `color: ${colors.magenta}` : '')};

  .label {
    line-height: normal;
    padding-bottom: 0.25rem;
    ${({ isDiscounted }) => (isDiscounted ? `` : `color: ${colors.ironGray}`)};
  }
  .value {
    ${({ isDiscounted }) =>
      isDiscounted ? `` : `color: ${colors.mediumGray}`};
  }
  .dt_icon svg path {
    ${({ isDiscounted }) => (isDiscounted ? '' : `fill: ${colors.ironGray}`)};
  }
  /** this is a hack solution because of safari issue: ECDEV-3489 ***/
  .dt_title::before {
    content: '';
  }
`;
