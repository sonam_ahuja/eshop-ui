import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Variations, {
  IProps
} from '@productDetailed/ProductView/Mobile/Variations';
import appState from '@store/states/app';
import { tariffDetail, totalPrices } from '@mocks/tariff/tariff.mock';
import { productDetailsSuccessResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { ITEM_STATUS } from '@src/common/store/enums';
import { tariffCategoryMock } from '@src/common/mock-api/category/category.mock';

describe('<Variations />', () => {
  test('should render properly', () => {
    const props: IProps = {
      addToBasketLoading: true,
      addToBasketRef: null,
      buyDeviceWithInstallments: true,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      installmentGroup: productDetailsSuccessResponse.installmentGroup,
      tariffGroup: tariffDetail,
      tariffCategory: tariffCategoryMock,
      totalPrices,
      showOnlineStockAvailability: true,
      variant: {
        id: '',
        quantity: 12,
        group: '',
        unavailabilityReasonCodes: [],
        name: '',
        description: '',
        characteristics: [
          {
            name: '',
            values: [{ value: 'value1' }]
          }
        ],
        prices: [
          {
            priceType: '',
            actualValue: 0,
            discountedValue: 0,
            recurringChargePeriod: '12',
            discounts: [
              {
                discount: 23,
                name: 'name',
                dutyFreeValue: 34,
                taxRate: 45,
                label: '',
                taxPercentage: 3
              }
            ]
          }
        ],
        tags: []
      },
      productDetailedTranslation: appState().translation.cart.productDetailed,
      variantGroups: {
        Colour: [
          {
            value: 'value2',
            label: '',
            enabled: false,
            name: 'dummy'
          }
        ]
      },
      buyDeviceOnly: true,
      showVariantSelection: true,
      showDevicePrice: true,
      showTariffInfo: true,
      showNotifyMe: true,
      colorTranslatedKey: 'key',
      storageTranslatedKey: 'shifter',
      showDisabledAttributes: true,
      disabledAttributesClickable: true,
      openNotificationModal: jest.fn(),
      onAttributeChange: jest.fn(),
      setAddToBasketBtnRef: jest.fn(),
      addToBasket: jest.fn(),
      showbadges: true,
      newProductBeforeLaunchDays: 34,
      onInstallmentChange: jest.fn(),
      onPlanChange: jest.fn(),
      youngTariffCheck: jest.fn(),
      onAgreementChange: jest.fn(),
      showAllPlans: jest.fn(),
      considerInventory: false,
      deviceStock: ITEM_STATUS.IN_STOCK
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Variations {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
