import { IPrices } from '@tariff/store/types';
import Header from '@productDetailed/ProductView/Mobile/Variations/Badges';
import Storage from '@productDetailed/ProductView/Mobile/Variations/Storage';
import Color from '@productDetailed/ProductView/Mobile/Variations/Color';
import { Button } from 'dt-components';
import React, { Component, ReactNode } from 'react';
import {
  eligibilityUnavailabilityReasonType,
  IInstallmentData,
  IKeyValue,
  ITariffDetail,
  IVariant,
  IVariantGroups
} from '@productDetailed/store/types';
import Plans from '@productDetailed/ProductView/Mobile/Variations/Plans';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import {
  getSelectedCharacterstic,
  getTagCharacterstics,
  getTags,
  getVariantLaunchDate,
  getVariantPrices
} from '@productDetailed/utils';
import { ITEM_STATUS } from '@common/store/enums';
import { getOutOfStock } from '@productList/utils/getProductDetail';
import EVENT_NAME from '@events/constants/eventName';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';
import { ICategory } from '@src/common/routes/category/store/types';

import { PRICE_TYPE } from '../../../store/enum';

import { StyledMonthlyUpfrontWrap, StyledVariations } from './style';
import Upfront from './Upfront';
import Monthly from './Monthly';
import Installment from './Installment';

export interface IProps {
  installmentGroup: IInstallmentData[];
  tariffGroup: ITariffDetail[];
  totalPrices: IPrices[];
  tariffCategory: ICategory;
  variant: IVariant;
  buyDeviceOnly: boolean;
  variantGroups: IVariantGroups;
  showVariantSelection: boolean;
  showDevicePrice: boolean;
  addToBasketRef: HTMLElement | null;
  showTariffInfo: boolean;
  buyDeviceWithInstallments: boolean;
  showbadges: boolean;
  showNotifyMe: boolean;
  colorTranslatedKey: string;
  currency: ICurrencyConfiguration;
  storageTranslatedKey: string;
  productDetailedTranslation: IProductDetailedTranslation;
  showDisabledAttributes: boolean;
  disabledAttributesClickable: boolean;
  newProductBeforeLaunchDays: number;
  showOnlineStockAvailability: boolean;
  addToBasketLoading: boolean;
  considerInventory: boolean;
  deviceStock: eligibilityUnavailabilityReasonType;
  addToBasket(): void;
  openNotificationModal(): void;
  onAttributeChange(atribute: IKeyValue): void;
  setAddToBasketBtnRef(ref: HTMLElement | null): void;
  onInstallmentChange(installmentValue: string | null): void;
  onPlanChange(planId: string | null): void;
  youngTariffCheck(planId: string): void;
  onAgreementChange(agreementId: string | null): void;
  showAllPlans(): void;
}

export default class Variations extends Component<IProps> {
  basketBtnRef: HTMLElement | null = null;
  btnRef: HTMLElement | null = null;

  constructor(props: IProps) {
    super(props);

    this.onScroll = this.onScroll.bind(this);
    this.setBasketBtnRef = this.setBasketBtnRef.bind(this);
    this.setBtnRef = this.setBtnRef.bind(this);
    this.getElementInView = this.getElementInView.bind(this);
  }

  componentDidMount(): void {
    window.addEventListener('scroll', this.onScroll);
    if (
      this.basketBtnRef &&
      this.btnRef &&
      !this.getElementInView(this.basketBtnRef)
    ) {
      const addToBasketBtnClass = 'add-basket-btn-sticky';
      this.btnRef.classList.add(addToBasketBtnClass);
    }
  }

  // tslint:disable-next-line:cognitive-complexity
  onScroll(): void {
    const addToBasketBtnClass = 'add-basket-btn-sticky';
    if (
      this.basketBtnRef &&
      this.btnRef &&
      this.props.addToBasketRef &&
      this.basketBtnRef.getBoundingClientRect
    ) {
      const bounding = this.basketBtnRef.getBoundingClientRect();
      if (
        -bounding.top > bounding.height &&
        this.props.addToBasketRef &&
        this.props.addToBasketRef.classList.contains('sticky')
      ) {
        this.btnRef.classList.remove(addToBasketBtnClass);
      } else {
        if (this.getElementInView(this.basketBtnRef)) {
          this.btnRef.classList.remove(addToBasketBtnClass);
        } else {
          this.btnRef.classList.add(addToBasketBtnClass);
        }
      }
    }
  }

  getElementInView(elment: HTMLElement): boolean {
    const scroll = window.scrollY || window.pageYOffset;
    const boundsTop = elment.getBoundingClientRect().top + scroll;

    const viewport = {
      top: scroll,
      bottom: scroll + window.innerHeight
    };

    const bounds = {
      top: boundsTop,
      bottom: boundsTop + elment.clientHeight
    };

    return (
      (bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom) ||
      (bounds.top +
        (this.btnRef && this.btnRef.offsetHeight
          ? this.btnRef.offsetHeight
          : 48) <=
        viewport.bottom &&
        bounds.top >= viewport.top)
    );
  }

  componentWillUnmount(): void {
    window.removeEventListener('scroll', this.onScroll);
  }

  setBasketBtnRef(ref: HTMLElement | null): void {
    if (ref) {
      this.basketBtnRef = ref;
      this.props.setAddToBasketBtnRef(ref);
    }
  }

  setBtnRef(ref: HTMLElement | null): void {
    if (ref) {
      this.btnRef = ref;
    }
  }

  // tslint:disable-next-line:cognitive-complexity
  render(): ReactNode {
    const {
      variant,
      variantGroups,
      openNotificationModal,
      onAttributeChange,
      showVariantSelection,
      showDevicePrice,
      showTariffInfo,
      showbadges,
      showNotifyMe,
      productDetailedTranslation,
      colorTranslatedKey,
      addToBasket,
      tariffCategory,
      storageTranslatedKey,
      showDisabledAttributes,
      disabledAttributesClickable,
      newProductBeforeLaunchDays,
      showOnlineStockAvailability,
      installmentGroup,
      tariffGroup,
      totalPrices,
      onInstallmentChange,
      buyDeviceWithInstallments,
      onPlanChange,
      currency,
      onAgreementChange,
      youngTariffCheck,
      showAllPlans,
      addToBasketLoading,
      considerInventory,
      deviceStock
    } = this.props;
    const tagsCharacterstic = getTagCharacterstics(variant.characteristics);
    const { basePrice, upfrontPrice, recurringFee } = getVariantPrices(
      totalPrices
    );
    const launchDate = getVariantLaunchDate(variant.characteristics);
    const tags = getTags(
      tagsCharacterstic && tagsCharacterstic.values[0].label,
      launchDate,
      newProductBeforeLaunchDays,
      productDetailedTranslation,
      showOnlineStockAvailability,
      deviceStock
    );

    const addBtnText = (
      (variant && variant.unavailabilityReasonCodes) ||
      []
    ).includes(ITEM_STATUS.PRE_ORDER)
      ? productDetailedTranslation.preOrder
      : productDetailedTranslation.addToBasket;

    const isOutOfStock =
      variant.unavailabilityReasonCodes &&
      variant.unavailabilityReasonCodes.length &&
      getOutOfStock(variant.unavailabilityReasonCodes, considerInventory);

    const addToCartBtn = isOutOfStock ? (
      showNotifyMe ? (
        <Button
          onClickHandler={openNotificationModal}
          className='button add-to-basket-btn'
          data-event-id={EVENT_NAME.DEVICE_DETAIL.EVENTS.NOTIFY_ME}
          data-event-message={productDetailedTranslation.notifyMe}
          loading={addToBasketLoading}
        >
          {productDetailedTranslation.notifyMe}
        </Button>
      ) : null
    ) : (
      <Button
        className='button add-to-basket-btn'
        data-event-id={EVENT_NAME.DEVICE_DETAIL.EVENTS.ADD_TO_BASKET}
        data-event-message={addBtnText}
        onClickHandler={addToBasket}
        loading={addToBasketLoading}
      >
        {addBtnText}
      </Button>
    );

    const selectedColor = getSelectedCharacterstic(
      variant.characteristics,
      variantGroups[colorTranslatedKey],
      colorTranslatedKey
    );
    const selectedStorage = getSelectedCharacterstic(
      variant.characteristics,
      variantGroups[storageTranslatedKey],
      storageTranslatedKey
    );

    const buyOnlyDeviceItem = tariffGroup.find(item => {
      return item.id === '-1';
    });

    const selectedTariff = tariffGroup.find(item => {
      return item.active === true && item.id !== '-1';
    });

    const noInstallmentItem = installmentGroup.find(item => {
      return item.amountType === PRICE_TYPE.BASE_PRICE;
    });

    const monthlyHide =
      buyOnlyDeviceItem &&
      buyOnlyDeviceItem.active &&
      noInstallmentItem &&
      noInstallmentItem.active;

    return (
      <StyledVariations>
        {showbadges ? <Header tags={tags} /> : null}
        {showVariantSelection ? (
          <Color
            productDetailedTranslation={productDetailedTranslation}
            variants={variantGroups[colorTranslatedKey]}
            selected={selectedColor}
            onAttributeChange={onAttributeChange}
            colorTranslatedKey={colorTranslatedKey}
            showDisabledAttributes={showDisabledAttributes}
            disabledAttributesClickable={disabledAttributesClickable}
          />
        ) : null}
        {showVariantSelection ? (
          <Storage
            productDetailedTranslation={productDetailedTranslation}
            variants={variantGroups[storageTranslatedKey]}
            selected={selectedStorage}
            onAttributeChange={onAttributeChange}
            storageTranslatedKey={storageTranslatedKey}
            showDisabledAttributes={showDisabledAttributes}
            disabledAttributesClickable={disabledAttributesClickable}
          />
        ) : null}
        <Installment
          productDetailedTranslation={productDetailedTranslation}
          installmentGroup={installmentGroup}
          onAttributeChange={onAttributeChange}
          buyDeviceWithInstallments={buyDeviceWithInstallments}
          showDisabledAttributes={this.props.showDisabledAttributes}
          disabledAttributesClickable={disabledAttributesClickable}
          onInstallmentChange={onInstallmentChange}
        />

        <Plans
          productDetailedTranslation={productDetailedTranslation}
          tariffGroup={tariffGroup}
          onAttributeChange={onAttributeChange}
          onPlanChange={onPlanChange}
          onAgreementChange={onAgreementChange}
          youngTariffCheck={youngTariffCheck}
          tariffCategory={tariffCategory}
          showAllPlans={showAllPlans}
          showTariffInfo={showTariffInfo}
        />

        {showDevicePrice ? (
          <StyledMonthlyUpfrontWrap>
            {!monthlyHide ? (
              <Monthly
                variantId={variant.id}
                productDetailedTranslation={productDetailedTranslation}
                recurringFee={recurringFee}
                variantName={variant.name}
                currency={currency}
                tariffName={(selectedTariff && selectedTariff.label) as string}
              />
            ) : null}
            <Upfront
              variantId={variant.id}
              noInstallment={
                (noInstallmentItem && noInstallmentItem.active) as boolean
              }
              buyDeviceOnly={
                (buyOnlyDeviceItem && buyOnlyDeviceItem.active) as boolean
              }
              productDetailedTranslation={productDetailedTranslation}
              basePrice={basePrice}
              currency={currency}
              variantName={variant.name}
              tariffName={(selectedTariff && selectedTariff.label) as string}
              upfrontPrice={upfrontPrice}
            />
          </StyledMonthlyUpfrontWrap>
        ) : null}
        {addToCartBtn ? (
          <div ref={this.setBasketBtnRef} className='buttonWrap'>
            <div ref={this.setBtnRef}>{addToCartBtn}</div>
          </div>
        ) : null}
      </StyledVariations>
    );
  }
  // tslint:disable-next-line:max-file-line-count
}
