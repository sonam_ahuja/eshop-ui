import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledHorizontalScrollRow } from '@src/common/components/styles';

import { StyledUpfront } from './Upfront/styles';
import { StyledMonthly } from './Monthly/styles';

export const StyledVariationsRow = styled.div`
  > ul {
    padding: 1.1875rem 0 1.0625rem;
    display: flex;
    justify-content: space-between;

    > li:first-child {
      color: ${colors.ironGray};
    }

    > li:last-child {
      text-align: right;
      padding-right: 0.75rem;
      padding-top: 0.05rem;
    }
  }

  > .divider {
    color: ${colors.silverGray};
    margin: 0;
    min-height: 1px;
  }
`;

export const StyledVariationsRowSingle = styled.div`
  > ul {
    padding: 1.25rem 0 3.25rem;

    > li:first-child {
      color: ${colors.mediumGray};
      margin-bottom: 0.5rem;
    }
  }

  > .divider {
    color: ${colors.silverGray};
    margin: 0;
    min-height: 1px;
  }
`;

export const StyledVariations = styled.div`
  background: ${colors.coldGray};
  padding: 1.25rem 1.25rem 0rem 1.25rem;

  ${StyledHorizontalScrollRow} {
    margin: 0 -1.25rem;
    .horizontalScrollRowInner {
      padding: 0 1.25rem;
    }
  }

  .buttonWrap {
    margin: 1.25rem -1.25rem 0 -1.25rem;
    height: 48px;
    .button {
      width: 100%;
      height: 48px;
    }
  }

  .add-basket-btn-sticky {
    position: fixed;
    bottom: 0;
    width: 100%;
    z-index: 10;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding-left: 2.25rem;
    padding-right: 2.25rem;

    .buttonWrap {
      margin-left: -2.25rem;
      margin-right: -2.25rem;
    }
  }
`;

export const StyledMonthlyUpfrontWrap = styled.div`
  margin-top: 1.5rem;

  ${StyledMonthly} + ${StyledUpfront} {
    color: ${colors.ironGray};
    margin-top: 0.2rem;
    .value{
      font-size: 0.875rem;
      font-weight: 500;
    }
  }
`;
