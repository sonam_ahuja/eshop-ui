import translation from '@store/states/translation';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import { BrowserRouter as Router } from 'react-router-dom';
import configuration from '@store/states/configuration';
import { mount } from 'enzyme';
import { ITEM_STATUS, PAGE_THEME } from '@src/common/store/enums';
import { IWindow } from '@src/client/index';
import { tariff, tariffDetail, totalPrices } from '@mocks/tariff/tariff.mock';
import { productDetailsSuccessResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { tariffCategoryMock } from '@src/common/mock-api/category/category.mock';

import Component, { IProps } from '.';

const config = configuration().cms_configuration.modules.productDetailed;
// tslint:disable-next-line: no-big-function
describe('<Mobile />', () => {
  (window as IWindow).binkies = {
    onPageStyleSheetUrl: '',
    inModalStyleSheetUrl: '',
    contentIdentifier: '12',
    // on3DNotSupported: jest.fn(),
    onLoadingProgress: jest.fn(),
    show: jest.fn(),
    hide: jest.fn()
  };

  test('should render properly on variant length', () => {
    const newBinkies: IWindow['binkies'] = {
      onPageStyleSheetUrl: '/url',
      inModalStyleSheetUrl: '/url',
      contentIdentifier: '/url',
      // on3DNotSupported: jest.fn(),
      onLoadingProgress: jest.fn(),
      show: jest.fn(),
      hide: jest.fn()
    };
    (window as IWindow).binkies = newBinkies;
    const props: IProps = {
      addToBasketLoading: true,
      addToBasketRef: null,
      categoryId: '',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      tariffCategory: tariffCategoryMock,
      changeBinkiesFallbackVisible: jest.fn(),
      categoryName: 'category',
      deviceStock: ITEM_STATUS.IN_STOCK,
      catalogConfiguration: configuration().cms_configuration.modules.catalog,
      showImageAppShell: true,
      binkiesFallbackVisible: false,
      setImageAppShell: jest.fn(),
      setVariantNameRef: jest.fn(),
      parentCategorySlug: '',
      globalTranslation: translation().cart.global,
      lastListURL: '1',
      totalPrices,
      tariffDropdownData: tariffDetail,
      installmentDropdownData: productDetailsSuccessResponse.installmentGroup,
      productDetailsData: {
        productName: '',
        categoryName: '',
        linkedCategory: '',
        groups: {
          Colour: [
            {
              value: '',
              label: '',
              enabled: false,
              name: 'dummy'
            }
          ]
        },
        tariffs: tariff,
        totalPrices,
        variants: [
          {
            id: '',
            name: '',
            description: '',
            group: '',
            quantity: 1,
            characteristics: [
              {
                name: '',
                values: [{ value: 'value1' }]
              }
            ],
            attachments: {
              thumbnail: [{ type: '', url: '', name: '' }]
            },
            prices: [
              {
                priceType: 'string', // TODO: Different from contract is type
                actualValue: 1,
                discounts: [
                  {
                    discount: 23,
                    name: 'name',
                    dutyFreeValue: 34,
                    taxRate: 45,
                    label: '',
                    taxPercentage: 3
                  }
                ],
                discountedValue: 1,
                recurringChargePeriod: 'string'
              }
            ],
            tags: ['', 'asd'],
            unavailabilityReasonCodes: [ITEM_STATUS.IN_STOCK]
          }
        ]
      },
      productDetailedConfiguration: config,
      refereshDetailedPage: jest.fn(),
      addToBasket: jest.fn(),
      openNotificationModal: jest.fn(),
      onAttributeChange: jest.fn(),
      productDetailedTranslation: translation().cart.productDetailed,
      setAddToBasketBtnRef: jest.fn(),
      onInstallmentChange: jest.fn(),
      onPlanChange: jest.fn(),
      youngTariffCheck: jest.fn(),
      onAgreementChange: jest.fn(),
      setDeviceList: jest.fn(),
      setLandingPageDeviceList: jest.fn(),
      showAllPlans: jest.fn(),
      buyDeviceOnly: false
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Component {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly on variant length zero', () => {
    const props: IProps = {
      categoryName: 'category',
      changeBinkiesFallbackVisible: jest.fn(),
      addToBasketRef: null,
      setImageAppShell: jest.fn(),
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      tariffCategory: tariffCategoryMock,
      setVariantNameRef: jest.fn(),
      totalPrices,
      tariffDropdownData: tariffDetail,
      installmentDropdownData: productDetailsSuccessResponse.installmentGroup,
      binkiesFallbackVisible: false,
      deviceStock: ITEM_STATUS.IN_STOCK,
      catalogConfiguration: configuration().cms_configuration.modules.catalog,
      showImageAppShell: true,
      parentCategorySlug: '',
      categoryId: '',
      lastListURL: '1',
      globalTranslation: translation().cart.global,
      productDetailsData: {
        productName: '',
        linkedCategory: '',
        categoryName: '',
        variants: [],
        groups: {
          Colour: [
            {
              value: 'value',
              label: 'label',
              enabled: true,
              name: 'dummy'
            }
          ]
        },
        tariffs: tariff,
        totalPrices
      },
      productDetailedConfiguration: {
        theme: PAGE_THEME.SECONDARY,
        numberOfInstalments: '12',
        storage: 'asd',
        color: 'yellow',
        showBasketDrawer: true,
        showDisabledAttributes: true,
        disabledAttributesClickable: true,
        newProductBeforeLaunchDays: 23,
        buyDeviceWithInstallments: true,
        productDetailedTemplate1: {
          showOnlineStockAvailability: false,
          showVariantSelection: false,
          showDevicePrice: false,
          showTariffInfo: false,
          showHighlightPromo: false,
          showbadges: false,
          showNotifyMe: false,
          showOverViewSection: false,
          showTechnicalSpecification: false
        }
      },
      refereshDetailedPage: jest.fn(),
      addToBasket: jest.fn(),
      openNotificationModal: jest.fn(),
      onAttributeChange: jest.fn(),
      setAddToBasketBtnRef: jest.fn(),
      productDetailedTranslation: translation().cart.productDetailed,
      buyDeviceOnly: false,
      onInstallmentChange: jest.fn(),
      onPlanChange: jest.fn(),
      youngTariffCheck: jest.fn(),
      onAgreementChange: jest.fn(),
      showAllPlans: jest.fn(),
      setDeviceList: jest.fn(),
      setLandingPageDeviceList: jest.fn(),
      addToBasketLoading: true
    };

    const component = mount(
      <Router>
        <ThemeProvider theme={{}}>
          <Component {...props} />
        </ThemeProvider>
      </Router>
    );
    expect(component).toMatchSnapshot();
  });
});
