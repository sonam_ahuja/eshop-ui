import React from 'react';
import Carousel from '@productDetailed/ProductView/Mobile/Carousel';
import Variations from '@productDetailed/ProductView/Mobile/Variations';
import {
  eligibilityUnavailabilityReasonType,
  IInstallmentData,
  IKeyValue,
  IProductDetailedData,
  ITariffDetail
} from '@productDetailed/store/types';
import { IPrices } from '@tariff/store/types';
import {
  IGlobalTranslation,
  IProductDetailedTranslation
} from '@src/common/store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductDetailedConfiguration
} from '@src/common/store/types/configuration';
import { ICategory } from '@category/store/types';

import { StyledProductViewMobile } from './styles';

export interface IProps {
  productDetailsData: IProductDetailedData;
  productDetailedConfiguration: IProductDetailedConfiguration;
  catalogConfiguration: ICatalogConfiguration;
  productDetailedTranslation: IProductDetailedTranslation;
  buyDeviceOnly: boolean;
  tariffCategory: ICategory;
  installmentDropdownData: IInstallmentData[];
  tariffDropdownData: ITariffDetail[];
  totalPrices: IPrices[];
  globalTranslation: IGlobalTranslation;
  categoryId: string | null;
  currency: ICurrencyConfiguration;
  showImageAppShell: boolean;
  binkiesFallbackVisible: boolean;
  categoryName: string;
  parentCategorySlug: string;
  addToBasketRef: HTMLElement | null;
  lastListURL: string | null;
  deviceStock: eligibilityUnavailabilityReasonType;
  addToBasketLoading: boolean;
  addToBasket(): void;
  setVariantNameRef(ref: HTMLElement | null): void;
  openNotificationModal(): void;
  refereshDetailedPage(): void;
  changeBinkiesFallbackVisible(visible: boolean): void;
  setImageAppShell(showAppShell: boolean): void;
  onAttributeChange(atribute: IKeyValue): void;
  setAddToBasketBtnRef(ref: HTMLElement | null): void;
  onInstallmentChange(installmentValue: string | null): void;
  onPlanChange(planId: string | null): void;
  youngTariffCheck(planId: string): void;
  onAgreementChange(agreementId: string | null): void;
  showAllPlans(): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}

const ProductViewMobile = (props: IProps) => {
  const {
    productDetailsData: { variants, groups, productName },
    addToBasket,
    openNotificationModal,
    onAttributeChange,
    productDetailedConfiguration: { productDetailedTemplate1 },
    catalogConfiguration: {
      preorder: { considerInventory }
    },
    productDetailedTranslation,
    buyDeviceOnly,
    globalTranslation,
    showImageAppShell,
    categoryId,
    addToBasketRef,
    parentCategorySlug,
    categoryName,
    lastListURL,
    setImageAppShell,
    refereshDetailedPage,
    setVariantNameRef,
    deviceStock,
    binkiesFallbackVisible,
    changeBinkiesFallbackVisible,
    installmentDropdownData,
    tariffDropdownData,
    totalPrices,
    onInstallmentChange,
    onPlanChange,
    onAgreementChange,
    tariffCategory,
    youngTariffCheck,
    currency,
    showAllPlans,
    setDeviceList,
    setLandingPageDeviceList,
    addToBasketLoading
  } = props;
  if (variants.length === 0) {
    return null;
  }

  return (
    <StyledProductViewMobile>
      <Carousel
        productDetailedTranslation={productDetailedTranslation}
        variant={variants[0]}
        productName={productName}
        globalTranslation={globalTranslation}
        categoryId={categoryId}
        setImageAppShell={setImageAppShell}
        showImageAppShell={showImageAppShell}
        parentCategorySlug={parentCategorySlug}
        categoryName={categoryName}
        lastListURL={lastListURL}
        deviceStock={deviceStock}
        changeBinkiesFallbackVisible={changeBinkiesFallbackVisible}
        setVariantNameRef={setVariantNameRef}
        refereshDetailedPage={refereshDetailedPage}
        binkiesFallbackVisible={binkiesFallbackVisible}
        setDeviceList={setDeviceList}
        setLandingPageDeviceList={setLandingPageDeviceList}
      />
      <Variations
        installmentGroup={installmentDropdownData}
        tariffGroup={tariffDropdownData}
        totalPrices={totalPrices}
        addToBasket={addToBasket}
        openNotificationModal={openNotificationModal}
        variant={variants[0]}
        tariffCategory={tariffCategory}
        variantGroups={groups}
        currency={currency}
        addToBasketRef={addToBasketRef}
        onAttributeChange={onAttributeChange}
        showVariantSelection={productDetailedTemplate1.showVariantSelection}
        showDevicePrice={productDetailedTemplate1.showDevicePrice}
        showTariffInfo={productDetailedTemplate1.showTariffInfo}
        showbadges={productDetailedTemplate1.showbadges}
        showNotifyMe={productDetailedTemplate1.showNotifyMe}
        productDetailedTranslation={productDetailedTranslation}
        buyDeviceOnly={buyDeviceOnly}
        colorTranslatedKey={props.productDetailedConfiguration.color}
        storageTranslatedKey={props.productDetailedConfiguration.storage}
        setAddToBasketBtnRef={props.setAddToBasketBtnRef}
        buyDeviceWithInstallments={
          props.productDetailedConfiguration.buyDeviceWithInstallments
        }
        showDisabledAttributes={
          props.productDetailedConfiguration.showDisabledAttributes
        }
        disabledAttributesClickable={
          props.productDetailedConfiguration.disabledAttributesClickable
        }
        newProductBeforeLaunchDays={
          props.productDetailedConfiguration.newProductBeforeLaunchDays
        }
        showOnlineStockAvailability={
          productDetailedTemplate1.showOnlineStockAvailability
        }
        onInstallmentChange={onInstallmentChange}
        onPlanChange={onPlanChange}
        onAgreementChange={onAgreementChange}
        youngTariffCheck={youngTariffCheck}
        showAllPlans={showAllPlans}
        addToBasketLoading={addToBasketLoading}
        considerInventory={considerInventory}
        deviceStock={deviceStock}
      />
    </StyledProductViewMobile>
  );
};

export default ProductViewMobile;
