import styled from 'styled-components';
import { StyledBreadcrumbWrap } from '@src/common/components/CatalogBreadCrumb/styles';

export const StyledProductViewMobile = styled.div`
  /* .folder-wrap {
    margin-top: 1.875rem;
  } */
  ${StyledBreadcrumbWrap}{
    padding-top:1rem;
  }
`;
