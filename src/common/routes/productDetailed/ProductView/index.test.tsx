import translation from '@common/store/states/translation';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import { BrowserRouter as Router } from 'react-router-dom';
import configuration from '@common/store/states/configuration';
import { mount } from 'enzyme';
import { ITEM_STATUS } from '@common/store/enums';
import { IWindow } from '@src/client/index';
import { productDetailsSuccessResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { tariffDetail, totalPrices } from '@mocks/tariff/tariff.mock';
import { tariffCategoryMock } from '@src/common/mock-api/category/category.mock';

import ProductView, { IProps } from '.';

describe('<Variations />', () => {
  const newBinkies: IWindow['binkies'] = {
    onPageStyleSheetUrl: '/url',
    inModalStyleSheetUrl: '/url',
    contentIdentifier: '/url',
    // on3DNotSupported: jest.fn(),
    onLoadingProgress: jest.fn(),
    show: jest.fn(),
    hide: jest.fn()
  };
  (window as IWindow).binkies = newBinkies;
  test('should render properly', () => {
    const props: IProps = {
      addToBasketLoading: true,
      categoryName: 'category',
      showImageAppShell: true,
      addToBasketRef: null,
      tariffCategory: tariffCategoryMock,
      tariffDropdownData: tariffDetail,
      totalPrices,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      installmentDropdownData: productDetailsSuccessResponse.installmentGroup,
      binkiesFallbackVisible: false,
      setVariantNameRef: jest.fn(),
      deviceStock: ITEM_STATUS.IN_STOCK,
      setImageAppShell: jest.fn(),
      parentCategorySlug: '',
      productDetailsData: productDetailsSuccessResponse.productDetailsData,
      productDetailedConfiguration: configuration().cms_configuration.modules
        .productDetailed,
      productDetailedTranslation: translation().cart.productDetailed,
      globalTranslation: translation().cart.global,
      buyDeviceOnly: true,
      categoryId: '',
      lastListURL: '',
      addToBasket: jest.fn(),
      refereshDetailedPage: jest.fn(),
      changeBinkiesFallbackVisible: jest.fn(),
      openNotificationModal: jest.fn(),
      onAttributeChange: jest.fn(),
      setAddToBasketBtnRef: jest.fn(),
      onInstallmentChange: jest.fn(),
      onPlanChange: jest.fn(),
      youngTariffCheck: jest.fn(),
      onAgreementChange: jest.fn(),
      showAllPlans: jest.fn(),
      setDeviceList: jest.fn(),
      setLandingPageDeviceList: jest.fn(),
      catalogConfiguration: configuration().cms_configuration.modules.catalog
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <ProductView {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
