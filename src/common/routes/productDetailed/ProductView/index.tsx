import React from 'react';
import {
  eligibilityUnavailabilityReasonType,
  IInstallmentData,
  IKeyValue,
  IProductDetailedData,
  ITariffDetail
} from '@productDetailed/store/types';
import { IPrices } from '@tariff/store/types';
import {
  IGlobalTranslation,
  IProductDetailedTranslation
} from '@src/common/store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductDetailedConfiguration
} from '@src/common/store/types/configuration';
import { isMobile } from '@src/common/utils';
import { ICategory } from '@category/store/types';

import ProductViewDesktop from './Desktop';
import ProductViewMobile from './Mobile';
import { StyledProductView } from './styles';

export interface IProps {
  productDetailsData: IProductDetailedData;
  currency: ICurrencyConfiguration;
  globalTranslation: IGlobalTranslation;
  tariffCategory: ICategory;
  productDetailedConfiguration: IProductDetailedConfiguration;
  catalogConfiguration: ICatalogConfiguration;
  productDetailedTranslation: IProductDetailedTranslation;
  installmentDropdownData: IInstallmentData[];
  tariffDropdownData: ITariffDetail[];
  totalPrices: IPrices[];
  buyDeviceOnly: boolean;
  categoryId: string | null;
  showImageAppShell: boolean;
  binkiesFallbackVisible: boolean;
  categoryName: string;
  parentCategorySlug: string;
  lastListURL: string | null;
  deviceStock: eligibilityUnavailabilityReasonType;
  addToBasketLoading: boolean;
  addToBasketRef: HTMLElement | null;
  addToBasket(): void;
  openNotificationModal(): void;
  setVariantNameRef(ref: HTMLElement | null): void;
  setImageAppShell(showAppShell: boolean): void;
  refereshDetailedPage(): void;
  changeBinkiesFallbackVisible(visible: boolean): void;
  onAttributeChange(atribute: IKeyValue): void;
  setAddToBasketBtnRef(ref: HTMLElement | null): void;
  onInstallmentChange(installmentValue: string | null): void;
  onPlanChange(planId: string | null): void;
  onAgreementChange(agreementId: string | null): void;
  youngTariffCheck(planId: string): void;
  showAllPlans(): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}
const ProductView = (props: IProps) => {
  return (
    <StyledProductView>
      {isMobile.phone || isMobile.tablet ? (
        <ProductViewMobile {...props} />
      ) : (
        <ProductViewDesktop {...props} />
      )}
    </StyledProductView>
  );
};

export default ProductView;
