import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { PAGE_THEME } from '@src/common/store/enums';

import Component from '.';

describe('<Technical Specification />', () => {
  test('should render properly', () => {
    const props = {
      theme: PAGE_THEME.SECONDARY,
      technicalSpecification: {
        name: 'TestCase',
        values: [{ value: 'red', unit: 'Rs.' }],
        isCustomerVisible: true
      }
    };

    const component = mount(
      <ThemeProvider theme={{}}>
        <Component {...props} />
      </ThemeProvider>
    );

    expect(component).toMatchSnapshot();
  });
});
