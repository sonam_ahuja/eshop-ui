import React from 'react';
import { List, ListItem } from 'dt-components';
import SpecificationBlock from '@productDetailed/TechnicalSpecification/common/SpecificationBlock';
import { IProductSpecification } from '@productDetailed/store/types';
import { pageThemeType } from '@src/common/store/enums';

export interface IProps {
  theme: pageThemeType;
  technicalSpecification: IProductSpecification;
}

const Specification = (props: IProps) => {
  const { technicalSpecification, theme } = props;

  return (
    <SpecificationBlock theme={theme}>
      <List heading={technicalSpecification.name}>
        {technicalSpecification.values.map(
          (values: { value: string; unit?: string }, idx) => (
            <ListItem key={idx} description={`${values.value}`} />
          )
        )}
      </List>
    </SpecificationBlock>
  );
};

export default Specification;
