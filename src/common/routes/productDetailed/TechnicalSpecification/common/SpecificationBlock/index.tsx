import { Divider } from 'dt-components';
import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { pageThemeType } from '@src/common/store/enums';

import { theme as themeObj } from '../../theme';

const StyledSpecificationBlock = styled.div<{ theme: pageThemeType }>`
  font-size: 1rem;
  line-height: 1.5;

  .dt_paragraph {
    font-size: inherit;
    line-height: inherit;
  }
  .divider {
    height: 1px;
    min-height: 1px;
    margin: 1.25rem 0;
    color: ${({ theme }) => themeObj[theme].dividerColor};
    margin-right: -1.25rem;
  }

  .listWrap {
    .heading {
      color: ${({ theme }) => themeObj[theme].list.listHeadingColor};
    }

    .listItem {
      padding-left: 1.8125rem;
      color: ${({ theme }) => themeObj[theme].list.listItemColor};
      &:after {
        top: 0.6rem;
        height: 5px;
        width: 5px;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .listWrap {
      display: flex;

      .heading {
        width: 13.7rem;
        min-width: 13.7rem;
        margin: 0;
      }
      .listItem {
      }
    }
    .divider {
      margin-right: 0rem;
    }
  }
`;
interface IProps {
  theme: pageThemeType;
  children: ReactNode;
}

const SpecificationBlock = (props: IProps) => {
  const { children, theme } = props;

  return (
    <StyledSpecificationBlock className='specificationBlock' theme={theme}>
      {children}
      <Divider className='divider' />
    </StyledSpecificationBlock>
  );
};

export default SpecificationBlock;
