import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import translation from '@src/common/store/states/translation';
import { PAGE_THEME } from '@src/common/store/enums';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
const mockStore = configureStore();

import TechnicalSpecification from '.';

describe('<Technical Specification />', () => {
  window.scroll = jest.fn();
  const props = {
    theme: PAGE_THEME.SECONDARY,
    productSpecificationData: [
      {
        name: '',
        values: [{ value: 'string', unit: 'string' }],
        isCustomerVisible: true
      }
    ],
    productDetailedTranslation: translation().cart.productDetailed,
    offsetHeight: 0
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <TechnicalSpecification {...props} />
      </ThemeProvider>
    );

    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount<TechnicalSpecification>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <TechnicalSpecification {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component
      .find('TechnicalSpecification')
      .instance() as TechnicalSpecification).scrollToSpecification();
    (component
      .find('TechnicalSpecification')
      .instance() as TechnicalSpecification).onAccordionScroll();
    (component
      .find('TechnicalSpecification')
      .instance() as TechnicalSpecification).setAccordionRef(null);
    (component
      .find('TechnicalSpecification')
      .instance() as TechnicalSpecification).restoreScrollPosition();
    (component
      .find('TechnicalSpecification')
      .instance() as TechnicalSpecification).setStickyRef(null);
    (component
      .find('TechnicalSpecification')
      .instance() as TechnicalSpecification).componentWillUnmount();
  });
});
