import React, { Component, ReactNode } from 'react';
import { Accordion } from 'dt-components';
import { IProductSpecification } from '@productDetailed/store/types';
import Specification from '@productDetailed/TechnicalSpecification/Specification';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { pageThemeType } from '@src/common/store/enums';

import {
  StyledTechnicalSpecification,
  StyledTechnicalSpecificationWrap
} from './styles';

export interface IProps {
  theme: pageThemeType;
  offsetHeight: number;
  productSpecificationData: IProductSpecification[];
  productDetailedTranslation: IProductDetailedTranslation;
}
export interface IState {
  showSticky: boolean;
}

export default class TechnicalSpecification extends Component<IProps, IState> {
  accordionRef: HTMLElement | null = null;
  stickRef: HTMLElement | null = null;
  previousScrollTop = 0;

  constructor(props: IProps) {
    super(props);
    this.scrollToSpecification = this.scrollToSpecification.bind(this);
    this.setAccordionRef = this.setAccordionRef.bind(this);
    this.restoreScrollPosition = this.restoreScrollPosition.bind(this);
    this.onAccordionScroll = this.onAccordionScroll.bind(this);
    this.setStickyRef = this.setStickyRef.bind(this);
    this.state = {
      showSticky: false
    };
  }

  scrollToSpecification(): void {
    if (!this.accordionRef) {
      return;
    }
    this.previousScrollTop = window.scrollY;
    const y = this.accordionRef.getBoundingClientRect().top + window.scrollY;
    window.scroll({
      top: y - this.props.offsetHeight,
      behavior: 'smooth'
    });
  }

  onAccordionScroll(): void {
    if (!this.accordionRef) {
      return;
    }
  }

  setAccordionRef(ref: HTMLElement | null): void {
    this.accordionRef = ref;
    if (this.accordionRef) {
      this.accordionRef.addEventListener('scroll', this.onAccordionScroll);
    }
  }

  restoreScrollPosition(): void {
    if (this.previousScrollTop !== undefined) {
      window.scroll({
        top: this.previousScrollTop,
        behavior: 'smooth'
      });
    }
  }

  setStickyRef(ref: HTMLElement | null): void {
    this.stickRef = ref;
  }

  componentWillUnmount(): void {
    if (this.accordionRef) {
      this.accordionRef.removeEventListener('scroll', this.onAccordionScroll);
    }
  }

  render(): ReactNode {
    const {
      productSpecificationData,
      productDetailedTranslation,
      theme
    } = this.props;

    return (
      <>
        <StyledTechnicalSpecificationWrap theme={theme}>
          <Accordion
            className='accordion'
            getRef={this.setAccordionRef}
            onOpen={this.scrollToSpecification}
            onClose={this.restoreScrollPosition}
            headerTitle={productDetailedTranslation.technicalSpecification}
          >
            <StyledTechnicalSpecification theme={theme}>
              {productSpecificationData.map(
                (technicalSpecification: IProductSpecification, idx) => (
                  <Specification
                    theme={theme}
                    key={idx}
                    technicalSpecification={technicalSpecification}
                  />
                )
              )}{' '}
            </StyledTechnicalSpecification>
          </Accordion>
        </StyledTechnicalSpecificationWrap>
      </>
    );
  }
}
