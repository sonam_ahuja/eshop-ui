import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { pageThemeType } from '@src/common/store/enums';

import { theme as themeObj } from './theme';

export const StyledStickyDiv = styled.div<{ showSticky: boolean }>`
  position: fixed;
  z-index: 300;
  transition: 1s ease all;
  ${({ showSticky }) => (showSticky ? 'top: 0' : 'top: -2.5rem')};
`;

export const StyledTechnicalSpecification = styled.div<{
  theme: pageThemeType;
}>`
  padding: 0 1.25rem 3rem;
  background-color: ${({ theme }) => themeObj[theme].backgroundColor};

  .specificationBlock:last-child {
    .divider {
      display: none;
    }
  }
`;
export const StyledTechnicalSpecificationWrap = styled.div<{
  theme: pageThemeType;
}>`
  .accordion {
    .accordionHeader {
      background: ${({ theme }) => themeObj[theme].accordion.backgroundColor};
      padding: 1.75rem 1.25rem;

      .accordionTitle {
        color: ${({ theme }) => themeObj[theme].accordion.color};
      }
    }
    .dt_icon {
      font-size: 1.5rem;
    }
    .dt_divider {
      color: ${colors.shadowGray};
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .accordion {
      > div:first-child {
        padding-left: 4.9rem;
        padding-right: 4.9rem;
      }
      ${StyledTechnicalSpecification} {
        padding-top: 1rem;
        padding-left: 4.9rem;
        padding-right: 4.9rem;
      }
      .dt_icon {
        font-size: 1.5rem;
      }
    }
  }
`;

/* .specificationColors {
    .heading {
      color: ${theme[theme.activeTheme].list.listHeadingColor};
      font-size: 1rem;
      line-height: 1.5;
      letter-spacing: normal;
    }

    ul li {
      width: 7.5rem;

      img {
        width: 100%;
        display: block;
      }

      .colorName {
        color: ${theme[theme.activeTheme].list.listItemColor};
        font-size: 16px;
        font-weight: 500;
        line-height: 1.25;
        margin-top: 0.625rem;
      }
    }
  }
  */
