import { colors } from '@src/common/variables';

export const theme = {
  primary: {
    backgroundColor: colors.silverGray,
    list: {
      listHeadingColor: colors.mediumGray,
      listItemColor: colors.darkGray
    },
    dividerColor: colors.lightGray,
    accordion: {
      backgroundColor: colors.silverGray,
      color: colors.darkGray,
      iconColor: colors.mediumGray
    }
  },
  secondary: {
    backgroundColor: colors.charcoalGray,
    list: {
      listHeadingColor: colors.mediumGray,
      listItemColor: colors.cloudGray
    },
    dividerColor: colors.shadowGray,
    accordion: {
      backgroundColor: colors.charcoalGray,
      color: colors.white,
      iconColor: colors.magenta
    }
  }
};
