import { mapDispatchToProps } from '@productDetailed/mapProps';
import React from 'react';
import { MemoryRouter, StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import ProductDetailed, {
  IProps,
  ProductDetailed as ProductDetailedCompoent
} from '@productDetailed/index';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';
import {
  productDetailsResponse,
  productDetailsSuccessResponse
} from '@common/mock-api/productDetailed/details.mock';
import { tariffDetail } from '@common/mock-api/productDetailed/tariffDetail.mock';
import { ErrorData } from '@mocks/common/index';
import { notificationData } from '@mocks/productDetailed/details.mock';
import { histroyParams } from '@mocks/common/histroy';
import { ProductDetailedMobileVariationShell } from '@productDetailed/mobile.shell';
import { ProductDetailedImageMobileVariationShell } from '@productDetailed/mobileImage.shell';
import { MODAL_TYPE } from '@productDetailed/store/enum';
import { PAGE_THEME } from '@src/common/store/enums';

import { IGetNotificationRequestTemplatePayload } from '../store/types';
const exampleRoutePath = `/Devices-Mobile/Nokia%2017%20-32GB%20White/nokia_14/nokia_14`;
const exampleRoutePath2 = `localhost:3000/Devices-Mobile/Nokia%2017%20-32GB%20White/nokia_14/nokia_14`;
const mockStore = configureStore();

// tslint:disable-next-line:no-big-function
describe('<productDetailed />', () => {
  const props: IProps = {
    ...histroyParams,
    addToBasketLoading: true,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    commonError: {
      code: '',
      retryable: false
    },
    discountId: null,
    productDetailsData: productDetailsResponse,
    closeAgeLimitModal: jest.fn(),
    tariffId: '',
    agreementId: '',
    showYoungTariff: true,
    showYoungTariffErrorMessage: false,
    tariffStateProps: {
      ...histroyParams,
      confirmPlanBtnLoading: true,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      commonError: {
        code: '',
        retryable: false
      },
      tariff: appState().tariff,
      tariffTranslation: appState().translation.cart.tariff,
      tariffConfiguration: appState().configuration.cms_configuration.modules
        .tariff,
      selectedProductOfferingTerm: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      globalTranslation: appState().translation.cart.global,
      categories: appState().categories.categories,
      selectedProductOfferingBenefit: {
        label: 'label',
        value: 'value',
        isSelected: true
      }
    },
    tariffDispatchProps: {
      fetchTariff: jest.fn(),
      fetchCategories: jest.fn(),
      setDiscount: jest.fn(),
      setLoyalty: jest.fn(),
      showMagentaDiscount: jest.fn(),
      showAppShell: jest.fn(),
      showTermAndConditions: jest.fn(),
      setTariffId: jest.fn(),
      buyPlanOnly: jest.fn(),
      saveTariff: jest.fn(),
      toggleSelectedBenefit: jest.fn(),
      setCategoryId: jest.fn(),
      resetPlansData: jest.fn(),
      validateAgeLimit: jest.fn(),
      youngTariffCheck: jest.fn(),
      clickOnAddToBasket: jest.fn(),
      clickOnBuyPlanOnly: jest.fn(),
      ageLimitSuccess: jest.fn(),
      getTariffAddonPrice: jest.fn()
    },
    installmentDropdownData: productDetailsSuccessResponse.installmentGroup,
    tariffDropdownData: tariffDetail,
    totalPrices: [
      {
        priceType: 'priceType',
        actualValue: 23,
        recurringChargePeriod: '12',
        discounts: [
          {
            discount: 23,
            name: 'name',
            dutyFreeValue: 34,
            taxRate: 45,
            label: '',
            taxPercentage: 3
          }
        ],
        discountedValue: 3,
        recurringChargeOccurrence: 34,
        dutyFreeValue: 34,
        taxRate: 4,
        taxPercentage: 3
      }
    ],
    instalmentId: '',
    productHighlightsData: [
      {
        name: '',
        values: [{ value: 'string', unit: 'string' }],
        isCustomerVisible: true
      }
    ],
    productSpecificationData: [
      {
        name: '',
        values: [{ value: 'string', unit: 'string' }],
        isCustomerVisible: true
      }
    ],
    showModal: false,
    notificationModalType: MODAL_TYPE.CONFIRMATION_MODAL,
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,
    notificationData,
    error: null,
    productListtranslation: appState().translation.cart.productList,
    loading: false,
    notificationLoading: false,
    showAppShell: false,
    globalTranslation: appState().translation.cart.global,
    categoryId: null,
    productDetailedTranslation: appState().translation.cart.productDetailed,
    productDetailedConfiguration: appState().configuration.cms_configuration
      .modules.productDetailed,
    buyDeviceOnly: false,
    showImageAppShell: false,
    binkiesFallbackVisible: false,
    categoryName: 'demo',
    theme: PAGE_THEME.PRIMARY,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    parentCategorySlug: '/',
    lastListURL: null,
    fetchProductDetails: jest.fn(),
    openModal: jest.fn(),
    setDeviceList: jest.fn(),
    setLandingPageDeviceList: jest.fn(),
    sendStockNotification: jest.fn(),
    fetchProductSpecification: jest.fn(),
    setError: jest.fn(),
    setNotificationData: jest.fn(),
    addToBasket: jest.fn(),
    setProductVariantCategoryId: jest.fn(),
    setAppShell: jest.fn(),
    setCategoryAttributes: jest.fn(),
    setImageAppShell: jest.fn(),
    changeBinkiesFallbackVisible: jest.fn(),
    setTariffData: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn()
  };
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    window.scrollTo = jest.fn();
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <MemoryRouter
            initialEntries={[
              { pathname: exampleRoutePath, key: 'productDetailed' }
            ]}
          >
            <ProductDetailed />
          </MemoryRouter>
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('product detail component, ', () => {
    const newProps: IProps = { ...props };
    newProps.showAppShell = false;
    window.scrollTo = jest.fn();
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <MemoryRouter
            initialEntries={[
              { pathname: exampleRoutePath, key: 'productDetailed' }
            ]}
          >
            <ProductDetailedCompoent {...newProps} />
          </MemoryRouter>
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('ProductDetailedMobileVariationShell', () => {
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    window.scrollTo = jest.fn();
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <ProductDetailedMobileVariationShell />
          <ProductDetailedImageMobileVariationShell />
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('<productDetailed /> with details data & specification data', () => {
    const initStateValue = appState();
    initStateValue.productDetailed.productDetailsData = productDetailsResponse;
    initStateValue.productDetailed.loading = false;
    const store = mockStore(initStateValue);
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <MemoryRouter
            initialEntries={[
              { pathname: exampleRoutePath2, key: 'productDetailed' }
            ]}
          >
            <ProductDetailed />
          </MemoryRouter>
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
  test('handle trigger', () => {
    const copyComponentProps = { ...props };
    const initStateValue = appState();
    const store = mockStore(initStateValue);
    const component = mount<ProductDetailedCompoent>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ProductDetailedCompoent {...copyComponentProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).closePlanModal(false);
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).openPlanModal();

    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).onScroll();

    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).setVariantNameRef(null);
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).refereshDetailedPage();
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).componentDidUpdate(
      copyComponentProps
    );
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).componentWillUnmount();
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).addToBasket();
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).openNotificationModal();
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).onInstallmentChange(null);
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).onPlanChange(null);
    (component
      .find('ProductDetailed')
      .instance() as ProductDetailedCompoent).onAgreementChange(null);
  });

  test('getMetaData:: conditions', () => {
    const initStateValue = appState();
    initStateValue.productDetailed.productDetailsData.variants = [
      {
        id: '1',
        name: 'productName',
        description: '',
        group: '',
        characteristics: [],
        prices: [],
        tags: ['out of stock, in stock'],
        quantity: 2,
        unavailabilityReasonCodes: []
      }
    ];
    initStateValue.productDetailed.productDetailsData.variants[0].characteristics = [
      {
        name: 'metaTagTitle',
        values: [{ value: 'metaTagTitleValue' }]
      },
      {
        name: 'metaTagDescription',
        values: [{ value: 'metaTagDescriptionValue' }]
      }
    ];
    initStateValue.productDetailed.productSpecificationData = [
      {
        name: 'productExtraFeature',
        values: [{ value: 'productExtraFeatureValue' }],
        isCustomerVisible: true
      },
      {
        name: 'marketingContent',
        values: [{ value: 'marketingContentValue' }],
        isCustomerVisible: true
      },
      {
        name: 'highlightProductDetailedSection',
        values: [{ value: 'highlightProductDetailedSectionValue' }],
        isCustomerVisible: true
      }
    ];
    const store = mockStore(initStateValue);
    const component = mount(
      <ThemeProvider theme={{}}>
        <Provider store={store}>
          <MemoryRouter
            initialEntries={[
              { pathname: exampleRoutePath, key: 'productDetailed' }
            ]}
          >
            <ProductDetailed />
          </MemoryRouter>
        </Provider>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    const sendStockData: IGetNotificationRequestTemplatePayload = {
      type: NOTIFICATION_MEDIUM.EMAIL,
      variantId: '12',
      mediumValue: '122'
    };

    mapDispatchToProps(dispatch).sendStockNotification(sendStockData);
    mapDispatchToProps(dispatch).openModal(true);
    mapDispatchToProps(dispatch).setError(ErrorData);
    mapDispatchToProps(dispatch).setNotificationData(notificationData);
    mapDispatchToProps(dispatch).addToBasket('23');
    mapDispatchToProps(dispatch).setImageAppShell(true);
    mapDispatchToProps(dispatch).validateAgeLimit('string');
    mapDispatchToProps(dispatch).youngTariffCheck('string');
    mapDispatchToProps(dispatch).closeAgeLimitModal();
  });
  // tslint:disable-next-line: max-file-line-count
});
