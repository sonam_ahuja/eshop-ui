import {
  createQueryParams,
  getOutOfStockReason,
  getPlansFromTariff,
  getProdDetailTranslation,
  getProductDetailConfiguration,
  getSelectedCharacterstic,
  getTags
} from '@productDetailed/utils';
import appState from '@store/states/app';
import { tariff } from '@common/mock-api/tariff/tariff.mock';
import { ITEM_STATUS } from '@common/store/enums';

describe('<product Details utils />', () => {
  test('productDetails :: createQueryParams', () => {
    expect(typeof createQueryParams({ name: 'string', value: 'string' })).toBe(
      'string'
    );
    expect(typeof createQueryParams(null)).toBe('string');
  });
  test('productDetails :: getPlansFromTariff', () => {
    expect(getPlansFromTariff(tariff)).toBeDefined();
  });

  test('productDetails :: getProductDetailConfiguration', () => {
    expect(getProductDetailConfiguration(appState())).toBeDefined();
  });

  test('productDetails :: getProdDetailTranslation', () => {
    expect(getProdDetailTranslation(appState())).toBeDefined();
  });
  test('productDetails :: getSelectedCharacterstic', () => {
    const charecterstics = [
      {
        name: 'name',
        values: [
          {
            value: 'value'
          }
        ]
      }
    ];
    const varient = [
      {
        value: 'value',
        label: 'label',
        enabled: true,
        name: 'dummy'
      }
    ];
    expect(
      getSelectedCharacterstic(charecterstics, varient, 'name')
    ).toBeDefined();
    expect(
      getSelectedCharacterstic(charecterstics, varient, 'no-name')
    ).toBeUndefined();
  });
  test('productDetails :: getTags', () => {
    const tagsCharacterstic = 'string';
    const launchDateString = '2012-10-10';
    const newProductBeforeLaunchDay = 10;
    const productDetailedTranslation = appState().translation.cart
      .productDetailed;
    const showOnlineStockAvailability = true;
    expect(
      getTags(
        tagsCharacterstic,
        launchDateString,
        newProductBeforeLaunchDay,
        productDetailedTranslation,
        showOnlineStockAvailability,
        ITEM_STATUS.IN_STOCK
      )
    ).toBeDefined();
    expect(
      getTags(
        tagsCharacterstic,
        launchDateString,
        newProductBeforeLaunchDay,
        productDetailedTranslation,
        showOnlineStockAvailability,
        ITEM_STATUS.IN_STOCK
      )
    ).toBeDefined();
    expect(
      getTags(
        tagsCharacterstic,
        launchDateString,
        newProductBeforeLaunchDay,
        productDetailedTranslation,
        showOnlineStockAvailability,
        ITEM_STATUS.IN_STOCK
      )
    ).toBeDefined();
    expect(
      getTags(
        tagsCharacterstic,
        launchDateString,
        newProductBeforeLaunchDay,
        productDetailedTranslation,
        false,
        ITEM_STATUS.IN_STOCK
      )
    ).toBeDefined();
  });
  test('productDetails :: getOutOfStockReason', () => {
    expect(typeof getOutOfStockReason([ITEM_STATUS.IN_STOCK])).toBe('string');
  });
});
