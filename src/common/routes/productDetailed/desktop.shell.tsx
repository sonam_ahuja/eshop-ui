import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { Button } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

import ProductDetailedImageShell from './image.shell';

export const StyledProductShell = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  min-height: calc(100vh - 6.5625rem);
  padding-left: 3rem;

  .carousel {
    width: 50%;
    padding: 1rem 4.75rem 3rem;

    .breadCrumb {
      display: block;
    }
  }

  .variants {
    width: 47.512%;
    padding: 1rem 0rem 0 0;
    display: flex;
    flex-direction: column;
    background: ${colors.coldGray};
    overflow-y: auto;
    .heading {
      height: 2rem;
      margin-bottom: 1rem;
      margin-left: 4.75rem;
      margin-right: 4.75rem;
    }

    .list {
      margin-left: 4.75rem;
      margin-right: 4.75rem;
      li {
        display: flex;
        justify-content: space-between;
        padding: 0.75rem 0 0.7rem;
        border-bottom: 1px solid ${colors.cloudGray};

        .shine {
          height: 1.25rem;
          width: 40%;

          &.installment {
            height: 2rem;
          }

          &:first-child {
            width: 30%;
          }
        }
      }
    }

    .plan {
      height: 4.5rem;
      margin-bottom: 2rem;
      margin-left: 4.75rem;
      margin-right: 4.75rem;
    }
    .footer {
      align-self: flex-end;
      margin-top: auto;
      width: 100%;

      .totalAmount {
        height: 3rem;
        margin-left: 4.75rem;
        margin-right: 4.75rem;
      }
      .buttonWrap {
        margin-top: 2rem;
        .dt_button {
          width: 100%;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.desktop - 1}px) and (min-height: ${
  breakpoints.tabletPortrait
}px) {
    min-height: auto !important;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding-left: 4.9rem;
    min-height: calc(100vh - 6.25rem);
    .carousel {
      padding: 1rem 6rem 3rem 0;
    }
    .variants {
      width: 50%;
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding-left: 5.5rem;
    min-height: calc(100vh - 7.25rem);
  }

  /* @media (min-width: ${
    breakpoints.desktopLarge
  }px) and (min-height: 1100px) {
    min-height: auto !important;
  } */
`;

const ProductDetailedDesktopVariationShell = () => {
  return (
    <StyledShell className='primary'>
      <StyledProductShell>
        <div className='carousel'>
          <ProductDetailedImageShell />
        </div>
        <div className='variants'>
          <div className='shine heading' />
          <ul className='list'>
            <li>
              <div className='shine' />
              <div className='shine' />
            </li>
            <li>
              <div className='shine' />
              <div className='shine' />
            </li>
            <li>
              <div className='shine' />
              <div className='shine installment' />
            </li>
          </ul>
          <div className='plan shine' />
          <div className='footer'>
            <div className='totalAmount shine' />
            <div className='buttonWrap'>
              <Button type='primary' className='loading' />
            </div>
          </div>
        </div>
      </StyledProductShell>
    </StyledShell>
  );
};

export default ProductDetailedDesktopVariationShell;
