import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';

export const StyledProductShell = styled(StyledShell)`
  margin-top: 0rem;
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  .breadCrumb {
    height: 1rem;
    width: 100%;
    margin-bottom: 4rem;
    display: none;
  }

  .carouselWrap {
    display: flex;
    flex: 1;
    width: 100%;
    .thumbnails {
      display: flex;
      flex-wrap: wrap;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: 100%;
      .shine {
        height: 2rem;
        width: 2rem;
        margin: 1.25rem 0;
      }
    }
    .productImage {
      margin-left: 3rem;
      height: 100%;
      width: 100%;
    }
  }
`;

const ProductDetailedImageDesktopVariationShell = () => {
  return (
    <StyledProductShell className='primary'>
      <div className='shine breadCrumb' />
      <div className='carouselWrap'>
        <div className='thumbnails'>
          <div className='shine' />
          <div className='shine' />
          <div className='shine' />
          <div className='shine' />
          <div className='shine' />
        </div>
        <div className='shine productImage' />
      </div>
    </StyledProductShell>
  );
};

export default ProductDetailedImageDesktopVariationShell;
