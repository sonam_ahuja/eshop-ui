import React from 'react';
import { isMobile } from '@src/common/utils';

import ProductDetailedImageMobileVariationShell from './mobileImage.shell';
import ProductDetailedImageDesktopVariationShell from './desktopImage.shell';

const ProductDetailedImageShell = (): JSX.Element => {
  return (
    <>
      {isMobile.phone || isMobile.tablet ? (
        <ProductDetailedImageMobileVariationShell />
      ) : (
        <ProductDetailedImageDesktopVariationShell />
      )}
    </>
  );
};

export default ProductDetailedImageShell;
