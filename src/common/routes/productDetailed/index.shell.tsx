import React from 'react';
import { isMobile } from '@src/common/utils';

import ProductDetailedMobileVariationShell from './mobile.shell';
import ProductDetailedDesktopVariationShell from './desktop.shell';

const ProductDetailedShell = (): JSX.Element => {
  return (
    <>
      {isMobile.phone || isMobile.tablet ? (
        <ProductDetailedMobileVariationShell />
      ) : (
          <ProductDetailedDesktopVariationShell />
        )}
    </>
  );
};

export default ProductDetailedShell;
