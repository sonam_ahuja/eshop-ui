import {
  getProductCharacteristics,
  getProductHtml,
  getSelectedFiltersFromParams,
  getSelectedTariffName
} from '@productDetailed/utils';
import Loadable from 'react-loadable';
import { ITEM_STATUS, PAGE_THEME } from '@src/common/store/enums';
import {
  eligibilityUnavailabilityReasonType,
  ICharacteristic,
  IKeyValue,
  ITariffDetail
} from '@productDetailed/store/types';
import MarketingContent from '@productDetailed/MarketingContent';
import ProductView from '@productDetailed/ProductView';
import React, { Component, ReactNode } from 'react';
import { RootState } from '@src/common/store/reducers';
import Highlights from '@productDetailed/Highlights';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import ProductDetailedShell from '@productDetailed/index.shell';
import { Helmet } from 'react-helmet';
import { IWindow } from '@src/client/index';
import Error from '@src/common/components/Error';
import { Loader, Utils } from 'dt-components';
import AddToBasketSticky from '@productDetailed/AddToBasketSticky';
import { isMobile } from '@common/utils';
import CONSTANTS, { BIRTH_DATE_LIMIT } from '@common/constants/appConstants';
import { NOTIFICATION_MEDIUM } from '@productDetailed/store/enum';
import Header from '@common/components/Header';
import Footer from '@common/components/Footer';
import AgeLimit from '@common/routes/tariff/AgeLimit';
import { pageViewEvent } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import { TARIFF_CATEGORY } from '@src/common/constants/appConstants';
import { sendTariffImpressionEvent } from '@src/common/events/tariff';
import BasketStripWrapper from '@common/components/BasketStripWrapper';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';

import { getTariffDataFromURL } from './utils';
import { mapDispatchToProps, mapStateToProps } from './mapProps';
import {
  AppTariffDialogBox,
  StyledContentWrapper,
  StyledProductDetailed
} from './styles';
import {
  ICommonModalProps,
  IMapDispatchToProps,
  IMapStateToProps
} from './types';

const TechnicalSpecification = Loadable({
  loading: () => null,
  loader: () => import('@productDetailed/TechnicalSpecification')
});

const CommonModal = Loadable<ICommonModalProps, {}>({
  loading: () => null,
  loader: () => import('@productList/Modals/CommonModal'),
  render(
    loaded: typeof import('@productList/Modals/CommonModal'),
    props: ICommonModalProps
  ): ReactNode {
    const CommonModalLoaded = loaded.default;

    return <CommonModalLoaded {...props} />;
  }
});

const TariffHeader = Loadable({
  loading: () => null,
  loader: () => import('@tariff/TariffHeader')
});

const TariffPlans = Loadable({
  loading: () => null,
  loader: () => import('@tariff/TariffPlans')
});

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export interface IState {
  planModalOnOpenScrollPosition: number | null;
  planModalOpen: boolean;
}

export class ProductDetailed extends Component<IProps, IState> {
  addToBasketBtnRef: HTMLElement | null = null;
  addToBasketRef: HTMLElement | null = null;
  variantNameRef: HTMLElement | null = null;

  constructor(props: IProps) {
    super(props);
    this.state = {
      planModalOnOpenScrollPosition: null,
      planModalOpen: false
    };
    this.openNotificationModal = this.openNotificationModal.bind(this);
    this.addToBasket = this.addToBasket.bind(this);
    this.onAttributeChange = this.onAttributeChange.bind(this);
    this.getMetaData = this.getMetaData.bind(this);
    this.getProductVariantIDFromURL = this.getProductVariantIDFromURL.bind(
      this
    );
    this.setAddToBasketBtnRef = this.setAddToBasketBtnRef.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.addBinkiesScript = this.addBinkiesScript.bind(this);
    this.refereshDetailedPage = this.refereshDetailedPage.bind(this);
    this.setVariantNameRef = this.setVariantNameRef.bind(this);
    this.updateBinkiesDetails = this.updateBinkiesDetails.bind(this);
    this.closeTariffModal = this.closeTariffModal.bind(this);
    this.onInstallmentChange = this.onInstallmentChange.bind(this);
    this.onPlanChange = this.onPlanChange.bind(this);
    this.onAgreementChange = this.onAgreementChange.bind(this);
    this.closePlanModal = this.closePlanModal.bind(this);
    this.openPlanModal = this.openPlanModal.bind(this);
    this.getTheme = this.getTheme.bind(this);
    this.getShowMetaTitle = this.getShowMetaTitle.bind(this);
    this.getDeviceStockInfo = this.getDeviceStockInfo.bind(this);
  }

  closePlanModal(removeHash: boolean): void {
    if (removeHash) {
      this.props.history.replace({
        pathname: this.props.location.pathname,
        search: this.props.location.search,
        hash: ''
      });
    }
    document.body.classList.remove('overflowHide');
    if (this.state.planModalOnOpenScrollPosition) {
      window.scrollTo({
        top: this.state.planModalOnOpenScrollPosition
      });
    }
    this.setState({
      planModalOnOpenScrollPosition: 0,
      planModalOpen: false
    });
  }

  getDeviceStockInfo(): eligibilityUnavailabilityReasonType {
    const considerInventory = this.props.catalogConfiguration.preorder
      .considerInventory;
    // true value after checking if flag is set or not!
    const variant = this.props.productDetailsData.variants[0];
    let availability = ITEM_STATUS.IN_STOCK;
    if (
      variant &&
      variant.unavailabilityReasonCodes &&
      variant.unavailabilityReasonCodes.length
    ) {
      if (variant.unavailabilityReasonCodes.includes(ITEM_STATUS.PRE_ORDER)) {
        availability =
          variant.unavailabilityReasonCodes.includes(
            ITEM_STATUS.OUT_OF_STOCK
          ) && considerInventory
            ? ITEM_STATUS.OUT_OF_STOCK
            : ITEM_STATUS.PRE_ORDER;
      } else if (
        variant.unavailabilityReasonCodes.includes(ITEM_STATUS.OUT_OF_STOCK)
      ) {
        availability = ITEM_STATUS.OUT_OF_STOCK;
      }
    }

    return availability;
  }

  openPlanModal(): void {
    this.props.history.push({
      pathname: this.props.location.pathname,
      search: this.props.location.search,
      hash: '#tariffModal'
    });
    const scrollY = window.scrollY;
    document.body.classList.add('overflowHide');
    this.setState({
      planModalOnOpenScrollPosition: scrollY,
      planModalOpen: true
    });
    sendTariffImpressionEvent();
  }

  redirectToListingPage = (planId: string): void => {
    this.props.tariffDispatchProps.setTariffId(planId);
    this.props.setTariffData(
      planId,
      this.props.tariffStateProps.tariff.selectedProductOfferingTerm.value,
      this.props.instalmentId,
      this.props.tariffStateProps.tariff.selectedProductOfferingBenefit.value
    );

    this.props.tariffDispatchProps.saveTariff(this.closeTariffModal);
  }

  closeTariffModal(): void {
    this.closePlanModal(true);
    this.props.fetchProductDetails(false, null, null, false, false);
  }

  // tslint:disable:cognitive-complexity
  onScroll(): void {
    if (this.addToBasketBtnRef) {
      const boundingRect = this.addToBasketBtnRef.getBoundingClientRect();
      if (-boundingRect.top > boundingRect.height) {
        if (
          this.addToBasketRef &&
          !this.addToBasketRef.classList.contains('sticky')
        ) {
          this.addToBasketRef.classList.add('sticky');

          const appContainer = document.querySelector('.styledAppInner');
          const basketStrip = document.getElementById('basketStrip');

          if (appContainer) {
            const appWidth = `${appContainer.clientWidth -
              (basketStrip ? basketStrip.clientWidth : 0)}px`;
            this.addToBasketRef.style.width = appWidth;
          }
        }
      } else {
        if (
          this.addToBasketRef &&
          this.addToBasketRef.classList.contains('sticky')
        ) {
          this.addToBasketRef.classList.remove('sticky');
        }
      }
    }
  }

  setAddToBasketBtnRef(ref: HTMLElement | null): void {
    this.addToBasketBtnRef = ref;
  }

  setVariantNameRef(ref: HTMLElement | null): void {
    this.variantNameRef = ref;
  }

  refereshDetailedPage(): void {
    const { variantId } = this.getProductVariantIDFromURL(
      this.props.location.pathname
    );
    const { changedAttribute } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    this.props.fetchProductDetails(
      false,
      variantId,
      changedAttribute,
      false,
      false
    );
  }

  componentWillUnmount(): void {
    window.removeEventListener('scroll', this.onScroll);
    this.props.openModal(false);
    (window as IWindow).binkies.contentIdentifier = null;
    (window as IWindow).binkies.hide();
    // set tariff module showAppShell as true
    this.props.tariffDispatchProps.showAppShell(true);
    this.closePlanModal(false);
  }

  componentDidMount(): void {
    window.scrollTo({
      top: 0
    });
    pageViewEvent(PAGE_VIEW.DEVICE_DETAILED);

    if (
      !(
        (window as IWindow) &&
        (window as IWindow).binkies &&
        (window as IWindow).binkies.show
      )
    ) {
      this.updateBinkiesDetails();
    }
    window.addEventListener(
      'scroll',
      this.onScroll,
      Utils.passiveEvents
        ? {
            passive: true
          }
        : false
    );
    const {
      categorySlug,
      productId,
      variantId
    } = this.getProductVariantIDFromURL(this.props.location.pathname);
    const {
      instalmentId,
      agreementId,
      tariffId,
      discountId
    } = getTariffDataFromURL(this.props.location.search);
    this.props.setTariffData(tariffId, agreementId, instalmentId, discountId);

    const { changedAttribute } = getSelectedFiltersFromParams(
      this.props.location.search
    );

    this.props.setProductVariantCategoryId(productId, variantId, categorySlug);
    this.props.setCategoryAttributes([
      this.props.productDetailedConfiguration.color,
      this.props.productDetailedConfiguration.storage,
      this.props.productDetailedConfiguration.numberOfInstalments
    ]);
    this.props.fetchProductDetails(
      false,
      variantId,
      changedAttribute,
      true,
      false,
      true
    );
    this.props.setAppShell(true);
    this.props.changeBinkiesFallbackVisible(false);
    const tariffCategoryId = TARIFF_CATEGORY;
    this.props.tariffDispatchProps.setCategoryId(tariffCategoryId);
    this.props.tariffDispatchProps.fetchTariff(null, null, false);
    this.props.tariffDispatchProps.fetchCategories(tariffCategoryId);
    this.props.fetchProductSpecification(productId);
  }

  updateBinkiesDetails(): void {
    (window as IWindow).binkies = {
      onPageStyleSheetUrl: CONSTANTS.BINKIES_PAGE_STYLE(
        CONSTANTS.ESHOP_CDN_STATIC_URL
      ),
      inModalStyleSheetUrl: CONSTANTS.BINKIES_MODAL_STYLE(
        CONSTANTS.ESHOP_CDN_STATIC_URL
      ),
      contentIdentifier: null,
      onLoadingProgress: (_contentIdentifier: number, _progress: string) => {
        // tslint:disable-next-line:no-commented-code
        // logInfo(
        //   `Binkies progress is ${progress}, and contentIdentifier ${contentIdentifier}`
        // );
      },
      show: () => {
        document.body.classList.add('binkies-show');
        document.body.classList.remove('binkies-hide');
      },
      hide: () => {
        document.body.classList.remove('binkies-show');
        document.body.classList.add('binkies-hide');
      }
    };
    this.addBinkiesScript();
  }

  addBinkiesScript(): void {
    // TODO: Verify this check
    if ((window as IWindow).BinkiesIntegrationId === undefined) {
      const script = document.createElement('script');
      script.src = CONSTANTS.BINKIES_URL;
      script.async = true;
      document.body.appendChild(script);
    }
  }

  getProductVariantIDFromURL(
    pathname: string
  ): { categorySlug: string; productId: string; variantId: string } {
    const arrPath = pathname.split('/');
    if (arrPath && arrPath[1] && (arrPath[3] || arrPath[4])) {
      return {
        categorySlug: arrPath[1],
        productId: arrPath[3],
        variantId: arrPath[4]
      };
    }

    return { categorySlug: '', productId: '', variantId: '' };
  }

  componentDidUpdate(prevProps: IProps): void {
    // Plan modal check
    if (this.props.location.hash !== prevProps.location.hash) {
      const planModalOpen = this.props.location.hash.includes('#tariffModal');
      if (this.state.planModalOpen && !planModalOpen && this.props.history.action === 'POP') {
        this.closePlanModal(false);
      }
    }
    if (
      this.props.location.pathname === prevProps.location.pathname ||
      this.props.location.pathname.split('/').length < 3
    ) {
      return;
    }
    const {
      categorySlug,
      productId,
      variantId
    } = this.getProductVariantIDFromURL(this.props.location.pathname);
    const currentVariant = this.props.productDetailsData.variants[0];
    if (currentVariant && variantId !== currentVariant.id) {
      this.props.setProductVariantCategoryId(
        productId,
        variantId,
        categorySlug
      );
      // tslint:disable-next-line:no-commented-code
      // this.props.setImageAppShell(true);
      this.props.fetchProductDetails(false, null, null, false, false);
    }
  }

  onAttributeChange(changedAttribute: IKeyValue): void {
    this.props.fetchProductDetails(false, null, changedAttribute, false, false);
  }

  addToBasket(): void {
    const {
      productDetailsData: { variants }
    } = this.props;

    const selectedPlan = this.props.tariffDropdownData.find(
      (item: ITariffDetail) => {
        return item.active;
      }
    );
    if (selectedPlan) {
      let flag = false;
      selectedPlan.characteristics.forEach((item: ICharacteristic) => {
        if (item.name === BIRTH_DATE_LIMIT) {
          flag = true;
        }
      });
      if (flag) {
        this.props.youngTariffCheck(selectedPlan.id.toString());
      } else {
        if (variants[0] && variants[0].id) {
          this.props.addToBasket(variants[0].id);
        }
      }
    }
  }

  openNotificationModal(): void {
    if (
      this.props.productDetailsData.variants[0] &&
      this.props.productDetailsData.variants[0].unavailabilityReasonCodes
    ) {
      const { id } = this.props.productDetailsData.variants[0];
      const deviceName = this.props.productDetailsData.productName;
      this.props.setNotificationData({
        variantId: id,
        deviceName,
        mediumValue: '',
        reason: '',
        type: NOTIFICATION_MEDIUM.PHONE
      });
      this.props.openModal(true);
    }
  }

  onInstallmentChange(installmentValue: string | null): void {
    this.props.setTariffData(
      this.props.tariffId,
      this.props.agreementId,
      installmentValue,
      this.props.discountId
    );
    this.props.fetchProductDetails(false, null, null, false, false);
  }

  onPlanChange(planId: string | null): void {
    this.props.setTariffData(
      planId,
      this.props.agreementId,
      this.props.instalmentId,
      this.props.discountId
    );
    this.props.fetchProductDetails(false, null, null, false, true);
  }

  onAgreementChange(agreementId: string | null): void {
    this.props.setTariffData(
      this.props.tariffId,
      agreementId,
      this.props.instalmentId,
      this.props.discountId
    );
    this.props.fetchProductDetails(false, null, null, false, true);
  }

  getMetaData(): {
    title: string;
    description: string;
    productExtraFeature: string;
    marketingContent: string;
    highlightProductDetailedSection: string;
  } {
    if (!this.props.productDetailsData.variants[0]) {
      return {
        title: '',
        description: '',
        productExtraFeature: '',
        marketingContent: '',
        highlightProductDetailedSection: ''
      };
    }
    const characteristics = this.props.productDetailsData.variants[0]
      .characteristics;
    const { title, description } = getProductCharacteristics(characteristics);
    const {
      productExtraFeature,
      marketingContent,
      highlightProductDetailedSection
    } = getProductHtml(this.props.productHighlightsData);

    return {
      title: title ? title : '',
      description: description ? description : '',
      productExtraFeature:
        productExtraFeature &&
        productExtraFeature.values &&
        productExtraFeature.values[0]
          ? productExtraFeature.values[0].value
          : '',
      marketingContent:
        marketingContent &&
        marketingContent.values &&
        marketingContent.values[0]
          ? marketingContent.values[0].value
          : '',
      highlightProductDetailedSection:
        highlightProductDetailedSection &&
        highlightProductDetailedSection.values &&
        highlightProductDetailedSection.values[0]
          ? highlightProductDetailedSection.values[0].value
          : ''
    };
  }

  getTheme(): PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY {
    const fallbackTheme = this.props.productDetailedConfiguration.theme;

    return (
      this.props.theme ||
      ([PAGE_THEME.PRIMARY, PAGE_THEME.SECONDARY].includes(fallbackTheme)
        ? fallbackTheme
        : PAGE_THEME.SECONDARY)
    );
  }

  getShowMetaTitle(title: string): boolean {
    return !!(title && title !== '' && title.length !== 0);
  }

  // tslint:disable-next-line
  render(): ReactNode {
    const {
      productSpecificationData,
      productDetailsData,
      showModal,
      error,
      setError,
      notificationLoading,
      notificationData,
      formConfiguration,
      sendStockNotification,
      notificationModalType,
      openModal,
      productListtranslation,
      showAppShell,
      productDetailedConfiguration,
      productDetailedTranslation,
      buyDeviceOnly,
      categoryId,
      setImageAppShell,
      globalTranslation,
      showImageAppShell,
      loading,
      addToBasketLoading,
      parentCategorySlug,
      categoryName,
      lastListURL,
      binkiesFallbackVisible,
      changeBinkiesFallbackVisible,
      installmentDropdownData,
      tariffDropdownData,
      totalPrices,
      showYoungTariff,
      validateAgeLimit,
      currency,
      setDeviceList,
      setLandingPageDeviceList,
      youngTariffCheck,
      commonError,
      catalogConfiguration
    } = this.props;

    const tariffName = getSelectedTariffName(tariffDropdownData);

    const {
      title,
      description,
      productExtraFeature,
      marketingContent,
      highlightProductDetailedSection
    } = this.getMetaData();

    const showMetaTitle = this.getShowMetaTitle(title)
      ? title
      : productDetailsData.productName;
    const tariffProps = {
      ...this.props.tariffDispatchProps,
      ...this.props.tariffStateProps,
      history: this.props.history,
      location: this.props.location,
      match: this.props.match,
      planSelectionOnly: true
    };

    return (
      <>
        <Header showBasketStrip={false} />
        {commonError.httpStatusCode ? (
          <Error />
        ) : (
          <>
            <StyledProductDetailed>
              <StyledContentWrapper>
                <AppTariffDialogBox
                  isOpen={this.state.planModalOpen}
                  closeOnBackdropClick={true}
                  closeOnEscape={true}
                  onEscape={() => {
                    this.closePlanModal(true);
                  }}
                  onBackdropClick={() => {
                    this.closePlanModal(true);
                  }}
                  backgroundScroll={false}
                  type='fullHeight'
                  onClose={() => {
                    this.closePlanModal(true);
                  }}
                >
                  <TariffHeader {...tariffProps} />
                  <TariffPlans
                    {...tariffProps}
                    setMultiplePlansWrapRef={() => null}
                    setMobileAccordionPanelRef={() => null}
                    addPlanToBasket={() => null}
                    redirectToListingPage={this.redirectToListingPage}
                  />
                </AppTariffDialogBox>
                <Helmet
                  title={showMetaTitle ? showMetaTitle : ''}
                  meta={[
                    {
                      name: 'description',
                      content: description ? description : ''
                    }
                  ]}
                />
                <AddToBasketSticky
                  loading={loading}
                  tariffName={tariffName}
                  productName={productDetailsData.productName}
                  addToBasket={this.addToBasket}
                  productDetailedTranslation={productDetailedTranslation}
                  productDetailedConfiguration={productDetailedConfiguration}
                  notifyMe={this.openNotificationModal}
                  variant={productDetailsData.variants[0]}
                  variantGroups={productDetailsData.groups}
                  showNotifyMe={
                    productDetailedConfiguration.productDetailedTemplate1
                      .showNotifyMe
                  }
                  setAddToBasketRef={ref => {
                    this.addToBasketRef = ref;
                  }}
                />
                {
                  <Loader
                    open={
                      !showAppShell &&
                      (loading || this.props.tariffStateProps.tariff.loading)
                    }
                    label={productDetailedTranslation.loadingText}
                  />
                }{' '}
                {!showAppShell && (
                  <>
                    <CommonModal
                      isOpen={showModal}
                      deviceName={notificationData.deviceName}
                      variantId={notificationData.variantId}
                      errorMessage={error && error.message}
                      sendStockNotification={sendStockNotification}
                      setError={setError}
                      notificationLoading={notificationLoading}
                      modalType={notificationModalType}
                      translation={productListtranslation}
                      notificationMediumType={notificationData.type}
                      notificationMediumValue={notificationData.mediumValue}
                      openModal={openModal}
                      formConfiguration={formConfiguration}
                    />
                    <ProductView
                      binkiesFallbackVisible={binkiesFallbackVisible}
                      deviceStock={this.getDeviceStockInfo()}
                      productDetailsData={productDetailsData}
                      currency={currency}
                      tariffCategory={this.props.tariffStateProps.categories}
                      addToBasket={this.addToBasket}
                      openNotificationModal={this.openNotificationModal}
                      onAttributeChange={this.onAttributeChange}
                      globalTranslation={globalTranslation}
                      addToBasketLoading={addToBasketLoading}
                      productDetailedTranslation={productDetailedTranslation}
                      productDetailedConfiguration={
                        productDetailedConfiguration
                      }
                      buyDeviceOnly={buyDeviceOnly}
                      categoryId={categoryId}
                      showImageAppShell={showImageAppShell}
                      changeBinkiesFallbackVisible={
                        changeBinkiesFallbackVisible
                      }
                      parentCategorySlug={parentCategorySlug}
                      categoryName={categoryName}
                      lastListURL={lastListURL}
                      setVariantNameRef={this.setVariantNameRef}
                      setImageAppShell={setImageAppShell}
                      refereshDetailedPage={this.refereshDetailedPage}
                      setAddToBasketBtnRef={this.setAddToBasketBtnRef}
                      installmentDropdownData={installmentDropdownData}
                      tariffDropdownData={tariffDropdownData}
                      totalPrices={totalPrices}
                      addToBasketRef={this.addToBasketRef}
                      onInstallmentChange={this.onInstallmentChange}
                      onPlanChange={this.onPlanChange}
                      onAgreementChange={this.onAgreementChange}
                      youngTariffCheck={youngTariffCheck}
                      showAllPlans={this.openPlanModal}
                      setDeviceList={setDeviceList}
                      setLandingPageDeviceList={setLandingPageDeviceList}
                      catalogConfiguration={catalogConfiguration}
                    />
                    {productDetailedConfiguration.productDetailedTemplate1
                      .showHighlightPromo ? (
                      <Highlights
                        highlightProductDetailedSection={
                          highlightProductDetailedSection
                        }
                      />
                    ) : null}
                    {productDetailedConfiguration.productDetailedTemplate1
                      .showTechnicalSpecification ? (
                      <TechnicalSpecification
                        offsetHeight={
                          isMobile.phone || isMobile.tablet ? 64 : 80
                        }
                        productSpecificationData={productSpecificationData}
                        productDetailedTranslation={productDetailedTranslation}
                        theme={this.getTheme()}
                      />
                    ) : null}

                    {productDetailedConfiguration.productDetailedTemplate1
                      .showOverViewSection ? (
                      <MarketingContent
                        productExtraFeature={productExtraFeature}
                        marketingContent={marketingContent}
                      />
                    ) : null}
                  </>
                )}
                {showAppShell ? <ProductDetailedShell /> : null}
                <AuxiliaryRoute
                  isChildrenRender={showYoungTariff}
                  hashPath={auxillaryRouteConfig.DEVICE_DETAILED.YOUNG_TARIFF}
                  isModal={true}
                  onClose={this.props.closeAgeLimitModal}
                >
                  {showYoungTariff && (
                    <AgeLimit
                      youngTariffTranslation={
                        productDetailedTranslation.youngTariff
                      }
                      validateAgeLimit={validateAgeLimit}
                      closeModal={this.props.closeAgeLimitModal}
                      error={this.props.showYoungTariffErrorMessage}
                    />
                  )}
                </AuxiliaryRoute>
              </StyledContentWrapper>
              <BasketStripWrapper />
            </StyledProductDetailed>
            <Footer pageName={PAGE_VIEW.DEVICE_DETAILED} />
          </>
        )}
      </>
    );
  }
}

export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(ProductDetailed)
  // tslint:disable-next-line:max-file-line-count
);
