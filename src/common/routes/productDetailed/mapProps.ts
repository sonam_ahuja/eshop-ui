import actions from '@productDetailed/store/actions';
import { Dispatch } from 'redux';
import { RootState } from '@src/common/store/reducers';
import {
  IError,
  IGetNotificationRequestTemplatePayload,
  IKeyValue,
  INotificationData
} from '@productDetailed/store/types';
import {
  mapDispatchToProps as tariffMapDispatchToProps,
  mapStateToProps as tariffMapStateToProps
} from '@tariff/mapProps';
import commonActions from '@common/store/actions/common';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  commonError: state.common.error,
  currency: state.configuration.cms_configuration.global.currency,
  productDetailsData: state.productDetailed.productDetailsData,
  productHighlightsData: state.productDetailed.productHighlightsData,
  productSpecificationData: state.productDetailed.productSpecificationData,
  showModal: state.productDetailed.openModal,
  notificationModalType: state.productDetailed.notificationModal,
  notificationData: state.productDetailed.notificationData,
  error: state.productDetailed.error,
  notificationLoading: state.productDetailed.notificationLoading,
  productListtranslation: state.translation.cart.productList,
  showAppShell: state.productDetailed.showAppShell,
  productDetailedTranslation: state.translation.cart.productDetailed,
  loading: state.productDetailed.loading,
  addToBasketLoading: state.productDetailed.addToBasketLoading,
  discountId: state.productDetailed.discountId,
  formConfiguration:
    state.configuration.cms_configuration.global.loginFormRules,
  productDetailedConfiguration:
    state.configuration.cms_configuration.modules.productDetailed,
  catalogConfiguration: state.configuration.cms_configuration.modules.catalog,
  buyDeviceOnly: state.productDetailed.buyDeviceOnly,
  globalTranslation: state.translation.cart.global,
  categoryId: state.productDetailed.categoryId,
  showImageAppShell: state.productDetailed.showImageAppShell,
  categoryName: state.productDetailed.categoryName,
  theme: state.productDetailed.theme,
  parentCategorySlug: state.productDetailed.parentCategorySlug,
  lastListURL: state.productList.lastListURL,
  binkiesFallbackVisible: state.productDetailed.binkiesFallbackVisible,
  tariffId: state.productDetailed.tariffId,
  agreementId: state.productDetailed.agreementId,
  installmentDropdownData: state.productDetailed.installmentDropdownData,
  tariffDropdownData: state.productDetailed.tariffDropdownData,
  totalPrices: state.productDetailed.productDetailsData.totalPrices,
  showYoungTariff: state.productDetailed.showYoungTariff,
  instalmentId: state.productDetailed.instalmentId,
  showYoungTariffErrorMessage:
    state.productDetailed.showYoungTariffErrorMessage,
  tariffStateProps: tariffMapStateToProps(state)
});
export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  setProductVariantCategoryId(
    productId: string,
    variantId: string,
    categoryId: string
  ): void {
    dispatch(
      actions.setProductVariantCategoryId({ productId, variantId, categoryId })
    );
  },
  fetchProductDetails(
    preselectedPreferences: boolean,
    variantId: string | null,
    changedAttribute: IKeyValue | null,
    fetchCategoryData: boolean,
    savePlan: boolean,
    // tslint:disable-next-line:bool-param-default
    showFullPageError?: boolean
    // setAllVariantAttributesAsSelected: boolean
  ): void {
    dispatch(
      actions.fetchProductDetailed({
        preselectedPreferences,
        variantId,
        changedAttribute,
        fetchCategoryData,
        savePlan,
        showFullPageError
        // setAllVariantAttributesAsSelected
      })
    );
  },
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void {
    dispatch(actions.sendStockNotification(payload));
  },
  fetchProductSpecification(productId: string): void {
    dispatch(actions.fetchProductSpecification(productId));
  },
  openModal(isModalOpen: boolean): void {
    dispatch(actions.openModal(isModalOpen));
  },
  setError(error: IError): void {
    dispatch(actions.setError(error));
  },
  setNotificationData(data: INotificationData): void {
    dispatch(actions.setNotificationData(data));
  },
  addToBasket(): void {
    dispatch(actions.addToBasket());
  },
  setAppShell(showAppShell: boolean): void {
    dispatch(actions.setAppShell(showAppShell));
  },
  setCategoryAttributes(attributes: string[]): void {
    dispatch(actions.setCategoryAttributes({ categorySlug: '', attributes }));
  },
  setImageAppShell(showImageAppShell: boolean): void {
    dispatch(actions.setImageAppShell(showImageAppShell));
  },
  changeBinkiesFallbackVisible(binkiesFallbackVisible: boolean): void {
    dispatch(actions.changeBinkiesFallbackVisible(binkiesFallbackVisible));
  },
  setTariffData(
    tariffId: string | null,
    agreementId: string | null,
    instalmentId: string | null,
    discountId: string | null
  ): void {
    dispatch(
      actions.setTariffData({
        tariffId,
        agreementId,
        instalmentId,
        discountId
      })
    );
  },
  validateAgeLimit(data: string): void {
    dispatch(actions.validateAgeLimit(data));
  },
  youngTariffCheck(planId: string): void {
    dispatch(actions.checkYoungTariff(planId));
  },
  closeAgeLimitModal(): void {
    dispatch(actions.ageLimit());
  },
  setDeviceList(list: string): void {
    dispatch(commonActions.setDeviceList(list));
  },
  setLandingPageDeviceList(list: string): void {
    dispatch(commonActions.setLandingPageDeviceList(list));
  },
  tariffDispatchProps: (() => {
    return tariffMapDispatchToProps(dispatch);
  })()
});
