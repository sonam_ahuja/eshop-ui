import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { Button } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

import ProductDetailedImageShell from './image.shell';

export const StyledProductShell = styled.div`
  width: 100%;

  .carousel {
    padding: 1rem 3rem;
    /* carousel shell */
    .StyledProductShell {
      position: static;
    }
    .breadCrumb {
      display: block;
    }
  }

  .variants {
    padding: 1rem 1.25rem 0;
    /* min-height: calc(100vh - 5rem); */
    min-height: 30rem;
    display: flex;
    flex-direction: column;
    background: ${colors.coldGray};
    .heading {
      height: 2rem;
      margin-bottom: 1rem;
    }

    .list {
      li {
        display: flex;
        justify-content: space-between;
        padding: 0.75rem 0 0.7rem;
        border-bottom: 1px solid ${colors.cloudGray};

        .shine {
          height: 1.25rem;
          width: 40%;

          &.installment {
            height: 2rem;
          }

          &:first-child {
            width: 30%;
          }
        }
      }
    }

    .plan {
      height: 4.5rem;
      margin-bottom: 2rem;
    }
    .footer {
      align-self: flex-end;
      margin-top: auto;
      width: 100%;

      .totalAmount {
        height: 3rem;
      }
      .buttonWrap {
        margin-top: 2rem;
        margin: 0 -1.25rem 0rem;
        .dt_button {
          margin-top: 1rem;
          width: 100%;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .variants {
      padding: 1.25rem 2.25rem 0;
      min-height: 40rem;
      .footer {
        .buttonWrap {
          margin: 0 -2.25rem 0rem;
          .dt_button {
            margin-top: 1.25rem;
          }
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .variants {
      padding: 1.25rem 3rem 0;
      .footer {
        .buttonWrap {
          margin: 0 -3rem 0rem;
        }
      }
    }
  }
`;

export const ProductDetailedMobileVariationShell = () => {
  return (
    <StyledShell className='primary'>
      <StyledProductShell>
        <div className='carousel'>
          <ProductDetailedImageShell />
        </div>
        <div className='variants'>
          <div className='shine heading' />
          <ul className='list'>
            <li>
              <div className='shine' />
              <div className='shine' />
            </li>
            <li>
              <div className='shine' />
              <div className='shine' />
            </li>
            <li>
              <div className='shine' />
              <div className='shine installment' />
            </li>
          </ul>
          <div className='plan shine' />
          <div className='footer'>
            <div className='totalAmount shine' />
            <div className='buttonWrap'>
              <Button type='primary' className='loading' />
            </div>
          </div>
        </div>
      </StyledProductShell>
    </StyledShell>
  );
};

export default ProductDetailedMobileVariationShell;
