import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints } from '@src/common/variables';

export const StyledProductShell = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;

  .breadCrumb {
    height: 1rem;
    width: 100%;
    margin-bottom: 2.25rem;
    display: none;
  }

  .carouselWrap {
    display: flex;
    align-items: center;

    .productImage {
      height: 15rem;
      width: 15rem;
      margin: auto;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .carouselWrap {
      height: 39rem;
    .productImage {
      height: 30rem;
      width: 25rem;
    }
  }
  }
`;

export const ProductDetailedImageMobileVariationShell = () => {
  return (
    <StyledShell className='primary'>
      <StyledProductShell className='StyledProductShell'>
        <div className='shine breadCrumb' />
        <div className='carouselWrap'>
          <div className='shine productImage' />
        </div>
      </StyledProductShell>
    </StyledShell>
  );
};

export default ProductDetailedImageMobileVariationShell;
