import { productDetailedReducer } from '@productDetailed/store';
import IProductDetailedState from '@productDetailed/store/state';

describe('basketReducer ', () => {
  it('basketReducer test', () => {
    const definedAction: { type: string } = { type: '' };
    expect(
      productDetailedReducer(IProductDetailedState(), definedAction)
    ).toBeDefined();
  });
});
