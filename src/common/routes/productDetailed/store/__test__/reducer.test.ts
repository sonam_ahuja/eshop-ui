import productDetailedReducer from '@productDetailed/store/reducer';
import productDetailedState from '@productDetailed/store/state';
import actions from '@productDetailed/store/actions';
import { IProductDetailedState } from '@productDetailed/store/types';
import {
  productDetailsResponse,
  productDetailsSuccessResponse,
  productSpecificationResponse
} from '@src/common/mock-api/productDetailed/details.mock';
import { MODAL_TYPE } from '@productDetailed/store/enum';

const errorMessage = 'Dummy Error';

describe('Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IProductDetailedState = productDetailedReducer(
    undefined,
    definedAction
  );
  let expectedState: IProductDetailedState = productDetailedState();
  const initializeValue = () => {
    initialStateValue = productDetailedReducer(undefined, definedAction);
    expectedState = productDetailedState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('setProductVariantCategoryId ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      productId: '1',
      variantId: '1',
      categoryId: '1'
    };
    const action = actions.setProductVariantCategoryId({
      productId: '1',
      variantId: '1',
      categoryId: '1'
    });
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('fetchProductDetailedLoading ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      loading: true,
      error: null
    };
    const action = actions.fetchProductDetailedLoading();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('fetchProductDetailedError ::', () => {
    const error = new Error(errorMessage);
    const expected: IProductDetailedState = {
      ...expectedState,
      error
    };
    const action = actions.fetchProductDetailedError(new Error(errorMessage));
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('fetchProductDetailedSuccess ::', () => {
    let expected: IProductDetailedState = {
      ...expectedState,
      productDetailsData: productDetailsResponse,
      selectedAttributes: [],
      loading: false,
      error: null,
      showAppShell: false,
      instalmentId: ''
    };
    const action = actions.fetchProductDetailedSuccess(
      productDetailsSuccessResponse
    );
    expected = {
      ...expected,
      ...{
        buyWithoutInstallments: false,
        instalmentId: '',
        installmentDropdownData: productDetailsSuccessResponse.installmentGroup,
        selectedAttributes: productDetailsSuccessResponse.selectedAttributes
      }
    };
    expected.instalmentId = null;
    expected.showImageAppShell = false;
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('fetchProductSpecificationLoading ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      productSpecificationLoading: true,
      productSpecificationError: null
    };
    const action = actions.fetchProductSpecificationLoading();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('fetchProductSpecificationError ::', () => {
    const error = new Error(errorMessage);
    const expected: IProductDetailedState = {
      ...expectedState,
      productSpecificationError: error
    };
    const action = actions.fetchProductSpecificationError(error);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('fetchProductSpecificationSuccess ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      productSpecificationData: productSpecificationResponse,
      productSpecificationLoading: false,
      productSpecificationError: null
    };
    const action = actions.fetchProductSpecificationSuccess(
      productSpecificationResponse
    );
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('addToBasketLoading ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      loading: false,
      error: null,
      addToBasketLoading: true
    };
    const action = actions.addToBasketLoading();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('addToBasketError ::', () => {
    const error = new Error(errorMessage);
    const expected: IProductDetailedState = {
      ...expectedState,
      error
    };
    const action = actions.addToBasketError(error);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('addToBasketSuccess ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      loading: false
    };
    const action = actions.addToBasketSuccess();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('fetchProductHighlightSuccess', () => {
    const productHighlightsData = [
      {
        name: '',
        values: [{ value: '' }],
        isCustomerVisible: true
      }
    ];
    const expected: IProductDetailedState = {
      ...expectedState,
      productHighlightsData
    };
    const action = actions.fetchProductHighlightSuccess(productHighlightsData);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('setCategoryAttributes', () => {
    const categoryAttributes = {
      categorySlug: 'slug',
      attributes: ['a1', 'a2', 'a3']
    };
    const expected: IProductDetailedState = {
      ...expectedState,
      categoryAttributesMap: categoryAttributes.attributes
    };
    const action = actions.setCategoryAttributes(categoryAttributes);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('sendStockNotificationSuccess', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      notificationModal: MODAL_TYPE.CONFIRMATION_MODAL,
      notificationLoading: false
    };
    const action = actions.sendStockNotificationSuccess();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
});
