import productDetailedReducer from '@productDetailed/store/reducer';
import productDetailedState from '@productDetailed/store/state';
import actions from '@productDetailed/store/actions';
import { IProductDetailedState, ITariffDataPayload } from '@productDetailed/store/types';
import { SendStockPayload } from '@common/mock-api/productList/productList.mock';
import { PAGE_THEME } from '@src/common/store/enums';
import categoryState from '@category/store/state';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productDetailed/store/enum';
const errorMessage = 'Dummy Error';

describe('Reducer2', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IProductDetailedState = productDetailedReducer(
    undefined,
    definedAction
  );
  let expectedState: IProductDetailedState = productDetailedState();
  const initializeValue = () => {
    initialStateValue = productDetailedReducer(undefined, definedAction);
    expectedState = productDetailedState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('setCategoryData', () => {
    const expected: IProductDetailedState | unknown = {
      ...expectedState,
      showAppShell: true,
      theme: PAGE_THEME.SECONDARY,
      parentCategorySlug: null
    };
    const action = actions.setCategoryData(categoryState().categories);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('sendStockNotification', () => {
    const data = {
      variantId: '',
      deviceName: '',
      mediumValue: '12',
      reason: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    };
    const expected: IProductDetailedState | unknown = {
      ...expectedState,
      showAppShell: true,
      notificationData: data
    };
    const action = actions.sendStockNotification(SendStockPayload);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('setImageAppShell::', () => {
    const expected: IProductDetailedState | unknown = {
      ...expectedState,
      showImageAppShell: true
    };
    const action = actions.setImageAppShell(true);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('sendStockNotificationLoading ::', () => {
    const expected: IProductDetailedState | unknown = {
      ...expectedState,
      notificationLoading: true,
      notificationError: null
    };
    const action = actions.sendStockNotificationLoading();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('changeBinkiesFallbackVisible ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      loading: false,
      binkiesFallbackVisible: true,
      error: {
        code: 0,
        message: ''
      }
    };
    const action = actions.changeBinkiesFallbackVisible(true);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('setTariffData ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      loading: false,
      error: {
        code: 0,
        message: ''
      }
    };
    const newParams: ITariffDataPayload = {
      tariffId: null,
      agreementId: null,
      instalmentId: null,
      discountId: null
    };
    const action = actions.setTariffData(newParams);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('checkYoungTariff ::', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      loading: false,
      showYoungTariff: true,
      youngTariffId: 'string',
      error: {
        code: 0,
        message: ''
      }
    };
    const action = actions.checkYoungTariff('string');
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('setNotificationData', () => {
    const data = {
      variantId: '',
      deviceName: '',
      mediumValue: '',
      reason: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    };
    const expected: IProductDetailedState = {
      ...expectedState,
      notificationData: data
    };
    const action = actions.setNotificationData(data);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('setAppShell', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      showAppShell: true
    };
    const action = actions.setAppShell(true);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('sendStockNotificationError', () => {
    const error = new Error(errorMessage);
    const action = actions.sendStockNotificationError(error);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toBeDefined();
  });
  it('setError', () => {
    const error = new Error(errorMessage);
    const expected: IProductDetailedState = {
      ...expectedState,
      error
    };
    const action = actions.setError(error);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('openModal: true', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      openModal: true,
      error: null,
      notificationModal: expectedState.notificationModal
    };
    const action = actions.openModal(true);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
  it('openModal: false', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      openModal: false,
      error: null,
      notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL
    };
    const action = actions.openModal(false);
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
});
