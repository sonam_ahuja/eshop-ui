import productDetailedReducer from '@productDetailed/store/reducer';
import productDetailedState from '@productDetailed/store/state';
import actions from '@productDetailed/store/actions';
import { IProductDetailedState } from '@productDetailed/store/types';
import { MODAL_TYPE } from '@productDetailed/store/enum';

describe('Reducer3', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IProductDetailedState = productDetailedReducer(
    undefined,
    definedAction
  );
  let expectedState: IProductDetailedState = productDetailedState();
  const initializeValue = () => {
    initialStateValue = productDetailedReducer(undefined, definedAction);
    expectedState = productDetailedState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('ageLimitSuccess', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      openModal: false,
      error: {
        code: 0,
        message: ''
      },
      notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL
    };
    const action = actions.ageLimitSuccess();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('ageLimitError', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      openModal: false,
      showYoungTariffErrorMessage: true,
      error: {
        code: 0,
        message: ''
      },
      notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL
    };
    const action = actions.ageLimitError();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });

  it('ageLimit', () => {
    const expected: IProductDetailedState = {
      ...expectedState,
      openModal: false,
      error: null,
      notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL
    };
    const action = actions.ageLimit();
    const state = productDetailedReducer(initialStateValue, action);
    expect(state).toEqual(expected);
  });
});
