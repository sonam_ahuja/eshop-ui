import sagas, {
  addToBasket,
  ageLimitSuccess,
  fetchProductDetails,
  fetchProductSpecification,
  saveTariffPlan
} from '@src/common/routes/productDetailed/store/sagas';
import appState from '@store/states/app';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import * as productDetailedApi from '@common/types/api/productDetailed';
import apiEndpoints from '@src/common/constants/apiEndpoints';
import { productDetailsResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { ISaveTariff } from '@tariff/store/types';

import { IFetchProductDetailsAction, IProductDetailedData } from '../types';

import { getAddToBasketReqObject } from './../utils';

describe('Saga', () => {
  const mock = new MockAdapter(axios);
  const params: {
    buyDeviceOnly: boolean;
    buyWithoutInstallments: boolean;
    productDetailsData: IProductDetailedData;
    considerStockForPreOrder: boolean;
  } = {
    buyDeviceOnly: true,
    buyWithoutInstallments: false,
    productDetailsData: productDetailsResponse,
    considerStockForPreOrder: false
  };
  it('Product saga test', () => {
    const generator = sagas();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('addToBasket saga test', () => {
    const addToBasketGenerator = addToBasket();
    expect(
      addToBasketGenerator.next(appState().productDetailed).value
    ).toBeDefined();
    expect(
      addToBasketGenerator.next(
        appState().configuration.cms_configuration.modules.catalog
      ).value
    ).toBeDefined();
    expect(addToBasketGenerator.next().value).toBeDefined();
    expect(addToBasketGenerator.next().value).toBeDefined();
  });

  // tslint:disable-next-line:no-identical-functions
  it('addToBasket saga Error test', () => {
    const addToBasketGenerator = addToBasket();
    expect(addToBasketGenerator.next().value).toBeDefined();
    expect(addToBasketGenerator.next().value).toBeDefined();
    const getAddToBasketReq = getAddToBasketReqObject(params);
    mock
      .onPatch(apiEndpoints.BASKET.UPDATE_BASKET_ITEM.url, getAddToBasketReq)
      .replyOnce(500, {});
    expect(addToBasketGenerator.next().value).toBeDefined();
  });

  it('fetchProductDetails saga Error', () => {
    const action: {
      type: string;
      payload: IFetchProductDetailsAction;
    } = {
      type: '',
      payload: {
        variantId: '1',
        fetchCategoryData: true,
        preselectedPreferences: true,
        changedAttribute: null,
        savePlan: false
      }
    };
    const notifyMeGenerator = fetchProductDetails(action);
    expect(notifyMeGenerator.next().value).toBeDefined();
    mock
      .onPost(
        `${apiEndpoints.PRODUCT_DETAILED.NOTIFY_ME.url}/${action.payload}`
      )
      .replyOnce(500, {});
  });

  it('fetchProductSpecification saga Error', () => {
    const action: {
      type: string;
      payload: productDetailedApi.GET_PRODUCT_SPECIFICATION.IRequest;
    } = {
      type: '',
      payload: 'payload'
    };
    const notifyMeGenerator = fetchProductSpecification(action);
    expect(notifyMeGenerator.next().value).toBeDefined();
    mock
      .onPost(
        `${apiEndpoints.PRODUCT_DETAILED.NOTIFY_ME.url}/${action.payload}`
      )
      .replyOnce(500, {});
  });

  it('saveTariffPlan saga Error', () => {
    const saveTariff: ISaveTariff = {
      category: 'string',
      selectedProductOfferingTerm: 'string',
      selectedTariff: 'string',
      discount: 'string',
      addons: null
    };
    const saveTariffGenerator = saveTariffPlan(saveTariff);
    expect(saveTariffGenerator.next().value).toBeDefined();
  });

  it('ageLimitSuccess saga Error', () => {
    const ageLimitGenerator = ageLimitSuccess();
    expect(ageLimitGenerator.next().value).toBeDefined();
  });
});
