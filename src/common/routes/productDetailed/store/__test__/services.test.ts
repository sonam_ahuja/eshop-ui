import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import apiEndpoints from '@src/common/constants/apiEndpoints';
import {
  addToBasketService,
  fetchCategoryAttributes,
  fetchProductDetailsService,
  fetchProductSpecificationService,
  jsonLD
} from '@productDetailed/store/services';

import { getProductDetailsRequestTemplate } from '../utils';

const errorResponse = { errorMessage: 'An Error has Occured!' };
const productSpecificationUrl = `${apiEndpoints.PRODUCT_DETAILED.GET_PRODUCT_SPECIFICATIONS.url(
  ''
)}`;
const addToBasketUrl = `${apiEndpoints.BASKET.UPDATE_BASKET_ITEM.url}`;
const productDetailedUrl =
  apiEndpoints.PRODUCT_DETAILED.GET_PRODUCT_DETAILED.url;
const productAttributesUrl = `${
  apiEndpoints.PRODUCT_DETAILED.GET_CATEGORY_ATTRIBUTES.url
}?categoryId=1`;

describe('GoogleMap Service', () => {
  const mock = new MockAdapter(axios);

  it('fetchProductDetailsService success test', async () => {
    const response = {
      categoryName: '',
      variants: [],
      variantGroups: {}
    };
    const productId = '';
    const preselectedPreferences = false;

    const payloadData = getProductDetailsRequestTemplate(
      productId,
      null,
      preselectedPreferences,
      null,
      [],
      [],
      '',
      null,
      null,
      null,
      true,
      true
    );

    mock.onPost(productDetailedUrl).replyOnce(200, response);
    const result = await fetchProductDetailsService(payloadData);
    expect(result).toBeDefined();
  });

  it('fetchProductDetailsService error test', async () => {
    const productId = '';
    const url = apiEndpoints.PRODUCT_DETAILED.GET_PRODUCT_DETAILED.url;
    const preselectedPreferences = false;

    const payloadData = getProductDetailsRequestTemplate(
      productId,
      null,
      preselectedPreferences,
      null,
      [],
      [],
      '',
      null,
      null,
      null,
      false,
      true
    );

    mock.onPost(url).replyOnce(500, errorResponse);
    try {
      await fetchProductDetailsService(payloadData);
    } catch (e) {
      expect(e).toBeDefined();
    }
  });

  it('fetchCategoryAttributes success test', async () => {
    mock.onGet(productAttributesUrl).replyOnce(200, {});
    const result = await fetchCategoryAttributes('1');
    expect(result).toBeDefined();
  });

  // tslint:disable-next-line:no-identical-functions
  it('fetchCategoryAttributes error test', async () => {
    mock.onGet(productAttributesUrl).replyOnce(500, errorResponse);
    try {
      await fetchCategoryAttributes('1');
    } catch (e) {
      expect(e).toBeDefined();
    }
  });

  it('fetchProductSpecificationService success test', async () => {
    mock.onGet(productSpecificationUrl).replyOnce(200, {});
    const result = await fetchProductSpecificationService('');
    expect(result).toBeDefined();
  });

  // tslint:disable-next-line:no-identical-functions
  it('fetchProductSpecificationService error test', async () => {
    mock.onGet(productSpecificationUrl).replyOnce(500, errorResponse);
    try {
      await fetchProductSpecificationService('');
    } catch (e) {
      expect(e).toBeDefined();
    }
  });

  it('addToBasketService success test', async () => {
    mock.onPatch(addToBasketUrl).replyOnce(200, {});
    const result = await addToBasketService({ cartItems: [] });
    expect(result).toBeDefined();
  });

  // tslint:disable-next-line:no-identical-functions
  it('fetchProductSpecificationService error test', async () => {
    mock.onPatch(addToBasketUrl).replyOnce(500, errorResponse);
    try {
      await addToBasketService({ cartItems: [] });
    } catch (e) {
      expect(e).toBeDefined();
    }
  });

  it('jsonLD error test', async () => {
    const url = apiEndpoints.PRODUCT_DETAILED.GET_PRODUCT_DETAILED.url;

    mock.onPost(url).replyOnce(500, errorResponse);
    try {
      await jsonLD({
        variantId: 'string',
        categoryId: 'string'
      });
    } catch (e) {
      expect(e).toBeUndefined();
    }
  });
});
