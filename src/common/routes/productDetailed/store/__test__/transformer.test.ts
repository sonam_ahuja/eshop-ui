import * as transformers from '@productDetailed/store/transformer';
import appState from '@store/states/app';
import {
  installmentData,
  productDetailsResponse,
  productSpecificationResponse
} from '@src/common/mock-api/productDetailed/details.mock';
import { IInstallmentData } from '@productDetailed/store/types';
import { PRICE_TYPE } from '@productDetailed/store/enum';

describe('Transformer', () => {
  it('getProductHighlights ::', () => {
    const data = transformers.getProductHighlights(
      productSpecificationResponse
    );
    expect(data).toEqual(data);
  });
  it('getProductHighlights ::', () => {
    const data = transformers.getProductTechnicalSpecification(
      productSpecificationResponse
    );
    expect(data).toEqual(data);
  });

  it('sortAttachments ::', () => {
    const data = transformers.sortAttachments(productDetailsResponse);
    expect(data).toEqual(data);
  });

  it('getBuyDeviceOnly ::', () => {
    const data = transformers.getBuyDeviceOnly('124');
    expect(data).toEqual(data);
  });

  it('getBuyDeviceOnly :: null', () => {
    const data = transformers.getBuyDeviceOnly(null);
    expect(data).toEqual(data);
  });

  it('getBuyWithoutInstallments ::', () => {
    const params: IInstallmentData[] = installmentData;
    const data = transformers.getBuyWithoutInstallments(params);
    expect(data).toEqual(data);
  });

  it('getInstallments ::', () => {
    const data = transformers.getInstallments(
      productDetailsResponse,
      appState().translation.cart.productDetailed,
      appState().configuration.cms_configuration.modules.productDetailed
    );
    expect(data).toEqual(data);
  });

  it('getDeviceBasePrice ::', () => {
    const data = transformers.getDeviceBasePrice(
      PRICE_TYPE.BASE_PRICE,
      productDetailsResponse.variants[0].prices
    );
    expect(data).toEqual(data);
  });

  it('getInstallmentAmount ::', () => {
    const data = transformers.getInstallmentAmount(
      12,
      productDetailsResponse.variants[0].prices
    );
    expect(data).toEqual(data);
  });

  it('getDeviceBasePrice ::', () => {
    const data = transformers.getDeviceBasePrice(
      'upfront',
      productDetailsResponse.variants[0].prices
    );
    expect(data).toEqual(data);
  });

  it('getTariff ::', () => {
    const data = transformers.getTariff(
      productDetailsResponse,
      appState().translation.cart.productDetailed,
      false
    );
    expect(data).toEqual(data);
  });
});
