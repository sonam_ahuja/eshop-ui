import {
  getAddToBasketReqObject,
  getAttributesFromCharacteristics,
  getHideBuyDeviceOnly,
  getJsonLdRequest,
  getJSONLDRequestTemplate,
  getOutOfStockReason,
  getProductDetailedState,
  getProductDetailsRequestTemplate,
  getSelectedTariffAgreementInstalmentId,
  jsonLdProductDetials
} from '@productDetailed/store/utils';
import appState from '@store/states/app';
import { productDetailsResponse } from '@src/common/mock-api/productDetailed/details.mock';
import { Characteristics } from '@common/mock-api/productList/productList.mock';
import state from '@common/store/states/app';
import { tariff, totalPrices } from '@mocks/tariff/tariff.mock';
import { ITEM_STATUS } from '@src/common/store/enums';

describe('Utils', () => {
  it('getProductDetailsRequestTemplate', () => {
    const result = getProductDetailsRequestTemplate(
      '1',
      null,
      true,
      null,
      [],
      [],
      '',
      null,
      null,
      null,
      true,
      true
    );
    expect(result).toEqual(result);
  });
  it('getProductDetailsRequestTemplate with changed attribute', () => {
    const result = getProductDetailsRequestTemplate(
      '1',
      null,
      false,
      { name: 'Color', value: '#000' },
      [{ name: 'Color', value: '#000' }, { name: 'Color', value: '#fff' }],
      [],
      '',
      null,
      null,
      null,
      true,
      true
    );
    expect(result).toEqual(result);
  });
  it('getProductDetailedState', () => {
    const result = getProductDetailedState(state());
    expect(result).toEqual(result);
  });
  it('getAddToBasketReqObject', () => {
    const result = getAddToBasketReqObject({
      buyDeviceOnly: false,
      buyWithoutInstallments: true,
      productDetailsData: {
        productName: '',
        categoryName: '',
        linkedCategory: '',
        groups: {
          Colour: [
            {
              value: '',
              label: '',
              enabled: false,
              name: 'dummy'
            }
          ]
        },
        tariffs: tariff,
        totalPrices,
        variants: [
          {
            id: '',
            name: '',
            description: '',
            group: '',
            quantity: 1,
            characteristics: [
              {
                name: '',
                values: [{ value: 'value1' }]
              }
            ],
            attachments: {
              thumbnail: [{ type: '', url: '', name: '' }]
            },
            prices: [
              {
                priceType: 'string', // TODO: Different from contract is type
                actualValue: 1,
                discounts: [
                  {
                    discount: 23,
                    name: 'name',
                    dutyFreeValue: 34,
                    taxRate: 45,
                    label: '',
                    taxPercentage: 3
                  }
                ],
                discountedValue: 1,
                recurringChargePeriod: 'string'
              }
            ],
            tags: ['', 'asd'],
            unavailabilityReasonCodes: [ITEM_STATUS.IN_STOCK]
          }
        ]
      },
      considerStockForPreOrder: false
    });
    expect(result).toEqual(result);
  });

  it('getProductDetailedState', () => {
    const result = getProductDetailedState(state());
    expect(result).toEqual(result);
  });

  it('getOutOfStockReason', () => {
    const result = getOutOfStockReason(productDetailsResponse);
    expect(result).toEqual(result);
  });

  it('getAttributesFromCharacteristics', () => {
    const result = getAttributesFromCharacteristics(Characteristics, ['name']);
    expect(result).toEqual(result);
  });

  it('getJsonLdRequest', () => {
    const result = getJsonLdRequest();
    expect(getJsonLdRequest()).toEqual(result);
  });

  it('getSelectedTariffAgreementInstalmentId', () => {
    const result = getSelectedTariffAgreementInstalmentId(
      productDetailsResponse
    );
    expect(result).toEqual(result);
  });

  it('jsonLdProductDetials', () => {
    const result = jsonLdProductDetials(productDetailsResponse);
    expect(jsonLdProductDetials(productDetailsResponse)).toEqual(result);
  });

  it('getHideBuyDeviceOnly', () => {
    const result = getHideBuyDeviceOnly(appState().categories.categories);
    expect(getHideBuyDeviceOnly(appState().categories.categories)).toEqual(
      result
    );
  });

  it('getJSONLDRequestTemplate', () => {
    const result = getJSONLDRequestTemplate(
      1,
      2,
      'Mobile-Devices',
      'samsung',
      []
    );
    expect(
      getJSONLDRequestTemplate(1, 2, 'Mobile-Devices', 'samsung', [])
    ).toEqual(result);
  });
});
