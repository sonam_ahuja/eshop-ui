import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@productDetailed/store/constants';
import * as productDetailedAPI from '@common/types/api/productDetailed';
import * as categoryApi from '@src/common/types/api/categories';

import {
  IError,
  IFetchProductDetailsAction,
  IGetNotificationRequestTemplatePayload,
  INotificationData,
  IProductDetailResponse,
  ITariffDataPayload
} from './types';

export default {
  fetchProductDetailed: actionCreator<IFetchProductDetailsAction>(
    CONSTANTS.FETCH_PRODUCT_DETAIL_REQUESTED
  ),
  fetchProductDetailedLoading: actionCreator(
    CONSTANTS.FETCH_PRODUCT_DETAIL_LOADING
  ),
  fetchProductDetailedError: actionCreator<Error>(
    CONSTANTS.FETCH_PRODUCT_DETAIL_ERROR
  ),
  fetchProductDetailedSuccess: actionCreator<IProductDetailResponse>(
    CONSTANTS.FETCH_PRODUCT_DETAIL_SUCCESS
  ),

  addToBasket: actionCreator(CONSTANTS.ADD_TO_BASKET_REQUESTED),
  addToBasketLoading: actionCreator(CONSTANTS.ADD_TO_BASKET_LOADING),
  addToBasketSuccess: actionCreator(CONSTANTS.ADD_TO_BASKET_SUCCESS),
  addToBasketError: actionCreator<Error>(CONSTANTS.ADD_TO_BASKET_ERROR),

  fetchProductSpecification: actionCreator<string>(
    CONSTANTS.FETCH_PRODUCT_SPECIFICATION_REQUESTED
  ),
  fetchProductSpecificationSuccess: actionCreator<
    productDetailedAPI.GET_PRODUCT_SPECIFICATION.IResponse
  >(CONSTANTS.FETCH_PRODUCT_SPECIFICATION_SUCCESS),
  fetchProductHighlightSuccess: actionCreator<
    productDetailedAPI.GET_PRODUCT_SPECIFICATION.IResponse
  >(CONSTANTS.FETCH_PRODUCT_HIGHLIGHT_SUCCESS),
  fetchProductSpecificationError: actionCreator<Error>(
    CONSTANTS.FETCH_PRODUCT_SPECIFICATION_ERROR
  ),
  fetchProductSpecificationLoading: actionCreator(
    CONSTANTS.FETCH_PRODUCT_SPECIFICATION_LOADING
  ),

  setCategoryAttributes: actionCreator<{
    categorySlug: string;
    attributes: string[];
  }>(CONSTANTS.SET_CATEGORY_ATTRIBUTES),
  setError: actionCreator<IError>(CONSTANTS.SET_ERROR),
  openModal: actionCreator<boolean>(CONSTANTS.OPEN_MODAL),
  setNotificationData: actionCreator<INotificationData>(
    CONSTANTS.SET_NOTIFICATION_DATA
  ),
  sendStockNotification: actionCreator<IGetNotificationRequestTemplatePayload>(
    CONSTANTS.SEND_STOCK_NOTIFICATION
  ),
  sendStockNotificationError: actionCreator<Error>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_ERROR
  ),
  sendStockNotificationSuccess: actionCreator<void>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_SUCCESS
  ),
  sendStockNotificationLoading: actionCreator<void>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_LOADING
  ),
  setProductVariantCategoryId: actionCreator<{
    productId: string;
    variantId: string;
    categoryId: string;
  }>(CONSTANTS.SET_PRODUCT_VARIANT_CATEGORY_ID),
  setAppShell: actionCreator<boolean>(CONSTANTS.SET_APP_SHELL),
  setImageAppShell: actionCreator<boolean>(CONSTANTS.SET_IMAGE_APP_SHELL),
  setCategoryData: actionCreator<categoryApi.GET.IResponse>(
    CONSTANTS.SET_CATEGORIES_DATA
  ),
  changeBinkiesFallbackVisible: actionCreator<boolean>(
    CONSTANTS.CHANGE_BINKIES_FALL_BACK
  ),
  hideFallBackImage: actionCreator<boolean>(CONSTANTS.IMAGE_HIDE),
  setTariffData: actionCreator<ITariffDataPayload>(CONSTANTS.SET_TARIFF_DATA),
  validateAgeLimit: actionCreator<string>(CONSTANTS.VALIDATE_AGE_LIMIT),
  checkYoungTariff: actionCreator<string>(CONSTANTS.CHECK_YOUNG_TARIFF),
  ageLimitSuccess: actionCreator<void>(CONSTANTS.AGE_LIMIT_SUCCESS),
  ageLimitError: actionCreator<void>(CONSTANTS.AGE_LIMIT_ERROR),
  ageLimit: actionCreator<void>(CONSTANTS.AGE_LIMIT_CLOSE_MODAL),
  showAllPlans: actionCreator<void>(CONSTANTS.SHOW_ALL_PLANS),
  setCMSBuyDeviceOnly: actionCreator<boolean>(CONSTANTS.HIDE_BUY_DEVICE_ONLY)
};
