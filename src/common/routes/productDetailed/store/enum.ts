export enum NOTIFY_ME_REASON {
  COMING_SOON = 'COMING_SOON',
  OUT_OF_STOCK = 'OUT_OF_STOCK'
}

export enum NOTIFY_ME_CONTACT_TYPE {
  EMAIL = 'email',
  PHONE = 'phone',
  MOBILE = 'mobile'
}

export enum NOTIFICATION_MEDIUM {
  EMAIL = 'email',
  PHONE = 'mobile'
}

export enum MODAL_TYPE {
  CONFIRMATION_MODAL = 'CONFIRMATION_MODAL',
  SEND_NOTIFICATION_MODAL = 'SEND_NOTIFICATION_MODAL'
}

/** USED ON PROLONGATION */
export enum PRICE_TYPE {
  BASE_PRICE = 'basePrice',
  UPFRONT_PRICE = 'upfrontPrice',
  RECURRING_FEE = 'recurringFee'
}

export enum DROPDOWN_TYPE {
  INSTALLMENTS = 'numberOfInstalments',
  LOYALTY = 'agreement',
  TARIFF = 'tariff'
}

export enum PRODUCT_ATTRIBUTES {
  STORAGE = 'storage',
  COLOUR = 'colour',
  PLAN = 'plan'
}

export const DEVICE_PRICE_ALTERATION_ID = 'priceAlterationId';
export const DEVICE_DISCOUNT_ID = 'discountId';
