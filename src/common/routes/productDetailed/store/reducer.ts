import { AnyAction, Reducer } from 'redux';
import { StateType } from 'typesafe-actions';
import {
  IError,
  IGetNotificationRequestTemplatePayload,
  IInstallmentData,
  IKeyValue,
  INotificationData,
  IProductDetailedData,
  IProductDetailedState,
  IProductSpecification,
  ITariffDataPayload,
  ITariffDetail
} from '@productDetailed/store/types';
import CONSTANTS from '@productDetailed/store/constants';
import initialState from '@productDetailed/store/state';
import withProduce from '@utils/withProduce';
import { MODAL_TYPE } from '@productList/store/enum';
import { sortAttachments } from '@productDetailed/store/transformer';
import { ICategory } from '@category/store/types';

const reducers = {
  [CONSTANTS.SET_PRODUCT_VARIANT_CATEGORY_ID]: (
    state: IProductDetailedState,
    payload: {
      productId: string;
      variantId: string;
      categoryId: string;
    }
  ) => {
    const { productId, variantId, categoryId } = payload;
    state.productId = productId;
    state.variantId = variantId;
    state.categoryId = categoryId;
  },
  [CONSTANTS.FETCH_PRODUCT_DETAIL_LOADING]: (state: IProductDetailedState) => {
    state.loading = true;
    state.error = null;
  },
  [CONSTANTS.FETCH_PRODUCT_DETAIL_ERROR]: (
    state: IProductDetailedState,
    payload: Error
  ) => {
    state.error = payload;
    state.loading = false;
  },
  [CONSTANTS.FETCH_PRODUCT_DETAIL_SUCCESS]: (
    state: IProductDetailedState,
    payload: {
      productDetailsData: IProductDetailedData;
      selectedAttributes: IKeyValue[];
      installmentGroup: IInstallmentData[];
      tariffGroup: ITariffDetail[];
      instalmentId: string | null;
      tariffId: string | null;
      agreementId: string | null;
      buyDeviceOnly: boolean;
      buyWithoutInstallments: boolean;
    }
  ) => {
    const {
      productDetailsData,
      selectedAttributes,
      installmentGroup,
      tariffGroup,
      instalmentId,
      tariffId,
      agreementId,
      buyDeviceOnly,
      buyWithoutInstallments
    } = payload;
    state.loading = false;
    state.error = null;
    state.showImageAppShell = false;
    state.productDetailsData = sortAttachments(productDetailsData);
    state.showAppShell = false;
    state.selectedAttributes = selectedAttributes;
    state.installmentDropdownData = installmentGroup;
    state.tariffId = tariffId;
    state.instalmentId = instalmentId;
    state.agreementId = agreementId;
    state.tariffDropdownData = tariffGroup;
    state.buyDeviceOnly = buyDeviceOnly;
    state.buyWithoutInstallments = buyWithoutInstallments;
  },
  [CONSTANTS.FETCH_PRODUCT_SPECIFICATION_LOADING]: (
    state: IProductDetailedState
  ) => {
    state.productSpecificationLoading = true;
    state.productSpecificationError = null;
  },
  [CONSTANTS.FETCH_PRODUCT_SPECIFICATION_ERROR]: (
    state: IProductDetailedState,
    payload: Error
  ) => {
    state.productSpecificationLoading = false;
    state.productSpecificationError = payload;
  },
  [CONSTANTS.FETCH_PRODUCT_SPECIFICATION_SUCCESS]: (
    state: IProductDetailedState,
    payload: IProductSpecification[]
  ) => {
    state.productSpecificationLoading = false;
    state.productSpecificationError = null;
    state.productSpecificationData = payload;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_LOADING]: (
    state: IProductDetailedState
  ) => {
    state.notificationLoading = true;
    state.notificationError = null;
  },
  [CONSTANTS.FETCH_PRODUCT_HIGHLIGHT_SUCCESS]: (
    state: IProductDetailedState,
    payload: IProductSpecification[]
  ) => {
    state.productHighlightsData = payload;
  },
  [CONSTANTS.SET_CATEGORY_ATTRIBUTES]: (
    state: IProductDetailedState,
    payload: {
      categorySlug: string;
      attributes: string[];
    }
  ) => {
    const { attributes } = payload;
    state.categoryAttributesMap = attributes;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION]: (
    state: IProductDetailedState,
    payload: IGetNotificationRequestTemplatePayload
  ) => {
    state.notificationData.mediumValue = payload.mediumValue;
    state.notificationData.type = payload.type;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_SUCCESS]: (
    state: IProductDetailedState
  ) => {
    state.notificationModal = MODAL_TYPE.CONFIRMATION_MODAL;
    state.notificationLoading = false;
    state.notificationError = null;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_ERROR]: (
    state: IProductDetailedState,
    error: Error | null
  ) => {
    state.notificationLoading = false;
    state.notificationError = error;
  },
  [CONSTANTS.SET_ERROR]: (state: IProductDetailedState, error: IError) => {
    state.error = error;
  },
  [CONSTANTS.OPEN_MODAL]: (
    state: IProductDetailedState,
    isModalOpen: boolean
  ) => {
    state.openModal = isModalOpen;
    state.error = null;
    state.notificationModal = isModalOpen
      ? state.notificationModal
      : MODAL_TYPE.SEND_NOTIFICATION_MODAL;
  },
  [CONSTANTS.SET_NOTIFICATION_DATA]: (
    state: IProductDetailedState,
    notificationData: INotificationData
  ) => {
    state.notificationData = notificationData;
  },
  [CONSTANTS.SET_APP_SHELL]: (
    state: IProductDetailedState,
    showAppShell: boolean
  ) => {
    state.showAppShell = showAppShell;
  },
  [CONSTANTS.ADD_TO_BASKET_LOADING]: (state: IProductDetailedState) => {
    state.addToBasketLoading = true;
    state.error = null;
  },
  [CONSTANTS.ADD_TO_BASKET_SUCCESS]: (state: IProductDetailedState) => {
    state.addToBasketLoading = false;
  },
  [CONSTANTS.ADD_TO_BASKET_ERROR]: (
    state: IProductDetailedState,
    payload: IError
  ) => {
    state.addToBasketLoading = false;
    state.error = payload;
  },
  [CONSTANTS.SET_IMAGE_APP_SHELL]: (
    state: IProductDetailedState,
    payload: boolean
  ) => {
    state.showImageAppShell = payload;
  },
  [CONSTANTS.SET_CATEGORIES_DATA]: (
    state: IProductDetailedState,
    payload: ICategory
  ) => {
    const categories = payload;
    state.theme = categories.theme;
    state.parentCategorySlug = categories.parentSlug as string;
    state.categoryName = categories.name;
  },
  [CONSTANTS.CHANGE_BINKIES_FALL_BACK]: (
    state: IProductDetailedState,
    payload: boolean
  ) => {
    state.binkiesFallbackVisible = payload;
  },
  [CONSTANTS.IMAGE_HIDE]: (state: IProductDetailedState, payload: boolean) => {
    state.binkiesFallbackVisible = payload;
  },
  [CONSTANTS.SET_TARIFF_DATA]: (
    state: IProductDetailedState,
    payload: ITariffDataPayload
  ) => {
    const { tariffId, agreementId, instalmentId, discountId } = payload;
    state.tariffId = tariffId;
    state.agreementId = agreementId;
    state.instalmentId = instalmentId;
    state.discountId = discountId;
  },
  [CONSTANTS.CHECK_YOUNG_TARIFF]: (
    state: IProductDetailedState,
    payload: string
  ) => {
    state.youngTariffId = payload;
    state.showYoungTariff = true;
    state.showYoungTariffErrorMessage = false;
  },
  [CONSTANTS.AGE_LIMIT_SUCCESS]: (state: IProductDetailedState) => {
    state.showYoungTariff = false;
  },
  [CONSTANTS.AGE_LIMIT_ERROR]: (state: IProductDetailedState) => {
    state.showYoungTariffErrorMessage = true;
  },
  [CONSTANTS.AGE_LIMIT_CLOSE_MODAL]: (state: IProductDetailedState) => {
    state.showYoungTariff = false;
    state.youngTariffId = '';
    state.error = null;
  },
  [CONSTANTS.HIDE_BUY_DEVICE_ONLY]: (
    state: IProductDetailedState,
    payload: boolean
  ) => {
    state.hideBuyDeviceOnly = payload;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IProductDetailedState,
  AnyAction
>;

export type ProductDetailedState = StateType<typeof withProduce>;
