import { isMobile, logError } from '@common/utils';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import {
  createQueryParams,
  getBinkiesID,
  getProdDetailTranslation,
  getProductDetailConfiguration
} from '@productDetailed/utils';
import * as productDetailedApi from '@common/types/api/productDetailed';
import * as tariffApi from '@common/types/api/tariff';
import CONSTANTS from '@productDetailed/store/constants';
import actions from '@productDetailed/store/actions';
import tariffActions from '@tariff/store/actions';
import {
  addToBasketService,
  checkBinkiesContentService,
  fetchCategoryAttributes,
  fetchProductDetailsService,
  fetchProductSpecificationService
} from '@productDetailed/store/services';
import { sendStockNotificationService } from '@productList/store/services';
import {
  getHideBuyDeviceOnly,
  getProductCatalogConfiguration,
  getSelectedTariffAgreementInstalmentId
} from '@productDetailed/store/utils';
import { fetchCategoriesService } from '@category/store/services';
import { transformCategoriesResponse } from '@category/store/transformer';
import {
  getBuyDeviceOnly,
  getBuyWithoutInstallments,
  getInstallments,
  getProductHighlights,
  getProductTechnicalSpecification,
  getTariff
} from '@productDetailed/store/transformer';
import { BIRTH_DATE_LIMIT } from '@src/common/constants/appConstants';
import moment from 'moment';
import { IWindow } from '@src/client/index';
import basketActions from '@basket/store/actions';
import {
  IBasket as IBasketTranslation,
  IProductDetailedTranslation
} from '@src/common/store/types/translation';
import {
  ICatalogConfiguration,
  IProductDetailedConfiguration
} from '@src/common/store/types/configuration';
import { saveTariffService } from '@tariff/store/services';
import * as categoryApi from '@src/common/types/api/categories';
import history from '@client/history';
import { sendProductDetailedViewEvent } from '@src/common/events/DeviceDetail';
import { sendOnBasketLoadEvent, sendonBasketLoadTrigger } from '@events/basket';
import { getState } from '@tariff/store/utils';
import { getBasketTranslation } from '@src/common/store/common';
import ROUTES_CONSTANTS from '@src/common/constants/routes';
import { BFF_ERROR_CODE } from '@src/common/components/Error/constants';

import {
  IAddonMap,
  IAddons,
  ICharacteristic,
  IFetchProductDetailsAction,
  IGetNotificationRequestTemplatePayload,
  IInstallmentData,
  IKeyValue,
  IProductDetailedState,
  ITariffDetail
} from './types';
import {
  getAddToBasketReqObject,
  getAttributesFromCharacteristics,
  getOutOfStockReason,
  getProductDetailedState,
  getProductDetailsRequestTemplate
} from './utils';

// tslint:disable-next-line:no-commented-code
// import basketActions from '@basket/store/actions';

export function* saveTariffPlan(payload: tariffApi.POST.IRequest): Generator {
  // TODO: Hardcoding category slug only for MVP,
  // it w;ill rem;ove after link;ing; of categories in the sales catalog
  try {
    yield saveTariffService(payload);
  } catch (error) {
    logError(error);
  }
}

// tslint:disable-next-line: cognitive-complexity
export function* updateUrlAndSaveTariff(payload: {
  productDetailsData: productDetailedApi.POST.IResponse;
  actionPayload: IFetchProductDetailsAction;
  state: IProductDetailedState;
}): Generator {
  if (history) {
    const {
      productDetailsData,
      actionPayload: { changedAttribute, savePlan },
      state
    } = payload;

    const { categoryId, productId, discountId } = state;

    const {
      instalmentId: updatedInstalmentId,
      tariffId: updatedTariffId,
      agreementId: updatedAgreementId
    } = getSelectedTariffAgreementInstalmentId(productDetailsData);

    const variantName = productDetailsData.variants[0].name;
    const id = productDetailsData.variants[0].id;
    let queryParam = '';
    if (changedAttribute) {
      queryParam += `${createQueryParams(changedAttribute)}`;
    }
    if (updatedTariffId) {
      queryParam += `&tariffId=${updatedTariffId}`;
    }
    if (updatedAgreementId) {
      queryParam += `&agreementId=${updatedAgreementId}`;
    }
    if (updatedInstalmentId) {
      queryParam += `&instalmentId=${updatedInstalmentId}`;
    }
    // discount will be updated based on the response from api
    let updatedDiscountId = discountId;
    // no loyality case
    let selectedProductOfferingTerm = '0';

    let addons: IAddonMap = {};

    if (productDetailsData.tariffs.length > 0) {
      const selectedTariff = productDetailsData.tariffs.find(tariff => {
        return tariff.id === updatedTariffId;
      });
      if (selectedTariff) {
        const discount = selectedTariff.groups.productOfferingBenefit.find(
          discountItem => {
            return !!discountItem.isSelected;
          }
        );
        addons = getSelectedAddon(selectedTariff.addons);
        const productOfferingTerm = selectedTariff.groups.productOfferingTerm.find(
          productOfferingTermItem => {
            return !!productOfferingTermItem.isSelected;
          }
        );
        if (discount) {
          updatedDiscountId = discount.value;
        }
        if (productOfferingTerm) {
          selectedProductOfferingTerm = productOfferingTerm.value;
        }
      }
    }
    if (updatedDiscountId) {
      queryParam += `&discountId=${updatedDiscountId}`;
    }
    const updatedPathName = `/${categoryId}/${variantName}/${productId}/${id}?${encodeURIComponent(
      queryParam
    )}`;
    if (history.location.pathname !== updatedPathName) {
      const loginStep = new URLSearchParams(
        decodeURIComponent(history.location.search)
      ).get('loginStep');
      if (loginStep) {
        queryParam += `&loginStep=${loginStep}`;
      }
      history.replace({
        pathname: `/${categoryId}/${variantName}/${productId}/${id}`,
        search: encodeURIComponent(queryParam)
      });
    }

    /*** NOW Preference call need to be made */
    if (!getBuyDeviceOnly(updatedTariffId) && savePlan) {
      yield call(saveTariffPlan, {
        category: productDetailsData.linkedCategory,
        selectedProductOfferingTerm,
        selectedTariff: updatedTariffId,
        discount: updatedDiscountId,
        addons
      });
    }
  }
}

const getSelectedAddon = (addons: IAddons[]) => {
  const addonArr = {};
  if (addons && addons.length) {
    addons.forEach((addon: IAddons) => {
      if (addon.categoryId) {
        if (!addonArr[addon.categoryId]) {
          addonArr[addon.categoryId] = [];
        }
        addonArr[addon.categoryId].push(addon.id);
      }
    });
  }

  return addonArr;
};

// tslint:disable-next-line:cognitive-complexity
export function* fetchProductDetails(action: {
  type: string;
  payload: IFetchProductDetailsAction;
}): Generator {
  try {
    const state: IProductDetailedState = yield select(getProductDetailedState);
    const prodDetailTranslation: IProductDetailedTranslation = yield select(
      getProdDetailTranslation
    );
    const productDetailConfiguration: IProductDetailedConfiguration = yield select(
      getProductDetailConfiguration
    );
    const {
      payload: {
        preselectedPreferences,
        variantId,
        changedAttribute,
        fetchCategoryData,
        showFullPageError
      }
    } = action;
    yield put(actions.fetchProductDetailedLoading());
    const {
      categoryAttributesMap,
      selectedAttributes,
      categoryId,
      productId,
      // state tariff
      tariffId,
      instalmentId,
      agreementId,
      discountId
    } = state;
    // check if we have attributes or make an api call
    if (!categoryAttributesMap) {
      const categoryAttributes = yield fetchCategoryAttributes(
        categoryId as string
      );
      // set product attributes in store
      yield put(
        actions.setCategoryAttributes({
          categorySlug: categoryId as string,
          attributes: categoryAttributes
        })
      );
    }

    let payloadSelectedAttributes: IKeyValue[] = selectedAttributes;

    if (changedAttribute) {
      payloadSelectedAttributes = selectedAttributes.filter(
        selectedAttribute => selectedAttribute.name !== changedAttribute.name
      );
    }

    payloadSelectedAttributes = [...payloadSelectedAttributes];
    if (instalmentId !== null) {
      let numberOfInstalments = '0';
      if (productDetailConfiguration.buyDeviceWithInstallments) {
        numberOfInstalments = instalmentId ? instalmentId : '0';
      }
      payloadSelectedAttributes = [
        ...payloadSelectedAttributes,
        {
          name: 'numberOfInstalments',
          value: numberOfInstalments
        }
      ];
    }
    // check if changedAttribute exist otherwise send variantId in request
    const payloadData = getProductDetailsRequestTemplate(
      productId as string,
      variantId,
      preselectedPreferences,
      changedAttribute,
      payloadSelectedAttributes,
      categoryAttributesMap,
      categoryId as string,
      tariffId,
      agreementId,
      discountId,
      !!getBuyDeviceOnly(tariffId),
      productDetailConfiguration.productDetailedTemplate1.showTariffInfo
    );

    const sagas = [];
    sagas.push(
      call(fetchProductDetailsService, {
        ...payloadData,
        showFullPageError: !!showFullPageError
      })
    );
    if (fetchCategoryData) {
      sagas.push(call(fetchCategoriesService, categoryId as string));
    }
    const results = yield all(sagas);
    const productDetailsData: productDetailedApi.POST.IResponse = results[0];
    // transformer call utils
    if (fetchCategoryData) {
      const categories: categoryApi.GET.IResponse = results[1];
      yield put(
        actions.setCategoryData(transformCategoriesResponse(categories))
      );
    }
    // defaults to value already in state
    const updatedInstallmentGroup: IInstallmentData[] = getInstallments(
      productDetailsData,
      prodDetailTranslation,
      productDetailConfiguration
    );

    const updatedTariffDetails = getTariff(
      productDetailsData,
      prodDetailTranslation,
      fetchCategoryData
        ? getHideBuyDeviceOnly(results[1])
        : state.hideBuyDeviceOnly
    );

    const {
      instalmentId: updatedInstalmentId,
      tariffId: updatedTariffId,
      agreementId: updatedAgreementId
    } = getSelectedTariffAgreementInstalmentId(productDetailsData);

    const tariffState = yield select(getState);
    if (
      tariffState.selectedTariffId !== updatedTariffId &&
      updatedTariffId !== '-1'
    ) {
      yield put(tariffActions.setTariffId(updatedTariffId));
    }

    let updatedSelectedAttributes: IKeyValue[] = payloadSelectedAttributes;
    if (productDetailsData.variants.length > 0) {
      updatedSelectedAttributes = getAttributesFromCharacteristics(
        productDetailsData.variants[0].characteristics,
        categoryAttributesMap
      );
    }

    yield put(
      actions.fetchProductDetailedSuccess({
        productDetailsData,
        selectedAttributes: updatedSelectedAttributes,
        installmentGroup: updatedInstallmentGroup,
        tariffGroup: updatedTariffDetails,
        instalmentId: updatedInstalmentId,
        tariffId: updatedTariffId,
        agreementId: updatedAgreementId,
        buyDeviceOnly: !!getBuyDeviceOnly(updatedTariffId),
        buyWithoutInstallments: !!getBuyWithoutInstallments(
          updatedInstallmentGroup
        )
      })
    );

    const binkiesID: string | undefined = getBinkiesID(
      productDetailsData.variants &&
        productDetailsData.variants[0] &&
        productDetailsData.variants[0].characteristics
        ? productDetailsData.variants[0].characteristics
        : []
    );
    (window as IWindow).binkies.contentIdentifier = null;
    if (binkiesID !== null && binkiesID !== undefined) {
      try {
        const binkiesResponse = yield checkBinkiesContentService(binkiesID);
        yield put(actions.changeBinkiesFallbackVisible(binkiesResponse === -1));
      } catch (error) {
        yield put(actions.changeBinkiesFallbackVisible(true));
      }
    } else {
      yield put(actions.changeBinkiesFallbackVisible(true));
    }

    (window as IWindow).binkies.contentIdentifier = binkiesID || null;

    sendProductDetailedViewEvent(productDetailsData);
    yield call(updateUrlAndSaveTariff, {
      productDetailsData,
      actionPayload: action.payload,
      state
    });
  } catch (error) {
    logError(error);
    yield put(actions.fetchProductDetailedError(error));
  }
}

export function* fetchProductSpecification(action: {
  type: string;
  payload: productDetailedApi.GET_PRODUCT_SPECIFICATION.IRequest;
}): Generator {
  try {
    const { payload } = action;
    yield put(actions.fetchProductSpecificationLoading());
    const productSpecification: productDetailedApi.GET_PRODUCT_SPECIFICATION.IResponse = yield fetchProductSpecificationService(
      payload
    );
    const productSpecificationData = getProductTechnicalSpecification(
      productSpecification
    );
    const productHighlightsData = getProductHighlights(productSpecification);
    yield put(
      actions.fetchProductSpecificationSuccess(productSpecificationData)
    );
    yield put(actions.fetchProductHighlightSuccess(productHighlightsData));
  } catch (error) {
    yield put(actions.fetchProductSpecificationError(error));
  }
}

export function* sendStockNotification(action: {
  type: string;
  payload: IGetNotificationRequestTemplatePayload;
}): Generator {
  const { payload } = action;
  yield put(actions.sendStockNotificationLoading());

  const state: IProductDetailedState = yield select(getProductDetailedState);
  const { productDetailsData } = state;

  const reason = getOutOfStockReason(productDetailsData);

  try {
    yield sendStockNotificationService(payload, reason);
    yield put(actions.sendStockNotificationSuccess());
  } catch (error) {
    yield put(actions.sendStockNotificationError(error));
  }
}

export function* validateAgeLimit(action: {
  type: string;
  payload: string;
}): Generator {
  const state: IProductDetailedState = yield select(getProductDetailedState);
  const { tariffDropdownData, youngTariffId } = state;

  const selectedTariff = tariffDropdownData.find((item: ITariffDetail) => {
    return item.id === youngTariffId;
  });

  const selectedCharacteristic =
    selectedTariff &&
    selectedTariff.characteristics.find((item: ICharacteristic) => {
      return item.name === BIRTH_DATE_LIMIT;
    });

  let fromDate = new Date();
  let toDate = new Date();

  if (selectedCharacteristic) {
    fromDate = new Date(selectedCharacteristic.values[0].valueFrom as string);
    toDate = new Date(selectedCharacteristic.values[0].valueTo as string);
  }

  try {
    const enteredData = new Date(action.payload);
    const isBetweenBool = moment(enteredData).isBetween(
      fromDate,
      toDate,
      'seconds',
      '()'
    );

    if (isBetweenBool) {
      yield put(actions.addToBasket());
      yield put(actions.ageLimit());
    } else {
      yield put(actions.ageLimitError());
    }
  } catch (error) {
    yield put(actions.sendStockNotificationError(error));
  }
}

export function* ageLimitSuccess(): Generator {
  const state: IProductDetailedState = yield select(getProductDetailedState);
  const {
    youngTariffId: tariffId,
    agreementId,
    instalmentId,
    discountId
  } = state;

  try {
    const preselectedPreferences = false;
    const variantId = null;
    const changedAttribute = null;
    const fetchCategoryData = false;

    yield put(
      actions.setTariffData({ tariffId, agreementId, instalmentId, discountId })
    );
    yield put(
      actions.fetchProductDetailed({
        preselectedPreferences,
        variantId,
        changedAttribute,
        fetchCategoryData,
        savePlan: false
      })
    );
  } catch (error) {
    yield put(actions.sendStockNotificationError(error));
  }
}

export function* addToBasket(): Generator {
  try {
    const state: IProductDetailedState = yield select(getProductDetailedState);
    const catalogConfiguration: ICatalogConfiguration = yield select(
      getProductCatalogConfiguration
    );
    const basketTranslation: IBasketTranslation = yield select(
      getBasketTranslation
    );
    const priceChangedMessage: string = basketTranslation.priceChanged;
    const { productDetailsData, buyDeviceOnly, buyWithoutInstallments } = state;
    yield put(actions.addToBasketLoading());
    const requestObj = getAddToBasketReqObject({
      buyDeviceOnly,
      buyWithoutInstallments,
      productDetailsData,
      considerStockForPreOrder: catalogConfiguration.preorder.considerInventory
    });
    if (requestObj) {
      const {
        cartItems,
        cartSummary,
        totalItems,
        priceChanged,
        code
      } = yield addToBasketService(requestObj);
      if (code && code === BFF_ERROR_CODE.DT_DEVICE_TARIFF_RESTRICTION) {
        yield put(basketActions.setCartRestrictionMessage(true));
        yield put(actions.addToBasketSuccess());
        if (history) {
          history.push(ROUTES_CONSTANTS.BASKET);

          return;
        }
      }
      yield put(
        basketActions.setBasketData({ cartItems, cartSummary, totalItems })
      );
      yield put(
        basketActions.setPriceNotificationStrip({
          show: priceChanged,
          notification: priceChangedMessage
        })
      );
      yield put(basketActions.setCheckoutButtonStatus(cartItems));
      yield put(basketActions.setCartRestrictionMessage(false));
    }
    yield put(actions.addToBasketSuccess());
    yield put(basketActions.setCartRestrictionMessage(true));

    if (!(isMobile.phone || isMobile.tablet)) {
      sendOnBasketLoadEvent();
      sendonBasketLoadTrigger();
      yield put(basketActions.showBasketSideBar(true));
    } else {
      if (history) {
        history.push('/basket');
        document.documentElement.scrollTop = 0;
      }
    }
  } catch (error) {
    yield put(actions.addToBasketError(error));
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(
    CONSTANTS.FETCH_PRODUCT_DETAIL_REQUESTED,
    fetchProductDetails
  );
  yield takeLatest(
    CONSTANTS.FETCH_PRODUCT_SPECIFICATION_REQUESTED,
    fetchProductSpecification
  );
  yield takeLatest(CONSTANTS.SEND_STOCK_NOTIFICATION, sendStockNotification);
  yield takeLatest(CONSTANTS.VALIDATE_AGE_LIMIT, validateAgeLimit);
  yield takeLatest(CONSTANTS.ADD_TO_BASKET_REQUESTED, addToBasket);
  yield takeLatest(CONSTANTS.AGE_LIMIT_SUCCESS, ageLimitSuccess);
}

// tslint:disable-next-line: max-file-line-count
export default watcherSaga;
