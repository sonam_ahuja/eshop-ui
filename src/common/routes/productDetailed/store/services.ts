import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import * as productDetailedApi from '@common/types/api/productDetailed';
import { logError } from '@src/common/utils';
import { getJSONLDRequestTemplate } from '@productDetailed/store/utils';
import CONSTANTS from '@common/constants/appConstants';

export const fetchCategoryAttributes = async (
  payload: productDetailedApi.GET_CATEGORY_ATTRIBUTES.IRequest
): Promise<productDetailedApi.GET_CATEGORY_ATTRIBUTES.IResponse | Error> => {
  try {
    return await apiCaller.get(
      `${
        apiEndpoints.PRODUCT_DETAILED.GET_CATEGORY_ATTRIBUTES.url
      }?categoryId=${payload}`
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchProductDetailsService = async (
  payload: productDetailedApi.POST.IRequest
): Promise<productDetailedApi.POST.IResponse | Error> => {
  try {
    return await apiCaller.post(
      apiEndpoints.PRODUCT_DETAILED.GET_PRODUCT_DETAILED.url,
      payload,
      { showFullPageError: !!payload.showFullPageError }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchProductSpecificationService = async (
  payload: productDetailedApi.GET_PRODUCT_SPECIFICATION.IRequest
): Promise<productDetailedApi.GET_PRODUCT_SPECIFICATION.IResponse> => {
  try {
    return await apiCaller.get(
      `${apiEndpoints.PRODUCT_DETAILED.GET_PRODUCT_SPECIFICATIONS.url(payload)}`
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const addToBasketService = async (
  payload: productDetailedApi.ADD_TO_BASKET.IRequest
): Promise<productDetailedApi.ADD_TO_BASKET.IResponse> => {
  try {
    return await apiCaller.patch(
      apiEndpoints.BASKET.UPDATE_BASKET_ITEM.url,
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const jsonLD = async (
  payload: productDetailedApi.JSON_LD.IRequest
  // tslint:disable-next-line: no-any
): Promise<any> => {
  const { categoryId, variantId } = payload;
  const baseURL = CONSTANTS.ESHOP_BASE_URL;
  const requestBody = getJSONLDRequestTemplate(0, 1, categoryId, variantId, []);
  try {
    return await apiCaller.post(
      `${baseURL}/${apiEndpoints.PRODUCT_DETAILED.GET_JSON_LD.url}`,
      requestBody
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const checkBinkiesContentService = async (
  binkiesId: string
): Promise<number> => {
  return new Promise((resolve, reject) => {
    const url = `https://embed.binki.es/contentavailable/kxRQOYWn/${binkiesId.toLocaleLowerCase()}`;

    const xhttp = new XMLHttpRequest();

    // tslint:disable-next-line:typedef
    xhttp.onreadystatechange = function() {
      // tslint:disable-next-line:no-invalid-this
      if (this.readyState === 4) {
        // tslint:disable-next-line:no-invalid-this
        if (this.status === 204) {
          return resolve(1);
        }
        // tslint:disable-next-line:no-invalid-this
        if (this.status === 404) {
          return reject(-1);
        }

        return reject(-1);
      }
    };

    xhttp.open('GET', url, true);
    xhttp.send();
  });
};
