import { IProductDetailedState } from '@productDetailed/store/types';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';
import { PAGE_THEME } from '@common/store/enums/index';

export default (): IProductDetailedState => ({
  productId: null,
  categoryId: null,
  variantId: null,
  discountId: null,
  tariffId: null,
  instalmentId: null,
  agreementId: null,
  numberOfInstalments: null,
  buyDeviceOnly: false,
  buyWithoutInstallments: true,
  categoryAttributesMap: [],
  selectedAttributes: [],
  showImageAppShell: true,
  productDetailsData: {
    categoryName: '',
    productName: '',
    variants: [],
    linkedCategory: '',
    tariffs: [],
    groups: {},
    totalPrices: []
  },
  receivedProductDetailedData: false,
  // outOfStock: false,
  loading: false,
  addToBasketLoading: false,
  notifyMeContactInfo: '',
  showNotifyMeSuccessScreen: false,
  // notifyMeLoading: false,
  // notifyMeError: null,
  productSpecificationData: [],
  productHighlightsData: [],
  productSpecificationError: null,
  productSpecificationLoading: false,
  notificationLoading: false,
  notificationError: null,
  showAppShell: true,
  notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL,
  notificationData: {
    variantId: '',
    deviceName: '',
    mediumValue: '',
    reason: '',
    type: NOTIFICATION_MEDIUM.PHONE
  },
  openModal: false,
  error: {
    code: 0,
    message: ''
  },
  theme: PAGE_THEME.PRIMARY,
  parentCategorySlug: '',
  categoryName: '',
  binkiesFallbackVisible: false,
  installmentDropdownData: [],
  tariffDropdownData: [],
  showYoungTariff: false,
  youngTariffId: '',
  showYoungTariffErrorMessage: false,
  hideBuyDeviceOnly: false
});
