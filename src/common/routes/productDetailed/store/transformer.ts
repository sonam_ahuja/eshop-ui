import * as productDetailedApi from '@common/types/api/productDetailed';
import {
  IGroup1,
  IInstallmentData,
  ILoyaltyAgreement,
  IProductDetailedData,
  IProductSpecification,
  ITariffDetail
} from '@productDetailed/store/types';
import { IPrices } from '@tariff/store/types';
import htmlKeys from '@common/constants/appkeys';
import { IMAGE_POSITION } from '@common/store/enums';
import { formatCurrency } from '@common/utils/currency';
import { PRICE_TYPE } from '@productDetailed/store/enum';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { IProductDetailedConfiguration } from '@src/common/store/types/configuration';
import { getCurrencyCode } from '@store/common/index';

export const getProductTechnicalSpecification = (
  productSpecificationData: productDetailedApi.GET_PRODUCT_SPECIFICATION.IResponse
) => {
  return productSpecificationData.filter(
    (technicalSpecification: IProductSpecification) => {
      return technicalSpecification.isCustomerVisible;
    }
  );
};

export const getBuyDeviceOnly = (tariffId: string | null) => {
  return tariffId === '-1';
};

export const getBuyWithoutInstallments = (
  installmentDropdownData: IInstallmentData[]
) => {
  const selectedInstallment = installmentDropdownData.find(dropdownItem => {
    return dropdownItem.active;
  });

  return selectedInstallment && String(selectedInstallment.id) === '0';
};

export const getProductHighlights = (
  productSpecificationData: productDetailedApi.GET_PRODUCT_SPECIFICATION.IResponse
) => {
  return productSpecificationData.filter(
    (technicalSpecification: IProductSpecification) => {
      return (
        technicalSpecification.name ===
          htmlKeys.PRODUCT_DETAILED_HIGHIGHTS_SECTION ||
        technicalSpecification.name ===
          htmlKeys.PRODUCT_DETAILED_EXTRA_FEATURE ||
        technicalSpecification.name ===
          htmlKeys.PRODUCT_DETAILED_MARKETING_CONTENT
      );
    }
  );
};

export const sortAttachments = (productDetailedData: IProductDetailedData) => {
  if (
    productDetailedData.variants &&
    productDetailedData.variants[0] &&
    productDetailedData.variants[0].attachments &&
    productDetailedData.variants[0].attachments.thumbnail
  ) {
    const thumbnail = [
      ...productDetailedData.variants[0].attachments.thumbnail
    ];
    thumbnail.sort((image1: IGroup1, _image2: IGroup1) => {
      return image1.name === IMAGE_POSITION.FRONT ? -1 : 1;
    });

    productDetailedData.variants[0].attachments.thumbnail = thumbnail;

    return productDetailedData;
  }

  return productDetailedData;
};

/** psr code added here */
export const getInstallments = (
  productDetailedData: IProductDetailedData,
  prodDetailedTranslation: IProductDetailedTranslation,
  productDetailedConfiguration: IProductDetailedConfiguration
) => {
  const variant = productDetailedData.variants[0];
  if (!variant) {
    return [];
  }
  const installmentDropdownItems: IInstallmentData[] = [];
  const installmentTranslation = prodDetailedTranslation.installments;
  const upfrontTranslation = prodDetailedTranslation.upfront;
  let noInstallmentSelected = true;
  const moSymbolTranslation = prodDetailedTranslation.moSymbol;
  const installmentGroupItems =
    productDetailedData.groups.numberOfInstalments || [];
  const upfrontPriceAmount = getDeviceBasePrice(
    PRICE_TYPE.UPFRONT_PRICE,
    variant.prices
  );

  // installment dropdown will only be added if buyDeviceWithInstallments config is set to true
  if (productDetailedConfiguration.buyDeviceWithInstallments) {
    installmentGroupItems.forEach(installmentGroupItem => {
      const installmentAmount = getInstallmentAmount(
        Number(installmentGroupItem.value),
        variant.prices
      );
      // if any installment is selected, no installment item wont be selected
      if (installmentGroupItem.isSelected) {
        noInstallmentSelected = false;
      }
      installmentDropdownItems.push({
        id: Number(installmentGroupItem.value),
        title: `${installmentGroupItem.value} * ${installmentAmount}`,
        label: `${installmentGroupItem.label} ${installmentTranslation}`,
        amount: installmentAmount as string,
        active: !!installmentGroupItem.isSelected,
        amountType: PRICE_TYPE.RECURRING_FEE,
        monthlySymbol: moSymbolTranslation,
        upfrontAmount: `${upfrontPriceAmount} ${upfrontTranslation}`,
        enabled: installmentGroupItem.enabled
      });
    });
  }

  // no installmentdropdown item
  const basePriceAmount = getDeviceBasePrice(
    PRICE_TYPE.BASE_PRICE,
    variant.prices
  );
  installmentDropdownItems.push({
    id: 0,
    title: `${upfrontTranslation} ${basePriceAmount}`,
    label: `${upfrontTranslation} ${basePriceAmount}`,
    amount: basePriceAmount as string,
    active: noInstallmentSelected,
    amountType: PRICE_TYPE.BASE_PRICE,
    monthlySymbol: null,
    upfrontAmount: '',
    enabled: true
  });

  return installmentDropdownItems;
};

export const getInstallmentAmount = (
  installmentPeriod: number,
  devicePrices: IPrices[]
) => {
  const price = devicePrices.find(priceItem => {
    return priceItem.recurringChargeOccurrence === installmentPeriod;
  });
  if (!price) {
    return null;
  }
  const amount =
    price.discountedValue !== undefined
      ? price.discountedValue
      : price.actualValue;

  return formatCurrency(amount, getCurrencyCode());
};

export const getDeviceBasePrice = (
  priceType: string,
  devicePrices: IPrices[]
) => {
  const price = devicePrices.find(variantPrice => {
    return variantPrice.priceType === priceType;
  });
  if (!price) {
    return null;
  }
  const amount =
    price.discountedValue !== undefined
      ? price.discountedValue
      : price.actualValue;

  return formatCurrency(amount, getCurrencyCode());
};

/** Tariff Dropdown */
export const getTariff = (
  productDetailedData: IProductDetailedData,
  prodDetailedTranslation: IProductDetailedTranslation,
  hideBuyDeviceOnly: boolean
) => {
  const tariffDetailArr: ITariffDetail[] = [];
  const noLoyalty = prodDetailedTranslation.noLoyalty;
  const buyDeviceOnlyTranslation = prodDetailedTranslation.buyDeviceOnly;
  const moSymbolTranslation = prodDetailedTranslation.moSymbol;

  let buyDeviceOnlySelected = true;

  productDetailedData.tariffs.forEach(tariff => {
    const price = tariff && tariff.totalPrices && tariff.totalPrices[0];
    if (price) {
      const amount =
        price.discountedValue !== undefined
          ? price.discountedValue
          : price.actualValue;
      // if no plan is selected buy Device Only will be selected
      if (tariff.isSelected) {
        buyDeviceOnlySelected = false;
      }

      const amountWithCurrency = formatCurrency(amount, getCurrencyCode());
      const loyaltyAgreementsArr: ILoyaltyAgreement[] = [];
      let noLoyaltySelected = true;

      tariff.groups.productOfferingTerm.forEach(loyaltyItem => {
        const { isSelected, label, value } = loyaltyItem;
        if (isSelected) {
          noLoyaltySelected = false;
        }

        loyaltyAgreementsArr.push({
          id: value,
          title: label,
          active: isSelected
        });
      });

      loyaltyAgreementsArr.push({
        id: '0',
        title: noLoyalty,
        active: noLoyaltySelected
      });

      tariffDetailArr.push({
        id: tariff.id,
        title: tariff.name,
        value: tariff.id,
        label: tariff.name,
        description: tariff.description,
        agreements: loyaltyAgreementsArr,
        amount: amountWithCurrency,
        active: tariff.isSelected,
        monthlySymbol: moSymbolTranslation,
        characteristics: tariff.characteristics
      });
    }
  });

  const deviceOnlyPrice = getDeviceBasePrice(
    PRICE_TYPE.BASE_PRICE,
    productDetailedData.variants[0].prices
  );

  if (!hideBuyDeviceOnly) {
    tariffDetailArr.push({
      id: '-1',
      title: `${buyDeviceOnlyTranslation} ${deviceOnlyPrice}`,
      value: '-1',
      label: `${buyDeviceOnlyTranslation}`,
      description: '',
      agreements: [],
      amount: deviceOnlyPrice as string,
      active: buyDeviceOnlySelected,
      monthlySymbol: '',
      characteristics: []
    });
  }

  return tariffDetailArr;
};
