import {
  // FILTER_TYPE,
  MODAL_TYPE,
  NOTIFICATION_MEDIUM
} from '@productList/store/enum';
import { ITEM_STATUS, pageThemeType } from '@src/common/store/enums';
import { IPrices } from '@tariff/store/types';

import { NOTIFY_ME_CONTACT_TYPE, NOTIFY_ME_REASON } from './enum';
export interface IProductDetailedState {
  productId: string | null;
  categoryId: string | null;
  variantId: string | null;
  discountId: string | null;
  instalmentId: string | null;
  tariffId: string | null;
  agreementId: string | null;
  buyDeviceOnly: boolean;
  buyWithoutInstallments: boolean;
  numberOfInstalments: string | null;
  categoryAttributesMap: string[];
  selectedAttributes: IKeyValue[];
  productDetailsData: IProductDetailedData;
  showImageAppShell: boolean;
  loading: boolean;
  addToBasketLoading: boolean;
  showAppShell: boolean;
  // outOfStock: boolean;
  error: IError | null;
  notifyMeContactInfo: string; // mobile or email
  showNotifyMeSuccessScreen: boolean;
  productSpecificationData: IProductSpecification[];
  productHighlightsData: IProductSpecification[];
  productSpecificationError: Error | null;
  productSpecificationLoading: boolean;
  receivedProductDetailedData: boolean;
  openModal: boolean;
  notificationLoading: boolean;
  notificationError: Error | null;
  notificationData: INotificationData;
  notificationModal: MODAL_TYPE;
  parentCategorySlug: string;
  theme?: pageThemeType;
  categoryName: string;
  binkiesFallbackVisible: boolean;
  installmentDropdownData: IInstallmentData[];
  tariffDropdownData: ITariffDetail[];
  showYoungTariff: boolean;
  youngTariffId: string;
  showYoungTariffErrorMessage: boolean;
  hideBuyDeviceOnly: boolean;
}

export interface IJsonLdProductDetails {
  productName: string;
  image: string;
  description: string;
  brand: string;
}

export interface ITariffDataPayload {
  tariffId: string | null;
  agreementId: string | null;
  instalmentId: string | null;
  discountId: string | null;
}
export interface INotificationData {
  variantId: string;
  deviceName: string;
  mediumValue: string;
  reason: string;
  type: NOTIFICATION_MEDIUM.EMAIL | NOTIFICATION_MEDIUM.PHONE;
}

export interface IGetNotificationRequestTemplatePayload {
  type: NOTIFICATION_MEDIUM;
  variantId: string;
  mediumValue: string;
}

export interface IError {
  code?: number;
  message: string;
}

export interface ICategoryAttributesMap {
  [categorySlug: string]: string[];
}
export interface IProductDetailedData {
  // categoryName: string;
  productName: string;
  categoryName: string; // TODO: categoryName in contract but response is category
  variants: IVariant[];
  groups: IVariantGroups;
  tariffs: ITariff[];
  linkedCategory: string;
  totalPrices: IPrices[];
}

export interface IAddons {
  id: string;
  categoryId: string;
  prices: IPrices[];
}

export interface IAddonMap {
  [key: string]: string[];
}

export interface ITariff {
  id: string;
  name: string;
  addons: IAddons[];
  group: string | null;
  totalPrices: IPrices[];
  prices: IPrices[];
  characteristics: ICharacteristic[];
  isSelected: boolean;
  description: string;
  groups: {
    productOfferingTerm: {
      label: string;
      value: string;
      isSelected: boolean;
      name: string;
    }[];
    productOfferingBenefit: {
      label: string;
      value: string;
      isSelected: boolean;
      name: string;
    }[];
  };
}

/** USED ON PROLONGATION */
export interface IKeyValue {
  name: string;
  value: string;
}

export interface IKeyValueLabel extends IKeyValue {
  name: string;
  value: string;
  label?: string;
}

export interface IVariantGroups {
  [key: string]: IVariantGroupsType[];
}

/** USED ON PROLONGATION */
export interface IVariantGroupsType {
  value: string;
  label: string;
  enabled: boolean;
  name: string;
  isSelected?: boolean; // only applicable in numberOfInstalments
}

export interface IVariant {
  id: string;
  name: string;
  brand?: string;
  group: string | null;
  description?: string;
  characteristics: ICharacteristic[];
  // eligibilityUnavailabilityReasons?: eligibilityUnavailabilityReasonType[];
  attachments?: IAttachments;
  prices: IPrices[];
  quantity: number;
  unavailabilityReasonCodes: eligibilityUnavailabilityReasonType[];
  tags?: string[];
}

export interface ICharacteristic {
  name: string;
  values: IValue[];
}

export interface IValue {
  value: string;
  label?: string;
  valueFrom?: string;
  valueTo?: string;
}

export interface IDiscountData {
  discount: number;
  name: string;
  dutyFreeValue: number;
  taxRate: number;
}

export interface IAttachments {
  thumbnail?: IGroup1[];
  // more groups will come here as hardcoded groups in ui
}

export interface IGroup1 {
  type: string;
  name: string;
  url: string;
}

export type eligibilityUnavailabilityReasonType =
  | ITEM_STATUS.IN_STOCK
  | ITEM_STATUS.OUT_OF_STOCK
  | ITEM_STATUS.PRE_ORDER;

export interface INotifyMeContact {
  type:
    | NOTIFY_ME_CONTACT_TYPE.EMAIL
    | NOTIFY_ME_CONTACT_TYPE.MOBILE
    | NOTIFY_ME_CONTACT_TYPE.PHONE;
  medium: { number: string } | { emailAddress: string };
  validated: true;
}

export interface INotifyMeInteractionItem {
  item: {
    id: string;
    entityReferredType: 'productOffering'; // TODO: hardcoded right now for product details page // LIFAQ
  };
}

export interface IProductAttributesResponse {
  attributes: string[];
}
export interface IFetchProductDetailsPayload {
  channel: string;
  category: string;
  productId: string;
  variantId: string | null;
  tariffId: string | null;
  agreementId: string | null;
  discountId: string | null;
  provideTariffs: boolean;
  preselectedPreferences: boolean;
  changedAttributes: IKeyValue[];
  selectedAttributes: IKeyValue[];
  requestedAttributes: string[];
  showFullPageError?: boolean;
}

type notifyMeReasonType =
  | NOTIFY_ME_REASON.COMING_SOON
  | NOTIFY_ME_REASON.OUT_OF_STOCK;

export interface INotifyMePayload {
  channel: {
    id: string;
  };
  reason: notifyMeReasonType;
  description: string;
  contacts: INotifyMeContact[];
  interactionItems: INotifyMeInteractionItem[];
}

export interface IProductSpecification {
  name: string;
  values: { value: string; unit?: string }[];
  isCustomerVisible: boolean;
}

export interface IFetchProductDetailsAction {
  preselectedPreferences: boolean;
  variantId: string | null;
  changedAttribute: IKeyValue | null;
  fetchCategoryData: boolean;
  savePlan: boolean;
  showFullPageError?: boolean;
  // setAllVariantAttributesAsSelected: boolean;
}

export interface IAddToBasketPayload {
  cartItems: ICartItem[];
}

export interface ICartItem {
  action: string;
  quantity: number;
  group: string | null;
  product: {
    id: string;
    characteristic: IBasketCharacteristic[];
  };
  prices: IPrices[];
  cartItems?: ICartItem[];
}

export interface IBasketCharacteristic {
  name: string;
  value: string;
}

export interface IBasketCondition {
  recurringChargePeriod: string;
  recurringChargeOccurrence: number;
  alterationPriceType: string;
  conditionType: string;
  conditionId: string;
}

export interface IInstallmentData {
  id: number;
  title: string;
  label: string;
  amount: string | null;
  active: boolean;
  amountType: string;
  monthlySymbol: string | null;
  upfrontAmount: string;
  enabled: boolean;
}

export interface ILoyaltyAgreement {
  id: string;
  title: string;
  active: boolean;
}

export interface ITariffDetail {
  id: string;
  title: string;
  value: string;
  label: string;
  description: string;
  agreements: ILoyaltyAgreement[];
  amount: string;
  active: boolean;
  monthlySymbol: string;
  characteristics: ICharacteristic[];
}

export interface IProductDetailResponse {
  productDetailsData: IProductDetailedData;
  selectedAttributes: IKeyValue[];
  installmentGroup: IInstallmentData[];
  tariffGroup: ITariffDetail[];
  instalmentId: string | null;
  tariffId: string | null;
  agreementId: string | null;
  buyDeviceOnly: boolean;
  buyWithoutInstallments: boolean;
}

export interface IJsonLdProductPayload {
  variantId: string;
  categoryId: string;
}

export interface IJsonLdProductResponse {
  '@graph': [
    [
      {
        '@type': string;
        '@context': {
          characteristics: string;
          attachments: string;
          name: string;
          description: string;
          id: string;
        };
        id: string;
        name: string;
        description: string | null;
        characteristics: [
          {
            '@type': string;
            '@context': {
              charactericValues: string;
              name: string;
              label: string;
            };
            name: string;
            label: string;
            charactericValues: [
              {
                '@type': string;
                '@context': {
                  label: string;
                  value: string;
                };
                label: string;
                value: string;
              }
            ];
          }
        ];
        attachments: [];
      }
    ]
  ];
  // tslint:disable-next-line:max-file-line-count
}
