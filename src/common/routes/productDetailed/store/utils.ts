import { RootState } from '@src/common/store/reducers';
import * as productDetailedApi from '@common/types/api/productDetailed';
import APP_CONSTANTS from '@common/constants/appConstants';
import appkeys from '@common/constants/appkeys';
import { IPrices } from '@tariff/store/types';
import { ICondition, IJSONLdRequest } from '@productList/store/types';
import { IFiltersRequest } from '@productList/store/services';
import { sendAddToBasketEvent } from '@events/basket/index';
import {
  DEVICE_DISCOUNT_ID,
  DEVICE_PRICE_ALTERATION_ID
} from '@productDetailed/store/enum';

import { ICategory } from '../../category/store/types';

import {
  IAddToBasketPayload,
  IBasketCharacteristic,
  ICharacteristic,
  IGroup1,
  IJsonLdProductDetails,
  IKeyValue,
  IKeyValueLabel,
  IProductDetailedData,
  IVariant
} from './types';

export const getJsonLdRequest = (): IJSONLdRequest => {
  return {
    channel: APP_CONSTANTS.CHANNEL,
    page: 0,
    itemPerPage: 100,
    variantId: null,
    filterCharacteristics: [],
    categories: [],
    conditions: []
  };
};

export const getProductDetailsRequestTemplate = (
  productId: string,
  variantId: string | null,
  preselectedPreferences: boolean,
  changedAttribute: IKeyValue | null,
  selectedAttributes: IKeyValue[],
  requestedAttributes: string[],
  category: string,
  tariffId: string | null,
  agreementId: string | null,
  discountId: string | null,
  buyDeviceOnly: boolean,
  showTariffInfo: boolean
  // tslint:disable-next-line: parameters-max-number
): productDetailedApi.POST.IRequest => {
  const provideTariffs = showTariffInfo ? !buyDeviceOnly : false;

  return {
    productId,
    variantId,
    channel: APP_CONSTANTS.CHANNEL,
    preselectedPreferences,
    changedAttributes: changedAttribute ? [changedAttribute] : [],
    selectedAttributes,
    requestedAttributes: [...requestedAttributes],
    provideTariffs, // provide tariff false if buyDeviceOnly is true
    category,
    tariffId: tariffId === '-1' ? null : tariffId,
    agreementId,
    discountId
  };
};

export const getProductDetailedState = (state: RootState) => {
  return state.productDetailed;
};

export const getProductCatalogConfiguration = (state: RootState) => {
  return state.configuration.cms_configuration.modules.catalog;
};

export const getCartItemObj = (
  id: string,
  prices: IPrices[],
  characteristics: IBasketCharacteristic[],
  group: string | null
) => ({
  action: 'add',
  quantity: 1,
  group: group ? group : null,
  product: {
    id,
    characteristic: characteristics
  },
  prices: prices.filter(price => !!price)
});

const getSubsidy = (characteristics: ICharacteristic[]) => {
  if (characteristics && characteristics.length) {
    const subsidary = characteristics.filter(
      (characteristic: ICharacteristic) => {
        return characteristic.name === DEVICE_PRICE_ALTERATION_ID;
      }
    );
    if (subsidary && subsidary[0] && subsidary[0].values) {
      return [
        {
          name: subsidary[0].name,
          value:
            subsidary[0].values &&
            subsidary[0].values[0] &&
            subsidary[0].values[0].value
        }
      ];
    }
  }

  return [];
};

const getDiscountId = (characteristics: ICharacteristic[]) => {
  if (characteristics && characteristics.length) {
    const discountId = characteristics.filter(
      (characteristic: ICharacteristic) => {
        return characteristic.name === DEVICE_DISCOUNT_ID;
      }
    );
    if (discountId && discountId[0] && discountId[0].values) {
      return [
        {
          name: discountId[0].name,
          value:
            discountId[0].values &&
            discountId[0].values[0] &&
            discountId[0].values[0].value
        }
      ];
    }
  }

  return [];
};

// tslint:disable-next-line: one-variable-per-declaration
export const getAddToBasketReqObject = ({
  buyDeviceOnly,
  buyWithoutInstallments,
  productDetailsData,
  considerStockForPreOrder
}: {
  buyDeviceOnly: boolean;
  buyWithoutInstallments: boolean;
  productDetailsData: IProductDetailedData;
  considerStockForPreOrder: boolean;
}): IAddToBasketPayload | undefined => {
  const variant = productDetailsData.variants[0];
  if (!variant) {
    return;
  }

  const payload: IAddToBasketPayload = {
    cartItems: []
  };

  const numberOfInstalments = productDetailsData.groups.numberOfInstalments;
  const subsidy = getSubsidy(productDetailsData.variants[0].characteristics);
  const discountId = getDiscountId(
    productDetailsData.variants[0].characteristics
  );
  const selectedInstallment = (numberOfInstalments || []).find(
    instalmentItem => {
      return instalmentItem.isSelected as boolean;
    }
  );
  const selectedInstallmentId =
    selectedInstallment && selectedInstallment.value
      ? selectedInstallment.value
      : null;

  // add Device Price
  if (buyWithoutInstallments) {
    const basePrice = variant.prices.find(price => {
      return price.priceType === 'basePrice';
    });
    payload.cartItems.push(
      getCartItemObj(
        variant.id,
        basePrice ? [basePrice] : [],
        selectedInstallment && selectedInstallment.value
          ? [
              {
                name: selectedInstallment.name,
                value: selectedInstallment.value
              },
              ...subsidy,
              ...discountId
            ]
          : [],
        variant.group
      )
    );
  } else {
    const deviceUpfrontPrice = variant.prices.find(price => {
      return price.priceType === 'upfrontPrice';
    });

    const deviceRecurringPrice = variant.prices.find(price => {
      return (
        price.priceType === 'recurringFee' &&
        String(price.recurringChargeOccurrence) === selectedInstallmentId
      );
    });
    const devicePricesArr: IPrices[] = [];
    [deviceUpfrontPrice, deviceRecurringPrice].forEach(price => {
      if (price) {
        devicePricesArr.push(price);
      }
    });
    payload.cartItems.push(
      getCartItemObj(
        variant.id,
        devicePricesArr,
        selectedInstallment && selectedInstallment.value
          ? [
              {
                name: selectedInstallment.name,
                value: selectedInstallment.value
              },
              ...subsidy,
              ...discountId
            ]
          : [],
        variant.group
      )
    );
  }

  if (!buyDeviceOnly) {
    const selectedTariff = productDetailsData.tariffs.find(
      tariff => tariff.isSelected
    );
    if (selectedTariff) {
      sendAddToBasketEvent(
        productDetailsData,
        selectedTariff.id,
        considerStockForPreOrder
      );

      const tariffPricesArr: IPrices[] = [];
      const tariffUpfrontPrice = selectedTariff.prices.find(
        price => price.priceType === 'upfrontPrice'
      );
      const tariffRecurringPrice = selectedTariff.prices.find(
        price => price.priceType === 'recurringFee'
      );
      [tariffUpfrontPrice, tariffRecurringPrice].forEach(price => {
        if (price) {
          tariffPricesArr.push(price);
        }
      });
      let discountCharacterstic:
        | null
        | undefined
        | IBasketCharacteristic = null;
      discountCharacterstic = selectedTariff.groups.productOfferingBenefit.find(
        benefit => {
          return benefit.isSelected;
        }
      );
      if (discountCharacterstic) {
        discountCharacterstic = {
          name: discountCharacterstic.name,
          value: discountCharacterstic.value
        };
      }
      let loyalityCharacterstic = null;
      loyalityCharacterstic = selectedTariff.groups.productOfferingTerm.find(
        loyality => {
          return loyality.isSelected;
        }
      );
      if (loyalityCharacterstic) {
        loyalityCharacterstic = {
          name: loyalityCharacterstic.name,
          value: loyalityCharacterstic.value
        };
      }
      const basketCharacterstics: IBasketCharacteristic[] = [];
      [discountCharacterstic, loyalityCharacterstic].forEach(characterstic => {
        if (characterstic) {
          basketCharacterstics.push(characterstic);
        }
      });

      // numberOfInstallments
      // discount
      // tariff
      // loyality
      payload.cartItems.push(
        getCartItemObj(
          selectedTariff.id,
          tariffPricesArr,
          basketCharacterstics,
          selectedTariff.group
        )
      );
      payload.cartItems[1].cartItems = [];
      selectedTariff.addons.forEach(addOn => {
        if (payload.cartItems[1].cartItems) {
          payload.cartItems[1].cartItems.push(
            getCartItemObj(addOn.id, addOn.prices, [], null)
          );
        }
      });
    }
  } else {
    sendAddToBasketEvent(
      productDetailsData,
      undefined,
      considerStockForPreOrder
    );
  }

  return payload;
};

export const getOutOfStockReason = (
  _productDetailsData: IProductDetailedData
) => {
  // tslint:disable-next-line: no-commented-code
  // if (
  //   productDetailsData.variants[0] &&
  //   productDetailsData.variants[0].eligibilityUnavailabilityReasons &&
  //   productDetailsData.variants[0].eligibilityUnavailabilityReasons[0] &&
  //   productDetailsData.variants[0].eligibilityUnavailabilityReasons[0]
  //     .description
  // ) {
  //   return productDetailsData.variants[0].eligibilityUnavailabilityReasons[0]
  //     .description;
  // }

  return '';
};

export const getAttributesFromCharacteristics = (
  characteristics: ICharacteristic[],
  categoryAttributeNames: string[]
): IKeyValueLabel[] => {
  return characteristics
    .filter(
      characteristic =>
        categoryAttributeNames.includes(characteristic.name) &&
        characteristic.name !== 'numberOfInstalments'
    )
    .map(characteristic => {
      return {
        name: characteristic.name,
        value: characteristic.values[0] && characteristic.values[0].value,
        label: characteristic.values[0] && characteristic.values[0].label
      };
    });
};

export const getSelectedTariffAgreementInstalmentId = (
  productDetailsData: IProductDetailedData
) => {
  const selectedInstalment = (
    productDetailsData.groups.numberOfInstalments || []
  ).find(numberOfInstalmentItem => {
    return !!numberOfInstalmentItem.isSelected;
  });
  const instalmentId =
    selectedInstalment && selectedInstalment.value
      ? selectedInstalment.value
      : '0';
  const selectedTariff = productDetailsData.tariffs.find(tariff => {
    return !!tariff.isSelected;
  });
  const tariffId =
    selectedTariff && selectedTariff.id ? selectedTariff.id : '-1';
  const selectedLoyalty = (
    (selectedTariff &&
      selectedTariff.groups &&
      selectedTariff.groups.productOfferingTerm) ||
    []
  ).find(productOfferingTermItem => {
    return productOfferingTermItem.isSelected;
  });
  const agreementId =
    selectedLoyalty && selectedLoyalty.value ? selectedLoyalty.value : '0';

  return { instalmentId, tariffId, agreementId };
};

export const jsonLdProductDetials = (
  product: IProductDetailedData
): IJsonLdProductDetails => {
  const productDetails: IJsonLdProductDetails = {
    productName: '',
    image: '',
    description: '',
    brand: ''
  };
  if (product.variants.length > 0) {
    const variants: IVariant = product.variants[0];
    if (variants.name) {
      productDetails.productName = variants.name;
    }
    if (
      variants.attachments &&
      variants.attachments.thumbnail &&
      variants.attachments.thumbnail.length > 0
    ) {
      variants.attachments.thumbnail.forEach((image: IGroup1) => {
        if (image.name === 'front') {
          productDetails.image = image.url;
        }
      });
    }
    if (variants.characteristics && variants.characteristics.length > 0) {
      variants.characteristics.forEach((characteristic: ICharacteristic) => {
        if (characteristic.name === 'brand') {
          productDetails.brand = characteristic.values[0].value;
        }
      });
    }
    if (variants.description) {
      productDetails.description = variants.description;
    }
  }

  return productDetails;
};

// tslint:disable-next-line: max-file-line-count
export const getHideBuyDeviceOnly = (category: ICategory) => {
  const characteristic = category.characteristics.find(
    (characteristicItem: ICharacteristic) => {
      return characteristicItem.name === appkeys.CATEGORY_HIDE_BUY_DEVICE_ONLY;
    }
  );
  if (
    characteristic &&
    characteristic.values &&
    characteristic.values[0].value
  ) {
    return characteristic.values[0].value === 'true';
  }

  return false;
  // tslint:disable-next-line: max-file-line-count
};

export const getJSONLDRequestTemplate = (
  page: number,
  itemPerPage: number,
  categoryId: string,
  variantId: string,
  filterCharacteristics: IFiltersRequest[]
) => {
  const conditions: ICondition[] = [];
  const requestBody = getJsonLdRequest();
  requestBody.page = page;
  requestBody.itemPerPage = itemPerPage;
  requestBody.variantId = variantId;
  requestBody.categories = [];
  requestBody.categories.push({
    id: categoryId,
    characteristics: []
  });
  Array.prototype.push.apply(
    requestBody.filterCharacteristics,
    filterCharacteristics.map(filterCharacterstic => {
      return {
        id: filterCharacterstic.name,
        characteristicValues: filterCharacterstic.characteristicValues
      };
    })
  );

  Array.prototype.push.apply(requestBody.conditions, conditions);

  return requestBody;
  // tslint:disable-next-line:max-file-line-count
};
