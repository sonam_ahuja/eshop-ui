import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { DialogBox } from 'dt-components';

import StyledTariffHeader from '../tariff/TariffHeader/styles';
import SinglePlanWrap from '../tariff/TariffPlans/DesktopPlans/styles/SinglePlanStyles';

export const AppTariffDialogBox = styled(DialogBox)`
  -webkit-overflow-scrolling: touch;
  -webkit-transform: translateZ(0);
  .dt_overlay {
    .dt_outsideClick {
      width: 100%;
      height: 100%;
      /* overflow-y: auto; */
      .closeIcon {
        z-index: 1;
        position: absolute;
        float: right;
      }
      .dialogBoxContentWrap {
        height: 100%;
        overflow-y: auto;
        width: 100%;
      }
      ${StyledTariffHeader} {
        padding-top: 2rem;
        .main-plans-wrapper {
          padding: 0rem;
          .header-panel {
            .left-panel {
              h1 {
                font-size: 1.875rem;
                line-height: 2rem;
                margin-bottom: 1rem;
                padding-right: 2.5rem;
              }
            }
            .right-panel {
              .list-inline {
                li {
                  &.dropdown-wrap {
                    margin-top: 2rem;
                  }
                }
              }
            }
          }
        }
      }
      .main-plans-wrapper {
        padding: 0rem 4.9rem 3.4rem;
        .panel-wrap {
          padding: 4.25rem 0 1.5rem;
          .multiple-plans .pricing-plans-wrapper {
            .flex-container {
              padding-left: 2.8rem;
            }
            .aside-panel {
              .plans-body-wrap {
                .btn-wrapper {
                  min-height: 2.5rem;
                }
              }
            }
          }
        }
      }
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    .dt_overlay {
      .dt_outsideClick {
        ${StyledTariffHeader} {
          padding-top: 5.5rem;
          .main-plans-wrapper {
            .header-panel {
              .left-panel {
                width: 60%;
                h1 {
                  padding-right: 0rem;
                  font-size: 2.5rem;
                  margin-bottom: 0.5rem;
                }
                p {
                  max-width: inherit;
                }
              }
              .right-panel {
                width: 40%;
                .list-inline {
                  li {
                    &.dropdown-wrap {
                      margin-top: 0rem;
                    }
                  }
                }
              }
            }
          }
        }
        ${SinglePlanWrap} {
          .btn-fluid {
            width: 14.95rem;
            height: 3rem;
            margin-top: 0.7rem;
          }
        }
        .closeIcon {
          position: sticky;
        }
      }
    }
  }
`;

export const StyledProductDetailed = styled.div`
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: 100%;
    display: flex;
    position: relative;
    .notificationPopup {
      .dt_floatLabelInput {
        padding-bottom: 0;
      }
    }
  }
`;

export const StyledContentWrapper = styled.div`
  width: 100%;
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: calc(100% - 5.75rem);
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: calc(100% - 4.75rem);
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    /* width: calc(100% - 5.75rem); */
    width: 100%;
  }
`;
