import {
  IError,
  IGetNotificationRequestTemplatePayload,
  IInstallmentData,
  IKeyValue,
  INotificationData,
  IProductDetailedData,
  IProductSpecification,
  ITariffDetail
} from '@productDetailed/store/types';
import { IPrices } from '@tariff/store/types';
import { IError as IGenricError } from '@src/common/store/types/common';
import {
  IGlobalTranslation,
  IProductDetailedTranslation,
  IProductListTranslation
} from '@src/common/store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  ILoginFormRules,
  IProductDetailedConfiguration
} from '@src/common/store/types/configuration';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productDetailed/store/enum';
import { pageThemeType } from '@src/common/store/enums';
import {
  IMapDispatchToProps as IMapTariffDispatchToProps,
  IMapStateToProps as IMapTariffStateToProps
} from '@tariff/types';

export interface IMapStateToProps {
  commonError: IGenricError;
  currency: ICurrencyConfiguration;
  productDetailsData: IProductDetailedData;
  productHighlightsData: IProductSpecification[];
  productSpecificationData: IProductSpecification[];
  showModal: boolean;
  notificationModalType: MODAL_TYPE;
  formConfiguration: ILoginFormRules;
  notificationData: INotificationData;
  error: IError | null;
  productListtranslation: IProductListTranslation;
  loading: boolean;
  notificationLoading: boolean;
  addToBasketLoading: boolean;
  showAppShell: boolean;
  globalTranslation: IGlobalTranslation;
  categoryId: string | null;
  productDetailedTranslation: IProductDetailedTranslation;
  productDetailedConfiguration: IProductDetailedConfiguration;
  catalogConfiguration: ICatalogConfiguration;
  buyDeviceOnly: boolean;
  showImageAppShell: boolean;
  binkiesFallbackVisible: boolean;
  categoryName: string;
  theme?: pageThemeType;
  parentCategorySlug: string;
  lastListURL: string | null;
  tariffId: string | null;
  agreementId: string | null;
  showYoungTariff: boolean;
  installmentDropdownData: IInstallmentData[];
  tariffDropdownData: ITariffDetail[];
  totalPrices: IPrices[];
  instalmentId: string | null;
  discountId: string | null;
  tariffStateProps: IMapTariffStateToProps;
  showYoungTariffErrorMessage: boolean;
}
export interface IMapDispatchToProps {
  tariffDispatchProps: IMapTariffDispatchToProps;
  fetchProductDetails(
    preselectedPreferences: boolean,
    variantId: string | null,
    changedAttribute: IKeyValue | null,
    fetchCategoryData: boolean,
    saveTariff: boolean,
    showFullPageError?: boolean
  ): // setAllVariantAttributesAsSelected: boolean
  void;
  openModal(isModalOpen: boolean): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  fetchProductSpecification(productId: string): void;
  setError(error: IError): void;
  setNotificationData(data: INotificationData): void;
  addToBasket(variantId: string): void;
  setProductVariantCategoryId(
    productId: string,
    variantId: string,
    categoryId: string
  ): void;
  setAppShell(showAppShell: boolean): void;
  setCategoryAttributes(attributes: string[]): void;
  setImageAppShell(showImageAppShell: boolean): void;
  changeBinkiesFallbackVisible(binkiesFallbackVisible: boolean): void;
  setTariffData(
    tariffId: string | null,
    agreementId: string | null,
    instalmentId: string | null,
    discountId: string | null
  ): void;
  validateAgeLimit(data: string): void;
  youngTariffCheck(planId: string): void;
  closeAgeLimitModal(): void;
  setDeviceList(list: string): void;
  setLandingPageDeviceList(list: string): void;
}

export interface ICommonModalProps {
  isOpen: boolean;
  deviceName: string;
  variantId: string;
  errorMessage: string | null;
  notificationLoading: boolean;
  modalType: MODAL_TYPE;
  translation: IProductListTranslation;
  notificationMediumValue: string;
  formConfiguration: ILoginFormRules;
  notificationMediumType: NOTIFICATION_MEDIUM.PHONE | NOTIFICATION_MEDIUM.EMAIL;
  openModal(isModalOpen: boolean): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
}
