import {
  eligibilityUnavailabilityReasonType,
  ICharacteristic,
  IKeyValue,
  IProductSpecification,
  ITariff,
  ITariffDetail,
  IVariantGroupsType
} from '@productDetailed/store/types';
import { IPrices } from '@tariff/store/types';
import { PRICE_TYPE } from '@productDetailed/store/enum';
import { IProductDetailedTranslation } from '@src/common/store/types/translation';
import { IProductDetailedConfiguration } from '@src/common/store/types/configuration';
import htmlKeys from '@common/constants/appkeys';
import { ITEM_STATUS } from '@common/store/enums';
import { getDateDifference } from '@common/utils/dateTime';
import { RootState } from '@src/common/store/reducers';

export const getProductDetailConfiguration = (
  state: RootState
): IProductDetailedConfiguration => {
  return state.configuration.cms_configuration.modules.productDetailed;
};

export const getProdDetailTranslation = (
  state: RootState
): IProductDetailedTranslation => {
  return state.translation.cart.productDetailed;
};

/** USED ON PROLONGATION */
export const getSelectedCharacterstic = (
  characterstics: ICharacteristic[],
  variantGroupsType: IVariantGroupsType[] | undefined,
  charactersticName: string
) => {
  const variantCharacterstic = characterstics.find(characterstic => {
    return characterstic.name === charactersticName;
  });
  if (
    variantCharacterstic &&
    variantCharacterstic.values &&
    variantGroupsType
  ) {
    const value = variantCharacterstic.values[0].value;

    return variantGroupsType.find(groupType => {
      return groupType.value === value;
    });
  }

  return undefined;
};

export const getOutOfStockReason = (
  _eligibilityUnavailabilityReasons: eligibilityUnavailabilityReasonType[]
) => {
  return '';
  // tslint:disable-next-line: no-commented-code
  // const index = eligibilityUnavailabilityReasons.findIndex(
  //   (reason: IEligibilityUnavailabilityReasons) => {
  //     return reason.code === NOTIFY_ME_REASON.OUT_OF_STOCK;
  //   }
  // );
  // return eligibilityUnavailabilityReasons[index].description;
};

export const getTags = (
  tagsCharacterstic: string | undefined,
  launchDateString: string | undefined,
  newProductBeforeLaunchDays: number,
  productDetailedTranslation: IProductDetailedTranslation,
  showOnlineStockAvailability: boolean,
  deviceStock: eligibilityUnavailabilityReasonType
) => {
  let tags: string[] = [];
  if (tagsCharacterstic) {
    tags = tagsCharacterstic.split(',');
  }

  if (showOnlineStockAvailability) {
    if (
      deviceStock &&
      deviceStock.includes(ITEM_STATUS.OUT_OF_STOCK)
    ) {
      tags.push(productDetailedTranslation.outOfStock);
    } else if (
      deviceStock &&
      deviceStock.includes(ITEM_STATUS.PRE_ORDER)
    ) {
      tags.push(productDetailedTranslation.preOrder);
    } else {
      tags.push(productDetailedTranslation.inStock);
    }
  }

  if (launchDateString) {
    const launchDate = new Date(launchDateString);
    const dateDiff = getDateDifference(new Date(), launchDate);
    if (!isNaN(dateDiff) && dateDiff < newProductBeforeLaunchDays) {
      tags.push(productDetailedTranslation.newDevice);
    }
  }

  return tags;
};

export const getVariantPrices = (prices: IPrices[]) => {
  const basePrice = prices.find(variantPrice => {
    return variantPrice.priceType === PRICE_TYPE.BASE_PRICE;
  });
  const upfrontPrice = prices.find(variantPrice => {
    return variantPrice.priceType === PRICE_TYPE.UPFRONT_PRICE;
  });
  const recurringFee = prices.find(variantPrice => {
    return variantPrice.priceType === PRICE_TYPE.RECURRING_FEE;
  });

  return { basePrice, upfrontPrice, recurringFee };
};

export const getTagCharacterstics = (characteristics: ICharacteristic[]) => {
  return characteristics.find(characterstic => {
    return characterstic.name === htmlKeys.PRODUCT_DETAILED_VARIANT_TAGS;
  });
};

export const getVariantLaunchDate = (characteristics: ICharacteristic[]) => {
  const launchDate = characteristics.find(characterstic => {
    return characterstic.name === htmlKeys.PRODUCT_DETAILED_LAUNCH_DATE;
  });

  return (
    launchDate &&
    launchDate.values &&
    launchDate.values[0] &&
    launchDate.values[0].label
  );
};
export const getProductCharacteristics = (
  characteristics: ICharacteristic[]
) => {
  const title = characteristics.find(characteristic => {
    return characteristic.name === htmlKeys.META_TAG_TITLE;
  });
  const description = characteristics.find(characteristic => {
    return characteristic.name === htmlKeys.META_TAG_DESCRIPTION;
  });

  return {
    title: title && title.values && title.values[0].value,
    description:
      description && description.values && description.values[0].value
  };
};

export const getPromotion = (characteristics: ICharacteristic[]) => {
  const promotion = characteristics.find(characteristic => {
    return characteristic.name === htmlKeys.PRODUCT_PROMOTION;
  });

  return {
    promotionEndDate:
      promotion && promotion.values && promotion.values[0].valueTo,
    promotionStartDate:
      promotion && promotion.values && promotion.values[0].valueFrom
  };
};

export const getBinkiesID = (characteristics: ICharacteristic[]) => {
  const binkiesId = characteristics.find(characteristic => {
    return characteristic.name === htmlKeys.PRODUCT_DETAILED_BINKIES_ID;
  });

  return binkiesId && binkiesId.values && binkiesId.values[0].label;
};

export const getProductHtml = (
  productSpecificationData: IProductSpecification[]
) => {
  const productExtraFeature = productSpecificationData.find(specification => {
    return specification.name === htmlKeys.PRODUCT_DETAILED_EXTRA_FEATURE;
  });
  const marketingContent = productSpecificationData.find(specification => {
    return specification.name === htmlKeys.PRODUCT_DETAILED_MARKETING_CONTENT;
  });
  const highlightProductDetailedSection = productSpecificationData.find(
    specification => {
      return specification.name === htmlKeys.PRODUCT_DETAILED_HIGHIGHTS_SECTION;
    }
  );

  return {
    productExtraFeature,
    marketingContent,
    highlightProductDetailedSection
  };
};

export const createQueryParams = (
  changedAttribute: IKeyValue | null
): string => {
  let changedAttrUrl = '';
  if (changedAttribute) {
    changedAttrUrl = `changedAttr[]=changedAttr.${changedAttribute.name}=${
      changedAttribute.value
    }`;
  }

  return changedAttrUrl;
};

/** USED ON PROLONGATION */
export const getSelectedFiltersFromParams = (
  query: string
): {
  changedAttribute: IKeyValue | null;
} => {
  let changedAttribute = null;
  const decodedParams = decodeURIComponent(query);
  const params = new URLSearchParams(decodedParams);
  const changedAttrParam = params.get('changedAttr[]');
  if (changedAttrParam) {
    const changedAttrParamArrPair = changedAttrParam.split('=');
    if (changedAttrParamArrPair.length > 1) {
      changedAttribute = {
        name: changedAttrParamArrPair[0].replace(
          /(\[|\]|\.|changedAttr)/g,
          () => ''
        ),
        value: changedAttrParamArrPair[1]
      };
    }
  }

  return { changedAttribute };
};

export const checkForBinkiesID = (
  characteristics: ICharacteristic[]
): boolean => {
  const index = characteristics.findIndex(
    characteristic =>
      characteristic.name === htmlKeys.PRODUCT_DETAILED_BINKIES_ID
  );

  if (index !== -1 && characteristics[index].values[0].value) {
    return true;
  }

  return false;
};

export const getTariffDataFromURL = (query: string) => {
  const decodedParams = decodeURIComponent(query);
  const params = new URLSearchParams(decodedParams);
  const tariffId = params.get('tariffId');
  const agreementId = params.get('agreementId');
  const instalmentId = params.get('instalmentId');
  const discountId = params.get('discountId');

  return { tariffId, agreementId, instalmentId, discountId };
};

export const getPlansFromTariff = (tariffs: ITariff[]) => {
  return tariffs.map(tariff => {
    return {
      id: tariff.id,
      name: tariff.name,
      description: tariff.description,
      agreement: tariff.groups.productOfferingTerm
    };
  });
};

export const getSelectedTariffName = (tariffDropdownData: ITariffDetail[]) => {
  const selectedTariff = tariffDropdownData.find(dropdownItem => {
    return dropdownItem.active;
  });

  if (selectedTariff && selectedTariff.id === '-1') {
    return null;
  }

  return selectedTariff && selectedTariff.title ? selectedTariff.title : null;
};
