import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import FilterHeader, {
  getFilteredChips,
  IProps,
  onClearAllClick,
  onFilterChipClick
} from '@productList/Filter/Desktop/FilterHeader';
import {
  SelectedFilter,
  SortByList,
  SortedBy
} from '@mocks/productList/productList.mock';
import appState from '@store/states/app';
import APP_KEYS from '@common/constants/appkeys';

describe('<FilterSection />', () => {
  const props: IProps = {
    isOpen: true,
    selectedFilters: SelectedFilter,
    translation: appState().translation.cart.productList,
    sortBy: SortedBy,
    sortByList: SortByList,
    onOpen: jest.fn(),
    onClose: jest.fn(),
    setSelectedFilter: jest.fn(),
    clearAllFilters: jest.fn(),
    setSortBy: jest.fn(),
    setFilteredByRef: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterHeader {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when sortBy is undefined', () => {
    const newProps: IProps = { ...props, sortBy: null };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterHeader {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when filterCount = 0', () => {
    const newProps: IProps = { ...props, sortBy: null, selectedFilters: {} };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterHeader {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when  0 < filterCount < 3', () => {
    const newProps: IProps = {
      ...props,
      sortBy: null,
      selectedFilters: {
        1: [
          {
            name: '',
            default: false,
            value: ''
          }
        ]
      }
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterHeader {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when filterCount = 0 & sortByList is empty', () => {
    const newProps: IProps = {
      ...props,
      sortBy: null,
      selectedFilters: {},
      sortByList: []
    };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterHeader {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('onFilterChipClick test', () => {
    const fn = onFilterChipClick(
      '1',
      {
        name: 'string',
        default: true,
        value: 'string',
        quantity: 0
      },
      jest.fn()
    );
    fn();
    getFilteredChips(
      {
        [APP_KEYS.BFF_PRICE_RANGE_ID]: [
          {
            name: 'string',
            default: false,
            value: 'string',
            quantity: 10
          }
        ],
        [APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID]: [
          {
            name: 'string',
            default: false,
            value: 'string',
            quantity: 10
          }
        ]
      },
      jest.fn(),
      appState().translation.cart.productList
    );
    const fnOnClearAllClick = onClearAllClick(() => null);
    // tslint:disable-next-line: no-any
    fnOnClearAllClick({ stopPropagation: jest.fn() } as any);
  });
});
