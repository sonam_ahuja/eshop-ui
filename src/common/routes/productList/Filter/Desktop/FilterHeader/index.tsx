import { IProductListTranslation } from '@common/store/types/translation';
import React, { ReactNode } from 'react';
import { isMobile } from '@src/common/utils';
import { IFilter, ISelectedFilters, ISortBy } from '@productList/store/types';
import { Column, Row } from '@common/components/Grid/styles';
import { Accordion, Anchor, FilterChips, Select } from 'dt-components';
import {
  addCurrencyCodeInPriceRange,
  getTotalFiltersCount
} from '@productList/utils';
import APP_KEYS from '@common/constants/appkeys';
import EVENT_NAME from '@events/constants/eventName';
import { sendDropdownClickEvent } from '@src/common/events/common';
import appConstants from '@src/common/constants/appConstants';

import HorizontalScroll from './../../common/HorizontalScroll';
import { StyledFilterHeader } from './styles';
export interface IProps {
  isOpen: boolean;
  selectedFilters: ISelectedFilters;
  sortBy: ISortBy | null;
  translation: IProductListTranslation;
  sortByList: { id: string; title: string }[];
  onOpen(): void;
  onClose(): void;
  setSelectedFilter(filterId: string, filter: IFilter): void;
  clearAllFilters(): void;
  setSortBy(sortBy: ISortBy): void;
  setFilteredByRef(ref: HTMLElement | null): void;
}

interface IFilterChipProp extends IFilter {
  filterId: string;
}

export const onFilterChipClick = (
  filterId: string,
  filter: IFilter,
  setSelectedFilter: IProps['setSelectedFilter']
) => {
  return () => setSelectedFilter(filterId, filter);
};

export const getFilteredChips = (
  selectedFilters: ISelectedFilters,
  setSelectedFilter: IProps['setSelectedFilter'],
  translation: IProductListTranslation
) => {
  let filters: IFilterChipProp[] = [];
  Object.keys(selectedFilters).forEach(filterId => {
    const tempFilters: IFilterChipProp[] = [];
    (selectedFilters[filterId] as IFilterChipProp[]).forEach(
      (item: IFilterChipProp) => {
        if (item.quantity && item.quantity > 0) {
          item.filterId = filterId;
          // price range is exception where we are showing value instead of name as label
          if (filterId === APP_KEYS.BFF_PRICE_RANGE_ID) {
            tempFilters.push({
              ...item,
              name: addCurrencyCodeInPriceRange(
                item.value,
                translation.priceRangeAboveFilter
              )
            });
          } else if (filterId === APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID) {
            tempFilters.push({
              ...item,
              name: translation.availability[item.value]
            });
          } else {
            tempFilters.push(item);
          }
        }
      }
    );
    filters = filters.concat(tempFilters);
  });

  return filters.map(filter => (
    <FilterChips
      disabled={filter.quantity === 0}
      cancelable={true}
      onClose={onFilterChipClick(filter.filterId, filter, setSelectedFilter)}
      label={filter.name}
      key={filter.value}
      active={true}
    />
  ));
};

export const onClearAllClick = (clearAllFilters: () => void) => {
  return (event: React.SyntheticEvent) => {
    event.stopPropagation();
    clearAllFilters();
  };
};

const FilterHeader = (props: IProps) => {
  const {
    isOpen,
    onOpen,
    onClose,
    selectedFilters,
    setSelectedFilter,
    clearAllFilters,
    sortBy,
    setSortBy,
    sortByList,
    translation,
    setFilteredByRef
  } = props;
  let title: ReactNode = translation.filterByTitle;
  const filterCount = getTotalFiltersCount(selectedFilters);
  if (filterCount > 0) {
    title = (
      <div className='filterChipsAvailable'>
        {translation.filterByTitle}
        <div className='selectionWrap'>
          <HorizontalScroll>
            {getFilteredChips(selectedFilters, setSelectedFilter, translation)}{' '}
          </HorizontalScroll>
          {filterCount >= 3 ? (
            <span
              className='clearBtnSpan'
              onClick={onClearAllClick(clearAllFilters)}
              data-event-id={EVENT_NAME.DEVICE_CATALOG.EVENTS.CLEAR_ALL}
              data-event-message={translation.clearAll}
            >
              <Anchor hreflang={appConstants.LANGUAGE_CODE} title={translation.clearAll}>
                {translation.clearAll}
              </Anchor>
            </span>
          ) : null}
        </div>
      </div>
    );
  }

  return (
    <StyledFilterHeader>
      <Row>
        <Column
          colTabletPortrait={sortByList && sortByList.length !== 0 ? 9 : 12}
          className='filter-column'
        >
          <div className='filterBy light-filter-wrap'>
            {/* <Dropdown>Filter by</Dropdown> */}
            <Accordion
              className='accordion'
              getRef={setFilteredByRef}
              onClose={onClose}
              onOpen={onOpen}
              isOpen={isOpen}
              headerTitle={title}
              iconOpen='ec-triangle-up'
              iconClose='ec-triangle-down'
            />
          </div>
        </Column>
        {/* <Column colTabletPortrait={3}>
          <div className='compare'>
            <Switch on disabled onLabel='Compare' />
          </div>
        </Column> */}
        {sortByList && sortByList.length !== 0 && (
          <Column colTabletPortrait={3} className='price-column'>
            <div className='mostPopular popular-drop-wrap'>
              <Select
                className='select'
                placeholder={translation.sortByTitle}
                selectedItem={sortBy}
                onItemSelect={(item: ISortBy) => {
                  sendDropdownClickEvent(item.title);
                  setSortBy(item);
                }}
                items={sortByList}
                useNativeDropdown={isMobile.phone}
              />
              {/* <Dropdown>Most Popular</Dropdown> */}
            </div>
          </Column>
        )}
      </Row>
    </StyledFilterHeader>
  );
};

export default FilterHeader;
