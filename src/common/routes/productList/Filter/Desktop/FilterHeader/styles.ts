import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Column, Row } from '@common/components/Grid/styles';
import { hexToRgbA } from '@src/common/utils';

const variables = {
  background: hexToRgbA(colors.cloudGray, 0.4),
  padding: '1.25rem 1rem'
};

export const StyledFilterHeader = styled.div`
  border-left: 1px solid ${colors.semiLightGray};
  border-top: 1px solid ${colors.semiLightGray};
  border-bottom: 1px solid ${colors.semiLightGray};
  ${Row},
  ${Column} {
    margin: 0;
    padding: 0;
  }
  ${Column} {
    border-right: 1px solid ${colors.semiLightGray};
  }

  .filterBy,
  .compare,
  .mostPopular {
    font-size: 17.5px;
    line-height: 1.43;
    height: 4rem;
    display: flex;
    align-items: center;
  }

  .dropdown {
    padding: ${variables.padding};
    border: none !important;
  }

  .filterBy {
    .accordion {
      height: 100%;
      width: 100%;
      > div {
        height: 100%;
        padding: 0 1rem 0 2.25rem;
        font-size: 0.875rem;
        line-height: 1.43;
        > i {
          font-size: 0.55rem;
        }

        > div {
          height: 100%;
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
      }
    }

    .accordion .selectionWrap {
      height: 100%;
      display: flex;
      flex: 1;
      justify-content: space-between;

      .horizontalScrollWrap {
        margin: 0 0.5rem;
        flex: 1;
        .horizontalScroll {
          padding: 0.6rem 0.5rem;
          position: absolute;
          left: 0;
          max-width: 100%;
        }
      }

      .horizontalScrollWrap:after,
      .horizontalScrollWrap:before {
        content: '';
        width: 1rem;
        height: 100%;
        position: absolute;
        top: 0;
        z-index: 1;
      }
      .horizontalScrollWrap:after {
        background-image: linear-gradient(
          to right,
          rgba(255, 255, 255, 0),
          #fff
        );
        right: 0;
      }
      .horizontalScrollWrap:before {
        background-image: linear-gradient(
          to left,
          rgba(255, 255, 255, 0),
          #fff
        );
        left: 0;
      }

      .clearBtnSpan {
        display: inline-flex;
        a {
          font-size: 0.875rem;
        }
      }
    }
  }

  .compare {
    justify-content: space-between;
    background: ${variables.background};
    padding: ${variables.padding};
    > span {
      flex-direction: row-reverse;
      justify-content: space-between;
      width: 100%;
      align-items: center;
      font-size: 0.875rem;
      line-height: 1.43;
    }
  }
  .mostPopular {
    .select {
      background: ${variables.background};
      padding: 1rem;
      height: 100%;
      font-size: 0.875rem;
      line-height: 1.43;
      .optionsList {
        background: ${colors.white};
        padding: 0;
        border-radius: 8px;
        border: 1px solid ${colors.transparent};
        color: ${colors.darkGray};
        max-width: 8rem;
        margin-top: 0;
        left: 1.5rem;
        > div:hover {
          background: rgba(255, 255, 255, 0.2);
        }
        div {
          font-size: 0.875rem;
          line-height: 1.25rem;
          padding: 0.25rem 0.75rem;
          height: auto;
          font-weight: bold;
          margin-bottom: 0rem;
          /* text-transform: capitalize; */
          &:last-child {
            margin-bottom: 0;
          }
        }
      }
    }
  }
  .light-filter-wrap {
    .accordion {
      & > .accordionHeader {
        background: ${colors.white};
        padding: 0 1.5rem 0 2.25rem;
      }
      i {
        color: ${colors.magenta};
      }
      .accordionTitle {
        font-size: 0.875rem;
        color: ${colors.mediumGray};
        font-weight: normal;
        line-height: 1.25rem;
        margin-right: 3.15rem;
      }
      &.isOpen {
        .accordionTitle {
          color: ${colors.darkGray};
        }
      }
    }
    /* Added for #48 */
    .filterChipsContent {
      i {
        font-size: 0.75rem;
      }
    }
    .filterChipsAvailable {
      display: flex;
      width: 100%;
      height: 100%;
      align-items: center;
      color: ${colors.darkGray};
    }
    /* Added for #48 */
  }
  .popular-drop-wrap {
    width: 100%;
    .select {
      width: 100%;
      display: flex;
      align-items: center;
      background: ${colors.white};
      padding: 0;
      span {
        width: 100%;
        .styledSelectWrap {
          padding-left: 1.5rem;
          padding-right: 1.5rem;
          padding-top: 1.4rem;
          padding-bottom: 1.4rem;
        }
      }
      .styledSelect {
        font-size: 0.875rem;
        color: ${colors.mediumGray};
        font-weight: normal;
        line-height: 1.25rem;
      }
      .optionsList {
        top: 20px;
        width: calc(100% - 1.6rem);
        max-width: initial;
        left: 0.8rem;
        right: inherit;

        > div:hover {
          background: ${colors.fogGray};
        }
      }
    }
  }
  /* .filter-column {
    max-width: calc(100% - 14.7rem);
  }
  .price-column {
    max-width: 14.7rem;
  } */

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .light-filter-wrap {
      .accordion {
        & > .accordionHeader {
          padding-left: 3rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .light-filter-wrap {
      .accordion {
        & > .accordionHeader {
          padding-left: 4.9rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .light-filter-wrap {
      .accordion {
        & > .accordionHeader {
          padding-left: 5.5rem;
        }
      }
    }
  }
`;
