import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import FilterSection, {
  IProps
} from '@productList/Filter/Desktop/FilterSection';
import { Filter, SelectedFilters } from '@mocks/productList/productList.mock';
import appState from '@store/states/app';

describe('<FilterSection />', () => {
  const props: IProps = {
    translation: appState().translation.cart.productList,
    data: Filter,
    id: '12',
    type: 'filter',
    title: 'title',
    selectedFilter: SelectedFilters,
    setSelectedFilter: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterSection {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when selectedFilter is undefined', () => {
    const newProps: IProps = { ...props, selectedFilter: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterSection {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
