import React from 'react';
import { IProductListTranslation } from '@common/store/types/translation';

import ListGenerator from '../../common/ListGenerator';
import { IFilter, ISelectedFilters } from '../../../store/types';

import { StyledFilterSection } from './styles';

export interface IProps {
  data: IFilter[];
  id: string;
  type: string;
  title: string;
  selectedFilter?: ISelectedFilters['some_id'];
  className?: string;
  translation: IProductListTranslation;
  setSelectedFilter(filterId: string, filter: IFilter): void;
}

const FilterSection = (props: IProps) => {
  const {
    data,
    id,
    type,
    title,
    selectedFilter,
    setSelectedFilter,
    className,
    translation
  } = props;

  return (
    <StyledFilterSection className={className}>
      <div className='title'>{title}</div>
      <ListGenerator
        className='listItemsWrap'
        type={type}
        data={data}
        id={id}
        setSelectedFilter={setSelectedFilter}
        selectedFilters={selectedFilter}
        translation={translation}
      />
    </StyledFilterSection>
  );
};

export default FilterSection;
