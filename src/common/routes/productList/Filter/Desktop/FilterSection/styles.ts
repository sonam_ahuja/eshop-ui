import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledFilterSection = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;
  padding: 1.5rem 1.55rem;
  padding-left: 2.25rem;

  &.restFilters {
    height: auto;
  }

  .title {
    font-size: 0.625rem;
    line-height: 1.2;
    letter-spacing: 0.3px;

    padding-bottom: 1.5rem;
    text-transform: uppercase;

    color: ${colors.mediumGray};
  }

  .listItemsWrap {
    /* listLabelWithSuper START */
    &.listLabelWithSuper {
      display: flex;
      /* margin: -6px -4px; */
      .labelWithSuper {
        width: 6rem;
        margin: 6px 4px;
      }
    }
    /* listLabelWithSuper END */

    /* listVariantSwitcher START */
    &.listVariantSwitcher {
      .variantSwitcher {
      }
    }
    /* listVariantSwitcher END */
    /* Disabled Css */
    .variantSwitcher[disabled] {
      opacity: 0.3;
    }
    /* Disabled Css */
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .listItemsWrap {
      &.listLabelWithSuper {
        flex-wrap: wrap;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .listItemsWrap {
      &.listLabelWithSuper {
        flex-direction: column;
        flex-wrap: wrap;

        .labelWithSuper {
          width: auto;
          min-width: auto;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .listItemsWrap {
      &.listLabelWithSuper {
        flex-direction: row;

        .labelWithSuper {
          width: 50%;
          padding-right: 0.5rem;
          margin-right: 0;
          margin-left: 0;
        }
      }
    }
  }
`;
