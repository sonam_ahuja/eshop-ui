import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import DesktopFilters, { IProps } from '@productList/Filter/Desktop';
import {
  FilterCharacteristics,
  SelectedFilter,
  SortByList,
  SortedBy
} from '@mocks/productList/productList.mock';
import appState from '@store/states/app';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { RootState } from '@src/common/store/reducers';

describe('<DesktopFilters />', () => {
  const props: IProps = {
    filterCharacteristics: FilterCharacteristics,
    setFilterHeaderRef: jest.fn(),
    translation: appState().translation.cart.productList,
    selectedFilters: SelectedFilter,
    loading: false,
    sortBy: SortedBy,
    sortByList: SortByList,
    setSelectedFilter: jest.fn(),
    clearAllFilters: jest.fn(),
    setSortBy: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <DesktopFilters {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when sort by is null', () => {
    const newProps: IProps = { ...props, sortBy: null };
    const component = mount(
      <ThemeProvider theme={{}}>
        <DesktopFilters {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<DesktopFilters>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <DesktopFilters {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    const desktopFilter = component
      .find('DesktopFilters')
      .instance() as DesktopFilters;
    desktopFilter.openFilter();
    desktopFilter.closeFilter();
    desktopFilter.onOutsideClick({
      path: null
      // tslint:disable-next-line: no-any
    } as any);
    desktopFilter.onOutsideClick({
      path: [desktopFilter.state.filterByElRef]
      // tslint:disable-next-line: no-any
    } as any);
  });

  test('loading true condition test', () => {
    const newProps: IProps = { ...props, sortBy: null, loading: true };
    const component = mount(
      <ThemeProvider theme={{}}>
        <DesktopFilters {...newProps} />
      </ThemeProvider>
    );
    const desktopFilter = component
      .find('DesktopFilters')
      .instance() as DesktopFilters;
    desktopFilter.openFilter();
    desktopFilter.closeFilter();
    desktopFilter.onOutsideClick({
      path: [desktopFilter.state.filterByElRef]
      // tslint:disable-next-line: no-any
    } as any);
    expect(component).toMatchSnapshot();
  });
  test('null renders of values', () => {
    const newProps: IProps = { ...props, filterCharacteristics: [] };
    const component = mount(
      <ThemeProvider theme={{}}>
        <DesktopFilters {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
