import React, { Component, ReactNode } from 'react';
import { OutsideClick, Overlay } from 'dt-components';
import { getComposedPath } from '@client/DOMUtils';
import { Column, Row } from '@common/components/Grid/styles';
import { IProductListTranslation } from '@common/store/types/translation';
import {
  IFilter,
  IFilterCharacteristic,
  ISelectedFilters,
  ISortBy
} from '@productList/store/types';

import FilterSection from './FilterSection';
import { StyledDesktopFilters, StyledDesktopFilterWrap } from './styles';
import FilterHeader from './FilterHeader';

export interface IProps {
  loading: boolean;
  filterCharacteristics: IFilterCharacteristic[];
  selectedFilters: ISelectedFilters;
  sortBy: ISortBy | null;
  sortByList: { id: string; title: string }[];
  translation: IProductListTranslation;
  setSelectedFilter(filterId: string, filter: IFilter): void;
  clearAllFilters(): void;
  setSortBy(sortBy: ISortBy): void;
  setFilterHeaderRef(ref: HTMLElement | null): void;
}

interface IState {
  isFilterOpen: boolean;
  filterByElRef: HTMLElement | null;
}

export default class DesktopFilters extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isFilterOpen: false,
      filterByElRef: null
    };
    this.openFilter = this.openFilter.bind(this);
    this.closeFilter = this.closeFilter.bind(this);
    this.onOutsideClick = this.onOutsideClick.bind(this);
    this.setFilteredByRef = this.setFilteredByRef.bind(this);
  }

  openFilter(): void {
    this.setState({
      isFilterOpen: true
    });
  }

  closeFilter(): void {
    this.setState({
      isFilterOpen: false
    });
  }

  onOutsideClick(e: Event): void {
    if (
      (getComposedPath(e) || []).includes(this.state
        .filterByElRef as HTMLElement)
    ) {
      return;
    }
    if (!this.state.isFilterOpen || this.props.loading) {
      return;
    }
    this.setState({
      ...this.state,
      isFilterOpen: false
    });
  }

  setFilteredByRef(filterByElRef: HTMLElement | null): void {
    this.setState({
      ...this.state,
      filterByElRef
    });
  }

  render(): ReactNode {
    const {
      filterCharacteristics,
      setSelectedFilter,
      selectedFilters,
      clearAllFilters,
      sortBy,
      setSortBy,
      sortByList,
      translation
    } = this.props;
    const { isFilterOpen } = this.state;
    const devices = filterCharacteristics[0]; // brand
    const priceRange = filterCharacteristics[1]; // color
    const color = filterCharacteristics[3]; // storage
    const availability = filterCharacteristics[2]; // priceRange
    const storage = filterCharacteristics[4]; // availability
    const restFilters = filterCharacteristics.slice(5);

    const filterLayout = (
      <div className='filterLayout'>
        <Overlay
          overlayStyle={{
            background: 'rgba( 255, 255 ,255 ,0.98)',
            zIndex: -1
          }}
          backgroundScroll={true}
          open={true}
          // tslint:disable-next-line:no-empty
          onClose={() => { }}
        >
          <OutsideClick clickHandler={this.onOutsideClick}>
            <Row>
              <Column
                className='firstColumn'
                colTabletPortrait={12}
                colTabletLandscape={3}
              >
                {devices ? (
                  <FilterSection
                    title={devices.name}
                    type={devices.type}
                    id={devices.id}
                    data={devices.data}
                    selectedFilter={selectedFilters[devices.id]}
                    setSelectedFilter={setSelectedFilter}
                    translation={translation}
                  />
                ) : null}
              </Column>
              <Column className='columnWrap' colTabletPortrait={12} colTabletLandscape={3}>
                <Row>
                  <Column colTabletPortrait={6} colTabletLandscape={12}>
                    {priceRange ? (
                      <FilterSection
                        title={priceRange.name}
                        id={priceRange.id}
                        type={priceRange.type}
                        data={priceRange.data}
                        selectedFilter={selectedFilters[priceRange.id]}
                        setSelectedFilter={setSelectedFilter}
                        translation={translation}
                      />

                    ) : null}
                  </Column>
                  <Column colTabletPortrait={6} colTabletLandscape={12}>
                    {color ? (
                      <FilterSection
                        title={color.name}
                        type={color.type}
                        id={color.id}
                        data={color.data}
                        selectedFilter={selectedFilters[color.id]}
                        setSelectedFilter={setSelectedFilter}
                        translation={translation}
                      />
                    ) : null}
                  </Column>
                </Row>
              </Column>

              <Column
                colTabletPortrait={12}
                colTabletLandscape={3}
              >
                {availability ? (
                  <FilterSection
                    title={availability.name}
                    type={availability.type}
                    id={availability.id}
                    data={availability.data}
                    selectedFilter={selectedFilters[availability.id]}
                    setSelectedFilter={setSelectedFilter}
                    translation={translation}
                  />
                ) : null}
              </Column>

              <Column
                colTabletPortrait={12}
                colTabletLandscape={3}
              >
                {storage ? (
                  <FilterSection
                    title={storage.name}
                    type={storage.type}
                    id={storage.id}
                    data={storage.data}
                    selectedFilter={selectedFilters[storage.id]}
                    setSelectedFilter={setSelectedFilter}
                    translation={translation}
                  />
                ) : null}
              </Column>
            </Row>
            {restFilters.length > 0 ? (
              <Row>
                <Column colTabletPortrait={12}>
                  {restFilters.map(filter => (
                    <FilterSection
                      className='restFilters'
                      title={filter.name}
                      type={filter.type}
                      id={filter.id}
                      data={filter.data}
                      selectedFilter={selectedFilters[filter.id]}
                      setSelectedFilter={setSelectedFilter}
                      translation={translation}
                    />
                  ))}
                </Column>
              </Row>
            ) : null}
          </OutsideClick>
        </Overlay>
      </div>
    );

    return (
      <StyledDesktopFilterWrap ref={this.props.setFilterHeaderRef}>
        <StyledDesktopFilters className='filterContent'>
          <FilterHeader
            setFilteredByRef={this.setFilteredByRef}
            selectedFilters={selectedFilters}
            isOpen={isFilterOpen}
            onOpen={this.openFilter}
            onClose={this.closeFilter}
            setSelectedFilter={setSelectedFilter}
            clearAllFilters={clearAllFilters}
            sortBy={sortBy}
            translation={translation}
            setSortBy={setSortBy}
            sortByList={sortByList}
          />
          {this.state.isFilterOpen ? filterLayout : null}
        </StyledDesktopFilters>
      </StyledDesktopFilterWrap>
    );
  }
}
