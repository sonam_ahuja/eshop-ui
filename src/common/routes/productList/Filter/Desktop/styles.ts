import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Column, Row } from '@common/components/Grid/styles';
import { hexToRgbA } from '@src/common/utils';

import { StyledFilterSection } from './FilterSection/styles';

export const StyledDesktopFilterWrap = styled.div`
  min-height: 4.1rem;
  width: 100%;
`;

export const StyledDesktopFilters = styled.div`
  background: ${colors.white};
  position: relative;
  z-index: 2;
  width: 100%;
  margin: auto;
  &.sticky {
    max-width: ${breakpoints.desktopLarge}px;
    position: fixed;
    z-index: 300;
    margin: 0;
    top: 0;
    .filterLayout {
      .dt_overlay {
        top: 4rem;
      }
    }
  }
  .filterLayout {
    position: absolute;
    width: 100%;
    left: 0;
    background: ${hexToRgbA(colors.black, 0.6)};
    height: 100vh; /********* for overlay visible***********/
    /* z-index: 300; */
    ${Row} {
      padding: 0;
      margin: 0;
      background: ${hexToRgbA(colors.white, 0.98)};
      ${Column} {
        padding: 0;
        margin: 0;
        border-right: 1px solid ${colors.semiLightGray};
        border-bottom: 1px solid ${colors.semiLightGray};
        overflow-y: auto;
      }
    }

    .columnWrap {
      border: none !important;
    }
    div::selection {
      background: transparent;
    }
    /******* Add backdrop while opening filter *******/
    .dt_overlay {
      top: 10.3rem;
      right: 0;
      display: block;
      position: initial;
      overflow-y: auto;
      max-height: calc(100vh - 80px);
      border-left: 1px solid ${colors.semiLightGray};
    }
    /******* Add backdrop while opening filter *******/
  }

  .isOpen {
    div::selection {
      background: transparent;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    &.sticky {
      max-width: calc(100% - 5.75rem);
    }
    ${Row} {
      ${Column} {
        max-height: 21.875rem;
        ${Row} {
          height: 100%;
          ${Column} {
            min-height: 50%;
            max-height: 50%;
          }
        }
      }
    }

    .filterLayout {
      ${StyledFilterSection} {
        padding-left: 1rem;
      }
      .firstColumn {
        ${StyledFilterSection} {
          padding-left: 2.25rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    &.sticky {
      max-width: calc(100% - 4.75rem);
    }
    .filterLayout {
      ${StyledFilterSection} {
        padding-left: 1.55rem;
      }
      .firstColumn {
        ${StyledFilterSection} {
          padding-left: 4.9rem;
        }
      }
      /******* Add backdrop while opening filter *******/
      .dt_overlay {
        right: 4.75rem;
      }
      /******* Add backdrop while opening filter *******/
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    &.sticky {
      /* max-width: calc(${breakpoints.desktopLarge}px - 5.75rem); */
      max-width: ${breakpoints.desktopLarge}px;
    }
    .filterLayout {
      .firstColumn {
        ${StyledFilterSection} {
          padding-left: 5.5rem;
        }
      }
    }
  }
`;
