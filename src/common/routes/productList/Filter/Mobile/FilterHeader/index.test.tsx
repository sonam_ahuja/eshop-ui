import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import FilterHeader, { IProps } from '@productList/Filter/Mobile/FilterHeader';
import appState from '@store/states/app';

describe('<FilterHeader />', () => {
  const props: IProps = {
    translation: appState().translation.cart.productList,
    onClick: jest.fn(),
    filterCount: 0
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <FilterHeader {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
