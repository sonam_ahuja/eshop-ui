import React from 'react';
import { Accordion } from 'dt-components';
import { Column, Row } from '@common/components/Grid/styles';
import { IProductListTranslation } from '@common/store/types/translation';
import FilterCounter from '@productList/Filter/common/FilterCounter';

import { StyledFilterHeader } from './styles';

export interface IProps {
  translation: IProductListTranslation;
  filterCount: number;
  onClick(): void;
}

const FilterHeader = (props: IProps) => {
  const { translation, filterCount } = props;

  return (
    <StyledFilterHeader onClick={props.onClick}>
      <Row>
        <Column colTabletPortrait={6}>
          <div className='filterBy light-filter-wrap'>
            {/* <Dropdown>Filter by</Dropdown> */}
            <Accordion
              isOpen={false}
              className='accordion'
              headerTitle={
                <FilterCounter
                  translation={translation}
                  filterCount={filterCount}
                />
              }
              iconOpen='ec-triangle-down'
              iconClose='ec-triangle-down'
            />
          </div>
        </Column>
        {/* <Column colTabletPortrait={6}>
          <div className='compare'>
            <Switch on disabled onLabel='Compare' />
          </div>
        </Column> */}
      </Row>
    </StyledFilterHeader>
  );
};

export default FilterHeader;
