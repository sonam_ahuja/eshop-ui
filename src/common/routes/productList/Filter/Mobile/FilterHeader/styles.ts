import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { Column, Row } from '@common/components/Grid/styles';

const variables = {
  padding: '1.25rem 1rem'
};

export const StyledFilterHeader = styled.div`
  border-left: 1px solid ${colors.semiLightGray};
  border-top: 1px solid ${colors.semiLightGray};
  border-bottom: 1px solid ${colors.semiLightGray};
  ${Row},
  ${Column} {
    margin: 0;
    padding: 0;
  }

  .filterBy,
  .compare {
    font-size: 1.09375rem;
    line-height: 1.43;
    height: 4rem;
    display: flex;
    align-items: center;
    background: ${colors.white};
  }

  .filterBy {
    border-right: 0.0625rem solid ${colors.semiLightGray};
    opacity: 0.98;
    .accordion {
      height: 100%;
      width: 100%;
      i {
        font-size: 0.5rem;
      }
      > div {
        height: 100%;
        padding: 0 1.25rem 0 1.25rem;
        font-size: 0.875rem;
        line-height: 1.43;

        > div {
          height: 100%;
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
      }
    }
  }

  .compare {
    justify-content: space-between;
    padding: ${variables.padding};
    > span {
      flex-direction: row-reverse;
      justify-content: space-between;
      width: 100%;
      align-items: center;
      font-size: 0.875rem;
      line-height: 1.43;
    }
  }
  .light-filter-wrap {
    .accordion {
      & > div {
        background: ${colors.white};
        padding: 0 1.25rem 0 1.25rem;
      }
      i {
        color: ${colors.magenta};
        font-size:0.6875rem;
      }
      .accordionTitle {
        font-size: 0.875rem;
        color: ${colors.darkGray};
        font-weight: normal;
        line-height: 1.25rem;
      }
    }
  }
`;
