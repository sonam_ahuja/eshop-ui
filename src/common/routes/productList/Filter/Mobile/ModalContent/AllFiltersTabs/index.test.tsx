import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import AllFiltersTabs, {
  IProps
} from '@productList/Filter/Mobile/ModalContent/AllFiltersTabs';
import {
  FilterCharacteristics,
  SelectedFilter
} from '@mocks/productList/productList.mock';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
const mockStore = configureStore();

describe('<AllFiltersTabs />', () => {
  const props: IProps = {
    translation: appState().translation.cart.productList,
    className: 'someClassName',
    loading: false,
    resultCount: 10,
    filterCharacteristics: FilterCharacteristics,
    selectedFilters: SelectedFilter,
    setSelectedFilter: jest.fn(),
    submitFilters: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <AllFiltersTabs {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when className is undefined', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <AllFiltersTabs {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when loading is true', () => {
    const newProps: IProps = { ...props, loading: true };
    const component = mount(
      <ThemeProvider theme={{}}>
        <AllFiltersTabs {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<AllFiltersTabs>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <AllFiltersTabs {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('AllFiltersTabs')
      .instance() as AllFiltersTabs).setListGeneratorRef(null);

    (component.find('AllFiltersTabs').instance() as AllFiltersTabs).onSelect(2);
  });
});
