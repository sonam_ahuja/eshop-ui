import React, { Component, ReactNode } from 'react';
import { Tab, Tabs } from 'dt-components';
import { StyledHorizontalScrollWrap } from '@productList/Filter/common/HorizontalScroll/styles';
import {
  IFilter,
  IFilterCharacteristic,
  ISelectedFilters
} from '@productList/store/types';
import ListGenerator from '@productList/Filter/common/ListGenerator';
import { IProductListTranslation } from '@common/store/types/translation';

import { StyledAllFiltersTabs } from './styles';

export interface IProps {
  className?: string;
  loading: boolean;
  resultCount: number;
  filterCharacteristics: IFilterCharacteristic[];
  selectedFilters: ISelectedFilters;
  translation: IProductListTranslation;
  setSelectedFilter(filterId: string, filter: IFilter): void;
  submitFilters(): void;
}

export interface IState {
  selectedTabIndex: number;
}

export default class AllFiltersTabs extends Component<IProps, IState> {
  listGeneratorRef: HTMLElement | null = null;

  constructor(props: IProps) {
    super(props);
    this.state = {
      selectedTabIndex: 0
    };
    this.onSelect = this.onSelect.bind(this);
    this.setListGeneratorRef = this.setListGeneratorRef.bind(this);
  }

  setListGeneratorRef(ref: HTMLElement | null): void {
    this.listGeneratorRef = ref;
  }

  onSelect(idx: number): void {
    this.setState(
      {
        selectedTabIndex: idx
      },
      () => {
        if (this.listGeneratorRef) {
          const el = this.listGeneratorRef.querySelector(
            '.tab-content .horizontalScroll'
          );
          if (el) {
            el.scrollTo(0, 0);
          }
        }
      }
    );
  }

  render(): ReactNode {
    const {
      className,
      filterCharacteristics,
      selectedFilters,
      setSelectedFilter,
      translation
    } = this.props;

    const filters = filterCharacteristics.map(
      (filter: IFilterCharacteristic, idx: number) => (
        <Tab key={idx} label={filter.name}>
          <ListGenerator
            type={filter.type}
            data={filter.data}
            id={filter.id}
            selectedFilters={selectedFilters[filter.id]}
            setSelectedFilter={setSelectedFilter}
            translation={translation}
          />
        </Tab>
      )
    );

    return (
      <StyledAllFiltersTabs
        ref={this.setListGeneratorRef}
        className={className}
      >
        <StyledHorizontalScrollWrap className='StyledHorizontalScrollWrap'>
          {filters.length > 0 ? (
            <Tabs
              onSelect={this.onSelect}
              selected={this.state.selectedTabIndex}
              className='tabsWrap'
            >
              {filters}
            </Tabs>
          ) : null}
        </StyledHorizontalScrollWrap>
      </StyledAllFiltersTabs>
    );
  }
}
