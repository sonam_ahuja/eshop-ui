import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledButtonWrap = styled.div`
  width: 100%;
  button {
    width: 100%;
  }
`;

export const StyledAllFiltersTabs = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  background: ${colors.white};

  /* tab links and tab content Style START */
  .navTabs {
    justify-content: flex-start;

    .horizontalScroll {
      .overflowContainer {
        padding: 1.75rem 1.25rem 0 1.25rem;
        margin: 0 -0.625rem;

        .navItem {
          width: auto;
          color: ${colors.mediumGray};
          text-transform: uppercase;
          margin: 0 0.625rem;
          font-size: 0.875rem;
          font-weight: 500;
          line-height: 1.43;

          &.active {
            span {
              color: ${colors.magenta};
            }
          }
        }
      }
    }
  }

  .tab-content {
    .horizontalScroll {
      min-width: 100%;
      .overflowContainer {
        padding: 0 1.25rem;
        min-width: 100%;
      }
    }
  }
  /* tab links and tab content Style END */

  .StyledHorizontalScrollWrap {
    flex: 1;

    .tabsWrap {
      display: flex;
      flex-direction: column;
      height: 100%;
      position: absolute;
      width: 100%;

      .tab-content {
        flex: 1;
        display: flex;
        align-items: center;
        /* fixed for adjusting viewport height form brands */
        /* padding: 0 0 1rem; */
        padding: 0;

        /* HACK */
        .overflowContainer {
          > div {
            width: 100%;
            text-align: center;
          }
        }
      }
    }
  }

  /* all filter content */
  .tab-content {
    /* classes are setting in ListGenerator component */
    .listLabelWithSuper {
      display: flex;
      flex-direction: column;
      flex-wrap: wrap;
      /* fixed for adjusting viewport height form brands */
      /* height: 9.375rem;*/
      height: 18vh;
      margin: -0.5rem;

      .labelWithSuper {
        /* padding: 0.625rem 0.5rem; */
        padding: 0.25rem 0.5rem;
        .innerText {
          font-size: 1.09375rem;
          font-weight: 500;
          font-style: normal;
          line-height: 1.43;
          letter-spacing: normal;

          /* &::first-letter {
            text-transform: uppercase;
          } */
          sup {
            font-size: 12.5px;
            line-height: 1.2;
            margin-left: 2px;
          }
        }
      }
    }

    .listVariantSwitcher {
      .variantSwitcher {
        height: 56px;
        width: 56px;

        &:after {
          height: 40px;
          width: 40px;
        }
      }
    }

    /* listVariantSwitcher END */
    /* Disabled Css */
    .variantSwitcher[disabled] {
      opacity: 0.3;
    }
    /* Disabled Css */
  }
`;
