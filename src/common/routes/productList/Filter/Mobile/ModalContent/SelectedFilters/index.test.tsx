import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SelectedFilters, {
  IProps
} from '@productList/Filter/Mobile/ModalContent/SelectedFilters';
import { SelectedFilter } from '@mocks/productList/productList.mock';
import appState from '@store/states/app';

describe('<AllFiltersTabs />', () => {
  const props: IProps = {
    filterByText: '',
    className: 'someClassName',
    selectedFilters: SelectedFilter,
    translation: appState().translation.cart.productList,
    setSelectedFilter: jest.fn(),
    clearAllFilters: jest.fn(),
    clearAllText: 'Clear All'
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <SelectedFilters {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when className is undefined', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <SelectedFilters {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
