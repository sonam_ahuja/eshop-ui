import React from 'react';
import { Anchor, FilterChips, Paragraph, Title } from 'dt-components';
import HorizontalScroll from '@productList/Filter/common/HorizontalScroll';
import { IFilter, ISelectedFilters } from '@productList/store/types';
import {
  addCurrencyCodeInPriceRange,
  getTotalFiltersCount
} from '@productList/utils';
import APP_KEYS from '@common/constants/appkeys';
import { IProductListTranslation } from '@common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import appConstants from '@src/common/constants/appConstants';

import { StyledSelectedFilters } from './styles';

export interface IProps {
  className?: string;
  selectedFilters: ISelectedFilters;
  clearAllText: string;
  filterByText: string;
  translation: IProductListTranslation;
  clearAllFilters(): void;
  setSelectedFilter(filterId: string, filter: IFilter): void;
}

export interface IFilterChipProp extends IFilter {
  filterId: string;
}

const onFilterChipClick = (
  filterId: string,
  filter: IFilter,
  setSelectedFilter: IProps['setSelectedFilter']
) => {
  return () => setSelectedFilter(filterId, filter);
};

const SelectedFilters = (props: IProps) => {
  const {
    className,
    selectedFilters,
    setSelectedFilter,
    clearAllFilters,
    clearAllText,
    filterByText,
    translation
  } = props;
  let filters: IFilterChipProp[] = [];
  const totalFiltersCount = getTotalFiltersCount(selectedFilters);
  Object.keys(selectedFilters).forEach(filterId => {
    const tempFilters: IFilterChipProp[] = [];

    (selectedFilters[filterId] as IFilterChipProp[]).forEach(
      (item: IFilterChipProp) => {
        if (item.quantity && item.quantity > 0) {
          item.filterId = filterId;
          // price range is exception where we are showing value instead of name as label
          if (filterId === APP_KEYS.BFF_PRICE_RANGE_ID) {
            tempFilters.push({
              ...item,
              name: addCurrencyCodeInPriceRange(
                item.value,
                translation.priceRangeAboveFilter
              )
            });
          } else if (filterId === APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID) {
            tempFilters.push({
              ...item,
              name: translation.availability[item.value]
            });
          } else {
            tempFilters.push(item);
          }
        }
      }
    );
    filters = filters.concat(tempFilters);
  });
  const filterChips = filters.map((filter, index) => (
    <FilterChips
      key={index}
      disabled={filter.quantity === 0}
      cancelable={true}
      onClose={onFilterChipClick(filter.filterId, filter, setSelectedFilter)}
      label={filter.name}
      active={true}
    />
  ));

  return (
    <StyledSelectedFilters className={className}>
      <Title
        className='title'
        size='xsmall'
        weight='bold'
        children={filterByText}
      />

      <HorizontalScroll>
        {totalFiltersCount >= 3 ? (
          <Anchor
            data-event-id={EVENT_NAME.DEVICE_CATALOG.EVENTS.CLEAR_ALL}
            data-event-message={translation.clearAll}
            hreflang={appConstants.LANGUAGE_CODE}
            title={clearAllText}
            className='clearAll'
            onClickHandler={() => {
              clearAllFilters();
            }}
          >
            <Paragraph
              size='small'
              weight='normal'
              firstLetterTransform='uppercase'
            >
              {clearAllText}
            </Paragraph>
          </Anchor>
        ) : null}

        {filterChips}
        {/* <FilterChips disable cancelable={false} label='Apple' />
        <FilterChips active label='Samsung' />
        <FilterChips label='Blackberry' />
        <FilterChips active cancelable label='LG' />
        <FilterChips label='Nokia' />
        <FilterChips label='Asus' />
        <FilterChips label='Apple' />
        <FilterChips label='Samsung' />
        <FilterChips label='Blackberry' />
        <FilterChips label='LG' />
        <FilterChips label='Nokia' />
        <FilterChips label='Asus' />
        <FilterChips label='Apple' />
        <FilterChips label='Samsung' />
        <FilterChips label='Blackberry' />
        <FilterChips label='LG' />
        <FilterChips label='Nokia' />
        <FilterChips label='Asus' /> */}
      </HorizontalScroll>
    </StyledSelectedFilters>
  );
};

export default SelectedFilters;
