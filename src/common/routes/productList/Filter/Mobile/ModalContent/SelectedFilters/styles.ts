import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const StyledSelectedFilters = styled.div`
  background: ${colors.cloudGray};
  padding: 1.75rem 1.25rem 0;
  height: 10rem;

  .title {
    line-height: 1.2;
    /* &::first-letter {
      text-transform: uppercase;
    } */
  }

  .horizontalScrollWrap {
    margin-top: 2.25rem;
    /* TODONEW */

    .horizontalScroll {
      margin: 0 -1.25rem;
      margin-left: 0;
      padding: 0 1.25rem;
      padding-left: 0;
      .overflowContainer {
        .clearAll {
          position: sticky;
          height: 2.75rem;
          cursor: pointer;
          left: 0;
          background: ${colors.cloudGray};
          padding-right: 0.5rem;
          margin-right: 0.75rem;

          &:before {
            content: '';
            position: absolute;
            height: 100%;
            width: 1rem;
            background-image: linear-gradient(
              to left,
              ${hexToRgbA(colors.cloudGray, 0.01)},
              ${colors.cloudGray}
            );
            right: -1rem;
          }
        }
      }
    }
  }
`;
