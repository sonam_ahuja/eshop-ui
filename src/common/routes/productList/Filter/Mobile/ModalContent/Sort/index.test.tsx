import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Sort, { IProps } from '@productList/Filter/Mobile/ModalContent/Sort';
import { SortByList, SortedBy } from '@mocks/productList/productList.mock';
import appState from '@store/states/app';

describe('<AllFiltersTabs />', () => {
  const props: IProps = {
    className: 'someClassName',
    sortBy: SortedBy,
    title: 'title',
    translation: appState().translation.cart.productList,
    sortByList: SortByList,
    setSortBy: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Sort {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when className is undefined', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Sort {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when sortBy is null', () => {
    const newProps: IProps = { ...props, sortBy: null };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Sort {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
