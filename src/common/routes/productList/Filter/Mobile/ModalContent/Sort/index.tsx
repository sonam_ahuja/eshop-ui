import React from 'react';
import { Select } from 'dt-components';
import { ISortBy } from '@productList/store/types';
import { IProductListTranslation } from '@common/store/types/translation';
import { sendDropdownClickEvent } from '@src/common/events/common';

import { StyledSort } from './styles';

export interface IProps {
  className?: string;
  sortBy: ISortBy | null;
  sortByList: ISortBy[];
  title: string;
  translation: IProductListTranslation;
  setSortBy(sortBy: ISortBy): void;
}

const Sort = (props: IProps) => {
  const {
    className,
    sortBy,
    sortByList,
    setSortBy,
    title,
    translation
  } = props;

  return (
    <StyledSort className={className}>
      <div className='label'>{translation.sortByDropdownLabel}</div>
      <Select
        className='selectSort'
        placeholder={title}
        selectedItem={sortBy}
        onItemSelect={(item: ISortBy) => {
          sendDropdownClickEvent(item.title);
          setSortBy(item);
        }}
        items={sortByList}
        useNativeDropdown={true}
      />
    </StyledSort>
  );
};

export default Sort;
