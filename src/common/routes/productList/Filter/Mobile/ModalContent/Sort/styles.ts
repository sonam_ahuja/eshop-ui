import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledSort = styled.div`
  width: 100%;
  background: ${colors.white};

  .label {
    font-size: 0.875rem;
    line-height: 1.2;
    letter-spacing: 0.01875rem;
    text-transform: uppercase;
    color: ${colors.mediumGray};
    padding: 1rem 1.25rem 1.5rem 1.25rem;
  }

  .selectSort {
    line-height: 1.11;
    padding: 0 1.25rem 1.5rem 1.25rem;
    border-bottom: 0.0625rem solid ${colors.warmGray};

    .styledSelect {
      font-size: 1.125rem;
      color: ${colors.darkGray};
    }
  }
`;
