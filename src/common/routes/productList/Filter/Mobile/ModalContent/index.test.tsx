import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import ModalContent, { IProps } from '@productList/Filter/Mobile/ModalContent';
import {
  FilterCharacteristics,
  SelectedFilter,
  SortByList,
  SortedBy
} from '@mocks/productList/productList.mock';
import appState from '@store/states/app';

describe('<ModalContent />', () => {
  const props: IProps = {
    filterCharacteristics: FilterCharacteristics,
    translation: appState().translation.cart.productList,
    selectedFilters: SelectedFilter,
    sortBy: SortedBy,
    sortByList: SortByList,
    resultCount: 10,
    loading: true,
    setSelectedFilter: jest.fn(),
    clearAllFilters: jest.fn(),
    setSortBy: jest.fn(),
    submitFilters: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <ModalContent {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when sortBy is null', () => {
    const newProps: IProps = { ...props, sortBy: null };
    const component = mount(
      <ThemeProvider theme={{}}>
        <ModalContent {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
