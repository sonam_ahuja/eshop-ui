import React from 'react';
import { IProductListTranslation } from '@common/store/types/translation';
import {
  IFilter,
  IFilterCharacteristic,
  ISelectedFilters,
  ISortBy
} from '@productList/store/types';
import { Button } from 'dt-components';
import EVENT_NAME from '@events/constants/eventName';

import { StyledMobileFilters } from '../styles';

import SelectedFilters from './SelectedFilters';
import AllFiltersTabs from './AllFiltersTabs';
import Sort from './Sort';

export interface IProps {
  filterCharacteristics: IFilterCharacteristic[];
  selectedFilters: ISelectedFilters;
  sortBy: ISortBy | null;
  sortByList: ISortBy[];
  resultCount: number;
  loading: boolean;
  translation: IProductListTranslation;
  setSelectedFilter(filterId: string, filter: IFilter): void;
  setSortBy(sortBy: ISortBy): void;
  // closeModal(): void;
  submitFilters(): void;
  clearAllFilters(): void;
}
export default (props: IProps) => {
  const {
    // closeModal,
    selectedFilters,
    setSelectedFilter,
    sortBy,
    setSortBy,
    sortByList,
    filterCharacteristics,
    submitFilters,
    loading,
    translation,
    clearAllFilters,
    resultCount
  } = props;

  const results =
    resultCount < 2 ? translation.singularViewResults : translation.viewResults;

  const viewResultText = results.replace('{0}', String(resultCount));

  return (
    <StyledMobileFilters>
      {/* <CloseButton onCloseClick={closeModal} /> */}
      <SelectedFilters
        selectedFilters={selectedFilters}
        setSelectedFilter={setSelectedFilter}
        clearAllFilters={clearAllFilters}
        clearAllText={translation.clearAll}
        filterByText={translation.filterByTitle}
        translation={translation}
        className='SelectedFilters'
      />
      {sortByList && sortByList.length !== 0 && (
        <Sort
          className='Sort'
          setSortBy={setSortBy}
          title={translation.sortByTitle}
          sortBy={sortBy}
          sortByList={sortByList}
          translation={translation}
        />
      )}
      <AllFiltersTabs
        filterCharacteristics={filterCharacteristics}
        selectedFilters={selectedFilters}
        setSelectedFilter={setSelectedFilter}
        className='AllFiltersTabs'
        submitFilters={submitFilters}
        loading={loading}
        resultCount={resultCount}
        translation={translation}
      />
      <Button
        onClickHandler={submitFilters}
        disabled={loading}
        data-event-id={EVENT_NAME.DEVICE_CATALOG.EVENTS.VIEW_RESULT}
        data-event-message={
          loading ? translation.filterLoadingText : viewResultText
        }
      >
        {loading ? translation.filterLoadingText : viewResultText}
      </Button>
    </StyledMobileFilters>
  );
};
