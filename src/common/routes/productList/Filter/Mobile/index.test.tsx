import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import MobileFilters, { IProps } from '@productList/Filter/Mobile';
import configureStore from 'redux-mock-store';
import { RootState } from '@src/common/store/reducers';
import {
  FilterCharacteristics,
  SelectedFilter,
  SortByList,
  SortedBy
} from '@mocks/productList/productList.mock';
import appState from '@store/states/app';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

// tslint:disable
describe('<MobileFilters />', () => {
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const props: IProps = {
    history: {
      length: 2,
      action: 'POP',
      location: {
        pathname: '/basket',
        search: '',
        hash: '',
        key: 'hmi41z',
        state: ''
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      listen: jest.fn(),
      createHref: jest.fn()
    },
    location: {
      pathname: '/basket',
      search: '',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    match: {
      path: '/basket',
      url: '/basket',
      isExact: true,
      params: {
        match: '',
        history: '',
        location: ''
      }
    },
    setFilterHeaderRef: jest.fn(),
    filterCharacteristics: FilterCharacteristics,
    translation: appState().translation.cart.productList,
    selectedFilters: SelectedFilter,
    sortBy: SortedBy,
    resultCount: 10,
    loading: true,
    categoryId: '1234',
    sortByList: SortByList,
    setSelectedFilter: jest.fn(),
    setSortBy: jest.fn(),
    setTempFilters: jest.fn(),
    setFiltersFromTempFilters: jest.fn(),
    submitFilters: jest.fn(),
    clearAllFilters: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <MemoryRouter>
        <ThemeProvider theme={{}}>
          <MobileFilters {...props} />
        </ThemeProvider>
      </MemoryRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when sortBy is null', () => {
    const newProps: IProps = { ...props, sortBy: null };
    const component = mount(
      <MemoryRouter>
        <ThemeProvider theme={{}}>
          <MobileFilters {...newProps} />
        </ThemeProvider>
      </MemoryRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when sortBy is null', () => {
    const newProps: IProps = { ...props, sortBy: null };
    const component = mount(
      <MemoryRouter>
        <ThemeProvider theme={{}}>
          <MobileFilters {...newProps} />
        </ThemeProvider>
      </MemoryRouter>
    );
    component.setState({ isOpen: true });
    component.update();
    expect(component).toMatchSnapshot();
  });

  test('handle close filter trigger ', () => {
    const store = mockStore(initStateValue);
    const newProps = { ...props };
    newProps.loading = false;
    const component = mount<typeof MobileFilters>(
      <MemoryRouter>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileFilters {...newProps} />
          </Provider>
        </ThemeProvider>
      </MemoryRouter>
    );

    component
      .find('MobileFilters')
      .instance()
      ['closeFilters']();
  });
  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<typeof MobileFilters>(
      <MemoryRouter>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MobileFilters {...props} />
          </Provider>
        </ThemeProvider>
      </MemoryRouter>
    );

    component
      .find('MobileFilters')
      .instance()
      ['openFilters']();
    component
      .find('MobileFilters')
      .instance()
      ['closeFilters']();
    component
      .find('MobileFilters')
      .instance()
      ['submitFilters']();
    component
      .find('MobileFilters')
      .instance()
      ['setSelectedFilter']('filterId', {
        name: 'string',
        default: true,
        value: 'string',
        quantity: 2
      });
    component
      .find('MobileFilters')
      .instance()
      ['setSortBy']({
        id: 'string',
        title: 'string'
      });
  });
});
