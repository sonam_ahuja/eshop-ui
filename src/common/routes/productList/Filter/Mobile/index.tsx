import React, { Component, ReactNode } from 'react';
import { IProductListTranslation } from '@common/store/types/translation';
import axios, { CancelTokenSource } from 'axios';
import {
  IFilter,
  IFilterCharacteristic,
  ISelectedFilters,
  ISortBy
} from '@productList/store/types';
import { sendMostPopularEvent } from '@events/DeviceList/index';
import { getTotalFiltersCount } from '@productList/utils';
import { RouteComponentProps, withRouter } from 'react-router';

import {
  FilterFlowPanel,
  StyledFilterInner,
  StyledFilterWrap,
  StyledMobileFilters
} from './styles';
import ModalContent from './ModalContent';
import FilterHeader from './FilterHeader';

export interface IProps extends RouteComponentProps {
  categoryId: string;
  filterCharacteristics: IFilterCharacteristic[];
  selectedFilters: ISelectedFilters;
  sortBy: ISortBy | null;
  resultCount: number;
  loading: boolean;
  translation: IProductListTranslation;
  sortByList: { id: string; title: string }[];
  setSelectedFilter(
    filterId: string,
    filter: IFilter,
    categoryId: string,
    signal: CancelTokenSource
  ): void;
  setSortBy(sortBy: ISortBy): void;
  setTempFilters(): void;
  setFiltersFromTempFilters(): void;
  submitFilters(): void;
  clearAllFilters(categoryId: string): void;
  setFilterHeaderRef(el: HTMLElement | null): void;
}
interface IState {
  isOpen: boolean;
  signal: CancelTokenSource;
}
class MobileFilters extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isOpen: false,
      signal: axios.CancelToken.source()
    };
    this.openFilters = this.openFilters.bind(this);
    this.closeFilters = this.closeFilters.bind(this);
    this.submitFilters = this.submitFilters.bind(this);
    this.setSelectedFilter = this.setSelectedFilter.bind(this);
    this.setSortBy = this.setSortBy.bind(this);
  }

  componentDidMount(): void {
    this.setState({
      signal: axios.CancelToken.source()
    });
  }

  componentDidUpdate(prevProps: IProps): void {
    if (this.props.location.hash === prevProps.location.hash) {
      return;
    }
    const isOpen = this.props.location.hash.includes('#deviceListFilters');
    if (this.state.isOpen && !isOpen && this.props.history.action === 'POP') {
      this.props.setFiltersFromTempFilters();
      if (this.props.loading) {
        this.state.signal.cancel('Api is being canceled');
      }
      this.setState({
        isOpen: false
      });
    }
  }

  openFilters(): void {
    this.props.setTempFilters();
    this.props.history.push({
      pathname: this.props.location.pathname,
      search: `${this.props.location.search}`,
      hash: '#deviceListFilters'
    });
    this.setState({
      isOpen: true
    });
  }

  closeFilters(): void {
    this.props.setFiltersFromTempFilters();
    if (this.props.loading) {
      this.state.signal.cancel('Api is being canceled');
    }
    this.setState({
      isOpen: false
    });
    this.props.history.goBack();
  }

  submitFilters(): void {
    this.props.submitFilters();
    this.setState({
      isOpen: false
    });
  }

  setSelectedFilter(filterId: string, filter: IFilter): void {
    const signal = axios.CancelToken.source();
    this.setState({
      signal
    });
    this.props.setSelectedFilter(
      filterId,
      filter,
      this.props.categoryId,
      signal
    );
  }

  setSortBy(sortBy: ISortBy): void {
    sendMostPopularEvent(sortBy.title);
    this.props.setSortBy(sortBy);
  }

  render(): ReactNode {
    const {
      filterCharacteristics,
      selectedFilters,
      sortBy,
      resultCount,
      loading,
      translation,
      setFilterHeaderRef,
      sortByList,
      clearAllFilters,
      categoryId
    } = this.props;

    const filterCount = getTotalFiltersCount(selectedFilters);

    return (
      <StyledFilterWrap ref={setFilterHeaderRef}>
        <StyledFilterInner className='filterContent'>
          <FilterHeader
            translation={translation}
            onClick={this.openFilters}
            filterCount={filterCount}
          />
          {this.state.isOpen && (
            <StyledMobileFilters>
              <FilterFlowPanel
                isOpen={this.state.isOpen}
                onClose={this.closeFilters}
              >
                <ModalContent
                  filterCharacteristics={filterCharacteristics}
                  sortByList={sortByList}
                  setSelectedFilter={this.setSelectedFilter}
                  selectedFilters={selectedFilters}
                  sortBy={sortBy}
                  setSortBy={this.setSortBy}
                  // closeModal={this.closeFilters}
                  submitFilters={this.submitFilters}
                  clearAllFilters={() => clearAllFilters(categoryId)}
                  translation={translation}
                  loading={loading}
                  resultCount={resultCount}
                />
              </FilterFlowPanel>
            </StyledMobileFilters>
          )}
        </StyledFilterInner>
      </StyledFilterWrap>
    );
  }
}

export default withRouter(MobileFilters);
