import styled from 'styled-components';
import { AppFlowPanel } from '@src/common/components/FlowPanel/styles';
import { hexToRgbA } from '@src/common/utils';
import { colors } from '@src/common/variables';

export const FilterFlowPanel = styled(AppFlowPanel)`
  .flowPanelInner {
    background: ${hexToRgbA(colors.white, 0.97)};

    .Sort {
      background: none;
    }
    .AllFiltersTabs {
      background: none;
    }
  }
  div::selection,
  span::selection {
    background: transparent;
  }
`;

export const StyledFilterWrap = styled.div`
  min-height: 4rem;
  &.sticky .filterContent {
    position: fixed;
    z-index: 2;
  }
`;

export const StyledFilterInner = styled.div`
  position: relative;
  /* z-index: 1; */
  /* z-index: 102;  */
  /******** Add upper zindex coz of header implementation *********/
  top: 0;
  width: 100%;
  left: 0;
`;

export const StyledMobileFilters = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;

  position: fixed;
  top: 0;
  left: 0;
  /* bottom: 0; */
  min-height: 100%;
  max-height: 100%;

  /* z-index: 10; */ /******* Add coz of error states implementation ********/
  z-index: 22;
  width: 100%;

  .closeIcon {
    /* position: absolute;
    right: 1.25rem;
    top: 1.25rem; */
    z-index: 24;
  }

  .SelectedFilters {
    .title {
      color: ${colors.darkGray};
    }
    .active {
      &:first-child {
        margin-left: 0;
      }
    }
  }
  .Sort {
  }
  .AllFiltersTabs {
    &.listBadge {
      text-align: center;
    }

    &.listLabelWithSuper {
      flex: 1;
      display: flex;
      flex-direction: column;
      flex-wrap: wrap;

      .labelWithSuper {
        margin: 0.625rem 1.875rem 0.625rem 0;
      }
    }
  }
`;
