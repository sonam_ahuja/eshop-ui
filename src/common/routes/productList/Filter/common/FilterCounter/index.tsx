import React from 'react';
import { IProductListTranslation } from '@common/store/types/translation';

import { StyledFilterCounter } from './styles';

interface IProps {
  translation: IProductListTranslation;
  filterCount: number;
  className?: string;
}

const FilterCounter = (props: IProps) => {
  return (
    <StyledFilterCounter>
      <span className='filterTitle'>{props.translation.filterByTitle}</span>
      {props.filterCount ? (
        <span className='countsWrap'>
          <span className='counts'>{props.filterCount}</span>
        </span>
      ) : null}
    </StyledFilterCounter>
  );
};

export default FilterCounter;
