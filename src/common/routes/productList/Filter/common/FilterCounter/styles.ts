import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledFilterCounter = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  .filterTitle {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.darkGray};
  }
  .countsWrap {
    background: ${colors.magenta};
    min-width: 1.25rem;
    min-height: 1.25rem;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    .counts {
      color: ${colors.white};
      font-size: 0.625rem;
      line-height: 0.75rem;
      font-weight: bold;
    }
  }
`;
