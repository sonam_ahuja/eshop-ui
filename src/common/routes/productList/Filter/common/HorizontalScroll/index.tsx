import React, { ReactNode } from 'react';

import { StyledHorizontalScrollWrap } from './styles';

export interface IProps {
  children: ReactNode;
  className?: string;
}

const HorizontalScroll = (props: IProps) => {
  const { children, className } = props;

  const classes = ['horizontalScrollWrap', className ? className : ''].join(
    ' '
  );

  return (
    <StyledHorizontalScrollWrap className={classes}>
      <div className='horizontalScroll'>
        <div className='overflowContainer'>{children}</div>
      </div>
    </StyledHorizontalScrollWrap>
  );
};

export default HorizontalScroll;
