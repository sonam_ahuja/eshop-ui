import styled from 'styled-components';

export const StyledHorizontalScrollWrap = styled.div`
  position: relative;

  .horizontalScroll {
    /* Make this scrollable when needed */
    overflow-x: auto;
    /* We don't want vertical scrolling */
    overflow-y: hidden;
    /* Make an auto-hiding scroller for the 3 people using a IE */
    -ms-overflow-style: -ms-autohiding-scrollbar;
    /* For WebKit implementations, provide inertia scrolling */
    -webkit-overflow-scrolling: auto;
    /* We don't want internal inline elements to wrap */
    white-space: nowrap;
  }
  .horizontalScroll::-webkit-scrollbar {
    /* Remove the default scrollbar for WebKit implementations */
    display: none;
    width: 0 !important ;
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
  &,
  * {
    scrollbar-width: none;
    overflow: -moz-scrollbars-none;
  }
  .horizontalScroll .overflowContainer {
    display: inline-flex;
    align-items: center;
  }
`;
