import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import ListGenerator, {
  IProps
} from '@productList/Filter/common/ListGenerator';
import { Filter, SelectedFilters } from '@mocks/productList/productList.mock';
import APP_KEYS from '@common/constants/appkeys';
import appState from '@store/states/app';

describe('<ListGenerator />', () => {
  const props: IProps = {
    translation: appState().translation.cart.productList,
    id: '1',
    type: 'product',
    className: 'classname',
    data: Filter,
    selectedFilters: SelectedFilters,
    setSelectedFilter: jest.fn()
  };
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <ThemeProvider theme={{}}>
        <ListGenerator {...newProps} />
      </ThemeProvider>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without class name', () => {
    const newProps: IProps = {
      ...props,
      selectedFilters: undefined,
      className: undefined
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without class name and type is filter chips', () => {
    const newProps: IProps = {
      ...props,
      className: undefined,
      type: APP_KEYS.FILTER_CHIPS
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without class name and type is label with supper', () => {
    const newProps: IProps = {
      ...props,
      className: 'class',
      type: APP_KEYS.LABEL_WITH_SUPER
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without class name and type is badge', () => {
    const newProps: IProps = {
      ...props,
      className: 'class',
      type: APP_KEYS.BADGE
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, without class name and type is varient switcher', () => {
    const newProps: IProps = {
      ...props,
      className: 'class',
      type: APP_KEYS.VARIANT_SWITCHER
    };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
