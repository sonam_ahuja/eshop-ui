import React, { ReactNode } from 'react';
import { Badge, FilterChips, LabelWithSuper, Variant } from 'dt-components';
import APP_KEYS from '@common/constants/appkeys';
import { IFilter, ISelectedFilters } from '@productList/store/types';
import { IProductListTranslation } from '@common/store/types/translation';
import cx from 'classnames';

import { StyledListItemsWrap } from './styles';

export interface IProps {
  id: string;
  type: string;
  className?: string;
  data: IFilter[];
  selectedFilters?: ISelectedFilters['key'];
  translation: IProductListTranslation;
  setSelectedFilter(itemId: string, filter: IFilter): void;
  setRef?(ref: HTMLElement | null): void;
}

const isActive = (
  selectedFilters: ISelectedFilters['key'],
  value: string
): boolean => selectedFilters.some((filter: IFilter) => filter.value === value);

const extractData = (
  id: string,
  type: string,
  data: IFilter[],
  selectedFilters: ISelectedFilters['key'],
  setSelectedFilter: IProps['setSelectedFilter'],
  translation: IProductListTranslation
): ReactNode => {
  switch (type) {
    case APP_KEYS.FILTER_CHIPS:
      return data.map((item, index) => {
        return (
          <FilterChips
            className='filterChips'
            key={index}
            label={
              APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID === id
                ? translation.availability[item.name]
                : item.name
            }
            onClick={() => setSelectedFilter(id, item)}
            active={isActive(selectedFilters, item.value)}
            disabled={item.quantity === 0}
          />
        );
      });

    case APP_KEYS.LABEL_WITH_SUPER:
      return data.map((item, index) => {
        return (
          <LabelWithSuper
            className='labelWithSuper'
            key={index}
            label={
              APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID === id
                ? translation.availability[item.name]
                : item.name
            }
            active={isActive(selectedFilters, item.value)}
            onClick={() => setSelectedFilter(id, item)}
            disabled={item.quantity === 0}
          />
        );
      });

    case APP_KEYS.VARIANT_SWITCHER:
      return data.map((item, index) => {
        return (
          <Variant
            className={cx('variantSwitcher', { outOfStock: item.quantity === 0 })}
            key={index}
            variantSize={'medium'}
            value={item.value}
            name={item.name}
            disabled={item.quantity === 0}
            active={isActive(selectedFilters, item.value)}
            onClick={() => setSelectedFilter(id, item)}
          />
        );
      });

    case APP_KEYS.BADGE:
      return data.map((item, index) => {
        return (
          <Badge
            className='badge'
            label={
              APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID === id
                ? translation.availability[item.name]
                : item.name
            }
            active={isActive(selectedFilters, item.value)}
            onClick={() => setSelectedFilter(id, item)}
            key={index}
            disabled={item.quantity === 0}
          />
        );
      });

    default:
      return;
  }
};

const ListGenerator = (props: IProps) => {
  const {
    id,
    type,
    className,
    data,
    selectedFilters,
    setSelectedFilter,
    setRef,
    translation
  } = props;

  const classes = [
    className ? className : '',
    type === APP_KEYS.FILTER_CHIPS ? 'listFilterChips' : '',
    type === APP_KEYS.LABEL_WITH_SUPER ? 'listLabelWithSuper' : '',
    type === APP_KEYS.BADGE ? 'listBadge' : '',
    type === APP_KEYS.VARIANT_SWITCHER ? 'listVariantSwitcher' : ''
  ].join(' ');

  const listItems = extractData(
    id,
    type,
    data,
    selectedFilters || [],
    setSelectedFilter,
    translation
  );

  return (
    <StyledListItemsWrap
      ref={ref => {
        if (setRef) {
          setRef(ref);
        }
      }}
      className={classes}
    >
      {listItems}
    </StyledListItemsWrap>
  );
};

export default ListGenerator;
