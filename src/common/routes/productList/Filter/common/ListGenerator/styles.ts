import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

export const StyledListItemsWrap = styled.div`
  margin-left: -7px;
  &.listBadge {
    margin: -0.125rem;
    /* fixed because badgesWrap width exceeds */
    /* min-width: calc(100vw - 1.25rem); */
    display: flex;

    .badge {
      margin: 0.125rem;
      flex: 1;
      text-align: left;

      &:first-child {
        border-radius: 1.25rem 4px 4px 1.25rem;
      }
      &:last-child {
        border-radius: 4px 1.25rem 1.25rem 4px;
      }
    }
  }

  &.listFilterChips {
    margin: -0.25rem;
    padding: 0.25rem 0;
    .filterChips {
      text-align: left;
    }
  }

  &.listLabelWithSuper {
    .labelWithSuper {
      text-align: left;
    }
  }

  .variantSwitcher {
    /* margin: 7px 10px; */
    &.outOfStock::before {
      height: 40px;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .variantSwitcher {
      &.outOfStock::before {
        height: 15px;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-left: 0px;
    &.listBadge {
      /* fixed because badgesWrap width exceeds */
      /* min-width: auto; */
      .badge {
        padding-top: 0.25rem;
        padding-bottom: 0.25rem;
      }
    }

    &.listFilterChips {
      margin: -0.25rem;
      .filterChips {
        text-align: left;
      }
    }

    &.listLabelWithSuper {
      .labelWithSuper {
        text-align: left;
        margin-right: 0.5rem;
      }
    }
  }
`;
