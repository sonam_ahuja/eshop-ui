import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import PrductList, { IProps } from '@productList/Grid/List';
import appState from '@store/states/app';
import { ITEM_STATUS } from '@common/store/enums';
import {
  getCTAText,
  getSpecialOffer
} from '@productList/utils/getProductDetail';

describe('<PrductList />', () => {
  const props: IProps = {
    headerKeys: ['headerkey1'],
    translation: appState().translation.cart.productList,
    data: [],
    loading: true,
    error: true,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    filters: {},
    redirectToProductDetailed: jest.fn(),
    currencyConfiguration: appState().configuration.cms_configuration.global
      .currency,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    getProductDetailUrl: jest.fn(),
    onLinkClick: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <PrductList {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when error is true', () => {
    const newProps: IProps = { ...props, error: true };

    const component = mount(
      <ThemeProvider theme={{}}>
        <PrductList {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when loading is true', () => {
    const newProps: IProps = { ...props, loading: true };

    const component = mount(
      <ThemeProvider theme={{}}>
        <PrductList {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when loading and error is false', () => {
    const newProps: IProps = { ...props, loading: false, error: false };

    const component = mount(
      <ThemeProvider theme={{}}>
        <PrductList {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when getSpecialOffer has no charecterstics', () => {
    const newProps: IProps = { ...props };

    const component = mount(
      <ThemeProvider theme={{}}>
        <PrductList {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
    getSpecialOffer(
      // tslint:disable:no-any
      '' as any
    );
    const keyValue = [ITEM_STATUS.OUT_OF_STOCK, ITEM_STATUS.PRE_ORDER];

    getCTAText(keyValue, true, appState().translation.cart.productList);
  });
});
