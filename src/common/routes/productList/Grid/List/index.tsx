import { PRICES } from '@productList/store/enum';
import React, { Component, ComponentType, ReactNode } from 'react';
import { Column, Row } from '@common/components/Grid/styles';
import {
  eligibilityUnavailabilityReasonType,
  IProductListItem
} from '@productList/store/types';
import { ITEM_STATUS } from '@common/store/enums';
import { IGrid } from 'dt-components';
import { IProductListTranslation } from '@store/types/translation';
import htmlKeys from '@common/constants/appkeys';
import {
  getCTAText,
  getImage,
  getOutOfStock,
  getPrice,
  getSpecialOffer
} from '@productList/utils/getProductDetail';
import ProductCard from '@productList/common/ProductCard';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration
} from '@src/common/store/types/configuration';

export type IProps = IGrid.IListProps<IProductListItem> & {
  translation: IProductListTranslation;
  catalogConfiguration: ICatalogConfiguration;
  currency: ICurrencyConfiguration;
  onLinkClick(variantId: string): void;
  getProductDetailUrl(
    variantId: string,
    variantName: string,
    productId: string
  ): string;
};
export class PrductList extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.isPreOrder = this.isPreOrder.bind(this);
  }

  isPreOrder(
    eligibilityUnavailabilityReason: eligibilityUnavailabilityReasonType[],
    considerInventory: boolean
  ): boolean {
    if (!eligibilityUnavailabilityReason.includes(ITEM_STATUS.PRE_ORDER)) {
      return false;
    }

    if (
      eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK) &&
      considerInventory
    ) {
      return false;
    }

    return true;
  }

  render(): ReactNode {
    const {
      data,
      openNotificationModal,
      translation,
      currency,
      onLinkClick
    } = this.props;

    return (
      <Row className='gridList'>
        {data &&
          data.map((productItem: IProductListItem, index: number) => (
            <Column
              colMobile={6}
              colTabletPortrait={3}
              colDesktop={3}
              key={index}
            >
              <ProductCard
                heading={productItem.variant.productName}
                alt={productItem.variant.name}
                productId={productItem.variant.id}
                onLinkClick={() => onLinkClick(productItem.variant.productId)}
                frontImageUrl={getImage(
                  productItem.variant.attachments[htmlKeys.THUMBNAIL_IMAGE],
                  htmlKeys.PRODUCT_IMAGE_FRONT
                )}
                frontAndBackImageUrl={getImage(
                  productItem.variant.attachments[htmlKeys.THUMBNAIL_IMAGE],
                  htmlKeys.PRODUCT_IMAGE_FRONT_AND_BACK
                )}
                monthlyAmount={getPrice(
                  productItem.variant.prices,
                  PRICES.MONTHLY,
                  currency
                )}
                translation={translation}
                buttonText={getCTAText(
                  productItem.variant.unavailabilityReasonCodes,
                  this.props.catalogConfiguration.preorder.considerInventory,
                  translation
                )}
                upfrontAmount={getPrice(
                  productItem.variant.prices,
                  PRICES.UPFRONT,
                  currency
                )}
                baseAmount={getPrice(
                  productItem.variant.prices,
                  PRICES.BASE_PRICE,
                  currency
                )}
                isPreOrder={this.isPreOrder(
                  productItem.variant.unavailabilityReasonCodes,
                  this.props.catalogConfiguration.preorder.considerInventory
                )}
                outOfStock={getOutOfStock(
                  productItem.variant.unavailabilityReasonCodes,
                  this.props.catalogConfiguration.preorder.considerInventory
                )}
                productDetailUrl={this.props.getProductDetailUrl(
                  productItem.variant.id,
                  productItem.variant.name,
                  productItem.variant.productId
                )}
                openNotificationModal={openNotificationModal}
                headingIconName='ec-solid-gift'
                specialOffer={getSpecialOffer(
                  productItem.variant.characteristics
                )}
              />
            </Column>
          ))}
      </Row>
    );
  }
}

export default (PrductList as never) as ComponentType<
  IGrid.IListProps<IProductListItem>
>;
