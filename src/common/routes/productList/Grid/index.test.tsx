import appState from '@store/states/app';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import Grid, { IProps, ProductGrid } from '@productList/Grid';
import configureStore from 'redux-mock-store';
import { ProductListItem } from '@mocks/productList/productList.mock';
import { mount } from 'enzyme';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, StaticRouter } from 'react-router-dom';
import { PAGE_THEME } from '@common/store/enums/index';
import { histroyParams } from '@mocks/common/histroy';
const mockStore = configureStore();

describe('<Grid />', () => {
  const props: IProps = {
    currency: {
      currencySymbol: 'Ft',
      isPrecede: false,
      locale: 'pl'
    },
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    theme: PAGE_THEME.PRIMARY,
    productList: [ProductListItem],
    translation: appState().translation.cart.productList,
    categoryId: '1234',
    loading: true,
    pagination: { currentPage: 2, itemsPerPage: 10, totalItems: 100 },
    configuration: appState().configuration.cms_configuration.modules
      .productList,
    openNotificationModal: jest.fn(),
    loadMoreProductList: jest.fn(),
    setlastListURL: jest.fn(),
    setDeviceDetailedList: jest.fn(),
    goToPage: jest.fn(),
    tariffId: null,
    loyalty: null,
    discountId: null,
    ...histroyParams
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Router>
          <Grid {...props} />
        </Router>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const newProps = { ...props };
    newProps.tariffId = '1221';
    newProps.loyalty = '24';
    newProps.discountId = 'magenta';

    const component = mount<ProductGrid>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ProductGrid {...newProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('ProductGrid')
      .instance() as ProductGrid).getProductDetailUrl(
      'variantId',
      'variantName',
      'productId'
    );
  });
});
