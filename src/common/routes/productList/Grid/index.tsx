import React, { Component, ReactNode } from 'react';
import { Grid, IGrid } from 'dt-components';
import List from '@productList/Grid/List';
import { IProductListItem, pageThemeType } from '@productList/store/types';
import { IProductListTranslation } from '@store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductListConfiguration
} from '@store/types/configuration';
import { RouteComponentProps, withRouter } from 'react-router';
import { sendClickedProductListEvent } from '@events/DeviceList/index';
import { DEVICE_LIST } from '@src/common/events/constants/eventName';
import { PAGE } from '@src/common/constants/appConstants';

export interface IComponentProps {
  productList: IProductListItem[];
  categoryId: string;
  loading: boolean;
  currency: ICurrencyConfiguration;
  pagination: IGrid.IPagination;
  configuration: IProductListConfiguration;
  translation: IProductListTranslation;
  catalogConfiguration: ICatalogConfiguration;
  theme: pageThemeType;
  tariffId: string | null;
  loyalty: string | null;
  discountId: string | null;
  view?: string;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
  loadMoreProductList(): void;
  setDeviceDetailedList(list: string): void;
  goToPage(currentPage: number): void;
  setlastListURL(lastListURL: string): void;
}

export type IProps = RouteComponentProps & IComponentProps;

const paginationConfig = {
  infinite: {
    infiniteScrollDistance: 30,
    infiniteScrollThrottle: 0,
    immediateCheck: false,
    useInfiniteScrollContainer: true
  }
};

export class ProductGrid extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.getProductDetailUrl = this.getProductDetailUrl.bind(this);
  }

  getProductDetailUrl(
    variantId: string,
    variantName: string,
    productId: string
  ): string {
    this.props.setlastListURL(this.props.history.location.search);
    const paramsArr: string[] = [];
    if (this.props.tariffId !== null) {
      paramsArr.push(`tariffId=${this.props.tariffId}`);
    }
    if (this.props.loyalty !== null) {
      paramsArr.push(`agreementId=${this.props.loyalty}`);
    }
    if (this.props.discountId !== null) {
      paramsArr.push(`discountId=${this.props.discountId}`);
    }

    return `/${
      this.props.categoryId
    }/${variantName}/${productId}/${variantId}?${encodeURIComponent(
      paramsArr.join('&')
    )}`;
  }

  render(): ReactNode {
    const {
      productList,
      loading,
      openNotificationModal,
      goToPage,
      loadMoreProductList,
      configuration,
      setDeviceDetailedList,
      translation,
      currency,
      view,
      pagination: { currentPage, itemsPerPage, totalItems }
    } = this.props;

    const paginationTranslationConfig = {
      simplified: {
        previous: translation.simplifiedPagination.previous,
        next: translation.simplifiedPagination.next
      },
      showMore: {
        loadMore: translation.showMorePagination.loadMore,
        loading: translation.showMorePagination.loading,
        loaded: translation.showMorePagination.loaded
      },
      infinite: {
        loading: translation.infinitePagination.loading,
        loaded: translation.infinitePagination.loaded
      }
    };

    return (
      <Grid<IProductListItem>
        renderer={{
          List
        }}
        data={productList}
        error={null}
        loading={loading}
        translation={translation}
        paginationTranslationConfig={paginationTranslationConfig}
        currentPage={currentPage}
        itemsPerPage={itemsPerPage}
        getProductDetailUrl={this.getProductDetailUrl}
        totalItems={totalItems}
        paginationConfig={paginationConfig}
        currency={currency}
        onLinkClick={(productId: string) => {
          setDeviceDetailedList(
            PAGE.SEARCH === view
              ? DEVICE_LIST.SEARCH_RESULT
              : DEVICE_LIST.DEVICE_LIST_SECTION
          );
          sendClickedProductListEvent(
            this.props.productList,
            productId,
            this.props.categoryId,
            PAGE.SEARCH === view
              ? DEVICE_LIST.SEARCH_RESULT
              : DEVICE_LIST.DEVICE_LIST_SECTION
          );
        }}
        goToPage={goToPage}
        loadMore={loadMoreProductList}
        paginationType={configuration.paginationType}
        openNotificationModal={openNotificationModal}
        themeType={
          (this.props.theme as unknown) as IGrid.IGridPaginationThemeType
        }
        catalogConfiguration={this.props.catalogConfiguration}
      />
    );
  }
}

export default withRouter(ProductGrid);
