import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Header, { IProps } from '@productList/Header';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { PAGE_THEME } from '@common/store/enums/index';

describe('<Header />', () => {
  const props: IProps = {
    parentCategorySlug: 'dearta',
    productList: [
      {
        category: '',
        groups: {},
        variant: {
          id: '',
          productId: '',
          productName: '',
          name: '',
          description: '',
          group: '',
          bundle: true,
          prices: [],
          attachments: {},
          characteristics: [],
          unavailabilityReasonCodes: []
        }
      }
    ],
    title: 'title',
    theme: PAGE_THEME.PRIMARY,
    pathName: '/device/mobile',
    description: 'description',
    perPageTranslation: '10',
    itemPerPage: 10,
    globalTranslation: appState().translation.cart.global,
    selectitemPerPage: '1',
    itemPerPageList: '10',
    configuration: appState().configuration.cms_configuration.modules
      .productList,
    productListLength: 10,
    changeItemPerPage: jest.fn()
  };
  const componentWrapper = (newProps: IProps) => {
    return mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Header {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
