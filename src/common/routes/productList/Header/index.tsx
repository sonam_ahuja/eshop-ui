import {
  IItemPerPageOptions,
  IProductListItem
} from '@productList/store/types';
import React from 'react';
import { StyledHeader } from '@productList/Header/styles';
import { getItemPerPage } from '@productList/utils/requestTemplate';
import { IGrid, Section, Select, Title } from 'dt-components';
import { IProductListConfiguration } from '@common/store/types/configuration';
import { IGlobalTranslation } from '@common/store/types/translation';
import isMobile from 'ismobilejs';
import { PAGE_THEME } from '@common/store/enums/index';
import CatalogBreadCrumb from '@common/components/CatalogBreadCrumb';
import { sendDropdownClickEvent } from '@src/common/events/common';
import { isCategoryLandingPageRequired } from '@common/store/common/index';

export interface IProps {
  title: string;
  productList: IProductListItem[];
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
  description: string;
  perPageTranslation: string;
  pathName: string;
  selectitemPerPage: string;
  itemPerPage: number;
  globalTranslation: IGlobalTranslation;
  itemPerPageList: string;
  productListLength: number;
  configuration: IProductListConfiguration;
  parentCategorySlug: string;
  changeItemPerPage(item: IItemPerPageOptions): void;
}

const Header = (props: IProps) => {
  const {
    title,
    description,
    perPageTranslation,
    itemPerPage,
    changeItemPerPage,
    selectitemPerPage,
    globalTranslation,
    itemPerPageList,
    configuration,
    productList,
    productListLength,
    theme,
    parentCategorySlug
  } = props;

  const options: IItemPerPageOptions[] = getItemPerPage(
    perPageTranslation,
    itemPerPageList
  );

  const selectedItem = {
    title: `${itemPerPage} ${perPageTranslation}`,
    id: itemPerPage
  };

  const categoryURL = `category/${parentCategorySlug}`;

  const categorylandingPage = isCategoryLandingPageRequired()
    ? [
        {
          title: globalTranslation.products,
          active: false,
          link: categoryURL
        }
      ]
    : [];

  const breadCrumbItems = [
    {
      title: globalTranslation.home,
      active: false,
      link: '/'
    },
    ...categorylandingPage,
    {
      title,
      active: true
    }
  ];

  const hideItemPerPage =
    configuration.paginationType === IGrid.IGridPaginationTypeEnum.infinite ||
    productListLength < configuration.itemPerPage ||
    configuration.paginationType === IGrid.IGridPaginationTypeEnum.showMore;

  return (
    <StyledHeader theme={theme}>
      <CatalogBreadCrumb breadCrumbItems={breadCrumbItems} children={null} />
      {productList && productList.length !== 0 && (
        <>
          <Title
            tag='h1'
            className='title'
            size='xlarge'
            weight='ultra'
            transform='capitalize'
          >
            {title}
          </Title>

          <div className='itemPerPage'>
            <Section className='description' size='large'>
              {description}
            </Section>
            {!hideItemPerPage && (
              <div className='selectPerPageWrap'>
                <Select
                  className='selectPerPage'
                  selectedItem={selectedItem}
                  placeholder={selectitemPerPage}
                  items={options}
                  onItemSelect={(item: IItemPerPageOptions) => {
                    sendDropdownClickEvent(item.title);
                    changeItemPerPage(item);
                  }}
                  useNativeDropdown={isMobile.phone}
                />
              </div>
            )}
          </div>
        </>
      )}
    </StyledHeader>
  );
};

export default Header;
