import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledBreadcrumbWrap } from '@common/components/CatalogBreadCrumb/styles';

import { themeObj } from '../theme';
import { pageThemeType } from '../store/types';

export const StyledHeader = styled.div<{ theme: pageThemeType }>`
  color: ${colors.white};

  ${StyledBreadcrumbWrap} {
    .dt_breadcrumb {
      li {
        a {
          color: ${({ theme }) => {
            return themeObj[theme].breadcrumb.inactiveColor;
          }};
        }
        &.active {
          color: ${({ theme }) => {
            return themeObj[theme].breadcrumb.activeColor;
          }};
        }
      }
    }
  }

  .title {
    width: 100%;
  }

  .itemPerPage {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0rem 0 1.75rem;

    .selectPerPageWrap {
      padding-left: 0.875rem;
      flex-shrink: 0;
      .selectPerPage {
        /* width: 124px; */
        padding: 0.688rem 1.0625rem 0.688rem 1rem;
        border: 1px solid ${colors.white};
        color: ${colors.white};
        border-radius: 10px;
        .caret {
          margin-right: 0.25rem;
          path {
            fill: ${colors.white};
          }
        }

        .optionsList {
          position: absolute;
          width: calc(100% + 2px);
          top: 50%;
          left: -1px;
          transform: translateY(-50%);

          background: ${colors.zBlack};
          padding: 0.5rem 0;
          border-radius: 0.5rem;
          border: 1px solid ${colors.ironGray};
          color: ${colors.white};

          > div:hover {
            background: rgba(0, 0, 0, 0.2);
          }
        }
        .styledSelect {
          font-size: 0.875rem;
          line-height: 1.25rem;
          padding-right: 0.5rem;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .itemPerPage {
      align-items: flex-end;
      padding: 0rem 0 2rem;
      .selectPerPageWrap {
        .selectPerPage {
          padding: 0;
          /* width: 131px; */
          .outsideClick {
            .styledSelect {
              font-size: 15px;
              font-weight: normal;
              line-height: 1.33;
            }
            i {
              width: 11px;
              height: 6px;
            }
          }
        }
        .styledSelectWrap {
          padding: 0.6rem 0.75rem 0.65rem 1rem;
          height: 2.25rem;
          .caret {
            margin: 0;
          }
          .selectPerPage {
            /* width: 131px; */
            padding: 0.75rem 0.5rem 0.75rem 1rem;
          }
          .styledSelect {
            font-size: 0.75rem;
            line-height: 1rem;
          }
        }
      }
    }
  }
`;
