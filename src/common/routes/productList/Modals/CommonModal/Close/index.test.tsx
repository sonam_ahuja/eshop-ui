import React from 'react';
import { mount, shallow } from 'enzyme';
import { ThemeProvider } from 'dt-components';

import { StyledCommonModal } from '../styles';

import Close, { IProps } from '.';

describe('<Close />', () => {
  const props: IProps = {
    className: 'someClass',
    openModal: jest.fn()
  };
  test('should render properly', () => {
    const component = shallow(
      <ThemeProvider theme={{}}>
        <Close {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('StyledCommonModal, should render properly', () => {
    const newpProps = { isOpen: true, children: 'children' };
    const component = shallow(
      <ThemeProvider theme={{}}>
        <StyledCommonModal {...newpProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when outofStock is true', () => {
    const newProps: IProps = { className: undefined, openModal: jest.fn() };
    const component = shallow(
      <ThemeProvider theme={{}}>
        <Close {...newProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('simulate event on close', () => {
    const newProps: IProps = { className: undefined, openModal: jest.fn() };
    const component = mount(
      <ThemeProvider theme={{}}>
        <Close {...newProps} />
      </ThemeProvider>
    );
    component.find('div[onClick]').simulate('click');
  });
});
