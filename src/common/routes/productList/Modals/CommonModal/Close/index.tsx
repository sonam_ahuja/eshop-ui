import React from 'react';
import styled from 'styled-components';
import { Icon } from 'dt-components';
import { colors } from '@src/common/variables';

const StyledCloseIconWrap = styled.div`
  display: inline-flex;
  height: 40px;
  width: 40px;
  border-radius: 50%;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.09);
  background: ${colors.white};
  font-size: 24px;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: opacity 0.2s linear 0s;

  /* &:hover {
    opacity: 0.8;
  } */
`;

export interface IProps {
  className?: string;
  openModal(isModalOpen: boolean): void;
}

const Close = (props: IProps) => {
  const { className } = props;

  return (
    <StyledCloseIconWrap
      className={className}
      onClick={() => props.openModal(false)}
    >
      <Icon size='inherit' name='ec-cancel' />
    </StyledCloseIconWrap>
  );
};

export default Close;
