import { getChildrenModal, IProps } from '@productList/Modals/CommonModal';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';
import appState from '@store/states/app';

describe('getChildrenModal', () => {
  const props: IProps = {
    isOpen: false,
    modalType: MODAL_TYPE.SEND_NOTIFICATION_MODAL,
    notificationLoading: false,
    errorMessage: null,
    translation: appState().translation.cart.productList,
    notificationMediumType: NOTIFICATION_MEDIUM.PHONE,
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,
    sendStockNotification: jest.fn(),
    setError: jest.fn(),
    openModal: jest.fn()
  };
  it('getChildrenModal ::', () => {
    getChildrenModal(props);
    expect(true).toEqual(true);
    // tslint:disable:no-any
    props.modalType = '' as any;
    getChildrenModal(props);
    expect(true).toEqual(true);
  });
});
