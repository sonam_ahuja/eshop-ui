import React, { ReactNode } from 'react';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';
import {
  IError,
  IGetNotificationRequestTemplatePayload
} from '@productList/store/types';
import { ILoginFormRules } from '@common/store/types/configuration';
import { IProductListTranslation } from '@common/store/types/translation';
import StockNotificationModal from '@productList/Modals/StockNotificationModal';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';

import ConfirmationModal from '../ConfirmationModal';

export interface IProps {
  isOpen: boolean;
  className?: string;
  modalType: MODAL_TYPE;
  deviceName?: string;
  variantId?: string;
  notificationLoading: boolean;
  errorMessage: string | null;
  translation: IProductListTranslation;
  notificationMediumType: NOTIFICATION_MEDIUM.PHONE | NOTIFICATION_MEDIUM.EMAIL;
  notificationMediumValue?: string;
  formConfiguration: ILoginFormRules;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
  openModal(isModalOpen: boolean): void;
}

export const getChildrenModal = (props: IProps): ReactNode => {
  const { translation, modalType, notificationLoading } = props;
  switch (modalType) {
    case MODAL_TYPE.SEND_NOTIFICATION_MODAL:
      return (
        <DialogBoxApp
          isOpen={props.isOpen}
          backgroundScroll={false}
          type='flexibleHeight'
          className='notificationPopup'
          onClose={() => props.openModal(false)}
          onEscape={() => props.openModal(false)}
        >
          <StockNotificationModal
            deviceName={props.deviceName as string}
            variantId={props.variantId as string}
            notificationLoading={notificationLoading}
            errorMessage={props.errorMessage as string}
            sendStockNotification={props.sendStockNotification}
            translation={translation}
            setError={props.setError}
            formConfiguration={props.formConfiguration}
          />
        </DialogBoxApp>
      );
    case MODAL_TYPE.CONFIRMATION_MODAL:
      return (
        <DialogBoxApp
          isOpen={props.isOpen}
          backgroundScroll={false}
          type='flexibleHeight'
          className={props.className}
          onClose={() => props.openModal(false)}
          onEscape={() => props.openModal(false)}
        >
          <ConfirmationModal
            notificationMediumValue={props.notificationMediumValue}
            notificationMediumType={props.notificationMediumType}
            openModal={props.openModal}
            translation={translation}
          />
        </DialogBoxApp>
      );
    default:
      return null;
  }
};

const CommonModal = (props: IProps) => {
  return (
    <>
      {/* <Close className='closeIcon' openModal={props.openModal} /> */}
      {getChildrenModal(props)}
    </>
  );
};

export default CommonModal;
