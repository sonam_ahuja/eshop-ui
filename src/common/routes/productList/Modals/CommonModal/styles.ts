import styled from 'styled-components';
import { Modal } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledCommonModal = styled(Modal)`
  .outsideClick {
    max-width: 30rem;
    width: 100%;
  }
  .contentWrap {
    width: 100%;
    padding: 3.25rem 2rem 1rem 2rem;
    position: relative;

    .closeIcon {
      position: absolute;
      right: 2rem;
      top: 2rem;
    }

    .mainIcon {
      margin-bottom: 2rem;
    }

    .mainContent {
      width: 19.8rem;
      color: ${colors.darkGray};
    }
  }

  /* common modal */
  .inputWrap {
    margin: 1.875rem 0;

    .button {
      width: 100%;
      margin-top: 0.75rem;
    }
  }

  .outsideClick,
  .contentWrap {
    width: 100%;
  }

  .overlay {
    align-items: flex-end;

    .contentWrap {
      border-radius: 0.5rem 0.5rem 0 0;
      background: ${colors.coldGray};

      .closeIcon {
        position: fixed;
        top: 1.25rem;
        right: 1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .outsideClick,
    .contentWrap {
      width: 30rem;
    }

    .overlay .contentWrap .closeIcon {
      position: absolute;
    }

    .mainText {
      max-width: 20rem;
    }

    .inputWrap {
      display: flex;
      align-items: flex-end;
      justify-content: space-between;
      margin: 2.6rem 0 1rem;

      .input {
        width: 15.5rem;
        margin-bottom: -1rem;
      }

      .button {
        width: auto;
        margin: auto;
        margin-right: 0;
      }
    }

    .overlay {
      align-items: center;

      .contentWrap {
        border-radius: 0;
      }
    }
  }
`;
