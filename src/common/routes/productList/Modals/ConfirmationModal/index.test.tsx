import ConfirmationModal, {
  IProps
} from '@routes/productList/Modals/ConfirmationModal';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';
import appState from '@store/states/app';

describe('getChildrenModal', () => {
  const props: IProps = {
    translation: appState().translation.cart.productList,
    notificationMediumType: NOTIFICATION_MEDIUM.PHONE,
    openModal: jest.fn()
  };
  it('getChildrenModal ::', () => {
    ConfirmationModal(props);
    expect(true).toEqual(true);

    props.notificationMediumType = NOTIFICATION_MEDIUM.EMAIL;
    ConfirmationModal(props);
    expect(true).toEqual(true);
  });
});
