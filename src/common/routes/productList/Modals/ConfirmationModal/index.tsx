import React from 'react';
import { Button, Icon, Title } from 'dt-components';
import { IProductListTranslation } from '@common/store/types/translation';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';
import EVENT_NAME from '@events/constants/eventName';

import { StyledConfirmationModal } from './styles';

export interface IProps {
  notificationMediumValue?: string;
  translation: IProductListTranslation;
  notificationMediumType: NOTIFICATION_MEDIUM.PHONE | NOTIFICATION_MEDIUM.EMAIL;
  openModal(isModalOpen: boolean): void;
}

const ConfirmationModal = (props: IProps) => {
  const {
    notificationMediumValue,
    translation,
    notificationMediumType
  } = props;

  const mainTextEl = (
    <>
      {notificationMediumType === NOTIFICATION_MEDIUM.PHONE ? (
        <>
          {translation.notificationSuccessMobile}
          <span className='mobileNumber'> {notificationMediumValue}</span>
        </>
      ) : (
        <> {translation.notificationSuccessEmail} </>
      )}
    </>
  );

  return (
    <StyledConfirmationModal>
      <div className='dialogBoxHeader'>
        <Icon className='mainIcon' size='xlarge' name='ec-confirm' />
      </div>
      <div className='dialogBoxBody'>
        <Title className='infoMessage' size='large' weight='ultra'>
          {mainTextEl}
        </Title>
      </div>
      <div className='dialogBoxFooter'>
        <Button
          type='complementary'
          className='button'
          size='small'
          data-event-id={EVENT_NAME.DEVICE_CATALOG.EVENTS.OUT_OF_STOCK}
          data-event-message={translation.notificationAction}
          onClickHandler={() => props.openModal(false)}
        >
          {translation.notificationAction}
        </Button>
      </div>
    </StyledConfirmationModal>
  );
};

export default ConfirmationModal;
