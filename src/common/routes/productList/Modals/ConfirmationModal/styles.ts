import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledConfirmationModal = styled.div`
  .mobileNumber {
    color: ${colors.mediumGray};
  }
`;
