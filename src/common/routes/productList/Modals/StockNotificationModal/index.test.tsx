import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import configureStore from 'redux-mock-store';
import StockNotificationModal, {
  IComponentProps
} from '@productList/Modals/StockNotificationModal';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
const mockStore = configureStore();

// tslint:disable-next-line:no-big-function
describe('<StockNotificationModal />', () => {
  const props: IComponentProps = {
    variantId: '123',
    translation: appState().translation.cart.productList,
    notificationLoading: true,
    errorMessage: 'message',
    deviceName: 'phone',
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,
    sendStockNotification: jest.fn(),
    setError: jest.fn()
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StockNotificationModal {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('should call onChange function if value is entered', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StockNotificationModal {...props} />
      </ThemeProvider>
    );
    const spy = jest.spyOn(StockNotificationModal.prototype, 'onInputChange');
    const onSearchMock = jest.fn();
    const event = {
      preventDefault: () => {
        //
      },
      target: { value: 'the-value' }
    };

    component.find('input').simulate('change', event);
    expect(onSearchMock).toHaveBeenCalledTimes(0);
    expect(spy).toHaveBeenCalledTimes(0);
    expect(component).toMatchSnapshot();

    const event2 = {
      preventDefault: () => {
        //
      },
      target: { value: '' }
    };

    component.find('input').simulate('change', event2);
    expect(onSearchMock).toHaveBeenCalledTimes(0);
    expect(spy).toHaveBeenCalledTimes(0);
    expect(component).toMatchSnapshot();
  });

  test('should send notification on button click if entered value is username', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StockNotificationModal {...props} />
      </ThemeProvider>
    );

    const spy = jest.spyOn(
      StockNotificationModal.prototype,
      'sendNotification'
    );

    component.instance().setState({ inputValue: '1234123' });
    component.find('button').simulate('click');
    expect(spy).toHaveBeenCalledTimes(0);
  });

  test('should send notification on button click if entered value is email', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StockNotificationModal {...props} />
      </ThemeProvider>
    );

    const spy = jest.spyOn(
      StockNotificationModal.prototype,
      'sendNotification'
    );

    const spy2 = jest.spyOn(StockNotificationModal.prototype, 'validateEmail');

    component.instance().setState({ inputValue: 'hello@gmail.com' });
    component.find('button').simulate('click');
    expect(spy).toHaveBeenCalledTimes(0);
    expect(spy2).toHaveBeenCalledTimes(0);
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line:no-identical-functions
  test('should send notification on button click if entered value is phone number', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <StockNotificationModal {...props} />
      </ThemeProvider>
    );

    const spy = jest.spyOn(
      StockNotificationModal.prototype,
      'validatePhoneNumber'
    );

    component.instance().setState({ inputValue: '9977557755' });
    component.find('button').simulate('click');
    expect(spy).toHaveBeenCalledTimes(0);
    expect(component).toMatchSnapshot();
    component.instance().setState({ inputValue: '' });
    component.find('button').simulate('click');
    expect(spy).toHaveBeenCalledTimes(0);
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);

    const component = mount<StockNotificationModal>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <StockNotificationModal {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).onInputChange(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        currentTarget: { value: '9876567893' }
      } as React.ChangeEvent<HTMLInputElement>
    );

    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).createPayload(true);
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).createPayload(false);
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).sendNotification();
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validatePhoneNumber(
      'avinash@gmail.com'
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validateEmail('abc@gmail.com');
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validateEmail('');
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validatePhoneNumber('avinash');
    // const newProps = { ...props };
    // newProps.formConfiguration.phoneNumber. =
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validatePhoneNumber('1324422343');
  });

  test('handle trigger with else conditions ', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);

    const component = mount<StockNotificationModal>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <StockNotificationModal {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).onInputChange(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        currentTarget: { value: 'abc' }
      } as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).sendNotification();
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).onInputChange(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        currentTarget: { value: 'abc@gmail.com' }
      } as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).sendNotification();
  });

  test('handle trigger', () => {
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<StockNotificationModal>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <StockNotificationModal {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).onInputChange(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        currentTarget: { value: 'value' }
      } as React.ChangeEvent<HTMLInputElement>
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).createPayload(true);
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).sendNotification();
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validatePhoneNumber(
      'avinash@gmail.com'
    );
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validateEmail('avinash');
    (component
      .find('StockNotificationModal')
      .instance() as StockNotificationModal).validatePhoneNumber('avinash');
  });
});
