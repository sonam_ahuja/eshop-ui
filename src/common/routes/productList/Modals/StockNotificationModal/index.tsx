import React, { Component, ReactNode } from 'react';
import { Button, FloatLabelInput, Icon, Title } from 'dt-components';
import { IGetNotificationRequestTemplatePayload } from '@productList/store/types';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';
import { isEmail } from '@productList/utils/emailCheck';
import { isPhoneNumber } from '@productList/utils/phonerNumberCheck';
import { isFormFieldValidAsPerType } from '@src/common/routes/checkout/CheckoutSteps/PersonalInfo/utils';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';
import { checkPhoneNumberValidation } from '@src/common/routes/productList/utils/validation';
import EVENT_NAME from '@events/constants/eventName';

import { StyledFooter } from './styles';
import { IProps, IState } from './types';

export type IComponentProps = IProps;

export class StockNotificationModal extends Component<IComponentProps, IState> {
  constructor(props: IComponentProps) {
    super(props);
    this.state = {
      isButtonDisable: true,
      inputValue: ''
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.sendNotification = this.sendNotification.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.validatePhoneNumber = this.validatePhoneNumber.bind(this);
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputFieldValue = event.currentTarget.value;
    if (inputFieldValue && inputFieldValue.length) {
      this.setState({
        isButtonDisable: false,
        inputValue: inputFieldValue
      });
    } else {
      this.setState({
        isButtonDisable: true,
        inputValue: ''
      });
    }
  }

  createPayload(
    enterValueIsEmail: boolean
  ): IGetNotificationRequestTemplatePayload {
    const mediumType = enterValueIsEmail
      ? NOTIFICATION_MEDIUM.EMAIL
      : NOTIFICATION_MEDIUM.PHONE;

    return {
      type: mediumType,
      variantId: this.props.variantId,
      mediumValue: this.state.inputValue
    };
  }

  validatePhoneNumber(inputValue: string): boolean {
    if (!checkPhoneNumberValidation(this.props.formConfiguration, inputValue)) {
      this.props.setError({
        message: 'phone is invalid'
      });

      return false;
    }

    return true;
  }

  validateEmail(inputValue: string): boolean {
    let emailValidation = true;

    emailValidation = isFormFieldValidAsPerType(
      this.props.formConfiguration.email.regex as string,
      inputValue,
      VALIDATION_TYPE.REGEX
    );
    if (!emailValidation) {
      this.props.setError({
        message: 'email is invalid'
      });
    }

    return emailValidation;
  }

  sendNotification(): void {
    const { inputValue } = this.state;
    const enterValueIsEmail = isEmail(inputValue);
    const enterValueIsPhone = isPhoneNumber(inputValue);

    if (this.props.setError) {
      if (!(enterValueIsEmail || enterValueIsPhone)) {
        this.props.setError({
          message: 'Please enter correct value',
          code: 1
        });
      }
      if (enterValueIsPhone && this.validatePhoneNumber(inputValue)) {
        this.props.sendStockNotification(this.createPayload(false));
      } else if (enterValueIsEmail && this.validateEmail(inputValue)) {
        this.props.sendStockNotification(this.createPayload(enterValueIsEmail));
      }
    }
  }

  render(): ReactNode {
    const { isButtonDisable } = this.state;
    const {
      errorMessage,
      deviceName,
      translation,
      notificationLoading
    } = this.props;

    return (
      <>
        <div className='dialogBoxHeader'>
          <Icon className='mainIcon' size='xlarge' name='ec-email' />
        </div>
        <div className='dialogBoxBody'>
          <Title className='infoMessage' size='large' weight='ultra'>
            {translation.notificationTitle.replace('{0}', deviceName)}
          </Title>
        </div>
        <StyledFooter className='dialogBoxFooter'>
          <FloatLabelInput
            className='input'
            label='Mobile number or Email'
            onChange={this.onInputChange}
            error={!!(errorMessage && errorMessage.length)}
            errorMessage={errorMessage}
            value={this.state.inputValue}
          />
          <Button
            className='button'
            size='medium'
            loading={notificationLoading}
            disabled={isButtonDisable}
            data-event-id={EVENT_NAME.DEVICE_CATALOG.EVENTS.VALIDATE_CREDENTIAL}
            data-event-message={translation.validateButton}
            onClickHandler={this.sendNotification}
          >
            {translation.validateButton}
          </Button>
        </StyledFooter>
      </>
    );
  }
}
export default StockNotificationModal;
