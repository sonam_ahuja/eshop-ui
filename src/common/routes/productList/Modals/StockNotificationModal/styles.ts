import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledFooter = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: space-between;
  margin-top: -0.75rem;

  .dt_button {
    margin-top: 0.85rem;
  }
  .date-field {
    width: 100%;
    margin-top: 0rem;
  }
  .date-field + .dt_button {
    margin-top: 1.875rem;
  }
  .date-field input::-webkit-input-placeholder {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.mediumGray};
  }
  .date-field input::-moz-placeholder {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.mediumGray};
  }
  .date-field input:-ms-input-placeholder {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.mediumGray};
  }
  .date-field input:-moz-placeholder {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.mediumGray};
  }
  .date-field .rdtOpen .rdtPicker {
    top: inherit;
    bottom: 0;
    left: inherit;
  }
  @media (max-width: ${breakpoints.mobile}px) {
    .date-field {
      /* padding: 0; */
    }
    .closeInputWrap {
      & + {
        div {
          top: -11px;
          top: 0;
        }
      }
      & ~ .errorMessage {
        bottom: -14px;
      }
    }
  }
  @media (max-width: ${breakpoints.desktop - 1}px) {
    .date-field .rdtOpen .rdtPicker {
      position: fixed;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: row;
    margin-bottom: -1rem;
    align-items: flex-end;

    .dt_floatLabelInput {
      width: 15.75rem;
    }
    .date-field {
      width: 15.75rem;
      margin-top: 8px;
    }
    .date-field + .dt_button {
      margin-top: 0;
      margin-left: 2rem;
      width: 11.6rem;
      white-space: nowrap;
      margin-bottom: 1rem;
    }
  }
`;
