import {
  IError,
  IGetNotificationRequestTemplatePayload
} from '@productList/store/types';
import { IProductListTranslation } from '@common/store/types/translation';
import { ILoginFormRules } from '@common/store/types/configuration';

export interface IProps {
  variantId: string;
  translation: IProductListTranslation;
  errorMessage: string;
  deviceName: string;
  formConfiguration: ILoginFormRules;
  notificationLoading: boolean;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
}

export interface IState {
  isButtonDisable: boolean;
  inputValue: string;
}
