import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import Card, { IProps } from '@productList/common/Card';

describe('<Card />', () => {
  const props: IProps = {
    children: <>some children</>,
    bgColor: 'white'
  };

  test('should render properly', () => {
    const component = mount(
      <ThemeProvider theme={{}}>
        <Card {...props} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });
});
