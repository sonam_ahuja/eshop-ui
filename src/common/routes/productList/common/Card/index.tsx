import React from 'react';

import { StyledCard } from './styles';

export interface IProps {
  children?: React.ReactNode;
  bgColor?: string;
}

const Card = (props: IProps) => {
  return <StyledCard bgColor={props.bgColor}>{props.children}</StyledCard>;
};

export default Card;
