import styled from 'styled-components';

export const StyledCard = styled.div<{ bgColor?: string }>`
  background: ${({ bgColor }) => (bgColor ? bgColor : 'transparent')};
  border-radius: 0.5rem;
  display: flex;
  height: 100%;
  cursor: pointer;
`;
