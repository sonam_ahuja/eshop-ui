import React from 'react';
import { IProductListTranslation } from '@store/types/translation';
import { Paragraph, Section, Title } from 'dt-components';
import cx from 'classnames';

import { StyledSectionBottom } from './styles';

export interface IProps {
  monthlyAmount?: string;
  upfrontAmount?: string;
  baseAmount?: string;
  className?: string;
  translation: IProductListTranslation;
}

const SectionBottom = (props: IProps) => {
  const {
    monthlyAmount,
    upfrontAmount,
    className,
    translation,
    baseAmount
  } = props;

  const montlyEl = (
    <>
      <Section size='small' transform='uppercase' className='sm-title'>
        {translation.monthly}
      </Section>
      <Title size='xsmall' weight='bold'>
        {monthlyAmount}
      </Title>
    </>
  );

  const upfrontEl = (
    <>
      {upfrontAmount && (
        <Section className='label' size='small' transform='uppercase'>
          {translation.upFront}
        </Section>
      )}
      <Paragraph className='value' size='medium' weight='bold'>
        {upfrontAmount}
      </Paragraph>
    </>
  );

  const baseEl = (
    <>
      {baseAmount && (
        <Section className='label' size='small' transform='uppercase'>
          {translation.upFront}
        </Section>
      )}
      <Paragraph className='value' size='medium' weight='bold'>
        {baseAmount}
      </Paragraph>
    </>
  );

  const wrapperClasses = [
    'sectionBottom',
    className,
    'sectionBottom_monthly',
    'sectionBottom_upfront'
  ].join(' ');

  const upfrontClasses = cx('upfront', !monthlyAmount ? 'emphasize' : '');

  return (
    <StyledSectionBottom className={wrapperClasses}>
      {monthlyAmount && <div className='monthly'>{montlyEl}</div>}
      {upfrontAmount && <div className={upfrontClasses}>{upfrontEl}</div>}
      {!upfrontAmount && !monthlyAmount ? (
        <div className={upfrontClasses}>{baseEl}</div>
      ) : null}
    </StyledSectionBottom>
  );
};

export default SectionBottom;
