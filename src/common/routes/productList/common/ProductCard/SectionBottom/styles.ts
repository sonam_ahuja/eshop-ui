import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSectionBottom = styled.div`
  padding: 1rem;
  position: relative;
  min-height: 5.25rem;
  .monthly {
    color: ${colors.magenta};
    .sm-title {
      margin-bottom: 0.25rem;
    }
  }

  .upfront {
    .label {
      color: ${colors.mediumGray};
      margin-bottom: 0.25rem;
    }
    .value {
      color: ${colors.ironGray};
    }
  }

  &.sectionBottom_monthly.sectionBottom_monthly {
    display: flex;
    justify-content: space-between;

    display: block;
    .monthly {
      margin-bottom: 0.75rem;
    }
  }

  &.sectionBottom_monthly.sectionBottom_monthly.sectionBottom_variants {
    display: block;
    .monthly {
      margin-bottom: 0.75rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    &.sectionBottom_monthly.sectionBottom_monthly {
      display: flex;
      justify-content: flex-start;
      .monthly {
        width: 50%;
      }
    }
    .monthly,
    .upfront {
      .dt_title {
        font-size: 1rem;
      }
    }
  }
`;
