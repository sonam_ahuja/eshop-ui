import React, { Component } from 'react';
import { Button, Icon, Paragraph } from 'dt-components';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import { IProductListTranslation } from '@store/types/translation';
import { isMobile } from '@src/common/utils';
import PreloadImage from '@src/common/components/Preload';
import { IMAGE_TYPE, IMGAE_WIDTH_RESOLUTION } from '@common/store/enums';
import EVENT_NAME from '@events/constants/eventName';
import cx from 'classnames';
import { calculateTruncation } from '@src/common/utils/calculateTruncation';

import { StyledPreorderText, StyledSectionTop } from './styles';

export interface IProps {
  heading: string;
  subHeading?: string;
  headingIconName?: iconNamesType;
  outOfStock?: boolean;
  frontImageUrl: string;
  frontAndBackImageUrl: string;
  className?: string;
  buttonText: string;
  productId: string;
  translation: IProductListTranslation;
  alt?: string;
  isPreOrder: boolean;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
}

class SectionTop extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.setPlanNameRef = this.setPlanNameRef.bind(this);
  }

  setPlanNameRef(ref: HTMLElement | null): void {
    if (ref) {
      const innerDiv = ref.querySelector('.productTitle') as HTMLElement;
      if (innerDiv) {
        calculateTruncation(innerDiv);
      }
    }
  }

  render(): React.ReactNode {
    const {
      frontImageUrl,
      frontAndBackImageUrl,
      heading,
      subHeading,
      headingIconName,
      outOfStock,
      buttonText,
      className,
      translation,
      productId,
      openNotificationModal,
      alt,
      isPreOrder
    } = this.props;

    const frontImage = (
      <PreloadImage
        className='frontImage'
        isObserveOnScroll={true}
        imageHeight={10}
        width={IMGAE_WIDTH_RESOLUTION.MEDIUM_DESKTOP_WIDTH}
        mobileWidth={IMGAE_WIDTH_RESOLUTION.MEDIUM_MOBILE_WIDTH}
        type={IMAGE_TYPE.WEBP}
        imageWidth={10}
        imageUrl={frontImageUrl}
        intersectionObserverOption={{
          root: null,
          rootMargin: '10px',
          threshold: 0.4
        }}
        alt={alt}
      />
    );

    const fronAndBackImage = (
      <PreloadImage
        className='backImage'
        isObserveOnScroll={true}
        imageHeight={10}
        width={IMGAE_WIDTH_RESOLUTION.MEDIUM_DESKTOP_WIDTH}
        mobileWidth={IMGAE_WIDTH_RESOLUTION.MEDIUM_MOBILE_WIDTH}
        type={IMAGE_TYPE.WEBP}
        imageWidth={10}
        imageUrl={frontAndBackImageUrl}
        intersectionObserverOption={{
          root: null,
          rootMargin: '5px',
          threshold: 0.1
        }}
        alt={alt}
      />
    );

    const iconEl = headingIconName && (
      <Icon
        size='xsmall'
        className='headingIcon'
        color='currentColor'
        name={headingIconName}
      />
    );

    const preOrderEl = isPreOrder ? (
      <StyledPreorderText>{translation.preOrderCardTitle}</StyledPreorderText>
    ) : null;

    const outOfStockEl = outOfStock && (
      <div className='outOfStockText'>{translation.outOfStockDescription}</div>
    );

    const subHeadingEl = subHeading && (
      <div className='subHeading'>{subHeading}</div>
    );

    const productTitleClasses = cx('productTitle', {
      withIcon: headingIconName
    });
    const wrapperClasses = cx('sectionTop', className);
    const btnWrapClasses = cx('buttonWrap', { btnOutOfStock: outOfStock });

    return (
      <>
        <StyledSectionTop className={wrapperClasses}>
          <span title={heading} ref={this.setPlanNameRef}>
            <Paragraph
              className={productTitleClasses}
              size='small'
              weight='medium'
              transform='capitalize'
            >
              <h3 className='text-auto-truncate'>
                {preOrderEl}
                {heading}
              </h3>
              {subHeadingEl}
              {outOfStockEl}
              {iconEl}
            </Paragraph>
          </span>

          <div className='productImage'>
            {!isMobile.phone && fronAndBackImage}
            {frontImage}
          </div>

          {!isMobile.phone && (
            <div className={btnWrapClasses}>
              {outOfStock ? (
                <Button
                  data-event-id={EVENT_NAME.DEVICE_CATALOG.EVENTS.OUT_OF_STOCK}
                  data-event-message={buttonText}
                  data-event-path={'cart.productList.outOfStock'}
                  className={'btnOutOfStock'}
                  onClickHandler={(event: React.MouseEvent) =>
                    openNotificationModal(event, productId, heading)
                  }
                  size='small'
                >
                  {buttonText}
                </Button>
              ) : (
                <Button
                  data-event-id={
                    EVENT_NAME.DEVICE_CATALOG.EVENTS.REDIRECT_TO_DETAIL
                  }
                  data-event-message={buttonText}
                  data-event-path={'cart.productList.viewDetails'}
                  size='small'
                >
                  {buttonText}
                </Button>
              )}
            </div>
          )}
        </StyledSectionTop>
      </>
    );
  }
}

export default SectionTop;
