import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSectionTop = styled.div`
  color: ${colors.magenta};
  position: relative;
  background: ${colors.coldGray};
  height: 252px;
  overflow: hidden;

  .productTitle {
    color: ${colors.darkGray};
    height: 2.5rem;
    font-size: 0.875rem;
    margin: 1rem;
    word-break: break-word;
    overflow: hidden;
  }
  .productTitle.withIcon {
    position: relative;
    padding-right: 1.5rem;

    .headingIcon {
      position: absolute;
      right: 0;
      top: 0;
      font-size: 1rem;
    }
  }

  .productImage {
    width: 8.75rem;
    margin: auto;
    padding: 0rem 0 1.5rem;
    transition: transform 0.2s ease-in 0s;
    transform: scale(1);
    transform-origin: top;
    padding-bottom: 1.5rem;
    position: relative;

    img[src^='undefined'] {
      /* display: none; */
    }

    .frontImage,
    .backImage {
      transition: opacity 0.2s ease-in 0s;
    }
    .frontImage {
      opacity: 1;
      visibility: visible;
    }
    .backImage {
      display: none;
      opacity: 0;
      visibility: hidden;
      margin-bottom: auto;
      position: absolute;
      top: 0;
      left: 0;

      .dt_placeholderImage {
        display: none;
      }

      .dt_mainImage {
        opacity: 0;
        visibility: hidden;
      }
      .dt_mainImage.imgLoaded {
        transition: all 1200ms cubic-bezier(0.215, 0.61, 0.355, 1) 0.2s;
        opacity: 1;
        visibility: visible;
      }
    }
    img {
      margin: auto;
    }
  }

  .buttonWrap {
    width: 100%;
    visibility: hidden;
    opacity: 0;
    transition: transform, opacity, 0.2s linear 0s;
    position: absolute;
    bottom: 0;
    left: 0;
    transform: translateY(100%);

    &.btnOutOfStock {
      z-index: 2;
    }

    button {
      width: 100%;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    height: 315px;
    .productImage {
      width: 9.5rem;
    }
    .productTitle {
      margin: 1rem 1rem 0.95rem 1rem;
      height: 2rem;
      line-height: 1.1;
      &.withIcon {
        .headingIcon {
          font-size: 1.75rem;
        }
      }
    }
  }
`;

export const StyledPreorderText = styled.div`
  color: ${colors.magenta};
`;
