import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import ProductCard, { IProps } from '@productList/common/ProductCard';
import appState from '@store/states/app';
import { BrowserRouter as Router } from 'react-router-dom';

describe('<ProductCard />', () => {
  const props: IProps = {
    heading: 'Heading',
    subHeading: 'Sub Heading',
    headingIconName: 'ec-americanexpress',
    specialOffer: true,
    frontImageUrl: 'http://localhost:3001/demo.jpg',
    frontAndBackImageUrl: 'http://localhost:3001/demo.jpg',
    monthlyAmount: '200 UHF',
    upfrontAmount: '200 UHF',
    outOfStock: true,
    buttonText: 'Button',
    productId: '1234',
    openNotificationModal: jest.fn(),
    productDetailUrl: '',
    onLinkClick: jest.fn(),
    translation: appState().translation.cart.productList,
    isPreOrder: false
  };
  test('should render properly', () => {
    const component = mount(
      <Router>
        <ThemeProvider theme={{}}>
          <ProductCard {...props} />
        </ThemeProvider>
      </Router>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without sub heading', () => {
    const newProps = { ...props, subHeading: undefined };
    const component = mount(
      <Router>
        <ThemeProvider theme={{}}>
          <ProductCard {...newProps} />
        </ThemeProvider>
      </Router>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly without  heading icon name', () => {
    const newProps = { ...props, headingIconName: undefined };
    const component = mount(
      <Router>
        <ThemeProvider theme={{}}>
          <ProductCard {...newProps} />
        </ThemeProvider>
      </Router>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly when special offer is false', () => {
    const newProps = { ...props, specialOffer: false };
    const component = mount(
      <Router>
        <ThemeProvider theme={{}}>
          <ProductCard {...newProps} />
        </ThemeProvider>
      </Router>
    );
    expect(component).toMatchSnapshot();
  });
});
