import React, { FunctionComponent } from 'react';
import { iconNamesType } from 'dt-components/lib/es/components/atoms/icon/types';
import Card from '@productList/common/Card';
import { IProductListTranslation } from '@store/types/translation';
import { Link } from 'react-router-dom';
import { sendCTAClicks } from '@events/index';
import cx from 'classnames';

import { StyledProductCard } from './styles';
import SectionTop from './SectionTop';
import SectionBottom from './SectionBottom';

export interface IProps {
  isPreOrder: boolean;
  heading: string;
  subHeading?: string;
  headingIconName?: iconNamesType;
  specialOffer?: boolean;
  frontImageUrl: string;
  frontAndBackImageUrl: string;
  monthlyAmount?: string;
  upfrontAmount?: string;
  outOfStock: boolean;
  baseAmount?: string;
  buttonText: string;
  productId: string;
  translation: IProductListTranslation;
  productDetailUrl: string;
  alt?: string;
  onLinkClick(): void;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
}

export const ProductCard: FunctionComponent<IProps> = (props: IProps) => {
  const {
    heading,
    subHeading,
    frontAndBackImageUrl,
    frontImageUrl,
    monthlyAmount,
    upfrontAmount,
    headingIconName,
    specialOffer,
    outOfStock,
    productDetailUrl,
    buttonText,
    productId,
    translation,
    baseAmount,
    openNotificationModal,
    alt,
    onLinkClick,
    isPreOrder
  } = props;

  const wrapperClasses = cx({ specialOffer, outOfStock });

  return (
    <Card>
      <StyledProductCard className={wrapperClasses}>
        <Link
          className='primaryLink'
          to={productDetailUrl}
          onClick={() => {
            sendCTAClicks(buttonText, window.location.href);
            onLinkClick();
          }}
        />

        <SectionTop
          heading={heading}
          subHeading={subHeading}
          frontImageUrl={frontImageUrl}
          frontAndBackImageUrl={frontAndBackImageUrl}
          headingIconName={
            specialOffer ? (headingIconName as iconNamesType) : undefined
          }
          isPreOrder={isPreOrder}
          outOfStock={outOfStock}
          productId={productId}
          translation={translation}
          buttonText={buttonText}
          openNotificationModal={openNotificationModal}
          alt={alt}
        />
        <SectionBottom
          monthlyAmount={monthlyAmount}
          translation={translation}
          baseAmount={baseAmount}
          upfrontAmount={upfrontAmount}
        />
      </StyledProductCard>
    </Card>
  );
};

export default ProductCard;
