import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledProductCard = styled.div`
  width: 100%;
  border-radius: inherit;
  overflow: hidden;
  position: relative;
  transition: 0.2s ease-in 0s;
  display: flex;
  flex-direction: column;
  box-shadow: 0 8px 40px 0 rgba(0, 0, 0, 0);
  position: relative;

  /* LINKS START */
  .primaryLink {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    z-index: 1;
  }
  /* LINKS END */

  .sectionTop {
    border-radius: 0.5rem 0.5rem 0 0;
    .buttonWrap {
      .buttonText {
        text-transform: inherit;
      }
    }
  }

  .sectionBottom {
    flex: 1;
    height: 100%;
    border-radius: 0 0 0.5rem 0.5rem;
    background: ${colors.white};
    .emphasize {
      .value {
        font-size: 1.25rem;
      }
    }
  }

  /* out of stock START */
  &.outOfStock {
    .text-auto-truncate {
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }

    .sectionTop {
      .productTitle {
        color: ${colors.mediumGray};

        .outOfStockText {
          color: ${colors.darkGray};
        }
      }

      .productImage {
        opacity: 0.6;
      }
    }
  }
  /* out of stock END */

  /* special offer START */
  &.specialOffer {
    .sectionTop {
      background: ${colors.darkGray};

      .productTitle {
        color: ${colors.white};

        .outOfStockText {
          color: ${colors.white};
        }
      }
    }

    .sectionBottom {
      background: ${colors.charcoalGray};

      .monthly,
      .upfront .label,
      .upfront .value {
        color: ${colors.white};
      }
    }
  }
  /* special offer END */

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    &:hover {
      box-shadow: 0 8px 40px 0 rgba(0, 0, 0, 0.2);
      .sectionTop {
        .productImage {
          transform: scale(0.8421);

          .frontImage {
            opacity: 0;
            visibility: hidden;
          }
          .backImage {
            opacity: 1;
            visibility: visible;
            display: block;
          }
        }
        .buttonWrap {
          visibility: visible;
          opacity: 1;
          transform: translateY(0%);
        }
      }
    }
  }
`;
