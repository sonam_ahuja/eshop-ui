import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import EmptyListsPanel from '@productList/empty-list';
import { PAGE_THEME } from '@common/store/enums/index';
import appState from '@store/states/app';

describe('<EmptyListsPanel />', () => {
  const props = {
    translation: appState().translation.cart.productList,
    theme: PAGE_THEME.PRIMARY
  };
  const componentWrapper = () => {
    return mount(
      <ThemeProvider theme={{}}>
        <EmptyListsPanel {...props} />
      </ThemeProvider>
    );
  };
  test('should render properly', () => {
    const component = componentWrapper();
    expect(component).toMatchSnapshot();
  });
});
