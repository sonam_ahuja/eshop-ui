import React from 'react';
import { Icon, Title } from 'dt-components';
import { IProductListTranslation } from '@src/common/store/types/translation';
import { PAGE_THEME } from '@src/common/store/enums';

import { pageThemeType } from '../store/types';

import { StyledEmptyListsPanel } from './styles';
export interface IProps {
  // tslint:disable-next-line:no-any
  configuration?: any;
  theme: pageThemeType;
  translation: IProductListTranslation;
}

const EmptyListsPanel = (props: IProps) => {
  const { translation } = props;
  const theme: pageThemeType = props.theme
    ? props.theme
    : [PAGE_THEME.PRIMARY, PAGE_THEME.SECONDARY].includes(
        props.configuration.theme
      )
    ? props.configuration.theme
    : PAGE_THEME.SECONDARY;

  return (
    <StyledEmptyListsPanel theme={theme}>
      <Icon color='currentColor' size='inherit' name='dt-sad-face' />
      <Title size='normal' weight='bold'>
        <p className='text-panel'>
          {translation.noProductText}
          <span> {translation.comeBackText}</span>
        </p>
      </Title>
    </StyledEmptyListsPanel>
  );
};

export default EmptyListsPanel;
