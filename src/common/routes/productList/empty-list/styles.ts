import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';

import { themeObj } from '../theme';
import { pageThemeType } from '../store/types';

export const StyledEmptyListsPanel = styled.div<{ theme: pageThemeType }>`
  margin-top: 1.6875rem;

  .dt_icon {
    font-size: 3.5rem;
    margin-bottom: 0.5rem;
    color: ${props => themeObj[props.theme].emptyState.icon.color};
  }

  .dt_title {
    color: ${props => themeObj[props.theme].emptyState.title.color};
    span {
      color: ${props => themeObj[props.theme].emptyState.subTitle.color};
      display: inline;
    }
  }

  .text-panel {
    span {
      display: inline;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    align-items: center;
    margin-top: 1.65rem;
    .dt_icon {
      font-size: 4.5rem;
      margin-bottom: 0;
      margin-right: 1.5rem;
    }
    .text-panel {
      span {
        display: block;
      }
    }
  }
`;
