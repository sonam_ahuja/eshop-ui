import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints, colors } from '@src/common/variables';

export const StyledProductShell = styled.div`
  color: #dadada;
  padding: 1.25rem;
  width: 100%;
  .mainHeading {
    padding: 2.5rem 0 3rem;
    max-width: 20rem;
    div {
      height: 0.75rem;
      background: currentColor;
      margin-bottom: 0.5rem;
    }
    div:first-child {
      width: 80%;
      margin-bottom: 1rem;
    }
    div:last-child {
      width: 60%;
    }
  }

  .card {
    flex: 1;
    height: 11.4375rem;
    width: 100%;
    padding: 0;
    padding-bottom: 0;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    background: ${colors.cloudGray};

    .heading {
      height: 0.5rem;
      width: 90%;
      background: currentColor;
      max-width: 15rem;
    }

    .image {
      width: 6rem;
      height: 140px;
      background: currentColor;
      margin: auto;
    }

    .bottom {
      border-top: 1px solid currentColor;
      padding: 2.25rem;
      margin: 0 -1.25rem;

      ul li {
        height: 0.5rem;
        width: 50%;
        background: currentColor;
        margin-top: 0.25rem;

        &:last-child {
          width: 75%;
        }
      }
      ul + ul {
        margin-top: 1rem;
      }
    }
  }

  .row {
    margin: -0.125rem;
    display: flex;
    flex-wrap: wrap;

    .col {
      padding: 0.125rem;
      width: 50%;
    }
  }

  .mainShellTitleWrap {
    margin-bottom: 2rem;
    .headTitle {
      height: 1rem;
      width: 100%;
      background: ${colors.cloudGray};
      margin-bottom: 0.75rem;
    }
    .info {
      height: 1rem;
      width: 50%;
      background: ${colors.cloudGray};
    }
  }

  .shine {
    height: 100%;
    width: 100%;
    margin: 0;
    &:after {
      background: ${colors.silverGray};
    }
  }

  .breadCrumb {
    height: 1rem;
    width: 100%;
    margin-bottom: 4rem;
    background: ${colors.cloudGray};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 2rem 2.25rem;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 2rem 3rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2rem 4.9rem;
    .card {
      height: 8.65rem;
    }
    .row {
      .col {
        width: 25%;
      }
    }
    .breadCrumb {
      width: 13.25rem;
    }
    .mainShellTitleWrap {
      .headTitle {
        width: 50%;
      }
      .info {
        width: 30%;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 5.75rem;
  }
`;

const ProductShell = () => {
  return (
    <StyledShell>
      <StyledProductShell>
        <div className='shine breadCrumb' />
        <div className='mainShellTitleWrap'>
          <div className='shine headTitle' />
          <div className='shine info' />
        </div>
        <section>
          <div className='row'>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
          </div>
        </section>
      </StyledProductShell>
    </StyledShell>
  );
};

export default ProductShell;
