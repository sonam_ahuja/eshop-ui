import configureStore from 'redux-mock-store';
import {
  ErrorData,
  Filter,
  FilterCharacteristics,
  NotificationData,
  PopularDevices,
  SelectedFilter,
  SendStockPayload,
  SortedBy
} from '@mocks/productList/productList.mock';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { MODAL_TYPE } from '@productList/store/enum';
import { histroyParams } from '@mocks/common/histroy';
import { PAGE_THEME } from '@common/store/enums/index';

import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IProps, ProductList } from '.';
import AppShell from './index.shell';

// tslint:disable-next-line: no-big-function
describe('<ProductList />', () => {
  const props: IProps = {
    ...histroyParams,
    commonError: {
      retryable: false,
      code: ''
    },
    discountId: null,
    defaultSortBy: '',
    notificationLoading: false,
    tariffId: '',
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    tariffName: 'tariff',
    loyalty: '',
    sortListing: [],
    fetchPopularDevices: jest.fn(),
    filterCharacteristics: FilterCharacteristics,
    productList: appState().productList.data,
    popularDevices: PopularDevices,
    loading: true,
    clearScrollPosition: jest.fn(),
    gridLoading: true,
    parentCategorySlug: 'sdsdds',
    plan: {
      id: '',
      productId: '',
      productName: '',
      name: '',
      description: '',
      group: '',
      bundle: false,
      prices: [],
      attachments: {},
      characteristics: [],
      unavailabilityReasonCodes: []
    },
    installmentEnabled: true,
    loyality: '',
    installment: '',
    showAppShell: true,
    categoryName: 'product',
    lastScrollPositionMap: { urlPath: 2 },
    setScrollPosition: jest.fn(),
    categoryId: '1234',
    error: null,
    backgroundImage: 'https://i.ibb.co/mHxM497/Untitled-1.jpg',
    theme: PAGE_THEME.PRIMARY,
    pagination: {
      currentPage: 2,
      itemsPerPage: 10,
      totalItems: 10
    },
    receivedProductListDataFromServer: true,
    selectedFilters: SelectedFilter,
    setSelectedCategory: jest.fn(),
    sortBy: SortedBy,
    tempSelectedFilters: SelectedFilter,
    tempSortBy: SortedBy,
    globalTranslation: appState().translation.cart.global,
    translation: appState().translation.cart.productList,
    configuration: appState().configuration.cms_configuration.modules
      .productList,
    characteristics: [{ name: 'key', values: [{ value: 'value1' }] }],
    currencyConfiguration: appState().configuration.cms_configuration.global
      .currency,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    productListConfig: appState().configuration.cms_configuration.modules
      .productList,
    notificationModalType: MODAL_TYPE.CONFIRMATION_MODAL,
    notificationData: NotificationData,
    showModal: true,
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,

    loadMoreProductList: jest.fn(),
    fetchProductList: jest.fn(),
    updateReceivedProductListDataFromServer: jest.fn(),
    changeItemPerPage: jest.fn(),
    updateShowAppShell: jest.fn(),
    setSelectedFilter: jest.fn(),
    setSortBy: jest.fn(),
    clearAllFilters: jest.fn(),
    setFiltersFromTempFilters: jest.fn(),
    setTempFilters: jest.fn(),
    sendStockNotification: jest.fn(),
    setError: jest.fn(),
    openModal: jest.fn(),
    setlastListURL: jest.fn(),
    setNotificationData: jest.fn(),
    clearAllSelectedFilter: jest.fn(),
    resetData: jest.fn(),
    setTariffLoyalty: jest.fn(),
    setDeviceDetailedList: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ProductList {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = {};

    const component = mount(
      <ThemeProvider theme={{}}>
        <AppShell {...appShellProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<ProductList>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <ProductList {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );

    (component.find('ProductList').instance() as ProductList).onScroll();
    (component
      .find('ProductList')
      .instance() as ProductList).getProductDetailUrl(
      'variantId',
      'variantName',
      'productId'
    );
    (component
      .find('ProductList')
      .instance() as ProductList).componentDidUpdate(props);
    (component
      .find('ProductList')
      .instance() as ProductList).setItemPerPageValue({
      title: 'string',
      // selected: boolean;
      id: 1
    });
    (component.find('ProductList').instance() as ProductList).onScroll();
    (component
      .find('ProductList')
      .instance() as ProductList).loadMoreProductList();
    (component.find('ProductList').instance() as ProductList).goToPage(2);
    (component.find('ProductList').instance() as ProductList).setSelectedFilter(
      'filterId',
      {
        name: 'string',
        default: true,
        value: 'string',
        quantity: 2
      }
    );
    (component.find('ProductList').instance() as ProductList).setSortBy({
      id: 'string',
      title: 'string'
    });
    (component.find('ProductList').instance() as ProductList).submitFilters();
    (component.find('ProductList').instance() as ProductList).clearAllFilters();
    (component
      .find('ProductList')
      .instance() as ProductList).openNotificationModal(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        // tslint:disable-next-line: no-empty
        stopPropagation: () => {}
      } as React.MouseEvent,
      'variantId',
      'deviceName'
    );
    (component
      .find('ProductList')
      .instance() as ProductList).componentWillUnmount();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();

    mapDispatchToProps(dispatch).fetchProductList(2, '1234', {
      filters: {},
      sortBy: 'value',
      itemPerPage: '1'
    });

    mapDispatchToProps(dispatch).loadMoreProductList('12', true, 12);
    mapDispatchToProps(dispatch).updateReceivedProductListDataFromServer(true);
    mapDispatchToProps(dispatch).setSelectedFilter('12344', Filter[0], '12344');
    mapDispatchToProps(dispatch).setSortBy(SortedBy[0]);
    mapDispatchToProps(dispatch).setTempFilters();
    mapDispatchToProps(dispatch).setFiltersFromTempFilters();
    mapDispatchToProps(dispatch).changeItemPerPage(3);
    mapDispatchToProps(dispatch).updateShowAppShell(true);
    mapDispatchToProps(dispatch).sendStockNotification(SendStockPayload);
    mapDispatchToProps(dispatch).setError(ErrorData);
    mapDispatchToProps(dispatch).openModal(true);
    mapDispatchToProps(dispatch).setNotificationData(NotificationData);
    mapDispatchToProps(dispatch).setSelectedCategory('1234');
    mapDispatchToProps(dispatch).resetData();
    mapDispatchToProps(dispatch).setScrollPosition('/basket', 23);
    mapDispatchToProps(dispatch).clearScrollPosition('/basket');
    mapDispatchToProps(dispatch).clearAllSelectedFilter();
    mapDispatchToProps(dispatch).setlastListURL('12');
    mapDispatchToProps(dispatch).clearAllFilters('12');
  });
});
