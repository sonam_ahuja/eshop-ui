import ProductShell from '@productList/index.shell';
import { NOTIFICATION_MEDIUM, PAGE_NUMBER } from '@productList/store/enum';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  ICharacteristics,
  IFilter,
  IItemPerPageOptions,
  ISelectedFilters,
  ISortBy,
  pageThemeType
} from '@productList/store/types';
import ProductGrid from '@productList/Grid';
import { connect } from 'react-redux';
import { IGrid, Loader, Utils } from 'dt-components';
import { Helmet } from 'react-helmet';
import React, { Component, ReactNode } from 'react';
import { isMobile } from '@src/common/utils';
import { RootState } from '@common/store/reducers';
import { getCategorySlug } from '@productList/utils/getCategorySlug';
import ProductListHeader from '@productList/Header';
import htmlKeys from '@common/constants/appkeys';
import { PAGE_THEME } from '@src/common/store/enums';
import Header from '@common/components/Header';
import Footer from '@common/components/Footer';
import { updatedImageURL } from '@src/common/utils/index';
import PopularDevice from '@productList/popularDevice';
import { pageViewEvent, sendCTAClicks } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import {
  sendApplyFilterEvent,
  sendMostPopularEvent
} from '@events/DeviceList/index';
import CommonModal from '@productList/Modals/CommonModal';
import Error from '@src/common/components/Error';
import { getCurrencySymbol } from '@src/common/utils/currency';
import BasketStripWrapper from '@common/components/BasketStripWrapper';
import ProductListListDesktopFilter from '@productList/Filter/Desktop';
import ProductListListMobileFilter from '@productList/Filter/Mobile';
import EmptyListsPanel from '@productList/empty-list/index';

import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IMapDispatchToProps, IMapStateToProps } from './types';
import {
  computeSelectedFilters,
  createQueryParams,
  getSelectedFiltersFromParams,
  isFilterSortByAndItemPerPageChanged,
  isUpdated
} from './utils';
import { StyledContentWrapper, StyledProductList } from './styles';

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export class ProductList extends Component<IProps> {
  filterHeaderRef: HTMLElement | null = null;
  scrollAmount = 0;
  locationSearch = '';
  popularIndexCalled = 0;

  constructor(props: IProps) {
    super(props);
    this.setSelectedFilter = this.setSelectedFilter.bind(this);
    this.setSortBy = this.setSortBy.bind(this);
    this.submitFilters = this.submitFilters.bind(this);
    this.clearAllFilters = this.clearAllFilters.bind(this);
    this.setItemPerPageValue = this.setItemPerPageValue.bind(this);
    this.getDescription = this.getDescription.bind(this);
    this.openNotificationModal = this.openNotificationModal.bind(this);
    this.loadMoreProductList = Utils.debounce(
      this.loadMoreProductList.bind(this),
      100
    );
    this.goToPage = this.goToPage.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.sendFilterEvent = this.sendFilterEvent.bind(this);
    const { itemPerPage } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    this.props.changeItemPerPage(
      itemPerPage !== null
        ? Number(itemPerPage)
        : this.props.configuration.itemPerPage
    );
    this.getProductDetailUrl = this.getProductDetailUrl.bind(this);
  }

  // tslint:disable-next-line:cognitive-complexity
  onScroll(): void {
    this.scrollAmount = window.scrollY;

    if (this.filterHeaderRef) {
      const filterContent = this.filterHeaderRef.querySelector(
        '.filterContent'
      );
      if (filterContent) {
        if (this.filterHeaderRef.getBoundingClientRect().top <= 0) {
          if (!filterContent.classList.contains('sticky')) {
            filterContent.classList.add('sticky');
            this.popularIndexCalled++;
            if (this.popularIndexCalled === 1) {
              this.props.fetchPopularDevices(this.props.categoryId);
            }
          }
        } else {
          if (filterContent.classList.contains('sticky')) {
            filterContent.classList.remove('sticky');
          }
        }
      }
    }
  }

  getProductDetailUrl(
    variantId: string,
    variantName: string,
    productId: string
  ): string {
    this.props.setlastListURL(this.props.history.location.search);
    const paramsArr: string[] = [];
    if (this.props.tariffId !== null) {
      paramsArr.push(`tariffId=${this.props.tariffId}`);
    }
    if (this.props.loyalty !== null) {
      paramsArr.push(`agreementId=${this.props.loyalty}`);
    }
    if (this.props.discountId !== null) {
      paramsArr.push(`discountId=${this.props.discountId}`);
    }

    return `/${
      this.props.categoryId
    }/${variantName}/${productId}/${variantId}?${encodeURIComponent(
      paramsArr.join('&')
    )}`;
  }

  componentDidMount(): void {
    window.scrollTo({
      top: 0
    });
    window.addEventListener(
      'scroll',
      this.onScroll,
      Utils.passiveEvents
        ? {
            passive: true
          }
        : false
    );
    const categorySlug = getCategorySlug(this.props.location.pathname);
    this.props.setSelectedCategory(categorySlug);
    pageViewEvent(`${PAGE_VIEW.DEVICE_LIST}/${categorySlug}`);

    if (!this.props.receivedProductListDataFromServer) {
      const {
        currentPage,
        tariffId,
        loyalty,
        discountId,
        ...params
      } = getSelectedFiltersFromParams(this.props.location.search);

      this.props.setTariffLoyalty(tariffId, loyalty, discountId);

      // get new data when component will mount first!
      const position = this.props.lastScrollPositionMap[
        this.props.location.search
      ];
      if (position !== undefined) {
        this.props.clearScrollPosition(this.props.location.search);
      }

      if (
        ([
          IGrid.IGridPaginationTypeEnum.showMore,
          IGrid.IGridPaginationTypeEnum.infinite
        ] as IGrid.IGridPaginationType[]).includes(
          this.props.configuration.paginationType
        )
      ) {
        this.props.loadMoreProductList(
          categorySlug,
          true, // initial render get all the data
          position,
          params,
          currentPage,
          true
        );
      } else {
        this.props.fetchProductList(
          currentPage,
          categorySlug,
          params,
          position,
          true
        );
      }
    }
    this.props.updateReceivedProductListDataFromServer(false);
    this.locationSearch = this.props.location.search;
  }

  fetchDataFromURI(searchURI: string, successCallback?: () => void): void {
    const isChanged = isFilterSortByAndItemPerPageChanged(
      searchURI,
      this.props.location.search,
      this.props.defaultSortBy,
      this.props.configuration.itemPerPage
    );

    this.locationSearch = searchURI;

    const {
      currentPage,
      tariffId,
      loyalty,
      ...params
    } = getSelectedFiltersFromParams(searchURI);

    if (
      ([
        IGrid.IGridPaginationTypeEnum.showMore,
        IGrid.IGridPaginationTypeEnum.infinite
      ] as IGrid.IGridPaginationType[]).includes(
        this.props.configuration.paginationType
      )
    ) {
      this.props.loadMoreProductList(
        this.props.categoryId,
        isChanged,
        undefined,
        params,
        currentPage,
        false,
        successCallback
      );
    } else {
      this.props.fetchProductList(
        currentPage,
        this.props.categoryId,
        params,
        undefined,
        undefined,
        undefined,
        successCallback
      );
    }
  }

  componentDidUpdate(prevProps: IProps): void {
    // filter, sortBy, itemPerPage, pageNumber, tariffId, loyalty, discountId
    if (
      this.props.location.search === prevProps.location.search ||
      this.props.location.pathname.split('/').length !== 2 ||
      this.props.commonError.httpStatusCode
    ) {
      return;
    }
    this.locationSearch = this.props.location.search;

    const isChanged = isUpdated(this.props.location.search, {
      selectedFilters: this.props.selectedFilters,
      sortBy: this.props.sortBy
        ? this.props.sortBy.id
        : this.props.defaultSortBy,
      itemPerPage: this.props.pagination.itemsPerPage,
      pageNumber: this.props.pagination.currentPage,
      tariffId: this.props.tariffId,
      loyalty: this.props.loyalty,
      discountId: this.props.discountId
    });

    if (!isChanged) {
      return;
    }

    const {
      currentPage,
      tariffId,
      loyalty,
      ...params
    } = getSelectedFiltersFromParams(this.props.location.search);

    if (
      ([
        IGrid.IGridPaginationTypeEnum.showMore,
        IGrid.IGridPaginationTypeEnum.infinite
      ] as IGrid.IGridPaginationType[]).includes(
        this.props.configuration.paginationType
      )
    ) {
      this.props.loadMoreProductList(
        this.props.categoryId,
        isChanged,
        undefined,
        params,
        currentPage
      );
    } else {
      this.props.fetchProductList(currentPage, this.props.categoryId, params);
    }
  }

  updateURI(search: string, replace: boolean): void {
    if (replace) {
      this.props.history.replace({
        pathname: this.props.location.pathname,
        search
      });
    } else {
      this.props.history.push({
        pathname: this.props.location.pathname,
        search
      });
    }
  }

  setItemPerPageValue(item: IItemPerPageOptions): void {
    const { id } = item;
    const queryParam = createQueryParams(
      this.props.selectedFilters,
      this.props.sortBy ? this.props.sortBy.id : null,
      PAGE_NUMBER.INITIAL,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      id
    );
    const search = `?${queryParam}`;
    this.props.changeItemPerPage(id);
    if (search !== this.props.location.search) {
      this.fetchDataFromURI(search, () => this.updateURI(search, false));
    } else {
      const {
        currentPage,
        tariffId,
        loyalty,
        ...params
      } = getSelectedFiltersFromParams(this.props.location.search);
      if (
        ([
          IGrid.IGridPaginationTypeEnum.showMore,
          IGrid.IGridPaginationTypeEnum.infinite
        ] as IGrid.IGridPaginationType[]).includes(
          this.props.configuration.paginationType
        )
      ) {
        // filters are updated in this case as itemPerPage are changed so used true
        this.props.loadMoreProductList(
          this.props.categoryId,
          true,
          undefined,
          params,
          PAGE_NUMBER.INITIAL
        );
      } else {
        this.props.fetchProductList(
          PAGE_NUMBER.INITIAL,
          this.props.categoryId,
          params
        );
      }
    }
  }

  loadMoreProductList(): void {
    if (
      this.props.configuration.paginationType ===
      IGrid.IGridPaginationTypeEnum.showMore
    ) {
      sendCTAClicks(
        this.props.translation.showMorePagination.loadMore,
        window.location.href
      );
    }

    const search = `?${createQueryParams(
      this.props.selectedFilters,
      this.props.sortBy ? this.props.sortBy.id : null,
      this.props.pagination.currentPage + 1,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      this.props.pagination.itemsPerPage
    )}`;
    const path = `${this.props.location.pathname}${search}`;
    if (
      path !== `${this.props.location.pathname}${this.props.location.search}`
    ) {
      if (
        this.props.configuration.paginationType ===
        IGrid.IGridPaginationTypeEnum.infinite
      ) {
        this.fetchDataFromURI(search, () => this.updateURI(search, true));
      } else {
        this.fetchDataFromURI(search, () => this.updateURI(search, false));
      }
    }
  }

  goToPage(pageNumber: number): void {
    if (
      this.props.configuration.paginationType ===
      IGrid.IGridPaginationTypeEnum.simplified
    ) {
      this.props.pagination.currentPage < pageNumber
        ? sendCTAClicks(
            this.props.translation.simplifiedPagination.next,
            window.location.href
          )
        : sendCTAClicks(
            this.props.translation.simplifiedPagination.previous,
            window.location.href
          );
    } else if (
      this.props.configuration.paginationType ===
        IGrid.IGridPaginationTypeEnum.numbered &&
      pageNumber
    ) {
      sendCTAClicks(pageNumber.toString(), window.location.href);
    }
    const search = `?${createQueryParams(
      this.props.selectedFilters,
      this.props.sortBy ? this.props.sortBy.id : null,
      pageNumber,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      this.props.pagination.itemsPerPage
    )}`;
    const path = `${this.props.location.pathname}${search}`;
    /*
    We are updating URL and componentDidUpdate will handle API call
    */
    if (
      path !== `${this.props.location.pathname}${this.props.location.search}`
    ) {
      this.fetchDataFromURI(search, () => this.updateURI(search, false));
    }
  }

  getDescription(description: string): string {
    const iTotalItem = this.props.pagination.totalItems;
    let installmentExist = true;
    if (!this.props.installment && !this.props.installmentEnabled) {
      installmentExist = false;
    }

    if (iTotalItem) {
      return description
        .replace(
          '{0}',
          String(this.props.productList ? this.props.productList.length : 0)
        )
        .replace('{1}', String(iTotalItem))
        .replace(
          '{2}',
          installmentExist ? this.props.translation.titleSeparator : ''
        )
        .replace(
          '{3}',
          installmentExist
            ? `${this.props.installment} ${this.props.translation.installments}`
            : ''
        )
        .replace('{4}', this.getPlanInfo());
    }

    return '';
  }

  getPlanInfo = () => {
    if (this.props.plan && this.props.plan.prices) {
      const price =
        this.props.plan.prices && this.props.plan.prices.length
          ? this.props.plan.prices[0].discountedValue
          : null;

      const currencySymbol = getCurrencySymbol(this.props.currency.locale);

      return `${this.props.translation.planTitleSeparator} ${
        this.props.plan.name
      }${
        price
          ? ` ${price} ${currencySymbol}/${
              this.props.translation.pricePerMonth
            }`
          : ''
      }`;
    }

    return '';
  }

  componentWillUnmount(): void {
    window.removeEventListener('scroll', this.onScroll);
    this.popularIndexCalled = 0;
    this.props.setScrollPosition(this.locationSearch, this.scrollAmount);
    this.props.updateShowAppShell(true);
    this.props.resetData();
    this.props.openModal(false);
  }

  setSelectedFilter(filterId: string, filter: IFilter): void {
    // only called for desktop and we are updating URL and componentDidUpdate will called
    const selectedFilters = computeSelectedFilters(
      this.props.selectedFilters,
      filterId,
      filter
    );

    this.sendFilterEvent(selectedFilters);

    const search = `?${createQueryParams(
      selectedFilters,
      this.props.sortBy ? this.props.sortBy.id : null,
      1,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      this.props.pagination.itemsPerPage
    )}`;

    this.fetchDataFromURI(search, () => this.updateURI(search, false));
  }

  sendFilterEvent(selectedFilters: ISelectedFilters): void {
    const filtersFromState = Object.keys(selectedFilters);
    const filterName: string[] = [];
    filtersFromState.forEach(filterFromState => {
      if (selectedFilters && selectedFilters[filterFromState]) {
        selectedFilters[filterFromState].forEach((item: IFilter) => {
          filterName.push(item.name);
        });
      }
    });
    sendApplyFilterEvent(filterName.join(), filterName.length);
  }

  setSortBy(sortBy: ISortBy): void {
    sendMostPopularEvent(sortBy.title);
    // only called for desktop
    const search = `?${createQueryParams(
      this.props.selectedFilters,
      sortBy ? sortBy.id : null,
      PAGE_NUMBER.INITIAL,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      this.props.pagination.itemsPerPage
    )}`;
    this.fetchDataFromURI(search, () => this.updateURI(search, false));
  }

  submitFilters(): void {
    const queryParams = createQueryParams(
      this.props.selectedFilters,
      this.props.sortBy ? this.props.sortBy.id : null,
      1,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      this.props.pagination.itemsPerPage
    );
    const search = `?${queryParams}`;
    if (search === this.props.location.search) {
      return;
    }
    this.sendFilterEvent(this.props.selectedFilters);
    // only called for mobile filters
    this.fetchDataFromURI(search, () => this.updateURI(search, false));
  }

  clearAllFilters(): void {
    // only called for desktop
    this.props.clearAllSelectedFilter();
    const search = `?${createQueryParams(
      {},
      this.props.sortBy ? this.props.sortBy.id : null,
      PAGE_NUMBER.INITIAL,
      this.props.tariffId,
      this.props.loyalty,
      this.props.discountId,
      this.props.pagination.itemsPerPage
    )}`;
    this.fetchDataFromURI(search, () => this.updateURI(search, false));
  }

  openNotificationModal(
    event: React.MouseEvent,
    variantId: string,
    deviceName: string
  ): void {
    event.stopPropagation();
    this.props.setNotificationData({
      variantId,
      deviceName,
      mediumValue: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    });
    this.props.openModal(true);
  }

  // tslint:disable: no-big-function
  // tslint:disable-next-line:cognitive-complexity
  render(): ReactNode {
    const {
      productList,
      categoryId,
      categoryName,
      loading,
      gridLoading,
      pagination,
      translation,
      configuration,
      characteristics,
      showAppShell,
      notificationModalType,
      error,
      setError,
      sendStockNotification,
      notificationData,
      showModal,
      currency,
      openModal,
      globalTranslation,
      formConfiguration,
      notificationLoading,
      parentCategorySlug,
      setlastListURL,
      sortListing,
      popularDevices,
      sortBy,
      setDeviceDetailedList,
      commonError
    } = this.props;
    const theme: pageThemeType = this.props.theme
      ? this.props.theme
      : [PAGE_THEME.PRIMARY, PAGE_THEME.SECONDARY].includes(
          this.props.configuration.theme
        )
      ? this.props.configuration.theme
      : PAGE_THEME.SECONDARY;
    const backgroundImage = this.props.backgroundImage
      ? updatedImageURL(this.props.backgroundImage)
      : this.props.configuration.backgroundImage;
    const description = this.getDescription(
      productList && productList.length < 2
        ? translation.singularDescription
        : translation.description
    );
    const metaTagTitle = characteristics.filter(
      (characterstic: ICharacteristics) => {
        return characterstic.name === htmlKeys.META_TAG_TITLE;
      }
    )[0];

    const metaTagDescription = characteristics.filter(
      (characterstic: ICharacteristics) => {
        return characterstic.name === htmlKeys.META_TAG_DESCRIPTION;
      }
    )[0];

    const showMetaTitle =
      metaTagTitle && metaTagTitle.values.length !== 0
        ? metaTagTitle.values[0].value
        : categoryName;

    const showMetaDescription =
      metaTagDescription && metaTagDescription.values
        ? metaTagDescription.values[0].value
        : null;

    return (
      <>
        <Helmet
          title={showMetaTitle ? showMetaTitle : ''}
          meta={[
            {
              name: 'description',
              content: showMetaDescription ? showMetaDescription : ''
            }
          ]}
        />

        <Header showBasketStrip={false} />
        {!showAppShell && (
          <Loader open={loading} label={translation.loadingText} />
        )}
        {commonError.httpStatusCode ? (
          <Error />
        ) : (
          <>
            <CommonModal
              isOpen={showModal}
              deviceName={notificationData.deviceName}
              variantId={notificationData.variantId}
              errorMessage={error && error.message}
              sendStockNotification={sendStockNotification}
              setError={setError}
              notificationLoading={notificationLoading}
              modalType={notificationModalType}
              translation={translation}
              notificationMediumType={notificationData.type}
              notificationMediumValue={notificationData.mediumValue}
              openModal={openModal}
              formConfiguration={formConfiguration}
            />
            {showAppShell ? (
              <>
                <ProductShell />
                <Footer pageName={PAGE_VIEW.DEVICE_LIST} />
              </>
            ) : null}
            {
              <>
                {!showAppShell && isMobile.phone ? (
                  <ProductListListMobileFilter
                    sortByList={sortListing}
                    sortBy={sortBy}
                    filterCharacteristics={this.props.filterCharacteristics}
                    submitFilters={this.submitFilters}
                    translation={translation}
                    categoryId={categoryId}
                    setSelectedFilter={this.props.setSelectedFilter}
                    clearAllFilters={this.props.clearAllFilters}
                    setSortBy={this.props.setSortBy}
                    selectedFilters={this.props.selectedFilters}
                    setTempFilters={this.props.setTempFilters}
                    setFiltersFromTempFilters={
                      this.props.setFiltersFromTempFilters
                    }
                    resultCount={this.props.pagination.totalItems}
                    loading={this.props.gridLoading}
                    setFilterHeaderRef={ref => (this.filterHeaderRef = ref)}
                  />
                ) : null}
                <StyledProductList
                  {...this.props}
                  theme={theme}
                  backgroundImage={backgroundImage}
                >
                  <StyledContentWrapper>
                    {!showAppShell && !isMobile.phone ? (
                      <ProductListListDesktopFilter
                        loading={loading}
                        sortByList={sortListing}
                        setSortBy={this.setSortBy}
                        sortBy={sortBy}
                        filterCharacteristics={this.props.filterCharacteristics}
                        setSelectedFilter={this.setSelectedFilter}
                        translation={translation}
                        selectedFilters={this.props.selectedFilters}
                        // clearAllFilters={this.props.clearAllFilters}
                        clearAllFilters={this.clearAllFilters}
                        setFilterHeaderRef={ref => (this.filterHeaderRef = ref)}
                      />
                    ) : null}
                    {!showAppShell ? (
                      <>
                        <div className='productListContainer'>
                          <ProductListHeader
                            theme={theme}
                            title={categoryName}
                            changeItemPerPage={this.setItemPerPageValue}
                            globalTranslation={globalTranslation}
                            itemPerPageList={configuration.itemPerPageList}
                            perPageTranslation={translation.perPage}
                            selectitemPerPage={translation.selectitemPerPage}
                            itemPerPage={pagination.itemsPerPage}
                            description={description}
                            pathName={this.props.location.pathname}
                            configuration={configuration}
                            productListLength={pagination.totalItems}
                            parentCategorySlug={parentCategorySlug}
                            productList={productList}
                          />
                          {productList && productList.length === 0 ? (
                            <EmptyListsPanel
                              translation={translation}
                              theme={PAGE_THEME.SECONDARY}
                            />
                          ) : (
                            <ProductGrid
                              productList={productList}
                              loadMoreProductList={this.loadMoreProductList}
                              goToPage={this.goToPage}
                              categoryId={categoryId}
                              setDeviceDetailedList={setDeviceDetailedList}
                              translation={translation}
                              loading={gridLoading}
                              setlastListURL={setlastListURL}
                              pagination={pagination}
                              configuration={configuration}
                              currency={currency}
                              openNotificationModal={this.openNotificationModal}
                              theme={theme}
                              tariffId={this.props.tariffId}
                              loyalty={this.props.loyalty}
                              discountId={this.props.discountId}
                              catalogConfiguration={
                                this.props.catalogConfiguration
                              }
                            />
                          )}

                          {!!(popularDevices && popularDevices.length) && (
                            <PopularDevice
                              theme={theme}
                              configuration={configuration}
                              openNotificationModal={this.openNotificationModal}
                              setDeviceDetailedList={setDeviceDetailedList}
                              popularDevices={popularDevices}
                              productListTranslation={translation}
                              getProductDetailUrl={this.getProductDetailUrl}
                              categoryId={categoryId}
                              currency={currency}
                              catalogConfiguration={
                                this.props.catalogConfiguration
                              }
                            />
                          )}
                        </div>
                      </>
                    ) : null}
                  </StyledContentWrapper>
                  {!showAppShell ? <BasketStripWrapper /> : null}
                </StyledProductList>
                <Footer pageName={PAGE_VIEW.DEVICE_LIST} />
              </>
            }
          </>
        )}
      </>
    );
  }
}

export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(ProductList)
  // tslint:disable-next-line:max-file-line-count
);
