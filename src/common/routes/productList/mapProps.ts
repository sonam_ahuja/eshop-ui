import actions from '@productList/store/actions';
import { Dispatch } from 'redux';
import { CancelTokenSource } from 'axios';
import {
  IError,
  IFilter,
  IGetNotificationRequestTemplatePayload,
  INotificationData,
  IQueryParamFilter,
  ISortBy
} from '@productList/store/types';
import categoryActions from '@category/store/actions';
import { RootState } from '@common/store/reducers';
import commonActions from '@common/store/actions/common';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  commonError: state.common.error,
  currency: state.configuration.cms_configuration.global.currency,
  filterCharacteristics: state.productList.filterCharacteristics,
  productList: state.productList.data,
  lastScrollPositionMap: state.productList.lastScrollPositionMap,
  backgroundImage: state.productList.backgroundImage,
  theme: state.productList.theme,
  loading: state.productList.loading,
  gridLoading: state.productList.gridLoading,
  showAppShell: state.productList.showAppShell,
  categoryId: state.categories.selectedCategoryId,
  categoryName: state.productList.categoryName,
  pagination: state.productList.pagination,
  error: state.productList.error,
  showModal: state.productList.openModal,
  globalTranslation: state.translation.cart.global,
  notificationData: state.productList.notificationData,
  receivedProductListDataFromServer:
    state.productList.receivedProductListDataFromServer,
  selectedFilters: state.productList.selectedFilters,
  tempSelectedFilters: state.productList.tempSelectedFilters,
  tempSortBy: state.productList.tempSortBy,
  translation: state.translation.cart.productList,
  currencyConfiguration: state.configuration.cms_configuration.global.currency,
  configuration: state.configuration.cms_configuration.modules.productList,
  catalogConfiguration: state.configuration.cms_configuration.modules.catalog,
  characteristics: state.productList.characteristics,
  productListConfig: state.configuration.cms_configuration.modules.productList,
  notificationModalType: state.productList.notificationModal,
  formConfiguration:
    state.configuration.cms_configuration.global.loginFormRules,
  notificationLoading: state.productList.notificationLoading,
  parentCategorySlug: state.productList.parentCategorySlug,
  sortListing: state.productList.sortListing,
  sortBy: state.productList.sortBy,
  popularDevices: state.productList.popularDevices,
  tariffId: state.productList.tariffId,
  tariffName: state.productList.tariffName,
  loyalty: state.productList.loyalty,
  discountId: state.productList.discountId,
  plan: state.productList.plan,
  installmentEnabled: state.productList.installmentEnabled,
  loyality: state.productList.loyality,
  installment: state.productList.installment,
  defaultSortBy: state.productList.defaultSortBy
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  fetchProductList(
    currentPage: number,
    categoryId: string,
    queryParams: {
      filters: IQueryParamFilter;
      sortBy: string;
      itemPerPage: string | null;
    },
    scrollAmount?: number,
    // tslint:disable-next-line:bool-param-default
    showFullPageError?: boolean,
    signal?: CancelTokenSource,
    successCallback?: () => void
    // showLoading: boolean
  ): void {
    dispatch(
      actions.fetchProductList({
        currentPage,
        categoryId,
        queryParams,
        scrollAmount,
        showFullPageError,
        signal,
        successCallback
        // showLoading
      })
    );
  },
  loadMoreProductList(
    categoryId: string,
    isChanged: boolean,
    scrollAmount: number | undefined,
    queryParams: {
      filters: IQueryParamFilter;
      sortBy: string;
      itemPerPage: string | null;
    },
    pageNumber: number,
    showFullPageError: boolean,
    successCallback?: () => void
    // showLoading: boolean
  ): void {
    dispatch(
      actions.loadMoreProductList({
        categoryId,
        isChanged,
        scrollAmount,
        queryParams,
        pageNumber,
        showFullPageError,
        successCallback
        // showLoading
      })
    );
  },
  updateReceivedProductListDataFromServer(
    receivedProductListDataFromServer: boolean
  ): void {
    dispatch(
      actions.updateReceivedProductListDataFromServer({
        receivedProductListDataFromServer
      })
    );
  },
  /** @method: only used in mobile */
  setSelectedFilter(
    filterId: string,
    filter: IFilter,
    categoryId: string,
    signal?: CancelTokenSource
  ): void {
    dispatch(actions.setSelectedFilter({ filterId, filter }));
    dispatch(
      actions.fetchProductList({
        currentPage: 1,
        categoryId,
        signal
      })
    );
  },
  clearAllFilters(categoryId: string): void {
    dispatch(actions.clearAllSelectedFilter());
    dispatch(
      actions.fetchProductList({
        currentPage: 1,
        categoryId
      })
    );
  },
  /** @method: only used in mobile */
  setSortBy(sortBy: ISortBy): void {
    dispatch(actions.setSortBy(sortBy));
  },
  setTempFilters(): void {
    dispatch(actions.setTempFilters());
  },
  setFiltersFromTempFilters(): void {
    dispatch(actions.setFiltersFromTempFilters());
  },
  changeItemPerPage(itemPerPage: number): void {
    dispatch(actions.changeItemPerPage(itemPerPage));
  },
  updateShowAppShell(showAppShell: boolean): void {
    dispatch(actions.updateShowAppShell(showAppShell));
  },
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void {
    dispatch(actions.sendStockNotification(payload));
  },
  setError(error: IError): void {
    dispatch(actions.setError(error));
  },
  openModal(isModalOpen: boolean): void {
    dispatch(actions.openModal(isModalOpen));
  },
  setNotificationData(data: INotificationData): void {
    dispatch(actions.setNotificationData(data));
  },
  setSelectedCategory(categorySlug: string): void {
    dispatch(categoryActions.setSelectedCategory(categorySlug));
  },
  clearAllSelectedFilter(): void {
    dispatch(actions.clearAllSelectedFilter());
  },
  resetData(): void {
    dispatch(actions.resetData());
  },
  setScrollPosition(urlPath: string, position: number): void {
    dispatch(actions.setScrollPosition({ urlPath, position }));
  },
  clearScrollPosition(urlPath: string): void {
    dispatch(actions.clearScrollPosition({ urlPath }));
  },
  setlastListURL(lastListURL: string): void {
    dispatch(actions.setlastListURL(lastListURL));
  },
  fetchPopularDevices(categorySlug: string): void {
    dispatch(actions.fetchPopularDevices(categorySlug));
  },
  setTariffLoyalty(
    tariffId: string | null,
    loyalty: string | null,
    discountId: string | null
  ): void {
    dispatch(actions.setTariffLoyalty({ tariffId, loyalty, discountId }));
  },
  setDeviceDetailedList(list: string): void {
    dispatch(commonActions.setDeviceDetailedList(list));
  }
});
