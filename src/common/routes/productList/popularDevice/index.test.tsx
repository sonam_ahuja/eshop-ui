import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import { ITEM_STATUS, PAGE_THEME } from '@src/common/store/enums';
import { BrowserRouter as Router } from 'react-router-dom';

import PopularDevice, { IProps } from '.';

describe('<PopularDevice />', () => {
  const props: IProps = {
    popularDevices: [
      {
        id: '2',
        productId: '2',
        name: 'name',
        productName: 'product',
        description: 'description',
        group: 'group',
        bundle: true,
        characteristics: [],
        prices: [],
        attachments: {
          image: [
            {
              name: 'name',
              url: 'http://contact.jpg'
            }
          ]
        },
        unavailabilityReasonCodes: [ITEM_STATUS.IN_STOCK]
      }
    ],
    theme: PAGE_THEME.PRIMARY,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    categoryId: 'device-mobile',
    productListTranslation: appState().translation.cart.productList,
    configuration: appState().configuration.cms_configuration.modules
      .productList,
    setDeviceDetailedList: jest.fn(),
    openNotificationModal: jest.fn(),
    getProductDetailUrl: jest.fn(() => ''),
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <Router>
        <ThemeProvider theme={{}}>
          <PopularDevice {...newProps} />
        </ThemeProvider>
      </Router>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
