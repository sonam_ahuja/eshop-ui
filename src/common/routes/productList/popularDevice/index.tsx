import { Anchor, Title } from 'dt-components';
import React from 'react';
import {
  eligibilityUnavailabilityReasonType,
  IVariant
} from '@productList/store/types';
import { Column, Row } from '@common/components/Grid/styles';
import { PRICES } from '@productList/store/enum';
import {
  getCTAText,
  getImage,
  getOutOfStock,
  getPrice,
  getSpecialOffer
} from '@productList/utils/getProductDetail';
import htmlKeys from '@common/constants/appkeys';
import { IProductListTranslation } from '@store/types/translation';
import { ITEM_STATUS, pageThemeType } from '@src/common/store/enums';
import { sendClickedProductEvent } from '@events/DeviceList/index';
import { DEVICE_LIST } from '@events/constants/eventName';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductListConfiguration
} from '@common/store/types/configuration';
import ProductCard from '@productList/common/ProductCard';
import appConstants from '@src/common/constants/appConstants';

import { StyledHeaderWrap, StyledPopularDevice } from './styles';

export interface IProps {
  popularDevices: IVariant[];
  theme: pageThemeType;
  configuration: IProductListConfiguration;
  currency: ICurrencyConfiguration;
  productListTranslation: IProductListTranslation;
  categoryId: string;
  catalogConfiguration: ICatalogConfiguration;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
  getProductDetailUrl(
    variantName: string,
    variantId: string,
    productId: string
  ): string;
  setDeviceDetailedList(list: string): void;
}

const isPreOrder = (
  eligibilityUnavailabilityReason: eligibilityUnavailabilityReasonType[],
  considerInventory: boolean
): boolean => {
  if (!eligibilityUnavailabilityReason.includes(ITEM_STATUS.PRE_ORDER)) {
    return false;
  }

  if (
    eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK) &&
    considerInventory
  ) {
    return false;
  }

  return true;
};

const PopularDevice = (props: IProps) => {
  const {
    popularDevices,
    getProductDetailUrl,
    productListTranslation,
    categoryId,
    theme,
    setDeviceDetailedList,
    catalogConfiguration,
    currency,
    openNotificationModal,
    configuration
  } = props;

  return (
    <StyledPopularDevice theme={theme}>
      <StyledHeaderWrap>
        <Title
          transform='capitalize'
          className='heading'
          size='large'
          weight='ultra'
          tag='h2'
        >
          {productListTranslation.popularAccessories}
        </Title>
        {configuration.showViewAllLink && (
          <Anchor
            hreflang={appConstants.LANGUAGE_CODE}
            href={configuration.viewAllLink}
            target={configuration.openViewAllInTab ? '_blank' : '_self'}
            className='view-all-btn'
          >
            {productListTranslation.viewAllPopularDevice}
          </Anchor>
        )}
      </StyledHeaderWrap>
      <Row>
        {popularDevices.map(productItem => (
          <Column
            className='horizontalScrollableItem'
            colMobile={6}
            colDesktop={3}
            colTabletPortrait={3}
            key={productItem.id}
          >
            <ProductCard
              heading={productItem.productName}
              alt={productItem.name}
              productId={productItem.id}
              frontImageUrl={getImage(
                productItem.attachments[htmlKeys.THUMBNAIL_IMAGE],
                htmlKeys.PRODUCT_IMAGE_FRONT
              )}
              frontAndBackImageUrl={getImage(
                productItem.attachments[htmlKeys.THUMBNAIL_IMAGE],
                htmlKeys.PRODUCT_IMAGE_FRONT_AND_BACK
              )}
              onLinkClick={() => {
                setDeviceDetailedList(DEVICE_LIST.POPULAR_DEVICE);
                sendClickedProductEvent(
                  popularDevices,
                  productItem.productId,
                  categoryId,
                  DEVICE_LIST.POPULAR_DEVICE
                );
              }}
              monthlyAmount={getPrice(
                productItem.prices,
                PRICES.MONTHLY,
                currency
              )}
              baseAmount={getPrice(
                productItem.prices,
                PRICES.BASE_PRICE,
                currency
              )}
              upfrontAmount={getPrice(
                productItem.prices,
                PRICES.UPFRONT,
                currency
              )}
              isPreOrder={isPreOrder(
                productItem.unavailabilityReasonCodes,
                catalogConfiguration.preorder.considerInventory
              )}
              openNotificationModal={openNotificationModal}
              headingIconName='ec-solid-gift'
              specialOffer={getSpecialOffer(productItem.characteristics)}
              productDetailUrl={getProductDetailUrl(
                productItem.id,
                productItem.name,
                productItem.productId
              )}
              outOfStock={getOutOfStock(
                productItem.unavailabilityReasonCodes,
                catalogConfiguration.preorder.considerInventory
              )}
              translation={productListTranslation}
              buttonText={getCTAText(
                productItem.unavailabilityReasonCodes,
                catalogConfiguration.preorder.considerInventory,
                productListTranslation
              )}
            />
          </Column>
        ))}
      </Row>
    </StyledPopularDevice>
  );
};

export default PopularDevice;
