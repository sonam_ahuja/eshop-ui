import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { pageThemeType } from '@src/common/store/enums';
import { theme } from '@category/theme';

export const StyledHeaderWrap = styled.div`
  display: flex;
  margin: 4rem 0 1rem;
  align-items: flex-end;
  justify-content: space-between;
  .heading {
    padding: 0;
    margin-right: 1.625rem;
  }
  .view-all-btn {
    flex-shrink: 0;
    font-size: 1.09375rem;
    line-height: 1.5625rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 2rem;
    .heading {
      width: 23rem;
    }
    .view-all-btn {
      font-size: 0.875rem;
      line-height: 1.25rem;
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    .heading {
      width: auto;
    }
  }
`;

export const StyledPopularDevice = styled.div<{ theme: pageThemeType }>`
  margin-bottom: 1.8rem;
  .heading {
    color: ${props => theme[props.theme].sectionHeading.color};
  }

  ${StyledHeaderWrap} {
    .view-all-btn {
      color: ${props => theme[props.theme].viewAll.color};
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 2rem;
  }
`;
