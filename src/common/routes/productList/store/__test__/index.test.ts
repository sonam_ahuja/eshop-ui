import productListState from '@productList/store/state';
import productListReducer from '@productList/store/reducer';

describe('productList Reducer', () => {
  it('productList reducer test', () => {
    const action: { type: string } = { type: '' };
    expect(productListReducer(productListState(), action)).toEqual(
      productListState()
    );
  });
});
