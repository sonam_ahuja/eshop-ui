import productList from '@productList/store/reducer';
import productListState from '@productList/store/state';
import {
  INotificationData,
  IProductListState,
  ISortBy
} from '@productList/store/types';
import actions from '@productList/store/actions';
import {
  CustomErrorData,
  ErrorData,
  ProductListResponse,
  SendStockPayload,
  SortedBy
} from '@mocks/productList/productList.mock';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';

// tslint:disable-next-line:no-big-function
describe('Productlist Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IProductListState = productList(
    undefined,
    definedAction
  );
  let newState: IProductListState = productListState();
  const initializeValue = () => {
    initialStateValue = productList(undefined, definedAction);
    newState = productListState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('FETCH_PRODUCT_LIST_SUCCESS should mutate state', () => {
    const action = actions.fetchProductListSuccess(ProductListResponse);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('FETCH_PRODUCT_LIST_ERROR should mutate state', () => {
    const mutation: {
      loading: boolean;
      error?: { code: number; message: string };
      showAppShell: boolean;
    } = {
      loading: false,
      error: { code: 0, message: 'message' },
      showAppShell: false
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchProductListError(ErrorData);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PRODUCT_LIST_ERROR with non standard error!', () => {
    const mutation: {
      loading: boolean;
      showAppShell: boolean;
    } = {
      loading: false,
      showAppShell: false
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchProductListError(new Error());
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PRODUCT_LIST_LOADING should mutate state', () => {
    const mutation: {
      loading: boolean;
      gridLoading: boolean;
      error: Error | null;
    } = {
      loading: false,
      gridLoading: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchProductListLoading(true);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PRODUCT_LIST_LOADING with gridLoading false', () => {
    const mutation: {
      loading: boolean;
      error: Error | null;
    } = {
      loading: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchProductListLoading(false);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('RECEIVED_DATA_FROM_SERVER should mutate state', () => {
    const mutation: {
      receivedProductListDataFromServer: boolean;
    } = {
      receivedProductListDataFromServer: true
    };
    const expectedData = { ...newState, ...mutation };
    const payload: { receivedProductListDataFromServer: boolean } = {
      receivedProductListDataFromServer: true
    };
    const action = actions.updateReceivedProductListDataFromServer(payload);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('CHANGE_ITEM_PER_PAGE should mutate state', () => {
    const mutation: {
      pagination: {
        itemsPerPage: number;
        currentPage: number;
        totalItems: number;
      };
    } = {
      pagination: { itemsPerPage: 2, currentPage: 1, totalItems: 0 }
    };
    const expectedData = { ...newState, ...mutation };

    const action = actions.changeItemPerPage(2);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('UPDATE_SHOW_APP_SHELL should mutate state', () => {
    const mutation: {
      showAppShell: boolean;
    } = {
      showAppShell: true
    };
    const expectedData = { ...newState, ...mutation };

    const action = actions.updateShowAppShell(true);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SEND_STOCK_NOTIFICATION should mutate state', () => {
    const mutation: INotificationData = {
      deviceName: '',
      mediumValue: '12',
      variantId: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    };
    const expectedData = { ...newState, notificationData: { ...mutation } };

    const action = actions.sendStockNotification(SendStockPayload);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SEND_STOCK_NOTIFICATION_SUCCESS should mutate state', () => {
    const mutation: { notificationModal: MODAL_TYPE } = {
      notificationModal: MODAL_TYPE.CONFIRMATION_MODAL
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.sendStockNotificationSuccess();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_ERROR should mutate state', () => {
    const expectedData = { ...newState, error: { ...CustomErrorData } };
    const action = actions.setError(CustomErrorData);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('OPEN_MODAL should mutate state when modal is open', () => {
    const mutation: { openModal: boolean; error: null } = {
      openModal: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.openModal(true);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('OPEN_MODAL should mutate state, when modal is close', () => {
    const mutation: { openModal: boolean; error: null } = {
      openModal: false,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.openModal(false);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SORT_BY should mutate state, when modal is close', () => {
    const mutation: { sortBy: ISortBy } = {
      sortBy: SortedBy
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setSortBy(SortedBy);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_TEMP_FILTERS should mutate state, when modal is close', () => {
    const expectedData = {
      ...newState,
      tempSortBy: {
        id: '',
        title: ''
      }
    };
    const action = actions.setTempFilters();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
  it('SET_SELECTED_FILTER should mutate state ', () => {
    const state = {
      ...initialStateValue,
      selectedFilters: {
        Color: [
          {
            name: 'black',
            default: false,
            value: '#000',
            quantity: 300
          },
          {
            name: 'White',
            default: false,
            value: '#fff',
            quantity: 1
          }
        ]
      }
    };
    const selectedFilters = {
      Color: [
        {
          name: 'black',
          default: false,
          value: '#000',
          quantity: 300
        }
      ]
    };
    const expectedData = {
      ...newState,
      selectedFilters: {
        Color: [
          {
            name: 'White',
            default: false,
            value: '#fff',
            quantity: 1
          }
        ]
      }
    };
    const actionData = { filterId: 'Color', filter: selectedFilters.Color[0] };
    const action = actions.setSelectedFilter(actionData);
    const newMutatedState = productList(state, action);
    expect(newMutatedState).toEqual(expectedData);
  });
  it('SET_SELECTED_FILTER should mutate state ', () => {
    const state = {
      ...initialStateValue,
      selectedFilters: {
        Color: []
      }
    };
    const selectedFilters = {
      Color: [
        {
          name: 'White',
          default: false,
          value: '#fff',
          quantity: 1
        }
      ]
    };
    const expectedData = {
      ...newState,
      selectedFilters: {
        Color: [
          {
            name: 'White',
            default: false,
            value: '#fff',
            quantity: 1
          }
        ]
      }
    };
    const actionData = { filterId: 'Color', filter: selectedFilters.Color[0] };
    const action = actions.setSelectedFilter(actionData);
    const newMutatedState = productList(state, action);
    expect(newMutatedState).toEqual(expectedData);
  });
  // tslint:disable-next-line:max-file-line-count
});
