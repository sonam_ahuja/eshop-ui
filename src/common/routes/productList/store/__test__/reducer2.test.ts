import productList from '@productList/store/reducer';
import productListState from '@productList/store/state';
import {
  IFilter,
  IGAEvent,
  IGetLead,
  IProductListState,
  IQueryParams,
  ISortBy
} from '@productList/store/types';
import actions from '@productList/store/actions';
import {
  ErrorData,
  Filter,
  NotificationData,
  SortByList,
  SortedBy
} from '@mocks/productList/productList.mock';
import { PROLONGATION_EVENT_NAME } from '@src/common/oneAppRoutes/utils/enum';

// tslint:disable-next-line:no-big-function
describe('Productlist Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: IProductListState = productList(
    undefined,
    definedAction
  );
  let newState: IProductListState = productListState();
  const initializeValue = () => {
    initialStateValue = productList(undefined, definedAction);
    newState = productListState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('SET_NOTIFICATION_DATA should mutate state, when modal is close', () => {
    const expectedData = {
      ...newState,
      notificationData: { ...NotificationData }
    };
    const action = actions.setNotificationData(NotificationData);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SELECTED_FILTER should mutate state, when modal is close', () => {
    const payload: { filterId: string; filter: IFilter } = {
      filterId: '123',
      filter: Filter[0]
    };
    const action = actions.setSelectedFilter(payload);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_FILTERS_AND_SORT_FROM_QUERY_PARAMS should mutate state, when modal is close', () => {
    const payload: {
      queryParams: IQueryParams;
      sortByList: ISortBy[];
      defaultSortBy: ISortBy | null;
    } = {
      queryParams: {
        filters: { filterId: ['1234'] },
        sortBy: null,
        itemPerPage: '1'
      },
      sortByList: SortByList,
      defaultSortBy: SortedBy
    };
    const action = actions.setFiltersAndSortFromQueryParams(payload);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_FILTERS_AND_SORT_FROM_QUERY_PARAMS should mutate state', () => {
    const filterCharacteristics = [
      {
        id: '1',
        name: 'Color',
        type: 'Color',
        data: [
          {
            name: 'White',
            default: false,
            value: '#fff',
            quantity: 300
          },
          {
            name: 'Black',
            default: true,
            value: '#000',
            quantity: 0
          }
        ]
      }
    ];
    const state = { ...initialStateValue, filterCharacteristics };
    const payload: {
      queryParams: IQueryParams;
      sortByList: ISortBy[];
      defaultSortBy: ISortBy | null;
    } = {
      queryParams: {
        filters: { 1: ['#fff', '#000'], 2: ['1234'] },
        sortBy: null,
        itemPerPage: '1'
      },
      sortByList: SortByList,
      defaultSortBy: SortedBy
    };
    const action = actions.setFiltersAndSortFromQueryParams(payload);
    const newMutatedState = productList(state, action);
    expect(newMutatedState).toBeDefined();
  });

  it('SEND_STOCK_NOTIFICATION_ERROR should mutate state, when modal is close', () => {
    const action = actions.sendStockNotificationError(ErrorData);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('CLEAR_ALL_SELECTED_FILTERS should mutate state, when modal is close', () => {
    const action = actions.clearAllSelectedFilter();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('SEND_STOCK_NOTIFICATION_LOADING should mutate state, when modal is close', () => {
    const action = actions.sendStockNotificationLoading();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_FILTERS_FROM_TEMP_FILTERS should mutate state, when modal is close', () => {
    const expectedData = { ...newState, sortBy: null };
    const action = actions.setFiltersFromTempFilters();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
  it('CLEAR_PRODUCT_LIST_DATA should clear data', () => {
    const expectedData = { ...newState };
    const action = actions.clearProductListData();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
  it('RESET_DATA should reset data', () => {
    const expectedData = { ...newState, sortBy: null };
    const action = actions.resetData();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SCROLL_POSITION should reset data', () => {
    const expectedData = {
      ...newState,
      lastScrollPositionMap: {
        '/basket': 23
      }
    };
    const action = actions.setScrollPosition({
      urlPath: '/basket',
      position: 23
    });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_LAST_PAGE_NUMBER should reset data', () => {
    const expectedData = { ...newState, lastListURL: '/checkout' };
    const action = actions.setlastListURL('/checkout');
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('CLEAR_SCROLL_POSITION should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.clearScrollPosition({ urlPath: '/checkout' });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SORT_BY_LIST should reset data', () => {
    const expectedData = { ...newState, sortBy: null };
    const action = actions.setSortByList({
      sortByList: [],
      sortBy: null
    });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_POPULAR_DEVICE should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setPopularDevice([]);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_TARIFF_LOYALTY should reset data', () => {
    const expectedData = {
      ...newState,
      discountId: '1',
      loyalty: '',
      tariffId: ''
    };
    const action = actions.setTariffLoyalty({
      discountId: '1',
      loyalty: '',
      tariffId: ''
    });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_PROLONGATION_PRODUCT_LIST_DATA should reset data', () => {
    const expectedData = { ...newState, categoryName: '1' };
    const action = actions.setProlongationProductList({
      category: '1',
      data: []
    });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SELECTED_VARIANT should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setSelectedVariantId('');
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_SELECTED_PRODUCT should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setSelectedProductId('');
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_UUID should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setUuid('');
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_PROFILE_ID should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setProfileId('');
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_PROLONGATION_CHECKOUT_DETAILS should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setProlongationCheckoutDetails({ data: [] });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('UPDATE_PROLONGATION_FORM_FIELD should reset data', () => {
    const action = actions.updateFormField({ field: '', value: '' });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('SET_SELECTED_ATTR_FOR_PROLONGATION should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setSelectedAttrForProlongation([]);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_PROLONGATION_OPENED_FROM should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.setProlongationOpenedFrom('');
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SEND_MOENGAGE_EVENT_SUCCESS should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.sendMoengageEventSuccess();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SEND_MOENGAGE_EVENT should reset data', () => {
    const params: IGAEvent = {
      hitType: 'string',
      eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
      eventAction: 'string',
      eventLabel: 'string',
      eventValue: 1,
      page: 'string',
      url: 'string',
      opened_from: 'string',
      device_id: 'string',
      device_name: 'string',
      device_amt_upfront: 'string',
      device_amt_monthly: 'string',
      amt: 'string',
      currencyCode: '',
      currencySymbol: ''
    };
    const action = actions.sendMoengageEvent(params);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  it('GENERATE_TOKEN_SUCCESS should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.generateTokenSuccess({
      access_token: '',
      refresh_token: '',
      refresh_expires_in: 12,
      expires_in: 23,
      token_type: '',
      id_token: '',
      session_state: '',
      scope: ''
    });
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PROLONGATION_PRODUCT_LIST should reset data', () => {
    const expectedData = { ...newState };
    // tslint:disable-next-line: no-any
    const action = actions.fetchProlongationProductList({} as any);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PROLONGATION_PRODUCT_DETAILS should reset data', () => {
    const expectedData = { ...newState };
    // tslint:disable-next-line: no-any
    const action = actions.fetchProlongationProductDetails({} as any);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PROLONGATION_CHECKOUT_DETAILS should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.fetchProlongationCheckoutDetails(true);
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_PROLONGATION_CHECKOUT_DETAILS_SUCCESS should reset data', () => {
    const expectedData = { ...newState, showAppShell: false };
    const action = actions.fetchProlongationCheckoutDetailsSuccess();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('PLACE_ORDER should reset data', () => {
    const expectedData = { ...newState, loading: true };
    const action = actions.placeOrder();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('PLACE_ORDER_SUCCESS should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.placeOrderSuccess();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('PLACE_ORDER_ERROR should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.placeOrderError();
    const newMutatedState = productList(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  // tslint:disable-next-line: no-commented-code
  it('GET_LEAD_SUCCESS should reset data', () => {
    const getLead: IGetLead = {
      magenta: {
        type: 'A2A',
        bundles: ['c1e1d300-c2b9-46f5-8853-8bae9aa63b2e'],
        // tslint:disable-next-line:no-duplicate-string
        partyId: ['5fba0646-f499-46b9-862e-6a09f2e39b41']
      },
      digitalBanking: {
        id: '183c3f62-1d80-44f5-a329-9c9aa067f0fe',
        label: 'Your Digital Bank',
        name: 'Digital bank'
      },
      userProfileRelatedPartyId: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      status: 'validated',
      detailedStatus: 'validated',
      individual: {
        gender: 'male',
        birthDate: '1980-12-22',
        givenName: 'John',
        familyName: 'Smith'
      },
      characteristics: [
        {
          name: 'telekom_username',
          value: 'jsmith'
        },
        {
          name: 'engagementCardName',
          value: 'John Smith'
        },
        {
          name: 'engagementCardId',
          value: '1111 2222 3333 4444'
        }
      ],
      relatedParties: [
        {
          id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
          role: 'customer',
          name: 'John Smith'
        }
      ],
      contactMediums: [
        {
          type: 'email',
          role: {
            name: 'contact'
          },
          medium: {
            emailAddress: 'jsmith@natco.com',
            number: ''
          }
        },
        {
          type: 'phone',
          role: {
            name: 'contact'
          },
          medium: {
            number: '+3851444444',
            emailAddress: ''
          }
        },
        {
          type: 'mobile',
          role: {
            name: 'contact'
          },
          medium: {
            number: '+385981234567',
            emailAddress: 'string'
          }
        }
      ],
      manageableAssets: [
        {
          id: 'c228c28d-e19d-4d4c-a77e-0baaa0b87337',
          segment: 'B2C',
          privilege: 'user',
          priority: 'secondary',
          category: 'fixedVoice',
          name: '+385 999 888 88 ',
          label: 'Home landline',
          description: 'Home M plan (in Magenta1 package)',
          enabled: true,
          relatedParties: [],
          bundleId: 'c1e1d300-c2b9-46f5-8853-8bae9aa63b2e',
          userType: 'B2CUSER'
        }
      ],
      bundles: [
        {
          id: '17e0b3fb-1cc9-4c77-98e7-68a97608e440',
          description: '',
          privilege: 'user',
          name: 'Family package,',
          label: 'Family package',
          type: 'normal',
          enabled: true
        }
      ]
    };
    const action = actions.getLeadSuccess(getLead);
    productList(initialStateValue, action);
    expect(true).toEqual(true);
  });
  // tslint:disable-next-line: max-file-line-count
});
