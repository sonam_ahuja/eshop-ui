import sagas, {
  fetchProductList,
  loadMoreProductList
} from '@productList/store/sagas';
import constants from '@productList/store/constants';
import { FetchProductListParams } from '@mocks/productList/productList.mock';
import {
  IFetchProductListParams,
  ILoadMoreProductListParams
} from '@productList/store/types';

describe('Product List Saga', () => {
  window.scrollTo = jest.fn();
  it('Product saga test', () => {
    const generator = sagas();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  it('fetchProductList saga test', () => {
    const action: {
      type: string;
      payload: IFetchProductListParams;
    } = {
      type: constants.FETCH_PRODUCT_LIST_REQUESTED,
      payload: FetchProductListParams
    };

    const generator = fetchProductList(action);
    expect(generator.next().value).toBeDefined();
  });

  it('loadMoreProductList saga test', () => {
    const action: {
      type: string;
      payload: ILoadMoreProductListParams;
    } = {
      type: constants.LOAD_MORE_PRODUCT_LIST_REQUESTED,
      payload: { isChanged: true, categoryId: '1', pageNumber: 12 }
    };

    const closingModalGenerator = loadMoreProductList(action);
    expect(closingModalGenerator.next().value).toBeDefined();
  });
});
