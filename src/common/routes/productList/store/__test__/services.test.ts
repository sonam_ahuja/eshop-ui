import {
  fetchPopularDeviceService,
  fetchProductListService,
  fetchProlongationCheckoutDetailsService,
  fetchProlongationProductListService,
  generateTokenService,
  getLeadService,
  getProlongationLeadKeyService,
  IPayload,
  placeProlongationOrderService,
  postLeadService,
  refreshTokenService,
  sendMoengageEventService,
  sendStockNotificationService
} from '@productList/store/services';
import { apiEndpoints } from '@common/constants';
import {
  FetchProlongationProductList,
  ProlongationCheckoutRequestPayload,
  ProlongationPlaceOrderRequest
} from '@mocks/productList/porlongation.mock';
import categoryState from '@productList/store/state';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { SendStockPayload } from '@mocks/productList/productList.mock';

// tslint:disable-next-line: no-big-function
describe('Product List Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchProductListService test', async () => {
    const payload: IPayload = {
      tariffId: '123',
      loyalty: '',
      discountId: '',
      filters: [
        {
          name: 'filter',
          characteristicValues: ['string'],
          provideQuantity: false
        }
      ],
      sortBy: 'fiter',
      currentPage: 3,
      itemsPerPage: 10,
      categoryId: '1234'
    };
    const url: string = apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchProductListService(payload);
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('sendStockNotificationService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.SEND_STOCK_NOTIFICATION.url;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await sendStockNotificationService(
          SendStockPayload,
          'reason'
        );
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('fetchPopularDeviceService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.SEND_STOCK_NOTIFICATION.url;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchPopularDeviceService({
          categoryId: 'string',
          tariffId: null,
          loyalty: null,
          discountId: '1'
        });
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('fetchProlongationProductListService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.PROLONGATION.GET_PRODUCTS_LIST.url('op');
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchProlongationProductListService(
          FetchProlongationProductList
        );
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('fetchProlongationCheckoutDetailsService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.PROLONGATION.CHECKOUT.url('s');
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchProlongationCheckoutDetailsService(
          ProlongationCheckoutRequestPayload
        );
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('placeProlongationOrderService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.PROLONGATION.PLACE_ORDER.url('h');
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await placeProlongationOrderService(
          ProlongationPlaceOrderRequest
        );
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('getLeadService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.PROLONGATION.GET_LEAD.url(
      'sub'
    );
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await getLeadService({
          sub: 'string',
          bearerToken: 'string'
        });
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('postLeadService test', async () => {
    const url: string = apiEndpoints.PRODUCTLIST.PROLONGATION.POST_LEAD.url(
      'sub'
    );
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await postLeadService('uuid', {
          data: 'string',
          enc_key: 'string',
          rsa_key_label: 'string',
          uuid: 'string',
          category_id: 'string',
          segment_id: 'string',
          natco_code: 'string',
          profileId: 'string'
        });
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('generateTokenService test', async () => {
    const { url } = apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await generateTokenService();
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('generateTokenService test', async () => {
    const { url } = apiEndpoints.PRODUCTLIST.PROLONGATION.REFRESH_TOKEN;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await refreshTokenService({
          refreshToken: 'string'
        });
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('sendMoengageEventService test', async () => {
    const { url } = apiEndpoints.PRODUCTLIST.PROLONGATION.MOENGAGE_PUSH_EVENTS;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await sendMoengageEventService({
          payloadData: {
            eventId: 'string',
            customerId: 'string', // mandatory
            messageId: 'string',
            action: 'string',
            attributes: []
          },
          accessToken: 'string'
        });
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('getProlongationLeadKeyService test', async () => {
    const { url } = apiEndpoints.PRODUCTLIST.PROLONGATION.MOENGAGE_PUSH_EVENTS;
    const expectedData = categoryState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await getProlongationLeadKeyService({
          bearerToken: 'string',
          uuid: 'string'
        });
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });
});
