import {
  changeInnerConsent,
  createMarketingAgreement,
  getLeadTransform,
  getPopularDevice,
  marketingConsentOuterChange,
  payloadForPostUserInfotLead,
  transformFilterCharacteristics,
  transformMoengageAttribute,
  transformProlongationProductData
} from '@productList/store/transformer';
import appState from '@store/states/app';
import {
  FilterCharacteristics,
  ProductListResponse
} from '@mocks/productList/productList.mock';
import APP_KEYS from '@common/constants/appkeys';
import {
  GetLeadResponse,
  ProlongationCheckoutProduct,
  ProlongationProductListResponse
} from '@src/common/mock-api/productList/porlongation.mock';
import { PROLONGATION_EVENT_NAME } from '@src/common/oneAppRoutes/utils/enum';
import { IGetLead } from '@productList/store/types';

const userInfo = {
  email: '',
  msisdn: '',
  firstName: '',
  lastName: '',
  billingAddress: '',
  categoryId: '',
  deliveryContact: '',
  segmentId: '',
  deliveryAddress: '',
  contactEmail: ''
};

const gaEvent = {
  hitType: '',
  eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
  eventAction: '',
  eventLabel: '',
  eventValue: 1,
  page: '',
  url: '',
  opened_from: '',
  device_id: '',
  device_name: '',
  device_amt_upfront: '',
  device_amt_monthly: '',
  amt: ''
};

// tslint:disable-next-line: no-big-function
describe('Transformer', () => {
  it('transformFilterCharacteristics when product filter had id APP_KEYS ::', () => {
    expect(
      transformFilterCharacteristics(
        FilterCharacteristics,
        appState().translation.cart.productList
      )
    ).toBeDefined();
  });

  it('transformFilterCharacteristics ::', () => {
    FilterCharacteristics[0].id = APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID;
    expect(
      transformFilterCharacteristics(
        FilterCharacteristics,
        appState().translation.cart.productList
      )
    ).toBeDefined();
  });

  it('getPopularDevice ::', () => {
    expect(getPopularDevice(ProductListResponse)).toBeDefined();
  });

  it('transformProlongationProductData ::', () => {
    expect(
      transformProlongationProductData(ProlongationProductListResponse)
    ).toBeDefined();
  });

  it('getLeadTransform ::', () => {
    expect(getLeadTransform(userInfo, GetLeadResponse)).toBeUndefined();
  });

  it('transformMoengageAttribute ::', () => {
    expect(transformMoengageAttribute(gaEvent)).toBeDefined();
  });

  it('payloadForPostUserInfotLead ::', () => {
    expect(
      payloadForPostUserInfotLead(
        userInfo,
        ProlongationCheckoutProduct,
        'test_11_7',
        [
          {
            name: 'dummy',
            description: 'dummy',
            isSelected: true,
            isMandatory: false,
            subConsent: [
              {
                name: 'dummy',
                description: 'dummy',
                isSelected: false,
                isMandatory: false
              }
            ]
          }
        ],
        appState().translation.prolongation,
        false
      )
    ).toBeDefined();
  });

  it('should test changeInnerConsent ', () => {
    expect(
      changeInnerConsent({
        name: 'dummy',
        description: 'dummy',
        isSelected: true,
        isMandatory: false,
        subConsent: [
          {
            name: 'dummy',
            description: 'dummy',
            isMandatory: false,
            isSelected: false
          }
        ]
      })
    ).toBeUndefined();
  });

  it('should test marketingConsentOuterChange', () => {
    expect(
      marketingConsentOuterChange(
        [
          {
            name: 'dummy',
            description: 'dummy',
            isSelected: false,
            isMandatory: false
          }
        ],
        true
      )
    ).toBeUndefined();
  });

  it('should test createMarketingAgreement', () => {
    expect(
      createMarketingAgreement(
        [
          {
            name: 'dummy',
            description: 'dummy',
            isSelected: true,
            isMandatory: false,
            subConsent: [
              {
                name: 'dummy',
                description: 'dummy',
                isSelected: false,
                isMandatory: false
              }
            ]
          }
        ],
        appState().translation.prolongation,
        false
      )
    ).toBeDefined();
  });

  it('transformFilterCharacteristics ::', () => {
    appState().translation.cart.productList.availability.IN_STOCK = 'abc';
    appState().translation.cart.productList.availability.OUT_OF_STOCK = 'abc';
    appState().translation.cart.productList.availability.PRE_ORDER = 'abc';
    FilterCharacteristics[0].id = APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID;
    expect(
      transformFilterCharacteristics(
        FilterCharacteristics,
        appState().translation.cart.productList
      )
    ).toBeDefined();
  });

  it('transformMoengageAttribute ::', () => {
    const payloadGA = {
      hitType: '',
      eventCategory: PROLONGATION_EVENT_NAME.ERROR,
      eventAction: '',
      eventLabel: '',
      eventValue: 0,
      page: '',
      url: '',
      opened_from: '',
      device_id: '',
      device_name: '',
      device_amt_upfront: '',
      device_amt_monthly: '',
      amt: ''
    };
    expect(transformMoengageAttribute(payloadGA)).toBeDefined();
  });

  it('getLeadTransform ::', () => {
    const getLead: IGetLead = {
      magenta: {
        type: 'A2A',
        bundles: ['c1e1d300-c2b9-46f5-8853-8bae9aa63b2e'],
        // tslint:disable-next-line:no-duplicate-string
        partyId: ['5fba0646-f499-46b9-862e-6a09f2e39b41']
      },
      digitalBanking: {
        id: '183c3f62-1d80-44f5-a329-9c9aa067f0fe',
        label: 'Your Digital Bank',
        name: 'Digital bank'
      },
      userProfileRelatedPartyId: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
      status: 'validated',
      detailedStatus: 'validated',
      individual: {
        gender: 'male',
        birthDate: '1980-12-22',
        givenName: 'John',
        familyName: 'Smith'
      },
      characteristics: [
        {
          name: 'telekom_username',
          value: 'jsmith'
        },
        {
          name: 'engagementCardName',
          value: 'John Smith'
        },
        {
          name: 'engagementCardId',
          value: '1111 2222 3333 4444'
        }
      ],
      relatedParties: [
        {
          id: '5fba0646-f499-46b9-862e-6a09f2e39b41',
          role: 'customer',
          name: 'John Smith'
        }
      ],
      contactMediums: [
        {
          type: 'email',
          role: {
            name: 'contact'
          },
          medium: {
            emailAddress: 'jsmith@natco.com',
            number: ''
          }
        },
        {
          type: 'phone',
          role: {
            name: 'contact'
          },
          medium: {
            number: '+3851444444',
            emailAddress: ''
          }
        },
        {
          type: 'mobile',
          role: {
            name: 'contact'
          },
          medium: {
            number: '+385981234567',
            emailAddress: 'string'
          }
        }
      ],
      manageableAssets: [
        {
          id: 'c228c28d-e19d-4d4c-a77e-0baaa0b87337',
          segment: 'B2C',
          privilege: 'user',
          priority: 'secondary',
          category: 'fixedVoice',
          name: '+385 999 888 88 ',
          label: 'Home landline',
          description: 'Home M plan (in Magenta1 package)',
          enabled: true,
          relatedParties: [],
          bundleId: 'c1e1d300-c2b9-46f5-8853-8bae9aa63b2e',
          userType: 'B2CUSER'
        }
      ],
      bundles: [
        {
          id: '17e0b3fb-1cc9-4c77-98e7-68a97608e440',
          description: '',
          privilege: 'user',
          name: 'Family package,',
          label: 'Family package',
          type: 'normal',
          enabled: true
        }
      ]
    };

    expect(getLeadTransform(userInfo, getLead)).toBeUndefined();
  });

  it('transformProlongationProductData ::', () => {
    expect(
      transformProlongationProductData({
        category: 'string',
        data: []
      })
    ).toBeDefined();
  });
  // tslint:disable-next-line:max-file-line-count
});
