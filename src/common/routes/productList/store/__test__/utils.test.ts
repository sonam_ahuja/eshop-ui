import {
  getConfiguration,
  getFilterParamsFromQueryOrState,
  getProductVariantIDFromURLForProlongationCheckout,
  getProlongationCmsConfiguration,
  getProlongationOpenedFrom,
  getSortByFromSortList,
  getState,
  getTranslation
} from '@productList/store/utils';
import appState from '@store/states/app';
import { SelectedFilter, SortedBy } from '@mocks/productList/productList.mock';

describe('Product List Service', () => {
  it('getConfiguration test', () => {
    expect(getConfiguration(appState())).toBeDefined();
  });

  it('getConfiguration test', () => {
    expect(
      getFilterParamsFromQueryOrState(SortedBy, SelectedFilter)
    ).toBeDefined();
    expect(
      getFilterParamsFromQueryOrState(SortedBy, SelectedFilter)
    ).toBeDefined();
  });

  it('getState test', () => {
    expect(getState(appState())).toBeDefined();
  });

  it('getTranslation test', () => {
    expect(getTranslation(appState())).toBeDefined();
  });

  it('getProlongationOpenedFrom test', () => {
    expect(getProlongationOpenedFrom(appState())).toBeDefined();
  });

  it('getProlongationCmsConfiguration test', () => {
    expect(getProlongationCmsConfiguration(appState())).toBeDefined();
  });

  it('getProlongationCmsConfiguration', () => {
    expect(getProlongationCmsConfiguration(appState())).toBeDefined();
  });

  it('getSortByFromSortList test', () => {
    expect(
      getSortByFromSortList(
        [
          {
            id: 'string',
            title: 'string'
          }
        ],
        null
      )
    ).toBeDefined();
  });

  it('getSortByFromSortList test sortBy:null', () => {
    expect(
      getSortByFromSortList(
        [
          {
            id: '3',
            title: ''
          }
        ],
        '3'
      )
    ).toBeDefined();
  });

  it('getFilterParamsFromQueryOrState', () => {
    expect(
      getFilterParamsFromQueryOrState(
        {
          id: '1',
          title: ''
        },
        {
          1: [
            {
              name: 'name',
              default: false,
              value: '',
              quantity: 0
            }
          ]
        },
        {
          sortBy: '',
          filters: {
            1: ['1', '2', '3']
          },
          itemPerPage: '10'
        }
      )
    ).toBeDefined();
  });
  it('getProductVariantIDFromURLForProlongationCheckout', () => {
    const url =
      // tslint:disable-next-line: max-line-length
      '/Devices-Mobile/Nokia17%20-%20128Gb%20Yellow%20Nokia/Nokia_17/nokia17_v1?%26tariffId%3Dbest_s%26agreementId%3Dagreement12%26instalmentId%3D20';
    expect(getProductVariantIDFromURLForProlongationCheckout(url));
  });

  it('getProductVariantIDFromURLForProlongationCheckout test', () => {
    expect(
      getProductVariantIDFromURLForProlongationCheckout('string')
    ).toBeDefined();
  });
});
