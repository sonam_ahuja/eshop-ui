import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@productList/store/constants';
import {
  IChangedAttribute,
  IError,
  IFetchProductListParams,
  IFetchProlongationProductListPayload,
  IFilter,
  IGAEvent,
  IGetLead,
  IGetNotificationRequestTemplatePayload,
  ILoadMoreProductListParams,
  INotificationData,
  IProlongationCheckoutResponse,
  IProlongationFormValidation,
  IProlongationProductDetailedPayload,
  IProlongationProductListResponse,
  IQueryParams,
  ISortBy,
  ISortList,
  ITariffParams,
  ITokenResponse,
  IVariant
} from '@productList/store/types';
import * as productListApi from '@common/types/api/productList';
import { IMegaMenu } from '@src/common/store/types/common';

export default {
  fetchProductList: actionCreator<IFetchProductListParams>(
    CONSTANTS.FETCH_PRODUCT_LIST_REQUESTED
  ),
  fetchProductListLoading: actionCreator<boolean>(
    CONSTANTS.FETCH_PRODUCT_LIST_LOADING
  ),
  fetchProductListError: actionCreator<Error>(
    CONSTANTS.FETCH_PRODUCT_LIST_ERROR
  ),
  fetchProductListSuccess: actionCreator<productListApi.POST.IResponse>(
    CONSTANTS.FETCH_PRODUCT_LIST_SUCCESS
  ),
  clearProductListData: actionCreator(CONSTANTS.CLEAR_PRODUCT_LIST_DATA),
  loadMoreProductList: actionCreator<ILoadMoreProductListParams>(
    CONSTANTS.LOAD_MORE_PRODUCT_LIST_REQUESTED
  ),
  updateReceivedProductListDataFromServer: actionCreator<{
    receivedProductListDataFromServer: boolean;
  }>(CONSTANTS.RECEIVED_DATA_FROM_SERVER),
  setSelectedFilter: actionCreator<{ filterId: string; filter: IFilter }>(
    CONSTANTS.SET_SELECTED_FILTER
  ),
  clearAllSelectedFilter: actionCreator(CONSTANTS.CLEAR_ALL_SELECTED_FILTERS),
  setSortBy: actionCreator<ISortBy>(CONSTANTS.SET_SORT_BY),
  setTempFilters: actionCreator(CONSTANTS.SET_TEMP_FILTERS),
  setFiltersFromTempFilters: actionCreator(
    CONSTANTS.SET_FILTERS_FROM_TEMP_FILTERS
  ),
  setFiltersAndSortFromQueryParams: actionCreator<{
    queryParams: IQueryParams;
  }>(CONSTANTS.SET_FILTERS_AND_SORT_FROM_QUERY_PARAMS),
  changeItemPerPage: actionCreator<number>(CONSTANTS.CHANGE_ITEM_PER_PAGE),
  updateItemPerPage: actionCreator<number>(CONSTANTS.UPDATE_ITEM_PER_PAGE),
  updateShowAppShell: actionCreator<boolean>(CONSTANTS.UPDATE_SHOW_APP_SHELL),
  sendStockNotification: actionCreator<IGetNotificationRequestTemplatePayload>(
    CONSTANTS.SEND_STOCK_NOTIFICATION
  ),
  sendStockNotificationError: actionCreator<Error>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_ERROR
  ),
  sendStockNotificationSuccess: actionCreator<void>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_SUCCESS
  ),
  sendStockNotificationLoading: actionCreator<void>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_LOADING
  ),
  setError: actionCreator<IError>(CONSTANTS.SET_ERROR),
  openModal: actionCreator<boolean>(CONSTANTS.OPEN_MODAL),
  setNotificationData: actionCreator<INotificationData>(
    CONSTANTS.SET_NOTIFICATION_DATA
  ),
  setScrollPosition: actionCreator<{ urlPath: string; position: number }>(
    CONSTANTS.SET_SCROLL_POSITION
  ),
  clearScrollPosition: actionCreator<{ urlPath: string }>(
    CONSTANTS.CLEAR_SCROLL_POSITION
  ),
  setlastListURL: actionCreator<string>(CONSTANTS.SET_LAST_PAGE_NUMBER),
  fetchPopularDevices: actionCreator<string>(CONSTANTS.FETCH_POPULAR_DEVICE),
  setPopularDevice: actionCreator<IVariant[]>(CONSTANTS.SET_POPULAR_DEVICE),
  resetData: actionCreator(CONSTANTS.RESET_DATA),
  setSortByList: actionCreator<{
    sortByList: ISortList[];
    sortBy: ISortList | null;
  }>(CONSTANTS.SET_SORT_BY_LIST),

  setTariffLoyalty: actionCreator<ITariffParams>(CONSTANTS.SET_TARIFF_LOYALTY),

  /** BELOW ACTIONS USED ON PROLONGATION */
  fetchProlongationProductList: actionCreator<
    IFetchProlongationProductListPayload
  >(CONSTANTS.FETCH_PROLONGATION_PRODUCT_LIST),
  fetchProlongationProductDetails: actionCreator<
    IProlongationProductDetailedPayload
  >(CONSTANTS.FETCH_PROLONGATION_PRODUCT_DETAILS),
  setProlongationProductList: actionCreator<IProlongationProductListResponse>(
    CONSTANTS.SET_PROLONGATION_PRODUCT_LIST_DATA
  ),
  setSelectedVariantId: actionCreator<string>(CONSTANTS.SET_SELECTED_VARIANT),
  setSelectedProductId: actionCreator<string>(CONSTANTS.SET_SELECTED_PRODUCT),
  setSelectedAttrForProlongation: actionCreator<IChangedAttribute[]>(
    CONSTANTS.SET_SELECTED_ATTR_FOR_PROLONGATION
  ),
  setUuid: actionCreator<string>(CONSTANTS.SET_UUID),
  setProfileId: actionCreator<string>(CONSTANTS.SET_PROFILE_ID),
  fetchProlongationCheckoutDetails: actionCreator<boolean>(
    CONSTANTS.FETCH_PROLONGATION_CHECKOUT_DETAILS
  ),
  fetchProlongationCheckoutDetailsSuccess: actionCreator(
    CONSTANTS.FETCH_PROLONGATION_CHECKOUT_DETAILS_SUCCESS
  ),
  setProlongationCheckoutDetails: actionCreator<IProlongationCheckoutResponse>(
    CONSTANTS.SET_PROLONGATION_CHECKOUT_DETAILS
  ),
  updateFormField: actionCreator<{ field: string; value: string }>(
    CONSTANTS.UPDATE_PROLONGATION_FORM_FIELD
  ),
  updateFormValidStatus: actionCreator<{ field: string; value: boolean }>(
    CONSTANTS.UPDATE_PROLONGATION_FORM_FIELD_STATUS
  ),
  placeOrder: actionCreator(CONSTANTS.PLACE_ORDER),
  placeOrderSuccess: actionCreator(CONSTANTS.PLACE_ORDER_SUCCESS),
  placeOrderError: actionCreator(CONSTANTS.PLACE_ORDER_ERROR),
  getLead: actionCreator(CONSTANTS.GET_LEAD),
  getLeadSuccess: actionCreator<IGetLead>(CONSTANTS.GET_LEAD_SUCCESS),
  getLeadError: actionCreator(CONSTANTS.GET_LEAD_ERROR),
  generateToken: actionCreator(CONSTANTS.GENERATE_TOKEN),
  refreshToken: actionCreator<{ type: string; data?: IGAEvent }>(
    CONSTANTS.REFRESH_TOKEN
  ),
  generateTokenSuccess: actionCreator<ITokenResponse>(
    CONSTANTS.GENERATE_TOKEN_SUCCESS
  ),
  generateTokenError: actionCreator(CONSTANTS.GENERATE_TOKEN_ERROR),
  setProlongationOpenedFrom: actionCreator<string>(
    CONSTANTS.SET_PROLONGATION_OPENED_FROM
  ),
  sendMoengageEvent: actionCreator<IGAEvent>(CONSTANTS.SEND_MOENGAGE_EVENT),
  sendMoengageEventSuccess: actionCreator(
    CONSTANTS.SEND_MOENGAGE_EVENT_SUCCESS
  ),
  sendMoengageEventError: actionCreator(CONSTANTS.SEND_MOENGAGE_EVENT_ERROR),
  getRSALeadKey: actionCreator(CONSTANTS.GET_RSA_LEAD_REQUESTED),
  getRSALeadKeySuccess: actionCreator(CONSTANTS.GET_RSA_LEAD_SUCCESS),
  getRSALeadKeyError: actionCreator(CONSTANTS.GET_RSA_LEAD_ERROR),
  orderSummaryEvent: actionCreator<boolean>(
    CONSTANTS.ORDER_SUMMARY_OPEN_CLOSE_EVENT
  ),
  toggleTermsAndCondition: actionCreator<void>(
    CONSTANTS.TOGGLE_TERMS_AND_CONDITION
  ),
  setMarketingAgreement: actionCreator<IMegaMenu>(
    CONSTANTS.SET_MARKETING_AGREEMENT
  ),
  mainAgreementToggle: actionCreator<number>(CONSTANTS.MAIN_AGREEMENT_TOGGLE),
  callProlongationCheckoutAction: actionCreator<void>(
    CONSTANTS.CALL_PROLONGATION_CHECKOUT_ACTION
  ),
  innerAgreementToggle: actionCreator<{
    outerIndex: number;
    innerIndex: number;
  }>(CONSTANTS.INNER_AGREEMENT_TOGGLE),
  checkProlongationFormValidation: actionCreator<IProlongationFormValidation>(CONSTANTS.CHECK_FORM_PROLONGATION_VALIDATION),
  formSubmitAction: actionCreator<boolean>(CONSTANTS.FORM_SUBMIT_ACTION),
};
