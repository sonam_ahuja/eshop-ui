export const namespace = 'PRODUCT_LIST';
export const prolongation = 'PROLONGATION_PRODUCT_LIST';

export default {
  FETCH_PRODUCT_LIST_REQUESTED: `${namespace}_FETCH_PRODUCT_LIST_REQUESTED`,
  FETCH_PRODUCT_LIST_LOADING: `${namespace}_FETCH_PRODUCT_LIST_LOADING`,
  FETCH_PRODUCT_LIST_ERROR: `${namespace}_FETCH_PRODUCT_LIST_ERROR`,
  FETCH_PRODUCT_LIST_SUCCESS: `${namespace}_FETCH_PRODUCT_LIST_SUCCESS`,
  CLEAR_PRODUCT_LIST_DATA: `${namespace}_CLEAR_PRODUCT_LIST_DATA`,
  LOAD_MORE_PRODUCT_LIST_REQUESTED: `${namespace}_LOAD_MORE_PRODUCT_LIST_REQUESTED`,

  RECEIVED_DATA_FROM_SERVER: `${namespace}_RECEIVED_DATA_FROM_SERVER`,

  SET_PRODUCT_LIST_DATA: `${namespace}_SET_PRODUCT_LIST_DATA`,
  CHANGE_ITEM_PER_PAGE: `${namespace}_CHANGE_ITEM_PER_PAGE`,
  UPDATE_ITEM_PER_PAGE: `${namespace}_UPDATE_ITEM_PER_PAGE`,
  UPDATE_SHOW_APP_SHELL: `${namespace}_UPDATE_SHOW_APP_SHELL`,

  SET_SELECTED_FILTER: `${namespace}_SET_SELECTED_FILTER`,
  SET_SORT_BY: `${namespace}_SET_SORT_BY`,
  SET_TEMP_FILTERS: `${namespace}_SET_TEMP_FILTERS`,
  SET_FILTERS_FROM_TEMP_FILTERS: `${namespace}_SET_FILTERS_FROM_TEMP_FILTERS`,
  SET_FILTERS_AND_SORT_FROM_QUERY_PARAMS: `${namespace}_SET_FILTERS_AND_SORT_FROM_QUERY_PARAMS`,
  CLEAR_ALL_SELECTED_FILTERS: `${namespace}_CLEAR_ALL_SELECTED_FILTERS`,

  SEND_STOCK_NOTIFICATION: `${namespace}_SEND_STOCK_NOTIFICATION`,
  SEND_STOCK_NOTIFICATION_ERROR: `${namespace}_SEND_STOCK_NOTIFICATION_ERROR`,
  SEND_STOCK_NOTIFICATION_SUCCESS: `${namespace}_SEND_STOCK_NOTIFICATION_SUCCESS`,
  SET_ERROR: `${namespace}_SET_ERROR`,
  OPEN_MODAL: `${namespace}_OPEN_MODAL`,
  SET_NOTIFICATION_DATA: `${namespace}_SET_NOTIFICATION_DATA`,
  SEND_STOCK_NOTIFICATION_LOADING: `${namespace}_SEND_STOCK_NOTIFICATION_LOADING`,

  SET_SCROLL_POSITION: `${namespace}_SET_SCROLL_POSITION`,
  CLEAR_SCROLL_POSITION: `${namespace}_CLEAR_SCROLL_POSITION`,
  SET_LAST_PAGE_NUMBER: `${namespace}_SET_LAST_PAGE_NUMBER`,
  FETCH_POPULAR_DEVICE: `${namespace}_FETCH_POPULAR_DEVICE`,
  SET_POPULAR_DEVICE: `${namespace}_SET_POPULAR_DEVICE`,
  RESET_DATA: `${namespace}_RESET_DATA`,
  SET_SORT_BY_LIST: `${namespace}_SET_SORT_BY_LIST`,
  SET_TARIFF_LOYALTY: `${namespace}_SET_TARIFF_LOYALTY`,

  // PROLONGATION SET OPENED FROM
  SET_PROLONGATION_OPENED_FROM: `${prolongation}_SET_PROLONGATION_OPENED_FROM`,
  // PROLONGATION PRODUCT LIST
  FETCH_PROLONGATION_PRODUCT_LIST: `${prolongation}_FETCH_PROLONGATION_PRODUCT_LIST`,
  FETCH_PROLONGATION_PRODUCT_LIST_ERROR: `${prolongation}_FETCH_PROLONGATION_PRODUCT_LIST_ERROR`,
  FETCH_PROLONGATION_PRODUCT_LIST_SUCCESS: `${prolongation}_FETCH_PROLONGATION_PRODUCT_LIST_SUCCESS`,

  // PROLONGATION PRODUCT Details
  FETCH_PROLONGATION_PRODUCT_DETAILS: `${prolongation}_FETCH_PROLONGATION_PRODUCT_DETAILS`,

  SET_PROLONGATION_PRODUCT_LIST_DATA: `${prolongation}_SET_PROLONGATION_PRODUCT_LIST_DATA`,
  SET_SELECTED_VARIANT: `${prolongation}_SET_SELECTED_VARIANT`,
  SET_SELECTED_PRODUCT: `${prolongation}_SET_SELECTED_PRODUCT`,
  SET_UUID: `${prolongation}_SET_UUID`,
  SET_PROFILE_ID: `${prolongation}_SET_PROFILE_ID`,
  SET_SELECTED_ATTR_FOR_PROLONGATION: `${prolongation}_SET_SELECTED_ATTR_FOR_PROLONGATION`,
  // CHECKOUT DETAILS OF PROLONGATION
  FETCH_PROLONGATION_CHECKOUT_DETAILS: `${prolongation}_FETCH_PROLONGATION_CHECKOUT_DETAILS`,
  FETCH_PROLONGATION_CHECKOUT_DETAILS_ERROR: `${prolongation}_FETCH_PROLONGATION_CHECKOUT_DETAILS_ERROR`,
  FETCH_PROLONGATION_CHECKOUT_DETAILS_SUCCESS: `${prolongation}_FETCH_PROLONGATION_CHECKOUT_DETAILS_SUCCESS`,
  SET_PROLONGATION_CHECKOUT_DETAILS: `${prolongation}_SET_PROLONGATION_CHECKOUT_DETAILS`,
  UPDATE_PROLONGATION_FORM_FIELD: `${prolongation}_UPDATE_PROLONGATION_FORM_FIELD`,
  PLACE_ORDER: `${prolongation}_PLACE_ORDER`,
  PLACE_ORDER_SUCCESS: `${prolongation}_PLACE_ORDER_SUCCESS`,
  PLACE_ORDER_ERROR: `${prolongation}_PLACE_ORDER_ERROR`,
  GET_LEAD: `${prolongation}_GET_LEAD`,
  GET_LEAD_SUCCESS: `${prolongation}_GET_LEAD_SUCCESS`,
  GET_LEAD_ERROR: `${prolongation}_GET_LEAD_ERROR`,
  GENERATE_TOKEN: `${prolongation}_GENERATE_TOKEN`,
  REFRESH_TOKEN: `${prolongation}_REFRESH_TOKEN`,
  GENERATE_TOKEN_SUCCESS: `${prolongation}_GENERATE_TOKEN_SUCCESS`,
  GENERATE_TOKEN_ERROR: `${prolongation}_GENERATE_TOKEN_ERROR`,
  SEND_MOENGAGE_EVENT: `${prolongation}_SEND_MOENGAGE_EVENT`,
  SEND_MOENGAGE_EVENT_SUCCESS: `${prolongation}_SEND_MOENGAGE_EVENT_SUCCESS`,
  SEND_MOENGAGE_EVENT_ERROR: `${prolongation}_SEND_MOENGAGE_EVENT_ERROR`,
  GET_RSA_LEAD_REQUESTED: `${prolongation}_GET_RSA_LEAD_REQUESTED`,
  GET_RSA_LEAD_SUCCESS: `${prolongation}_GET_RSA_LEAD_SUCCESS`,
  GET_RSA_LEAD_ERROR: `${prolongation}_GET_RSA_LEAD_ERROR`,
  ORDER_SUMMARY_OPEN_CLOSE_EVENT: `${prolongation}_ORDER_SUMMARY_OPEN_CLOSE_EVENT`,
  TOGGLE_TERMS_AND_CONDITION: `${prolongation}_TOGGLE_TERMS_AND_CONDITION`,
  SET_MARKETING_AGREEMENT: `${prolongation}_SET_MARKETING_AGREEMENT`,
  MAIN_AGREEMENT_TOGGLE: `${prolongation}_MAIN_AGREEMENT_TOGGLE`,
  INNER_AGREEMENT_TOGGLE: `${prolongation}_INNER_AGREEMENT_TOGGLE`,
  CALL_PROLONGATION_CHECKOUT_ACTION: `${prolongation}_CALL_PROLONGATION_CHECKOUT_ACTION`,
  GET_PROLONGATION_MARKETING_CONSENT: `${prolongation}_GET_PROLONGATION_MARKETING_CONSENT`,
  UPDATE_PROLONGATION_FORM_FIELD_STATUS: `${prolongation}_UPDATE_PROLONGATION_FORM_FIELD_STATUS`,
  CHECK_FORM_PROLONGATION_VALIDATION: `${prolongation}_CHECK_FORM_PROLONGATION_VALIDATION`,
  FORM_SUBMIT_ACTION: `${prolongation}_FORM_SUBMIT_ACTION`,
};
