export enum PRICES {
  MONTHLY = 'recurringFee',
  UPFRONT = 'upfrontPrice',
  BASE_PRICE = 'basePrice'
}

export enum NOTIFICATION_MEDIUM {
  EMAIL = 'email',
  PHONE = 'mobile'
}

export enum ENTITY_REFERRED_TYPE {
  PRODUCT_OFFERING = 'productOffering'
}

export enum MODAL_TYPE {
  CONFIRMATION_MODAL = 'CONFIRMATION_MODAL',
  SEND_NOTIFICATION_MODAL = 'SEND_NOTIFICATION_MODAL'
}

export const PAGE_NUMBER = {
  INITIAL: 1
};

/** USED ON PROLONGATION */
export const PRODUCT_ATTRIBUTE = {
  STORAGE: 'storage',
  COLOUR: 'colour',
  PLAN: 'plan',
  RAM: 'ram'
};
