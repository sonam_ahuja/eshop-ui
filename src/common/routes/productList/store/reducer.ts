import initialState from '@productList/store/state';
import * as productListApi from '@common/types/api/productList';
import {
  IChangedAttribute,
  IError,
  IFilter,
  IFilterCharacteristic,
  IGAEvent,
  IGetLead,
  IGetNotificationRequestTemplatePayload,
  ILastScrollPositionMap,
  INotificationData,
  IProductListState,
  IProlongationCheckoutResponse,
  IProlongationFormValidation,
  IProlongationProductListResponse,
  IQueryParams,
  IRSAKeyResponse,
  ISortBy,
  ISortList,
  ITariffParams,
  ITokenResponse,
  IVariant
} from '@productList/store/types';
import appKeys from '@src/common/constants/appkeys';
import { AnyAction, Reducer } from 'redux';
import CONSTANTS from '@productList/store/constants';
import withProduce from '@utils/withProduce';
import { MODAL_TYPE } from '@productList/store/enum';
import history from '@src/client/history';
import { PROLONGATION_EVENT_NAME } from '@src/common/oneAppRoutes/utils/enum';
import { transformAgreement } from '@src/common/oneAppRoutes/utils/transformMarketingAgreement';
import { IMegaMenu } from '@src/common/store/types/common';

import { getValueFromParams } from '../utils';

import { getCharacterstic } from './utils';
import {
  changeInnerConsent,
  getLeadTransform,
  marketingConsentOuterChange
} from './transformer';

interface ICharactersticMap {
  [filterId: string]: {
    // value of filter for all filters except price range where we have value
    [nameOfFilter: string]: IFilter;
  };
}

const getCharactersticMap = (
  filterCharacteristics: IFilterCharacteristic[]
) => {
  const charactersticMap: ICharactersticMap = {};
  // create a map for easy access to object
  // TODO: Add to a new function
  filterCharacteristics.forEach(chracterterstic => {
    charactersticMap[chracterterstic.id] = {};
    chracterterstic.data.forEach(filterData => {
      if (chracterterstic.id === appKeys.BFF_PRICE_RANGE_ID) {
        charactersticMap[chracterterstic.id][filterData.value] = filterData;
      } else {
        charactersticMap[chracterterstic.id][filterData.name] = filterData;
      }
    });
  });

  return charactersticMap;
};

const reducers: object = {
  [CONSTANTS.CLEAR_PRODUCT_LIST_DATA]: (state: IProductListState) => {
    state.data = [];
  },
  [CONSTANTS.FETCH_PRODUCT_LIST_SUCCESS]: (
    state: IProductListState,
    payload: productListApi.POST.IResponse
  ) => {
    const {
      data,
      itemsPerPage,
      pageNumber,
      resultCount,
      filterCharacteristics,
      characteristics,
      categoryName,
      plan,
      installmentEnabled,
      loyality,
      installment
    } = payload;
    Object.assign(state, {
      data,
      pagination: {
        currentPage: pageNumber,
        itemsPerPage,
        totalItems: resultCount
      },
      filterCharacteristics,
      loading: false,
      gridLoading: false,
      error: null,
      showAppShell: false,
      categoryName,
      characteristics,
      theme: getCharacterstic(characteristics, appKeys.PRODUCT_LIST_THEME),
      backgroundImage: getCharacterstic(
        characteristics,
        appKeys.PRODUCT_LIST_BACKGROUND_IMAGE
      ),
      defaultSortBy: getCharacterstic(characteristics, appKeys.DEFAULT_SORT),
      parentCategorySlug: getCharacterstic(
        characteristics,
        appKeys.CATEGORY_PARENT_SLUG
      ),
      plan,
      installmentEnabled,
      loyality,
      installment
    });
  },
  [CONSTANTS.FETCH_PRODUCT_LIST_ERROR]: (
    state: IProductListState,
    error: Error
  ) => {
    state.loading = false;
    state.gridLoading = false;
    state.showAppShell = false;
    if (error && error.message) {
      state.error = { message: '', code: 0 };
      state.error.message = error.message;
    }
  },
  [CONSTANTS.FETCH_PRODUCT_LIST_LOADING]: (
    state: IProductListState,
    gridLoading: boolean
  ) => {
    if (gridLoading) {
      state.gridLoading = true;
    } else {
      state.loading = true;
    }
    state.error = null;
  },
  [CONSTANTS.RECEIVED_DATA_FROM_SERVER]: (
    state: IProductListState,
    payload: { receivedProductListDataFromServer: boolean }
  ) => {
    state.receivedProductListDataFromServer =
      payload.receivedProductListDataFromServer;
  },
  [CONSTANTS.CHANGE_ITEM_PER_PAGE]: (
    state: IProductListState,
    payload: number
  ) => {
    state.pagination.itemsPerPage = payload;
  },
  [CONSTANTS.UPDATE_SHOW_APP_SHELL]: (
    state: IProductListState,
    payload: boolean
  ) => {
    state.showAppShell = payload;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION]: (
    state: IProductListState,
    payload: IGetNotificationRequestTemplatePayload
  ) => {
    state.notificationData.mediumValue = payload.mediumValue;
    state.notificationData.type = payload.type;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_SUCCESS]: (state: IProductListState) => {
    state.notificationModal = MODAL_TYPE.CONFIRMATION_MODAL;
    state.notificationLoading = false;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_ERROR]: (state: IProductListState) => {
    state.notificationLoading = false;
  },
  [CONSTANTS.SET_ERROR]: (state: IProductListState, error: IError) => {
    state.error = error;
  },
  [CONSTANTS.OPEN_MODAL]: (state: IProductListState, isModalOpen: boolean) => {
    state.openModal = isModalOpen;
    state.error = null;
    state.notificationModal = isModalOpen
      ? state.notificationModal
      : MODAL_TYPE.SEND_NOTIFICATION_MODAL;
  },
  [CONSTANTS.SET_NOTIFICATION_DATA]: (
    state: IProductListState,
    notificationData: INotificationData
  ) => {
    state.notificationData = notificationData;
  },
  [CONSTANTS.SET_FILTERS_AND_SORT_FROM_QUERY_PARAMS]: (
    state: IProductListState,
    payload: {
      queryParams: IQueryParams;
    }
  ) => {
    const { queryParams } = payload;
    state.selectedFilters = {};
    const paramsFilterIds = Object.keys(queryParams.filters);
    // create a charactersticMap for easy iteration
    const charactersticMap: ICharactersticMap = getCharactersticMap(
      state.filterCharacteristics
    );
    paramsFilterIds.forEach(filterId => {
      if (charactersticMap[filterId]) {
        state.selectedFilters[filterId] = [];
        queryParams.filters[filterId].forEach(valueOfFilter => {
          if (charactersticMap[filterId][valueOfFilter]) {
            state.selectedFilters[filterId].push(
              charactersticMap[filterId][valueOfFilter]
            );
          }
        });
      }
    });
  },
  [CONSTANTS.SET_SELECTED_FILTER]: (
    state: IProductListState,
    payload: {
      filterId: string;
      filter: IFilter;
    }
  ) => {
    const { filterId, filter } = payload;
    if (!state.selectedFilters[filterId]) {
      state.selectedFilters[filterId] = [];
      state.selectedFilters[filterId].push(filter);
    } else {
      const idx = state.selectedFilters[filterId].findIndex(
        selectedFilter => selectedFilter.value === filter.value
      );
      if (idx !== -1) {
        state.selectedFilters[filterId].splice(idx, 1);
      } else {
        state.selectedFilters[filterId].push(filter);
      }
    }
  },
  [CONSTANTS.CLEAR_ALL_SELECTED_FILTERS]: (state: IProductListState) => {
    state.selectedFilters = {};
  },
  [CONSTANTS.SET_SORT_BY]: (state: IProductListState, sortBy: ISortBy) => {
    state.sortBy = sortBy;
  },
  [CONSTANTS.SET_TEMP_FILTERS]: (state: IProductListState) => {
    state.tempSelectedFilters = state.selectedFilters;
    state.tempSortBy = state.sortBy;
    state.tempData = state.data;
    state.tempPagination = state.pagination;
    state.tempFilterCharacterstics = state.filterCharacteristics;
  },
  [CONSTANTS.SET_FILTERS_FROM_TEMP_FILTERS]: (state: IProductListState) => {
    state.selectedFilters = state.tempSelectedFilters;
    state.sortBy = state.tempSortBy;
    state.data = state.tempData;
    state.pagination = state.tempPagination;
    state.filterCharacteristics = state.tempFilterCharacterstics;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_LOADING]: (state: IProductListState) => {
    state.notificationLoading = true;
  },
  [CONSTANTS.RESET_DATA]: (state: IProductListState) => {
    state.data = [];
    state.tempData = [];
    state.selectedFilters = {};
    state.tempSelectedFilters = {};
    state.sortBy = null;
    state.tempSortBy = null;
  },
  [CONSTANTS.SET_SCROLL_POSITION]: (
    state: IProductListState,
    payload: {
      urlPath: string;
      position: number;
    }
  ) => {
    const { urlPath, position } = payload;
    state.lastScrollPositionMap = {
      ...state.lastScrollPositionMap,
      [urlPath]: position
    };
  },
  [CONSTANTS.SET_LAST_PAGE_NUMBER]: (
    state: IProductListState,
    payload: string
  ) => {
    state.lastListURL = payload;
  },
  [CONSTANTS.SET_SORT_BY_LIST]: (
    state: IProductListState,
    payload: { sortByList: ISortList[]; sortBy: ISortList | null }
  ) => {
    state.sortListing = payload.sortByList;
    state.sortBy = payload.sortBy;
  },
  [CONSTANTS.CLEAR_SCROLL_POSITION]: (
    state: IProductListState,
    payload: {
      urlPath: string;
    }
  ) => {
    const { urlPath } = payload;
    const newMap: ILastScrollPositionMap = {};
    Object.keys(state.lastScrollPositionMap).forEach(key => {
      if (key !== urlPath) {
        newMap[key] = state.lastScrollPositionMap[key];
      }
    });
    state.lastScrollPositionMap = newMap;
    // const { [urlPath], ...rest} = state.lastScrollPositionMap;
    // state.lastScrollPositionMap = {
    //   ...rest
    // };
  },
  [CONSTANTS.SET_POPULAR_DEVICE]: (
    state: IProductListState,
    payload: IVariant[]
  ) => {
    state.popularDevices = payload;
  },
  [CONSTANTS.SET_TARIFF_LOYALTY]: (
    state: IProductListState,
    payload: ITariffParams
  ) => {
    const { tariffId, loyalty, discountId } = payload;
    state.tariffId = tariffId;
    state.loyalty = loyalty;
    state.discountId = discountId;
  },
  [CONSTANTS.SET_PROLONGATION_PRODUCT_LIST_DATA]: (
    state: IProductListState,
    payload: IProlongationProductListResponse
  ) => {
    state.categoryName = payload.category;
    state.prolongationData = payload.data;
  },
  [CONSTANTS.SET_SELECTED_VARIANT]: (
    state: IProductListState,
    variantId: string
  ) => {
    state.prolongationSelectedVariant = variantId;
  },
  [CONSTANTS.SET_SELECTED_PRODUCT]: (
    state: IProductListState,
    productId: string
  ) => {
    state.prolongationSelectedProduct = productId;
  },
  [CONSTANTS.SET_UUID]: (state: IProductListState, uuid: string) => {
    state.uuid = uuid;
  },
  [CONSTANTS.SET_PROFILE_ID]: (state: IProductListState, profileId: string) => {
    state.profileId = profileId;
  },
  [CONSTANTS.FETCH_PROLONGATION_PRODUCT_LIST]: (state: IProductListState) => {
    state.showAppShell = true;
  },
  [CONSTANTS.FETCH_PROLONGATION_PRODUCT_DETAILS]: (
    state: IProductListState
  ) => {
    state.showAppShell = true;
  },
  [CONSTANTS.FETCH_PROLONGATION_CHECKOUT_DETAILS]: (
    state: IProductListState,
    payload: boolean
  ) => {
    state.showAppShell = payload;
  },
  [CONSTANTS.FETCH_PROLONGATION_CHECKOUT_DETAILS_SUCCESS]: (
    state: IProductListState
  ) => {
    state.showAppShell = false;
  },
  [CONSTANTS.SET_MARKETING_AGREEMENT]: (
    state: IProductListState,
    payload: IMegaMenu
  ) => {
    state.prolongationCheckoutDetails.marketingConsentAgreement = [];
    state.prolongationCheckoutDetails.marketingConsentAgreement = transformAgreement(
      payload
    );
  },
  [CONSTANTS.SET_PROLONGATION_CHECKOUT_DETAILS]: (
    state: IProductListState,
    payload: IProlongationCheckoutResponse
  ) => {
    state.prolongationCheckoutDetails.data = payload.data;
  },
  [CONSTANTS.UPDATE_PROLONGATION_FORM_FIELD]: (
    state: IProductListState,
    payload: { field: string; value: string }
  ) => {
    state.prolongationCheckoutDetails.userInfo[payload.field] = payload.value;
  },
  [CONSTANTS.UPDATE_PROLONGATION_FORM_FIELD_STATUS]: (
    state: IProductListState,
    payload: { field: string; value: boolean }
  ) => {
    state.prolongationCheckoutDetails.formValidation[`${payload.field}Valid`] =
      payload.value;
  },
  [CONSTANTS.PLACE_ORDER]: (state: IProductListState) => {
    state.loading = true;
  },
  [CONSTANTS.PLACE_ORDER_SUCCESS]: (state: IProductListState) => {
    state.loading = false;
  },
  [CONSTANTS.PLACE_ORDER_ERROR]: (state: IProductListState) => {
    state.loading = false;
  },
  [CONSTANTS.SET_SELECTED_ATTR_FOR_PROLONGATION]: (
    state: IProductListState,
    payload: IChangedAttribute[]
  ) => {
    state.selectedAttributes = payload;
  },
  [CONSTANTS.SET_PROLONGATION_OPENED_FROM]: (
    state: IProductListState,
    payload: string
  ) => {
    state.prolongationOpenedFrom = payload;
  },
  [CONSTANTS.GET_LEAD]: (state: IProductListState) => {
    state.showAppShell = true;
  },
  [CONSTANTS.GET_LEAD_ERROR]: (state: IProductListState) => {
    state.showAppShell = false;
    state.prolongationCheckoutDetails.userDataReceived = false;
  },
  [CONSTANTS.GET_LEAD_SUCCESS]: (
    state: IProductListState,
    payload: IGetLead
  ) => {
    state.showAppShell = false;
    state.prolongationCheckoutDetails.userDataReceived = true;
    getLeadTransform(state.prolongationCheckoutDetails.userInfo, payload);
    if (history) {
      const segmentId = getValueFromParams(
        history.location.search,
        'segmentId'
      );
      const categoryId = getValueFromParams(
        history.location.search,
        'categoryId'
      );

      state.prolongationCheckoutDetails.userInfo.segmentId = segmentId as string;
      state.prolongationCheckoutDetails.userInfo.categoryId = categoryId as string;
    }
  },
  [CONSTANTS.GENERATE_TOKEN_SUCCESS]: (
    state: IProductListState,
    payload: ITokenResponse
  ) => {
    state.accessToken = payload.access_token;
    state.refreshToken = payload.refresh_token;
  },
  [CONSTANTS.GET_RSA_LEAD_SUCCESS]: (
    state: IProductListState,
    payload: IRSAKeyResponse
  ) => {
    state.prolongationRSAKey = payload.key;
    state.prolongationRSALabel = payload.label;
  },
  [CONSTANTS.SEND_MOENGAGE_EVENT]: (
    state: IProductListState,
    payload: IGAEvent
  ) => {
    state.moengageEventTemp = payload;
  },
  [CONSTANTS.CHECK_FORM_PROLONGATION_VALIDATION]: (
    state: IProductListState,
    payload: IProlongationFormValidation
  ) => {
    state.prolongationCheckoutDetails.formValidation = payload;
  },
  [CONSTANTS.FORM_SUBMIT_ACTION]: (
    state: IProductListState,
    payload: boolean
  ) => {
    state.prolongationCheckoutDetails.isFormSubmitted = payload;
  },
  [CONSTANTS.SEND_MOENGAGE_EVENT_SUCCESS]: (state: IProductListState) => {
    state.moengageEventTemp = {
      hitType: '',
      eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
      eventAction: '',
      eventLabel: '',
      eventValue: 1,
      page: '',
      url: '',
      opened_from: '',
      device_id: '',
      device_name: '',
      device_amt_upfront: '',
      device_amt_monthly: '',
      amt: '',
      currencyCode: '',
      currencySymbol: ''
    };
  },
  [CONSTANTS.TOGGLE_TERMS_AND_CONDITION]: (state: IProductListState) => {
    state.prolongationCheckoutDetails.agreeOnTermsAndCondition = !state
      .prolongationCheckoutDetails.agreeOnTermsAndCondition;
  },
  [CONSTANTS.MAIN_AGREEMENT_TOGGLE]: (
    state: IProductListState,
    payload: number
  ) => {
    state.prolongationCheckoutDetails.marketingConsentAgreement[
      payload
    ].isSelected = !state.prolongationCheckoutDetails.marketingConsentAgreement[
      payload
    ].isSelected;

    marketingConsentOuterChange(
      state.prolongationCheckoutDetails.marketingConsentAgreement[payload]
        .subConsent,
      state.prolongationCheckoutDetails.marketingConsentAgreement[payload]
        .isSelected
    );
  },
  [CONSTANTS.INNER_AGREEMENT_TOGGLE]: (
    state: IProductListState,
    payload: { outerIndex: number; innerIndex: number }
  ) => {
    const subConsents =
      state.prolongationCheckoutDetails.marketingConsentAgreement[
        payload.outerIndex
      ].subConsent;
    if (subConsents) {
      subConsents[payload.innerIndex].isSelected = !subConsents[
        payload.innerIndex
      ].isSelected;

      changeInnerConsent(
        state.prolongationCheckoutDetails.marketingConsentAgreement[
          payload.outerIndex
        ]
      );
    }
  }
};

export default withProduce(initialState, reducers) as Reducer<
  IProductListState,
  AnyAction
  // tslint:disable-next-line: max-file-line-count
>;
