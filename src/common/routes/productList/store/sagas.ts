// tslint:disable
import { Effect, SagaIterator } from 'redux-saga';
import CONSTANTS from '@productList/store/constants';
import axios from 'axios';
import * as productListApi from '@common/types/api/productList';
import {
  fetchPopularDeviceService,
  fetchProductListService,
  fetchProlongationProductListService,
  fetchProlongationCheckoutDetailsService,
  getLeadService,
  IFiltersRequest,
  sendStockNotificationService,
  placeProlongationOrderService,
  generateTokenService,
  sendMoengageEventService,
  refreshTokenService,
  getProlongationLeadKeyService,
  fetchProlongationConsentService
} from '@productList/store/services';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  IProlongationConfig,
  ICatalogConfiguration
} from '@src/common/store/types/configuration';
import {
  ICharacteristics,
  IFetchProductListParams,
  IGetNotificationRequestTemplatePayload,
  ILoadMoreProductListParams,
  IProductListState,
  IProlongationProductListResponse,
  IProlongationCheckoutResponse,
  IQueryParams,
  ISortBy,
  ISortList,
  IProlongationProductDetailedPayload,
  IChangedAttribute,
  IFetchProlongationProductListPayload,
  IGAEvent,
  IMoengageEvent,
  IProlongationCheckoutRequestPayload
} from '@productList/store/types';
import actions from '@productList/store/actions';
import { IProductListTranslation } from '@src/common/store/types/translation';
import {
  getDefaultSort,
  getSortList,
  getSortProductList,
  getValueFromParams,
  addPriceCurrency
} from '@productList/utils';
import { isBrowser, logError, logInfo } from '@common/utils';
import { getOutOfStockReason } from '@productList/utils/getOutOfStockReason';
import APP_CONSTANTS, { ERROR_CODE_BFF } from '@common/constants/appConstants';
import { sendProductImpressionEvent } from '@events/DeviceList/index';
import ROUTES from '@common/constants/routes';
import { getProlongaitionProductDetailsRequestTemplate } from '@productList/utils/requestTemplate';
import history from '@src/client/history';
import { createQueryParams } from '@productDetailed/utils';
import {
  sendForLandingPage,
  sendForProductDetailPage,
  sendForProductDetailOptionChange,
  sendForPersonalDetailPage,
  sendForPersonalDetailProductSelected,
  sendForPersonalDetailSuccess,
  sendForOrderSummaryOpen,
  sendForOrderSummaryClosed,
  sendForApiError
} from '@src/common/oneAppRoutes/utils/googleTracking';
import { prolongationEncryption } from '@common/utils/encryptions';
import store from '@src/common/store';
import {
  getConfiguration,
  getFilterParamsFromQueryOrState,
  getSortByFromSortList,
  getState,
  getTranslation,
  getProductVariantIDFromURLForProlongationCheckout,
  getProlongationCmsConfiguration,
  getProlongationOpenedFrom,
  getCatalogConfiguration
} from './utils';
import {
  getPopularDevice,
  transformProlongationProductData,
  payloadForPostUserInfotLead,
  transformMoengageAttribute
} from './transformer';
import { PRODUCT_ATTRIBUTE } from './enum';
import { getAttributesFromCharacteristics } from '../../productDetailed/store/utils';
import { PRICE_TYPE } from '../../productDetailed/store/enum';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { IWindow } from '@src/client';
import { encodeRFC5987ValueChars } from '@src/common/utils/encodeURL';
import { getPriceAndCurrency } from '@src/common/routes/tariff/store/utils';
import { getCurrencyCode } from '@store/common/index';

import { getUpfrontPrice } from '@src/common/oneAppRoutes/productList/product-card';
import appConstants from '@common/constants/appConstants';
import { apiEndpoints } from '@src/common/constants';
import { isFormFieldValidAsPerType } from '@src/common/oneAppRoutes/utils/personalDetailForm';

export function* loadMoreSetPreviousPageData(
  payload: ILoadMoreProductListParams
): SagaIterator {
  const { pageNumber } = payload;
  const state: IProductListState = yield select(getState);
  const {
    data,
    filterCharacteristics,
    pagination: { itemsPerPage },
    plan
  } = state;
  const newData = data.slice(0, itemsPerPage * pageNumber);

  const newState = {
    data: newData,
    itemsPerPage,
    pageNumber,
    resultCount: state.pagination.totalItems,
    filterCharacteristics,
    characteristics: state.characteristics,
    categoryName: state.categoryName,
    plan,
    installmentEnabled: state.installmentEnabled,
    loyality: state.loyality,
    installment: state.installment
  };
  yield put(actions.fetchProductListSuccess(newState));
  if(payload.successCallback){
    payload.successCallback();
  }
  if (isBrowser) {
    window.scrollTo({
      top: 0
    });
  }
}

export function* setSortingListAndSortBy(payload: {
  characteristics: ICharacteristics[];
  queryParams?: IQueryParams;
  sortListing: ISortList[];
  sortBy: ISortBy | null;
}): SagaIterator {
  const {
    characteristics,
    queryParams,
    sortListing: stateSortListing,
    sortBy: stateSortBy
  } = payload;
  const translation: IProductListTranslation = yield select(getTranslation);
  const sortByList = getSortList(characteristics, translation);
  const sortBy = getDefaultSort(characteristics, translation);
  if (queryParams && queryParams.sortBy) {
    yield put(
      actions.setSortByList({
        sortByList,
        sortBy: getSortByFromSortList(sortByList, queryParams.sortBy)
      })
    );
  } else if (stateSortListing.length === 0 || !stateSortBy) {
    yield put(actions.setSortByList({ sortByList, sortBy }));
  }
}

export function* setFiltersFromQueryParams(
  queryParams?: IQueryParams
): SagaIterator {
  if (queryParams) {
    yield put(
      // map filters & sortBy from queryParams with values from filterCharacterstics
      actions.setFiltersAndSortFromQueryParams({
        queryParams
      })
    );
  }
}

export function* scrollPage(
  scrollAmount?: number,
  defaultScroll?: number
): SagaIterator {
  if (isBrowser) {
    if (scrollAmount !== undefined) {
      window.scrollTo({
        top: scrollAmount
      });
    } else if (defaultScroll !== undefined) {
      window.scrollTo({
        top: defaultScroll
      });
    }
  }
}

export function* loadMoreProductList(action: {
  type: string;
  payload: ILoadMoreProductListParams;
}): SagaIterator {
  const {
    payload: { categoryId, isChanged, scrollAmount, queryParams, pageNumber, successCallback, showFullPageError }
  } = action;

  if (pageNumber && pageNumber > 1) {
    yield put(actions.fetchProductListLoading(true));
  } else {
    yield put(actions.fetchProductListLoading(false));
  }

  const state: IProductListState = yield select(getState);
  const {
    selectedFilters,
    data,
    pagination: { currentPage, itemsPerPage },
    sortBy,
    sortListing,
    tariffId,
    loyalty,
    discountId
  } = state;

  // ischanged means if filters are updated !
  if (!isChanged && currentPage >= pageNumber) {
    yield call(loadMoreSetPreviousPageData, action.payload);

    return;
  }

  const { sortOutStock } = yield select(getConfiguration);
  // currentPage is number so no need to add + 1
  const apiParams = {
    filters: [] as IFiltersRequest[],
    categoryId,
    itemsPerPage: isChanged ? itemsPerPage * pageNumber : itemsPerPage,
    currentPage: isChanged ? 0 : currentPage,
    sortBy: queryParams && queryParams.sortBy ? queryParams.sortBy : '',
    tariffId,
    loyalty,
    discountId,
    showFullPageError
  };
  // due to mobile check get from store if mobile
  const { paramFilters, paramSortBy } = getFilterParamsFromQueryOrState(
    sortBy,
    selectedFilters,
    queryParams
  );
  apiParams.filters = paramFilters;
  apiParams.sortBy = paramSortBy;

  try {
    const responseData: productListApi.POST.IResponse = yield call<
      typeof apiParams
    >(fetchProductListService, apiParams);
    sendProductImpressionEvent(responseData, categoryId);

    let updatedData: productListApi.POST.IResponse['data'] = isChanged
      ? responseData.data
      : [...data, ...responseData.data];

    if (sortOutStock) {
      const {
        preorder: { considerInventory }
      }: ICatalogConfiguration = yield select(getCatalogConfiguration);
      updatedData = getSortProductList(updatedData, considerInventory);
    }

    const updatedResponse = {
      ...responseData,
      filterCharacteristics: addPriceCurrency(
        responseData.filterCharacteristics
      ),
      data: updatedData,
      pageNumber: Math.ceil(updatedData.length / itemsPerPage),
      itemsPerPage,
      plan: responseData.plan,
      installmentEnabled: responseData.installmentEnabled,
      loyality: responseData.loyality,
      installment: responseData.installment
    };
    yield put(actions.fetchProductListSuccess(updatedResponse));
    yield call(scrollPage, scrollAmount);
    yield call(setSortingListAndSortBy, {
      characteristics: responseData.characteristics,
      queryParams,
      sortListing,
      sortBy
    });
    yield call(setFiltersFromQueryParams, queryParams);
    if(successCallback){
      successCallback();
    }
  } catch (error) {
    yield put(actions.fetchProductListError(error));
  }
}

export function* fetchProductList(action: {
  type: string;
  payload: IFetchProductListParams;
}): SagaIterator {
  const state: IProductListState = yield select(getState);

  const {
    selectedFilters,
    sortBy,
    pagination: { itemsPerPage },
    sortListing,
    tariffId,
    loyalty,
    discountId
  } = state;

  const {
    payload: {
      currentPage,
      categoryId,
      queryParams,
      scrollAmount,
      showFullPageError,
      signal,
      successCallback
    }
  } = action;

  const { sortOutStock } = yield select(getConfiguration);

  if (queryParams) {
    yield put(actions.fetchProductListLoading(false));
  } else {
    yield put(actions.fetchProductListLoading(true));
  }
  const apiParams = {
    // as pageNumber here is number instead of index, so subtract 1
    currentPage: currentPage - 1,
    itemsPerPage,
    categoryId,
    filters: [] as IFiltersRequest[],
    sortBy: queryParams && queryParams.sortBy ? queryParams.sortBy : '',
    tariffId,
    loyalty,
    discountId,
    showFullPageError,
    signal
  };

  const { paramFilters, paramSortBy } = getFilterParamsFromQueryOrState(
    sortBy,
    selectedFilters,
    queryParams
  );

  apiParams.filters = paramFilters;
  apiParams.sortBy = paramSortBy;

  try {
    const responseData: productListApi.POST.IResponse = yield call<
      typeof apiParams
    >(fetchProductListService, apiParams);
    if (queryParams && queryParams.filters) {
      sendProductImpressionEvent(responseData, categoryId);
    }
    if (sortOutStock) {
      const {
        preorder: { considerInventory }
      }: ICatalogConfiguration = yield select(getCatalogConfiguration);
      responseData.data = getSortProductList(
        responseData.data,
        considerInventory
      );
    }

    yield put(
      actions.fetchProductListSuccess({
        ...responseData,
        filterCharacteristics: addPriceCurrency(
          responseData.filterCharacteristics
        ),
        pageNumber: currentPage,
        itemsPerPage,
        plan: responseData.plan,
        installmentEnabled: responseData.installmentEnabled,
        loyality: responseData.loyality,
        installment: responseData.installment
      })
    );
    yield call(scrollPage, scrollAmount, 0);
    yield call(setSortingListAndSortBy, {
      characteristics: responseData.characteristics,
      queryParams,
      sortListing,
      sortBy
    });
    yield call(setFiltersFromQueryParams, queryParams);
    if(successCallback){
      successCallback();
    }
  } catch (error) {
    if (axios.isCancel(error)) {
    }
    yield put(actions.fetchProductListError(error));
  }
}

export function* sendStockNotification(action: {
  type: string;
  payload: IGetNotificationRequestTemplatePayload;
}): IterableIterator<Effect | Effect[] | Promise<void>> {
  const { payload } = action;
  yield put(actions.sendStockNotificationLoading());
  const state: IProductListState = yield select(getState);
  const { data } = state;

  const reason = getOutOfStockReason(payload.variantId, data);

  try {
    yield sendStockNotificationService(payload, reason);
    yield put(actions.sendStockNotificationSuccess());
  } catch (error) {
    yield put(actions.sendStockNotificationError(error));
  }
}

export function* fetchPopularDevice(action: {
  type: string;
  payload: string;
}): SagaIterator {
  const { payload } = action;
  const state: IProductListState = yield select(getState);
  const { tariffId, loyalty, discountId } = state;
  const products: productListApi.POST.IResponse = yield call(
    fetchPopularDeviceService,
    { categoryId: payload, tariffId, loyalty, discountId }
  );
  sendProductImpressionEvent(products, payload);
  // TODO: LOADING AND ERROR handling
  const popularDevices = getPopularDevice(products);

  yield put(actions.setPopularDevice(popularDevices));
}

/** PROLONGATION FUNCTION */
export function* fetchProlongationProductList(action: {
  type: string;
  payload: IFetchProlongationProductListPayload;
}): SagaIterator {
  const { payload } = action;
  try {
    const response: IProlongationProductListResponse = yield call(
      fetchProlongationProductListService,
      payload
    );

    if (
      response &&
      response.code &&
      response.code === 'HISTORICAL_LEAD_FOUND' &&
      history
    ) {
      localStorage.setItem('leadExist', 'yes');
      history.push(ROUTES.ORDER_CONFIRMATION);
      return;
    }

    localStorage.removeItem('leadExist');
    const transformedResp = transformProlongationProductData(response);
    yield put(actions.setProlongationProductList(transformedResp));
    /*** GA CODE */
    const openedFrom: string = yield select(getProlongationOpenedFrom);
    const configuration: IProlongationConfig = yield select(
      getProlongationCmsConfiguration
    );

    if (transformedResp.data && transformedResp.data.length > 0) {
      const variantWithImage = transformedResp.data.find(variant => {
        return (
          variant.attachments &&
          Array.isArray(variant.attachments.thumbnail) &&
          variant.attachments.thumbnail.length > 0
        );
      });
      if (
        variantWithImage &&
        transformedResp.data &&
        transformedResp.data.length > 1 &&
        Array.isArray(transformedResp.data[1].prices)
      ) {
        const upfrontAmt = transformedResp.data[1].prices.find(price => {
          return price && price.priceType === PRICE_TYPE.UPFRONT_PRICE;
        });
        const monthlyAmt = transformedResp.data[1].prices.find(price => {
          return price && price.priceType === PRICE_TYPE.RECURRING_FEE;
        });
        const variantOptionValues = transformedResp.data[1].optionValues;
        const variantName = transformedResp.data[1].name;
        const actualUpfrontPrice = getUpfrontPrice(
          transformedResp.data[1].prices
        );
        const currencySymbol = getPriceAndCurrency(
          actualUpfrontPrice,
          getCurrencyCode()
        ).discountCurrency;
        const currencyCode = getCurrencyCode();

        sendForLandingPage({
          label: `${transformedResp.data[1].variantId}_${variantName}_${
            variantOptionValues[1]
          }_${variantOptionValues[0]}_${variantOptionValues[2]}`,
          deviceId: transformedResp.data[1].variantId,
          deviceName: transformedResp.data[1].name,
          deviceUpfrontAmt: upfrontAmt
            ? String(upfrontAmt.discountedValue)
            : '',
          deviceMonthlyAmt: monthlyAmt
            ? String(monthlyAmt.discountedValue)
            : '',
          openedFrom,
          pageUrl: encodeRFC5987ValueChars(window.location.href),
          gaCode: configuration.gaCode,
          currencySymbol,
          currencyCode
        });
      }
    }

    /*** GA CODE */

    yield put(actions.fetchProlongationCheckoutDetailsSuccess());
  } catch (error) {
    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* fetchProlongationCheckoutDetails(): SagaIterator {
  try {
    if (history) {
      const marketingConsentArr = store.getState().productList
        .prolongationCheckoutDetails.marketingConsentAgreement;

      const uuid = getValueFromParams(history.location.search, 'uuid');
      const profileId = getValueFromParams(
        history.location.search,
        'profileId'
      );
      const {
        categorySlug,
        variantId
      } = getProductVariantIDFromURLForProlongationCheckout(
        history.location.pathname
      );
      const payload: IProlongationCheckoutRequestPayload = {
        uuid,
        profileId,
        channel: APP_CONSTANTS.CHANNEL,
        category: categorySlug,
        variantId,
        requestedAttributes: [
          PRODUCT_ATTRIBUTE.STORAGE,
          PRODUCT_ATTRIBUTE.COLOUR,
          PRODUCT_ATTRIBUTE.PLAN,
          PRODUCT_ATTRIBUTE.RAM
        ],
        marketingDiscount:
          marketingConsentArr &&
          marketingConsentArr[0] &&
          marketingConsentArr[0].isSelected
      };
      const response: IProlongationCheckoutResponse = yield call(
        fetchProlongationCheckoutDetailsService,
        payload
      );
      yield put(actions.setProlongationCheckoutDetails(response));

      /*** GA */
      const configuration: IProlongationConfig = yield select(
        getProlongationCmsConfiguration
      );

      const openedFrom: string = yield select(getProlongationOpenedFrom);
      if (
        response &&
        response.data &&
        response.data.length > 0 &&
        Array.isArray(response.data[0].prices) &&
        response.data[0].prices.length
      ) {
        const upfrontAmt = response.data[0].prices.find(price => {
          return price.priceType === PRICE_TYPE.UPFRONT_PRICE;
        });
        const monthlyAmt = response.data[0].prices.find(price => {
          return price.priceType === PRICE_TYPE.RECURRING_FEE;
        });
        const variantOptionValues = response.data[0].optionValues;
        if (Array.isArray(variantOptionValues)) {
          const variantName = response.data[0].name;

          const actualUpfrontPrice = getUpfrontPrice(response.data[0].prices);
          const currencySymbol = getPriceAndCurrency(
            actualUpfrontPrice,
            getCurrencyCode()
          ).discountCurrency;
          const currencyCode = getCurrencyCode();

          sendForPersonalDetailPage({
            label: `${variantId}_${variantName}_${variantOptionValues[1]}_${
              variantOptionValues[0]
            }_${variantOptionValues[2]}`,
            deviceId: variantId,
            deviceName: variantName,
            deviceUpfrontAmt: upfrontAmt
              ? String(upfrontAmt.discountedValue)
              : '',
            deviceMonthlyAmt: monthlyAmt
              ? String(monthlyAmt.discountedValue)
              : '',
            openedFrom,
            pageUrl: encodeRFC5987ValueChars(window.location.href),
            gaCode: configuration.gaCode,
            currencySymbol,
            currencyCode
          });
        }
      }
      /*** GA */
      yield put(actions.fetchProlongationCheckoutDetailsSuccess());
    }
  } catch (error) {
    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* fetchProlongationProductDetails(action: {
  type: string;
  payload: IProlongationProductDetailedPayload;
}): SagaIterator {
  try {
    if (history) {
      const {
        payload: { changedAttribute, productId, category, variantId }
      } = action;
      const uuid = getValueFromParams(history.location.search, 'uuid');
      const segment = getValueFromParams(history.location.search, 'segmentId');
      const profileId = getValueFromParams(
        history.location.search,
        'profileId'
      );
      const state: IProductListState = yield select(getState);
      const { selectedAttributes } = state;
      let payloadSelectedAttributes: IChangedAttribute[] = selectedAttributes;
      if (
        changedAttribute &&
        changedAttribute.name &&
        changedAttribute.name.length &&
        Array.isArray(selectedAttributes)
      ) {
        payloadSelectedAttributes = selectedAttributes.filter(
          selectedAttribute => selectedAttribute.name !== changedAttribute.name
        );
      }
      if (uuid) {
        const requestBody = getProlongaitionProductDetailsRequestTemplate(
          uuid,
          productId,
          category,
          segment as string,
          variantId,
          changedAttribute &&
            changedAttribute.name &&
            changedAttribute.name.length
            ? [changedAttribute]
            : [],
          payloadSelectedAttributes
        );
        requestBody.checkHistoricalLead = false; // check for lead already exist
        requestBody.profileId = profileId as string; // profile id appended
        const response: IProlongationProductListResponse = yield call(
          fetchProlongationProductListService,
          requestBody
        );
        let updatedSelectedAttributes: IChangedAttribute[] = payloadSelectedAttributes;
        if (response.data.length > 0) {
          updatedSelectedAttributes = getAttributesFromCharacteristics(
            response.data[0].characteristics,
            [
              PRODUCT_ATTRIBUTE.STORAGE,
              PRODUCT_ATTRIBUTE.COLOUR,
              PRODUCT_ATTRIBUTE.PLAN,
              PRODUCT_ATTRIBUTE.RAM
            ]
          );

          yield put(
            actions.setProlongationProductList(
              transformProlongationProductData(response)
            )
          );

          yield put(
            actions.setSelectedAttrForProlongation(updatedSelectedAttributes)
          );

          const {
            name: variantName,
            optionValues: variantOptionValues,
            variantId: id
          } = response.data[0];
          let queryParam = '';
          if (changedAttribute) {
            queryParam += `${encodeURIComponent(
              `${createQueryParams(changedAttribute)}`
            )}`;
          }

          const parsedQuery = parseQueryString(history.location.search);

          const updatedPathName = `/${
            response.category
          }/${variantName}/${productId}/${id}`;
          const updatedSearchQuery = `${queryParam}&categoryId=${
            parsedQuery.categoryId
          }`;
          if (
            history.location.pathname + history.location.search !==
            updatedPathName + updatedSearchQuery
          ) {
            history.replace({
              pathname: updatedPathName,
              search: `${queryParam}&categoryId=${parsedQuery.categoryId}`
            });
          }

          /*** GA */
          const configuration: IProlongationConfig = yield select(
            getProlongationCmsConfiguration
          );
          const openedFrom: string = yield select(getProlongationOpenedFrom);
          if (Array.isArray(response.data[0].prices)) {
            const upfrontAmt = response.data[0].prices.find(price => {
              return price.priceType === PRICE_TYPE.UPFRONT_PRICE;
            });
            const monthlyAmt = response.data[0].prices.find(price => {
              return price.priceType === PRICE_TYPE.RECURRING_FEE;
            });

            const actualUpfrontPrice = getUpfrontPrice(response.data[0].prices);
            const currencySymbol = getPriceAndCurrency(
              actualUpfrontPrice,
              getCurrencyCode()
            ).discountCurrency;
            const currencyCode = getCurrencyCode();

            const ga_payload = {
              label: `${id}_${variantName}_${variantOptionValues[1]}_${
                variantOptionValues[0]
              }_${variantOptionValues[2]}`,
              deviceId: id,
              deviceName: variantName,
              deviceUpfrontAmt: upfrontAmt
                ? String(upfrontAmt.discountedValue)
                : '',
              deviceMonthlyAmt: monthlyAmt
                ? String(monthlyAmt.discountedValue)
                : '',
              openedFrom,
              pageUrl: encodeRFC5987ValueChars(window.location.href),
              gaCode: configuration.gaCode,
              currencySymbol,
              currencyCode
            };
            if (
              changedAttribute &&
              changedAttribute.name &&
              changedAttribute.name.length
            ) {
              sendForProductDetailOptionChange(ga_payload);
            } else {
              sendForProductDetailPage(ga_payload);
            }
          }
        }
        /*** GA */

        yield put(actions.fetchProlongationCheckoutDetailsSuccess());
      }
    }
  } catch (error) {
    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* getLeadRequested(): Generator {
  try {
    if (history) {
      const profileId = getValueFromParams(
        history.location.search,
        'profileId'
      );
      const profileDataReceived = store.getState().productList
        .prolongationCheckoutDetails.userDataReceived;
      const {
        isProfilePrefilled
      } = store.getState().configuration.cms_configuration.global.prolongation;

      const bearerToken = store.getState().common.prolongation
        .authorizationToken;
      if (isProfilePrefilled && !profileDataReceived) {
        const response = yield getLeadService({
          sub: profileId as string,
          bearerToken: bearerToken as string
        });
        if (response && response.length) {
          yield put(actions.getLeadSuccess(response[0]));
        }
      }
    }
  } catch (error) {
    const timeOutSeconds = store.getState().configuration.cms_configuration
      .modules.prolongation.webviewCloseTime;
    logError(error);
    if (error.status === 401 && (window as IWindow).OneApp) {
      setTimeout(() => {
        (window as IWindow).OneApp.closeOneAppWebview();
      }, timeOutSeconds);
    }
    yield put(actions.getLeadError());
  }
}

export function* orderSummaryOpenCloseEvent(action: {
  payload: boolean;
  type: string;
}): Generator {
  try {
    if (history) {
      const state: IProductListState = yield select(getState);
      /** GA */
      const configuration: IProlongationConfig = yield select(
        getProlongationCmsConfiguration
      );
      const openedFrom: string = yield select(getProlongationOpenedFrom);
      if (
        state.prolongationCheckoutDetails.data.length > 0 &&
        Array.isArray(state.prolongationCheckoutDetails.data[0].prices)
      ) {
        const variant = state.prolongationCheckoutDetails.data[0];
        const upfrontAmt = variant.prices.find(price => {
          return price.priceType === PRICE_TYPE.UPFRONT_PRICE;
        });
        const monthlyAmt = variant.prices.find(price => {
          return price.priceType === PRICE_TYPE.RECURRING_FEE;
        });
        const variantId = history.location.pathname.split('/')[4];
        if (Array.isArray(variant.optionValues)) {
          const actualUpfrontPrice = getUpfrontPrice(variant.prices);
          const currencySymbol = getPriceAndCurrency(
            actualUpfrontPrice,
            getCurrencyCode()
          ).discountCurrency;
          const currencyCode = getCurrencyCode();

          const eventPayload = {
            label: `${variantId}_${variant.name}_${variant.optionValues[1]}_${
              variant.optionValues[0]
            }_${variant.optionValues[2]}`,
            deviceId: variantId,
            deviceName: variant.name,
            deviceUpfrontAmt: upfrontAmt
              ? String(upfrontAmt.discountedValue)
              : '',
            deviceMonthlyAmt: monthlyAmt
              ? String(monthlyAmt.discountedValue)
              : '',
            openedFrom,
            pageUrl: encodeRFC5987ValueChars(window.location.href),
            gaCode: configuration.gaCode,
            currencySymbol,
            currencyCode
          };

          action.payload
            ? sendForOrderSummaryOpen(eventPayload)
            : sendForOrderSummaryClosed(eventPayload);
        }
        /** GA */
      }
    }
  } catch (error) {
    logError(error);
  }
}
/** PROLONGATION FUNCTION */
export function* placeOrder(): Generator {
  try {
    if (history) {
      // const uuid = getValueFromParams(history.location.search, 'uuid');
      const state: IProductListState = yield select(getState);
      const uuid = getValueFromParams(history.location.search, 'uuid');
      const segment = getValueFromParams(history.location.search, 'segmentId');
      const profileId = getValueFromParams(
        history.location.search,
        'profileId'
      );
      const parsedQuery = parseQueryString(history.location.search);
      const {
        applyConsentDiscount
      } = store.getState().configuration.cms_configuration.global.prolongation;

      /** GA */
      const configuration: IProlongationConfig = yield select(
        getProlongationCmsConfiguration
      );
      const openedFrom: string = yield select(getProlongationOpenedFrom);

      if (
        state.prolongationCheckoutDetails.data.length > 0 &&
        Array.isArray(state.prolongationCheckoutDetails.data[0].prices) &&
        state.prolongationCheckoutDetails.data[0].prices.length
      ) {
        const variant = state.prolongationCheckoutDetails.data[0];
        const upfrontAmt = variant.prices.find(price => {
          return price.priceType === PRICE_TYPE.UPFRONT_PRICE;
        });
        const monthlyAmt = variant.prices.find(price => {
          return price.priceType === PRICE_TYPE.RECURRING_FEE;
        });
        const variantId = history.location.pathname.split('/')[4];
        if (Array.isArray(variant.optionValues)) {
          const actualUpfrontPrice = getUpfrontPrice(variant.prices);
          const currencySymbol = getPriceAndCurrency(
            actualUpfrontPrice,
            getCurrencyCode()
          ).discountCurrency;
          const currencyCode = getCurrencyCode();

          sendForPersonalDetailProductSelected({
            label: `${variantId}_${variant.name}_${variant.optionValues[1]}_${
              variant.optionValues[0]
            }_${variant.optionValues[2]}`,
            deviceId: variantId,
            deviceName: variant.name,
            deviceUpfrontAmt: upfrontAmt
              ? String(upfrontAmt.discountedValue)
              : '',
            deviceMonthlyAmt: monthlyAmt
              ? String(monthlyAmt.discountedValue)
              : '',
            openedFrom,
            pageUrl: encodeRFC5987ValueChars(window.location.href),
            gaCode: configuration.gaCode,
            currencySymbol,
            currencyCode
          });
        }
        /** GA */

        const payloadData = payloadForPostUserInfotLead(
          state.prolongationCheckoutDetails.userInfo,
          state.prolongationCheckoutDetails.data[0],
          variantId,
          state.prolongationCheckoutDetails.marketingConsentAgreement,
          store.getState().translation.prolongation,
          applyConsentDiscount
        );

        logInfo('payloadData', JSON.stringify(payloadData));

        const publicKeyPem = `-----BEGIN PUBLIC KEY-----
  ${state.prolongationRSAKey}
  -----END PUBLIC KEY-----`;

        const {
          encryptedAesData,
          encryptedAesKey
        } = yield prolongationEncryption(
          JSON.stringify(payloadData),
          publicKeyPem
        );

        console.log(
          'encrypted data ',
          JSON.stringify({
            data: encryptedAesData,
            enc_key: encryptedAesKey,
            rsa_key_label: state.prolongationRSALabel,
            uuid: uuid ? uuid : '',
            category_id: parsedQuery.categoryId,
            segment_id: segment ? segment : '',
            natco_code: APP_CONSTANTS.COUNTRY_CODE
          })
        );

        yield call(placeProlongationOrderService, {
          data: encryptedAesData,
          enc_key: encryptedAesKey,
          rsa_key_label: state.prolongationRSALabel,
          uuid: uuid ? uuid : '',
          profileId: profileId ? profileId : '',
          category_id: parsedQuery.categoryId,
          segment_id: segment ? segment : '',
          natco_code: APP_CONSTANTS.COUNTRY_CODE
        });
        /** GA */
        const prolongationConfiguration: IProlongationConfig = yield select(
          getProlongationCmsConfiguration
        );
        if (Array.isArray(variant.optionValues)) {
          const actualUpfrontPrice = getUpfrontPrice(variant.prices);
          const currencySymbol = getPriceAndCurrency(
            actualUpfrontPrice,
            getCurrencyCode()
          ).discountCurrency;
          const currencyCode = getCurrencyCode();

          console.log(getCurrencyCode());

          sendForPersonalDetailSuccess({
            label: `${variantId}_${variant.name}_${variant.optionValues[1]}_${
              variant.optionValues[0]
            }_${variant.optionValues[2]}`,
            deviceId: variantId,
            deviceName: variant.name,
            deviceUpfrontAmt: upfrontAmt
              ? String(upfrontAmt.discountedValue)
              : '',
            deviceMonthlyAmt: monthlyAmt
              ? String(monthlyAmt.discountedValue)
              : '',
            openedFrom,
            pageUrl: encodeRFC5987ValueChars(window.location.href),
            gaCode: prolongationConfiguration.gaCode,
            currencySymbol,
            currencyCode
          });
        }
        /** GA */
      }
      yield put(actions.placeOrderSuccess());
      history.push(ROUTES.ORDER_CONFIRMATION);
    }
  } catch (error) {
    yield put(actions.placeOrderError());
    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* generateToken(): Generator {
  try {
    const response = yield generateTokenService();
    const moengageEvent = store.getState().productList.moengageEventTemp;
    yield put(actions.generateTokenSuccess(response));
    yield put(actions.sendMoengageEvent(moengageEvent));
  } catch (error) {
    if (error.status === 401) {
      yield put(actions.refreshToken({ type: 'expireToken' }));
    }
    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* refreshToken(action: {
  type: string;
  payload: { type: string; data: IGAEvent };
}): Generator {
  try {
    const refreshToken = store.getState().productList.refreshToken;

    const response = yield refreshTokenService({ refreshToken });
    yield put(actions.generateTokenSuccess(response));
    const errorsObj = store.getState().translation.prolongation.global.errors;

    if (action.payload.type === 'moengage') {
      yield put(actions.sendMoengageEvent(action.payload.data));
      sendForApiError(
        errorsObj[ERROR_CODE_BFF.HANDLE_401] as string,
        401,
        action.payload.data.url as string
      );
    }
  } catch (error) {
    yield put(actions.generateTokenError());

    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* sendMoengageEvent(action: {
  type: string;
  payload: IGAEvent;
}): Generator {
  try {
    if (history) {
      const profileId = getValueFromParams(
        history.location.search,
        'profileId'
      );
      const uuid = getValueFromParams(history.location.search, 'uuid');
      const accessToken = store.getState().productList.accessToken;
      const payload: IMoengageEvent = {
        eventId: uuid as string,
        customerId: profileId as string,
        messageId: uuid as string,
        action: action.payload.eventAction,
        attributes: transformMoengageAttribute(action.payload)
      };

      if (accessToken !== '' && payload.action != '') {
        const response = yield sendMoengageEventService({
          payloadData: payload,
          accessToken
        });
        yield put(actions.sendMoengageEventSuccess(response));
      }
    }
  } catch (error) {
    if (error.status === 401) {
      yield put(
        actions.refreshToken({ type: 'moengage', data: action.payload })
      );
    }
    logError(error);
  }
}

/** PROLONGATION FUNCTION */
export function* getRSALeadKey(): Generator {
  try {
    if (history) {
      const uuid_data = getValueFromParams(history.location.search, 'uuid');
      const bearerToken = store.getState().common.prolongation
        .authorizationToken;
      const rsaKeyReceived = store.getState().productList.prolongationRSAKey;

      if (rsaKeyReceived === '') {
        const response = yield getProlongationLeadKeyService({
          uuid: uuid_data as string,
          bearerToken: bearerToken as string
        });
        if (response) {
          yield put(actions.getRSALeadKeySuccess(response));
        }
      }
    }
  } catch (error) {
    yield put(actions.getRSALeadKeyError());
    logError(error);
  }
}

export function* callProlongationCheckoutDetailAction(): Generator {
  try {
    yield put(actions.fetchProlongationCheckoutDetails(false));
  } catch (error) {
    logError(error);
  }
}

export function* getProlongationMarketing(action: {
  type: string;
  payload: {
    oneApp: boolean;
    callbackFn: any;
    languageParam: string | undefined;
  };
}): Generator {
  try {
    if (action.payload.oneApp) {
      let languageCode = appConstants.LANGUAGE_CODE;

      if (action.payload.languageParam) {
        languageCode = action.payload.languageParam;
      }

      if (
        appConstants.PROLONGATION_INSTANCE_KEY &&
        appConstants.PROLONGATION_INSTANCE_KEY !== ''
      ) {
        const response = yield fetchProlongationConsentService(languageCode);

        action.payload.callbackFn(
          'Prolongation running for HNS',
          `${
            appConstants.WEB_VIEW_MARKETING_URL
          }${apiEndpoints.CONFIG.GET_PROLONGATION_MARKETING.url(
            appConstants.PROLONGATION_INSTANCE_KEY,
            languageCode
          )}`
        );

        action.payload.callbackFn(
          'Marketing Consent Data log ',
          JSON.stringify(response)
        );

        yield put(actions.setMarketingAgreement(response));
      }
    }
  } catch (error) {
    action.payload.callbackFn(
      'Marketing Consent Error log ',
      JSON.stringify(error)
    );
    logError(error);
  }
}

export function* updateProlongationFormField(action: {
  type: string;
  payload: { field: string; value: string };
}): Generator {
  try {
    const prolongationFormConfig = store.getState().configuration
      .cms_configuration.modules.prolongation.form;

    let cmsFormField = action.payload.field;

    if (action.payload.field === 'msisdn') {
      cmsFormField = 'phoneNumber';
    } else if (action.payload.field === 'billingAddress') {
      cmsFormField = 'deliveryAddress';
    }

    const isFieldValid = isFormFieldValidAsPerType(
      prolongationFormConfig[cmsFormField].validation,
      action.payload.value,
      prolongationFormConfig[cmsFormField].mandatory
    );

    yield put(
      actions.updateFormValidStatus({
        field: action.payload.field,
        value: isFieldValid
      })
    );
  } catch (error) {
    logError(error);
  }
}

export function* checkProlongationFormValidation(): Generator {
  try {
    setTimeout(() => {
      if (document) {
        const errorElement = document.getElementsByClassName('error')[0];

        if (errorElement) {
           errorElement.scrollIntoView();
        }
      }
    }, 0);
  } catch (error) {
    logError(error);
  }
}

export default function* watcherSagas(): Generator {
  yield takeLatest(
    CONSTANTS.LOAD_MORE_PRODUCT_LIST_REQUESTED,
    loadMoreProductList
  );
  yield takeLatest(CONSTANTS.FETCH_PRODUCT_LIST_REQUESTED, fetchProductList);
  yield takeLatest(CONSTANTS.SEND_STOCK_NOTIFICATION, sendStockNotification);
  yield takeLatest(CONSTANTS.FETCH_POPULAR_DEVICE, fetchPopularDevice);
  /** PROLONGATION FUNCTIONS */
  yield takeLatest(
    CONSTANTS.FETCH_PROLONGATION_PRODUCT_LIST,
    fetchProlongationProductList
  );
  yield takeLatest(
    CONSTANTS.FETCH_PROLONGATION_CHECKOUT_DETAILS,
    fetchProlongationCheckoutDetails
  );
  yield takeLatest(
    CONSTANTS.FETCH_PROLONGATION_PRODUCT_DETAILS,
    fetchProlongationProductDetails
  );
  yield takeLatest(CONSTANTS.PLACE_ORDER, placeOrder);
  yield takeLatest(CONSTANTS.GET_LEAD, getLeadRequested);
  yield takeLatest(CONSTANTS.GENERATE_TOKEN, generateToken);
  yield takeLatest(CONSTANTS.REFRESH_TOKEN, refreshToken);
  yield takeLatest(CONSTANTS.SEND_MOENGAGE_EVENT, sendMoengageEvent);
  yield takeLatest(CONSTANTS.GET_RSA_LEAD_REQUESTED, getRSALeadKey);
  yield takeLatest(
    CONSTANTS.CHECK_FORM_PROLONGATION_VALIDATION,
    checkProlongationFormValidation
  );
  yield takeLatest(
    CONSTANTS.CALL_PROLONGATION_CHECKOUT_ACTION,
    callProlongationCheckoutDetailAction
  );
  yield takeLatest(
    CONSTANTS.ORDER_SUMMARY_OPEN_CLOSE_EVENT,
    orderSummaryOpenCloseEvent
  );
  yield takeLatest(
    CONSTANTS.GET_PROLONGATION_MARKETING_CONSENT,
    getProlongationMarketing
  );
  yield takeLatest(
    CONSTANTS.UPDATE_PROLONGATION_FORM_FIELD,
    updateProlongationFormField
  );
}
