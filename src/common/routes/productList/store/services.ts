import { apiEndpoints } from '@common/constants';
import * as prolongationPlaceOrderApi from '@common/types/api/placeProlongationOrder';
import { CancelTokenSource } from 'axios';
import * as productListApi from '@common/types/api/productList';
import * as prolongationProductListApi from '@common/types/api/prolongationProductList';
import * as prolongationCheckoutApi from '@common/types/api/prolongationCheckout';
import * as prolongationGetLeadApi from '@common/types/api/prolongationGetLead';
import * as prolongationGetRSApi from '@common/types/api/prolongationGetRSA';
import * as prolongationGetConsentApi from '@common/types/api/prolongationConsent';
import apiCaller from '@common/utils/apiCaller';
import * as sendStockNotificationApi from '@common/types/api/sendStockNotification';
import {
  getNotificationRequestTemplate,
  getProductListingRequestTemplate
} from '@productList/utils/requestTemplate';
import { logError } from '@src/common/utils';
import {
  IGetNotificationRequestTemplatePayload,
  IMoengageEvent
} from '@productList/store/types';
import APP_KEYS from '@common/constants/appkeys';
import appConstants from '@src/common/constants/appConstants';
import store from '@src/common/store';

export interface IFiltersRequest {
  name: string;
  characteristicValues: string[];
  provideQuantity: boolean;
}

export interface IPayload {
  filters: IFiltersRequest[];
  currentPage: number;
  itemsPerPage: number;
  categoryId: string;
  sortBy: string;
  tariffId: string | null;
  loyalty: string | null;
  discountId: string | null;
  showFullPageError?: boolean;
  signal?: CancelTokenSource;
}

export const fetchProlongationProductListService = async (
  payload: prolongationProductListApi.POST.IRequest
): Promise<prolongationProductListApi.POST.IResponse> => {
  try {
    return await apiCaller.post<prolongationProductListApi.POST.IResponse>(
      apiEndpoints.PRODUCTLIST.PROLONGATION.GET_PRODUCTS_LIST.url(
        payload.profileId
      ),
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchProlongationCheckoutDetailsService = async (
  payload: prolongationCheckoutApi.POST.IRequest
): Promise<prolongationCheckoutApi.POST.IResponse> => {
  try {
    return await apiCaller.post<prolongationCheckoutApi.POST.IResponse>(
      apiEndpoints.PRODUCTLIST.PROLONGATION.CHECKOUT.url(
        payload.profileId as string
      ),
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchProductListService = async (
  payload: IPayload
): Promise<productListApi.POST.IResponse> => {
  const {
    currentPage,
    itemsPerPage,
    categoryId,
    sortBy,
    filters,
    tariffId,
    loyalty,
    discountId,
    showFullPageError,
    signal
  } = payload;
  const requestBody = getProductListingRequestTemplate(
    currentPage,
    itemsPerPage,
    categoryId,
    filters,
    tariffId,
    loyalty,
    discountId
  );

  const apiUrl = sortBy
    ? `${apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url}?sortBy=${sortBy}`
    : apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url;

  try {
    return await apiCaller.post<productListApi.POST.IResponse>(
      apiUrl,
      requestBody,
      {
        showFullPageError: !!showFullPageError,
        cancelToken: signal && signal.token
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const sendStockNotificationService = (
  payload: IGetNotificationRequestTemplatePayload,
  reason: string
): Promise<sendStockNotificationApi.POST.IResponse> => {
  const requestBody = getNotificationRequestTemplate(payload, reason);

  return apiCaller.post<sendStockNotificationApi.POST.IResponse>(
    apiEndpoints.PRODUCTLIST.SEND_STOCK_NOTIFICATION.url,
    requestBody
  );
};

export const fetchPopularDeviceService = async (payload: {
  categoryId: string;
  tariffId: string | null;
  loyalty: string | null;
  discountId: string | null;
}): Promise<productListApi.POST.IResponse> => {
  const { categoryId, tariffId, loyalty, discountId } = payload;
  const requestBody = getProductListingRequestTemplate(
    0,
    4,
    categoryId,
    [],
    tariffId,
    loyalty,
    discountId
  );

  const apiUrl = `${apiEndpoints.PRODUCTLIST.GET_PRODUCTS_LIST.url}?sortBy=${
    APP_KEYS.POPULAR_INDEX
  }`;

  try {
    return await apiCaller.post<productListApi.POST.IResponse>(
      apiUrl,
      requestBody
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const placeProlongationOrderService = async (
  payload: prolongationPlaceOrderApi.POST.IRequest
): Promise<{}> => {
  try {
    return await apiCaller.post<{}>(
      apiEndpoints.PRODUCTLIST.PROLONGATION.PLACE_ORDER.url(payload.profileId),
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const getLeadService = async (payload: {
  sub: string;
  bearerToken: string;
}): Promise<prolongationGetLeadApi.GET.IResponse> => {
  try {
    const oneAppBaseUrl = store.getState().configuration.cms_configuration
      .global.prolongation.bffBaseUrl;

    return await apiCaller.get(
      `${oneAppBaseUrl}${apiEndpoints.PRODUCTLIST.PROLONGATION.GET_LEAD.url(
        payload.sub
      )}`,
      {
        withBaseUrl: false,
        headers: {
          Authorization: payload.bearerToken
        }
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const postLeadService = async (
  uuid: string,
  payload: prolongationPlaceOrderApi.POST.IRequest
): Promise<{}> => {
  try {
    return await apiCaller.post<{}>(
      apiEndpoints.PRODUCTLIST.PROLONGATION.POST_LEAD.url(uuid),
      payload
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const generateTokenService = async (): Promise<{}> => {
  try {
    return await apiCaller.get(
      `${appConstants.MOENGAGE_BASE_URL}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN.url
      }`,
      {
        withBaseUrl: false,
        headers: {
          client_id: appConstants.CLIENT_ID,
          oneapp_token: `Basic ${btoa(
            `${appConstants.CLIENT_ID}:${appConstants.SECRET_KEY}`
          )}`
        }
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const refreshTokenService = async (payload: {
  refreshToken: string;
}): Promise<{}> => {
  try {
    return await apiCaller.get(
      `${appConstants.MOENGAGE_BASE_URL}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.REFRESH_TOKEN.url
      }`,
      {
        withBaseUrl: false,
        headers: {
          client_id: appConstants.CLIENT_ID,
          oneapp_token: `Basic ${btoa(
            `${appConstants.CLIENT_ID}:${appConstants.SECRET_KEY}`
          )}`,
          refresh_token: payload.refreshToken
        }
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const sendMoengageEventService = async (payload: {
  payloadData: IMoengageEvent;
  accessToken: string;
}): Promise<{}> => {
  try {
    return await apiCaller.post(
      `${appConstants.MOENGAGE_BASE_URL}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.MOENGAGE_PUSH_EVENTS.url
      }`,
      payload.payloadData,
      {
        withBaseUrl: false,
        headers: {
          Authorization: `Bearer ${payload.accessToken}`
        }
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const getProlongationLeadKeyService = async (payload: {
  bearerToken: string;
  uuid: string;
}): Promise<prolongationGetRSApi.GET.IResponse> => {
  try {
    const oneAppBaseUrl = store.getState().configuration.cms_configuration
      .global.prolongation.bffBaseUrl;

    return await apiCaller.get(
      `${oneAppBaseUrl}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.GET_RSA_KEY.url
      }`,
      {
        withBaseUrl: false,
        headers: {
          Authorization: payload.bearerToken,
          client: 'oneshop',
          'x-request-session-id': payload.uuid,
          'x-request-tracking-id': payload.uuid
        }
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchProlongationConsentService = async (
  languageCode: string
): Promise<prolongationGetConsentApi.GET.IResponse> => {
  try {
    // tslint:disable-next-line:no-console
    console.log(
      'URL For Prolongation Agreement',
      `${
        appConstants.WEB_VIEW_MARKETING_URL
      }${apiEndpoints.CONFIG.GET_PROLONGATION_MARKETING.url(
        appConstants.PROLONGATION_INSTANCE_KEY,
        languageCode
      )}`
    );

    return apiCaller.get(
      `${
        appConstants.WEB_VIEW_MARKETING_URL
      }${apiEndpoints.CONFIG.GET_PROLONGATION_MARKETING.url(
        appConstants.PROLONGATION_INSTANCE_KEY,
        languageCode
      )}`,
      {
        withBaseUrl: false
      }
    );
  } catch (error) {
    logError(error);
    throw error;
  }
  // tslint:disable-next-line:max-file-line-count
};
