import { IProductListState } from '@productList/store/types';
import { PAGE_THEME } from '@common/store/enums/index';
import { PROLONGATION_EVENT_NAME } from '@src/common/oneAppRoutes/utils/enum';

import { MODAL_TYPE, NOTIFICATION_MEDIUM } from './enum';

export default (): IProductListState => ({
  data: [],
  tempData: [],
  openModal: false,
  tariffId: null,
  tariffName: null,
  loyalty: null,
  discountId: null,
  defaultSortBy: null,
  error: {
    code: 0,
    message: ''
  },
  loading: false,
  gridLoading: false,
  selectedFilters: {},
  tempSelectedFilters: {},
  tempSortBy: null,
  pagination: {
    currentPage: 1,
    itemsPerPage: 8,
    totalItems: 0
  },
  tempFilterCharacterstics: [],
  tempPagination: {
    currentPage: 1,
    itemsPerPage: 8,
    totalItems: 0
  },
  categoryName: '',
  backgroundImage: '',
  parentCategorySlug: '',
  theme: PAGE_THEME.PRIMARY,
  filterCharacteristics: [],
  characteristics: [],
  showAppShell: true,
  receivedProductListDataFromServer: false,
  notificationLoading: false,
  notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL,
  notificationData: {
    variantId: '',
    deviceName: '',
    type: NOTIFICATION_MEDIUM.EMAIL,
    mediumValue: ''
  },
  lastScrollPositionMap: {},
  lastListURL: null,
  sortListing: [],
  sortBy: { id: '', title: '' },
  popularDevices: [],
  prolongationData: [],
  uuid: '',
  profileId: '',
  prolongationSelectedVariant: '',
  prolongationOpenedFrom: '',
  prolongationSelectedProduct: '',
  prolongationCheckoutDetails: {
    data: [],
    isFormSubmitted: false,
    userInfo: {
      email: '',
      msisdn: '',
      firstName: '',
      lastName: '',
      billingAddress: '',
      categoryId: '',
      deliveryContact: '',
      segmentId: '',
      deliveryAddress: '',
      contactEmail: ''
    },
    formValidation: {
      firstNameValid: true,
      lastNameValid: true,
      msisdnValid: true,
      emailValid: true,
      deliveryContactValid: true,
      billingAddressValid: true
    },
    agreeOnTermsAndCondition: false,
    userDataReceived: false,
    marketingConsentAgreement: []
  },
  plan: {
    id: '',
    productId: '',
    productName: '',
    name: '',
    description: '',
    group: '',
    bundle: false,
    prices: [],
    attachments: {},
    characteristics: [],
    unavailabilityReasonCodes: []
  },
  installmentEnabled: false,
  loyality: '12',
  installment: '24',
  selectedAttributes: [],
  accessToken: '',
  refreshToken: '',
  prolongationRSAKey: '',
  prolongationRSALabel: '',
  moengageEventTemp: {
    hitType: '',
    eventCategory: PROLONGATION_EVENT_NAME.PAGE_OPEN,
    eventAction: '',
    eventLabel: '',
    eventValue: 1,
    page: '',
    url: '',
    opened_from: '',
    device_id: '',
    device_name: '',
    device_amt_upfront: '',
    device_amt_monthly: '',
    amt: '',
    currencySymbol: '',
    currencyCode: ''
  }
});
