import {
  IProductListTranslation,
  IProlongation
} from '@src/common/store/types/translation';
import APP_KEYS from '@common/constants/appkeys';
import * as productListApi from '@src/common/types/api/productList';
import {
  IFilterCharacteristic,
  IGAEvent,
  IGetLead,
  IMoengageAttribute,
  IPayloadForPostUserInfotLead,
  IProductListItem,
  IPrologationProducts,
  IProlongationCheckoutProduct,
  IProlongationConsent,
  IProlongationProductListResponse,
  ITransformedConsentProlongation,
  ITransformedSubConsent,
  IUserInfo,
  IVariant
} from '@productList/store/types';
import { PRODUCT_ATTRIBUTE } from '@productList/store/enum';

import { PRICE_TYPE } from '../../productDetailed/store/enum';

// to be deleted
export const transformFilterCharacteristics = (
  filterCharacteristics: IFilterCharacteristic[],
  productListTranslation: IProductListTranslation
): IFilterCharacteristic[] => {
  return filterCharacteristics.map(filterCharacteristic => {
    if (filterCharacteristic.id === APP_KEYS.BFF_AVAILABILITY_TRANSLATION_ID) {
      filterCharacteristic.data = filterCharacteristic.data.map(dataItem => {
        if (productListTranslation.availability[dataItem.value] !== undefined) {
          dataItem.name = productListTranslation.availability[dataItem.value];
        }

        return dataItem;
      });
    }
    // else if (filterCharacteristic.id === APP_KEYS.BFF_PRICE_RANGE_ID) {
    //   filterCharacteristic.data = filterCharacteristic.data.map(dataItem => {
    //     dataItem.name = dataItem.value;

    //     return dataItem;
    //   });
    // }

    return filterCharacteristic;
  });
};

export const getPopularDevice = (
  products: productListApi.POST.IResponse
): IVariant[] => {
  return products.data.reduce(
    (result: IVariant[], product: IProductListItem): IVariant[] => {
      result.push(product.variant);

      return result;
    },
    []
  );
};

export const transformProlongationProductData = (
  productList: IProlongationProductListResponse
): IProlongationProductListResponse => {
  const data = productList.data || [];
  // NOTE: Why it is done, also it is asumed highest priority element will come at 0th index, to support the ui design!
  const firstElement = data.shift() as IPrologationProducts;
  data.splice(1, 0, firstElement);

  return productList;
};

export const getLeadTransform = (data: IUserInfo, getPayload: IGetLead) => {
  const {
    individual: { givenName, familyName },
    contactMediums
  } = getPayload;

  const obj = {
    email: '',
    msisdn: '',
    firstName: givenName,
    lastName: familyName
  };

  const emailData = contactMediums.find(item => {
    return item.type === 'email';
  });

  const msisdnData = contactMediums.find(item => {
    return item.type === 'mobile';
  });

  const contactData = contactMediums.find(item => {
    return item.type === 'phone';
  });

  obj.email = emailData ? emailData.medium.emailAddress : '';
  obj.msisdn = msisdnData
    ? msisdnData.medium.number
    : contactData
    ? contactData.medium.number
    : '';

  if (obj.msisdn !== '') {
    data.deliveryContact = obj.msisdn;
  }

  Object.assign(data, obj);
};

export const createMarketingAgreement = (
  marketingData: ITransformedConsentProlongation[],
  pppTranslation: IProlongation,
  applyConsentDiscount: boolean
): IProlongationConsent[] => {
  const transformedConsentData: IProlongationConsent[] = [];
  let consentObj: IProlongationConsent;

  const { accepted, notAccepted } = pppTranslation.global;
  const { termsAndConditionText } = pppTranslation.checkout;

  if (applyConsentDiscount) {
    marketingData.forEach((consent: ITransformedConsentProlongation) => {
      consentObj = {
        name: consent.name,
        status: consent.isSelected ? accepted : notAccepted,
        isAccepted: consent.isSelected,
        subAgreements: []
      };
      if (consent.subConsent && consent.subConsent.length) {
        consent.subConsent.forEach((subConsent: ITransformedSubConsent) => {
          const subConsentObj = {
            name: subConsent.name,
            status: subConsent.isSelected ? accepted : notAccepted,
            isAccepted: subConsent.isSelected
          };
          if (consentObj.subAgreements) {
            consentObj.subAgreements.push(subConsentObj);
          }
        });
      }
      transformedConsentData.push(consentObj);
    });

    transformedConsentData.push({
      name: termsAndConditionText,
      status: accepted,
      isAccepted: true
    });

    return transformedConsentData;
  }

  return transformedConsentData;
};

export const payloadForPostUserInfotLead = (
  userInfoPayLoad: IUserInfo,
  productData: IProlongationCheckoutProduct,
  variantId: string,
  marketingData: ITransformedConsentProlongation[],
  pppTranslation: IProlongation,
  applyConsentDiscount: boolean
): IPayloadForPostUserInfotLead => {
  // tslint:disable:variable-name

  let {
    firstName: first_name,
    lastName: last_name,
    billingAddress: billing_address,
    deliveryAddress: delivery_address
  } = userInfoPayLoad;
  const {
    email,
    msisdn,
    contactEmail: contact_email,
    deliveryContact: contact_number,
    categoryId: category_id,
    segmentId: segment_id
  } = userInfoPayLoad;
  const upfrontPrice = productData.prices.find(price => {
    return price.priceType === PRICE_TYPE.UPFRONT_PRICE;
  });
  const monthlyPrice = productData.prices.find(price => {
    return price.priceType === PRICE_TYPE.RECURRING_FEE;
  });

  const planItem =
    productData.characteristics &&
    productData.characteristics.find(item => {
      return item.name === PRODUCT_ATTRIBUTE.PLAN;
    });

  let planCode = null;

  if (planItem && Array.isArray(planItem.values) && planItem.values.length) {
    planCode = planItem.values[0].value;
  }

  first_name = first_name.replace(/^\s+|\s+$/g, '').replace(/\s+/g, ' ');
  last_name = last_name.replace(/^\s+|\s+$/g, '').replace(/\s+/g, ' ');
  delivery_address = delivery_address
    .replace(/^\s+|\s+$/g, '')
    .replace(/\s+/g, ' ');
  billing_address = billing_address
    .replace(/^\s+|\s+$/g, '')
    .replace(/\s+/g, ' ');

  return {
    email,
    msisdn,
    first_name,
    last_name,
    billing_address,
    category_id,
    contact_number,
    segment_id,
    delivery_address,
    contact_email,
    order_details: {
      orderDetails: {
        timestamp: new Date().getTime(),
        upfrontPrice: upfrontPrice ? upfrontPrice.discountedValue : '',
        recurringPrice: monthlyPrice ? monthlyPrice.discountedValue : ''
      },
      variantDetails: {
        variantId,
        color: productData.optionValues[1],
        storage: productData.optionValues[0],
        plan: productData.optionValues[2],
        model: productData.name,
        plan_code: planCode
      }
    },
    consent: createMarketingAgreement(
      marketingData,
      pppTranslation,
      applyConsentDiscount
    )
  };
};

export const transformMoengageAttribute = (gaEvent: IGAEvent) => {
  const attributeArr: IMoengageAttribute[] = [];

  Object.keys(gaEvent).forEach((event: string) => {
    attributeArr.push({
      name: event,
      value: gaEvent[event]
    });
  });

  return attributeArr;
};

export const changeInnerConsent = (data: ITransformedConsentProlongation) => {
  let checkOfInnerFlag = true;
  data.subConsent.forEach(item => {
    if (!item.isSelected) {
      checkOfInnerFlag = false;

      return;
    }
  });
  data.isSelected = checkOfInnerFlag;
};

export const marketingConsentOuterChange = (
  stateSubConsentData: ITransformedSubConsent[],
  isSelected: boolean
) => {
  stateSubConsentData.forEach(item => {
    item.isSelected = isSelected;
  });
};
