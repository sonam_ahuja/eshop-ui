import { IGrid } from 'dt-components';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';
import { ITEM_STATUS, PAGE_THEME } from '@common/store/enums/index';
import { IPrices } from '@tariff/store/types';
import { IAttachments as IProlongationAttachments } from '@productDetailed/store/types';
import { PROLONGATION_EVENT_NAME } from '@src/common/oneAppRoutes/utils/enum';
import { CancelTokenSource } from 'axios';
// tslint:disable
export interface IProductListState {
  pagination: IGrid.IPagination; // all temp values are for mobile modal opening
  tempPagination: IGrid.IPagination;
  defaultSortBy: null | string;
  // tariff related
  tariffId: string | null;
  tariffName: string | null;
  loyalty: string | null;
  discountId: string | null;
  data: IProductListItem[];
  backgroundImage: string;
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
  tempData: IProductListItem[];
  selectedFilters: ISelectedFilters;
  tempSelectedFilters: ISelectedFilters;
  tempSortBy: ISortList | null;
  filterCharacteristics: IFilterCharacteristic[];
  tempFilterCharacterstics: IFilterCharacteristic[];
  characteristics: ICharacteristics[];
  loading: boolean;
  gridLoading: boolean;
  categoryName: string;
  sortListing: ISortList[];
  sortBy: ISortList | null;
  notificationLoading: boolean;
  error: IError | null;
  parentCategorySlug: string;
  showAppShell: boolean;
  receivedProductListDataFromServer: boolean;
  notificationModal: MODAL_TYPE;
  openModal: boolean;
  plan: IVariant;
  notificationData: INotificationData;
  lastScrollPositionMap: ILastScrollPositionMap;
  lastListURL: string | null;
  popularDevices: IVariant[];
  prolongationData: IPrologationProducts[];
  uuid: string;
  installmentEnabled: boolean;
  loyality: string;
  installment: string;
  profileId: string;
  prolongationSelectedVariant: string;
  prolongationSelectedProduct: string;
  prolongationOpenedFrom: string;
  prolongationCheckoutDetails: IProlongationCheckoutDetails;
  selectedAttributes: IChangedAttribute[];
  accessToken: string;
  refreshToken: string;
  prolongationRSAKey: string;
  prolongationRSALabel: string;
  moengageEventTemp: IGAEvent;
}

/** USED ON PROLONGATION */
export interface IProlongationCheckoutDetails {
  data: IProlongationCheckoutProduct[];
  userInfo: IUserInfo;
  userDataReceived: boolean;
  isFormSubmitted: boolean;
  agreeOnTermsAndCondition: boolean;
  formValidation: IProlongationFormValidation;
  marketingConsentAgreement: ITransformedConsentProlongation[];
}

export interface IProlongationFormValidation {
  firstNameValid: boolean;
  lastNameValid: boolean;
  msisdnValid: boolean;
  emailValid: boolean;
  deliveryContactValid: boolean;
  billingAddressValid: boolean;
}

export interface ISortList {
  id: string;
  title: string;
}

export interface ILastScrollPositionMap {
  [urlPath: string]: number;
}
export interface INotificationData {
  variantId: string;
  deviceName: string;
  mediumValue: string;
  type: NOTIFICATION_MEDIUM.EMAIL | NOTIFICATION_MEDIUM.PHONE;
}

export interface IImageURL {
  name: string;
  url: string;
}

export interface IQueryParamFilter {
  [filterId: string]: string[];
}
export interface IQueryParams {
  filters: IQueryParamFilter;
  sortBy: null | string;
  itemPerPage: string | null;
}
export interface ITariffParams {
  tariffId: string | null;
  loyalty: string | null;
  discountId: string | null;
}
export interface ISortBy {
  id: string;
  title: string;
}
/** USED ON PROLONGATION */
export interface IError {
  code?: number;
  message: string;
}
export interface IFilterCharacteristic {
  id: string;
  name: string;
  type: string;
  data: IFilter[];
}

export interface IFilter {
  name: string;
  default: boolean;
  value: string;
  quantity?: number;
  // id?: string;
  // description?: string;
  // valueType?: string;
}

export interface ISelectedFilters {
  [key: string]: IFilter[];
}

export interface IProductListItem {
  category: string;
  variant: IVariant;
  groups: IGroups;
}

export interface IGroups {
  [key: string]: IKeyValue;
}

export type eligibilityUnavailabilityReasonType =
  | ITEM_STATUS.IN_STOCK
  | ITEM_STATUS.OUT_OF_STOCK
  | ITEM_STATUS.PRE_ORDER;
export interface IVariant {
  id: string;
  productId: string;
  productName: string;
  name: string;
  brand?: string;
  description: string;
  group: string;
  bundle: boolean;
  prices: IPrices[];
  attachments: IAttachments;
  characteristics: IKeyValue[];
  // changed due to change in pre-order
  unavailabilityReasonCodes: eligibilityUnavailabilityReasonType[];
  // eligibilityUnavailabilityReason: IKeyValue[];
}

export interface IKeyValue {
  key: string;
  value: string;
}

export interface IAttachments {
  [key: string]: IImageURL[];
}

export interface IProductListRequest {
  channel: string;
  page: number;
  itemPerPage: number;
  categories: {
    id: string;
    characteristics: {
      key: string;
      value: string;
    }[];
  }[];
  filterCharacteristics: IFilterCharacteristicValue[];
  conditions: ICondition[];
}
export interface IJSONLdRequest extends IProductListRequest {
  variantId: string | null;
}

export interface IFilterCharacteristicValue {
  id: string;
  characteristicValues: string[];
}

export interface ICondition {
  id: string;
  value: string;
}

export interface ICategory {
  id: string;
}

export interface IChannel {
  id: string;
}

export interface IFetchProductListParams {
  currentPage: number;
  categoryId: string;
  queryParams?: IQueryParams;
  scrollAmount?: number;
  showFullPageError?: boolean;
  signal?: CancelTokenSource;
  successCallback?(): void;
}

export interface ILoadMoreProductListParams {
  categoryId: string;
  isChanged: boolean;
  scrollAmount?: number;
  pageNumber: number;
  queryParams?: IQueryParams;
  showFullPageError?: boolean;
  successCallback?(): void;
}

export interface IItemPerPageOptions {
  title: string;
  // selected: boolean;
  id: number;
}

export interface IGetNotificationRequestTemplatePayload {
  type: NOTIFICATION_MEDIUM;
  variantId: string;
  mediumValue: string;
}
// API types

// NOTE: some of types are above also which are used in API as well as in code

export interface IOutOfStockNotification {
  channel: IChannel;
  reason: string;
  description: string;
  contacts: IContact[];
  interactionItems: IInteractionItem[];
}

interface IInteractionItem {
  item: IItem;
}

interface IItem {
  id: string;
  entityReferredType: string;
}

interface IContact {
  type: string;
  medium: IMedium;
  validated: boolean;
}

interface IMedium {
  number?: string;
  emailAddress?: string;
}
export interface IProductListResponse {
  filterCharacteristics: IFilterCharacteristic[];
  pageNumber: number;
  plan: IVariant;
  itemsPerPage: number;
  categoryId?: string;
  categoryName: string;
  characteristics: ICharacteristics[];
  resultCount: number;
  data: IProductListItem[];
  installmentEnabled: boolean;
  loyality: string;
  installment: string;
}

/** USED ON PROLONGATION */
export interface ICharacteristics {
  name: string;
  values: IValue[];
}

export interface ILabelValue {
  label?: string;
  value: string;
  isCustomerVisible?: boolean;
}

export interface IValue {
  value: string;
  label?: string;
}

export type pageThemeType = PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;

// Prolongation TYPES

export interface IChangedAttribute {
  name: string;
  value: string;
}

/** USED ON PROLONGATION */
export interface IFetchProlongationProductListPayload {
  uuid: string;
  channel: string;
  category: string;
  segment: string;
  checkHistoricalLead: boolean;
  productId?: string;
  variantId: string | null;
  changedAttributes?: IChangedAttribute[];
  selectedAttributes?: IChangedAttribute[];
  requestedAttributes: string[];
  profileId: string;
}
/** USED ON PROLONGATION */
export interface IProlongationProductListResponse {
  category: string;
  data: IPrologationProducts[];
  code?: string;
  errorMessage?: {
    message: string;
    debugMessage: string;
  };
}

/** USED ON PROLONGATION */
export interface IPrologationProducts {
  productId: string;
  variantId: string;
  name: string;
  shortDescription: string;
  description: string;
  optionValues: string[];
  characteristics: ICharacteristics[];
  prices: IPrices[];
  attachments: IProlongationAttachments;
  groups: IProlongationGroups;
}
/** USED ON PROLONGATION */
export interface IProlongationGroups {
  [key: string]: IProlongationGroupValue[];
}
/** USED ON PROLONGATION */
export interface IProlongationGroupValue {
  [key: string]: string | boolean;
}
/** USED ON PROLONGATION */
export interface IProlongationCheckoutRequestPayload {
  uuid: string | null;
  channel: string;
  category: string;
  variantId: string;
  requestedAttributes: string[];
  marketingDiscount: boolean;
  profileId: string | null;
}
/** USED ON PROLONGATION */
export interface IProlongationProductDetailedPayload {
  category: string;
  segment: string;
  productId: string;
  changedAttribute: IChangedAttribute;
  variantId?: string;
}
/** USED ON PROLONGATION */
export interface IProlongationCheckoutResponse {
  data: IProlongationCheckoutProduct[];
}
/** USED ON PROLONGATION */
export interface IProlongationCheckoutProduct {
  name: string;
  characteristics: ICharacteristics[];
  optionValues: string[];
  prices: IPrices[];
  attachments: IProlongationAttachments;
}

/** USED ON PROLONGATION */
export interface IUserInfo {
  email: string;
  msisdn: string;
  firstName: string;
  lastName: string;
  billingAddress: string;
  categoryId: string;
  deliveryContact: string;
  segmentId: string;
  deliveryAddress: string;
  contactEmail: string;
}

/** USED IN PROLONGATION */
export interface IPayloadForPostUserInfotLead {
  email: string;
  msisdn: string;
  first_name: string;
  last_name: string;
  billing_address: string;
  category_id: string;
  contact_number: string;
  segment_id: string;
  delivery_address: string;
  contact_email: string;
  order_details: {
    orderDetails: {
      timestamp: number;
      upfrontPrice: string | number;
      recurringPrice: string | number;
    };
    variantDetails: {
      variantId: string;
      color: string;
      storage: string;
      plan: string;
      model: string;
      plan_code: string | null;
    };
  };
  consent: IProlongationConsent[];
}

export interface IProlongationConsent {
  name: string;
  status: string;
  isAccepted: boolean;
  subAgreements?: IProlongationSubConsent[];
}

export interface IProlongationSubConsent {
  name: string;
  status: string;
  isAccepted: boolean;
}

export interface IPostUserInfo {
  email: string;
  msisdn: string;
  firstName: string;
  lastName: string;
  billingAddress: string;
  categoryId: string;
  contactNumber: string;
}

export interface IProlongationPlaceOrderRequest {
  data: string;
  enc_key: string;
  rsa_key_label: string;
  uuid: string;
  category_id: string;
  segment_id: string;
  natco_code: string;
  profileId: string;
}

// export interface IGetLead {
//   first_name: string;
//   last_name: string;
//   email: string;
//   msisdn: string;
//   billing_address: string;
//   contact_email: string;
//   contact_number: string;
//   delivery_address: string;
//   order_details: {};
//   category_id: string;
//   segment_id: string;
// }

export interface IProlongationEventDetail {
  deviceId: string;
  deviceName: string;
  deviceUpfrontAmt: string;
  deviceMonthlyAmt: string;
  openedFrom?: string;
  pageUrl: string;
  gaCode: string;
  label: string;
  currencyCode: string;
  currencySymbol: string;
}

export interface Magenta {
  type: string;
  bundles: string[];
  partyId: string[];
}

export interface DigitalBanking {
  id: string;
  label: string;
  name: string;
}

export interface Individual {
  gender: string;
  birthDate: string;
  givenName: string;
  familyName: string;
}

export interface Characteristic {
  name: string;
  value: string;
}

export interface RelatedParty {
  id: string;
  role: string;
  name: string;
}

export interface Role {
  name: string;
}

export interface Medium {
  emailAddress: string;
  number: string;
}

export interface ContactMedium {
  type: string;
  role: Role;
  medium: Medium;
}

export interface ManageableAsset {
  id: string;
  segment: string;
  privilege: string;
  priority: string;
  category: string;
  name: string;
  label: string;
  description: string;
  enabled: boolean;
  relatedParties: any[];
  bundleId: string;
  userType: string;
}

export interface Bundle {
  id: string;
  privilege: string;
  name: string;
  label: string;
  description: string;
  type: string;
  enabled: boolean;
}

export interface IGetLead {
  magenta?: Magenta;
  digitalBanking?: DigitalBanking;
  userProfileRelatedPartyId?: string;
  id: string;
  status: string;
  detailedStatus?: string;
  individual: Individual;
  characteristics: Characteristic[];
  relatedParties?: RelatedParty[];
  contactMediums: ContactMedium[];
  manageableAssets?: ManageableAsset[];
  bundles?: Bundle[];
}

export interface ITokenResponse {
  access_token: string;
  expires_in: number;
  refresh_expires_in: number;
  refresh_token: string;
  token_type: string;
  id_token: string;
  session_state: string;
  scope: string;
}

export interface IMoengageEvent {
  eventId: string;
  customerId: string; // mandatory
  messageId: string;
  action: string;
  attributes: IMoengageAttribute[];
}

export interface IMoengageAttribute {
  name: string;
  value: string;
}

export interface IGAEvent {
  hitType: string;
  eventCategory: PROLONGATION_EVENT_NAME;
  eventAction: string;
  eventLabel?: string;
  eventValue: number;
  page?: string;
  url?: string;
  opened_from?: string;
  device_id?: string;
  device_name?: string;
  device_amt_upfront?: string;
  device_amt_monthly?: string;
  amt?: string;
  currencySymbol?: string;
  currencyCode?: string;
}

export interface IRSAKeyResponse {
  key: string;
  label: string;
}

export interface ITransformedConsentProlongation {
  name: string;
  description: string;
  isSelected: boolean;
  isMandatory: boolean;
  subConsent: ITransformedSubConsent[];
}

export interface ITransformedSubConsent {
  name: string;
  description: string;
  isSelected: boolean;
  isMandatory: boolean;
}

export interface IJSONLdRequest extends IProductListRequest {
  variantId: string | null;
}
