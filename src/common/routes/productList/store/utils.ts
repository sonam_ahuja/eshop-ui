import {
  ICatalogConfiguration,
  IProductListConfiguration,
  IProlongationConfig
} from '@src/common/store/types/configuration';
import { IProductListTranslation } from '@src/common/store/types/translation';
import { RootState } from '@src/common/store/reducers';
import APP_KEYS from '@common/constants/appkeys';

import {
  ICharacteristics,
  IFilter,
  IQueryParams,
  ISelectedFilters,
  ISortBy,
  ISortList
} from './types';
import { IFiltersRequest } from './services';

export const getState = (state: RootState) => state.productList;
export const getConfiguration = (
  state: RootState
): IProductListConfiguration => {
  return state.configuration.cms_configuration.modules.productList;
};
export const getTranslation = (state: RootState): IProductListTranslation => {
  return state.translation.cart.productList;
};

export const getCatalogConfiguration = (
  state: RootState
): ICatalogConfiguration => {
  return state.configuration.cms_configuration.modules.catalog;
};

export const getProlongationOpenedFrom = (state: RootState): string => {
  return state.productList.prolongationOpenedFrom;
};

export const getProlongationCmsConfiguration = (
  state: RootState
): IProlongationConfig => {
  return state.configuration.cms_configuration.modules.prolongation;
};

export const getFilterParamsFromQueryOrState = (
  sortBy: ISortBy | null,
  selectedFilters: ISelectedFilters,
  queryParams?: IQueryParams
): { paramFilters: IFiltersRequest[]; paramSortBy: string } => {
  let paramFilters = [];
  let paramSortBy = queryParams && queryParams.sortBy ? queryParams.sortBy : '';
  if (queryParams && queryParams.filters) {
    const filters = Object.keys(queryParams.filters);

    paramFilters = filters.map(filter => {
      const provideQuantity = true;

      return {
        name: filter,
        provideQuantity,
        characteristicValues: queryParams.filters[filter]
      };
    });
  } else {
    // for mobile case
    const filtersFromState = Object.keys(selectedFilters);

    paramFilters = filtersFromState.map(filterFromState => {
      const provideQuantity = true;

      return {
        name: filterFromState,
        provideQuantity,
        characteristicValues: selectedFilters[filterFromState].map(
          (item: IFilter) => {
            // for price range we are taking value instead of mobile
            if (filterFromState === APP_KEYS.BFF_PRICE_RANGE_ID) {
              return item.value;
            } else {
              return item.name;
            }
          }
        )
      };
    });
    if (sortBy && sortBy.id) {
      paramSortBy = sortBy.id;
    }
  }

  return { paramFilters, paramSortBy };
};

export const getCharacterstic = (
  characterstics: ICharacteristics[],
  key: string
) => {
  const characterstic = characterstics.find(charact => {
    return charact.name === key;
  });

  return characterstic && characterstic.values
    ? characterstic.values[0].value
    : '';
};

export const getSortByFromSortList = (
  sortListing: ISortList[],
  sortBy: string | null
) => {
  let selectedSortBy: ISortList | null = null;
  sortListing.forEach(sort => {
    if (sort.id === sortBy) {
      selectedSortBy = sort;
    }
  });

  return selectedSortBy;
};

export const getProductVariantIDFromURLForProlongationCheckout = (
  pathname: string
): { categorySlug: string; variantId: string } => {
  const arrPath = pathname.split('/');
  if (arrPath && arrPath[1] && (arrPath[3] || arrPath[4])) {
    return {
      categorySlug: arrPath[1],
      variantId: arrPath[4]
    };
  }

  return { categorySlug: '', variantId: '' };
};
