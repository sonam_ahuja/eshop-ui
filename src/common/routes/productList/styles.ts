import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { Column, Row } from '@common/components/Grid/styles';
import { StyledFooterPanel } from '@src/common/components/Footer/styles';

import { IMapStateToProps } from './types';
import { themeObj } from './theme';
import { StyledProductShell } from './index.shell';

export const StyledProductList = styled.div<IMapStateToProps>`
  background-color: ${props => {
    return themeObj[props.theme].backgroundColor;
  }};
  background-size: 100% auto;
  background-repeat: repeat;

  ${StyledProductShell} {
    padding: 1.25rem;
  }

  .productListContainer {
    padding: 1rem 1.25rem 3rem;
    /* ROW COL */
    ${Row} {
      margin: -0.125rem;

      ${Column} {
        padding: 0.125rem;
      }
    }
    .title {
      color: ${props => themeObj[props.theme].titles.color};
      /* font-size: 2.5rem;
      line-height: 2.75rem; */
      letter-spacing: -0.2px;
      margin-bottom: 0.25rem;
      margin-top: 1.5rem;
    }
    .description {
      color: ${props => themeObj[props.theme].descriptions.color};
      /* font-size: 0.75rem;
      line-height: 1rem; */
    }
    .spinner {
      i {
        font-size: 2rem;
      }
    }
    .selectPerPageWrap {
      .selectPerPage {
        background: ${props =>
          themeObj[props.theme].selectBackgroundColor.background};
        .styledSelect {
          color: ${props => themeObj[props.theme].selectColor.color};
          /* font-size: 0.75rem; */
        }
        .caret {
          path {
            fill: ${props => themeObj[props.theme].caretColor.color};
          }
        }
        .optionsList {
          color: ${props => themeObj[props.theme].dropColor.color};
          background: ${props =>
            themeObj[props.theme].dropBackground.background};
          border-color: ${props =>
            themeObj[props.theme].borderColor.borderColor};
          padding: 0;
          height: auto;
          left: 0;
          & > div {
            font-size: 0.7rem;
            line-height: 1rem;
            padding: 0.25rem 0.6rem;
            height: auto;
            font-weight: bold;
            margin-bottom: 0.4rem;
            &:last-child {
              margin-bottom: 0;
            }
            &:hover {
              background: ${props =>
                themeObj[props.theme].dropHoverBgColor.background};
            }
          }
        }
      }
      select {
        z-index: 0;
      }
    }
    .dt_button {
      z-index: 0;
    }
  }
  ${StyledFooterPanel} {
    width: 100%;
  }

  @media (max-width: ${breakpoints.mobile}px) {
    .productListContainer {
      .prev-btn {
        position: absolute;
        left: 0;
      }
      .next-btn {
        position: absolute;
        right: 0;
      }
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .productListContainer {
      padding: 1rem 2.25rem 2.75rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    position: relative;
    display: flex;
    flex: 1;

    .productListContainer {
      padding: 1rem 3rem 2.75rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .productListContainer {
      padding: 1rem 4.9rem 2.75rem;
      width: 100%;
      .title {
        font-size: 3.25rem;
        line-height: 3.5rem;
        margin-bottom: 0;
        letter-spacing: -0.25px;
        margin-top: 1.5rem;
      }
      .description {
        font-size: 0.875rem;
        line-height: 1.25rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .productListContainer {
      padding: 1rem 5.5rem 2.75rem;
    }
  }
`;

export const StyledContentWrapper = styled.div`
  width: 100%;
`;
