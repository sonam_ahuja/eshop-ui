import { colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const themeObj = {
  primary: {
    backgroundColor: colors.cloudGray,
    breadcrumb: {
      inactiveColor: colors.mediumGray,
      activeColor: colors.darkGray
    },
    titles: {
      color: colors.darkGray
    },
    descriptions: {
      color: colors.darkGray
    },
    selectColor: {
      color: colors.ironGray
    },
    selectBackgroundColor: {
      background: hexToRgbA(colors.white, 0.5)
    },
    caretColor: {
      color: colors.magenta
    },
    dropColor: {
      color: colors.ironGray
    },
    dropBackground: {
      background: colors.coldGray
    },
    borderColor: {
      borderColor: colors.coldGray
    },
    dropHoverBgColor: {
      background: colors.lightishGray
    },
    emptyState: {
      title: {
        color: colors.darkGray
      },
      subTitle: {
        color: colors.mediumGray
      },
      icon: {
        color: colors.darkGray
      }
    }
  },
  secondary: {
    backgroundColor: colors.shadowGray,
    breadcrumb: {
      inactiveColor: hexToRgbA(colors.white, 0.6),
      activeColor: hexToRgbA(colors.white, 1)
    },
    titles: {
      color: colors.white
    },
    descriptions: {
      color: colors.white
    },
    selectColor: {
      color: colors.white
    },
    selectBackgroundColor: {
      background: colors.transparent
    },
    caretColor: {
      color: colors.white
    },
    dropColor: {
      color: colors.white
    },
    dropBackground: {
      background: colors.shadowGray
    },
    borderColor: {
      borderColor: colors.shadowGray
    },
    dropHoverBgColor: {
      background: hexToRgbA(colors.black, 0.2)
    },
    emptyState: {
      title: {
        color: colors.white
      },
      subTitle: {
        color: hexToRgbA(colors.white, 0.7)
      },
      icon: {
        color: colors.white
      }
    }
  }
};
