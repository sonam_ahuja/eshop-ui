import {
  ICharacteristics,
  IError,
  IFilter,
  IFilterCharacteristic,
  IGetNotificationRequestTemplatePayload,
  ILastScrollPositionMap,
  INotificationData,
  IProductListItem,
  IQueryParams,
  ISelectedFilters,
  ISortBy,
  ISortList,
  IVariant,
  pageThemeType
} from '@productList/store/types';
import { CancelTokenSource } from 'axios';
import { IError as IGenricError } from '@common/store/types/common';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  ILoginFormRules,
  IProductListConfiguration
} from '@common/store/types/configuration';
import {
  IGlobalTranslation,
  IProductListTranslation
} from '@common/store/types/translation';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';
import { IGrid } from 'dt-components';

export interface ICommonModalProps {
  isOpen: boolean;
  deviceName: string;
  variantId: string;
  errorMessage: string | null;
  notificationLoading: boolean;
  modalType: MODAL_TYPE;
  translation: IProductListTranslation;
  notificationMediumValue: string;
  formConfiguration: ILoginFormRules;
  notificationMediumType: NOTIFICATION_MEDIUM.PHONE | NOTIFICATION_MEDIUM.EMAIL;
  openModal(isModalOpen: boolean): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
}

export interface IDesktopFilterProps {
  loading: boolean;
  filterCharacteristics: IFilterCharacteristic[];
  selectedFilters: ISelectedFilters;
  sortBy: ISortBy | null;
  sortByList: { id: string; title: string }[];
  translation: IProductListTranslation;
  setSelectedFilter(filterId: string, filter: IFilter): void;
  clearAllFilters(): void;
  setSortBy(sortBy: ISortBy): void;
}

export interface IMobileFilterProps {
  categoryId: string;
  filterCharacteristics: IFilterCharacteristic[];
  selectedFilters: ISelectedFilters;
  sortBy: ISortBy | null;
  resultCount: number;
  loading: boolean;
  translation: IProductListTranslation;
  sortByList: { id: string; title: string }[];
  setSelectedFilter(
    filterId: string,
    filter: IFilter,
    categoryId: string
  ): void;
  setSortBy(sortBy: ISortBy): void;
  setTempFilters(): void;
  setFiltersFromTempFilters(): void;
  submitFilters(): void;
}

export interface IMapStateToProps {
  commonError: IGenricError;
  filterCharacteristics: IFilterCharacteristic[];
  currency: ICurrencyConfiguration;
  productList: IProductListItem[];
  lastScrollPositionMap: ILastScrollPositionMap;
  loading: boolean;
  gridLoading: boolean;
  showAppShell: boolean;
  categoryId: string;
  categoryName: string;
  error: IError | null;
  backgroundImage: string;
  theme: pageThemeType;
  pagination: IGrid.IPagination;
  receivedProductListDataFromServer: boolean;
  selectedFilters: ISelectedFilters;
  tempSelectedFilters: ISelectedFilters;
  tempSortBy: ISortBy | null;
  translation: IProductListTranslation;
  globalTranslation: IGlobalTranslation;
  configuration: IProductListConfiguration;
  catalogConfiguration: ICatalogConfiguration;
  characteristics: ICharacteristics[];
  productListConfig: IProductListConfiguration;
  notificationModalType: MODAL_TYPE;
  notificationData: INotificationData;
  showModal: boolean;
  formConfiguration: ILoginFormRules;
  notificationLoading: boolean;
  currencyConfiguration: ICurrencyConfiguration;
  parentCategorySlug: string;
  sortListing: ISortList[];
  sortBy: ISortList | null;
  popularDevices: IVariant[];
  tariffId: string | null;
  tariffName: string | null;
  loyalty: string | null;
  discountId: string | null;
  plan: IVariant;
  installmentEnabled: boolean;
  loyality: string;
  installment: string | null;
  defaultSortBy: string | null;
}

export interface IMapDispatchToProps {
  loadMoreProductList(
    categoryId: string,
    isChanged: boolean,
    scrollAmount?: number,
    queryParams?: IQueryParams,
    pageNumber?: number,
    showFullPageError?: boolean,
    // showLoading?: boolean,
    onSuccessCallback?: () => void
  ): void;
  fetchProductList(
    currentPage: number,
    categoryId: string,
    queryParams?: IQueryParams,
    scrollAmount?: number,
    showFullPageError?: boolean,
    signal?: CancelTokenSource,
    onSuccessCallback?: () => void
  ): void;
  updateReceivedProductListDataFromServer(
    receivedProductListDataFromServer: boolean
  ): void;
  changeItemPerPage(itemPerPage: number): void;
  updateShowAppShell(showAppShell: boolean): void;
  setSelectedFilter(
    filterId: string,
    filter: IFilter,
    categoryId: string
  ): void;
  setSortBy(sortBy: ISortBy): void;
  clearAllFilters(categoryId: string): void;
  setFiltersFromTempFilters(): void;
  setTempFilters(): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  setError(error: IError): void;
  openModal(isModalOpen: boolean): void;
  setNotificationData(data: INotificationData): void;
  setSelectedCategory(categoryId: string): void;
  clearAllSelectedFilter(): void;
  resetData(): void;
  setScrollPosition(urlPath: string, position: number): void;
  clearScrollPosition(urlPath: string): void;
  setlastListURL(lastListURL: string): void;
  fetchPopularDevices(category: string): void;
  setDeviceDetailedList(list: string): void;
  setTariffLoyalty(
    tariffId: string | null,
    loyalty: string | null,
    discountId: string | null
  ): void;
}
