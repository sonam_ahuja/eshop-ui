import {
  computeSelectedFilters,
  createQueryParams,
  getDefaultSort,
  getSelectedFiltersFromParams,
  getSortList,
  getSortProductList,
  getTotalFiltersCount,
  getValueFromParams,
  isFilterSortByAndItemPerPageChanged
} from '@productList/utils';
import appState from '@store/states/app';
import {
  Filter,
  ProductListItem,
  SelectedFilter
} from '@mocks/productList/productList.mock';

describe('<ProductList utils />', () => {
  test('getSelectedFiltersFromParams test', () => {
    const result = getSelectedFiltersFromParams('query');
    expect(getSelectedFiltersFromParams('query')).toEqual(result);
  });

  test('createQueryParams test', () => {
    const result = createQueryParams(
      SelectedFilter,
      '1',
      2,
      null,
      null,
      null,
      1
    );
    expect(
      createQueryParams(SelectedFilter, '1', 2, null, null, null, 1)
    ).toEqual(result);
  });

  test('computeSelectedFilters test', () => {
    const result = computeSelectedFilters(SelectedFilter, '1', Filter[0]);
    expect(computeSelectedFilters(SelectedFilter, '1', Filter[0])).toEqual(
      result
    );
  });

  test('getSortProductList test', () => {
    const result = getSortProductList([ProductListItem], false);
    expect(getSortProductList([ProductListItem], false)).toEqual(result);
  });

  test('getValueFromParams test', () => {
    const result = getValueFromParams('key', 'value');
    expect(getValueFromParams('key', 'value')).toEqual(result);
  });

  test('getSortList test', () => {
    const result = getSortList(
      [{ name: 'key', values: [{ value: 'value1' }] }],
      appState().translation.cart.productList
    );
    expect(
      getSortList(
        [{ name: 'key', values: [{ value: 'value1' }] }],
        appState().translation.cart.productList
      )
    ).toEqual(result);
  });

  test('getDefaultSort test', () => {
    const result = getDefaultSort(
      [{ name: 'key', values: [{ value: 'value1' }] }],
      appState().translation.cart.productList
    );
    expect(
      getDefaultSort(
        [{ name: 'key', values: [{ value: 'value1' }] }],
        appState().translation.cart.productList
      )
    ).toEqual(result);
  });

  test('isFilterSortByAndItemPerPageChanged test', () => {
    const result = isFilterSortByAndItemPerPageChanged(
      'query',
      'query1',
      null,
      0
    );
    expect(
      isFilterSortByAndItemPerPageChanged('query', 'query1', null, 0)
    ).toEqual(result);
  });

  test('isFilterSortByAndItemPerPageChanged test', () => {
    const result = getTotalFiltersCount(SelectedFilter);
    expect(getTotalFiltersCount(SelectedFilter)).toEqual(result);
  });
});
