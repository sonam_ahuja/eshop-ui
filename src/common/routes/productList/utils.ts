import { IProductListTranslation } from '@src/common/store/types/translation';
import {
  ICharacteristics,
  IFilter,
  IFilterCharacteristic,
  ILabelValue,
  IProductListItem,
  IQueryParamFilter,
  IQueryParams,
  ISelectedFilters,
  ISortList
} from '@productList/store/types';
import { getOutOfStock } from '@productList/utils/getProductDetail';
import appKeys from '@src/common/constants/appkeys';
import APP_KEYS from '@common/constants/appkeys';
import { getCurrencyLocale, getCurrencySymbol } from '@common/utils/currency';

export const getValueFromParams = (
  query: string,
  key: string
): string | null => {
  const decodedParams = decodeURIComponent(query);
  const params = new URLSearchParams(decodedParams);

  return params.get(key);
};

export const isFilterSortByAndItemPerPageChanged = (
  query: string,
  prevQuery: string,
  defaultSortBy: string | null,
  defaultItemPerPage: number
) => {
  const decodedParams = decodeURIComponent(query);
  const prevDecodedParams = decodeURIComponent(prevQuery);
  const params = new URLSearchParams(decodedParams);
  const prevParams = new URLSearchParams(prevDecodedParams);
  const filters = params.getAll('filter[]');
  const prevFilters = prevParams.getAll('filter[]');
  const sortBy = params.get('sortBy') ? params.get('sortBy') : defaultSortBy;
  const prevSortBy = prevParams.get('sortBy')
    ? prevParams.get('sortBy')
    : defaultSortBy;
  const itemPerPage = params.get('itemPerPage')
    ? params.get('itemPerPage')
    : defaultItemPerPage;
  const prevItemPerPage = prevParams.get('itemPerPage')
    ? prevParams.get('itemPerPage')
    : defaultItemPerPage;
  if (filters.length !== prevFilters.length) {
    return true;
  }
  for (let idx = 0; idx < filters.length; idx++) {
    if (filters[idx] !== prevFilters[idx]) {
      return true;
    }
  }
  // sortBy is added in url even if it is not changed in
  if (sortBy !== prevSortBy) {
    return true;
  }
  if (String(itemPerPage) !== String(prevItemPerPage)) {
    return true;
  }

  return false;
};

export const getSelectedFiltersFromParams = (
  query: string
): IQueryParams & {
  currentPage: number;
  // tariff related params
  tariffId: string | null;
  loyalty: string | null;
  discountId: string | null;
} => {
  const filters: IQueryParamFilter = {};
  const decodedParams = decodeURIComponent(query);
  const params = new URLSearchParams(decodedParams);
  params.getAll('filter[]').forEach(filterItem => {
    const filterItemArrPair = filterItem.split('=');
    if (filterItemArrPair.length > 1) {
      const filterId = filterItemArrPair[0].replace(
        /(\[|\]|\.|filter)/g,
        () => ''
      );
      filters[filterId] = [];
      if (filterId) {
        filterItemArrPair[1].split(',').forEach(filterValue => {
          if (filterValue) {
            filters[filterId].push(filterValue);
          }
        });
      }
    }
  });

  const currentPageParam = params.get('currentPage');
  let currentPage = 1;
  if (currentPageParam && !isNaN(+currentPageParam)) {
    currentPage = +currentPageParam;
  }

  return {
    filters,
    sortBy: params.get('sortBy'),
    currentPage,
    tariffId: params.get('tariffId'),
    loyalty: params.get('loyalty'),
    discountId: params.get('discountId'),
    itemPerPage: params.get('itemPerPage')
  };
};

export const getQueryParamString = (
  paramName: string,
  param: string | null
) => {
  return param !== null ? `&${paramName}=${param}` : '';
};

export const createQueryParams = (
  selectedFilters: ISelectedFilters,
  sortById: string | null,
  currentPage: number,
  tariffId: string | null,
  loyalty: string | null,
  discountId: string | null,
  itemPerPage: number | null
): string => {
  const filterValues = Object.keys(selectedFilters)
    .map(
      key =>
        `filter[]=filter.${key}[]=${selectedFilters[key]
          .map(f => {
            if (key === APP_KEYS.BFF_PRICE_RANGE_ID) {
              return f.value;
            } else {
              return f.name;
            }
          })
          .join(',')}`
    )
    .join('&');
  const sortByStr = getQueryParamString('sortBy', sortById);
  const tariffIdStr = getQueryParamString('tariffId', tariffId);
  const loyaltyStr = getQueryParamString('loyalty', loyalty);
  const discountIdStr = getQueryParamString('discountId', discountId);
  const itemPerPageStr = getQueryParamString(
    'itemPerPage',
    String(itemPerPage)
  );

  return encodeURIComponent(
    `${filterValues}${sortByStr}&currentPage=${currentPage}${tariffIdStr}${loyaltyStr}${discountIdStr}${itemPerPageStr}`
  );
};

export const computeSelectedFilters = (
  selectedFilters: ISelectedFilters,
  filterId: string,
  filter: IFilter
) => {
  const updatedFilters: ISelectedFilters = {};
  Object.keys(selectedFilters).forEach(key => {
    updatedFilters[key] = [...selectedFilters[key]];
  });
  if (!updatedFilters[filterId]) {
    updatedFilters[filterId] = [];
    updatedFilters[filterId].push(filter);
  } else {
    const idx = updatedFilters[filterId].findIndex(
      selectedFilter => selectedFilter.value === filter.value
    );
    if (idx !== -1) {
      updatedFilters[filterId].splice(idx, 1);
    } else {
      updatedFilters[filterId].push(filter);
    }
  }

  return updatedFilters;
};

export const getSortProductList = (
  productList: IProductListItem[],
  considerInventory: boolean
): IProductListItem[] => {
  return productList.sort(
    (product1: IProductListItem, product2: IProductListItem) => {
      return getOutOfStock(
        product1.variant.unavailabilityReasonCodes,
        considerInventory
      ) ===
        getOutOfStock(
          product2.variant.unavailabilityReasonCodes,
          considerInventory
        )
        ? 0
        : getOutOfStock(
            product1.variant.unavailabilityReasonCodes,
            considerInventory
          )
        ? 1
        : -1;
    }
  );
};

export const getSortList = (
  characteristics: ICharacteristics[],
  translation: IProductListTranslation
): ISortList[] => {
  const sortByList: ISortList[] = [];

  if (characteristics) {
    characteristics.forEach((characteristic: ICharacteristics) => {
      if (characteristic.name === appKeys.SORTING_LIST) {
        characteristic.values.forEach((sortValue: ILabelValue) => {
          sortByList.push({
            title: translation[sortValue.value],
            id: sortValue.value
          });
        });
      }
    });
  }

  return sortByList;
};

export const getDefaultSort = (
  characteristics: ICharacteristics[],
  translation: IProductListTranslation
): ISortList => {
  const sortBy: ISortList = { id: '', title: '' };

  if (characteristics) {
    characteristics.forEach((characteristic: ICharacteristics) => {
      if (characteristic.name === appKeys.DEFAULT_SORT) {
        characteristic.values.forEach((sortValue: ILabelValue) => {
          sortBy.title = translation[sortValue.value];
          sortBy.id = sortValue.value;
        });
      }
    });
  }

  return sortBy;
};

export const isUpdated = (
  search: string,
  {
    selectedFilters,
    sortBy,
    itemPerPage,
    pageNumber,
    tariffId,
    loyalty,
    discountId
  }: {
    selectedFilters: ISelectedFilters;
    sortBy: string | null; // default sortBy if null
    itemPerPage: number;
    pageNumber: number;
    tariffId: string | null;
    loyalty: string | null;
    discountId: string | null;
  }
) => {
  const decodedParams = decodeURIComponent(search);
  const params = new URLSearchParams(decodedParams);
  const urlSortBy = params.get('sortBy') || sortBy;
  const urlItemPerPage = params.get('itemPerPage') || String(itemPerPage);
  const urlPageNumber = params.get('currentPage') || '1';
  const urlTariffId = params.get('tariffId');
  const urlLoyalty = params.get('loyalty');
  const urlDiscountId = params.get('discountId');
  if (
    urlSortBy !== sortBy ||
    urlItemPerPage !== String(itemPerPage) ||
    urlPageNumber !== String(pageNumber) ||
    urlTariffId !== tariffId ||
    urlLoyalty !== loyalty ||
    urlDiscountId !== discountId
  ) {
    return true;
  }
  const filters: IQueryParamFilter = {};
  // tslint:disable-next-line: no-identical-functions
  params.getAll('filter[]').forEach(filterItem => {
    const filterItemArrPair = filterItem.split('=');
    if (filterItemArrPair.length > 1) {
      const filterId = filterItemArrPair[0].replace(
        /(\[|\]|\.|filter)/g,
        () => ''
      );
      filters[filterId] = [];
      if (filterId) {
        // tslint:disable-next-line: no-identical-functions
        filterItemArrPair[1].split(',').forEach(filterValue => {
          if (filterValue) {
            filters[filterId].push(filterValue);
          }
        });
      }
    }
  });
  const selectedFilterKeys = Object.keys(selectedFilters);
  const filterKeys = Object.keys(filters);
  if (selectedFilterKeys.length !== filterKeys.length) {
    return true;
  }
  for (let idx = 0; idx < selectedFilterKeys.length; idx++) {
    if (!filterKeys[idx]) {
      return true;
    }
    for (let idx1 = 0; idx1 < selectedFilterKeys[idx].length; idx1++) {
      if (selectedFilterKeys[idx][idx1] !== filterKeys[idx][idx1]) {
        return true;
      }
    }
  }

  return false;
};

export const getTotalFiltersCount = (selectedFilters: ISelectedFilters) => {
  let count = 0;
  Object.keys(selectedFilters).forEach(key => {
    count += selectedFilters[key].length;
  });

  return count;
};

export const addPriceCurrency = (
  filterCharacteristics: IFilterCharacteristic[]
) => {
  if (filterCharacteristics && filterCharacteristics.length) {
    filterCharacteristics.forEach(
      (filterCharacteristic: IFilterCharacteristic) => {
        if (
          filterCharacteristic.id === APP_KEYS.BFF_PRICE_RANGE_ID &&
          filterCharacteristic.data
        ) {
          filterCharacteristic.data = filterCharacteristic.data.map(
            (filter: IFilter) => {
              return {
                ...filter,
                name: `${filter.name} ${getCurrencySymbol(getCurrencyLocale())}`
              };
            }
          );
        }
      }
    );
  }

  return filterCharacteristics;
};

export const addCurrencyCodeInPriceRange = (
  price: string,
  aboveTranslation: string
) => {
  if (price) {
    const currencySymbol = getCurrencySymbol(getCurrencyLocale());
    const arrPrice = price.split('-');
    if (price.includes('*')) {
      return `${arrPrice[0]} ${currencySymbol} - ${aboveTranslation}`;
    } else {
      if (arrPrice && arrPrice.length && arrPrice[0] && arrPrice[1]) {
        return `${arrPrice[0]} ${currencySymbol} - ${
          arrPrice[1]
        } ${currencySymbol}`;
      }
    }
  }

  return price;
  // tslint:disable-next-line: max-file-line-count
};
