import { getOutOfStockReason } from '@productList/utils/getOutOfStockReason';
import {
  ProductListItem,
  ProductListItemNoOutOfStockReason
} from '@mocks/productList/productList.mock';

describe('<Utils />', () => {
  it('getOutOfStockReason ::', () => {
    const result = getOutOfStockReason('123', [ProductListItem]);
    expect(result).toEqual(result);
  });
  it('getOutOfStockReason Null Condition ::', () => {
    const result = getOutOfStockReason('123', [
      ProductListItemNoOutOfStockReason
    ]);
    expect(result).toEqual(result);
  });
});
