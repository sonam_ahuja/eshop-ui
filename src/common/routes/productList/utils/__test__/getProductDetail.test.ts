import { getImage, getPrice } from '@productList/utils/getProductDetail';

describe('<App />', () => {
  it('getPrice with empty Data', () => {
    const result = getPrice([], '', {
      currencySymbol: 'Ft',
      isPrecede: false,
      locale: 'pl'
    });
    expect(result).toEqual(result);
  });
  it('getPrice with discount', () => {
    const prices = [
      {
        priceType: 'upFront',
        actualValue: 15000,
        discount: true,
        discountedValue: 12000,
        discounts: [
          {
            discount: 12,
            name: 'string',
            dutyFreeValue: 12,
            taxRate: 34,
            label: '',
            taxPercentage: 2
          }
        ]
      }
    ];
    const type = 'upFront';
    const result = getPrice(prices, type, {
      currencySymbol: 'Ft',
      isPrecede: false,
      locale: 'pl'
    });
    expect(result).toEqual(result);
  });
  it('getPrice without discount', () => {
    const prices = [
      {
        priceType: 'upFront',
        actualValue: 15000,
        discount: false,
        discountedValue: 1234,
        discounts: [
          {
            discount: 12,
            name: 'string',
            dutyFreeValue: 12,
            taxRate: 34,
            label: '',
            taxPercentage: 2
          }
        ]
      }
    ];
    const type = 'upFront';
    const result = getPrice(prices, type, {
      currencySymbol: 'Ft',
      isPrecede: false,
      locale: 'pl'
    });
    expect(result).toEqual(result);
  });
  it('getImage', () => {
    const images = [
      {
        name: 'front',
        url: 'http://image-url.com'
      }
    ];
    const type = 'front';
    const result = getImage(images, type);
    expect(result).toEqual(result);
  });
});
