import {
  getProlongaitionProductDetailsRequest,
  getProlongaitionProductDetailsRequestTemplate
} from '@productList/utils/requestTemplate';

describe('<requestTemplate />', () => {
  it('getProlongaitionProductDetailsRequest', () => {
    const result = getProlongaitionProductDetailsRequest();
    expect(getProlongaitionProductDetailsRequest()).toEqual(result);
  });
  it('getProlongaitionProductDetailsRequestTemplate', () => {
    const result = getProlongaitionProductDetailsRequestTemplate(
      'uuid',
      'productId',
      'category',
      'segment',
      'variantId',
      [],
      []
    );
    expect(
      getProlongaitionProductDetailsRequestTemplate(
        'uuid',
        'productId',
        'category',
        'segment',
        'variantId',
        [],
        []
      )
    ).toEqual(result);
  });
});
