import { checkPhoneNumberValidation } from '@routes/productList/utils/validation';
import appState from '@store/states/app';

describe('<Validation />', () => {
  const loginFormRules = appState().configuration.cms_configuration.global
    .loginFormRules;
  test('checkPhoneNumberValidation test', () => {
    expect(
      checkPhoneNumberValidation(loginFormRules, '9876543221')
    ).toBeDefined();

    loginFormRules.phoneNumber.regex = '/asa';
    expect(checkPhoneNumberValidation(loginFormRules, '987654')).toBeDefined();
  });
});
