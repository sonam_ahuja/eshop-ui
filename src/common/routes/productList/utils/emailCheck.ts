import { VALIDATIONS } from '@src/common/constants/appConstants';

export const isEmail = (enteredValue: string): boolean =>
  VALIDATIONS.email.test(enteredValue);
