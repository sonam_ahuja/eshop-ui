const getLocation = (locationName: string) => locationName.split('/');

export const getCategorySlug = (locationName: string): string => {
  const url = getLocation(locationName);

  return url[1];
};

// tslint:disable-next-line:no-identical-functions
// export const getBaseCategorySlug = (locationName: string): string => {
//   const url = getLocation(locationName);
//   return url[1];
// };
