import {
  eligibilityUnavailabilityReasonType,
  IImageURL,
  IKeyValue
} from '@productList/store/types';
import { ITEM_STATUS } from '@common/store/enums';
import { formatCurrency } from '@src/common/utils/currency';
import { IPrices } from '@tariff/store/types';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';
import htmlKeys from '@common/constants/appkeys';
import { IProductListTranslation } from '@store/types/translation';

export const getPriceType = (prices: IPrices[], type: string) => {
  return prices.filter((price: IPrices) => {
    return price.priceType === type;
  });
};

export const getPrice = (
  prices: IPrices[],
  type: string,
  currency: ICurrencyConfiguration
): string | undefined => {
  const arrPrice = getPriceType(prices, type);
  if (arrPrice.length) {
    if (arrPrice[0].priceType === type && arrPrice[0].discounts) {
      const discountedValue = Number(arrPrice[0].discountedValue);
      if (!isNaN(discountedValue)) {
        return formatCurrency(discountedValue, currency.locale);
      }
    } else if (arrPrice[0].priceType === type && !arrPrice[0].discounts) {
      const actualValue = Number(arrPrice[0].actualValue);
      if (!isNaN(actualValue)) {
        return formatCurrency(actualValue, currency.locale);
      }
    }
  }

  return undefined;
};

/** USED ON PROLONGATION */
export const getImage = (images: IImageURL[], imageType: string): string => {
  const arrFront =
    images &&
    images.filter(image => {
      return image.name === imageType;
    });

  return arrFront && arrFront[0] ? arrFront[0].url : '';
};

export const getOutOfStock = (
  eligibilityUnavailabilityReason: eligibilityUnavailabilityReasonType[],
  considerInventory: boolean
): boolean => {
  if (eligibilityUnavailabilityReason.includes(ITEM_STATUS.PRE_ORDER)) {
    if (
      eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK) &&
      considerInventory
    ) {
      return true;
    } else {
      return false;
    }
  } else if (
    eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK)
  ) {
    return true;
  }

  return false;
};

export const getSpecialOffer = (characteristics: IKeyValue[]) => {
  if (!characteristics) {
    return false;
  }

  const index = characteristics.findIndex((characteristic: IKeyValue) => {
    return characteristic.key === htmlKeys.BEST_DEVICE_OFFER;
  });

  return index !== -1;
};

export const getCTAText = (
  eligibilityUnavailabilityReason: eligibilityUnavailabilityReasonType[],
  considerInventory: boolean,
  translation: IProductListTranslation
) => {
  let buttonText = translation.viewDetails;
  if (eligibilityUnavailabilityReason.includes(ITEM_STATUS.PRE_ORDER)) {
    buttonText =
      eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK) &&
      considerInventory
        ? translation.outOfStock
        : translation.preOrder;
  } else if (
    eligibilityUnavailabilityReason.includes(ITEM_STATUS.OUT_OF_STOCK)
  ) {
    buttonText = translation.outOfStock;
  }

  return buttonText;
};
