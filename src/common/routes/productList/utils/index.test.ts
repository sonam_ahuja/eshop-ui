import store from '@common/store';

import { isEmail } from './emailCheck';
import { getCategorySlug } from './getCategorySlug';
import {
  getImage,
  getOutOfStock,
  getPrice,
  getPriceType
} from './getProductDetail';
import { isPhoneNumber } from './phonerNumberCheck';
import {} from './requestTemplate';
import { checkPhoneNumberValidation } from './validation';

const conf = store.getState().configuration.cms_configuration.global
  .loginFormRules;

describe('<ProductList utils />', () => {
  test('getSelectedFiltersFromParams test', () => {
    isEmail('asd@gmail.com');
  });

  test('createQueryParams test', () => {
    getCategorySlug('ggn');
  });

  test('computeSelectedFilters test', () => {
    getImage([{ name: '', url: '' }, { name: '', url: '' }], 'png');
    getOutOfStock([], false);
    getPrice(
      [
        {
          priceType: 'upFront',
          actualValue: 15000,
          discountedValue: 12000,
          discounts: [
            {
              discount: 12,
              name: 'string',
              dutyFreeValue: 12,
              taxRate: 34,
              label: '',
              taxPercentage: 2
            }
          ]
        }
      ],
      'sdf',
      { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' }
    );
    getPriceType(
      [
        {
          priceType: 'upFront',
          actualValue: 15000,
          discountedValue: 12000,
          discounts: [
            {
              discount: 12,
              name: 'string',
              dutyFreeValue: 12,
              taxRate: 34,
              label: '',
              taxPercentage: 2
            }
          ]
        }
      ],
      'sdf'
    );
  });

  test('getSortProductList test', () => {
    isPhoneNumber('99999999');
  });

  test('getSortProductList test', () => {
    checkPhoneNumberValidation(conf, 'enter ');
  });
});
