import { VALIDATIONS } from '@src/common/constants/appConstants';

export const isPhoneNumber = (enteredValue: string): boolean =>
  VALIDATIONS.phone.test(enteredValue);
