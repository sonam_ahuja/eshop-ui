import {
  IChangedAttribute,
  ICondition,
  IFetchProlongationProductListPayload,
  IGetNotificationRequestTemplatePayload,
  IItemPerPageOptions,
  IProductListRequest
} from '@productList/store/types';
import * as sendStockNotificationApi from '@common/types/api/sendStockNotification';
import {
  ENTITY_REFERRED_TYPE,
  NOTIFICATION_MEDIUM,
  PRODUCT_ATTRIBUTE
} from '@productList/store/enum';
import APP_CONSTANTS from '@common/constants/appConstants';
import APP_KEYS from '@common/constants/appkeys';

import { IFiltersRequest } from '../store/services';

export const getProlongaitionProductDetailsRequest = (): IFetchProlongationProductListPayload => {
  return {
    uuid: '',
    channel: APP_CONSTANTS.CHANNEL,
    category: '',
    profileId: '',
    segment: '',
    productId: '',
    checkHistoricalLead: false,
    variantId: null,
    changedAttributes: [],
    selectedAttributes: [],
    requestedAttributes: [
      PRODUCT_ATTRIBUTE.STORAGE,
      PRODUCT_ATTRIBUTE.COLOUR,
      PRODUCT_ATTRIBUTE.PLAN,
      PRODUCT_ATTRIBUTE.RAM
    ]
  };
};

export const getProlongaitionProductDetailsRequestTemplate = (
  uuid: string,
  productId: string,
  category: string,
  segment: string,
  variantId?: string,
  changedAttributes?: IChangedAttribute[],
  selectedAttributes?: IChangedAttribute[]
): IFetchProlongationProductListPayload => {
  const requestBody = getProlongaitionProductDetailsRequest();
  requestBody.uuid = uuid;
  requestBody.productId = productId;
  requestBody.category = category;
  requestBody.segment = segment;
  requestBody.variantId = variantId ? variantId : requestBody.variantId;
  requestBody.changedAttributes = changedAttributes;
  requestBody.selectedAttributes = selectedAttributes;

  return requestBody;
};

export const getProductListRequest = (): IProductListRequest => {
  return {
    channel: APP_CONSTANTS.CHANNEL,
    page: 0,
    itemPerPage: 100,
    filterCharacteristics: [],
    categories: [],
    conditions: []
  };
};

const sendStockNotificationtRequest = (): sendStockNotificationApi.POST.IRequest => {
  return {
    channel: {
      id: APP_CONSTANTS.CHANNEL
    },
    reason: '',
    description: '',
    contacts: [],
    interactionItems: []
  };
};

export const getProductListingRequestTemplate = (
  page: number,
  itemPerPage: number,
  categoryId: string,
  filterCharacteristics: IFiltersRequest[],
  tariffId: string | null,
  loyalty: string | null,
  discountId: string | null
) => {
  const conditions: ICondition[] = [];
  const requestBody = getProductListRequest();
  requestBody.page = page;
  requestBody.itemPerPage = itemPerPage;
  requestBody.categories = [];
  requestBody.categories.push({
    id: categoryId,
    characteristics: []
  });
  Array.prototype.push.apply(
    requestBody.filterCharacteristics,
    filterCharacteristics.map(filterCharacterstic => {
      return {
        id: filterCharacterstic.name,
        characteristicValues: filterCharacterstic.characteristicValues
      };
    })
  );
  if (tariffId !== null) {
    requestBody.categories[0].characteristics.push({
      key: APP_KEYS.PRODUCT_LISTING_SELECTED_TARIFF,
      value: tariffId
    });
  }
  if (loyalty !== null) {
    requestBody.categories[0].characteristics.push({
      key: APP_KEYS.PRODUCT_LISTING_SELECTED_AGREEMENT,
      value: loyalty
    });
  }
  if (discountId !== null) {
    requestBody.categories[0].characteristics.push({
      key: APP_KEYS.PRODUCT_LISTING_SELECTED_DISCOUNT,
      value: discountId
    });
  }
  Array.prototype.push.apply(requestBody.conditions, conditions);

  return requestBody;
};

export const getNotificationRequestTemplate = (
  payload: IGetNotificationRequestTemplatePayload,
  reason: string
) => {
  const requestBody = sendStockNotificationtRequest();
  requestBody.reason = reason;

  switch (payload.type) {
    case NOTIFICATION_MEDIUM.EMAIL:
      requestBody.contacts.push({
        type: payload.type,
        medium: {
          emailAddress: payload.mediumValue
        },
        validated: true
      });
      break;
    case NOTIFICATION_MEDIUM.PHONE:
      requestBody.contacts.push({
        type: payload.type,
        medium: {
          number: payload.mediumValue
        },
        validated: true
      });
      break;

    default:
      break;
  }

  requestBody.interactionItems.push({
    item: {
      id: payload.variantId,
      entityReferredType: ENTITY_REFERRED_TYPE.PRODUCT_OFFERING
    }
  });

  return requestBody;
};

export const getItemPerPage = (
  perPageTranslation: string,
  // tslint:disable-next-line:no-any
  itemPerPageList: any
): IItemPerPageOptions[] => {
  let itemPerPageSelection: string[] = [];
  if (
    typeof itemPerPageList === 'string' ||
    itemPerPageList instanceof String
  ) {
    itemPerPageSelection = itemPerPageList.split(',');
  } else {
    itemPerPageList = itemPerPageList.toString();
    itemPerPageSelection.push(itemPerPageList);
  }

  return itemPerPageSelection.map(item => {
    return {
      title: `${item} ${perPageTranslation}`,
      id: Number(item.trim())
    };
  });
};
