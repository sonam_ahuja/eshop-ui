import { ILoginFormRules } from '@src/common/store/types/configuration';
import { isFormFieldValidAsPerType } from '@src/common/routes/checkout/CheckoutSteps/PersonalInfo/utils';
import { VALIDATION_TYPE } from '@src/common/types/modules/common';

export const checkPhoneNumberValidation = (
  configuration: ILoginFormRules,
  enteredValue: string
): boolean => {
  let regexValidation = true;
  let phoneValidation = true;
  if (configuration.phoneNumber.regex) {
    regexValidation = isFormFieldValidAsPerType(
      configuration.phoneNumber.regex,
      enteredValue,
      VALIDATION_TYPE.REGEX
    );
  }

  phoneValidation =
    regexValidation &&
    isFormFieldValidAsPerType(
      configuration.phoneNumber.minLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MIN
    ) &&
    isFormFieldValidAsPerType(
      configuration.phoneNumber.maxLength.toString(),
      enteredValue,
      VALIDATION_TYPE.MAX
    );

  return phoneValidation;
};
