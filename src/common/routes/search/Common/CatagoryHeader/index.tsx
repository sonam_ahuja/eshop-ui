import React, { FunctionComponent } from 'react';
import { Label, Title } from 'dt-components';
import { changeCategory } from '@search/utils/productListing';
import { sendCTAClicks } from '@events/index';
import { StyledCatagoryHeader } from '@search/Common/CatagoryHeader/styles';
export interface IProps {
  title: string;
  viewALL: string;
  isViewAllNeeded: boolean;
  updateShowAppShell(showAppShell: boolean): void;
}

const CatagoryHeader: FunctionComponent<IProps> = (props: IProps) => {
  const { viewALL, isViewAllNeeded, title, updateShowAppShell } = props;
  const onClick = (event: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
    if (event) {
      sendCTAClicks(viewALL, window.location.href);
      changeCategory();
      updateShowAppShell(true);
    }
  };

  return (
    <StyledCatagoryHeader>
      <Title tag='h2' className='header-title' size='xlarge' weight='ultra'>
        {title}
      </Title>
      {isViewAllNeeded ? (
        <div onClick={onClick}>
          <Label className='header-label'>{viewALL}</Label>
        </div>
      ) : null}
    </StyledCatagoryHeader>
  );
};

export default CatagoryHeader;
