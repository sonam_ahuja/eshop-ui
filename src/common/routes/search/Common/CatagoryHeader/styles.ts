import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledCatagoryHeader = styled.div`
  display: flex;
  flex-direction: 'row';
  flex-wrap: nowrap;
  align-items: flex-end;
  justify-content: space-between;
  color: ${colors.darkGray};
  padding-bottom: 1.25rem;
  margin-top: 0.75rem;
  .header-title {
    max-width: 13.25rem;
  }

  .header-label {
    font-size: 14px;
    padding-bottom: 5px;
    line-height: 1.43;
    color: ${colors.magenta};
  }

  &:first-child {
    padding-top: 2.5rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 0;
  }
`;
