import React, { FunctionComponent } from 'react';
import { LabelWithSuper } from 'dt-components';
import { ISearchTranslation } from '@store/types/translation';
import { onCategorySelect } from '@search/utils/productListing';
import { SEARCH_ROUTE } from '@search/store/enum';
import { isMobile } from '@common/utils';
import { StyledHorizontalScrollRow } from '@src/common/components/styles';
import { sendCTAClicks } from '@events/index';

import { StyledFilters, StyledFilterText, StyledFiterWrap } from './styles';
export interface IProps {
  total: number;
  searchTranslation: ISearchTranslation;
  type?: string;
  isSticky: boolean;
  query: string;
  updateShowAppShell(showAppShell: boolean): void;
}

const Filters: FunctionComponent<IProps> = (props: IProps) => {
  const { total, query, type, updateShowAppShell, isSticky } = props;
  const { filters, filterInBetweenMessageText } = props.searchTranslation;
  const onAllClick = (categoryName: string) => {
    if (type === SEARCH_ROUTE.SEARCH_RESULT && categoryName === 'Devices') {
      onCategorySelect(categoryName);
      updateShowAppShell(true);
    }
    if (type === undefined && categoryName === 'all') {
      onCategorySelect(categoryName);
      updateShowAppShell(true);
    }
  };

  const onClickAll = () => {
    sendCTAClicks(filters.all, window.location.href);
    onAllClick('all');
  };

  const onClickDevices = () => {
    sendCTAClicks(filters.devices, window.location.href);
    onAllClick('Devices');
  };

  const trimMessageQuery = (
    searchQuery: string
  ): { firstQuery: string; secondQuery: string } => {
    const newString = { firstQuery: '', secondQuery: '' };
    if (searchQuery.length > 20) {
      const newLength = searchQuery.length;
      const newFirstString = `“${searchQuery.slice(0, 20)}`;
      newString.firstQuery = newFirstString;
      const newSecondString = `${searchQuery.slice(20, newLength)}`;
      newString.secondQuery = `${newSecondString}”`;
      if (newSecondString.length > 36) {
        newString.secondQuery = `${newSecondString.slice(0, 31)}...”`;
      }
    } else {
      newString.firstQuery = `“${searchQuery}”`;
    }

    return newString;
  };

  const { firstQuery, secondQuery } = trimMessageQuery(query);

  return (
    <StyledFiterWrap isSticky={isSticky}>
      {!isMobile.phone ? (
        <StyledFilterText tag='h1'>
          {total} {filterInBetweenMessageText} {firstQuery}
          <div>{secondQuery}</div>
        </StyledFilterText>
      ) : null}

      <StyledFilters>
        <StyledHorizontalScrollRow>
          <div className='horizontalScrollRowInner'>
            <LabelWithSuper
              className={type ? 'active' : ''}
              label={filters.all}
              superText={`${total}`}
              onClick={onClickAll}
            />
            <LabelWithSuper
              className={type ? '' : 'active'}
              label={filters.devices}
              superText={`${total}`}
              onClick={onClickDevices}
            />
          </div>
        </StyledHorizontalScrollRow>
      </StyledFilters>
    </StyledFiterWrap>
  );
};

export default Filters;
