import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';
import { Paragraph } from 'dt-components';
import { StyledHorizontalScrollRow } from '@src/common/components/styles';

export const StyledFilterText = styled(Paragraph).attrs({
  size: 'small',
  weight: 'medium'
})`
  color: ${colors.darkGray};
  align-self: center;
  flex-shrink: 0;
  margin-right: 3rem;

  width: 15rem;
  max-height: 66%;
  overflow: hidden;
`;

export const StyledFilters = styled.div`
  display: flex;
  flex-wrap: nowrap;
  max-width: 100%;
  color: ${colors.mediumGray};

  .horizontalScrollRowInner {
    /* TODONEW */
    padding-right: 1rem;
    /* dt_labelWithSuper */
    > div {
      margin-left: 1rem;
      .innerText {
        color: ${colors.mediumGray};
        font-size: 1.5rem;
        font-weight: bold;
        line-height: 1.17;
        sup {
          font-size: 0.625rem;
          padding-left: 5px;
          font-weight: normal;
        }
      }
      &.active {
        .innerText {
          color: ${colors.magenta};
        }
      }
      &:last-child {
        margin-right: 0;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    height: 1.75rem;
    width: 100%;
    justify-content: flex-end;
    position: relative;

    ${StyledHorizontalScrollRow} {
      position: absolute;
      right: 0;
      top: 0;
      max-width: 100%;
      padding-right: 0;
      .horizontalScrollRowInner {
        padding-right: 0;
      }
    }
  }
`;

export const StyledFiterWrap = styled.div<{ isSticky: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${colors.darkGray};
  background-color: ${colors.white};
  height: 4rem;

  ${props =>
    props.isSticky
      ? `
   position: sticky;
    bottom: 0;
    width: 100%
    z-index: 1`
      : ''};

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0 4.9rem;
  }
`;
