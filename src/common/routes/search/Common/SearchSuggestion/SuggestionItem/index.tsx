import React, { FunctionComponent, ReactNode } from 'react';
import { Icon, Paragraph } from 'dt-components';
import cx from 'classnames';
import { SUGGESTION_TYPES } from '@search/store/enum';

import { StyledSuggestionItem } from './styles';

interface IProps {
  value: ReactNode;
  isInputChange?: boolean;
  isListingEnable: boolean;
  type: string;
  className?: string;
  isActive: boolean;
  onClick?(event: React.MouseEvent<HTMLDivElement>): void;
  onRemove?(event: React.MouseEvent<HTMLDivElement>): void;
}

const SuggestionItem: FunctionComponent<IProps> = (props: IProps) => {
  const {
    className,
    onClick,
    onRemove,
    value,
    isInputChange,
    type,
    isActive,
    isListingEnable
  } = props;
  const leftIconEl =
    type === SUGGESTION_TYPES.LOCAL ? (
      <Icon
        className='iconLeft'
        size='inherit'
        color='currentColor'
        name='ec-history'
      />
    ) : null;

  const rightIconEl =
    type === SUGGESTION_TYPES.LOCAL ? (
      <Icon
        className='iconRight'
        size='inherit'
        color='currentColor'
        name='ec-error'
        onClick={onRemove}
      />
    ) : null;
  const classes = cx(className, {
    isInputChange,
    isActive,
    isListingEnable: !isInputChange && isListingEnable
  });

  return (
    <StyledSuggestionItem className={classes}>
      {leftIconEl}
      <div className='text' onClick={onClick}>
        <Paragraph size='medium' weight='bold'>
          {value}
        </Paragraph>
      </div>
      {rightIconEl}
    </StyledSuggestionItem>
  );
};

export default SuggestionItem;
