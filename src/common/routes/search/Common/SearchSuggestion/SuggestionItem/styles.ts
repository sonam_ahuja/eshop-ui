import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledSuggestionItem = styled.div`
  position: relative;
  display: flex;
  flex: 1;
  align-items: center;
  margin-top: 1rem;
  color: ${colors.mediumGray};
  padding-left: 0.875rem;

  .iconLeft {
    font-size: 0.875rem;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    color: ${colors.mediumGray};
  }
  .iconRight {
    font-size: 1.25rem;
    justify-self: flex-end;
    margin-left: auto;
    color: ${colors.mediumGray};
    cursor: pointer;
  }
  .text {
    padding: 0 0.75rem;
    margin-right: 0.25rem;
    cursor: pointer;
    overflow: hidden;
    white-space: nowrap;

    .dt_paragraph div::before,
    .dt_paragraph span::before {
      content: '';
    }
  }

  &.isInputChange {
    color: ${colors.darkGray};
    .text span {
      color: ${colors.mediumGray};
    }
  }

  &.isListingEnable {
    color: ${colors.darkGray};
    .text span {
      color: ${colors.darkGray};
    }
  }

  &.isActive {
    color: ${colors.mediumGray};
    .text span {
      color: ${colors.mediumGray};
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    margin-top: 0.5rem;
    padding-left: 1rem;

    .iconLeft,
    .iconRight {
      font-size: 1rem;
    }
  }
`;
