import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SearchSuggestion, { IProps } from '@search/Common/SearchSuggestion';
import { combineSuggestionResult as dumpResult } from '@src/common/mock-api/search/search.mock';

describe('<SearchSuggestion />', () => {
  const props: IProps = {
    combineSuggestionResult: dumpResult,
    isInputChange: true,
    isListingEnable: true,
    inputValue: 'string',
    setSearchPriorityText: jest.fn(),
    setSelection: jest.fn(),
    removeSuggestion: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <SearchSuggestion {...newProps} />
      </ThemeProvider>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when input is not changing', () => {
    const newProps: IProps = { ...props, isInputChange: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when isListingEnable is not changing', () => {
    const newProps: IProps = { ...props, isListingEnable: false };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
