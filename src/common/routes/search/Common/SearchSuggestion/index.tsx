import React, { Fragment, FunctionComponent } from 'react';
import { INITIAL_VALUE, SUGGESTION_STATUS } from '@search/store/enum';
import { ISuggestionConverter } from '@search/store/transformer';
import SuggestionItem from '@search/Common/SearchSuggestion/SuggestionItem';
import he from 'he';

export interface IProps {
  combineSuggestionResult: ISuggestionConverter[];
  isInputChange: boolean;
  inputValue: string;
  isListingEnable: boolean;
  setSearchPriorityText(params: {
    activeText: string;
    inActiveText: string;
  }): void;
  setSelection(value: string): void;
  removeSuggestion(value: string): void;
}

const SearchSuggestion: FunctionComponent<IProps> = (props: IProps) => {
  const {
    combineSuggestionResult,
    setSearchPriorityText,
    isInputChange,
    inputValue,
    setSelection,
    removeSuggestion,
    isListingEnable
  } = props;

  return (
    <>
      {combineSuggestionResult.map(
        (result: ISuggestionConverter, index: number) => {
          const textEl = (
            <div>
              {(result.filterType === SUGGESTION_STATUS.MODIFIED ||
                result.filterType === SUGGESTION_STATUS.MATCH) && (
                <div
                  dangerouslySetInnerHTML={{ __html: he.decode(`${result.activeText}`) }}
                />
              )}
              {result.inActiveText}
            </div>
          );

          if (
            index === 0 &&
            (result.filterType === SUGGESTION_STATUS.MODIFIED ||
              result.filterType === SUGGESTION_STATUS.MATCH) &&
            isInputChange
          ) {
            setSearchPriorityText({
              activeText: inputValue,
              inActiveText:
                result.filterType === SUGGESTION_STATUS.MATCH
                  ? INITIAL_VALUE.BLANK
                  : (result.inActiveText as string)
            });
          }

          return (
            <Fragment key={result.index}>
              <SuggestionItem
                isInputChange={isInputChange}
                value={textEl}
                isListingEnable={isListingEnable}
                isActive={result.activeIndex}
                type={result.type}
                onClick={() => setSelection(`${result.fullText}`)}
                onRemove={() => removeSuggestion(`${result.fullText}`)}
              />
            </Fragment>
          );
        }
      )}
    </>
  );
};

export default SearchSuggestion;
