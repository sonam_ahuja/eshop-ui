import React, { FunctionComponent } from 'react';
import { Icon, List, ListItem, Paragraph, Title } from 'dt-components';
import { ISearchTranslation } from '@store/types/translation';

import { StyledEmptyCard, StyledMessageBox, StyledSearchTips } from './styles';

export interface IProps {
  queryText: string;
  searchTranslation: ISearchTranslation;
}

const EmptySearch: FunctionComponent<IProps> = (props: IProps) => {
  const { notFound, searchTips } = props.searchTranslation;
  const { queryText } = props;

  return (
    <StyledEmptyCard>
      <Paragraph className='noResultsFound' size='small' weight='medium' >{notFound.noResultsFound}</Paragraph>
      <StyledMessageBox>
        <Icon
          name='ec-eyebrow'
          color='currentColor'
          size='inherit'
        />
        <Title size='normal' weight='bold'>
          {notFound.message} "{queryText}"
        </Title>
      </StyledMessageBox>

      <StyledSearchTips>
        <List heading={searchTips.searchTipsText}>
          <ListItem label={searchTips.spellingText} />
          <ListItem label={searchTips.generalText} />
          <ListItem label={searchTips.differentWordsText} />
          <ListItem label={searchTips.helpText} />
        </List>
      </StyledSearchTips>
    </StyledEmptyCard>
  );
};
export default EmptySearch;
