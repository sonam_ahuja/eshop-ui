import styled from 'styled-components';
import { breakpoints, colors } from '@common/variables';

export const StyledMessageBox = styled.div`
  margin-top: 3.5rem;
  padding-right: 3.375rem;
  width: 100%;
  .dt_icon {
    font-size: 3.5rem;
    color: ${colors.darkGray};
  }
  .dt_title {
    margin-top: 0.5rem;
    color: ${colors.mediumGray};
    word-break: break-word;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    display: flex;
    align-items: center;
    padding: 0;
    /* width: calc(100% - 95px); */
    .dt_icon {
      font-size: 4.5rem;
      margin-right: 1.5rem;
    }
    .dt_title {
      margin: 0;
    }
  }
`;

export const StyledSearchTips = styled.div`
  margin-top: 4rem;
  .dt_paragraph {
    font-weight: 500;
    line-height: 1.5rem;
    font-size: 1rem;
  }

  .listWrap .listItemWrap .listItem {
    color: ${colors.darkGray};
    margin-bottom: 0.75rem;
    padding-left: 1.8rem;

    &:after {
      left: 0.125rem;
      top: 0.7rem;
      height: 0.3rem;
      width: 0.3rem;
    }
    &:last-child {
      margin-bottom: 0;
    }
  }

  .listWrap .heading {
    margin-bottom: 0.75rem;
    color: ${colors.darkGray};
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .listWrap .listItemWrap .listItem {
      margin-bottom: 0.5rem;
    }
  }
`;

export const StyledEmptyCard = styled.div`
  padding: 1.25rem 1.25rem 4rem 1.25rem;
  background-color: ${colors.silverGray};
  .noResultsFound {
    color: ${colors.darkGray};
    /* text-transform: lowercase; */
    /* &:first-letter {
      text-transform: uppercase;
    } */
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 1rem 4.9rem 5.4rem 4.9rem;
    height: 100%;
  }
`;
