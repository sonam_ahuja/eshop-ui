import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import SearchModal, {
  IProps,
  SearchModal as SearchModalComponent
} from '@src/common/routes/search/Modals/SearchModal';
import { RootState } from '@src/common/store/reducers';
import configureStore from 'redux-mock-store';
import { histroyParams } from '@mocks/common/histroy';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

describe('<SearchModal />', () => {
  const props: IProps = {
    ...histroyParams,
    search: appState().search,
    searchTranslation: appState().translation.cart.global.search,
    searchConfiguration: appState().configuration.cms_configuration.global
      .search,
    fetchCategories: jest.fn(),
    fetchSuggestion: jest.fn(),
    setSearchModal: jest.fn(),
    setListingEnable: jest.fn(),
    setInputContent: jest.fn(),
    setTempText: jest.fn(),
    setSearchPriorityText: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SearchModal {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<SearchModalComponent>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SearchModalComponent {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('SearchModal').instance() as SearchModalComponent).onCancel(
      true
    );
    (component.find('SearchModal').instance() as SearchModalComponent).onCancel(
      false
    );
    (component
      .find('SearchModal')
      .instance() as SearchModalComponent).updateUrlPath('test');
    (component
      .find('SearchModal')
      .instance() as SearchModalComponent).enableFilterListing(true);
    (component
      .find('SearchModal')
      .instance() as SearchModalComponent).removeSuggestion('suggestion');
    (component
      .find('SearchModal')
      .instance() as SearchModalComponent).onRightArrowClick('suggestion');
    (component
      .find('SearchModal')
      .instance() as SearchModalComponent).setSelection('suggestion');
    (component
      .find('SearchModal')
      .instance() as SearchModalComponent).onInputValueChange('suggestion');
    (component
      .find('SearchModal')
      // tslint:disable-next-line: no-object-literal-type-assertion
      .instance() as SearchModalComponent).onInputChange({
      target: { value: 'value' }
    } as React.ChangeEvent<HTMLInputElement>);
  });
});
