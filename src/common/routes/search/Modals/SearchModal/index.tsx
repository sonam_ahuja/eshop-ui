import { Utils } from 'dt-components';
import { IProps as IMobileSpeechRecognitionInputProps } from '@common/components/SpeechRecognition/utils/SpeechRecognitionHOC';
import { IFetchCategoriesParams, ISearchState } from '@search/store/types';
import { removeLocalStorageSuggestion } from '@common/components/SpeechRecognition/utils/localStorageOperation';
import { RouteComponentProps, withRouter } from 'react-router';
import React, { Component, ReactNode } from 'react';
import {
  StyledSearchModal,
  StyledSuggestionWrap
} from '@search/Modals/SearchModal/styles';
import { ISearchTranslation } from '@store/types/translation';
import MobileSpeechRecognitionInput from '@common/components/SpeechRecognition/mobile';
import { ISearchConfiguration } from '@store/types/configuration';
import { SEARCH_ROUTE } from '@search/store/enum';
import {
  ISuggestionConverter,
  searchSuggestionResolver
} from '@search/store/transformer';
import SearchSuggestion from '@search/Common/SearchSuggestion';
import { sendSearchClickEvent } from '@events/common/index';
import { SEARCH_MODE } from '@events/constants/eventName';

export interface IComponentProps {
  search: ISearchState;
  searchTranslation: ISearchTranslation;
  searchConfiguration: ISearchConfiguration;
  fetchCategories(params: IFetchCategoriesParams): void;
  fetchSuggestion(query: string): void;
  setSearchModal(showModal: boolean): void;
  setListingEnable(showModal: boolean): void;
  setInputContent(value: string): void;
  setTempText(value: string): void;
  setSearchPriorityText(params: {
    activeText: string;
    inActiveText: string;
  }): void;
}

export type IProps = RouteComponentProps & IComponentProps;
export interface IState {
  localStorageUpdated: boolean;
  inputValue: string;
  isInputChange: boolean;
  isOpen: boolean;
  currentIndex: number;
  isListingNavigationEnable: boolean;
}

export class SearchModal extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      localStorageUpdated: false,
      inputValue: '',
      isOpen: true,
      isInputChange: false,
      currentIndex: 0,
      isListingNavigationEnable: false
    };
    const { searchDebounce } = this.props.searchConfiguration;
    this.removeSuggestion = this.removeSuggestion.bind(this);
    this.setSelection = this.setSelection.bind(this);
    this.updateUrlPath = this.updateUrlPath.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onRightArrowClick = this.onRightArrowClick.bind(this);
    this.enableFilterListing = this.enableFilterListing.bind(this);
    this.onInputValueChange = this.onInputValueChange.bind(this);
    this.suggestionApi = Utils.debounce(
      this.suggestionApi,
      searchDebounce
    ).bind(this);
  }

  private mobileProps: IMobileSpeechRecognitionInputProps = {
    autoStart: false,
    continuous: false,
    // tslint:disable-next-line: no-empty
    onInputChange: () => {},
    // tslint:disable-next-line: no-empty
    onInputValueChange: (value: string) => {
      // tslint:disable-next-line: no-console
      console.log(value);
    }
  };

  onCancel(isOpen: boolean): void {
    this.setState({ isOpen });
  }

  updateUrlPath(text: string): void {
    this.props.fetchCategories({
      query: text,
      url: this.props.location.pathname,
      type: SEARCH_ROUTE.SEARCH_RESULT,
      isNewHit: true
    });
  }

  suggestionApi(value: string): void {
    this.props.fetchSuggestion(value);
  }

  setSelection(text: string): void {
    const { setSearchPriorityText, setTempText } = this.props;
    const { localStorageUpdated } = this.state;
    setSearchPriorityText({
      activeText: '',
      inActiveText: ''
    });
    sendSearchClickEvent(text, SEARCH_MODE.TYPED_MODE);
    this.enableFilterListing(false);
    setTempText(text);
    this.suggestionApi(text);
    this.updateUrlPath(text);
    this.setState({ localStorageUpdated: !localStorageUpdated });
  }

  enableFilterListing(isEnable: boolean): void {
    const { searchFilterSetting } = this.props.search;
    const { setListingEnable } = this.props;
    if (searchFilterSetting.showListing !== isEnable) {
      setListingEnable(isEnable);
    }
  }

  removeSuggestion(suggestion: string): void {
    const { localStorageUpdated } = this.state;
    const isSuggestionRemoved = removeLocalStorageSuggestion(suggestion);
    if (isSuggestionRemoved) {
      this.setState({ localStorageUpdated: !localStorageUpdated });
    }
  }

  onRightArrowClick(value: string): void {
    const { setSearchPriorityText } = this.props;
    this.setState({ inputValue: value });
    setSearchPriorityText({
      activeText: '',
      inActiveText: ''
    });
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
    const inputValue = (event.target as HTMLInputElement).value;
    const { setSearchPriorityText } = this.props;
    this.enableFilterListing(true);
    this.setState({ inputValue });
    if (inputValue.trim().length) {
      this.setState({ isInputChange: true });
    } else {
      this.setState({ inputValue: '', isInputChange: false });
      setSearchPriorityText({
        activeText: '',
        inActiveText: ''
      });
    }
  }

  onInputValueChange(value: string): void {
    const { setTempText } = this.props;
    if (value && value.length) {
      this.setState({ inputValue: value, isInputChange: true }, () => {
        setTempText(value);
        this.suggestionApi(value);
      });
    } else {
      this.setState({ inputValue: '', isInputChange: false });
    }
  }

  render(): ReactNode {
    const { isInputChange, inputValue } = this.state;
    const { suggestion, searchModal } = this.props.search;
    const { searchQueries } = this.props.searchTranslation;
    const { setSearchPriorityText } = this.props;
    const combineSuggestionResult: ISuggestionConverter[] = searchSuggestionResolver(
      suggestion.data,
      searchQueries,
      inputValue,
      inputValue,
      false,
      -1
    );

    if (!(combineSuggestionResult.length > 0)) {
      setSearchPriorityText({
        activeText: '',
        inActiveText: ''
      });
    }

    return (
      <StyledSearchModal
        type='fullHeight'
        showCloseButton={false}
        isOpen={searchModal.modalOpen}
      >
        <MobileSpeechRecognitionInput
          {...this.mobileProps}
          onInputChange={this.onInputChange}
          onRightArrowClick={this.onRightArrowClick}
          onInputValueChange={this.onInputValueChange}
        />
        <StyledSuggestionWrap>
          <SearchSuggestion
            inputValue={inputValue}
            combineSuggestionResult={combineSuggestionResult}
            isListingEnable={false}
            isInputChange={isInputChange}
            setSearchPriorityText={setSearchPriorityText}
            setSelection={this.setSelection}
            removeSuggestion={this.removeSuggestion}
          />
        </StyledSuggestionWrap>
      </StyledSearchModal>
    );
  }
}

export default withRouter<IProps>(SearchModal);
