import styled from 'styled-components';
import { colors } from '@common/variables';
import { DialogBoxApp } from '@src/common/components/DialogBox/styles';
import { StyledSuggestionItem } from '@search/Common/SearchSuggestion/SuggestionItem/styles';

export const StyledSuggestionWrap = styled.div`
  padding: 1.5rem 1.25rem 1.375rem 1.375rem;
  flex: 1;
  overflow: auto;

  ${StyledSuggestionItem} {
    &:first-child {
      margin-top: 0;
    }
  }
`;

export const StyledSearchModal = styled(DialogBoxApp)`
  &.dt_overlay {
    animation: none;
    align-items: flex-start !important;
    transition: opacity 0.3s ease-in, transfrom 0.3s ease-out;
    transform-origin: top left;
  }
  .dt_overlay .dt_outsideClick {
    margin: auto;
    width: 100%;
    height: 100%;
  }
  .dt_overlay .dt_outsideClick .dialogBoxContentWrap {
    background: ${colors.white};
    padding: 0;
    animation-name: fadeIn;
    height: 100%;
    max-height: 100%;
  }
`;
