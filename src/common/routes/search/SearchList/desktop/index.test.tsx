import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import SearchViewDesktop, { IProps } from '@search/SearchList/desktop';
import { histroyParams } from '@mocks/common/histroy';
import { StaticRouter } from 'react-router-dom';

describe('<SearchViewDesktop />', () => {
  const props: IProps = {
    ...histroyParams,
    categories: {
      loading: true,
      error: null,
      categoryList: [
        {
          categoryId: 'string',
          categoryName: 'string',
          categorySlug: 'string',
          count: 2
        }
      ],
      selectedCategory: {
        categoryId: 'string',
        categoryName: 'string',
        categorySlug: 'string',
        count: 2
      },
      total: 2
    },
    onCategorySelect: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SearchViewDesktop {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
