import React, { FunctionComponent } from 'react';
import { LabelWithSuper } from 'dt-components';
import { DesktopView } from '@search/SearchList/desktop/styles';
import { ICategories, ICategoryList } from '@search/store/types';
import { RouteComponentProps, withRouter } from 'react-router';
import { createQueryParams, getSelectedFiltersFromParams } from '@search/utils';
import { PAGE_NUMBER } from '@productList/store/enum';

interface IComponentProps {
  categories: ICategories;
  onCategorySelect(categories: ICategoryList): void;
}

export type IProps = IComponentProps & RouteComponentProps;

const SearchViewDesktop: FunctionComponent<IProps> = (props: IProps) => {
  const { categoryList } = props.categories;
  const { onCategorySelect } = props;
  const { categoryId, currentPage, ...params } = getSelectedFiltersFromParams(
    props.location.search
  );

  const onSelect = (selected: ICategoryList) => {
    const query: string = params.query ? params.query : '';
    if (event && query.length && selected.count > 0) {
      const path = `${props.location.pathname}?${createQueryParams(
        query,
        currentPage ? currentPage : PAGE_NUMBER.INITIAL,
        selected.categorySlug
      )}`;
      onCategorySelect(selected);
      props.history.push(path);
    }
  };

  return (
    <DesktopView>
      {categoryList &&
        categoryList.map((category: ICategoryList, index: number) => {
          if (category.count > 0) {
            return (
              <div
                onClick={(event: React.MouseEvent<HTMLDivElement>) => {
                  if (event) {
                    onSelect(category);
                  }
                }}
                key={index}
              >
                <LabelWithSuper
                  className={
                    category.categorySlug === categoryId
                      ? 'active'
                      : 'categories'
                  }
                  label={category.categoryName}
                  superText={`${category.count}`}
                />
              </div>
            );
          } else {
            return null;
          }
        })}
    </DesktopView>
  );
};

export default withRouter<IProps>(SearchViewDesktop);
