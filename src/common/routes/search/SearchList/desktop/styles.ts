import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const DesktopView = styled.div`
  display: flex;
  flex-direction: 'row';
  flex-wrap: nowrap;
  color: #383838;
  padding-top: 3.75rem;
  padding-bottom: 1.25rem;
  overflow: auto;
  white-space: nowrap;

  .dt_labelWithSuper {
    cursor: default;
  }

  /* TODO */
  .active {
    padding-right: 1rem;
    .innerText {
      font-size: 1.25rem;
      font-weight: bold;
      line-height: 1.2;
      color: ${colors.magenta};
    }
  }

  .categories {
    padding-right: 1rem;
    .innerText {
      font-size: 1.25rem;
      font-weight: bold;
      line-height: 1.2;
      color: ${colors.mediumGray};
    }
  }
`;
