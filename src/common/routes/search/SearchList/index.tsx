import React, { Component, ReactNode } from 'react';
import { IGrid } from 'dt-components';
import { IItemPerPageOptions, pageThemeType } from '@productList/store/types';
import { ICategoryList, ISearchState } from '@search/store/types';
import {
  IProductListTranslation,
  ISearchTranslation
} from '@store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductListConfiguration
} from '@store/types/configuration';
import ProductGrid from '@productList/Grid';
import SearchViewDesktop from '@search/SearchList/desktop';
import SearchViewMobile from '@search/SearchList/mobile';
import { StyledView } from '@search/SearchList/styles';
import { isMobile } from '@common/utils';
import { PAGE } from '@src/common/constants/appConstants';

export interface IProps {
  search: ISearchState;
  categorySlug: string;
  categoryName: string;
  loading: boolean;
  currency: ICurrencyConfiguration;
  gridLoading: boolean;
  pagination: IGrid.IPagination;
  productConfiguration: IProductListConfiguration;
  productTranslation: IProductListTranslation;
  productListLength: number;
  searchTranslation: ISearchTranslation;
  theme: pageThemeType;
  catalogConfiguration: ICatalogConfiguration;
  onCategorySelect(categories: ICategoryList): void;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
  loadMoreProductList(): void;
  goToPage(currentPage: number): void;
  setlastListURL(lastListURL: string): void;
  changeItemPerPage(item: IItemPerPageOptions): void;
  setDeviceDetailedList(list: string): void;
}

export class SearchList extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const {
      search,
      theme,
      categoryName,
      gridLoading,
      loadMoreProductList,
      changeItemPerPage,
      setlastListURL,
      goToPage,
      productConfiguration,
      categorySlug,
      searchTranslation,
      openNotificationModal,
      productTranslation,
      currency,
      onCategorySelect,
      productListLength,
      setDeviceDetailedList,
      pagination
    } = this.props;

    return (
      <StyledView>
        <div className='searchListContainer'>
          {isMobile.phone ? (
            <SearchViewMobile
              theme={theme}
              title={categoryName}
              search={search}
              category={'Device'}
              productListLength={productListLength}
              onCategorySelect={onCategorySelect}
              productConfiguration={productConfiguration}
              changeItemPerPage={changeItemPerPage}
              itemPerPageList={productConfiguration.itemPerPageList}
              perPageTranslation={productTranslation.perPage}
              searchTranslation={searchTranslation}
              selectitemPerPage={productTranslation.selectitemPerPage}
              itemPerPage={pagination.itemsPerPage}
            />
          ) : (
            <SearchViewDesktop
              categories={search.categories}
              onCategorySelect={onCategorySelect}
            />
          )}
          <ProductGrid
            productList={search.data}
            loadMoreProductList={loadMoreProductList}
            goToPage={goToPage}
            categoryId={categorySlug}
            translation={productTranslation}
            loading={gridLoading}
            currency={currency}
            setlastListURL={setlastListURL}
            pagination={pagination}
            configuration={productConfiguration}
            openNotificationModal={openNotificationModal}
            theme={theme}
            tariffId={''}
            view={PAGE.SEARCH}
            setDeviceDetailedList={setDeviceDetailedList}
            loyalty={''}
            discountId={''}
            catalogConfiguration={this.props.catalogConfiguration}
          />
        </div>
      </StyledView>
    );
  }
}
export default SearchList;
