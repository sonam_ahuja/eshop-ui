import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import appState from '@store/states/app';
import SearchViewMobile, {
  IProps
} from '@src/common/routes/search/SearchList/mobile';
import { histroyParams } from '@mocks/common/histroy';
import { StaticRouter } from 'react-router-dom';
import { PAGE_THEME } from '@common/store/enums/index';

describe('<SearchViewMobile />', () => {
  const props: IProps = {
    ...histroyParams,
    productListLength: 4,
    search: appState().search,
    productConfiguration: appState().configuration.cms_configuration.modules
      .productList,
    searchTranslation: appState().translation.cart.global.search,
    title: 'string',
    theme: PAGE_THEME.PRIMARY,
    category: 'Mobile',
    perPageTranslation: 'string',
    selectitemPerPage: 'string',
    itemPerPage: 2,
    itemPerPageList: 'string',
    onCategorySelect: jest.fn(),
    changeItemPerPage: jest.fn()
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SearchViewMobile {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
