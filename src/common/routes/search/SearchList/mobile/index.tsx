import { IItemPerPageOptions } from '@productList/store/types';
import React from 'react';
import { MobileView } from '@search/SearchList/mobile/styles';
import { getItemPerPage } from '@productList/utils/requestTemplate';
import {
  getCategories,
  getSelectedCategories
} from '@search/utils/requestTemplate';
import { IGrid, Section, Select, Title } from 'dt-components';
import { ISearchTranslation } from '@common/store/types/translation';
import { IProductListConfiguration } from '@common/store/types/configuration';
import isMobile from 'ismobilejs';
import { PAGE_THEME } from '@common/store/enums/index';
import { RouteComponentProps, withRouter } from 'react-router';
import { ICategoryList, ISearchState } from '@search/store/types';
import { createQueryParams, getSelectedFiltersFromParams } from '@search/utils';
import { PAGE_NUMBER } from '@productList/store/enum';
import { sendDropdownClickEvent } from '@src/common/events/common';

export interface IComponentProps {
  title: string;
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
  search: ISearchState;
  category: string;
  perPageTranslation: string;
  selectitemPerPage: string;
  itemPerPage: number;
  itemPerPageList: string;
  productListLength: number;
  productConfiguration: IProductListConfiguration;
  searchTranslation: ISearchTranslation;
  onCategorySelect(categories: ICategoryList): void;
  changeItemPerPage(item: IItemPerPageOptions): void;
}

export type IProps = RouteComponentProps & IComponentProps;

const SearchViewMobile = (props: IProps) => {
  const {
    perPageTranslation,
    itemPerPage,
    changeItemPerPage,
    selectitemPerPage,
    productConfiguration,
    productListLength,
    itemPerPageList,
    theme
  } = props;
  const { categoryList, selectedCategory } = props.search.categories;

  const categoriesOptions: IItemPerPageOptions[] = getCategories(categoryList);

  let categoriesSelectedItem = {
    title: ``,
    id: 0
  };

  if (selectedCategory) {
    categoriesSelectedItem = getSelectedCategories(
      categoriesOptions,
      selectedCategory.categoryName
    );
  }

  const options: IItemPerPageOptions[] = getItemPerPage(
    perPageTranslation,
    itemPerPageList
  );

  const onCategorySelection = (item: IItemPerPageOptions): void => {
    sendDropdownClickEvent(item.title);
    const { categoryId, currentPage, ...params } = getSelectedFiltersFromParams(
      props.location.search
    );
    const query: string = params.query ? params.query : '';
    categoryList.forEach((category: ICategoryList) => {
      if (category.categoryName === item.title) {
        if (query.length > 0) {
          const path = `${props.location.pathname}?${createQueryParams(
            query,
            PAGE_NUMBER.INITIAL,
            category.categorySlug
          )}`;
          props.onCategorySelect(category);
          props.history.push(path);
        }

        return;
      }
    });
  };

  const hideItemPerPage =
    productConfiguration.paginationType ===
      IGrid.IGridPaginationTypeEnum.infinite ||
    productListLength < productConfiguration.itemPerPage ||
    productConfiguration.paginationType ===
      IGrid.IGridPaginationTypeEnum.showMore;

  const selectedItem = {
    title: `${itemPerPage} ${perPageTranslation}`,
    id: itemPerPage
  };
  const { ...params } = getSelectedFiltersFromParams(props.location.search);
  const query: string = params.query ? params.query : '';
  const { filterInBetweenMessageText } = props.searchTranslation;

  return (
    <MobileView theme={theme}>
      {isMobile.phone && (
        <Section className='notFound'>
          {props.search.total} {filterInBetweenMessageText} “{query}"
        </Section>
      )}
      <div className='itemPerPage'>
        <div className='selectPerPageWrap d-flex-wrap'>
          <div className='leftPanel'>
            <Title size='small' weight='ultra' className='description'>
              {props.category}
            </Title>
            <Select
              className='phonesDropWrap'
              selectedItem={categoriesSelectedItem}
              placeholder={selectitemPerPage}
              items={categoriesOptions}
              onItemSelect={onCategorySelection}
              useNativeDropdown={isMobile.phone}
            />
          </div>
          {!hideItemPerPage && (
            <div className='rightPanel'>
              <Select
                className='selectPerPage'
                selectedItem={selectedItem}
                placeholder={selectitemPerPage}
                items={options}
                onItemSelect={(item: IItemPerPageOptions) => {
                  sendDropdownClickEvent(item.title);
                  changeItemPerPage(item);
                }}
                useNativeDropdown={isMobile.phone}
              />
            </div>
          )}
        </div>
      </div>
    </MobileView>
  );
};

export default withRouter<IProps>(SearchViewMobile);
