import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { StyledBreadcrumbWrap } from '@common/components/CatalogBreadCrumb/styles';
import { themeObj } from '@productList/theme';
import { pageThemeType } from '@search/store/types';
import { hexToRgbA } from '@src/common/utils';
export const MobileView = styled.div<{ theme: pageThemeType }>`
  color: ${colors.white};

  .notFound {
    font-size: 14px;
    font-weight: 500;
    line-height: 1.43;
    color: ${colors.darkGray};
    padding-bottom: 2.75rem;
  }

  ${StyledBreadcrumbWrap} {
    .dt_breadcrumb {
      li {
        a {
          color: ${({ theme }) => {
            return themeObj[theme].breadcrumb.inactiveColor;
          }};
        }
        &.active {
          color: ${({ theme }) => {
            return themeObj[theme].breadcrumb.activeColor;
          }};
        }
      }
    }
  }

  .title {
    width: 100%;
  }

  .itemPerPage {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0rem 0 1.25rem;

    .selectPerPageWrap {
      /* padding-left: 0.875rem; */
      .selectPerPage {
        width: 124px;
        padding: 0.688rem 1.0625rem 0.688rem 1rem;
        border: 1px solid ${colors.white};
        color: ${colors.white};
        border-radius: 10px;
        .caret {
          margin-right: 0.25rem;
          path {
            fill: ${colors.white};
          }
        }

        .optionsList {
          background: #323232;
          padding: 0.5rem;
          border-radius: 0.5rem;
          border: 1px solid ${colors.ironGray};
          color: ${colors.white};

          > div:hover {
            background: rgba(0, 0, 0, 0.2);
          }
        }
        .styledSelect {
          font-size: 0.875rem;
          line-height: 1.25rem;
          font-weight: 500;
        }
      }
    }
  }

  .d-flex-wrap {
    display: flex;
    width: 100%;
    align-items: flex-end;
    justify-content: space-between;
    .leftPanel {
      flex: 1;
      width: calc(100% - 124px);
      padding-left: 0.3125rem;
      padding-right: 0.625rem;
      .styledSelectWrap {
        width: auto;
        justify-content: initial;
        .caret {
          margin-left: 5px;
        }
      }
      .styledSelect {
        font-size: 1.5rem;
        color: ${colors.magenta};
        font-weight: 900;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
      }
    }
    .rightPanel {
      width: 118px;
      .selectPerPage {
        float: right;
        width: 118px;
        padding: 0.688rem 1.0625rem 0.688rem 1rem;
        border: 1px solid ${colors.ultraLightGray};
        color: #ffffff;
        border-radius: 8px;
        background: ${hexToRgbA(colors.white, 0.5)};
        .styledSelect {
          color: ${colors.ironGray};
        }
        .caret path {
          fill: ${colors.lightMagenta};
        }
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .itemPerPage {
      align-items: flex-end;
      padding: 0rem 0 2rem;
      .selectPerPageWrap {
        .selectPerPage {
          padding: 0;
          width: 131px;
          .outsideClick {
            .styledSelect {
              font-size: 15px;
              font-weight: normal;
              line-height: 1.33;
            }
            i {
              width: 11px;
              height: 6px;
            }
          }
        }
        .styledSelectWrap {
          padding: 0.6rem 0.75rem 0.65rem 1rem;
          height: 2.25rem;
          .caret {
            margin: 0;
          }
          .selectPerPage {
            width: 131px;
            padding: 0.75rem 0.5rem 0.75rem 1rem;
          }
          .styledSelect {
            font-size: 0.75rem;
            line-height: 1rem;
          }
        }
      }
    }
  }
`;
