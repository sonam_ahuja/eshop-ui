import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledView = styled.div`
  .infinitePagination {
    color: ${colors.ironGray};
    .spinner svg path {
      fill: ${colors.magenta};
    }
  }
`;
