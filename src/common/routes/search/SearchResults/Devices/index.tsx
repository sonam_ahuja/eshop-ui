import React, { FunctionComponent } from 'react';
import CatagoryHeader from '@search/Common/CatagoryHeader';
import ProductGrid from '@productList/Grid';
import { pageThemeType } from '@productList/store/types';
import { IGrid } from 'dt-components';
import {
  IProductListTranslation,
  ISearchTranslation
} from '@store/types/translation';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductListConfiguration
} from '@store/types/configuration';
import { resetPagination } from '@search/utils/productListing';
import { ISearchState } from '@search/store/types';
import { PAGE } from '@src/common/constants/appConstants';

export interface IProps {
  search: ISearchState;
  categorySlug: string;
  categoryName: string;
  currency: ICurrencyConfiguration;
  loading: boolean;
  productConfiguration: IProductListConfiguration;
  productTranslation: IProductListTranslation;
  searchTranslation: ISearchTranslation;
  theme: pageThemeType;
  catalogConfiguration: ICatalogConfiguration;
  updateShowAppShell(showAppShell: boolean): void;
  setlastListURL(lastListURL: string): void;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
  setDeviceDetailedList(list: string): void;
}

const Devices: FunctionComponent<IProps> = (props: IProps) => {
  const {
    search,
    theme,
    loading,
    setlastListURL,
    productConfiguration,
    categorySlug,
    productTranslation,
    openNotificationModal,
    currency,
    updateShowAppShell,
    setDeviceDetailedList,
    catalogConfiguration
  } = props;
  const { viewAll, categories } = props.searchTranslation;
  const {
    productList: trimProductList,
    pagination: resetPaginationValue
  } = resetPagination(search.data);

  return (
    <>
      <CatagoryHeader
        title={categories.devices}
        isViewAllNeeded={search.pagination.totalItems > 4}
        viewALL={viewAll}
        updateShowAppShell={updateShowAppShell}
      />
      <div className='device'>
        <ProductGrid
          productList={trimProductList}
          // tslint:disable-next-line:no-empty
          loadMoreProductList={() => {}}
          // tslint:disable-next-line:no-empty
          goToPage={() => {}}
          categoryId={categorySlug}
          translation={productTranslation}
          loading={loading}
          currency={currency}
          setlastListURL={setlastListURL}
          pagination={resetPaginationValue}
          view={PAGE.SEARCH}
          configuration={{
            ...productConfiguration,
            paginationType: 'numbered' as IGrid.IGridPaginationType
          }}
          openNotificationModal={openNotificationModal}
          theme={theme}
          setDeviceDetailedList={setDeviceDetailedList}
          tariffId={''}
          loyalty={''}
          discountId={''}
          catalogConfiguration={catalogConfiguration}
        />
      </div>
    </>
  );
};

export default Devices;
