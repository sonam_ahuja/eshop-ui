import React, { FunctionComponent } from 'react';
import { pageThemeType } from '@productList/store/types';
import { ISearchState } from '@search/store/types';
import { IGrid } from 'dt-components';
import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  IProductListConfiguration
} from '@store/types/configuration';
import {
  IProductListTranslation,
  ISearchTranslation
} from '@store/types/translation';
import Devices from '@search/SearchResults/Devices';
import { isMobile } from '@common/utils';

export interface IProps {
  search: ISearchState;
  categoryName: string;
  categorySlug: string;
  loading: boolean;
  pagination: IGrid.IPagination;
  currency: ICurrencyConfiguration;
  productConfiguration: IProductListConfiguration;
  productTranslation: IProductListTranslation;
  searchTranslation: ISearchTranslation;
  theme: pageThemeType;
  query: string;
  catalogConfiguration: ICatalogConfiguration;
  updateShowAppShell(showAppShell: boolean): void;
  openNotificationModal(
    event: React.MouseEvent,
    varaintId: string,
    deviceName: string
  ): void;
  setlastListURL(lastListURL: string): void;
  setDeviceDetailedList(list: string): void;
}

const SearchResults: FunctionComponent<IProps> = (props: IProps) => {
  const { filterInBetweenMessageText } = props.searchTranslation;

  return (
    <div className='searchListContainer'>
      {isMobile.phone && (
        <div className='notFound'>
          {props.search.total} {filterInBetweenMessageText} “{props.query}”
        </div>
      )}
      <Devices {...props} categorySlug={props.categorySlug} />
    </div>
  );
};

export default SearchResults;
