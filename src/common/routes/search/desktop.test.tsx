import { StaticRouter } from 'react-router-dom';
import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import {
  ErrorData,
  NotificationData,
  SendStockPayload
} from '@mocks/productList/productList.mock';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { MODAL_TYPE } from '@productList/store/enum';
import { histroyParams } from '@mocks/common/histroy';
import { PAGE_THEME } from '@common/store/enums/index';
import { mapDispatchToProps, mapStateToProps } from '@search/mapProps';
import { IProps, Search } from '@search/index';
import {
  SearchFilterHeaderDesktopShell,
  SearchListShell,
  SearchResultShell
} from '@search/index.shell';

describe('<Search Desktop/>', () => {
  const props: IProps = {
    ...histroyParams,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'pl' },
    searchTranslation: appState().translation.cart.global.search,
    searchConfiguration: appState().configuration.cms_configuration.global
      .search,
    search: appState().search,
    notificationLoading: false,
    loading: true,
    clearScrollPosition: jest.fn(),
    gridLoading: true,
    showAppShell: true,
    lastScrollPositionMap: { urlPath: 2 },
    setScrollPosition: jest.fn(),
    sendAllLoadingDisable: jest.fn(),
    error: null,
    backgroundImage: 'https://i.ibb.co/mHxM497/Untitled-1.jpg',
    theme: PAGE_THEME.PRIMARY,
    pagination: {
      currentPage: 2,
      itemsPerPage: 10,
      totalItems: 10
    },
    globalTranslation: appState().translation.cart.global,
    productTranslation: appState().translation.cart.productList,
    productConfiguration: appState().configuration.cms_configuration.modules
      .productList,
    characteristics: [{ name: 'key', values: [{ value: 'value1' }] }],
    notificationModalType: MODAL_TYPE.CONFIRMATION_MODAL,
    notificationData: NotificationData,
    showModal: true,
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    loadMoreSearchList: jest.fn(),
    fetchSearchList: jest.fn(),
    changeItemPerPage: jest.fn(),
    updateShowAppShell: jest.fn(),
    sendStockNotification: jest.fn(),
    setInputContent: jest.fn(),
    setTempText: jest.fn(),
    setListingEnable: jest.fn(),
    fetchWithOutConditionCategories: jest.fn(),
    setSearchModal: jest.fn(),
    setSearchPriorityText: jest.fn(),
    onCategorySelect: jest.fn(),
    fetchCategories: jest.fn(),
    setError: jest.fn(),
    openModal: jest.fn(),
    setlastListURL: jest.fn(),
    setNotificationData: jest.fn(),
    resetData: jest.fn(),
    setDeviceDetailedList: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Search {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = { isVisible: true, showAppShell: true };
    const component = mount(
      <ThemeProvider theme={{}}>
        <div>
          <SearchFilterHeaderDesktopShell />
          <SearchListShell {...appShellProps} />
          <SearchResultShell {...appShellProps} />
        </div>
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<Search>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Search {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('Search').instance() as Search).onScroll();
    (component.find('Search').instance() as Search).updateShowAppShell(true);
    (component.find('Search').instance() as Search).updateShowAppShell(false);
    (component.find('Search').instance() as Search).setItemPerPageValue({
      title: 'string',
      id: 1
    });
    (component.find('Search').instance() as Search).componentDidUpdate(props);
    (component.find('Search').instance() as Search).openNotificationModal(
      // tslint:disable-next-line: no-object-literal-type-assertion
      {
        // tslint:disable-next-line: no-empty
        stopPropagation: () => {}
      } as React.MouseEvent,
      'variantId',
      'deviceName'
    );
    (component.find('Search').instance() as Search).componentWillUnmount();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });
  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();

    mapDispatchToProps(dispatch).fetchSearchList(2, '1234');
    mapDispatchToProps(dispatch).loadMoreSearchList('12', true, 12);
    mapDispatchToProps(dispatch).changeItemPerPage(3);
    mapDispatchToProps(dispatch).updateShowAppShell(true);
    mapDispatchToProps(dispatch).sendStockNotification(SendStockPayload);
    mapDispatchToProps(dispatch).setError(ErrorData);
    mapDispatchToProps(dispatch).openModal(true);
    mapDispatchToProps(dispatch).setNotificationData(NotificationData);
    mapDispatchToProps(dispatch).resetData();
    mapDispatchToProps(dispatch).setScrollPosition('/basket', 23);
    mapDispatchToProps(dispatch).clearScrollPosition('/basket');
    mapDispatchToProps(dispatch).setlastListURL('12');
    mapDispatchToProps(dispatch).setInputContent('12');
    mapDispatchToProps(dispatch).onCategorySelect({
      categoryId: 'mobile',
      categoryName: 'Mobile_Phones',
      categorySlug: '/Mobile',
      count: 2
    });
    mapDispatchToProps(dispatch).fetchCategories({
      query: 'string',
      url: 'string',
      type: 'string'
    });
    mapDispatchToProps(dispatch).setListingEnable(true);
    mapDispatchToProps(dispatch).setTempText('search');
    mapDispatchToProps(dispatch).fetchWithOutConditionCategories({
      query: 'string',
      url: 'string',
      type: 'string'
    });
    mapDispatchToProps(dispatch).setSearchPriorityText({
      activeText: 'string',
      inActiveText: 'string'
    });
    mapDispatchToProps(dispatch).setSearchModal(true);
  });
});
