import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints, colors } from '@src/common/variables';
import { isMobile } from '@common/utils';

export const StyledFilters = styled.div`
  .filter {
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    margin-bottom: 1.25rem;
    align-items: center;
    .filter-message {
      height: 1rem;
      width: 8rem;
      margin-right: 1rem;
      background: ${colors.cloudGray};
      &:last-child {
        margin-right: 0;
      }
    }
  }
`;
export const StyledSearchListShell = styled.div`
  padding: 1.25rem 1.25rem 4.5rem;
  background: ${colors.extraDarkGray};
  width: 100%;
  .header {
    flex: 1;
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    margin-bottom: 20px;
    align-items: flex-end;
    .header-title {
      height: 44px;
      width: 70%;
    }
    .header-viewall {
      width: 42px;
      height: 20px;
    }
  }
  .resultCategory {
    flex: 1;
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    flex-direction: row;
    margin-bottom: 1.25rem;
    .category {
      height: 2.75rem;
      width: 70%;
      background: ${colors.cloudGray};
    }
    .viewall {
      width: 4rem;
      height: 1.25rem;
      background: ${colors.cloudGray};
    }
  }
  .searchFilter {
    flex: 1;
    display: flex;
    justify-content: space-between;
    flex-direction: row;
    margin-bottom: 1.25rem;
    align-items: flex-end;
    .searchFilter-catrgory {
      height: 3.5rem;
      width: 10rem;
      background: ${colors.cloudGray};
    }
    .searchFilter-pages {
      height: 2rem;
      width: 6rem;
      background: ${colors.cloudGray};
    }
  }
  .card {
    flex: 1;
    height: 15.75rem;
    width: 100%;
    padding: 0;
    padding-bottom: 0;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    background: ${colors.cloudGray};

    .heading {
      height: 0.5rem;
      width: 90%;
      background: currentColor;
      max-width: 15rem;
    }

    .image {
      width: 6rem;
      height: 8.75rem;
      background: currentColor;
      margin: auto;
    }

    .bottom {
      border-top: 1px solid currentColor;
      padding: 2.25rem;
      margin: 0 -1.25rem;

      ul li {
        height: 0.5rem;
        width: 50%;
        background: currentColor;
        margin-top: 0.25rem;

        &:last-child {
          width: 75%;
        }
      }
      ul + ul {
        margin-top: 1rem;
      }
    }
  }

  .row {
    margin: -0.125rem;
    display: flex;
    flex-wrap: wrap;

    .col {
      padding: 0.125rem;
      width: 50%;
    }
  }

  .mainShellTitleWrap {
    margin-bottom: 2rem;
    .headTitle {
      height: 1rem;
      width: 100%;
      background: ${colors.cloudGray};
      margin-bottom: 0.75rem;
    }
    .info {
      height: 1rem;
      width: 50%;
      background: ${colors.cloudGray};
    }
  }

  .shine {
    height: 100%;
    width: 100%;
    margin: 0;
    &:after {
      background: ${colors.silverGray};
      z-index: 0 !important;
    }
  }

  .breadCrumb {
    height: 1rem;
    width: 100%;
    margin-bottom: 2.75rem;
    background: ${colors.cloudGray};
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 2rem 4.9rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 4.75rem 4.9rem;
    width: 100%;
    .card {
      height: 16rem;
    }
    .row {
      .col {
        width: 25%;
      }
    }
    .resultCategory {
      margin-top: 0rem;
      margin-bottom: 1.25rem;
      display: flex;
      .category {
        padding-top: 1.25rem;
        width: 21.5rem;
        height: 2.75rem;
      }
      .viewall {
        padding-top: 1.25rem;
        width: 4rem;
        height: 1rem;
        justify-content: space-between;
      }
    }
    .breadCrumb {
      display: none;
    }
    .mainShellTitleWrap {
      .headTitle {
        width: 50%;
      }
      .info {
        width: 30%;
      }
    }
  }
`;

export const StyledFilterHeaderDesktopShell = styled.div`
  /* display: none; */
  padding: 1.3rem 4.9rem 1.25rem;
  background: ${colors.white};
  display: block;
  width: 100%;
  .desktopFilter {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
  .desktopFilter .message {
    padding-top: 1.25rem;
    width: 14.95rem;
    margin-bottom: 0px;
    background: ${colors.cloudGray};
  }
  .desktopFilter .categories {
    width: 28rem;
    display: flex;
    flex-direction: row;
    align-items: center;
    .results {
      height: 1.25rem;
      margin-right: 1rem;
      background: ${colors.cloudGray};
      &:last-child {
        margin-right: 0rem;
      }
    }
  }
  .shine {
    height: 100%;
    width: 100%;
    margin: 0;
    &:after {
      background: ${colors.silverGray};
      z-index: 0 !important;
    }
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: 100%;
  }
`;

export const SearchFilterHeaderDesktopShell = () => {
  return (
    <div>
      <StyledFilterHeaderDesktopShell>
        <div className='desktopFilter'>
          <div className='shine message' />
          <div className='categories'>
            <div className='shine results' />
            <div className='shine results' />
            <div className='shine results' />
            <div className='shine results' />
            <div className='shine results' />
          </div>
        </div>
      </StyledFilterHeaderDesktopShell>
    </div>
  );
};

export const SearchListShell = (props: {
  isVisible: boolean;
  showAppShell: boolean;
}) => {
  return (
    <StyledShell>
      <StyledSearchListShell>
        <div className='visible-lg hidden-xs'>
          <StyledFilters>
            <div className='filter'>
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
            </div>
          </StyledFilters>
        </div>
        {isMobile.phone && (
          <>
            <div className='shine breadCrumb' />
            <div className='searchFilter'>
              <div className='shine searchFilter-catrgory' />
              <div className='shine searchFilter-pages' />
            </div>
          </>
        )}
        <section>
          <div className='row'>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
          </div>
        </section>
      </StyledSearchListShell>
      <div className='visible-xs hidden-lg'>
        {isMobile.phone && props.isVisible && (
          <StyledFilters>
            <div className='filter'>
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
              <div className='shine filter-message' />
            </div>
          </StyledFilters>
        )}
      </div>
    </StyledShell>
  );
};

export const SearchResultShell = (props: {
  isVisible: boolean;
  showAppShell: boolean;
}) => {
  return (
    <StyledShell>
      <StyledSearchListShell>
        {isMobile.phone && <div className='shine breadCrumb' />}
        <div className='resultCategory'>
          <div className='shine category' />
          <div className='shine viewall' />
        </div>
        <section>
          <div className='row'>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
            <div className='col'>
              <div className='card'>
                <div className='shine' />
              </div>
            </div>
          </div>
        </section>
      </StyledSearchListShell>
      <div className='visible-xs hidden-lg'>
        {isMobile.phone && props.isVisible && (
          <StyledFilterHeaderDesktopShell>
            <div className='desktopFilter'>
              <div className='shine message' />
              <div className='categories'>
                <div className='shine results' />
                <div className='shine results' />
                <div className='shine results' />
                <div className='shine results' />
                <div className='shine results' />
              </div>
            </div>
          </StyledFilterHeaderDesktopShell>
        )}
      </div>
    </StyledShell>
  );
  // tslint:disable-next-line: max-file-line-count
};
