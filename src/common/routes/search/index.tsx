import { RootState } from '@common/store/reducers';
import { RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { IMapDispatchToProps, IMapStateToProps } from '@search/types';
import React, { Component, Fragment, ReactNode } from 'react';
import { IGrid, Loader, Utils } from 'dt-components';
import { Helmet } from 'react-helmet';
import Header from '@common/components/Header';
import { createQueryParams, getSelectedFiltersFromParams } from '@search/utils';
import { checkCategoryList, renderCheck } from '@search/utils/renderChecks';
import { mapDispatchToProps, mapStateToProps } from '@search/mapProps';
import BasketStripWrapper from '@common/components/BasketStripWrapper';
import Footer from '@common/components/Footer';
import { SEARCH_ROUTE } from '@search/store/enum';
import {
  SearchFilterHeaderDesktopShell,
  SearchListShell,
  SearchResultShell
} from '@search/index.shell';
import { isMobile } from '@common/utils';
import CommonModal from '@productList/Modals/CommonModal';
import {
  getMetaDescription,
  goToPage,
  loadMoreProductList
} from '@search/utils/productListing';
import {
  StyledContentWrapper,
  StyledSearch,
  StyledSearchList
} from '@search/styles';
import { IItemPerPageOptions } from '@productList/store/types';
import { NOTIFICATION_MEDIUM, PAGE_NUMBER } from '@productList/store/enum';
import {
  EmptySearch,
  Filters,
  SearchList,
  SearchResults
} from '@search/loadable';
import { PAGE_VIEW } from '@events/constants/eventName';

export type IProps = IMapStateToProps &
  IMapDispatchToProps &
  RouteComponentProps;

export class Search extends Component<IProps, {}> {
  scrollAmount = 0;
  locationSearch = '';

  constructor(props: IProps) {
    super(props);
    this.setItemPerPageValue = this.setItemPerPageValue.bind(this);
    this.openNotificationModal = this.openNotificationModal.bind(this);
    this.initSearch = this.initSearch.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.updateShowAppShell = this.updateShowAppShell.bind(this);
    this.props.changeItemPerPage(this.props.productConfiguration.itemPerPage);
  }

  onScroll(): void {
    this.scrollAmount = window.scrollY;
  }

  // @ added by Shubham Batra
  // This code is will resolve the bug https://jira.dtoneapp.telekom.net/browse/ECDEV-3938
  // if Product ask to implement
  //
  // componentWillReceiveProps(nextProps: IProps): void {
  //   const { paginationType } = this.props.productConfiguration;
  //   const { total, data, loading } = nextProps.search;
  //   const callInitailSearchRequest =
  //     paginationType === IGrid.IGridPaginationTypeEnum.numbered &&
  //     total > 0 &&
  //     data.length === 0 &&
  //     !loading;
  //   if (callInitailSearchRequest) {
  //     const { categoryId, ...params } = getSelectedFiltersFromParams(
  //       this.props.location.search
  //     );
  //     this.props.fetchSearchList(PAGE_NUMBER.INITIAL, categoryId, params);
  //   }
  // }

  componentDidMount(): void {
    window.scrollTo({
      top: 0
    });
    window.addEventListener(
      'scroll',
      this.onScroll,
      Utils.passiveEvents
        ? {
            passive: true
          }
        : false
    );

    const position = this.props.lastScrollPositionMap[
      this.props.location.search
    ];
    if (position !== undefined) {
      this.props.clearScrollPosition(this.props.location.search);
    }

    this.initSearch();
    SearchResults.preload();
  }

  // tslint:disable-next-line:cognitive-complexity
  initSearch(): void {
    const {
      currentPage,
      categoryId,
      type,
      ...params
    } = getSelectedFiltersFromParams(this.props.location.search);
    const position = this.props.lastScrollPositionMap[
      this.props.location.search
    ];
    if (position !== undefined) {
      this.props.clearScrollPosition(this.props.location.search);
    }
    const query: string = params.query ? params.query : '';
    if (query.length && query !== '') {
      if (
        ([
          IGrid.IGridPaginationTypeEnum.showMore,
          IGrid.IGridPaginationTypeEnum.infinite
        ] as IGrid.IGridPaginationType[]).includes(
          this.props.productConfiguration.paginationType
        )
      ) {
        this.props.loadMoreSearchList(
          categoryId,
          true, // initial render get all the data
          position,
          params,
          type === SEARCH_ROUTE.SEARCH_RESULT ? 1 : currentPage
        );
      } else {
        this.props.fetchSearchList(
          type === SEARCH_ROUTE.SEARCH_RESULT ? 1 : currentPage,
          categoryId,
          {
            query
          },
          position
        );
      }
      this.props.fetchWithOutConditionCategories({
        query: params.query,
        url: this.props.location.pathname
      });
    } else {
      this.props.sendAllLoadingDisable();
    }
  }

  componentDidUpdate(prevProps: IProps): void {
    const { eQueryParams } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    const {
      currentPage,
      categoryId,
      type,
      ...params
    } = getSelectedFiltersFromParams(this.props.location.search);
    const {
      currentPage: prevCurrentPage,
      categoryId: prevCategoryId,
      type: prevType,
      ...prevParams
    } = getSelectedFiltersFromParams(prevProps.location.search);
    if (
      (currentPage === prevCurrentPage &&
        categoryId === prevCategoryId &&
        type === prevType &&
        params.query === prevParams.query) ||
      this.props.location.pathname.split('/').length !== 2 ||
      eQueryParams
    ) {
      return;
    }
    this.locationSearch = this.props.location.search;
    const position = this.props.lastScrollPositionMap[
      this.props.location.search
    ];
    if (position !== undefined) {
      this.props.clearScrollPosition(this.props.location.search);
    }
    if (
      ([
        IGrid.IGridPaginationTypeEnum.showMore,
        IGrid.IGridPaginationTypeEnum.infinite
      ] as IGrid.IGridPaginationType[]).includes(
        this.props.productConfiguration.paginationType
      )
    ) {
      this.props.loadMoreSearchList(
        categoryId,
        true,
        position,
        params,
        currentPage
      );
    } else {
      this.props.fetchSearchList(
        currentPage,
        categoryId,
        {
          query: params.query
        },
        position
      );
    }
    this.props.fetchWithOutConditionCategories({
      query: params.query,
      url: 'string',
      type: 'none',
      isNewHit: true
    });
  }

  componentWillUnmount(): void {
    this.props.updateShowAppShell(true);
    this.props.resetData();
    this.props.openModal(false);
    window.removeEventListener('scroll', this.onScroll);
    this.props.setScrollPosition(this.locationSearch, this.scrollAmount);
  }

  openNotificationModal(
    event: React.MouseEvent,
    variantId: string,
    deviceName: string
  ): void {
    event.stopPropagation();
    this.props.setNotificationData({
      variantId,
      deviceName,
      mediumValue: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    });
    this.props.openModal(true);
  }

  updateShowAppShell(showAppShell: boolean): void {
    const { updateShowAppShell } = this.props;
    window.scrollTo({
      top: 0
    });
    updateShowAppShell(showAppShell);
  }

  setItemPerPageValue(item: IItemPerPageOptions): void {
    const { categoryId, ...params } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    const query: string = params.query ? params.query : '';
    const search = createQueryParams(query, PAGE_NUMBER.INITIAL, categoryId);
    const { id } = item;
    this.props.changeItemPerPage(id);
    if (`?${search}` !== this.props.location.search) {
      this.props.history.push(`${this.props.location.pathname}?${search}`);
    } else {
      if (
        ([
          IGrid.IGridPaginationTypeEnum.showMore,
          IGrid.IGridPaginationTypeEnum.infinite
        ] as IGrid.IGridPaginationType[]).includes(
          this.props.productConfiguration.paginationType
        )
      ) {
        // filters are updated in this case as itemPerPage are changed so used true
        this.props.loadMoreSearchList(
          categoryId,
          true,
          undefined,
          params,
          PAGE_NUMBER.INITIAL
        );
      } else {
        this.props.fetchSearchList(PAGE_NUMBER.INITIAL, categoryId, params);
      }
    }
  }

  render(): ReactNode {
    const {
      theme,
      search,
      loading,
      searchTranslation,
      showModal,
      showAppShell,
      notificationData,
      formConfiguration,
      sendStockNotification,
      notificationLoading,
      notificationModalType,
      updateShowAppShell,
      openModal,
      setError,
      error,
      catalogConfiguration
    } = this.props;
    const { type, categoryId, ...params } = getSelectedFiltersFromParams(
      this.props.location.search
    );
    const { productTranslation, pagination } = this.props;
    const { selectedCategory } = this.props.search.categories;
    const renderChecks = renderCheck(search.data, loading, showAppShell, type);

    return (
      <Fragment>
        <Helmet
          title={params.query}
          meta={[
            {
              name: 'description',
              content: getMetaDescription(search.categories.categoryList)
            }
          ]}
        />
        <Header showBasketStrip={false} />
        <StyledSearch>
          <StyledContentWrapper>
            <StyledSearchList theme={theme}>
              <CommonModal
                isOpen={showModal}
                deviceName={notificationData.deviceName}
                variantId={notificationData.variantId}
                errorMessage={error && error.message}
                sendStockNotification={sendStockNotification}
                setError={setError}
                notificationLoading={notificationLoading}
                modalType={notificationModalType}
                translation={productTranslation}
                notificationMediumType={notificationData.type}
                notificationMediumValue={notificationData.mediumValue}
                openModal={openModal}
                formConfiguration={formConfiguration}
              />
              {!isMobile.phone && (
                <div>
                  {checkCategoryList(search.categories.categoryList) ? (
                    <Filters
                      isSticky={false}
                      total={search.categories.total}
                      query={params.query}
                      type={type}
                      searchTranslation={searchTranslation}
                      updateShowAppShell={updateShowAppShell}
                    />
                  ) : (
                    <Fragment>
                      {(search.data.length || showAppShell) && (
                        <SearchFilterHeaderDesktopShell />
                      )}
                    </Fragment>
                  )}
                </div>
              )}
              <div>
                {renderChecks.isSearchList && (
                  <SearchList
                    {...this.props}
                    productListLength={search.data.length}
                    categoryName={selectedCategory.categoryName}
                    categorySlug={categoryId}
                    openNotificationModal={this.openNotificationModal}
                    changeItemPerPage={this.setItemPerPageValue}
                    loadMoreProductList={() => {
                      loadMoreProductList(pagination.currentPage);
                    }}
                    goToPage={goToPage}
                    catalogConfiguration={catalogConfiguration}
                  />
                )}
                {renderChecks.isSearchListShell && (
                  <SearchListShell
                    isVisible={
                      !checkCategoryList(search.categories.categoryList)
                    }
                    showAppShell={showAppShell}
                  />
                )}
                {renderChecks.isLoader && (
                  <Loader
                    open={loading}
                    label={productTranslation.loadingText}
                  />
                )}
              </div>
              <div>
                {renderChecks.isSearchResults && (
                  <SearchResults
                    {...this.props}
                    query={params.query}
                    categorySlug={categoryId}
                    openNotificationModal={this.openNotificationModal}
                    categoryName={selectedCategory.categoryName}
                    updateShowAppShell={updateShowAppShell}
                  />
                )}
                {renderChecks.isSearchResultShell && (
                  <SearchResultShell
                    isVisible={
                      !checkCategoryList(search.categories.categoryList)
                    }
                    showAppShell={showAppShell}
                  />
                )}
              </div>
              {renderChecks.isEmptySearch && (
                <EmptySearch
                  queryText={params.query}
                  searchTranslation={searchTranslation}
                />
              )}
            </StyledSearchList>
            {isMobile.phone && (
              <>
                {search.categories.categoryList.length > 0 &&
                  search.data.length > 0 && (
                    <Filters
                      total={search.categories.total}
                      query={params.query}
                      type={type}
                      isSticky={this.props.searchConfiguration.isFilterSticky}
                      searchTranslation={searchTranslation}
                      updateShowAppShell={this.updateShowAppShell}
                    />
                  )}
              </>
            )}
          </StyledContentWrapper>
          <BasketStripWrapper />
        </StyledSearch>
        <Footer pageName={PAGE_VIEW.SEARCH} />
      </Fragment>
    );
  }
}

export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(Search)
  // tslint:disable-next-line: max-file-line-count
);
