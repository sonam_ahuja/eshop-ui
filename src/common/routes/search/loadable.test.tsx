import appState from '@store/states/app';
import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import { StaticRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { PAGE_THEME } from '@common/store/enums/index';
import {
  EmptySearch,
  Filters,
  SearchList,
  SearchResults
} from '@search/loadable';
import { IProps as ISearchListProps } from '@search/SearchList';
import { IProps as ISearchResultsProps } from '@search/SearchResults';
import { IProps as IEmptySearchProps } from '@search/EmptySearch';
import { IProps as IFilterProps } from '@search/Common/Filters';

describe('<Loadable />', () => {
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('EmptySearch loadable', () => {
    const emptySearchProps: IEmptySearchProps = {
      queryText: 'string',
      searchTranslation: appState().translation.cart.global.search
    };
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <EmptySearch {...emptySearchProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('Filters loadable', () => {
    const filterProps: IFilterProps = {
      total: 2,
      searchTranslation: appState().translation.cart.global.search,
      type: undefined,
      query: 'string',
      isSticky: false,
      updateShowAppShell: jest.fn()
    };
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Filters {...filterProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('SearchList loadable', () => {
    const filterProps: ISearchListProps = {
      search: appState().search,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      productListLength: 2,
      categorySlug: 'mobile',
      categoryName: 'mobile',
      loading: true,
      gridLoading: true,
      pagination: {
        currentPage: 2,
        itemsPerPage: 10,
        totalItems: 10
      },
      searchTranslation: appState().translation.cart.global.search,
      productTranslation: appState().translation.cart.productList,
      productConfiguration: appState().configuration.cms_configuration.modules
        .productList,
      theme: PAGE_THEME.PRIMARY,
      onCategorySelect: jest.fn(),
      openNotificationModal: jest.fn(),
      loadMoreProductList: jest.fn(),
      goToPage: jest.fn(),
      setlastListURL: jest.fn(),
      changeItemPerPage: jest.fn(),
      setDeviceDetailedList: jest.fn(),
      catalogConfiguration: appState().configuration.cms_configuration.modules
        .catalog
    };
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SearchList {...filterProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('SearchResults loadable', () => {
    const filterProps: ISearchResultsProps = {
      search: appState().search,
      categorySlug: 'mobile',
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      categoryName: 'mobile',
      loading: true,
      pagination: {
        currentPage: 2,
        itemsPerPage: 10,
        totalItems: 10
      },
      searchTranslation: appState().translation.cart.global.search,
      productTranslation: appState().translation.cart.productList,
      productConfiguration: appState().configuration.cms_configuration.modules
        .productList,
      theme: PAGE_THEME.PRIMARY,
      openNotificationModal: jest.fn(),
      setlastListURL: jest.fn(),
      query: 'string',
      updateShowAppShell: jest.fn(),
      setDeviceDetailedList: jest.fn(),
      catalogConfiguration: appState().configuration.cms_configuration.modules
        .catalog
    };
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SearchResults {...filterProps} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });
});
