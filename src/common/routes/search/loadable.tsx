import React, { FunctionComponent, ReactNode } from 'react';
import Loadable, { LoadingComponentProps } from 'react-loadable';
import { IProps as ISearchListProps } from '@search/SearchList';
import { IProps as ISearchResultsProps } from '@search/SearchResults';
import { IProps as IEmptySearchProps } from '@search/EmptySearch';
import { IProps as IFilterProps } from '@search/Common/Filters';

const Loading: FunctionComponent<LoadingComponentProps> = () => null;

export const SearchList = Loadable<ISearchListProps, {}>({
  loading: Loading,
  loader: () =>
    import(/* webpackChunkName: 'SearchList' */ '@search/SearchList'),
  render(
    loaded: typeof import('@search/SearchList'),
    props: ISearchListProps
  ): ReactNode {
    const SearchListLoaded = loaded.default;

    return <SearchListLoaded {...props} />;
  }
});

export const SearchResults = Loadable<ISearchResultsProps, {}>({
  loading: Loading,
  loader: () =>
    import(/* webpackChunkName: 'SearchResults' */ '@search/SearchResults'),
  render(
    loaded: typeof import('@search/SearchResults'),
    props: ISearchResultsProps
  ): ReactNode {
    const SearchResultsLoaded = loaded.default;

    return <SearchResultsLoaded {...props} />;
  }
});

export const EmptySearch = Loadable<IEmptySearchProps, {}>({
  loading: Loading,
  loader: () =>
    import(/* webpackChunkName: 'EmptySearch' */ '@search/EmptySearch'),
  render(
    loaded: typeof import('@search/EmptySearch'),
    props: IEmptySearchProps
  ): ReactNode {
    const EmptySearchLoaded = loaded.default;

    return <EmptySearchLoaded {...props} />;
  }
});

export const Filters = Loadable<IFilterProps, {}>({
  loading: Loading,
  loader: () =>
    import(/* webpackChunkName: 'Filter' */ '@search/Common/Filters'),
  render(
    loaded: typeof import('@search/Common/Filters'),
    props: IFilterProps
  ): ReactNode {
    const FilterLoaded = loaded.default;

    return <FilterLoaded {...props} />;
  }
});
