import actions from '@search/store/actions';
import { Dispatch } from 'redux';
import {
  ICategoryList,
  IError,
  IFetchCategoriesParams
} from '@search/store/types';
import { RootState } from '@common/store/reducers';
import {
  IGetNotificationRequestTemplatePayload,
  INotificationData
} from '@productList/store/types';
import commonActions from '@common/store/actions/common';

import { IMapDispatchToProps, IMapStateToProps } from './types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  productConfiguration:
    state.configuration.cms_configuration.modules.productList,
  catalogConfiguration: state.configuration.cms_configuration.modules.catalog,
  productTranslation: state.translation.cart.productList,
  globalTranslation: state.translation.cart.global,
  search: state.search,
  lastScrollPositionMap: state.search.lastScrollPositionMap,
  backgroundImage: state.search.backgroundImage,
  theme: state.search.theme,
  loading: state.search.loading,
  gridLoading: state.search.gridLoading,
  currency: state.configuration.cms_configuration.global.currency,
  showAppShell: state.search.showAppShell,
  pagination: state.search.pagination,
  error: state.search.error,
  characteristics: state.search.characteristics,
  showModal: state.search.openModal,
  notificationModalType: state.search.notificationModal,
  formConfiguration:
    state.configuration.cms_configuration.global.loginFormRules,
  notificationData: state.search.notificationData,
  notificationLoading: state.search.notificationLoading,
  searchTranslation: state.translation.cart.global.search,
  searchConfiguration: state.configuration.cms_configuration.global.search
});

export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  openModal(isModalOpen: boolean): void {
    dispatch(actions.openModal(isModalOpen));
  },
  fetchSearchList(
    currentPage: number,
    categoryId: string,
    queryParams: {
      query: string;
    },
    scrollAmount?: number
    // showLoading: boolean
  ): void {
    dispatch(
      actions.fetchSearchList({
        currentPage,
        categoryId,
        queryParams,
        scrollAmount
        // showLoading
      })
    );
  },
  setNotificationData(data: INotificationData): void {
    dispatch(actions.setNotificationData(data));
  },
  loadMoreSearchList(
    categoryId: string,
    isChanged: boolean,
    scrollAmount: number | undefined,
    queryParams: {
      query: string;
    },
    pageNumber: number
    // showLoading: boolean
  ): void {
    dispatch(
      actions.loadMoreSearchList({
        categoryId,
        isChanged,
        scrollAmount,
        queryParams,
        pageNumber
        // showLoading
      })
    );
  },
  sendAllLoadingDisable(): void {
    dispatch(actions.sendAllLoadingDisable());
  },
  changeItemPerPage(itemPerPage: number): void {
    dispatch(actions.changeItemPerPage(itemPerPage));
  },
  setInputContent(value: string): void {
    dispatch(actions.setInputContent(value));
  },
  updateShowAppShell(showAppShell: boolean): void {
    dispatch(actions.updateShowAppShell(showAppShell));
  },

  setError(error: IError): void {
    dispatch(actions.setError(error));
  },
  resetData(): void {
    dispatch(actions.resetData());
  },
  setScrollPosition(urlPath: string, position: number): void {
    dispatch(actions.setScrollPosition({ urlPath, position }));
  },
  clearScrollPosition(urlPath: string): void {
    dispatch(actions.clearScrollPosition({ urlPath }));
  },
  setlastListURL(lastListURL: string): void {
    dispatch(actions.setlastListURL(lastListURL));
  },
  onCategorySelect(selected: ICategoryList): void {
    dispatch(actions.selectedCategories(selected));
  },
  fetchCategories(params: IFetchCategoriesParams): void {
    dispatch(actions.fetchCategories(params));
  },
  setListingEnable(params: boolean): void {
    dispatch(actions.setListingEnable(params));
  },
  setTempText(params: string): void {
    dispatch(actions.setTempText(params));
  },
  fetchWithOutConditionCategories(params: IFetchCategoriesParams): void {
    dispatch(actions.fetchWithOutConditionCategories(params));
  },
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void {
    dispatch(actions.sendStockNotification(payload));
  },
  setSearchPriorityText(search: {
    activeText: string;
    inActiveText: string;
  }): void {
    dispatch(actions.setSearchPriorityText(search));
  },
  setSearchModal(isEnable: boolean): void {
    dispatch(actions.setSearchModal(isEnable));
  },
  setDeviceDetailedList(list: string): void {
    dispatch(commonActions.setDeviceDetailedList(list));
  }
});
