import { StaticRouter } from 'react-router-dom';
import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import { NotificationData } from '@mocks/productList/productList.mock';
import configureStore from 'redux-mock-store';
import appState from '@store/states/app';
import { MODAL_TYPE } from '@productList/store/enum';
import { histroyParams } from '@mocks/common/histroy';
import { PAGE_THEME } from '@common/store/enums/index';
import { IProps, Search } from '@search/index';

describe('<Search Mobile />', () => {
  const props: IProps = {
    ...histroyParams,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'pl' },
    searchTranslation: appState().translation.cart.global.search,
    searchConfiguration: appState().configuration.cms_configuration.global
      .search,
    search: appState().search,
    notificationLoading: false,
    loading: true,
    clearScrollPosition: jest.fn(),
    sendAllLoadingDisable: jest.fn(),
    gridLoading: true,
    showAppShell: true,
    lastScrollPositionMap: { urlPath: 2 },
    setScrollPosition: jest.fn(),
    error: null,
    backgroundImage: 'https://i.ibb.co/mHxM497/Untitled-1.jpg',
    theme: PAGE_THEME.PRIMARY,
    pagination: {
      currentPage: 2,
      itemsPerPage: 10,
      totalItems: 10
    },
    globalTranslation: appState().translation.cart.global,
    productTranslation: appState().translation.cart.productList,
    productConfiguration: appState().configuration.cms_configuration.modules
      .productList,
    catalogConfiguration: appState().configuration.cms_configuration.modules
      .catalog,
    characteristics: [{ name: 'key', values: [{ value: 'value1' }] }],
    notificationModalType: MODAL_TYPE.CONFIRMATION_MODAL,
    notificationData: NotificationData,
    showModal: true,
    formConfiguration: appState().configuration.cms_configuration.global
      .loginFormRules,

    loadMoreSearchList: jest.fn(),
    fetchSearchList: jest.fn(),
    changeItemPerPage: jest.fn(),
    updateShowAppShell: jest.fn(),
    sendStockNotification: jest.fn(),
    setInputContent: jest.fn(),
    setTempText: jest.fn(),
    setListingEnable: jest.fn(),
    fetchWithOutConditionCategories: jest.fn(),
    setSearchModal: jest.fn(),
    setSearchPriorityText: jest.fn(),
    onCategorySelect: jest.fn(),
    fetchCategories: jest.fn(),
    setError: jest.fn(),
    openModal: jest.fn(),
    setlastListURL: jest.fn(),
    setNotificationData: jest.fn(),
    resetData: jest.fn(),
    setDeviceDetailedList: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Search {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });
});
