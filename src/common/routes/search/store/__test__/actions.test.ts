import searchActions from '@search/store/actions';
import configureMockStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';
import productListSaga from '@search/store/sagas';

describe('actions test', () => {
  it('changeItemPerPage action creator should return a object with expected value', () => {
    const sagaMiddleware = createSagaMiddleware();
    const mockStore = configureMockStore([sagaMiddleware]);
    const store = mockStore({});
    sagaMiddleware.run(productListSaga);
    const newState = store.dispatch(searchActions.changeItemPerPage(1));
    expect(newState).toBeDefined();
  });
});
