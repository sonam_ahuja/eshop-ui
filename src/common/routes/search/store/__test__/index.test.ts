import searchState from '@search/store/state';
import { searchReducer } from '@src/common/routes/search/store';

describe('search reducer', () => {
  it('search reducer test', () => {
    const action: { type: string } = { type: '' };
    expect(searchReducer(searchState(), action)).toEqual(searchState());
  });
});
