import searchReducer from '@search/store/reducer';
import searchState from '@search/store/state';
import { IFetchSearchListParams, ISearchState } from '@search/store/types';
import actions from '@search/store/actions';
import {
  ErrorData,
  ProductListResponse,
  SendStockPayload
} from '@mocks/productList/productList.mock';
import { INotificationData } from '@productList/store/types';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';

describe('Search Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ISearchState = searchReducer(undefined, definedAction);
  let newState: ISearchState = searchState();
  const initializeValue = () => {
    initialStateValue = searchReducer(undefined, definedAction);
    newState = searchState();
  };
  beforeEach(() => {
    initializeValue();
  });

  test('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  test('FETCH_SEARCH_LIST_REQUESTED should mutate state', () => {
    const params: IFetchSearchListParams = {
      queryParams: { query: 'string' },
      currentPage: 1,
      categoryId: 'Mobile-Devices',
      scrollAmount: 2
    };
    const expectedData = { ...newState };
    const action = actions.fetchSearchList(params);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('CLEAR_SEARCH_LIST_DATA should mutate state', () => {
    const expectedData = { ...newState };
    const action = actions.clearSearchListData();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_SEARCH_LIST_SUCCESS should mutate state', () => {
    const action = actions.fetchSearchListSuccess(ProductListResponse);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  test('FETCH_SEARCH_LIST_LOADING should mutate state, when gridLoading is true', () => {
    const mutation: {
      gridLoading: boolean;
      error: null;
    } = {
      gridLoading: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchSearchListLoading(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_SEARCH_LIST_LOADING should mutate state,  when gridLoading is false', () => {
    const mutation: {
      gridLoading: boolean;
      loading: boolean;
      error: null;
    } = {
      gridLoading: false,
      loading: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchSearchListLoading(false);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_SEARCH_LIST_ERROR should mutate state', () => {
    const mutation: {
      loading: boolean;
      error?: { code: number; message: string };
      showAppShell: boolean;
    } = {
      loading: false,
      error: { code: 0, message: 'message' },
      showAppShell: false
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchSearchListError(ErrorData);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('CHANGE_ITEM_PER_PAGE should mutate state', () => {
    const mutation: {
      pagination: {
        itemsPerPage: number;
        currentPage: number;
        totalItems: number;
      };
    } = {
      pagination: { itemsPerPage: 2, currentPage: 0, totalItems: 0 }
    };
    const expectedData = { ...newState, ...mutation };

    const action = actions.changeItemPerPage(2);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('UPDATE_SHOW_APP_SHELL should mutate state', () => {
    const mutation: {
      showAppShell: boolean;
    } = {
      showAppShell: true
    };
    const expectedData = { ...newState, ...mutation };

    const action = actions.updateShowAppShell(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('OPEN_MODAL should mutate state when modal is open', () => {
    const mutation: { openModal: boolean; error: null } = {
      openModal: true,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.openModal(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('OPEN_MODAL should mutate state, when modal is close', () => {
    const mutation: { openModal: boolean; error: null } = {
      openModal: false,
      error: null
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.openModal(false);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('RESET_DATA should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.resetData();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_SCROLL_POSITION should reset data', () => {
    const expectedData = {
      ...newState,
      lastScrollPositionMap: {
        '/basket': 23
      }
    };
    const action = actions.setScrollPosition({
      urlPath: '/basket',
      position: 23
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_LAST_PAGE_NUMBER should reset data', () => {
    const expectedData = { ...newState, lastListURL: '/checkout' };
    const action = actions.setlastListURL('/checkout');
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('CLEAR_SCROLL_POSITION should reset data', () => {
    const expectedData = { ...newState };
    const action = actions.clearScrollPosition({ urlPath: '/checkout' });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SEND_STOCK_NOTIFICATION should mutate state', () => {
    const mutation: INotificationData = {
      deviceName: '',
      mediumValue: '12',
      variantId: '',
      type: NOTIFICATION_MEDIUM.EMAIL
    };
    const expectedData = { ...newState, notificationData: { ...mutation } };

    const action = actions.sendStockNotification(SendStockPayload);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
});
