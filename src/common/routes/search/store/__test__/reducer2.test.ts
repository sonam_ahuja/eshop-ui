import searchReducer from '@search/store/reducer';
import searchState from '@search/store/state';
import { ISearchState } from '@search/store/types';
import actions from '@search/store/actions';
import {
  ErrorData,
  NotificationData
} from '@mocks/productList/productList.mock';
import { MODAL_TYPE } from '@productList/store/enum';

// tslint:disable-next-line: no-big-function
describe('Search Reducer 2', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ISearchState = searchReducer(undefined, definedAction);
  let newState: ISearchState = searchState();
  const initializeValue = () => {
    initialStateValue = searchReducer(undefined, definedAction);
    newState = searchState();
  };
  beforeEach(() => {
    initializeValue();
  });

  test('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  test('SEND_STOCK_NOTIFICATION_LOADING should mutate state, when modal is close', () => {
    const action = actions.sendStockNotificationLoading();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  test('SEND_STOCK_NOTIFICATION_ERROR should mutate state, when modal is close', () => {
    const action = actions.sendStockNotificationError(ErrorData);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });

  test('SET_NOTIFICATION_DATA should mutate state, when modal is close', () => {
    const expectedData = {
      ...newState,
      notificationData: { ...NotificationData }
    };
    const action = actions.setNotificationData(NotificationData);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_ERROR should mutate state, when modal is close', () => {
    const expectedData = {
      ...newState,
      error: {
        ...ErrorData
      }
    };
    const action = actions.setError(ErrorData);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SEND_STOCK_NOTIFICATION_SUCCESS should mutate state', () => {
    const mutation: { notificationModal: MODAL_TYPE } = {
      notificationModal: MODAL_TYPE.CONFIRMATION_MODAL
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.sendStockNotificationSuccess();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_SUGGESTION_LOADING should mutate state', () => {
    const expectedData = {
      ...newState,
      suggestion: { ...newState.suggestion, loading: true }
    };
    const action = actions.fetchSuggestionLoading(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_SUGGESTION_ERROR should mutate state', () => {
    const expectedData: ISearchState = {
      ...newState,
      suggestion: {
        ...newState.suggestion,
        error: { code: 404, message: 'error' }
      }
    };
    const action = actions.fetchSuggestionError({
      code: 404,
      message: 'error'
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_SUGGESTION_SUCCESS should mutate state', () => {
    const data: string[] = ['Samsung black', 'Samsung 32 gb'];
    const expectedData = {
      ...newState,
      suggestion: {
        ...newState.suggestion,
        data
      }
    };
    const action = actions.fetchSuggestionSuccess(data);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('CLEAR_SUGGESTION_DATA should mutate state', () => {
    const expectedData = {
      ...newState
    };
    const action = actions.clearSuggestionData();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_CATEGORIES_LOADING should mutate state', () => {
    const expectedData = {
      ...newState,
      categories: {
        ...newState.categories,
        loading: true
      }
    };
    const action = actions.fetchCategoriesLoading(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_CATEGORIES_ERROR should mutate state', () => {
    const expectedData = {
      ...newState,
      categories: {
        ...newState.categories,
        error: { code: 404, message: 'error' }
      }
    };
    const action = actions.fetchCategoriesError({
      code: 404,
      message: 'error'
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_SEARCH_MODAL should mutate state', () => {
    const expectedData = {
      ...newState,
      searchModal: {
        ...newState.searchModal,
        modalOpen: true
      }
    };
    const action = actions.setSearchModal(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_TEMP_TEXT should mutate state', () => {
    const expectedData = {
      ...newState,
      searchFilterSetting: {
        ...newState.searchFilterSetting,
        originalContent: 'samsung'
      }
    };
    const action = actions.setTempText('samsung');
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_INPUT_WITH_NO_TEXT should mutate state', () => {
    const expectedData = {
      ...newState,
      searchFilterSetting: {
        ...newState.searchFilterSetting,
        isInputWithNoText: true
      }
    };
    const action = actions.setInputWithNoText(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('FETCH_CATEGORIES_SUCCESS should mutate state', () => {
    const expectedData = {
      ...newState,
      categories: {
        ...newState.categories,
        categoryList: [
          {
            categoryId: 'string',
            categoryName: 'string',
            categorySlug: 'string',
            count: 2
          }
        ]
      },
      total: 0
    };
    const action = actions.fetchCategoriesSuccess({
      categoryList: [
        {
          categoryId: 'string',
          categoryName: 'string',
          categorySlug: 'string',
          count: 2
        }
      ],
      total: 0
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('CLEAR_CATEGORIES_DATA should mutate state', () => {
    const expectedData = {
      ...newState
    };
    const action = actions.clearCategoriesData();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SELECTED_CATEGORIES should mutate state', () => {
    const expectedData = {
      ...newState,
      categories: {
        ...newState.categories,
        selectedCategory: {
          categoryId: 'string',
          categoryName: 'string',
          categorySlug: 'string',
          count: 2
        }
      }
    };
    const action = actions.selectedCategories({
      categoryId: 'string',
      categoryName: 'string',
      categorySlug: 'string',
      count: 2
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_SPEECH_RECOGNITION should mutate state', () => {
    const expectedData = {
      ...newState,
      categories: {
        ...newState.categories,
        selectedCategory: {
          autoStart: true,
          continuous: true
        }
      }
    };
    const action = actions.setSpeechRecognition({
      autoStart: true,
      continuous: true
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_INPUT_CONTENT should mutate state', () => {
    const expectedData = {
      ...newState,
      searchFilterSetting: {
        ...newState.searchFilterSetting,
        inputContent: 'string'
      }
    };
    const action = actions.setInputContent('string');
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_LISTING_ENABLE should mutate state', () => {
    const expectedData = {
      ...newState,
      searchFilterSetting: {
        ...newState.searchFilterSetting,
        showListing: true
      }
    };
    const action = actions.setListingEnable(true);
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_SEARCH_PRIORITY_TEXT should mutate state', () => {
    const expectedData = {
      ...newState,
      search: {
        activeText: 'string',
        inActiveText: 'string'
      }
    };
    const action = actions.setSearchPriorityText({
      activeText: 'string',
      inActiveText: 'string'
    });
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  test('SET_ALL_LOADING_DISABLE should mutate state', () => {
    const action = actions.sendAllLoadingDisable();
    const newMutatedState = searchReducer(initialStateValue, action);
    expect(newMutatedState).toBeDefined();
  });
  // tslint:disable-next-line: max-file-line-count
});
