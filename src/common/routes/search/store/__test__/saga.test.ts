import {
  fetchCategories,
  fetchCategoriesWithCondtion,
  fetchSearchList,
  fetchSuggestion,
  loadMoreSearchList,
  loadMoreSetPreviousPageData,
  scrollPage,
  sendStockNotification
} from '@search/store/sagas';
import constants from '@search/store/constants';
import { NOTIFICATION_MEDIUM } from '@productList/store/enum';
import { searchSaga } from '@src/common/routes/search/store';

import {
  IFetchCategoriesParams,
  IFetchSearchListParams,
  ILoadMoreSearchListParams
} from '../types';

describe('Search Saga', () => {
  window.scrollTo = jest.fn();
  test('search saga test', () => {
    const generator = searchSaga();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  test('fetchSuggestion saga test', () => {
    const action: {
      type: string;
      payload: string;
    } = {
      type: constants.FETCH_SUGGESTION_REQUESTED,
      payload: 'string'
    };

    const generator = fetchSuggestion(action);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  test('fetchCategoriesWithCondtion saga test', () => {
    const action: {
      type: string;
      payload: IFetchCategoriesParams;
    } = {
      type: constants.FETCH_SUGGESTION_REQUESTED,
      payload: {
        query: 'string',
        url: 'string',
        type: 'string',
        isNewHit: true
      }
    };

    const generator = fetchCategoriesWithCondtion(action);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  test('fetchCategories saga test', () => {
    const action: {
      type: string;
      payload: IFetchCategoriesParams;
    } = {
      type: constants.FETCH_SUGGESTION_REQUESTED,
      payload: {
        query: 'string',
        url: 'string',
        type: 'string',
        isNewHit: true
      }
    };

    const generator = fetchCategories(action);
    expect(generator.next().value).toBeUndefined();
  });

  test('loadMoreSetPreviousPageData saga test', () => {
    const payload: ILoadMoreSearchListParams = {
      categoryId: 'string',
      isChanged: true,
      scrollAmount: 32,
      pageNumber: 3,
      queryParams: {
        query: 'string'
      }
    };
    const generator = loadMoreSetPreviousPageData(payload);
    expect(generator.next().value).toBeDefined();
  });

  test('fetchSearchList saga test', () => {
    const action: {
      type: string;
      payload: IFetchSearchListParams;
    } = {
      type: constants.FETCH_SEARCH_LIST_REQUESTED,
      payload: {
        queryParams: { query: 'string' },
        currentPage: 2,
        categoryId: 'Mobile',
        scrollAmount: 2
      }
    };

    const generator = fetchSearchList(action);
    expect(generator.next().value).toBeDefined();
  });

  test('loadMoreSearchList saga test', () => {
    const action: {
      type: string;
      payload: ILoadMoreSearchListParams;
    } = {
      type: constants.FETCH_SEARCH_LIST_REQUESTED,
      payload: {
        queryParams: { query: 'string' },
        categoryId: 'Mobile',
        scrollAmount: 2,
        isChanged: true,
        pageNumber: 1
      }
    };

    const generator = loadMoreSearchList(action);
    expect(generator.next().value).toBeDefined();
  });

  test('scrollPage saga test', () => {
    const generator = scrollPage(34, 34);
    expect(generator.next().value).toBeUndefined();
  });

  test('sendStockNotification saga test', () => {
    const generator = sendStockNotification({
      type: 'string',
      payload: {
        type: NOTIFICATION_MEDIUM.EMAIL,
        variantId: 'string',
        mediumValue: 'string'
      }
    });
    expect(generator.next().value).toBeDefined();
  });
});
