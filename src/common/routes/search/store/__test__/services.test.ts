import {
  fetchCategoryListService,
  fetchSearchListService,
  fetchSearchService,
  fetchSearchSuggestService
} from '@search/store/services';
import { apiEndpoints } from '@common/constants';
import searchState from '@search/store/state';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

describe('Search Service', () => {
  const mock = new MockAdapter(axios);
  test('fetchSearchListService test', async () => {
    const payload = {
      query: '',
      currentPage: 3,
      itemsPerPage: 10,
      categoryId: '1234'
    };
    const url: string = apiEndpoints.SEARCH.GET_SEARCH_LIST.url;
    const expectedData = searchState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchSearchListService(payload);
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  test('fetchSearchService test', async () => {
    const url: string = apiEndpoints.SEARCH.GET_SEARCH.url('query');
    const expectedData = searchState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchSearchService('string');
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  test('fetchSearchSuggestService test', async () => {
    const url: string = apiEndpoints.SEARCH.GET_SEARCH_SUGGEST.url('query');
    const expectedData = searchState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchSearchSuggestService('string');
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  test('fetchSearchSuggestService test', async () => {
    const url: string = apiEndpoints.SEARCH.GET_SEARCH_SUGGEST.url('query');
    const expectedData = searchState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchCategoryListService('query');
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });
});
