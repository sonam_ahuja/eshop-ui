import { searchSuggestionResolver } from '@search/store/transformer';
import appState from '@store/states/app';

window.localStorage.setItem(
  'suggestion',
  `[{"suggestionText":"samsung","timestamp":1560789520604},
   {"suggestionText":"samsung1","timestamp":1560789520604}]`
);
describe('Transformer', () => {
  test('searchSuggestionResolver filter, when local storage has data ::', () => {
    const result = searchSuggestionResolver(
      ['string', 'string'],
      appState().translation.cart.global.search.searchQueries,
      'string',
      'string',
      true,
      2
    );
    expect(
      searchSuggestionResolver(
        ['string', 'string'],
        appState().translation.cart.global.search.searchQueries,
        'string',
        'string',
        true,
        2
      )
    ).toMatchObject(result);
  });

  // tslint:disable-next-line: no-identical-functions
  test('searchSuggestionResolver filter, when local storage has data ::', () => {
    const result = searchSuggestionResolver(
      ['string', 'string'],
      appState().translation.cart.global.search.searchQueries,
      'string',
      '',
      true,
      2
    );
    expect(
      searchSuggestionResolver(
        ['string', 'string'],
        appState().translation.cart.global.search.searchQueries,
        'string',
        '',
        true,
        2
      )
    ).toMatchObject(result);
  });

  test('searchSuggestionResolver filter, when suggestion listing is false with localstorage data ::', () => {
    const result = searchSuggestionResolver(
      ['string', 'string'],
      appState().translation.cart.global.search.searchQueries,
      'string',
      'string',
      false,
      2
    );
    expect(
      searchSuggestionResolver(
        ['string', 'string'],
        appState().translation.cart.global.search.searchQueries,
        'string',
        'string',
        false,
        2
      )
    ).toMatchObject(result);
  });

  test('searchSuggestionResolver filter, when has no local storage has data ::', () => {
    window.localStorage.removeItem('suggestion');
    const data = ['string', 'string'];
    const result = searchSuggestionResolver(
      data,
      appState().translation.cart.global.search.searchQueries,
      'string',
      'string',
      true,
      2
    );
    expect(
      searchSuggestionResolver(
        ['string', 'string'],
        appState().translation.cart.global.search.searchQueries,
        'string',
        'string',
        true,
        2
      )
    ).toMatchObject(result);
  });

  test('searchSuggestionResolver filter, when suggestion listing is false with blank localstorage ::', () => {
    const data = 'string';
    const result = searchSuggestionResolver(
      ['string', 'string'],
      appState().translation.cart.global.search.searchQueries,
      data,
      'string',
      false,
      2
    );
    expect(
      searchSuggestionResolver(
        ['string', 'string'],
        appState().translation.cart.global.search.searchQueries,
        'string',
        'string',
        false,
        2
      )
    ).toMatchObject(result);
  });
});
