import {
  getConfiguration,
  getState,
  getTranslation
} from '@search/store/utils';
import appState from '@store/states/app';

describe('Search Utils', () => {
  test('getConfiguration test', () => {
    const result = getConfiguration(appState());
    expect(getConfiguration(appState())).toMatchObject(result);
  });

  test('getState test', () => {
    const result = getState(appState());
    expect(getState(appState())).toMatchObject(result);
  });

  test('getState test', () => {
    const result = getTranslation(appState());
    expect(getTranslation(appState())).toMatchObject(result);
  });
});
