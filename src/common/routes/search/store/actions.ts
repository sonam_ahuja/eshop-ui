import { actionCreator } from '@src/common/utils/actionCreator';
import CONSTANTS from '@search/store/constants';
import {
  ICategoryList,
  ICategoryListResponse,
  IError,
  IFetchCategoriesParams,
  IFetchSearchListParams,
  ILoadMoreSearchListParams
} from '@search/store/types';
import * as searchListApi from '@common/types/api/search/searchList';
import {
  IGetNotificationRequestTemplatePayload,
  INotificationData
} from '@productList/store/types';

export default {
  fetchSearchList: actionCreator<IFetchSearchListParams>(
    CONSTANTS.FETCH_SEARCH_LIST_REQUESTED
  ),
  fetchSearchListLoading: actionCreator<boolean>(
    CONSTANTS.FETCH_SEARCH_LIST_LOADING
  ),
  fetchSearchListError: actionCreator<IError>(
    CONSTANTS.FETCH_SEARCH_LIST_ERROR
  ),
  fetchSearchListSuccess: actionCreator<searchListApi.POST.IResponse>(
    CONSTANTS.FETCH_SEARCH_LIST_SUCCESS
  ),
  clearSearchListData: actionCreator(CONSTANTS.CLEAR_SEARCH_LIST_DATA),
  loadMoreSearchList: actionCreator<ILoadMoreSearchListParams>(
    CONSTANTS.LOAD_MORE_SEARCH_LIST_REQUESTED
  ),

  setError: actionCreator<IError>(CONSTANTS.SET_ERROR),
  setScrollPosition: actionCreator<{ urlPath: string; position: number }>(
    CONSTANTS.SET_SCROLL_POSITION
  ),
  clearScrollPosition: actionCreator<{ urlPath: string }>(
    CONSTANTS.CLEAR_SCROLL_POSITION
  ),
  setlastListURL: actionCreator<string>(CONSTANTS.SET_LAST_PAGE_NUMBER),
  resetData: actionCreator(CONSTANTS.RESET_DATA),
  changeItemPerPage: actionCreator<number>(CONSTANTS.CHANGE_ITEM_PER_PAGE),
  updateItemPerPage: actionCreator<number>(CONSTANTS.UPDATE_ITEM_PER_PAGE),
  updateShowAppShell: actionCreator<boolean>(CONSTANTS.UPDATE_SHOW_APP_SHELL),

  fetchSuggestion: actionCreator<string>(CONSTANTS.FETCH_SUGGESTION_REQUESTED),
  fetchSuggestionLoading: actionCreator<boolean>(
    CONSTANTS.FETCH_SUGGESTION_LOADING
  ),
  fetchSuggestionError: actionCreator<IError>(CONSTANTS.FETCH_SUGGESTION_ERROR),
  fetchSuggestionSuccess: actionCreator<string[]>(
    CONSTANTS.FETCH_SUGGESTION_SUCCESS
  ),
  clearSuggestionData: actionCreator(CONSTANTS.CLEAR_SUGGESTION_DATA),
  setInputContent: actionCreator<string>(CONSTANTS.SET_INPUT_CONTENT),

  fetchCategories: actionCreator<IFetchCategoriesParams>(
    CONSTANTS.FETCH_CATEGORIES_REQUESTED
  ),
  fetchWithOutConditionCategories: actionCreator<IFetchCategoriesParams>(
    CONSTANTS.FETCH_CATEGORIES_WITHOUT_CONDITION_REQUESTED
  ),
  fetchCategoriesLoading: actionCreator<boolean>(
    CONSTANTS.FETCH_CATEGORIES_LOADING
  ),
  setListingEnable: actionCreator<boolean>(CONSTANTS.SET_LISTING_ENABLE),
  setInputWithNoText: actionCreator<boolean>(CONSTANTS.SET_INPUT_WITH_NO_TEXT),
  setTempText: actionCreator<string>(CONSTANTS.SET_TEMP_TEXT),
  fetchCategoriesError: actionCreator<IError>(CONSTANTS.FETCH_CATEGORIES_ERROR),
  fetchCategoriesSuccess: actionCreator<ICategoryListResponse>(
    CONSTANTS.FETCH_CATEGORIES_SUCCESS
  ),
  clearCategoriesData: actionCreator(CONSTANTS.CLEAR_CATEGORIES_DATA),
  selectedCategories: actionCreator<ICategoryList>(
    CONSTANTS.SELECTED_CATEGORIES
  ),
  setSearchModal: actionCreator<boolean>(CONSTANTS.SET_SEARCH_MODAL),
  sendStockNotification: actionCreator<IGetNotificationRequestTemplatePayload>(
    CONSTANTS.SEND_STOCK_NOTIFICATION
  ),
  sendStockNotificationError: actionCreator<IError>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_ERROR
  ),
  sendStockNotificationSuccess: actionCreator<void>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_SUCCESS
  ),
  sendStockNotificationLoading: actionCreator<void>(
    CONSTANTS.SEND_STOCK_NOTIFICATION_LOADING
  ),
  sendAllLoadingDisable: actionCreator<void>(CONSTANTS.SET_ALL_LOADING_DISABLE),
  setSpeechRecognition: actionCreator<{
    autoStart: boolean;
    continuous: boolean;
  }>(CONSTANTS.SET_SPEECH_RECOGNITION),
  openModal: actionCreator<boolean>(CONSTANTS.OPEN_MODAL),
  setSearchPriorityText: actionCreator<{
    activeText: string;
    inActiveText: string;
  }>(CONSTANTS.SET_SEARCH_PRIORITY_TEXT),
  setNotificationData: actionCreator<INotificationData>(
    CONSTANTS.SET_NOTIFICATION_DATA
  )
};
