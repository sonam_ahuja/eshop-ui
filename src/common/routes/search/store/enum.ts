export const SEARCH_ROUTE = {
  SEARCH: '/search',
  SEARCH_RESULT: 'searchResult'
};

export enum SUGGESTION_STATUS {
  MODIFIED = 'MODIFIED',
  MATCH = 'MATCH',
  UNMODIFIED = 'UNMODIFIED'
}

export enum SUGGESTION_TYPES {
  LOCAL = 'LOCAL',
  RESPONSE = 'RESPONSE',
  DEFAULT = 'DEFAULT'
}

export enum KEY_CODE {
  ARROW_UP = 38,
  ARROW_RIGHT = 39,
  ARROW_DOWN = 40,
  ENTER = 13
}

export enum DEFAULT_CATEGORY {
  DEVICES_MOBILE = 'Devices-Mobile'
}

export enum INITIAL_VALUE {
  BLANK = ''
}
