import withProduce from '@utils/withProduce';
import initialState from '@search/store/state';
import {
  ICategoryList,
  ICategoryListResponse,
  IError,
  ISearchListResponse,
  ISearchState
} from '@search/store/types';
import {
  IGetNotificationRequestTemplatePayload,
  ILastScrollPositionMap,
  INotificationData
} from '@productList/store/types';
import { AnyAction, Reducer } from 'redux';
import CONSTANTS from '@search/store/constants';
import { getCharacterstic } from '@productList/store/utils';
import appKeys from '@src/common/constants/appkeys';
import history from '@src/client/history';
import { getSelectedFiltersFromParams } from '@search/utils';
import { MODAL_TYPE } from '@productList/store/enum';

const reducers: object = {
  [CONSTANTS.CLEAR_SEARCH_LIST_DATA]: (state: ISearchState) => {
    state.data = [];
  },
  [CONSTANTS.FETCH_SEARCH_LIST_SUCCESS]: (
    state: ISearchState,
    payload: ISearchListResponse
  ) => {
    const {
      data,
      itemsPerPage,
      pageNumber,
      resultCount,
      characteristics
    } = payload;
    Object.assign(state, {
      data,
      pagination: {
        currentPage: pageNumber,
        itemsPerPage,
        totalItems: resultCount
      },
      loading: false,
      gridLoading: false,
      error: null,
      showAppShell: false,
      characteristics,
      total: resultCount,
      theme: getCharacterstic(characteristics, appKeys.PRODUCT_LIST_THEME),
      backgroundImage: getCharacterstic(
        characteristics,
        appKeys.PRODUCT_LIST_BACKGROUND_IMAGE
      )
    });
  },
  [CONSTANTS.FETCH_SEARCH_LIST_ERROR]: (state: ISearchState, error: Error) => {
    state.loading = false;
    state.gridLoading = false;
    state.showAppShell = false;
    if (error && error.message) {
      state.error = { message: '', code: 0 };
      state.error.message = error.message;
    }
  },
  [CONSTANTS.FETCH_SEARCH_LIST_LOADING]: (
    state: ISearchState,
    gridLoading: boolean
  ) => {
    if (gridLoading) {
      state.gridLoading = true;
    } else {
      state.loading = true;
    }
    state.error = null;
  },
  [CONSTANTS.CHANGE_ITEM_PER_PAGE]: (state: ISearchState, payload: number) => {
    state.pagination.itemsPerPage = payload;
  },
  [CONSTANTS.UPDATE_SHOW_APP_SHELL]: (
    state: ISearchState,
    payload: boolean
  ) => {
    state.showAppShell = payload;
  },
  [CONSTANTS.SET_ERROR]: (state: ISearchState, error: IError) => {
    state.error = error;
  },
  [CONSTANTS.RESET_DATA]: (state: ISearchState) => {
    state.data = [];
  },
  [CONSTANTS.SET_SCROLL_POSITION]: (
    state: ISearchState,
    payload: {
      urlPath: string;
      position: number;
    }
  ) => {
    const { urlPath, position } = payload;
    state.lastScrollPositionMap = {
      ...state.lastScrollPositionMap,
      [urlPath]: position
    };
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION]: (
    state: ISearchState,
    payload: IGetNotificationRequestTemplatePayload
  ) => {
    state.notificationData.mediumValue = payload.mediumValue;
    state.notificationData.type = payload.type;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_SUCCESS]: (state: ISearchState) => {
    state.notificationModal = MODAL_TYPE.CONFIRMATION_MODAL;
    state.notificationLoading = false;
  },
  [CONSTANTS.SEND_STOCK_NOTIFICATION_ERROR]: (state: ISearchState) => {
    state.notificationLoading = false;
  },
  [CONSTANTS.SET_ERROR]: (state: ISearchState, error: IError) => {
    state.error = error;
  },
  [CONSTANTS.OPEN_MODAL]: (state: ISearchState, isModalOpen: boolean) => {
    state.openModal = isModalOpen;
    state.error = null;
    state.notificationModal = isModalOpen
      ? state.notificationModal
      : MODAL_TYPE.SEND_NOTIFICATION_MODAL;
  },
  [CONSTANTS.SET_NOTIFICATION_DATA]: (
    state: ISearchState,
    notificationData: INotificationData
  ) => {
    state.notificationData = notificationData;
  },
  [CONSTANTS.CLEAR_SCROLL_POSITION]: (
    state: ISearchState,
    payload: {
      urlPath: string;
    }
  ) => {
    const { urlPath } = payload;
    const newMap: ILastScrollPositionMap = {};
    Object.keys(state.lastScrollPositionMap).forEach(key => {
      if (key !== urlPath) {
        newMap[key] = state.lastScrollPositionMap[key];
      }
    });
    state.lastScrollPositionMap = newMap;
    // const { [urlPath], ...rest} = state.lastScrollPositionMap;
    // state.lastScrollPositionMap = {
    //   ...rest
    // };
  },
  [CONSTANTS.SET_LAST_PAGE_NUMBER]: (state: ISearchState, payload: string) => {
    state.lastListURL = payload;
  },
  [CONSTANTS.FETCH_SUGGESTION_LOADING]: (
    state: ISearchState,
    payload: boolean
  ) => {
    state.suggestion.loading = payload;
  },
  [CONSTANTS.FETCH_SUGGESTION_ERROR]: (
    state: ISearchState,
    payload: IError
  ) => {
    state.suggestion.loading = false;
    state.suggestion.error = payload;
  },
  [CONSTANTS.FETCH_SUGGESTION_SUCCESS]: (
    state: ISearchState,
    payload: string[]
  ) => {
    state.suggestion.data = payload;
  },
  [CONSTANTS.CLEAR_SUGGESTION_DATA]: (state: ISearchState) => {
    state.suggestion.data = [];
  },

  [CONSTANTS.FETCH_CATEGORIES_LOADING]: (
    state: ISearchState,
    payload: boolean
  ) => {
    state.categories.loading = payload;
  },
  [CONSTANTS.FETCH_CATEGORIES_ERROR]: (
    state: ISearchState,
    payload: IError
  ) => {
    state.categories.loading = false;
    state.categories.error = payload;
  },
  [CONSTANTS.SET_SEARCH_MODAL]: (state: ISearchState, payload: boolean) => {
    state.searchModal.modalOpen = payload;
  },
  [CONSTANTS.SET_TEMP_TEXT]: (state: ISearchState, payload: string) => {
    state.searchFilterSetting.originalContent = payload;
  },
  [CONSTANTS.SET_INPUT_WITH_NO_TEXT]: (
    state: ISearchState,
    payload: boolean
  ) => {
    state.searchFilterSetting.isInputWithNoText = payload;
  },
  [CONSTANTS.FETCH_CATEGORIES_SUCCESS]: (
    state: ISearchState,
    payload: ICategoryListResponse
  ) => {
    state.categories.categoryList = payload.categoryList;
    state.categories.total = payload.total;
    if (history && payload.categoryList.length) {
      const { categoryId } = getSelectedFiltersFromParams(
        history.location.search
      );
      payload.categoryList.forEach((category: ICategoryList) => {
        if (category.categorySlug === categoryId) {
          state.categories.selectedCategory = category;

          return;
        }
      });
    }
  },
  [CONSTANTS.CLEAR_CATEGORIES_DATA]: (state: ISearchState) => {
    state.categories.categoryList = [];
  },
  [CONSTANTS.SET_ALL_LOADING_DISABLE]: (state: ISearchState) => {
    state.loading = false;
    state.showAppShell = false;
  },
  [CONSTANTS.SELECTED_CATEGORIES]: (
    state: ISearchState,
    payload: ICategoryList
  ) => {
    state.categories.selectedCategory = payload;
  },
  [CONSTANTS.SET_SPEECH_RECOGNITION]: (
    state: ISearchState,
    payload: ICategoryList
  ) => {
    state.categories.selectedCategory = payload;
  },
  [CONSTANTS.SET_INPUT_CONTENT]: (state: ISearchState, payload: string) => {
    state.searchFilterSetting.inputContent = payload;
  },
  [CONSTANTS.SET_LISTING_ENABLE]: (state: ISearchState, payload: boolean) => {
    state.searchFilterSetting.showListing = payload;
  },
  [CONSTANTS.SET_SEARCH_PRIORITY_TEXT]: (
    state: ISearchState,
    searchText: {
      activeText: string;
      inActiveText: string;
    }
  ) => {
    state.search.activeText = searchText.activeText;
    state.search.inActiveText = searchText.inActiveText;
  }
};

export default withProduce(initialState, reducers) as Reducer<
  ISearchState,
  AnyAction
>;
