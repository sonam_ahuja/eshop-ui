import {
  fetchCategoryListService,
  fetchSearchListService,
  fetchSearchSuggestService
} from '@search/store/services';
import * as searchListApi from '@common/types/api/search/searchList';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { Effect, SagaIterator } from 'redux-saga';
import {
  ICategoryList,
  IFetchCategoriesParams,
  IFetchSearchListParams,
  ILoadMoreSearchListParams,
  ISearchState
} from '@search/store/types';
import actions from '@search/store/actions';
import { getOutOfStockReason } from '@productList/utils/getOutOfStockReason';
import { setLocalStorageSuggestion } from '@common/components/SpeechRecognition/utils/localStorageOperation';
import CONSTANTS from '@search/store/constants';
import { sendStockNotificationService } from '@productList/store/services';
import * as categoryListApi from '@common/types/api/search/categoryList';
import { getCatalogConfiguration, getState } from '@search/store/utils';
import { isBrowser } from '@common/utils';
import { getSortProductList } from '@productList/utils';
import { getConfiguration } from '@productList/store/utils';
import {
  compareUrl,
  createQueryParams,
  getSelectedFiltersFromParams
} from '@search/utils';
import history from '@src/client/history';
import { PAGE_NUMBER } from '@productList/store/enum';
import { DEFAULT_CATEGORY, SEARCH_ROUTE } from '@search/store/enum';
import { IGetNotificationRequestTemplatePayload } from '@productList/store/types';
import { ICatalogConfiguration } from '@src/common/store/types/configuration';

export function* fetchSuggestion(action: {
  type: string;
  payload: string;
}): SagaIterator {
  try {
    yield put(actions.fetchSuggestionLoading(true));

    const responseData: { text: string }[] = yield call(
      fetchSearchSuggestService,
      action.payload
    );
    yield put(
      actions.fetchSuggestionSuccess(responseData.map(item => item.text))
    );
  } catch (error) {
    yield put(actions.fetchSuggestionError(error));
  } finally {
    yield put(actions.fetchSuggestionLoading(false));
  }
}

export function* fetchCategoriesWithCondtion(action: {
  type: string;
  payload: IFetchCategoriesParams;
}): SagaIterator {
  try {
    yield put(actions.fetchCategoriesLoading(true));

    const { query } = action.payload;
    const responseData: categoryListApi.GET.IResponse = yield call(
      fetchCategoryListService,
      query
    );
    yield put(actions.fetchCategoriesSuccess(responseData));
  } catch (error) {
    yield put(actions.fetchCategoriesError(error));
  } finally {
    yield put(actions.fetchCategoriesLoading(false));
  }
}

// tslint:disable-next-line:cognitive-complexity
export function* fetchCategories(action: {
  type: string;
  payload: IFetchCategoriesParams;
}): SagaIterator {
  if (history) {
    const state: ISearchState = yield select(getState);
    const { categoryId: id, currentPage: page } = getSelectedFiltersFromParams(
      history.location.search
    );
    const { query: queryString, type: pageType } = action.payload;
    const oldUrl = createQueryParams(queryString, page, id, pageType);
    const isUrlUpdatable = compareUrl(
      `${history.location.pathname}?${oldUrl}`,
      `${history.location.pathname}${history.location.search}`
    );
    if (isUrlUpdatable || state.searchModal.modalOpen) {
      try {
        yield put(actions.fetchCategoriesLoading(true));
        yield put(actions.clearCategoriesData());

        let { url } = action.payload;
        const { query, type } = action.payload;
        const responseData: categoryListApi.GET.IResponse = yield call(
          fetchCategoryListService,
          query
        );
        const { payload } = yield put(
          actions.fetchCategoriesSuccess(responseData)
        );
        let newCategory: ICategoryList = {
          categorySlug: '',
          categoryId: '',
          categoryName: '',
          count: 0
        };
        let isMobileDevicesPresent = false;
        if (payload && history && payload.categoryList.length) {
          const { categoryId } = getSelectedFiltersFromParams(
            history.location.search
          );
          payload.categoryList.forEach((category: ICategoryList) => {
            if (category.categorySlug === categoryId) {
              newCategory = category;
            }
            if (category.categorySlug === DEFAULT_CATEGORY.DEVICES_MOBILE) {
              isMobileDevicesPresent = true;
            }
          });

          if (
            !newCategory.categoryName &&
            !newCategory.categorySlug &&
            !newCategory.categoryId
          ) {
            payload.categoryList.forEach((category: ICategoryList) => {
              if (category.count > 0) {
                newCategory = category;

                return;
              }
            });
          }

          actions.selectedCategories(newCategory);
          if (url !== `/${SEARCH_ROUTE.SEARCH}`) {
            url = SEARCH_ROUTE.SEARCH;
          }
          let path = `${url}?${createQueryParams(
            query,
            PAGE_NUMBER.INITIAL,
            newCategory.categorySlug
          )}`;
          if (type) {
            path = `${url}?${createQueryParams(
              query,
              PAGE_NUMBER.INITIAL,
              isMobileDevicesPresent && action.payload.isNewHit
                ? DEFAULT_CATEGORY.DEVICES_MOBILE
                : newCategory.categorySlug,
              type
            )}`;
          }
          history.push(path);
        }
      } catch (error) {
        yield put(actions.fetchCategoriesError(error));
      } finally {
        yield put(actions.fetchCategoriesLoading(false));
        if (state.searchModal.modalOpen) {
          yield put(actions.setSearchModal(false));
        }
        if (action.payload.query && action.payload.query.trim().length) {
          setLocalStorageSuggestion(action.payload.query.trim());
        }
      }
    }
  }
}

export function* loadMoreSetPreviousPageData(
  payload: ILoadMoreSearchListParams
): SagaIterator {
  const { pageNumber } = payload;
  const state: ISearchState = yield select(getState);
  const {
    data,
    pagination: { itemsPerPage }
  } = state;
  const newData = data.slice(0, itemsPerPage * pageNumber);

  const newState = {
    categoryId: '',
    data: newData,
    itemsPerPage,
    pageNumber,
    resultCount: state.pagination.totalItems,
    characteristics: state.characteristics
  };
  yield put(actions.fetchSearchListSuccess(newState));
  if (isBrowser) {
    window.scrollTo({
      top: 0
    });
  }
}

export function* fetchSearchList(action: {
  type: string;
  payload: IFetchSearchListParams;
}): SagaIterator {
  const state: ISearchState = yield select(getState);
  const {
    pagination: { itemsPerPage }
  } = state;
  const {
    payload: { currentPage, categoryId, queryParams, scrollAmount }
  } = action;
  if (queryParams) {
    yield put(actions.fetchSearchListLoading(false));
  } else {
    yield put(actions.fetchSearchListLoading(true));
  }
  const apiParams = {
    // as pageNumber here is number instead of index, so subtract 1
    currentPage: currentPage - 1,
    itemsPerPage,
    categoryId,
    query: queryParams ? queryParams.query : ''
  };

  if (history) {
    const { type } = getSelectedFiltersFromParams(history.location.search);
    if (type) {
      apiParams.itemsPerPage = 4;
    }
  }

  try {
    const responseData: searchListApi.POST.IResponse = yield call<
      typeof apiParams
    >(fetchSearchListService, apiParams);
    yield put(
      actions.fetchSearchListSuccess({
        ...responseData,
        pageNumber: currentPage,
        itemsPerPage
      })
    );
    yield call(scrollPage, scrollAmount, 0);
  } catch (error) {
    yield put(actions.fetchSearchListError(error));
  } finally {
    if (state.searchModal.modalOpen) {
      yield put(actions.setSearchModal(false));
    }
  }
}

export function* scrollPage(
  scrollAmount?: number,
  defaultScroll?: number
): SagaIterator {
  if (isBrowser) {
    if (scrollAmount !== undefined) {
      window.scrollTo({
        top: scrollAmount
      });
    } else if (defaultScroll !== undefined) {
      window.scrollTo({
        top: defaultScroll
      });
    }
  }
}

// tslint:disable-next-line: cognitive-complexity
export function* loadMoreSearchList(action: {
  type: string;
  payload: ILoadMoreSearchListParams;
}): SagaIterator {
  const {
    payload: { categoryId, isChanged, scrollAmount, queryParams, pageNumber }
  } = action;

  if (pageNumber && pageNumber > 1) {
    yield put(actions.fetchSearchListLoading(true));
  } else {
    yield put(actions.fetchSearchListLoading(false));
  }
  const state: ISearchState = yield select(getState);
  const {
    data,
    pagination: { currentPage, itemsPerPage }
  } = state;

  if (!isChanged && currentPage >= pageNumber) {
    yield call(loadMoreSetPreviousPageData, action.payload);

    return;
  }
  const { sortOutStock } = yield select(getConfiguration);
  let newItemsPerPage = 0;
  if (history) {
    const { type } = getSelectedFiltersFromParams(history.location.search);
    if (type) {
      newItemsPerPage = 4;
    }
  }

  const apiParams = {
    categoryId,
    itemsPerPage: newItemsPerPage
      ? newItemsPerPage
      : isChanged
      ? itemsPerPage * pageNumber
      : itemsPerPage,
    currentPage: isChanged ? 0 : currentPage,
    query: queryParams ? queryParams.query : ''
  };

  try {
    const responseData: searchListApi.POST.IResponse = yield call<
      typeof apiParams
    >(fetchSearchListService, apiParams);
    let updatedData: searchListApi.POST.IResponse['data'] = isChanged
      ? responseData.data
      : [...data, ...responseData.data];

    if (sortOutStock) {
      const {
        preorder: { considerInventory }
      }: ICatalogConfiguration = yield select(getCatalogConfiguration);
      updatedData = getSortProductList(updatedData, considerInventory);
    }

    const updatedResponse = {
      ...responseData,
      data: updatedData,
      pageNumber: Math.ceil(updatedData.length / itemsPerPage),
      itemsPerPage
    };
    yield put(actions.fetchSearchListSuccess(updatedResponse));
    yield call(scrollPage, scrollAmount);
  } catch (error) {
    yield put(actions.fetchSearchListError(error));
  }
}

export function* sendStockNotification(action: {
  type: string;
  payload: IGetNotificationRequestTemplatePayload;
}): IterableIterator<Effect | Effect[] | Promise<void>> {
  const { payload } = action;
  yield put(actions.sendStockNotificationLoading());
  const state: ISearchState = yield select(getState);
  const { data } = state;

  const reason = getOutOfStockReason(payload.variantId, data);

  try {
    yield sendStockNotificationService(payload, reason);
    yield put(actions.sendStockNotificationSuccess());
  } catch (error) {
    yield put(actions.sendStockNotificationError(error));
  }
}

export default function* watcherSagas(): Generator {
  yield takeLatest(CONSTANTS.FETCH_SEARCH_LIST_REQUESTED, fetchSearchList);
  yield takeLatest(
    CONSTANTS.LOAD_MORE_SEARCH_LIST_REQUESTED,
    loadMoreSearchList
  );
  yield takeLatest(CONSTANTS.FETCH_SUGGESTION_REQUESTED, fetchSuggestion);
  yield takeLatest(CONSTANTS.SEND_STOCK_NOTIFICATION, sendStockNotification);
  yield takeLatest(CONSTANTS.FETCH_CATEGORIES_REQUESTED, fetchCategories);
  yield takeLatest(
    CONSTANTS.FETCH_CATEGORIES_WITHOUT_CONDITION_REQUESTED,
    fetchCategoriesWithCondtion
  );
  // tslint:disable-next-line:max-file-line-count
}
