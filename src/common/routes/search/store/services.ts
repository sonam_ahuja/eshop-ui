import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import * as searchApi from '@common/types/api/search/search';
import * as categoryListApi from '@common/types/api/search/categoryList';
import * as searchListApi from '@common/types/api/search/searchList';
import * as searchSuggestApi from '@common/types/api/search/searchSuggest';
import { logError } from '@src/common/utils';
import { getSearchListingRequestTemplate } from '@search/utils/requestTemplate';
export interface IPayload {
  currentPage: number;
  itemsPerPage: number;
  categoryId: string;
  query: string;
}

export const fetchSearchListService = async (
  payload: IPayload
): Promise<searchListApi.POST.IResponse> => {
  const { currentPage, itemsPerPage, categoryId, query } = payload;
  const requestBody = getSearchListingRequestTemplate(
    currentPage,
    itemsPerPage,
    categoryId
  );
  const apiUrl = query
    ? `${apiEndpoints.SEARCH.GET_SEARCH_LIST.url}?query=${encodeURIComponent(
        query
      )}`
    : apiEndpoints.SEARCH.GET_SEARCH_LIST.url;

  try {
    return await apiCaller.post<searchListApi.POST.IResponse>(
      apiUrl,
      requestBody
    );
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchSearchService = async (
  query: string
): Promise<searchApi.GET.IResponse> => {
  const apiUrl = apiEndpoints.SEARCH.GET_SEARCH.url(query);

  try {
    return await apiCaller.get<searchApi.GET.IResponse>(apiUrl);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchSearchSuggestService = async (
  query: string
): Promise<searchSuggestApi.GET.IResponse> => {
  const apiUrl = apiEndpoints.SEARCH.GET_SEARCH_SUGGEST.url(query);

  try {
    return await apiCaller.get<searchSuggestApi.GET.IResponse>(apiUrl);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const fetchCategoryListService = async (
  query: string
): Promise<categoryListApi.GET.IResponse> => {
  const apiUrl = apiEndpoints.SEARCH.GET_CATEGORY_LIST.url(query);
  try {
    return await apiCaller.get<categoryListApi.GET.IResponse>(apiUrl);
  } catch (error) {
    logError(error);
    throw error;
  }
};
