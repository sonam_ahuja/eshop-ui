import { ISearchState } from '@search/store/types';
import { PAGE_THEME } from '@common/store/enums/index';
import { MODAL_TYPE, NOTIFICATION_MEDIUM } from '@productList/store/enum';

export default (): ISearchState => ({
  data: [],
  search: {
    activeText: '',
    inActiveText: ''
  },
  searchFilterSetting: {
    inputContent: '',
    showListing: false,
    originalContent: '',
    isInputWithNoText: false
  },
  categories: {
    loading: false,
    error: null,
    categoryList: [],
    selectedCategory: {
      categoryId: '',
      categoryName: '',
      categorySlug: '',
      count: 0
    },
    total: 0
  },
  openModal: false,
  suggestion: {
    loading: false,
    error: null,
    data: []
  },
  error: {
    code: 0,
    message: ''
  },
  loading: false,
  notificationLoading: false,
  gridLoading: false,
  pagination: {
    currentPage: 0,
    itemsPerPage: 8,
    totalItems: 0
  },
  notificationModal: MODAL_TYPE.SEND_NOTIFICATION_MODAL,
  notificationData: {
    variantId: '',
    deviceName: '',
    type: NOTIFICATION_MEDIUM.EMAIL,
    mediumValue: ''
  },
  backgroundImage: '',
  lastScrollPositionMap: {},
  lastListURL: null,
  theme: PAGE_THEME.PRIMARY,
  characteristics: [],
  showAppShell: true,
  searchModal: {
    modalOpen: false
  },
  speechRecognition: {
    autoStart: false,
    continuous: false
  },
  total: 0
});
