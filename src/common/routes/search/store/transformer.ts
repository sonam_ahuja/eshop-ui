import { ISuggestionParams } from '@search/store/types';
import { getLocalStorageSuggestion } from '@common/components/SpeechRecognition/utils/localStorageOperation';
import { ISearchQueries } from 'src/common/store/types/translation';
import { SUGGESTION_STATUS, SUGGESTION_TYPES } from '@search/store/enum';
import { textFilters } from '@search/utils/textUtils';

export interface ISuggestionConverter {
  type: string;
  index: number;
  filterType: string | null;
  activeText: string | null;
  fullText: string;
  inActiveText: string | null;
  activeIndex: boolean;
}

export const searchSuggestionResolver = (
  suggestion: string[],
  searchQueries: ISearchQueries,
  inputValue: string,
  actualInputValue: string,
  isListingNavigationEnable: boolean,
  currentIndex: number
): ISuggestionConverter[] => {
  const modifiedInputValue = inputValue.trim();
  const modifiedActualInputValue = actualInputValue.trim();
  const newSuggestion: ISuggestionConverter[] = [];
  const modifiedSuggestion: ISuggestionConverter[] = [];
  const localSuggestion: ISuggestionParams[] = getLocalStorageSuggestion().filter(
    query => query.suggestionText.includes(modifiedInputValue)
  );
  let newindex = 0;
  if (localSuggestion.length) {
    localSuggestion.forEach((local: ISuggestionParams) => {
      newindex++;
      newSuggestion.push({
        type: SUGGESTION_TYPES.LOCAL,
        index: newindex,
        filterType: null,
        activeText: null,
        fullText: local.suggestionText,
        inActiveText: local.suggestionText,
        activeIndex: false
      });
    });
  }

  if (!modifiedActualInputValue.length || !modifiedInputValue.length) {
    for (const key in searchQueries) {
      if (searchQueries.hasOwnProperty(key)) {
        newindex++;
        newSuggestion.push({
          type: SUGGESTION_TYPES.DEFAULT,
          index: newindex,
          filterType: null,
          activeText: null,
          fullText: searchQueries[key],
          inActiveText: searchQueries[key],
          activeIndex: false
        });
      }
    }
  }

  if (!modifiedActualInputValue.length || !modifiedInputValue.length) {
    if (
      isListingNavigationEnable &&
      newSuggestion.length &&
      newSuggestion[currentIndex]
    ) {
      newSuggestion[currentIndex].activeIndex = true;
    }

    return newSuggestion;
  }

  if (suggestion.length) {
    suggestion.forEach((response: string) => {
      newindex++;
      newSuggestion.push({
        type: SUGGESTION_TYPES.RESPONSE,
        index: newindex,
        filterType: null,
        activeText: null,
        fullText: response,
        inActiveText: response,
        activeIndex: false
      });
    });
  }

  if (modifiedInputValue && modifiedInputValue.length) {
    newSuggestion.forEach((result: ISuggestionConverter) => {
      const { type: filterType, filterActiveText } = textFilters(
        result.fullText,
        modifiedInputValue
      );

      if (filterType === SUGGESTION_STATUS.MATCH) {
        modifiedSuggestion.push({
          type: result.type,
          index: result.index,
          filterType,
          activeText: filterActiveText,
          fullText: result.fullText,
          inActiveText: '',
          activeIndex: false
        });
      }
      if (filterType === SUGGESTION_STATUS.MODIFIED) {
        modifiedSuggestion.push({
          type: result.type,
          index: result.index,
          filterType,
          activeText: filterActiveText,
          fullText: result.fullText,
          inActiveText: '',
          activeIndex: false
        });
      }
    });
  }

  if (isListingNavigationEnable) {
    if (modifiedInputValue && modifiedInputValue.length) {
      if (modifiedSuggestion.length && modifiedSuggestion[currentIndex]) {
        modifiedSuggestion[currentIndex].activeIndex = true;
      }
    } else {
      if (newSuggestion.length && newSuggestion[currentIndex]) {
        newSuggestion[currentIndex].activeIndex = true;
      }
    }
  }

  return modifiedInputValue && modifiedInputValue.length
    ? modifiedSuggestion
    : newSuggestion;
};
