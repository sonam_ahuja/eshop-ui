import { IGrid } from 'dt-components';
import { PAGE_THEME } from '@common/store/enums/index';
import {
  ICharacteristics,
  ILastScrollPositionMap,
  INotificationData,
  IProductListItem,
  pageThemeType
} from '@productList/store/types';
import { MODAL_TYPE } from '@productList/store/enum';

export interface ISearchState {
  categories: ICategories;
  searchFilterSetting: {
    inputContent: string;
    showListing: boolean;
    originalContent: string;
    isInputWithNoText: boolean;
  };
  suggestion: ISuggestion;
  pagination: IGrid.IPagination;
  data: IProductListItem[];
  search: {
    activeText: string | null;
    inActiveText: string | null;
  };
  backgroundImage: string;
  theme: PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;
  characteristics: ICharacteristics[];
  loading: boolean;
  lastScrollPositionMap: ILastScrollPositionMap;
  lastListURL: string | null;
  gridLoading: boolean;
  error: IError | null;
  showAppShell: boolean;
  total: number;
  searchModal: ISearchModal;
  notificationModal: MODAL_TYPE;
  notificationLoading: boolean;
  openModal: boolean;
  notificationData: INotificationData;
  speechRecognition: {
    autoStart: boolean;
    continuous: boolean;
  };
}

export interface ISearchModal {
  modalOpen: boolean;
}
export interface ISuggestion {
  loading: boolean;
  error: IError | null;
  data: string[];
}

export interface ICategories {
  loading: boolean;
  error: IError | null;
  categoryList: ICategoryList[];
  selectedCategory: ICategoryList;
  total: number;
}

export interface IQueryParams {
  query: string;
}

export interface IError {
  code?: number;
  message: string;
}

export interface ILoadMoreSearchListParams {
  categoryId: string;
  isChanged: boolean;
  scrollAmount?: number;
  pageNumber: number;
  queryParams?: IQueryParams;
}

export interface IFetchSearchListParams {
  queryParams?: IQueryParams;
  currentPage: number;
  categoryId: string;
  scrollAmount?: number;
}

export type pageThemeType = PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;

export interface ISearchResponse {
  category: ICategory[];
  data: IData[];
  total: number;
}

export interface ICategory {
  name: string;
  count: number;
}

export interface IData {
  name: string;
  searchdata: ISearchData[];
}

export interface ISearchData {
  data: IProductListItem[];
}

export interface ICategoryListResponse {
  categoryList: ICategoryList[];
  total: number;
}
export interface ICategoryList {
  categoryId: string;
  categoryName: string;
  categorySlug: string;
  count: number;
}

export interface ISearchListResponse {
  pageNumber: number;
  itemsPerPage: number;
  resultCount: number;
  characteristics: ICharacteristics[];
  data: IProductListItem[];
}

export interface ISearchListRequest {
  channel: string;
  queryString: string;
  categoryId: string;
  page: number;
  itemPerPage: number;
}
export interface IFetchCategoriesParams {
  query: string;
  url: string;
  type?: string;
  isNewHit?: boolean;
}

export interface ISuggestionParams {
  suggestionText: string;
  timestamp: number;
}

export interface IResetPagination {
  productList: IProductListItem[];
  pagination: IGrid.IPagination;
}

export interface IRenderCheck {
  isSearchList: boolean;
  isSearchListShell: boolean;
  isLoader: boolean;
  isSearchResults: boolean;
  isSearchResultShell: boolean;
  isEmptySearch: boolean;
}
