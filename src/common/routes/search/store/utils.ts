import { RootState } from '@src/common/store/reducers';
import {
  ICatalogConfiguration,
  IProductListConfiguration
} from '@src/common/store/types/configuration';
import { ISearchTranslation } from '@src/common/store/types/translation';

export const getState = (state: RootState) => state.search;

export const getTranslation = (state: RootState): ISearchTranslation => {
  return state.translation.cart.global.search;
};

export const getConfiguration = (
  state: RootState
): IProductListConfiguration => {
  return state.configuration.cms_configuration.modules.productList;
};

export const getCatalogConfiguration = (
  state: RootState
): ICatalogConfiguration => {
  return state.configuration.cms_configuration.modules.catalog;
};
