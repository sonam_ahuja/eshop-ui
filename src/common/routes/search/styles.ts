import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { Column, Row } from '@common/components/Grid/styles';
import { themeObj } from '@productList/theme';
import { pageThemeType } from '@productList/store/types';
import { StyledFooterPanel } from '@src/common/components/Footer/styles';

export const StyledSearchList = styled.div<{ theme: pageThemeType }>`
  background-color:${colors.silverGray};
  background-size: 100% auto;
  width:100%;
  /* background-attachment: fixed; */
  background-repeat: repeat;
  /* overflow: hidden; */
  .searchListContainer {
    padding: 1.25rem 1.25rem 4.5rem;

    .infite-content-txt{
      color: ${colors.mediumGray};
    }

/* hiding pagination div coz pagination is exists in this page */
    .device{
      .gridList + div{
        display: none;
      }
    }
    /* ROW COL */
    ${Row} {
      margin: -0.125rem;

      ${Column} {
        padding: 0.125rem;
      }
    }
    .title {
      color: ${props => themeObj[props.theme].titles.color};
      /* font-size: 2.5rem;
      line-height: 2.75rem; */
      letter-spacing: -0.2px;
      margin-bottom: 0.25rem;
      margin-top: 1.5rem;
    }
    .description {
      color: ${colors.darkGray};
      /* color: ${props => themeObj[props.theme].descriptions.color}; */
      /* font-size: 0.75rem;
      line-height: 1rem; */
    }
    .spinner {
      i {
        font-size: 2rem;
      }
    }
    .notFound {
      font-size: 0.875rem;
      font-weight: 500;
      line-height: 1.43;
      color: ${colors.darkGray};
      padding-bottom: 2.75rem;
    }
    .selectPerPageWrap {
      .selectPerPage {
        background: ${props =>
          themeObj[props.theme].selectBackgroundColor.background};
        .styledSelect {
          color: ${props => themeObj[props.theme].selectColor.color};
          /* font-size: 0.75rem; */
        }
        .caret {
          path {
            fill: ${props => themeObj[props.theme].caretColor.color};
          }
        }
        .optionsList {
          color: ${props => themeObj[props.theme].dropColor.color};
          background: ${props =>
            themeObj[props.theme].dropBackground.background};
          border-color: ${props =>
            themeObj[props.theme].borderColor.borderColor};
          padding: 0;
          height: auto;
          left: 0;
          & > div {
            font-size: 0.7rem;
            line-height: 1rem;
            padding: 0.25rem 0.6rem;
            height: auto;
            font-weight: bold;
            margin-bottom: 0.4rem;
            &:last-child {
              margin-bottom: 0;
            }
            &:hover {
              background: ${props =>
                themeObj[props.theme].dropHoverBgColor.background};
            }
          }
        }
      }
      select {
        z-index: 0;
      }
    }
    .dt_button {
      z-index: 0;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .searchListContainer {
      padding: 1rem 4.9rem 2.75rem;
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    min-height: 100%;
    .searchListContainer {
      padding: 1rem 4.9rem 2.75rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    /* temp css */
    /* width: 1366px;
    margin: auto;
    padding: 0;
    padding-right: 95px; */

    .searchListContainer {
      padding: 1rem 4.9rem 4.5rem;
      width: 100%;
      .title {
        font-size: 3.25rem;
        line-height: 3.5rem;
        margin-bottom: 0;
        letter-spacing: -0.25px;
        margin-top: 1.5rem;
      }
      .description {
        font-size: 0.875rem;
        line-height: 1.25rem;
      }
      .notFound {
        padding-bottom: 3.5rem;
      }
    }
  }
`;

export const StyledSearch = styled.div`
  width: 100%;
  ${StyledFooterPanel} {
    width: 100%;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    display: flex;
  }
`;

export const StyledContentWrapper = styled.div`
  width: 100%;
`;
