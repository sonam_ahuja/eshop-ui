import {
  ICatalogConfiguration,
  ICurrencyConfiguration,
  ILoginFormRules,
  IProductListConfiguration,
  ISearchConfiguration
} from '@common/store/types/configuration';
import {
  IGlobalTranslation,
  IProductListTranslation,
  ISearchTranslation
} from '@common/store/types/translation';
import { IGrid } from 'dt-components';
import {
  ICategoryList,
  IError,
  IFetchCategoriesParams,
  IQueryParams,
  ISearchState,
  pageThemeType
} from '@search/store/types';
import {
  ICharacteristics,
  IGetNotificationRequestTemplatePayload,
  ILastScrollPositionMap,
  INotificationData
} from '@productList/store/types';
import { MODAL_TYPE } from '@productList/store/enum';
export interface IMapStateToProps {
  productConfiguration: IProductListConfiguration;
  catalogConfiguration: ICatalogConfiguration;
  productTranslation: IProductListTranslation;
  searchTranslation: ISearchTranslation;
  searchConfiguration: ISearchConfiguration;
  search: ISearchState;
  currency: ICurrencyConfiguration;
  lastScrollPositionMap: ILastScrollPositionMap;
  globalTranslation: IGlobalTranslation;
  loading: boolean;
  gridLoading: boolean;
  showModal: boolean;
  notificationModalType: MODAL_TYPE;
  notificationData: INotificationData;
  formConfiguration: ILoginFormRules;
  notificationLoading: boolean;
  showAppShell: boolean;
  error: IError | null;
  backgroundImage: string;
  theme: pageThemeType;
  pagination: IGrid.IPagination;
  characteristics: ICharacteristics[];
}

export interface IMapDispatchToProps {
  fetchSearchList(
    currentPage: number,
    categoryId: string,
    queryParams?: IQueryParams,
    scrollAmount?: number
  ): void;
  loadMoreSearchList(
    categoryId: string,
    isChanged: boolean,
    scrollAmount?: number,
    queryParams?: IQueryParams,
    pageNumber?: number,
    showLoading?: boolean
  ): void;
  changeItemPerPage(itemPerPage: number): void;
  updateShowAppShell(showAppShell: boolean): void;
  setError(error: IError): void;
  resetData(): void;
  setScrollPosition(urlPath: string, position: number): void;
  setInputContent(value: string): void;
  setTempText(value: string): void;
  clearScrollPosition(urlPath: string): void;
  setlastListURL(lastListURL: string): void;
  onCategorySelect(selected: ICategoryList): void;
  fetchCategories(params: IFetchCategoriesParams): void;
  sendStockNotification(payload: IGetNotificationRequestTemplatePayload): void;
  openModal(isModalOpen: boolean): void;
  setListingEnable(isModalOpen: boolean): void;
  fetchWithOutConditionCategories(params: IFetchCategoriesParams): void;
  setNotificationData(data: INotificationData): void;
  setSearchModal(isEnable: boolean): void;
  sendAllLoadingDisable(): void;
  setSearchPriorityText(search: {
    activeText: string;
    inActiveText: string;
  }): void;
  setDeviceDetailedList(list: string): void;
}
