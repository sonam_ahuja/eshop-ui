import { IQueryParams } from '@search/store/types';

export const getSelectedFiltersFromParams = (
  query: string
): IQueryParams & {
  currentPage: number;
  categoryId: string;
  type?: string;
  eQueryParams?: string;
} => {
  const decodedParams = decodeURIComponent(query);
  const ampersandDetector: {
    query: string | null;
    isPresent: boolean;
  } = detectAmpersandAndMakeQuery(decodedParams);
  const params = new URLSearchParams(decodedParams);
  const currentPageParam = params.get('currentPage');
  const currentPageQuery = ampersandDetector.isPresent
    ? ampersandDetector.query
    : params.get('query');
  const categoryIdParam = params.get('categoryId');
  const typeQueryParams = params.get('type');
  const eQueryParams = params.get('e');

  let currentPage = 1;
  if (currentPageParam && !isNaN(+currentPageParam)) {
    currentPage = +currentPageParam;
  }

  const searchParams: IQueryParams & {
    currentPage: number;
    categoryId: string;
    type?: string;
    eQueryParams?: string;
  } = {
    query: currentPageQuery ? currentPageQuery : '',
    currentPage,
    categoryId: categoryIdParam as string
  };
  if (typeQueryParams) {
    searchParams.type = typeQueryParams;
  }
  if (eQueryParams) {
    searchParams.eQueryParams = eQueryParams;
  }

  return searchParams;
};

export const createQueryParams = (
  query: string | null,
  currentPage: number,
  categoryId: string,
  type?: string
): string => {
  let url = encodeURIComponent(
    `&query=${query}&currentPage=${currentPage}&categoryId=${categoryId}`
  );

  if (type) {
    url = encodeURIComponent(
      `&type=${type}&query=${query}&currentPage=${currentPage}&categoryId=${categoryId}`
    );
  }

  return url;
};

export const compareUrl = (oldUrl: string, newUrl: string): boolean => {
  if (decodeURIComponent(oldUrl) === decodeURIComponent(newUrl)) {
    return false;
  } else {
    return true;
  }
};

const detectAmpersandAndMakeQuery = (
  query: string
): { query: string | null; isPresent: boolean } => {
  const queryDetectorString = '&query=';
  const currentPageDetectorString = '&currentPage=';
  const indexOfQuery = query.indexOf(queryDetectorString);
  const indexOfcurrentPage = query.indexOf(currentPageDetectorString);
  const trimResult = query.slice(
    indexOfQuery + queryDetectorString.length,
    indexOfcurrentPage
  );

  if (
    trimResult.indexOf('&') > -1 &&
    indexOfQuery > -1 &&
    indexOfcurrentPage > -1
  ) {
    return { isPresent: true, query: trimResult };
  } else {
    return { isPresent: false, query: null };
  }
};
