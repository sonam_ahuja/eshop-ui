import {
  changeCategory,
  getMetaTitle,
  goToPage,
  loadMoreProductList,
  onCategorySelect,
  resetPagination
} from '@search/utils/productListing';
import { ProductListItem } from '@src/common/mock-api/productList/productList.mock';

describe('Product listing utils', () => {
  test('getConfiguration ::', () => {
    const result = changeCategory();
    expect(changeCategory()).toBe(result);
  });

  test('getMetaTitle ::', () => {
    const result = getMetaTitle('query');
    expect(getMetaTitle('query')).toBe(result);
  });

  test('goToPage ::', () => {
    const result = goToPage(2);
    expect(goToPage(2)).toBe(result);
  });

  test('loadMoreProductList ::', () => {
    const result = loadMoreProductList(2);
    expect(loadMoreProductList(2)).toBe(result);
  });

  test('onCategorySelect ::', () => {
    const result = onCategorySelect('Mobile-Device');
    expect(onCategorySelect('Mobile-Device')).toBe(result);
  });

  test('resetPagination ::', () => {
    const result = resetPagination([ProductListItem]);
    expect(resetPagination([ProductListItem])).toMatchObject(result);
  });
});
