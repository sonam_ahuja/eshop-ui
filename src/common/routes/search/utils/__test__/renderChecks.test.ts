import { checkCategoryList, renderCheck } from '@search/utils/renderChecks';
import { ProductListItem } from '@src/common/mock-api/productList/productList.mock';
import { SEARCH_ROUTE } from '@search/store/enum';
import { ICategoryList } from '@search/store/types';

describe('renderCheck utils', () => {
  test('renderCheck ::', () => {
    const result = renderCheck([ProductListItem], true, true);
    expect(renderCheck([ProductListItem], true, true)).toMatchObject(result);
  });

  test('renderCheck, when route is search result ::', () => {
    const result = renderCheck(
      [ProductListItem],
      true,
      true,
      SEARCH_ROUTE.SEARCH_RESULT
    );
    expect(
      renderCheck([ProductListItem], true, true, SEARCH_ROUTE.SEARCH_RESULT)
    ).toMatchObject(result);
  });

  test('renderCheck, when route is search result, when loading is false and app shell loading is true ::', () => {
    const result = renderCheck(
      [ProductListItem],
      false,
      true,
      SEARCH_ROUTE.SEARCH_RESULT
    );
    expect(
      renderCheck([ProductListItem], false, true, SEARCH_ROUTE.SEARCH_RESULT)
    ).toMatchObject(result);
  });

  test('renderCheck, when route is search result, when loading is false and app shell loading is false  ::', () => {
    const result = renderCheck([], false, false, SEARCH_ROUTE.SEARCH_RESULT);
    expect(
      renderCheck([], false, false, SEARCH_ROUTE.SEARCH_RESULT)
    ).toMatchObject(result);
  });

  test('checkCategoryList ::', () => {
    const categoryList: ICategoryList[] = [
      {
        categoryId: 'Mobile-Device',
        categoryName: 'Mobile',
        categorySlug: 'Device-Mobile',
        count: 1
      }
    ];
    expect(checkCategoryList(categoryList)).toBe(true);
  });
});
