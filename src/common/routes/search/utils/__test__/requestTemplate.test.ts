import {
  getCategories,
  getProductListingRequestTemplate,
  getSearchListingRequestTemplate,
  getSearchListRequest,
  getSelectedCategories
} from '@search/utils/requestTemplate';
import { ICategoryList } from '@search/store/types';
import { IItemPerPageOptions } from '@productList/store/types';

const category = 'Mobile-Devices';

describe('requestTemplate utils', () => {
  test('renderCheck ::', () => {
    const result = getSearchListRequest();
    expect(getSearchListRequest()).toMatchObject(result);
  });

  test('getProductListingRequestTemplate ::', () => {
    const result = getProductListingRequestTemplate(1, 2, category, 'query');
    expect(
      getProductListingRequestTemplate(1, 2, category, 'query')
    ).toMatchObject(result);
  });

  test('getSearchListingRequestTemplate ::', () => {
    const result = getSearchListingRequestTemplate(1, 2, category);
    expect(getSearchListingRequestTemplate(1, 2, category)).toMatchObject(
      result
    );
  });

  test('getCategories ::', () => {
    const categoryList: ICategoryList[] = [
      {
        categoryId: 'Mobile-Device',
        categoryName: 'Mobile',
        categorySlug: 'Device-Mobile',
        count: 1
      }
    ];
    const result = getCategories(categoryList);
    expect(getCategories(categoryList)).toMatchObject(result);
  });

  test('getSelectedCategories ::', () => {
    const itemPerPage: IItemPerPageOptions[] = [
      {
        title: 'string',
        id: 1
      }
    ];
    const result = getSelectedCategories(itemPerPage, category);
    expect(getSelectedCategories(itemPerPage, category)).toEqual(result);
  });
});
