import { textFilters } from '@search/utils/textUtils';

describe('textUtils utils', () => {
  test('renderCheck ::', () => {
    const result = textFilters('samsung', 'sam');
    expect(textFilters('samsung', 'sam')).toMatchObject(result);
  });

  test('renderCheck, when not matching text ::', () => {
    const result = textFilters('sam', 'samsung');
    expect(textFilters('sam', 'samsung')).toMatchObject(result);
  });
});
