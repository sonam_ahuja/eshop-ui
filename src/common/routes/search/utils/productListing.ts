import history from '@src/client/history';
import { createQueryParams, getSelectedFiltersFromParams } from '@search/utils';
import { ICategoryList, IResetPagination } from '@search/store/types';
import { IProductListItem } from '@productList/store/types';
import { PAGE_NUMBER } from '@productList/store/enum';
import { SEARCH_ROUTE } from '@search/store/enum';

export const resetPagination = (
  productList: IProductListItem[]
): IResetPagination => {
  const products = [...productList].splice(0, 4);

  return {
    productList: products,
    pagination: {
      currentPage: 1,
      itemsPerPage: products.length,
      totalItems: products.length
    }
  };
};

export const loadMoreProductList = (currentPage: number): void => {
  if (history) {
    const { categoryId, ...params } = getSelectedFiltersFromParams(
      history.location.search
    );
    const query: string = params.query ? params.query : '';
    if (query.length) {
      const path = `${history.location.pathname}?${createQueryParams(
        query,
        currentPage + 1,
        categoryId
      )}`;
      if (path !== `${history.location.pathname}${history.location.search}`) {
        history.push(path);
      }
    }
  }
};

export const goToPage = (pageNumber: number): void => {
  if (history) {
    const { categoryId, ...params } = getSelectedFiltersFromParams(
      history.location.search
    );
    if (params && params.query && params.query.length) {
      const path = `${history.location.pathname}?${createQueryParams(
        params.query,
        pageNumber,
        categoryId
      )}`;
      if (path !== `${history.location.pathname}${history.location.search}`) {
        history.push(path);
      }
    }
  }
};

export const getMetaTitle = (query: string): string => {
  return query;
};

export const getMetaDescription = (categories: ICategoryList[]): string => {
  let description = '';
  categories.forEach((category: ICategoryList, index: number) => {
    if (category.count > 0) {
      description =
        index === categories.length - 1
          ? `${description} ${category.categoryName}`
          : `${description} ${category.categoryName} `;
    }
  });

  return description;
};

export const changeCategory = () => {
  if (history) {
    const { categoryId, currentPage, ...params } = getSelectedFiltersFromParams(
      history.location.search
    );
    if (params.query && params.query.length) {
      const path = `${history.location.pathname}?${createQueryParams(
        params.query,
        currentPage,
        categoryId
      )}`;
      history.push(path);
    }
  }
};

export const onCategorySelect = (categoryName: string): void => {
  if (history) {
    const {
      categoryId,
      currentPage,
      type,
      ...params
    } = getSelectedFiltersFromParams(history.location.search);

    if (params.query && params.query.length && !type) {
      const path = `${history.location.pathname}?${createQueryParams(
        params.query,
        PAGE_NUMBER.INITIAL,
        categoryId,
        SEARCH_ROUTE.SEARCH_RESULT
      )}`;
      history.push(path);

      return;
    }
    if (categoryName === 'Devices') {
      const path = `${location.pathname}?${createQueryParams(
        params.query,
        type === SEARCH_ROUTE.SEARCH_RESULT ? PAGE_NUMBER.INITIAL : currentPage,
        categoryId
      )}`;
      history.push(path);

      return;
    }
  }
};
