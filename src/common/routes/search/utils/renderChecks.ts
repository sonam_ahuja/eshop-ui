import { ICategoryList, IRenderCheck } from '@search/store/types';
import { IProductListItem } from '@productList/store/types';
import { SEARCH_ROUTE } from '@search/store/enum';

export const renderCheck = (
  productList: IProductListItem[],
  loading: boolean,
  showAppShell: boolean,
  type?: string
): IRenderCheck => {
  const renderChecks: IRenderCheck = {
    isSearchList: false,
    isSearchListShell: false,
    isLoader: false,
    isSearchResults: false,
    isSearchResultShell: false,
    isEmptySearch: false
  };

  if (type === SEARCH_ROUTE.SEARCH_RESULT) {
    if (showAppShell) {
      renderChecks.isSearchResultShell = true;
    } else {
      if (!loading) {
        if (productList.length) {
          renderChecks.isSearchResults = true;
        } else {
          renderChecks.isEmptySearch = true;
        }
      } else {
        renderChecks.isSearchResultShell = true;
      }
    }
  } else {
    if (showAppShell) {
      renderChecks.isSearchListShell = true;
    } else {
      if (!loading) {
        if (!productList.length) {
          renderChecks.isEmptySearch = true;
        } else {
          renderChecks.isSearchList = true;
        }
      } else {
        renderChecks.isSearchList = true;
        renderChecks.isLoader = true;
      }
    }
  }

  return renderChecks;
};

export const checkCategoryList = (categoryList: ICategoryList[]): boolean => {
  let categoryCheck = false;
  categoryList.forEach((categroy: ICategoryList) => {
    if (categroy.count > 0) {
      categoryCheck = true;

      return;
    }
  });

  return categoryCheck;
};
