import { ICategoryList, ISearchListRequest } from '@search/store/types';
import APP_CONSTANTS from '@common/constants/appConstants';
import { IItemPerPageOptions } from '@productList/store/types';
import { getProductListRequest } from '@productList/utils/requestTemplate';

export const getSearchListRequest = (): ISearchListRequest => {
  return {
    channel: APP_CONSTANTS.CHANNEL,
    queryString: '',
    categoryId: '',
    page: 0,
    itemPerPage: 100
  };
};

export const getProductListingRequestTemplate = (
  page: number,
  itemPerPage: number,
  categoryId: string,
  queryString: string
) => {
  const requestBody = getSearchListRequest();
  requestBody.page = page;
  requestBody.itemPerPage = itemPerPage;
  requestBody.categoryId = categoryId;
  requestBody.queryString = queryString;

  return requestBody;
};

export const getSearchListingRequestTemplate = (
  page: number,
  itemPerPage: number,
  categoryId: string
) => {
  const requestBody = getProductListRequest();
  requestBody.page = page;
  requestBody.itemPerPage = itemPerPage;
  requestBody.categories = [];
  requestBody.conditions = [];
  requestBody.filterCharacteristics = [];
  requestBody.categories.push({
    id: categoryId,
    characteristics: []
  });

  return requestBody;
};

export const getCategories = (
  categoryList: ICategoryList[]
): IItemPerPageOptions[] => {
  const categoryOptions: IItemPerPageOptions[] = [];
  categoryList.forEach((category: ICategoryList, index: number) => {
    if (category.count > 0) {
      categoryOptions.push({
        title: `${category.categoryName}`,
        id: index
      });
    }
  });

  return categoryOptions;
};

export const getSelectedCategories = (
  categoryOptionList: IItemPerPageOptions[],
  category: string
): IItemPerPageOptions => {
  let selected: IItemPerPageOptions = {
    id: 0,
    title: ''
  };
  categoryOptionList.forEach((categoryOption: IItemPerPageOptions) => {
    if (category === categoryOption.title) {
      selected = categoryOption;

      return;
    }
  });

  return selected;
};
