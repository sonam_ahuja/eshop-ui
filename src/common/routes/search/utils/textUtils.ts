import { SUGGESTION_STATUS } from '@search/store/enum';

export const textFilters = (
  suggestionText: string,
  searchText: string
): {
  type: string | null;
  filterActiveText: string | null;
  filterInActiveText: string | null;
} => {
  const lowerCaseSuggestionText = suggestionText.toLowerCase();
  const lowerCaseSearchText = searchText.toLowerCase();
  if (!lowerCaseSuggestionText.localeCompare(lowerCaseSearchText)) {
    return {
      type: SUGGESTION_STATUS.MATCH,
      filterInActiveText: null,
      filterActiveText: textFormationFront(suggestionText, searchText)
    };
  } else {
    if (textCheck(lowerCaseSuggestionText, lowerCaseSearchText)) {
      return {
        type: SUGGESTION_STATUS.MODIFIED,
        filterActiveText: textFormationFront(suggestionText, searchText),
        filterInActiveText: null
      };
    } else {
      return {
        type: SUGGESTION_STATUS.MODIFIED,
        filterActiveText: suggestionText,
        filterInActiveText: null
      };
    }
  }
};

const textCheck = (suggestionText: string, searchText: string): boolean => {
  return suggestionText.includes(searchText);
};

const textFormationFront = (str: string, searchStr: string): string => {
  const index = str.toLowerCase().indexOf(searchStr.toLowerCase());
  if (index === -1) {
    return str;
  }

  return `${str.slice(0, index)}<span>${str.slice(
    index,
    index + searchStr.length
  )}</span>${str.slice(index + searchStr.length)}`;
};

// tslint:disable-next-line: no-commented-code
// const textFormationFront = (
//   suggestionText: string,
//   searchText: string
// ): string => {
//   const lowerCaseSuggestionText = suggestionText.toLowerCase();
//   const lowerCaseSearchText = searchText.toLowerCase();

// tslint:disable-next-line: no-commented-code
//   return suggestionText.substring(
//     lowerCaseSuggestionText.indexOf(lowerCaseSearchText) +
//       lowerCaseSearchText.length
//   );
// };
