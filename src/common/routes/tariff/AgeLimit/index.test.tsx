import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import AgeLimit, { IProps } from '@tariff/AgeLimit';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

describe('<AgeLimit />', () => {
  const props: IProps = {
    youngTariffTranslation: appState().translation.cart.productDetailed
      .youngTariff,
    error: true,
    errorMessage: 'string',
    validateAgeLimit: jest.fn(),
    closeModal: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <AgeLimit {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<AgeLimit>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <AgeLimit {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('AgeLimit').instance() as AgeLimit).handleChange('string');
    (component.find('AgeLimit').instance() as AgeLimit).onClose();
    (component.find('AgeLimit').instance() as AgeLimit).onValidateClick();
  });
});
