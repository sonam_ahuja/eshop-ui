import React, { Component } from 'react';
import { Button, FloatLabelInput, Icon, Title } from 'dt-components';
import { DialogBoxApp } from '@common/components/DialogBox/styles';
import { IYoungTariff } from '@common/store/types/translation';
import { DATE_FORMATTING } from '@common/constants/appConstants';
import { isMobile } from '@src/common/utils';
import { StyledFooter } from '@routes/productList/Modals/StockNotificationModal/styles';
import EVENT_NAME from '@events/constants/eventName';
import { colors } from '@src/common/variables';

export interface IState {
  dateOfBirth: string;
}

export interface IProps {
  youngTariffTranslation: IYoungTariff;
  error: boolean;
  errorMessage?: string;
  validateAgeLimit(data: string): void;
  closeModal(): void;
}

class AgeLimit extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      dateOfBirth: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.onValidateClick = this.onValidateClick.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  handleChange(data: string): void {
    this.setState({
      dateOfBirth: data
    });
  }

  onClose(): void {
    this.props.closeModal();
    document.body.style.overflow = null;
  }

  onValidateClick(): void {
    this.props.validateAgeLimit(this.state.dateOfBirth);
  }

  render(): React.ReactNode {
    const { error, youngTariffTranslation: youngTariff } = this.props;

    return (
      <>
        {youngTariff.titleText}
        <DialogBoxApp
          isOpen={true}
          showCloseButton={true}
          type='flexibleHeight'
          closeOnEscape={true}
          className='ageLimitPopup'
          onClose={this.onClose}
          onBackdropClick={this.onClose}
          closeOnBackdropClick={true}
          onEscape={this.onClose}
        >
          <div className='dialogBoxHeader'>
            <Icon name={'ec-cool'} size='xlarge' />
          </div>

          <div className='dialogBoxBody'>
            <Title size='large' weight='ultra' color={colors.darkGray}>
              {youngTariff.ageLimitText}
              <br />
              {youngTariff.enjoyTariff}
            </Title>
          </div>
          <StyledFooter className='dialogBoxFooter'>
            <FloatLabelInput
              type='date'
              label={youngTariff.placeHolder}
              dateFormat={DATE_FORMATTING}
              className='date-field'
              onChange={event => {
                this.handleChange((event.target as HTMLInputElement).value);
              }}
              isMobile={isMobile.phone || isMobile.tablet}
              value={this.state.dateOfBirth}
              error={error}
              errorMessage={youngTariff.errorMessage}
            />
            <Button
              disabled={this.state.dateOfBirth === ''}
              onClickHandler={this.onValidateClick}
              data-event-id={EVENT_NAME.TARIFF.EVENTS.YOUNG_TARIFF_VALIDATE}
              data-event-message={youngTariff.validate}
              type='secondary'
            >
              {youngTariff.validate}
            </Button>
          </StyledFooter>
        </DialogBoxApp>
      </>
    );
  }
}

export default AgeLimit;
