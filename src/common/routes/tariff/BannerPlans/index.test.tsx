import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import BannerPlans, { IProps } from '@tariff/BannerPlans';
import { StaticRouter } from 'react-router-dom';

describe('<BannerPlans />', () => {
  const props: IProps = {
    specialSpecialOfferHtml1: 'html1',
    specialSpecialOfferHtml2: 'html2',
    onRouteChange: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <BannerPlans {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
