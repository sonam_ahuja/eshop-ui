import React, { Component, ReactNode } from 'react';
import { Column } from '@common/components/Grid/styles';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { StyledHorizontalScrollRow } from '@common/components/styles';
import { StyledBannerPlans } from '@tariff/BannerPlans/styles';
import {
  sendPromotionClickEvent,
  sendPromotionViewEvent
} from '@events/category/index';

export interface IProps {
  specialSpecialOfferHtml1: string;
  specialSpecialOfferHtml2: string;
  onRouteChange(url: string): void;
}
class BannerPlans extends Component<IProps> {
  componentDidMount(): void {
    sendPromotionViewEvent(
      this.props.specialSpecialOfferHtml1 ? 'specialTariffOfferHtml1' : null,
      this.props.specialSpecialOfferHtml2 ? 'specialTariffOfferHtml2' : null
    );
  }

  render(): ReactNode {
    const { specialSpecialOfferHtml1, specialSpecialOfferHtml2 } = this.props;

    return (
      <StyledBannerPlans>
        <StyledHorizontalScrollRow className='scroll-wrap'>
          {specialSpecialOfferHtml1 && (
            <Column colMobile={12} colTabletPortrait={6}>
              <HTMLTemplate
                template={specialSpecialOfferHtml1}
                templateData={{}}
                onRouteChange={this.props.onRouteChange}
                sendEvents={() => {
                  sendPromotionClickEvent('specialTariffOfferHtml1', 'slot1');
                }}
                className='templateWrap'
              />
            </Column>
          )}
          {specialSpecialOfferHtml2 && (
            <Column colMobile={12} colTabletPortrait={6}>
              <HTMLTemplate
                template={specialSpecialOfferHtml2}
                templateData={{}}
                onRouteChange={this.props.onRouteChange}
                sendEvents={() => {
                  sendPromotionClickEvent(
                    'specialTariffOfferHtml2',
                    specialSpecialOfferHtml1 ? 'slot2' : 'slot1'
                  );
                }}
                className='templateWrap'
              />
            </Column>
          )}
        </StyledHorizontalScrollRow>
      </StyledBannerPlans>
    );
  }
}

export default BannerPlans;
