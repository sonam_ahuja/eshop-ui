import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { Column } from '@common/components/Grid/styles';
import { StyledHorizontalScrollRow } from '@common/components/styles';

export const StyledBannerPlans = styled.div`
  margin-bottom: 6rem;
  &:only-child {
    margin-bottom: 0;
  }

  ${StyledHorizontalScrollRow} {
    margin: 0 -1.375rem;
    padding: 0 1.25rem;

    ${Column} {
      padding: 0 0.125rem;
    }
  }
  /*********** Support for Iphone SE CSS ***********/
  .scroll-wrap {
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
    .plans-container {
      padding: 0;
      height: 100%;
    }
    ${Column} {
      padding: 0;
      margin-right: 0;
      flex-basis: 100%;
      max-width: 100%;
      &:last-child {
        margin-right: 0;
      }
    }
  }
  /*********** Support for Iphone SE CSS ***********/

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -2.25rem;
      padding: 0 2.25rem;
    }
    .scroll-wrap {
      flex-wrap: wrap;
      margin: 0;
      padding: 0;
      .plans-container {
        padding: 0;
      }
      ${Column} {
        padding: 0;
        margin-right: 5px;
        flex-basis: calc(50% - 2.5px);
        max-width: calc(50% - 2.5px);
        &:last-child {
          margin-right: 0;
        }
        .templateWrap {
          height: 100%;
          .plans-col {
            height: 100%;
          }
        }
      }
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -3rem;
      padding: 0 3rem;
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    ${StyledHorizontalScrollRow} {
      margin: 0 -4.9rem;
      padding: 0 4.9rem;
    }
  }
`;
