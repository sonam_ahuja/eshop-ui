import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import BenefitsAccordion, { IProps } from '@tariff/Discounts/BenefitsAccordion';

describe('<BenefitsAccordion />', () => {
  const props: IProps = {
    className: 'string',
    heading: 'string',
    isOpen: true,
    children: '',
    onClick: jest.fn()
  };
  const componentWrapper = (newProps: IProps) => mount(
    <ThemeProvider theme={{}}>
      <BenefitsAccordion {...newProps} />
    </ThemeProvider>
  );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when className is undefined', () => {
    const newProps: IProps = { ...props, className: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
