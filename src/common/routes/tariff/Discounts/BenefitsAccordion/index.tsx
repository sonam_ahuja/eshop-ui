import React, { ReactNode } from 'react';
import { Icon } from 'dt-components';

import { StyledAccrodion } from './styles';

export interface IProps {
  className?: string;
  heading: string;
  isOpen: boolean;
  children: ReactNode;
  onClick(): void;
}

export default (props: IProps) => {

  const { heading, children, onClick, isOpen, className } = props;

  const iconEl = isOpen ? <Icon name='ec-minus' color='currentColor' size='small' /> :
    <Icon name='ec-plus' color='currentColor' size='small' />;

  return (
    <StyledAccrodion
      className={className}
      isOpen={true}
      Header={({ headerTitle }) => {
        return (<div onClick={onClick} className='accordionHeader'>
          <h2 className='accordionTitle'>{headerTitle}</h2>
          {iconEl}
        </div>);
      }}
      headerTitle={heading}
    >
      <div className='panel-body'>
        {children}
      </div>
    </StyledAccrodion>
  );
};
