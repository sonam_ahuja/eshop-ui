import styled from 'styled-components';
import { Accordion } from 'dt-components';
import { IProps } from 'dt-components/lib/es/components/atoms/accordion';
import { colors } from '@src/common/variables';

export const StyledAccrodion = styled(Accordion)`
  .accordionHeader {
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    padding: 1.75rem 1.25rem;
    background: ${colors.silverGray};
    font-weight: bold;
    font-size: 1.125rem;

    .accordionTitle {
      width: 100%;
      display: inline-block;
      margin-right: 1rem;
      vertical-align: text-bottom;
    }

    .dt_icon {
      align-self: flex-start;
    }
  }
` as React.ComponentType<IProps>;
