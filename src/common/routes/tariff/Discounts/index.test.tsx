import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import { IProps } from '@tariff/index';
import DiscountPlans from '@tariff/Discounts';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';

describe('<DiscountPlans />', () => {
  const props: IProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    getTariffAddonPrice: jest.fn(),
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    tariff: appState().tariff,
    tariffTranslation: appState().translation.cart.tariff,
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    selectedProductOfferingTerm: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    globalTranslation: appState().translation.cart.global,
    categories: appState().categories.categories,
    selectedProductOfferingBenefit: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    showTermAndConditions: jest.fn(),
    setTariffId: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <DiscountPlans {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <DiscountPlans {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('DiscountPlans').instance() as DiscountPlans).onRouteChange(
      '/basket'
    );
    (component
      .find('DiscountPlans')
      .instance() as DiscountPlans).scrollToDiscountTop();
    (component
      .find('DiscountPlans')
      .instance() as DiscountPlans).componentWillUnmount();
  });
});
