import React, { Component } from 'react';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { IProps } from '@tariff/index';
import { DISCOUNT_TYPES } from '@tariff/store/enum';
import { getCharacterstic } from '@category/utils/helper';
import htmlKeys from '@common/constants/appkeys';
import { sendTariffTabEvent } from '@events/tariff/index';
import { isMobile } from '@src/common/utils';
import BenefitAccordion from '@tariff/Discounts/BenefitsAccordion';
import cx from 'classnames';

import { StyledDiscountPlans } from './styles';

export interface IState {
  magentaPlan: boolean;
  familyPlan: boolean;
}

class DiscountPlans extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      magentaPlan: false,
      familyPlan: false
    };

    this.clickHnadler = this.clickHnadler.bind(this);
  }

  componentWillUnmount(): void {
    this.props.showMagentaDiscount(false);
  }

  clickHnadler(type: string, status: boolean): void {
    const obj: IState = {
      magentaPlan: false,
      familyPlan: false
    };
    obj[type] = status;
    sendTariffTabEvent(
      type === 'magentaPlan'
        ? this.props.tariffTranslation.magentaDiscountsText
        : this.props.tariffTranslation.familyDiscountsText
    );
    this.setState(obj);

    this.props.showMagentaDiscount(obj.magentaPlan);
  }

  scrollToDiscountTop = (): void => {
    if (isMobile.phone || isMobile.tablet) {
      const elmnt = document.getElementById('aboutdiscounts');
      if (elmnt) {
        elmnt.scrollIntoView();
      }
    }
  }

  onRouteChange(route: string): void {
    this.props.history.push(route);
  }

  render(): React.ReactNode {
    const { magentaPlan, familyPlan } = this.state;
    const { tariffTranslation, tariff } = this.props;

    const magentaDisnounts = getCharacterstic(
      tariff.categories.characteristics,
      htmlKeys.TARIFF_MAGENTA_DISCOUNTS
    );

    const faimlyBenifits = getCharacterstic(
      tariff.categories.characteristics,
      htmlKeys.TARIFF_FAMILY_BENIFITS
    );

    return (
      <>
        <StyledDiscountPlans id='aboutdiscounts'>
          {magentaDisnounts && (
            <BenefitAccordion
              isOpen={magentaPlan}
              className={cx('magentaTariffPlans', { hidePlan: !magentaPlan })}
              heading={tariffTranslation.magentaDiscountsText}
              onClick={() => {
                this.scrollToDiscountTop();
                this.clickHnadler(DISCOUNT_TYPES.MAGENTA_PLAN, !magentaPlan);
              }}
            >
              <HTMLTemplate
                onRouteChange={this.onRouteChange}
                template={magentaDisnounts}
                templateData={{}}
              />
            </BenefitAccordion>
          )}
          {faimlyBenifits && (
            <BenefitAccordion
              isOpen={familyPlan}
              className={cx('familyTariffPlans', { hidePlan: !familyPlan })}
              heading={tariffTranslation.familyDiscountsText}
              onClick={() => {
                this.scrollToDiscountTop();
                this.clickHnadler(
                  DISCOUNT_TYPES.FAMILY_PLAN,
                  !this.state.familyPlan
                );
              }}
            >
              <HTMLTemplate
                onRouteChange={this.onRouteChange}
                template={faimlyBenifits}
                templateData={{}}
              />
            </BenefitAccordion>
          )}
        </StyledDiscountPlans>
      </>
    );
  }
}
export default DiscountPlans;
