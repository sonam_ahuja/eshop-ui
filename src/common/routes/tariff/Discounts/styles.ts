import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledDiscountPlans = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  .magentaTariffPlans {
    width: 100%;
    flex: 1;
    .accordionHeader {
      padding: 1.75rem 1.25rem;
      min-height: 5rem;
      background-color: ${colors.magenta};
      .accordionTitle,
      .dt_icon {
        color: ${colors.white};
      }
      .dt_icon {
        font-size: 1.5rem;
      }
    }
    .panel-body {
      position: relative;
      width: 100%;
      z-index: 1;
      left: 0;
    }
  }
  .familyTariffPlans {
    width: 100%;
    flex: 1;
    .accordionHeader {
      padding: 1.75rem 1.25rem;
      min-height: 5rem;
      background-color: ${colors.silverGray};
      .accordionTitle,
      .dt_icon {
        color: ${colors.darkGray};
      }
      .dt_icon {
        font-size: 1.5rem;
      }
    }
    .panel-body {
      position: relative;
      width: 100%;
      z-index: 1;
      left: 0;
    }
    &:only-child {
      .panel-body {
        left: 0;
      }
    }
  }

  .dt_accordion.hidePlan .accordionHeader + div {
    opacity: 0;
    visibility: hidden;
    display: none;
  }
  .dt_accordion .accordionHeader + div {
    transition: 0.2s ease-in 0s;
    opacity: 1;
    visibility: visible;
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    flex-direction: row;
    .magentaTariffPlans {
      width: 50%;
      .accordionHeader {
        padding: 1.75rem 2.25rem;
      }
    }
    .familyTariffPlans {
      width: 50%;
      .accordionHeader {
        padding: 1.75rem 2.25rem;
      }
      .panel-body {
        left: -100%;
      }
    }
    .magentaTariffPlans,
    .familyTariffPlans {
      .panel-body {
        width: calc(100% + 100%);
      }
      &:only-child {
        .panel-body {
          width: 100%;
        }
      }
    }
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    .magentaTariffPlans,
    .familyTariffPlans {
      .accordionHeader {
        padding: 1.75rem 3rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktop}px) {
    .magentaTariffPlans,
    .familyTariffPlans {
      .accordionHeader {
        padding: 1.75rem 4.9rem;
      }
    }
  }

  @media (min-width: ${breakpoints.desktopLarge}px) {
    .magentaTariffPlans,
    .familyTariffPlans {
      .accordionHeader {
        padding: 1.75rem 5.5rem;
      }
    }
  }
`;
