import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import Header from '@tariff/Faqs/header';
import appState from '@store/states/app';

import { IProps } from './index';

describe('<Header />', () => {
  test('should render properly', () => {
    const props: IProps = {
      label: 'faq',
      tariffTranslation: appState().translation.cart.tariff,
      tariffConfiguration: appState().configuration.cms_configuration.modules
        .tariff
    };
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Header {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
