import React from 'react';
import { Anchor, Title } from 'dt-components';
import { StyledHeaderFaqs } from '@tariff/Faqs/header/styles';
import { ITariffTranslation } from '@src/common/store/types/translation';
import { ITariffConfiguration } from '@src/common/store/types/configuration';
import appConstants from '@src/common/constants/appConstants';

export interface IProps {
  label: string;
  tariffTranslation: ITariffTranslation;
  tariffConfiguration: ITariffConfiguration;
}
const HeaderFaqs = (props: IProps) => {
  const { label, tariffTranslation, tariffConfiguration } = props;

  return (
    <StyledHeaderFaqs>
      <div className='faqs-header'>
        <Title tag='h4' size='xlarge' weight='ultra'>
          {label}
        </Title>
        {tariffConfiguration.showViewAllLink && (
          <Anchor
            hreflang={appConstants.LANGUAGE_CODE}
            href={tariffConfiguration.viewAllLink}
            target={tariffConfiguration.openViewAllInTab ? '_blank' : '_self'}
            className='view-all-btn'
          >
            {tariffTranslation.viewAll}
          </Anchor>
        )}
      </div>
    </StyledHeaderFaqs>
  );
};

export default HeaderFaqs;
