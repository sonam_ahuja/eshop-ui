import styled from 'styled-components';
import { colors } from '@src/common/variables';

export const StyledHeaderFaqs = styled.div`
  .faqs-header {
    display: flex;
    margin-bottom: 3.875rem;
    justify-content: space-between;
    align-items: flex-start;
    .dt_title {
      padding-right: 4.375rem;
      flex-basis: 80%;
      max-width: 80%;
      color: ${colors.white};
      line-height: 2.75rem;
      letter-spacing: -0.2px;
      max-width: 23rem;
    }
    .view-all-btn {
      color: ${colors.white};
      font-size: 0.875rem;
      line-height: 1.25rem;
      margin-top: 1.25rem;
    }
  }
`;
