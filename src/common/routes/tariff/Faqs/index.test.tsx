import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import { IProps } from '@tariff/index';
import FaqsHtml from '@tariff/Faqs';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

describe('<FaqsHtml />', () => {
  const props: IProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    getTariffAddonPrice: jest.fn(),
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    tariff: appState().tariff,
    tariffTranslation: appState().translation.cart.tariff,
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    selectedProductOfferingTerm: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    globalTranslation: appState().translation.cart.global,
    categories: appState().categories.categories,
    selectedProductOfferingBenefit: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    showTermAndConditions: jest.fn(),
    setTariffId: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <FaqsHtml {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const component = mount<FaqsHtml>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <FaqsHtml {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component.find('FaqsHtml').instance() as FaqsHtml).onRouteChange('string');
  });
});
