import React, { Component, ReactNode } from 'react';
import { IProps } from '@tariff/index';
import htmlKeys from '@common/constants/appkeys';
import { getCharacterstic } from '@category/utils/helper';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { StyledFaqsWrap } from '@tariff/Faqs/styles';
import HeaderFaqs from '@tariff/Faqs/header/index';

class FaqsHtml extends Component<IProps> {
  constructor(props: IProps) {
    super(props);

    this.onRouteChange = this.onRouteChange.bind(this);
  }

  onRouteChange(route: string): void {
    this.props.history.push(route);
  }

  render(): ReactNode {
    const { tariff, tariffTranslation, tariffConfiguration } = this.props;
    const tariffFaqHtml = getCharacterstic(
      tariff.categories.characteristics,
      htmlKeys.TARIFF_FAQ
    );

    return (
      <StyledFaqsWrap>
        <HeaderFaqs
          label={tariffTranslation.faqHeaderText}
          tariffConfiguration={tariffConfiguration}
          tariffTranslation={tariffTranslation}
        />
        <HTMLTemplate
          onRouteChange={this.onRouteChange}
          template={tariffFaqHtml}
          templateData={{}}
        />
      </StyledFaqsWrap>
    );
  }
}

export default FaqsHtml;
