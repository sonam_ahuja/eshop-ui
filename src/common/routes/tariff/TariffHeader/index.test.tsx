import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router-dom';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import configureStore from 'redux-mock-store';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';

import TariffHeader from '.';

// tslint:disable-next-line: no-big-function
describe('<TariffHeader />', () => {
  test('should render properly with plan true', () => {
    const props = {
      planSelectionOnly: true,
      confirmPlanBtnLoading: true,
      ...histroyParams,
      tariff: appState().tariff,
      tariffTranslation: appState().translation.cart.tariff,
      tariffConfiguration: appState().configuration.cms_configuration.modules
        .tariff,
      selectedProductOfferingTerm: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      globalTranslation: appState().translation.cart.global,
      categories: appState().categories.categories,
      selectedProductOfferingBenefit: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      commonError: {
        code: '',
        httpStatusCode: '',
        retryable: false
      },
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      fetchTariff: jest.fn(),
      fetchCategories: jest.fn(),
      setDiscount: jest.fn(),
      setLoyalty: jest.fn(),
      showMagentaDiscount: jest.fn(),
      showAppShell: jest.fn(),
      showTermAndConditions: jest.fn(),
      setTariffId: jest.fn(),
      buyPlanOnly: jest.fn(),
      saveTariff: jest.fn(),
      toggleSelectedBenefit: jest.fn(),
      setCategoryId: jest.fn(),
      resetPlansData: jest.fn(),
      validateAgeLimit: jest.fn(),
      youngTariffCheck: jest.fn(),
      clickOnAddToBasket: jest.fn(),
      clickOnBuyPlanOnly: jest.fn(),
      ageLimitSuccess: jest.fn(),
      getTariffAddonPrice: jest.fn()
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <TariffHeader {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('should render properly with plan only false', () => {
    const props = {
      planSelectionOnly: false,
      confirmPlanBtnLoading: true,
      ...histroyParams,
      tariff: appState().tariff,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      tariffTranslation: appState().translation.cart.tariff,
      tariffConfiguration: appState().configuration.cms_configuration.modules
        .tariff,
      selectedProductOfferingTerm: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      globalTranslation: appState().translation.cart.global,
      categories: appState().categories.categories,
      selectedProductOfferingBenefit: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      commonError: {
        code: '',
        httpStatusCode: '',
        retryable: false
      },
      fetchTariff: jest.fn(),
      fetchCategories: jest.fn(),
      setDiscount: jest.fn(),
      setLoyalty: jest.fn(),
      showMagentaDiscount: jest.fn(),
      showAppShell: jest.fn(),
      showTermAndConditions: jest.fn(),
      setTariffId: jest.fn(),
      buyPlanOnly: jest.fn(),
      saveTariff: jest.fn(),
      toggleSelectedBenefit: jest.fn(),
      setCategoryId: jest.fn(),
      resetPlansData: jest.fn(),
      validateAgeLimit: jest.fn(),
      youngTariffCheck: jest.fn(),
      clickOnAddToBasket: jest.fn(),
      clickOnBuyPlanOnly: jest.fn(),
      ageLimitSuccess: jest.fn(),
      getTariffAddonPrice: jest.fn()
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <TariffHeader {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  // tslint:disable-next-line: no-identical-functions
  test('should render properly with plan only false', () => {
    const props = {
      planSelectionOnly: true,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      ...histroyParams,
      tariff: appState().tariff,
      confirmPlanBtnLoading: true,
      tariffTranslation: appState().translation.cart.tariff,
      tariffConfiguration: appState().configuration.cms_configuration.modules
        .tariff,
      selectedProductOfferingTerm: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      globalTranslation: appState().translation.cart.global,
      categories: appState().categories.categories,
      selectedProductOfferingBenefit: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      commonError: {
        code: '',
        httpStatusCode: '',
        retryable: false
      },
      fetchTariff: jest.fn(),
      fetchCategories: jest.fn(),
      setDiscount: jest.fn(),
      setLoyalty: jest.fn(),
      showMagentaDiscount: jest.fn(),
      showAppShell: jest.fn(),
      showTermAndConditions: jest.fn(),
      setTariffId: jest.fn(),
      buyPlanOnly: jest.fn(),
      saveTariff: jest.fn(),
      toggleSelectedBenefit: jest.fn(),
      setCategoryId: jest.fn(),
      resetPlansData: jest.fn(),
      validateAgeLimit: jest.fn(),
      youngTariffCheck: jest.fn(),
      clickOnAddToBasket: jest.fn(),
      clickOnBuyPlanOnly: jest.fn(),
      ageLimitSuccess: jest.fn(),
      getTariffAddonPrice: jest.fn()
    };

    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <TariffHeader {...props} />
        </ThemeProvider>
      </StaticRouter>
    );

    expect(component).toMatchSnapshot();

    // tslint:disable-next-line:no-console
  });

  test('handleChange test for input field', () => {
    const mockStore = configureStore();
    const initStateValue: RootState = appState();
    const store = mockStore(initStateValue);
    const props = {
      planSelectionOnly: false,
      confirmPlanBtnLoading: true,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      ...histroyParams,
      tariff: appState().tariff,
      tariffTranslation: appState().translation.cart.tariff,
      tariffConfiguration: appState().configuration.cms_configuration.modules
        .tariff,
      selectedProductOfferingTerm: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      globalTranslation: appState().translation.cart.global,
      categories: appState().categories.categories,
      selectedProductOfferingBenefit: {
        label: 'label',
        value: 'value',
        isSelected: true
      },
      commonError: {
        code: '',
        httpStatusCode: '',
        retryable: false
      },
      fetchTariff: jest.fn(),
      fetchCategories: jest.fn(),
      setDiscount: jest.fn(),
      setLoyalty: jest.fn(),
      showMagentaDiscount: jest.fn(),
      showAppShell: jest.fn(),
      showTermAndConditions: jest.fn(),
      setTariffId: jest.fn(),
      buyPlanOnly: jest.fn(),
      saveTariff: jest.fn(),
      toggleSelectedBenefit: jest.fn(),
      setCategoryId: jest.fn(),
      resetPlansData: jest.fn(),
      validateAgeLimit: jest.fn(),
      youngTariffCheck: jest.fn(),
      clickOnAddToBasket: jest.fn(),
      clickOnBuyPlanOnly: jest.fn(),
      ageLimitSuccess: jest.fn(),
      getTariffAddonPrice: jest.fn()
    };
    const component = mount<TariffHeader>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <TariffHeader {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    (component.find('TariffHeader').instance() as TariffHeader).changeItem(
      {
        id: 1,
        title: 'string',
        disabled: true,
        demo: 'string'
      },
      true
    );
    (component.find('TariffHeader').instance() as TariffHeader).findPos(({
      offsetParent: 2,
      offsetTop: 45
    } as unknown) as HTMLElement);
    (component
      .find('TariffHeader')
      .instance() as TariffHeader).scrolltoDiscounts();
    (component.find('TariffHeader').instance() as TariffHeader).getSelectedItem(
      {
        label: 'string',
        value: 'string',
        isSelected: true
      }
    );
  });
});
