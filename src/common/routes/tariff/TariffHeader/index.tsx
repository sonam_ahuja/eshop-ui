import React, { Component, ReactNode } from 'react';
import { Anchor, Select } from 'dt-components';
import { IProps } from '@tariff/index';
import { ILabelValue } from '@tariff/store/types';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import CatalogBreadCrumb from '@common/components/CatalogBreadCrumb';
import { isMobile } from '@common/utils';
import StyledTariffHeader from '@tariff/TariffHeader/styles';
import EVENT_NAME from '@events/constants/eventName';
import { showDropdown } from '@tariff/utils';
import htmlKeys from '@common/constants/appkeys';
import appConstants from '@src/common/constants/appConstants';

export interface ITariffHeaderProps extends IProps {
  planSelectionOnly: boolean;
}
class TariffHeader extends Component<ITariffHeaderProps> {
  ref: HTMLElement | null = null;

  constructor(props: ITariffHeaderProps) {
    super(props);
    this.setRef = this.setRef.bind(this);
    this.getSelectedItem = this.getSelectedItem.bind(this);
    this.changeItem = this.changeItem.bind(this);
  }

  changeItem(selectedItem: IListItem, isDiscount: boolean): void {
    const filter = {
      selectedItem,
      isDiscount
    };
    this.props.fetchTariff(
      null,
      null,
      !this.props.planSelectionOnly,
      false,
      filter,
      true
    );
  }

  parseSelctInput = (arr: ILabelValue[]): IListItem[] => {
    return arr.map((loyal: ILabelValue) => {
      return { title: loyal.label, id: loyal.value, value: loyal.value };
    });
  }

  setRef(_el: HTMLElement | null): void {
    this.ref = _el;
  }

  findPos = (obj: HTMLElement): number => {
    let curtop = 0;
    if (obj && obj.offsetParent) {
      do {
        curtop += obj.offsetTop;
        // tslint:disable-next-line:no-conditional-assignment
      } while ((obj = obj.offsetParent as HTMLElement));

      return curtop;
    }

    return 0;
  }

  scrolltoDiscounts = (): void => {
    this.props.showMagentaDiscount(true);

    setTimeout(() => {
      if (this.ref) {
        const parent = this.ref.parentElement;
        if (parent) {
          const aboutDivEl = parent.querySelector(
            '#aboutdiscounts'
          ) as HTMLElement;
          if (aboutDivEl) {
            const domPostionY = this.findPos(aboutDivEl);
            if (domPostionY) {
              window.scrollTo({
                top: domPostionY,
                behavior: 'smooth'
              });
            }
          }
        }
      }
    }, 0);
  }

  getSelectedItem(selectedItems: ILabelValue): IListItem {
    return {
      id: selectedItems.value,
      title: selectedItems.label
    };
  }

  render(): ReactNode {
    const {
      tariff,
      tariffTranslation,
      globalTranslation,
      categories,
      selectedProductOfferingBenefit,
      selectedProductOfferingTerm
    } = this.props;

    const showLoyality = showDropdown(
      categories,
      htmlKeys.TARIFF_CATEGORY_HIDE_LOYALTY_DROPDOWN
    );
    const showDiscount = showDropdown(
      categories,
      htmlKeys.TARIFF_CATEGORY_HIDE_DISCOUNT_DROPDOWN
    );
    const loaylity = this.parseSelctInput(tariff.loyalty);
    const discounts = this.parseSelctInput(tariff.discounts);

    const termElement =
      loaylity.length === 0 ? null : (
        <Select
          placeholder={tariffTranslation.pleaseSelect}
          items={loaylity}
          useNativeDropdown={isMobile.phone || isMobile.tablet}
          selectedItem={this.getSelectedItem(selectedProductOfferingTerm)}
          onItemSelect={item => this.changeItem(item, false)}
        />
      );
    const benefitElement =
      discounts.length === 0 ? null : (
        <Select
          placeholder={tariffTranslation.pleaseSelect}
          items={discounts}
          useNativeDropdown={isMobile.phone || isMobile.tablet}
          selectedItem={this.getSelectedItem(selectedProductOfferingBenefit)}
          onItemSelect={item => this.changeItem(item, true)}
        />
      );
    const breadCrumbItems = [
      {
        title: globalTranslation.home,
        active: false,
        link: '/'
      },
      {
        title: categories.name,
        active: true
      }
    ];
    const termConditionEl =
      tariff.plans.length === 1 ? (
        <Anchor
          className='linker'
          hreflang={appConstants.LANGUAGE_CODE}
          title={this.props.tariffTranslation.termsAndConditions}
          onClickHandler={() => this.props.showTermAndConditions(true)}
          data-event-id={EVENT_NAME.TARIFF.EVENTS.TERMS_CONDITION}
          data-event-message={this.props.tariffTranslation.termsAndConditions}
        >
          <span>{this.props.tariffTranslation.termsAndConditions}</span>
        </Anchor>
      ) : null;

    return (
      <StyledTariffHeader ref={this.setRef}>
        <div className='main-plans-wrapper'>
          {!this.props.planSelectionOnly ? (
            <CatalogBreadCrumb
              breadCrumbItems={breadCrumbItems}
              children={null}
            />
          ) : null}
          <div className='header-panel'>
            <div className='left-panel'>
              <h1>
                {this.props.planSelectionOnly
                  ? tariffTranslation.selectBestPlansText
                  : tariffTranslation.getBestRateText}
              </h1>
              <p>
                {categories.description}{' '}
                <span className='hidden-xs visible-lg'>
                  {!this.props.planSelectionOnly ? termConditionEl : null}
                </span>
              </p>
              {!this.props.planSelectionOnly ? (
                <div className='visible-xs hidden-lg'>
                  <Anchor
                    className='linker'
                    hreflang={appConstants.LANGUAGE_CODE}
                    title={tariffTranslation.aboutDiscounts}
                    onClickHandler={this.scrolltoDiscounts}
                    data-event-id={EVENT_NAME.TARIFF.EVENTS.ABOUT_DISCOUNT}
                    data-event-message={tariffTranslation.aboutDiscounts}
                  >
                    <span>{tariffTranslation.aboutDiscounts}</span>
                  </Anchor>
                </div>
              ) : null}
            </div>
            <div className='right-panel'>
              <ul className='list-inline'>
                {showLoyality === 'true' && (
                  <li className='dropdown-wrap'>{termElement}</li>
                )}
                {showDiscount === 'true' && (
                  <li className='dropdown-wrap'>{benefitElement}</li>
                )}
              </ul>
              {!this.props.planSelectionOnly ? (
                <div className='visible-lg hidden-xs'>
                  <Anchor
                    className='linker'
                    hreflang={appConstants.LANGUAGE_CODE}
                    title={tariffTranslation.aboutDiscounts}
                    onClickHandler={this.scrolltoDiscounts}
                    data-event-id={EVENT_NAME.TARIFF.EVENTS.ABOUT_DISCOUNT}
                    data-event-message={tariffTranslation.aboutDiscounts}
                  >
                    <span>{tariffTranslation.aboutDiscounts}</span>
                  </Anchor>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </StyledTariffHeader>
    );
  }
}
export default TariffHeader;
