import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

const StyledTariffHeader = styled.div`
  padding: 1rem 1.25rem 0rem;
  background: ${colors.coldGray};
  .header-panel {
    margin-top: 0.5rem;
    padding-top: 0rem;
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    flex-direction: column;
    .left-panel {
      width: 100%;
      text-align: left;
      h1 {
        font-size: 2.5rem;
        line-height: 2.75rem;
        color: ${colors.darkGray};
        letter-spacing: -0.25px;
        font-weight: 900;
        margin-bottom: 0.5rem;
      }
      p {
        font-size: 0.875rem;
        line-height: 1.25rem;
        color: ${colors.ironGray};
        max-width: 16.5rem;
      }
      .linker {
        font-size: 0.875rem;
        margin-top: 0.75rem;
        line-height: 1.25rem;
      }
    }
    .right-panel {
      width: 100%;
      text-align: left;
      margin-top: auto;
      .list-inline {
        width: 100%;
        display: flex;
        justify-content: flex-end;
        li {
          margin-right: 0.25rem;
          line-height: 1.25rem;
          &:last-child {
            margin-right: 0;
          }
          &.dropdown-wrap {
            width: calc(50% - 0.125rem);
            background: ${hexToRgbA(colors.white, 0.5)};
            padding: 0rem;
            font-size: 0.875rem;
            line-height: 1.25rem;
            border-radius: 8px;
            border: 1px solid ${colors.ultraLightGray};
            margin-top: 2.25rem;
            .styledSelectWrap {
              padding: 0.75rem 1.0625rem 0.75rem 1rem;
              display: flex;
              justify-content: space-between;
              .styledSelect {
                color: ${colors.darkGray};
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
              }
            }
            .optionsList {
              margin-top: -2rem;
              border-radius: 0.4rem;
              left: -1px;
              width: calc(100% + 2px);
              div {
                color: ${colors.darkGray};
                padding: 0.25rem 0.75rem;
                font-size: 0.875rem;
                line-height: 1.25rem;
                font-weight: bold;
                height: auto;
              }
            }
          }
        }
      }
      .linker {
        font-size: 0.875rem;
        line-height: 1.25rem;
        color: ${colors.magenta};
        margin-top: 1.75rem;
        width: 100%;
        justify-content: flex-end;
      }
    }
    .visible-xs {
      display: block;
    }
    .hidden-xs {
      display: none;
    }
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 1rem 2.5rem 0rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 1rem 3rem 0rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 1rem 4.9rem 0rem;
    .header-panel {
      margin-top: 0rem;
      padding-top: 0.5rem;
      flex-direction: row;
      .left-panel {
        text-align: left;
        flex-shrink: 0;
        max-width: 60%;
        width: auto;
        h1 {
          font-size: 2.5rem;
          line-height: 2.75rem;
          color: ${colors.darkGray};
          letter-spacing: -0.25px;
          font-weight: 900;
          margin-bottom: 0.5rem;
          word-break: break-word;
        }
        p {
          font-size: 0.875rem;
          line-height: 1.25rem;
          color: ${colors.mediumGray};
          max-width: inherit;
          word-break: break-word;
          .visible-lg {
            display: inline-block;
          }
          .linker {
            margin: 0;
          }
        }
      }
      .right-panel {
        text-align: right;
        margin-top: initial;
        max-width: 40%;
        .list-inline {
          text-align: right;
          width: auto;
          flex-wrap: wrap;
          li {
            margin-right: 0.25rem;
            line-height: 1.25rem;
            &:last-child {
              margin-right: 0;
            }
            &.dropdown-wrap {
              width: 8.45rem;
              background: ${hexToRgbA(colors.white, 0.4)};
              padding: 0rem 0rem;
              font-size: 0.75rem;
              line-height: 1rem;
              border-radius: 6px;
              border: 1px solid ${colors.warmGray};
              margin-top: 0.5rem;
              .styledSelectWrap {
                padding: 0.625rem 0.75rem 0.625rem 1rem;
                .styledSelect {
                  color: ${colors.darkGray};
                  text-overflow: ellipsis;
                  white-space: nowrap;
                  overflow: hidden;
                }
              }
              .optionsList {
                margin-top: -2.5rem;
                border-radius: 0.4rem;
                left: -1px;
                width: calc(100% + 2px);
                div {
                  color: ${colors.darkGray};
                  padding: 0.25rem 0.75rem;
                  font-size: 0.875rem;
                  line-height: 1.25rem;
                  font-weight: bold;
                  height: auto;
                  text-align: left;
                }
              }
            }
          }
        }
        .linker {
          font-size: 0.875rem;
          line-height: 1.25rem;
          color: ${colors.magenta};
          margin-top: 1.75rem;
          justify-content: flex-end;
          width: auto;
        }
      }
      .visible-lg {
        display: block;
        clear: both;
      }
      .hidden-lg {
        display: none;
      }
    }
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding: 1rem 5.5rem 0rem;
  }
`;

export default StyledTariffHeader;
