import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import TariffPrice, { IProps } from '@tariff/TariffPlans/Common/TariffPrice';
import { totalPrices } from '@mocks/tariff/tariff.mock';

describe('<BenefitsAccordion />', () => {
  const props: IProps = {
    totalPrices,
    reverseOrder: true,
    priceFirst: true,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    hideActualPriceCurrency: true
  };
  const componentWrapper = (newProps: IProps) =>
    mount(
      <ThemeProvider theme={{}}>
        <TariffPrice {...newProps} />
      </ThemeProvider>
    );
  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when reverseOrder is undefined', () => {
    const newProps: IProps = { ...props, reverseOrder: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when priceFirst is undefined', () => {
    const newProps: IProps = { ...props, priceFirst: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when hideActualPriceCurrency is undefined', () => {
    const newProps: IProps = { ...props, hideActualPriceCurrency: undefined };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when prices is undefined', () => {
    const newProps: IProps = { ...props, totalPrices: [] };
    const component = componentWrapper(newProps);
    expect(component).toMatchSnapshot();
  });
});
