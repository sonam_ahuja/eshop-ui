import React from 'react';
import { IPrices } from '@tariff/store/types';
import { getPriceAndCurrency } from '@tariff/store/utils';
import { isMobile } from '@common/utils';
import cx from 'classnames';
import { ICurrencyConfiguration } from '@src/common/store/types/configuration';

export interface IProps {
  totalPrices: IPrices[];
  reverseOrder?: boolean;
  priceFirst?: boolean;
  hideActualPriceCurrency?: boolean;
  showEmptySpace?: boolean;
  currency: ICurrencyConfiguration;
}

const TariffPrice = (props: IProps) => {
  const price = props.totalPrices && props.totalPrices[0];

  if (!price) {
    return null;
  }

  const { discountPrice, discountCurrency } = getPriceAndCurrency(
    price.discountedValue,
    props.currency.locale
  );

  const actualPriceObj = getPriceAndCurrency(
    price.actualValue,
    props.currency.locale
  );
  const actualPrice = actualPriceObj.discountPrice;
  const actualCurrency = actualPriceObj.discountCurrency;
  // reverseOrder

  const discountedPriceDiv =
    price.discounts && price.discounts.length !== 0 ? (
      <span className='modify-text'>
        <span className='value'>
          {actualPrice} {''}
          <sup>{props.hideActualPriceCurrency ? actualCurrency : ''}</sup>
        </span>
      </span>
    ) : (
      props.showEmptySpace && (
        <span className='modify-text'>
          <span className='value'>&nbsp;&nbsp;&nbsp;&nbsp;</span>
        </span>
      )
    );
  const priceClassName = cx(
    !isMobile.phone && !isMobile.tablet && props.priceFirst
      ? 'price-text'
      : 'titles price-text-app'
  );

  const originalPriceDiv =
    !isMobile.phone && props.priceFirst ? (
      <h2 className={priceClassName}>
        {discountPrice}
        <sup>{discountCurrency}</sup>
      </h2>
    ) : (
      <span className={priceClassName}>
        {discountPrice}
        <sup>{discountCurrency}</sup>
      </span>
    );

  return (
    <>
      {props.reverseOrder ? (
        <>
          {originalPriceDiv} {discountedPriceDiv}
        </>
      ) : (
        <>
          {discountedPriceDiv} {originalPriceDiv}
        </>
      )}
    </>
  );
};

export default TariffPrice;
