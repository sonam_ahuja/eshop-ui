import React, { Component, ReactNode } from 'react';
import { Anchor } from 'dt-components';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { getAllPlansTitles, getAllPlansValues } from '@tariff/utils';
import MultiPlansWrap from '@src/common/routes/tariff/TariffPlans/DesktopPlans/styles/MultiPlanStyles';
import { IPlans } from '@tariff/store/types';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import appConstants from '@src/common/constants/appConstants';
import { calculateTruncation } from '@src/common/utils/calculateTruncation';

export interface IState {
  viewMore: boolean;
}

export interface IProps extends IPlansProps {
  setMultiplePlansWrapRef(ref: HTMLElement | null): void;
}

export class MultiplePlanComponent extends Component<IProps, IState> {
  multiplePlansRef: HTMLElement | null = null;

  constructor(props: IProps) {
    super(props);
    this.reCalculateHeights = this.reCalculateHeights.bind(this);
    this.setPlanNameRef = this.setPlanNameRef.bind(this);
    // vieMore will be true if this.props.planSelectionOnly is true, otherwise false
    this.state = {
      viewMore: this.props.planSelectionOnly
    };
  }

  setPlanNameRef(ref: HTMLElement | null): void {
    if (ref) {
      const innerDiv = ref.querySelector(
        '.multipleTruncateEllipsis'
      ) as HTMLElement;
      if (innerDiv) {
        calculateTruncation(innerDiv);
      }
    }
  }

  reCalculateHeights(): void {
    if (!this.multiplePlansRef) {
      return;
    }
    const plansBodyWrapEl = this.multiplePlansRef.querySelectorAll(
      '.plans-body-wrap'
    );

    const maxHeightArr: number[] = [];
    plansBodyWrapEl.forEach(planBody => {
      const lists = planBody.querySelectorAll<HTMLElement>('.lists');

      lists.forEach(listItem => {
        listItem.style.height = 'auto';
      });

      requestAnimationFrame(() => {
        lists.forEach((listItem, listItemIndex) => {
          if (!maxHeightArr[listItemIndex]) {
            maxHeightArr[listItemIndex] = 0;
          }
          if (listItem.offsetHeight > maxHeightArr[listItemIndex]) {
            maxHeightArr[listItemIndex] = listItem.offsetHeight;
          }
        });
      });
    });

    plansBodyWrapEl.forEach(planBody => {
      const lists = planBody.querySelectorAll<HTMLElement>('.lists');
      requestAnimationFrame(() => {
        lists.forEach((listItem, listItemIndex) => {
          listItem.style.height = `${maxHeightArr[listItemIndex]}px`;
        });
      });
    });
  }

  componentDidMount(): void {
    requestAnimationFrame(() => {
      requestAnimationFrame(() => {
        this.reCalculateHeights();
      });
    });
  }

  renderBenefits = (): ReactNode => {
    const { viewMore } = this.state;
    const { tariff } = this.props;
    const config = getCharactersticsConfig(tariff.categories.characteristics);

    return tariff.plans.map((plan: IPlans) => {
      return getAllPlansValues(
        plan,
        {
          ...this.props,
          toggleSelectedBenefit: (...args) => {
            this.props.toggleSelectedBenefit(...args);
            this.reCalculateHeights();
          }
        },
        config.showBenefits ? viewMore : true,
        this.setPlanNameRef
      );
    });
  }

  onClickHandler = () => {
    this.setState({ viewMore: !this.state.viewMore });
  }

  showTermAndConditions = () => {
    this.props.showTermAndConditions(true);
  }

  setRef(multiplePlansRef: HTMLElement | null): void {
    this.multiplePlansRef = multiplePlansRef;
  }

  render(): ReactNode {
    const { viewMore } = this.state;
    const config = getCharactersticsConfig(
      this.props.tariff.categories.characteristics
    );

    return (
      <MultiPlansWrap
        length={this.props.tariff.plans.length}
        planSelectionOnly={this.props.planSelectionOnly}
      >
        <div className='main-plans-wrapper'>
          <div className='panel-wrap'>
            <div
              ref={ref => {
                this.setRef(ref);
                this.props.setMultiplePlansWrapRef(ref);
              }}
              className='multiple-plans'
            >
              <div className='pricing-plans-wrapper clearfix'>
                {getAllPlansTitles(
                  this.props,
                  config.showBenefits ? viewMore : true,
                  this.onClickHandler
                )}
                <div className='flex-container'>{this.renderBenefits()}</div>
              </div>
            </div>
          </div>
          {!this.props.planSelectionOnly ? (
            <div className='btn-wrapper clearfix'>
              <div className='right-panel'>
                <Anchor
                  className='linker'
                  hreflang={appConstants.LANGUAGE_CODE}
                  title={this.props.tariffTranslation.termsAndConditions}
                  onClickHandler={this.showTermAndConditions}
                >
                  <span>{this.props.tariffTranslation.termsAndConditions}</span>
                </Anchor>
              </div>
            </div>
          ) : null}
        </div>
      </MultiPlansWrap>
    );
  }
}

export default MultiplePlanComponent;
