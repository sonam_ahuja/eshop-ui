import { TARIFF_BENEFIT_TEMPLATES } from '@common/constants/appkeys';
import React, { Component, ReactNode } from 'react';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { IBenefits, IPlans } from '@tariff/store/types';
import SinglePlanWrap from '@tariff/TariffPlans/DesktopPlans/styles/SinglePlanStyles';
import { Button, Icon } from 'dt-components';
import ButtonTemplate from '@tariff/TariffPlans/Plans/templates/button';
import DropdownTemplate from '@tariff/TariffPlans/Plans/templates/dropdown';
import ImageTemplate from '@tariff/TariffPlans/Plans/templates/image';
import { isMobile } from '@src/common/utils';
import checkBestDeviceOffers from '@tariff/utils/checkBestDeviceOffers';
import EVENT_NAME from '@events/constants/eventName';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import TariffPrice from '@tariff/TariffPlans/Common/TariffPrice';
import getIconOrLabel from '@tariff/common/getIconOrLabel';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import getTemplate from '@tariff/utils/getTemplate';

// @
// import { getFormatedCurrencyValue } from '@common/utils/currency';

export interface IState {
  viewMore: boolean;
}
export class SinglePlanComponent extends Component<IPlansProps, IState> {
  constructor(props: IPlansProps) {
    super(props);

    this.state = {
      viewMore: true
    };
    this.renderTemplatisedBenefits = this.renderTemplatisedBenefits.bind(this);
  }

  onClickHandler = () => {
    this.setState({ viewMore: !this.state.viewMore });
  }

  // tslint:disable-next-line: cognitive-complexity
  renderTemplatisedBenefits(plans: IPlans): ReactNode {
    const {
      tariffConfiguration,
      toggleSelectedBenefit,
      categories,
      currency
    } = this.props;
    const { benefits, id } = plans;

    const config = getCharactersticsConfig(categories.characteristics);

    const listEl = benefits.map((benefit: IBenefits) => {
      const template = getTemplate(tariffConfiguration, benefit.name);

      const templateProps = {
        label: benefit.label,
        tariffValues: benefit.values,
        tariffConfiguration,
        toggleBenefitSelection: (
          benefitValue: string,
          newSelected: boolean
        ) => {
          toggleSelectedBenefit(
            id,
            benefit.name,
            benefitValue,
            template.templateType,
            newSelected
          );
          // API call to fetch new prices for a tariff
          this.props.getTariffAddonPrice(id);
        }
      };
      // to identify addons isSelectable needs to be true
      if (!config.showAddons && benefit.isSelectable) {
        // TODO: TEST FOR THIS SCENARIO showAddons
        // return (
        //   <li className='lists'>
        //     <span className='titles' />
        //     <span className='data-text' />
        //   </li>
        // );

        return null;
      }
      // else in other cases isSelectable is false, we will simply render on ui list items
      if (!benefit.isSelectable) {
        return (
          <li className='lists'>
            <span className='titles'>{benefit.label}</span>
            {getIconOrLabel(benefit)}
          </li>
        );
      }
      // for addons we need to render according to their templates
      switch (template.templateName) {
        case TARIFF_BENEFIT_TEMPLATES.BUTTON: {
          if (isMobile.phone || isMobile.tablet) {
            return <DropdownTemplate {...templateProps} useImage={false} />;
          }

          return <ButtonTemplate {...templateProps} />;
        }
        case TARIFF_BENEFIT_TEMPLATES.DROPDOWN: {
          return <DropdownTemplate {...templateProps} useImage={false} />;
        }
        case TARIFF_BENEFIT_TEMPLATES.IMAGE: {
          if (isMobile.phone || isMobile.tablet) {
            return <DropdownTemplate {...templateProps} useImage={true} />;
          }

          return <ImageTemplate {...templateProps} />;
        }
        default: {
          return (
            <li className='lists'>
              <span className='titles'>{benefit.label}</span>
              {getIconOrLabel(benefit)}
            </li>
          );
        }
      }
    });

    return (
      <>
        <li className='flex-single-plans'>
          <ol className='flex-all-list clearfix'>
            {config.showBenefits && config.showAddons && listEl}
            {config.showUnlimitedBenefits && this.renderCommonBenifits(plans)}
          </ol>
          <ol className='flex-price-list clearfix'>
            <li className='lists'>
              <span className='titles'>&nbsp;</span>
              {tariffConfiguration.tariffListingTemplate ===
              TARIFF_LIST_TEMPLATE.PRICE_FIRST
                ? config.showPlanName && (
                    <span className='price-text'>{plans.name}</span>
                  )
                : config.showPrice && (
                    <>
                      <TariffPrice
                        totalPrices={plans.totalPrices}
                        currency={currency}
                        priceFirst={true}
                        hideActualPriceCurrency={true}
                      />
                    </>
                  )}
            </li>
          </ol>
        </li>
      </>
    );
  }

  renderCommonBenifits = (plans: IPlans) => {
    return plans.commonBenefits.map((benefit: IBenefits, index: number) => {
      return (
        <li key={index + plans.commonBenefits.length} className='lists'>
          <span className='titles'>{benefit.label}</span>
          <span className='data-text'>{getIconOrLabel(benefit)}</span>
        </li>
      );
    });
  }

  render(): ReactNode {
    const {
      addPlanToBasket,
      redirectToListingPage,
      categories,
      confirmPlanBtnLoading,
      currency
    } = this.props;
    const config = getCharactersticsConfig(categories.characteristics);
    const plans = this.props.tariff.plans[0];

    // tslint:disable-next-line:no-commented-code
    // const { viewMore } = this.state;
    const { tariffTranslation, tariffConfiguration } = this.props;

    const bestOfferIconEl = checkBestDeviceOffers(plans) ? (
      <div className='icon-wrap special-offer'>
        <Icon size='inherit' name='ec-special-offer' />
      </div>
    ) : null;

    return (
      <SinglePlanWrap>
        <div className='main-plans-wrapper'>
          <div className='panel-wrap'>
            <div className='single-wrap'>
              <header className='clearfix'>
                <div className='title-wrap'>
                  {tariffConfiguration.tariffListingTemplate ===
                  TARIFF_LIST_TEMPLATE.NAME_FIRST ? (
                    <h3>{config.showPlanName && plans.name}</h3>
                  ) : (
                    config.showPrice && (
                      <TariffPrice
                        totalPrices={plans.totalPrices}
                        reverseOrder={true}
                        currency={currency}
                        hideActualPriceCurrency={true}
                      />
                    )
                  )}
                </div>
                {bestOfferIconEl}
              </header>
              <div className='plans-body-wrap clearfix'>
                <ul className='plans-box list-inline'>
                  {this.renderTemplatisedBenefits(plans)}
                </ul>
              </div>
            </div>
          </div>
          <div className='btn-wrapper clearfix'>
            <div className='left-panel' />
            <div className='right-panel'>
              <ul className='list-inline'>
                {!this.props.planSelectionOnly && config.showBuyButton ? (
                  <li>
                    <Button
                      className={
                        config.showAddButton
                          ? 'btn btn-line'
                          : 'btn btn-primary'
                      }
                      onClickHandler={() => addPlanToBasket(plans.id)}
                      data-event-id={EVENT_NAME.TARIFF.EVENTS.BUY_PLAN}
                      data-event-message={tariffTranslation.buyPlanOnly}
                    >
                      {tariffTranslation.buyPlanOnly}
                    </Button>
                  </li>
                ) : null}
                {!this.props.planSelectionOnly && config.showAddButton ? (
                  <li>
                    <Button
                      className={'btn btn-primary'}
                      onClickHandler={() => redirectToListingPage(plans.id)}
                      data-event-id={EVENT_NAME.TARIFF.EVENTS.ADD_TO_PHONE}
                      data-event-message={tariffTranslation.addAPhone}
                    >
                      {tariffTranslation.addAPhone}
                    </Button>
                  </li>
                ) : null}
                {this.props.planSelectionOnly ? (
                  <li>
                    <Button
                      className='btn btn-primary btn-fluid'
                      onClickHandler={() => redirectToListingPage(plans.id)}
                      loading={confirmPlanBtnLoading}
                      data-event-id={EVENT_NAME.TARIFF.EVENTS.CONFIRM_PLAN}
                      data-event-message={tariffTranslation.confirmPlan}
                      type={'complementary'}
                    >
                      {tariffTranslation.confirmPlan}
                    </Button>
                  </li>
                ) : null}
              </ul>
            </div>
          </div>
        </div>
      </SinglePlanWrap>
    );
  }
}

// tslint:disable-next-line:max-file-line-count
export default SinglePlanComponent;
