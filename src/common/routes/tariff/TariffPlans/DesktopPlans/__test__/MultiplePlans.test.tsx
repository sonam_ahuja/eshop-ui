import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import MultiplePlans, {
  MultiplePlanComponent
} from '@tariff/TariffPlans/DesktopPlans/MultiplePlans';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { histroyParams } from '@mocks/common/histroy';
import appState from '@store/states/app';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

describe('<MultiplePlans />', () => {
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  const props: IPlansProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    getTariffAddonPrice: jest.fn(),
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    tariff: appState().tariff,
    planSelectionOnly: false,
    tariffTranslation: appState().translation.cart.tariff,
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    selectedProductOfferingTerm: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    globalTranslation: appState().translation.cart.global,
    categories: appState().categories.categories,
    selectedProductOfferingBenefit: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    showTermAndConditions: jest.fn(),
    setTariffId: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    redirectToListingPage: jest.fn(),
    addPlanToBasket: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn(),
    setMultiplePlansWrapRef: jest.fn(),
    setMobileAccordionPanelRef: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <MultiplePlans {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<MultiplePlanComponent>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <MultiplePlanComponent {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component
      .find('MultiplePlanComponent')
      .instance() as MultiplePlanComponent).onClickHandler();
    (component
      .find('MultiplePlanComponent')
      .instance() as MultiplePlanComponent).showTermAndConditions();
  });
});
