import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import SinglePlan, {
  SinglePlanComponent
} from '@tariff/TariffPlans/DesktopPlans/SinglePlan';
import { histroyParams } from '@mocks/common/histroy';
import { plan } from '@mocks/tariff/tariff.mock';
import appState from '@store/states/app';
import { Characteristics } from '@mocks/productList/productList.mock';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { RootState } from '@src/common/store/reducers';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

describe('<SinglePlan />', () => {
  const props: IPlansProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    getTariffAddonPrice: jest.fn(),
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    tariff: appState().tariff,
    planSelectionOnly: false,
    tariffTranslation: appState().translation.cart.tariff,
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    selectedProductOfferingTerm: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    globalTranslation: appState().translation.cart.global,
    categories: appState().categories.categories,
    selectedProductOfferingBenefit: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    showTermAndConditions: jest.fn(),
    setTariffId: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    redirectToListingPage: jest.fn(),
    addPlanToBasket: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn(),
    setMultiplePlansWrapRef: jest.fn(),
    setMobileAccordionPanelRef: jest.fn()
  };
  plan.characteristics = Characteristics;
  props.tariff.plans[0] = plan;
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <SinglePlan {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<SinglePlanComponent>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <SinglePlanComponent {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component
      .find('SinglePlanComponent')
      .instance() as SinglePlanComponent).onClickHandler();
  });
});
