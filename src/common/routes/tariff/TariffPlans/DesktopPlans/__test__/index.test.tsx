import React from 'react';
import {
  MultiPlansWrap,
  SinglePlanStyles
} from '@src/common/routes/tariff/TariffPlans/DesktopPlans/styles';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';

describe('<Import />', () => {
  const data: {
    length: number;
    planSelectionOnly: boolean;
  } = {
    length: 2,
    planSelectionOnly: true
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <div>
            <MultiPlansWrap {...data} />
            <SinglePlanStyles />
          </div>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
