import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

const MultiPlansWrap = styled.div<{
  length: number;
  planSelectionOnly: boolean;
}>`
  .main-plans-wrapper {
    padding: 0rem 4.9rem 4rem;
    background: ${colors.coldGray};
    .panel-wrap {
      padding: 3rem 0 1.5rem;
      .list-inline {
        li {
          margin-right: 0.25rem;
          line-height: 1.25rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
      .multiple-plans {
        .pricing-plans-wrapper {
          display: flex;

          .aside-panel {
            width: 7.25rem;
            display: flex;
            flex-direction: column;
            flex-shrink: 0;
            .plans-header {
              background: ${colors.transparent};
              border-bottom: 1px solid ${colors.silverGray};
              padding: 1rem 0;
              border-radius: 0;
              align-items: center;
              .title-wrap {
                float: none;
                width: 100%;
                text-align: center;
                h3 {
                  color: ${colors.darkGray};
                  font-size: 0.75rem;
                  line-height: 1rem;
                  text-align: left;
                }
              }
            }
            .plans-body-wrap {
              background: transparent;
              padding: 0;
              border-radius: 0px;
              padding-bottom: 0.75rem;

              .plans-box {
                display: block;
                .lists {
                  border-bottom: 1px solid ${colors.silverGray};
                  padding: 0.8rem 0;
                  min-height: 3.75rem;
                  display: flex;
                  align-items: center;
                  justify-content: flex-start;
                  transition: height 0.4s linear;
                }
              }
            }
            .btn-wrapper {
              min-height: 2.5rem;
              .linker {
                justify-content: flex-start;
              }
            }
          }
          .flex-container {
            width: calc(100% - 7.25rem);
            display: flex;
            padding-left: 1rem;
            justify-content: space-between;
            flex-grow: 0;
            .flex-panel {
              flex: 1 1 auto;
              margin-right: 0.5rem;
              text-align: center;
              border-radius: 0.5rem;
              border: 1px solid transparent;
              transition: all 0.5s ease;
              display: flex;
              flex-direction: column;
              ${({ length }) =>
                length ? `width:calc(${100 / length}% - 0.5rem)` : ''};
              cursor: default;
              flex-shrink: 0;
              &:focus,
              &:active {
                border: 1px solid ${colors.magenta};
              }
              &.active {
                border: 1px solid ${colors.magenta};
              }
              &.special-offer {
                .icon-wrap {
                  visibility: visible;
                }
              }
              &:last-child {
                margin-right: 0;
              }
            }
            .btn-wrapper{
              position:sticky;
              bottom:0;
              background:${colors.white};
            }
          }
        }
        .plans-body-wrap {
          background: ${colors.white};
          padding: 0rem 1rem 0.75rem;
          border-bottom-left-radius: 0.5rem;
          border-bottom-right-radius: 0.5rem;

          .plans-box {
            display: block;
            .lists {
              width: 100%;
              margin: 0;
              border-bottom: 1px solid ${colors.silverGray};
              padding: 0.8rem 0;
              min-height: 3.75rem;
              display: flex;
              align-items: center;
              justify-content: center;
              transition: height 0.4s linear;
              &:last-child {
                border-bottom: 1px solid transparent;
              }
            }
            .check-icon {
              font-size: 1rem;
              color: ${colors.black};
            }
            .titles {
              color: ${colors.darkGray};
              font-size: 0.75rem;
              line-height: 1rem;
              text-align: left;
              word-break: break-word;
              sup {
                vertical-align: super;
                font-size: 0.625rem;
                line-height: 0.75rem;
                font-weight: 500;
              }
            }
            .data-text {
              font-size: 1rem;
              line-height: 1.5rem;
              color: ${colors.mediumGray};
              font-weight: 500;
              display: block;
              word-break: break-word;
              strong {
                color: ${colors.black};
              }
            }
            .empty-value {
              display: flex;
              flex-shrink: 0;
              width: 100%;
              align-items: center;
              justify-content: center;
              &:before {
                content: '';
                width: 13px;
                height: 1px;
                background: ${colors.warmGray};
              }
            }
            .price-text {
              font-size: 1.5rem;
              line-height: 1.75rem;
              color: ${colors.magenta};
              font-weight: 600;
              word-break: break-word;
              sup {
                vertical-align: super;
                font-size: 0.625rem;
                line-height: 0.75rem;
                font-weight: 500;
              }
            }
            .circle-list {
              display: flex;
              align-items: center;
              justify-content: center;
              li {
                border-radius: 50px;
                height: 1.75rem;
                width: 1.75rem;
                border: 1px solid transparent;
                display: flex;
                align-items: center;
                justify-content: center;
                cursor: pointer;
                i {
                  font-size: 0.75rem;
                  color: #a4a4a4;
                }
                &.active {
                  border-color: ${colors.magenta};
                  i {
                    color: ${colors.black};
                  }
                  .social-icons {
                    filter: grayscale(0%);
                  }
                }
                .social-icons {
                  border-radius: 50%;
                  filter: grayscale(100%);
                  width: 100%;
                  height: 100%;
                  div {
                    width: 0.75rem;
                    height: 0.75rem;
                    margin: auto;
                  }
                }
              }
            }
            .rectangle-list {
               width:100%;
              .rect-list-value {
                display: flex;
                flex-wrap: wrap;
                padding: 0;
                align-items: center;
                justify-content: center;
                position: relative;
                top: -0.15rem;
              }
              .filter-chips {
                border-radius: 6px;
                border: 1px solid ${colors.warmGray};
                padding: 0.25rem 0.5rem;
                height: auto;
                margin: 0;
                margin-right: 0.15rem;
                // margin-bottom: 0.15rem;
                display: flex;
                margin-top:0.15rem;
                .filterChipsContent {
                  font-size: 0.7rem;
                  color: ${colors.zBlack};
                  display: block;
                  text-align: center;
                  word-break: break-word;
                }
                &.active {
                  border-color: ${colors.magenta};
                  color: ${colors.magenta};
                }
                &:last-child {
                  margin-right: 0;
                }
              }
            }
          }
        }
        .plans-header {
          min-height: 7.5rem;
          display: flex;
          align-items: flex-end;
          justify-content: center;
          padding: 0;
          background: ${colors.cloudGray};
          border-top-left-radius: 0.5rem;
          border-top-right-radius: 0.5rem;
          position: relative;
          padding: 1rem;
          flex-grow: 1;
          .title-wrap {
            float: none;
            width: 100%;
            text-align: center;
            h3 {
              color: ${colors.darkGray};
              font-size: 0.75rem;
              line-height: 1rem;
            }
          }
        }
      }
      header {
        .title-wrap {
          width: calc(100% - 5rem);
          h3 {
            font-size: 1.125rem;
            line-height: 1.25rem;
            color: ${colors.ironGray};
          }
          &.data-first-wrap,
          &.name-first-wrap {
            .titles {
              color: ${colors.black};
            }
          }
          &.data-first-wrap {
            .data-first-plan-name {
              color: ${colors.ironGray};
              font-size: 1rem;
              line-height: 1.5rem;
              display: block;
              font-weight: 500;
            }
            .tittle-hd{
              border-radius: 0.3rem;
              border: 1px solid ${colors.magenta};
              padding: 0.1rem 0.2rem;
              color:${colors.magenta};
              font-size:0.65rem;
              font-weight:600;
              position: relative;
              top: -0.3rem;
              left: 0.1rem;
            }
          }
        }
        .price-text {
          font-size: 1.6rem;
          line-height: 1.8rem;
          color: ${colors.magenta};
          font-weight: 900;
          word-break: break-word;
          margin-bottom:0.25rem;
          sup {
            vertical-align: super;
            font-size: 0.625rem;
            line-height: 0.75rem;
            font-weight: 500;
          }
        }
        .titles {
          font-size: 1rem;
          line-height: 1.5rem;
          color: ${colors.ironGray};
          display: block;
          font-weight: 500;
          sup {
            vertical-align: super;
            font-size: 0.625rem;
            line-height: 0.75rem;
            font-weight: 500;
          }
        }
        .modify-text {
          font-size: 0.75rem;
          line-height: 1rem;
          text-decoration: line-through;
          letter-spacing: -0.3px;
          color: ${colors.magenta};
          text-decoration: line-through;
          display: block;
          .value {
            color: ${colors.darkGray};
            sup {
              vertical-align: super;
              font-size: 0.625rem;
              line-height: 0.75rem;
              font-weight: 500;
              display: inline-block;
            }
          }
        }
        .icon-wrap {
          position: absolute;
          top: 0.15rem;
          right: 0.15rem;
          i {
            font-size: 1.25rem;
          }
        }
        .multipleTruncateEllipsis{
          max-height:4rem;
        }
      }
    }
    .btn-wrapper {
      display: flex;
      align-items: center;
      width: 100%;
      flex-wrap: wrap;
      .btn-primary {
        width: 100%;
      }
      .btn-line {
        font-size: 0.875rem;
        line-height: 1.25rem;
        padding: 0.58rem 1.5rem 0.571rem;
        width: 100%;
        text-align: center;
        background: transparent;
        color: ${colors.magenta};
        display: flex;
        align-items: center;
        justify-content: center;
        height: auto;
      }
      .btn + .btn {
        margin-top: 0.25rem;
      }
      .left-panel {
        width: 35%;
        text-align: left;
      }
      .right-panel {
        width: 65%;
        text-align: right;
        margin-left: 35%;
        margin-top: 1.5rem;
        .list-inline {
          li {
            margin-right: 0.25rem;
            line-height: 1.25rem;
            &:last-child {
              margin-right: 0;
            }
          }
        }
      }
    }
    .view-button {
      font-size: 0.875rem;
      line-height: 1.25rem;
      color: ${colors.magenta};
      font-weight: 500;
      padding: 0.65rem 0rem;
      width: 100%;
    }
    .linker {
      font-size: 0.875rem;
      line-height: 1.25rem;
      color: #e20074;
      margin-top: 0
      /* width: 100%; */
      justify-content: flex-end;
    }
    .select-without-border-wrap {
      .styledSelect {
        font-size: 1rem;
        line-height: 1.5rem;
        color: ${colors.ironGray};
        margin-right: 1rem;
        font-weight: 500;
        &:only-child {
          margin-right: 0;
        }
      }
      .optionsList {
        margin-top: -2rem;
        border-radius: 0.4rem;
        left: -1px;
        width: calc(100% + 0.5rem);
        div {
          color: ${colors.darkGray};
          padding: 0.25rem 0.75rem;
          font-size: 0.875rem;
          line-height: 1.25rem;
          font-weight: bold;
          height: auto;
          text-align: left;
          &:hover {
            background: ${colors.fogGray};
          }
        }
      }
      .caret {
        position: initial;
      }
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) and (max-width: ${
  breakpoints.tabletLandscape
}px) {
    .main-plans-wrapper {
      .panel-wrap {
        header {
          .icon-wrap {
            top: 0.5rem;
            right: 0.5rem;
            i {
              font-size: 1rem;
            }
          }
        }
      }
    }
  }
`;
// tslint:disable:max-file-line-count
export default MultiPlansWrap;
