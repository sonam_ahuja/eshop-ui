import { colors } from '@src/common/variables';
import styled from 'styled-components';
const SinglePlanWrap = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .main-plans-wrapper {
    padding: 0rem 4.9rem 4rem;
    background: ${colors.coldGray};
    .panel-wrap {
      padding: 3rem 0 1rem;
      .list-inline {
        li {
          float: left;
          margin-right: 0.25rem;
          line-height: 1.25rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
      .single-wrap {
        border-radius: 10px;
        border: 1px solid transparent;
        padding: 1rem 0.5rem 0.5rem 0.5rem;
        background: ${colors.cloudGray};
        transition: all 0.5s ease;
        &:hover {
          border-color: ${colors.magenta};
        }
      }
      header {
        display: flex;
        align-items: center;
        padding: 0 0.5rem 0.6rem 1.5rem;
        .title-wrap {
          display: flex;
          width: calc(100% - 5rem);
          float: left;
          h3 {
            font-size: 1.125rem;
            line-height: 1.25rem;
            color: ${colors.ironGray};
            font-weight: 500;
          }
          .modify-text {
            font-size: 1.15rem;
            line-height: 2rem;
            text-decoration: line-through;
            letter-spacing: -0.3px;
            color: ${colors.magenta};
            text-decoration: line-through;
            display: block;
            margin-left: 5px;
            .value {
              color: ${colors.darkGray};
              sup {
                vertical-align: super;
                font-size: 0.625rem;
                line-height: 0.75rem;
                font-weight: 500;
                display: inline-block;
              }
            }
          }
          .price-text-app {
            font-size: 1.75rem;
            line-height: 2rem;
            color: ${colors.magenta};
            font-weight: 900;
            sup {
              vertical-align: super;
              font-size: 0.625rem;
              line-height: 0.75rem;
              font-weight: 500;
            }
          }
          .empty-value {
            display: flex;
            flex-shrink: 0;
            width: 100%;
            align-items: center;
            justify-content: center;
            &:before {
              content: '';
              width: 13px;
              height: 1px;
              background: ${colors.warmGray};
            }
          }
        }
        .icon-wrap {
          width: 5rem;
          float: right;
          text-align: right;
          i {
            font-size: 1.75rem;
          }
        }
      }
      .plans-body-wrap {
        background: ${colors.white};
        padding: 1.5rem 1.5rem 1.5rem 0rem;
        border-radius: 5px;
        .plans-box {
          .lists {
            width: 33.333%;
            margin: 0;
            border-right: 1px solid ${colors.silverGray};
            padding-left: 1.5rem;
            padding-right: 1.5rem;
            &:nth-child(3),
            &:nth-child(6),
            &:nth-child(9),
            &:nth-child(12),
            &:nth-child(15) {
              border-right: 0;
            }
            &:nth-child(4),
            &:nth-child(5),
            &:nth-child(6),
            &:nth-child(7),
            &:nth-child(8),
            &:nth-child(9),
            &:nth-child(10),
            &:nth-child(11),
            &:nth-child(12),
            &:nth-child(13),
            &:nth-child(14),
            &:nth-child(15) {
              padding-top: 2rem;
            }
          }
          .titles {
            font-size: 0.875rem;
            line-height: 1.25rem;
            color: ${colors.mediumGray};
            margin-bottom: 0.75rem;
            display: block;
          }
          .modify-text {
            font-size: 1.15rem;
            line-height: 2rem;
            text-decoration: line-through;
            letter-spacing: -0.3px;
            color: ${colors.magenta};
            text-decoration: line-through;
            display: block;
            .value {
              color: ${colors.darkGray};
              sup {
                vertical-align: super;
                font-size: 0.625rem;
                line-height: 0.75rem;
                font-weight: 500;
              }
            }
          }
          /** psr code added here ***/
          .dt_select {
            min-height: 2rem;
            line-height: 2rem;
            .optionsList {
              top: 0;
              border-radius: 0.4rem;
              width: auto;
              > div {
                font-size: 1rem;
                height: 2rem;
                padding: 0.3rem 0.6rem;
              }
            }
          }
          /** psr code ends here ***/
          .data-text {
            font-size: 1.75rem;
            line-height: 2rem;
            color: ${colors.mediumGray};
            font-weight: bold;
            display: block;
            word-break: break-word;
            strong {
              color: ${colors.darkGray};
            }
          }
          .empty-value {
            display: flex;
            flex-shrink: 0;
            width: 100%;
            align-items: center;
            /* justify-content: center; */
            &:before {
              content: '';
              width: 13px;
              height: 1px;
              background: ${colors.warmGray};
            }
          }
          .price-text {
            font-size: 1.75rem;
            line-height: 2rem;
            color: ${colors.magenta};
            font-weight: 900;
            word-break: break-word;
            sup {
              vertical-align: super;
              font-size: 0.625rem;
              line-height: 0.75rem;
              font-weight: 500;
            }
          }
          .circle-list {
            margin-left: 0;
            li {
              border-radius: 50px;
              height: 2rem;
              width: 2rem;
              border: 1px solid transparent;
              display: flex;
              align-items: center;
              justify-content: center;
              cursor: pointer;
              i {
                font-size: 0.75rem;
                color: #a4a4a4;
              }
              &:hover,
              &:active,
              &:focus,
              &.active {
                border-color: ${colors.magenta};
                i {
                  color: ${colors.black};
                }
                .social-icons {
                  filter: grayscale(0%);
                }
              }
              .social-icons {
                border-radius: 50%;
                filter: grayscale(100%);
                div {
                  width: 0.75rem;
                  height: 0.75rem;
                  margin: auto;
                }
              }
            }
          }
          .rectangle-list {
            width: 100%;
            overflow-x: auto;
            display: flex;
            flex-wrap: nowrap;
            align-items: center;
            justify-content: center;
            overflow-y: hidden;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            scroll-behavior: smooth;
            -webkit-overflow-scrolling: touch;
            .rect-list-value {
              display: flex;
              width: 100%;
              .filter-chips {
                flex: inherit;
                margin-left: 0;
                height: auto;
                margin-top: 0;
                margin-bottom: 0;
                padding: 0.1rem 0.6rem;
                min-height: 1.6rem;
                border-radius: 6px;
                .filterChipsContent {
                  font-size: 0.7rem;
                }
                &:last-child {
                  margin-right: 0;
                }
              }
            }
            &::-webkit-scrollbar {
              display: none;
              width: 0 !important;
            }
          }
          .flex-single-plans {
            width: 100%;
            margin: 0;
            display: flex;
            .flex-all-list {
              width: calc(100% - 13.2rem);
              display: flex;
              flex-wrap: wrap;
              .lists {
                min-height: 4rem;
                flex: 1 1 33.333%;
                max-width: 33.333%;
              }
            }
            .flex-price-list {
              width: 13.2rem;
              border-left: 1px solid ${colors.silverGray};
              padding-left: 1.5rem;
              li {
                width: 100%;
                padding: 0;
                border: 0;
              }
            }
            .check-icon {
              color: ${colors.darkGray};
            }
            .select-without-border-wrap {
              .styledSelectWrap {
                justify-content: flex-start;
                align-items: flex-start;
                .styledSelect {
                  margin-right: 1rem;
                  &:only-child {
                    margin-right: 0;
                  }
                }
                .caret {
                  position: initial;
                  margin-top: 0.5rem;
                }
              }
            }
          }
          h4 {
            word-break: break-word;
          }
        }
      }
    }
    .btn-wrapper {
      display: flex;
      align-items: center;
      .left-panel {
        float: left;
        width: 35%;
        text-align: left;
        .linker {
          justify-content: flex-start;
        }
      }
      .right-panel {
        float: right;
        width: 65%;
        text-align: right;
        .list-inline {
          float: right;
          li {
            float: left;
            margin-right: 0.25rem;
            line-height: 1.75rem;
            &:last-child {
              margin-right: 0;
            }
          }
        }
      }
      ul {
        li {
          margin-right: 0.75rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
      .linker {
        font-size: 0.85rem;
        line-height: 1.25rem;
        color: #e20074;
        margin-top: 0;
        float: right;
        width: 100%;
        justify-content: flex-end;
      }
      .btn-primary {
        font-size: 0.875rem;
        line-height: 1.25rem;
        padding: 0.58rem 1.5rem 0.571rem;
        min-width: 8.75rem;
        text-align: center;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        height: auto;
        .buttonText {
          /* text-transform: none; */
        }
      }
      .btn-line {
        font-size: 0.875rem;
        line-height: 1.25rem;
        padding: 0.58rem 1.5rem 0.571rem;
        min-width: 8.75rem;
        width: 100%;
        text-align: center;
        background: transparent;
        color: ${colors.magenta};
        display: flex;
        align-items: center;
        justify-content: center;
        height: auto;
        .buttonText {
          /* text-transform: none; */
        }
      }
    }
  }
  @media (max-width: 767px) {
    .main-plans-wrapper {
      display: none;
    }
  }
`;

// tslint:disable-next-line:max-file-line-count
export default SinglePlanWrap;
