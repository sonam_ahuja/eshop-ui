import { IProps as ISelectedItemProps } from 'dt-components/lib/es/components/molecules/select/components/selected-item';
import React, {
  Component,
  FunctionComponent,
  ReactNode,
  SyntheticEvent
} from 'react';
import { ITariffTemplateProps } from '@tariff/types';
import { IBenefitsAttachments, IThumbnail } from '@tariff/store/types';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';
import { Icon, Select } from 'dt-components';
import appkeys from '@common/constants/appkeys';
import PreloadImage from '@src/common/components/Preload';
import { IMAGE_TYPE } from '@common/store/enums';
import { isMobile } from '@common/utils';
import { sendDropdownClickEvent } from '@src/common/events/common';

export const getFrontImage = (attachments: IBenefitsAttachments) => {
  const imageObj = attachments
    ? (attachments.thumbnail || []).find((thumbnailItem: IThumbnail) => {
      return thumbnailItem.name === appkeys.PRODUCT_IMAGE_FRONT;
    })
    : null;

  return imageObj ? imageObj.url : '';
};

const selectedImageItemComponent: FunctionComponent<ISelectedItemProps> = (
  props: ISelectedItemProps
) => {
  const { selectedItem } = props;
  const imgSrc = selectedItem && selectedItem.rest ? selectedItem.rest : null;

  return (
    <div className='imgDropdownWrap'>
      <div className='social-icons'>
        <PreloadImage
          isObserveOnScroll={false}
          imageHeight={20}
          width={20}
          mobileWidth={20}
          type={IMAGE_TYPE.WEBP}
          imageWidth={20}
          imageUrl={imgSrc}
          disablePreloadEffect={true}
          intersectionObserverOption={{
            root: null,
            rootMargin: '60px',
            threshold: 0
          }}
        />
      </div>
      <Icon color='currentColor' className='caret' name='dt-caret' />
    </div>
  );
};

export default class TariffDorpDown extends Component<
  ITariffTemplateProps & { useImage: boolean }
  > {

  constructor(props: ITariffTemplateProps & { useImage: boolean }) {
    super(props);
  }

  handleClick = (event: SyntheticEvent) => {
    event.preventDefault();
    event.stopPropagation();
  }

  render(): ReactNode {
    const {
      tariffValues,
      toggleBenefitSelection,
      useImage,
      label
    } = this.props;

    let selectedItem: IListItem | null = null;

    const items = tariffValues.map(benefitItem => {
      const {
        value,
        isSelected,
        attachments,
        label: displayValue
      } = benefitItem;
      const item = {
        id: value,
        title: `${displayValue}`,
        rest: getFrontImage(attachments)
      };
      if (isSelected) {
        selectedItem = item;
      }

      return item;
    });

    const select = (
      <Select
        onClick={this.handleClick}
        onItemSelect={(item: IListItem) => {
          sendDropdownClickEvent(item.title);
          toggleBenefitSelection(String(item.id), true);
        }}
        selectedItem={selectedItem}
        items={items}
        useNativeDropdown={isMobile.phone || isMobile.tablet}
        className='select-without-border-wrap'
        SelectedItemComponent={
          useImage ? selectedImageItemComponent : undefined
        }
      />
    );

    return (
      <>
        {label !== undefined ? <span className='titles'>{label}</span> : null}
        {items.length === 0 ? <span className='empty-value' /> : select}
      </>
    );
  }
}
