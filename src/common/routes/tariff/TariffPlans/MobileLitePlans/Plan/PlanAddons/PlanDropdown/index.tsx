import React from 'react';
import { IBenefits, IPlans } from '@tariff/store/types';
import { ITariffConfiguration } from '@common/store/types/configuration';
import { TARIFF_BENEFIT_TEMPLATE_TYPE } from '@src/common/constants/appkeys';

import DropdownTemplate from '../DropDown';

export interface IProps {
  benefit: IBenefits;
  configuration: ITariffConfiguration;
  plan: IPlans;
  setTariffId(tariffId: string): void;
  getTariffAddonPrice(tariffId: string): void;
  toggleSelectedBenefit(
    planName: string,
    benefitName: string,
    benefitValue: string,
    templateType: string,
    newSelected: boolean
  ): void;
}

const PlanDropdown = (props: IProps) => {
  const {
    plan,
    benefit,
    getTariffAddonPrice,
    toggleSelectedBenefit,
    setTariffId
  } = props;
  const templateProps = {
    tariffValues: benefit.values,
    tariffConfiguration: props.configuration,
    toggleBenefitSelection: (benefitValue: string, newSelected: boolean) => {
      setTariffId(plan.id);
      toggleSelectedBenefit(
        plan.id,
        benefit.name,
        benefitValue,
        TARIFF_BENEFIT_TEMPLATE_TYPE.SINGLE_SELECT,
        newSelected
      );
      // API call to fetch new prices for a tariff -> for a desktop
      getTariffAddonPrice(plan.id);
    }
  };

  return (
    <>
      <DropdownTemplate {...templateProps} useImage={false} />
    </>
  );
};

export default PlanDropdown;
