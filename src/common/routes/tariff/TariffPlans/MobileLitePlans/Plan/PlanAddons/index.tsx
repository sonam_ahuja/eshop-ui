import React from 'react';
import PlanDropdown from '@tariff/TariffPlans/MobileLitePlans/Plan/PlanAddons/PlanDropdown';
import { ICharacteristicConfig, IPlans } from '@tariff/store/types';
import { ITariffConfiguration } from '@common/store/types/configuration';
import getIconOrLabel from '@tariff/common/getIconOrLabel';
import cx from 'classnames';

import { StyledDivider } from '../styles';

import { StyledAddons, StyledLabel, StyledValue } from './styles';

export interface IProps {
  plan: IPlans;
  configuration: ITariffConfiguration;
  config: ICharacteristicConfig;
  getTariffAddonPrice(tariffId: string): void;
  toggleSelectedBenefit(
    planName: string,
    benefitName: string,
    benefitValue: string,
    templateType: string,
    newSelected: boolean
  ): void;
  setTariffId(tariffId: string): void;
}

const PlanAddons = (props: IProps) => {
  const {
    plan,
    toggleSelectedBenefit,
    configuration,
    setTariffId,
    config,
    getTariffAddonPrice
  } = props;

  return (
    <StyledAddons>
      {plan.allBenefits.map((arrBenefit, indx) => {
        return (
          <div
            key={indx}
            className={cx('rowWrapper', {
              _1_elem: plan.allBenefits.length === 1 && arrBenefit.length === 1
            })}
          >
            <ul>
              {arrBenefit && arrBenefit.length
                ? arrBenefit.map((benefit, index) => {
                    return (
                      <li key={index}>
                        {!benefit.isSelectable && config.showBenefits && (
                          <>
                            <StyledLabel>{benefit.label}</StyledLabel>
                            <StyledValue className='value'>
                              {getIconOrLabel(benefit)}
                            </StyledValue>
                          </>
                        )}
                        {benefit.isSelectable && config.showAddons && (
                          <>
                            <StyledLabel>{benefit.label}</StyledLabel>
                            <StyledValue>
                              <PlanDropdown
                                plan={plan}
                                benefit={benefit}
                                configuration={configuration}
                                setTariffId={setTariffId}
                                toggleSelectedBenefit={toggleSelectedBenefit}
                                getTariffAddonPrice={getTariffAddonPrice}
                              />
                            </StyledValue>
                          </>
                        )}
                      </li>
                    );
                  })
                : null}
            </ul>
            <StyledDivider />
          </div>
        );
      })}
    </StyledAddons>
  );
};

export default PlanAddons;
