import styled from 'styled-components';
import { Paragraph } from 'dt-components';

import { StyledDivider } from '../styles';

export const StyledLabel = styled(Paragraph).attrs({
  size: 'small',
  weight: 'bold'
})`
  margin-bottom: 0.15rem;
`;

export const StyledValue = styled(Paragraph).attrs({
  size: 'small',
  weight: 'ultra'
})``;

export const StyledAddons = styled.div`
  padding-top: 1rem;
  ${StyledDivider} {
    margin: 0;
    margin-bottom: 1rem;
  }
  .rowWrapper {
    ul {
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      justify-content: space-between;
      align-items: flex-start;
      margin: 0 -0.125rem;
      height: 100%;
      margin-bottom: 1rem;
      li {
        text-align: center;
        flex-grow: 1;
        flex-shrink: 0;
        flex-basis: 33.33%;
        width: 33.33%;
        padding: 0 0.125rem;
      }

      .value {
        line-height: 1;
      }

      li .dt_select {
        display: inline-flex;
        .caret {
          margin-left: 0.25rem;
        }
        .styledSimpleSelect {
          .dt_paragraph {
            font-size: 0.875rem;
          }
        }
        .styledSimpleSelect + .optionsList {
          width: inherit;
          white-space: nowrap;
          right: auto;
          left: 0;
        }
      }
      &:empty {
        margin: 0;
      }
    }
  }

  .rowWrapper:last-child,
  .rowWrapper:last-of-type {
    ${StyledDivider} {
      display: none;
    }
  }

  .rowWrapper._1_elem {
    ul {
      margin-top: -0.5rem;
      li {
        margin-top: 0.5rem;
        display: flex;
        justify-content: space-between;
        flex-basis: 100%;

        .value {
          display: flex;
          justify-content: flex-end;
          width: 35%;
          flex-shrink: 0;
        }
      }
    }
  }
`;
// =================AddonsEnd===========
