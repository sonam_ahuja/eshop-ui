import React from 'react';
import { Button } from 'dt-components';
import { ITariffTranslation } from '@src/common/store/types/translation';
import EVENT_NAME from '@events/constants/eventName';
import { ICharacteristicConfig } from '@tariff/store/types';

import { StyledButtonsWrap } from './styles';

export interface IProps {
  translation: ITariffTranslation;
  tariffId: string;
  planSelectionOnly: boolean;
  selectedTariffId: string;
  confirmPlanBtnLoading: boolean;
  config: ICharacteristicConfig;
  redirectToListingPage(tariffId: string): void;
  addPlanToBasket(tariffId: string): void;
}

const PlanCTA = (props: IProps) => {
  const {
    translation,
    redirectToListingPage,
    addPlanToBasket,
    tariffId,
    confirmPlanBtnLoading,
    selectedTariffId,
    config,
    planSelectionOnly
  } = props;

  return (
    <StyledButtonsWrap>
      {planSelectionOnly ? (
        <Button
          size='small'
          loading={confirmPlanBtnLoading && tariffId === selectedTariffId}
          className='btn btn-primary'
          onClickHandler={event => {
            event.stopPropagation();
            redirectToListingPage(tariffId);
          }}
          data-event-id={EVENT_NAME.TARIFF.EVENTS.CONFIRM_PLAN}
          data-event-message={translation.confirmPlan}
          type={'complementary'}
        >
          {translation.confirmPlan}
        </Button>
      ) : null}
      {!planSelectionOnly && config.showBuyButton ? (
        <>
          <Button
            className={config.showAddButton ? 'btn-line' : ''}
            size='small'
            onClickHandler={() => addPlanToBasket(tariffId)}
            data-event-id={EVENT_NAME.TARIFF.EVENTS.BUY_PLAN}
            data-event-message={translation.buyPlanOnly}
          >
            {translation.buyPlanOnly}
          </Button>
        </>
      ) : null}
      {!planSelectionOnly && config.showAddButton ? (
        <Button
          size='small'
          onClickHandler={event => {
            event.stopPropagation();
            redirectToListingPage(tariffId);
          }}
          data-event-id={EVENT_NAME.TARIFF.EVENTS.ADD_TO_PHONE}
          data-event-message={translation.addAPhone}
        >
          {translation.addAPhone}
        </Button>
      ) : null}
    </StyledButtonsWrap>
  );
};

export default PlanCTA;
