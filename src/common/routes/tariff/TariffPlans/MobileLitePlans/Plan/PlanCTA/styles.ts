import styled from 'styled-components';
import { colors } from '@src/common/variables';

// =================ButtonsWrapStart===========
export const StyledButtonsWrap = styled.div`
  padding: 1rem;
  justify-content: space-between;
  display: flex;
  border-radius: 0 0 0.5rem 0.5rem;
  margin-left: -1rem;
  margin-right: -1rem;
  margin-bottom: -1rem;
  background: ${colors.cloudGray};

  .dt_anchor {
    font-size: 0.875rem;
    font-weight: 700;
  }
  .dt_button,
  .dt_anchor {
    &:only-child {
      margin: auto;
    }
  }
  .dt_button.btn-line {
    color: ${colors.magenta};
    background: none;
  }
`;
// =================ButtonsWrapEnd===========
