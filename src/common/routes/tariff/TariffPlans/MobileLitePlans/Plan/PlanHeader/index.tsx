import React, { Component } from 'react';
import { ITariffTranslation } from '@src/common/store/types/translation';
import { ICurrencyConfiguration } from '@common/store/types/configuration';
import { ICharacteristicConfig, IPlans } from '@tariff/store/types';
import TariffPrice from '@tariff/TariffPlans/Common/TariffPrice';
import { calculateTruncation } from '@src/common/utils/calculateTruncation';

import { StyledDivider } from '../styles';

import { StyledPaymentType, StyledPlanHeader, StyledPrice, StylePlanName, } from './styles';

export interface IProps {
  translation: ITariffTranslation;
  plan: IPlans;
  config: ICharacteristicConfig;
  currency: ICurrencyConfiguration;
}

interface IState {
  planNameRef: HTMLElement | null;
}

class PlanHeader extends Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      planNameRef: null
    };
    this.setPlanNameRef = this.setPlanNameRef.bind(this);
  }

  setPlanNameRef(ref: HTMLElement | null): void {
    if (ref) {
      const innerDiv = ref.querySelector('.planName') as HTMLElement;
      if (innerDiv) { calculateTruncation(innerDiv); }
    }

  }

  render(): React.ReactNode {
    const {
      translation,
      plan,
      currency,
      config,
    } = this.props;

    return (

      <StyledPlanHeader>
        <ul>
          {config.showPlanName && (
            <li ref={this.setPlanNameRef}>
              <StylePlanName className='planName'>
                {plan.name}
              </StylePlanName>
            </li>
          )}
          {config.showPrice && (
            <li>
              <StyledPrice>
                <TariffPrice
                  totalPrices={plan.totalPrices}
                  reverseOrder={true}
                  currency={currency}
                  hideActualPriceCurrency={true}
                />
              </StyledPrice>
              <StyledPaymentType>{translation.monthly}</StyledPaymentType>
            </li>
          )}
        </ul>

        <StyledDivider />
      </StyledPlanHeader>
    );
  }
}

export default PlanHeader;
