import styled from 'styled-components';
import { Paragraph, Section, Title } from 'dt-components';
import { colors } from '@src/common/variables';

// =================HeaderStart==============
export const StylePlanName = styled(Paragraph).attrs({
  size: 'medium',
  weight: 'bold'
})`
  max-height: 3rem;
  position: relative;
  overflow: hidden;

  &.ellipsis:after {
    content: '...';
    position: absolute;
    bottom: 0;
    background: ${colors.white};
    right: 0;
  }
`;

export const StyledPrice = styled(Title).attrs({
  size: 'xsmall',
  weight: 'ultra'
})`
  color: ${colors.magenta};
  span {
    display: inline-block;
    padding-left: 0.25rem;
    sup {
      margin-left: 2px;
    }
  }
`;

export const StyledPaymentType = styled(Section).attrs({ size: 'large' })``;

export const StyledPlanHeader = styled.div`
  padding-top: 1rem;
  ul {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    height: 100%;
    li:first-child {
      text-align: left;
      padding-right: 0.25rem;
    }
    li:last-child {
      text-align: right;
      width: 35%;
      flex-shrink: 0;
    }

    .modify-text {
      display: block;
      text-decoration: line-through;
      font-size: 0.875em;
      line-height: 1;
      span {
        display: initial;
      }
    }
  }
`;
// =================HeaderEnd==============
