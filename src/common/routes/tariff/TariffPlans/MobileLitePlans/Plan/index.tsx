import React from 'react';
import { ICharacteristicConfig, IPlans } from '@tariff/store/types';
import {
  ICurrencyConfiguration,
  ITariffConfiguration
} from '@common/store/types/configuration';
import { ITariffTranslation } from '@src/common/store/types/translation';
import PlanHeader from '@tariff/TariffPlans/MobileLitePlans/Plan/PlanHeader';
import PlanAddons from '@tariff/TariffPlans/MobileLitePlans/Plan/PlanAddons';
import PlanCTA from '@tariff/TariffPlans/MobileLitePlans/Plan/PlanCTA';
import checkBestDeviceOffers from '@tariff/utils/checkBestDeviceOffers';

import { StyledRibbon, StylePlan } from './styles';

export type cardStateType = 'default' | 'active';
export interface IProps {
  planSelectionOnly: boolean;
  config: ICharacteristicConfig;
  cardState: cardStateType;
  currency: ICurrencyConfiguration;
  translation: ITariffTranslation;
  configuration: ITariffConfiguration;
  className?: string;
  selectedTariffId: string;
  plan: IPlans;
  confirmPlanBtnLoading: boolean;
  redirectToListingPage(tariffId: string): void;
  addPlanToBasket(tariffId: string): void;
  setTariffId(tariffId: string): void;
  tariffCardHandler(tariffId: string): void;
  getTariffAddonPrice(tariffId: string): void;
  toggleSelectedBenefit(
    planName: string,
    benefitName: string,
    benefitValue: string,
    templateType: string,
    newSelected: boolean
  ): void;
}

const Plan = (props: IProps) => {
  const {
    cardState,
    translation,
    config,
    plan,
    confirmPlanBtnLoading,
    currency,
    toggleSelectedBenefit,
    planSelectionOnly,
    className,
    redirectToListingPage,
    addPlanToBasket,
    tariffCardHandler,
    configuration,
    setTariffId,
    selectedTariffId,
    getTariffAddonPrice
  } = props;

  const bestOfferIconEl = checkBestDeviceOffers(plan) ? <StyledRibbon /> : null;

  return (
    <StylePlan
      cardState={cardState}
      className={className}
      onClick={() => tariffCardHandler(plan.id)}
    >
      {bestOfferIconEl}

      <PlanHeader
        plan={plan}
        currency={currency}
        translation={translation}
        config={config}
      />

      {plan && plan.allBenefits && (
        <PlanAddons
          config={config}
          toggleSelectedBenefit={toggleSelectedBenefit}
          configuration={configuration}
          setTariffId={setTariffId}
          plan={plan}
          getTariffAddonPrice={getTariffAddonPrice}
        />
      )}
      <PlanCTA
        tariffId={plan.id}
        confirmPlanBtnLoading={confirmPlanBtnLoading}
        planSelectionOnly={planSelectionOnly}
        translation={translation}
        redirectToListingPage={redirectToListingPage}
        addPlanToBasket={addPlanToBasket}
        config={config}
        selectedTariffId={selectedTariffId}
      />
    </StylePlan>
  );
};

export default Plan;
