import styled from 'styled-components';
import { colors } from '@src/common/variables';
import { Icon, Paragraph } from 'dt-components';
import { hexToRgbA } from '@src/common/utils';

import { StyledPaymentType } from './PlanHeader/styles';

const cardState = {
  default: {
    baseBoxShadow: '0 8px 16px 0 rgba(0,0,0,0.08)',
    baseColor: colors.zBlack,
    baseBorder: `1px solid ${hexToRgbA(colors.black, 0.12)}`,
    bgStartColor: colors.white,
    bgEndColor: colors.white,
    dividerColor: hexToRgbA(colors.gray, 0.7),
    paymentTypeColor: colors.gray
  },
  active: {
    baseBoxShadow: '0 11px 20px 0 rgba(0,0,0,0.13)',
    baseColor: colors.zBlack,
    baseBorder: `1px solid ${colors.magenta}`,
    bgStartColor: colors.white,
    bgEndColor: colors.white,
    dividerColor: hexToRgbA(colors.gray, 0.7),
    paymentTypeColor: colors.gray
  }
};

// =================CommonStart==============
export const StyledDivider = styled.div`
  height: 1px;
  width: 100%;
  margin-top: 1rem;
  opacity: 0.2;
`;

export const StyledRibbon = styled(Icon).attrs({ name: 'bookmark-ribbon' })`
  position: absolute;
  right: 1rem;
  top: 0;
  transform: scaleX(1.15) translateY(-35%);
  transform-origin: right;
  font-size: 1.5rem;

  svg path {
    stroke: ${colors.lightMagenta};
    stroke-width: 2px;
  }
`;
// =================CommonEnd==============

// =================AddonsStart===========
export const StyledLabel = styled(Paragraph).attrs({
  size: 'small',
  weight: 'bold'
})`
  margin-bottom: 0.15rem;
`;

export const StyledValue = styled(Paragraph).attrs({
  size: 'small',
  weight: 'ultra'
})``;

export const StylePlan = styled.div<{ cardState: string }>`
  padding: 0.25rem 1rem 1rem;
  width: 100%;
  box-shadow: ${props => cardState[props.cardState].baseBoxShadow};
  position: relative;

  transition: 0.2s ease-in 0s;

  border: ${props => cardState[props.cardState].baseBorder};

  background: linear-gradient(
    180deg,
    ${props => cardState[props.cardState].bgStartColor} 0%,
    ${props => cardState[props.cardState].bgEndColor} 100%
  );
  color: ${props => cardState[props.cardState].baseColor};

  ${StyledDivider} {
    background: ${props => cardState[props.cardState].dividerColor};
  }

  ${StyledPaymentType} {
    color: ${props => cardState[props.cardState].paymentTypeColor};
  }

  border-radius: 0.5rem;
`;
