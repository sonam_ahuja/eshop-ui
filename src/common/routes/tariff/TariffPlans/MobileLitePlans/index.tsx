import React, { Component } from 'react';
import { IPlans } from '@tariff/store/types';
import { ICategory } from '@routes/category/store/types';
import { ITariffTranslation } from '@src/common/store/types/translation';
import Plan from '@tariff/TariffPlans/MobileLitePlans/Plan';
import {
  ICurrencyConfiguration,
  ITariffConfiguration
} from '@common/store/types/configuration';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import { Anchor } from 'dt-components';
import appConstants from '@src/common/constants/appConstants';
import EVENT_NAME from '@events/constants/eventName';

import { StyledMobileLitePlans } from './styles';
export interface IMobileLitePlansProps {
  plans: IPlans[];
  planSelectionOnly: boolean;
  categories: ICategory;
  selectedTariffId: string;
  currency: ICurrencyConfiguration;
  translation: ITariffTranslation;
  configuration: ITariffConfiguration;
  confirmPlanBtnLoading: boolean;
  redirectToListingPage(tariffId: string): void;
  setTariffId(tariffId: string): void;
  addPlanToBasket(tariffId: string): void;
  saveTariff(): void;
  getTariffAddonPrice(tariffId: string): void;
  toggleSelectedBenefit(
    planName: string,
    benefitName: string,
    benefitValue: string,
    templateType: string,
    newSelected: boolean
  ): void;
  showTermAndConditions(status: boolean): void;
}
class MobileLitePlans extends Component<IMobileLitePlansProps, {}> {
  itemRefs = {};

  constructor(props: IMobileLitePlansProps) {
    super(props);

    this.tariffCardHandler = this.tariffCardHandler.bind(this);
  }

  componentDidMount(): void {
    if (
      this.props.plans.length &&
      this.itemRefs &&
      this.itemRefs[this.props.selectedTariffId]
    ) {
      this.itemRefs[this.props.selectedTariffId].scrollIntoView();
    }
  }

  tariffCardHandler(tariffId: string): void {
    this.props.setTariffId(tariffId);
    if (
      !this.props.planSelectionOnly &&
      this.props.selectedTariffId !== tariffId
    ) {
      this.props.saveTariff();
    }
  }

  render(): React.ReactNode {
    const {
      categories,
      plans,
      translation,
      selectedTariffId,
      planSelectionOnly,
      currency,
      redirectToListingPage,
      addPlanToBasket,
      configuration,
      getTariffAddonPrice,
      setTariffId,
      toggleSelectedBenefit,
      confirmPlanBtnLoading,
      showTermAndConditions
    } = this.props;
    const config = getCharactersticsConfig(categories.characteristics);

    return (
      <StyledMobileLitePlans>
        {plans && plans.length
          ? plans.map((plan: IPlans) => {
            return (
              <div
                className='planContainer'
                key={plan.id}
                ref={(el: HTMLElement | null) => {
                  if (el) {
                    this.itemRefs[plan.id] = el;
                  }
                }}
              >
                <Plan
                  key={plan.id}
                  currency={currency}
                  cardState={
                    selectedTariffId === plan.id ? 'active' : 'default'
                  }
                  confirmPlanBtnLoading={confirmPlanBtnLoading}
                  config={config}
                  configuration={configuration}
                  toggleSelectedBenefit={toggleSelectedBenefit}
                  selectedTariffId={selectedTariffId}
                  getTariffAddonPrice={getTariffAddonPrice}
                  setTariffId={setTariffId}
                  planSelectionOnly={planSelectionOnly}
                  translation={translation}
                  plan={plan}
                  redirectToListingPage={redirectToListingPage}
                  addPlanToBasket={addPlanToBasket}
                  tariffCardHandler={this.tariffCardHandler}
                />
              </div>
            );
          })
          : null}
        <div className='terms-panel'>
          <Anchor
            className='linker'
            hreflang={appConstants.LANGUAGE_CODE}
            title={translation.termsAndConditions}
            onClickHandler={() => showTermAndConditions(true)}
            data-event-id={EVENT_NAME.TARIFF.EVENTS.TERMS_CONDITION}
            data-event-message={this.props.translation.termsAndConditions}
          >
            <span>{translation.termsAndConditions}</span>
          </Anchor>
        </div>
      </StyledMobileLitePlans>
    );
  }
}

export default MobileLitePlans;
