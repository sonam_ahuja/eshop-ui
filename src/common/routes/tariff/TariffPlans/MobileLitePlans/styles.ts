import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledMobileLitePlans = styled.div`
  padding: 1rem 1.25rem 2.25rem;
  background: ${colors.coldGray};

  .planContainer {
    padding-top: 1.25rem;
    margin-top: 0.5rem;
  }
  .terms-panel {
    margin-top: 3.75rem;
    text-align: right;
    .linker {
      font-size: 0.875rem;
      line-height: 1.25rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding: 1rem 2.25rem 1.5rem;
  }

  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding: 1rem 3rem 1.5rem;
  }
`;
