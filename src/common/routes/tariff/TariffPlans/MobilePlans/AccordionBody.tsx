import { TARIFF_BENEFIT_TEMPLATES } from '@common/constants/appkeys';
import React, { Component, ReactNode } from 'react';
import { Anchor } from 'dt-components';
import { IBenefits, IPlans } from '@tariff/store/types';
import { ITariffTranslation } from '@common/store/types/translation';
import { IPlansProps } from '@tariff/TariffPlans/index';
import styled from 'styled-components';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import getTemplate from '@tariff/utils/getTemplate';
import cx from 'classnames';
import { isMobile } from '@common/utils';
import ButtonTemplate from '@tariff/TariffPlans/Plans/templates/button';
import DropdownTemplate from '@tariff/TariffPlans/Plans/templates/dropdown';
import ImageTemplate from '@tariff/TariffPlans/Plans/templates/image';
import getIconOrLabel from '@tariff/common/getIconOrLabel';
import EVENT_NAME from '@events/constants/eventName';
import appConstants from '@src/common/constants/appConstants';

import { ITariffTemplateProps } from '../../types';

const AccordionBodyWrap = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  @media (max-width: 767px) {
  }
`;

export interface IState {
  viewMore: boolean;
}
export interface IProps {
  plan: IPlans;
  tariffProps: IPlansProps;
  tariffTranslation: ITariffTranslation;
  saveTariff(): void;
  getTariffAddonPrice(tariffId: string): void;
}
// tslint:disable:no-commented-code
class AccordionBodyComponents extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      viewMore: false
    };
  }
  onClickHandler = () => {
    this.setState({
      viewMore: !this.state.viewMore
    });
  }

  // tslint:disable-next-line: cognitive-complexity
  render(): ReactNode {
    const { viewMore } = this.state;
    const { plan, getTariffAddonPrice } = this.props;
    const { id } = plan;
    const {
      tariffConfiguration,
      toggleSelectedBenefit,
      tariffTranslation,
      categories
    } = this.props.tariffProps;

    const config = getCharactersticsConfig(categories.characteristics);

    let benefitEl = config.showBenefits
      ? plan.benefits.map((benefit: IBenefits, index: number) => {
          const listClassName = cx({ lists: !!benefit.label }, 'clearfix');
          /*** benefits map starts */
          const template = getTemplate(tariffConfiguration, benefit.name);

          const templateProps = {
            tariffValues: benefit.values,
            tariffConfiguration,
            toggleBenefitSelection: (
              benefitValue: string,
              newSelected: boolean
            ) => {
              toggleSelectedBenefit(
                id,
                benefit.name,
                benefitValue,
                template.templateType,
                newSelected
              );
              // API call to fetch new prices for a tariff - for accordion body in mobile
              getTariffAddonPrice(plan.id);
            }
          };
          // to identify addons isSelectable needs to be true
          if (!config.showAddons && benefit.isSelectable) {
            return null;
          }
          // else in other cases isSelectable is false, we will simply render on ui list items
          if (!benefit.isSelectable) {
            return (
              <li key={index} className={listClassName}>
                <div className='left cols-4-xs'>
                  <span className='titles'>{benefit.label}</span>
                </div>
                <div className='right cols-6-xs'>
                  <span className='data-text'>
                    <strong>{getIconOrLabel(benefit)}</strong>
                  </span>
                </div>
              </li>
            );
          }

          return (
            <li key={index} className={listClassName}>
              <div className='left cols-4-xs'>
                <span className='titles'>{benefit.label}</span>
              </div>
              <div className='right cols-6-xs'>
                <span className='data-text'>
                  <strong>
                    {getAddonTemplate(
                      template.templateName,
                      templateProps,
                      tariffTranslation,
                      benefit
                    )}
                  </strong>
                  {/* {benefit.values[0].unit} */}
                </span>
              </div>
            </li>
          );
          /*** benefits map ends */
        })
      : [];

    if (viewMore || !config.showBenefits) {
      const commonEl = config.showUnlimitedBenefits
        ? plan.commonBenefits.map((benefit: IBenefits, index: number) => {
            const listClassName = cx({ lists: !!benefit.label }, '');

            return (
              <li key={benefitEl.length + index} className={listClassName}>
                <div className='left cols-4-xs'>
                  <span className='titles'>{benefit.label}</span>
                </div>
                <div className='right cols-6-xs'>
                  <span className='data-text'>{getIconOrLabel(benefit)}</span>
                </div>
              </li>
            );
          })
        : [];
      benefitEl = benefitEl.concat(commonEl);
    }

    return (
      <AccordionBodyWrap>
        <div className='accordion-body-wrapper'>
          <ul className='list-panel'>{benefitEl}</ul>
          {config.showBenefits &&
          config.showUnlimitedBenefits &&
          plan.commonBenefits.length ? (
            <div className='btn-wrapper clearfix'>
              <Anchor
                className='linker'
                hreflang={appConstants.LANGUAGE_CODE}
                title={
                  viewMore
                    ? tariffTranslation.viewLess
                    : tariffTranslation.viewMore
                }
                underline={false}
                data-event-id={EVENT_NAME.TARIFF.EVENTS.VIEW_ACTION_BUTTON}
                data-event-message={
                  viewMore
                    ? tariffTranslation.viewLess
                    : tariffTranslation.viewMore
                }
                data-event-path={
                  viewMore ? 'cart.tariff.viewLess' : 'cart.tariff.viewMore'
                }
                onClickHandler={this.onClickHandler}
              >
                {viewMore
                  ? tariffTranslation.viewLess
                  : tariffTranslation.viewMore}
              </Anchor>
            </div>
          ) : null}
        </div>
      </AccordionBodyWrap>
    );
  }
}

export const getAddonTemplate = (
  templateName: string,
  templateProps: ITariffTemplateProps,
  _tariffTranslation: ITariffTranslation,
  benefit: IBenefits
) => {
  switch (templateName) {
    case TARIFF_BENEFIT_TEMPLATES.BUTTON: {
      if (isMobile.phone || isMobile.tablet) {
        return <DropdownTemplate {...templateProps} useImage={false} />;
      }

      return <ButtonTemplate {...templateProps} />;
    }
    case TARIFF_BENEFIT_TEMPLATES.DROPDOWN: {
      return <DropdownTemplate {...templateProps} useImage={false} />;
    }
    case TARIFF_BENEFIT_TEMPLATES.IMAGE: {
      if (isMobile.phone || isMobile.tablet) {
        return <DropdownTemplate {...templateProps} useImage={true} />;
      }

      return <ImageTemplate {...templateProps} />;
    }
    default: {
      return getIconOrLabel(benefit);
    }
  }
};

// tslint:disable-next-line:max-file-line-count
export default AccordionBodyComponents;
