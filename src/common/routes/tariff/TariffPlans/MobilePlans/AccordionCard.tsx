import React, { Component, ReactNode } from 'react';
import { Icon } from 'dt-components';
import { IPlans } from '@tariff/store/types';
import getMobileList from '@tariff/utils/getMobileList';
import { checkBestDeviceOffers } from '@tariff/utils';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import TariffPrice from '@tariff/TariffPlans/Common/TariffPrice';
import { ITariffConfiguration } from '@src/common/store/types/configuration';
import {
  getCharactersticsConfig,
  getHDCharacterstics
} from '@tariff/utils/getCharactersticsConfig';
import { AccordionCardsWrap } from '@tariff/TariffPlans/MobilePlans/styles/AccordionCardStyles';
import { ITariffTranslation } from '@src/common/store/types/translation';

export interface IProps {
  plan: IPlans;
  planProps: IPlansProps;
  tariffTranslation: ITariffTranslation;
  tariffConfiguration: ITariffConfiguration;
  saveTariff(): void;
}
class AccordionCardsComponents extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
  }

  render(): ReactNode {
    const {
      plan,
      planProps,
      tariffConfiguration,
      tariffTranslation
    } = this.props;
    if (!plan.name) {
      return null;
    }
    const config = getCharactersticsConfig(
      planProps.categories.characteristics
    );
    let hdEnabled = null;
    if (plan.characteristics && plan.characteristics.length) {
      hdEnabled = getHDCharacterstics(plan.characteristics);
    }
    const bestOfferIconEl = checkBestDeviceOffers(plan) ? (
      <span className='icon-wrap'>
        <Icon size='inherit' color='currentColor' name='ec-solid-gift' />
      </span>
    ) : null;

    return (
      <AccordionCardsWrap>
        <div className='accordion-title-wrap'>
          <h3 className='accordionTitle'>
            {tariffConfiguration.tariffListingTemplate ===
              TARIFF_LIST_TEMPLATE.NAME_FIRST &&
              (config.showPlanName ? plan.name : '')}

            {tariffConfiguration.tariffListingTemplate ===
              TARIFF_LIST_TEMPLATE.PRICE_FIRST &&
              config.showPrice && (
                <TariffPrice
                  totalPrices={plan.totalPrices}
                  currency={planProps.currency}
                  reverseOrder={true}
                  hideActualPriceCurrency={true}
                />
              )}
            {tariffConfiguration.tariffListingTemplate ===
              TARIFF_LIST_TEMPLATE.DATA_FIRST && (
              <>
                {config.showBenefits && plan.data && plan.data.values[0].label}

                {hdEnabled &&
                hdEnabled[0] &&
                hdEnabled[0].values &&
                hdEnabled[0].values[0] &&
                hdEnabled[0].values[0].value === 'true' ? (
                  <span className='tittle-hd'>{tariffTranslation.hdData}</span>
                ) : null}
              </>
            )}
            {bestOfferIconEl}
          </h3>
          <div className='elements-wrap clearfix'>
            <ul className='list-inline'>
              {getMobileList(plan, planProps, false)}
            </ul>
          </div>
        </div>
      </AccordionCardsWrap>
    );
  }
}

export default AccordionCardsComponents;
