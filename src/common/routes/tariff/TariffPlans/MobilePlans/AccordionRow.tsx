import React, { ReactNode } from 'react';
import { Accordion } from 'dt-components';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { IPlans } from '@tariff/store/types';
import AccordionCardsComponents from '@tariff/TariffPlans/MobilePlans/AccordionCard';
import AccordionBodyComponents from '@tariff/TariffPlans/MobilePlans/AccordionBody';

interface IProps {
  className: string;
  plan: IPlans;
  planProps: IPlansProps;
  planSelected?: boolean;
  saveTariff(): void;
  getTariffAddonPrice(tariffId: string): void;
}

interface IState {
  isOpen: boolean;
}

export default class AccordionRow extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.toggleIsOpen = this.toggleIsOpen.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggleIsOpen(): void {
    this.setState(prevState => ({
      isOpen:
        !this.props.planSelected && prevState.isOpen ? true : !prevState.isOpen
    }));
  }

  render(): ReactNode {
    const {
      className,
      plan,
      planProps,
      saveTariff,
      getTariffAddonPrice
    } = this.props;

    return (
      <Accordion
        className={className}
        isOpen={this.state.isOpen}
        headerTitle={''}
        Header={() => null}
        headerContent={
          <div
            key={`plans_accordion_header_${plan.id}`}
            onClick={this.toggleIsOpen}
          >
            <AccordionCardsComponents
              plan={plan}
              planProps={planProps}
              tariffTranslation={planProps.tariffTranslation}
              tariffConfiguration={planProps.tariffConfiguration}
              saveTariff={saveTariff}
            />
          </div>
        }
      >
        <div className='panel-body clearfix'>
          <AccordionBodyComponents
            plan={plan}
            getTariffAddonPrice={getTariffAddonPrice}
            tariffProps={planProps}
            tariffTranslation={planProps.tariffTranslation}
            saveTariff={saveTariff}
          />
        </div>
      </Accordion>
    );
  }
}
