import React, { Component, ReactNode } from 'react';
import { Anchor, Button } from 'dt-components';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { IPlans } from '@tariff/store/types';
import { getLabels } from '@tariff/utils/getAllPlansTitles';
import { PlansListsWrap } from '@tariff/TariffPlans/MobilePlans/styles/PlanListStyles';
import EVENT_NAME from '@events/constants/eventName';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import appConstants from '@src/common/constants/appConstants';

import AccordionRow from './AccordionRow';

export interface IState {
  viewMore: boolean;
}

class PlansListComponent extends Component<IPlansProps, IState> {
  constructor(props: IPlansProps) {
    super(props);

    this.state = {
      viewMore: false
    };
  }
  cardClickHandler = (tariffId: string) => {
    this.props.setTariffId(tariffId);
    this.props.saveTariff();
  }

  addAPhoneClickHandler = () => {
    const { tariff, redirectToListingPage } = this.props;
    const { plans, selectedTariffId } = tariff;
    const selectedPlan = plans.filter(
      (plan: IPlans) => plan.id === selectedTariffId
    )[0];

    if (selectedPlan) {
      redirectToListingPage(selectedPlan.id);
    }
  }

  addPlanClickHandler = () => {
    const { addPlanToBasket } = this.props;
    addPlanToBasket(this.props.tariff.selectedTariffId);
  }

  renderPriceLabel = () => {
    const { tariffConfiguration, tariffTranslation, tariff } = this.props;
    const config = getCharactersticsConfig(tariff.categories.characteristics);

    return TARIFF_LIST_TEMPLATE.NAME_FIRST ===
      tariffConfiguration.tariffListingTemplate ||
      TARIFF_LIST_TEMPLATE.DATA_FIRST ===
        tariffConfiguration.tariffListingTemplate
      ? config.showPrice && (
          <li className='lists'>
            <h3 className='titles'>{tariffTranslation.pricePerMonth}</h3>
          </li>
        )
      : null;
  }

  render(): ReactNode {
    const {
      tariffTranslation,
      showTermAndConditions,
      tariffConfiguration,
      saveTariff,
      getTariffAddonPrice,
      tariff: { plans },
      categories
    } = this.props;

    const config = getCharactersticsConfig(categories.characteristics);

    /*** render Cards */
    const cardItems = plans.map((plan: IPlans) => {
      const selectedClassName = plan.isSelected
        ? 'panel-header active'
        : 'panel-header';

      return (
        <div
          key={plan.id}
          className='plans-accordion-wrapper'
          onClick={() => this.cardClickHandler(plan.id)}
        >
          <AccordionRow
            className={selectedClassName}
            planSelected={plan.isSelected}
            plan={plan}
            getTariffAddonPrice={getTariffAddonPrice}
            planProps={this.props}
            saveTariff={saveTariff}
          />
        </div>
      );
    });
    /*** render cards */
    const planLabel =
      tariffConfiguration.tariffListingTemplate ===
      TARIFF_LIST_TEMPLATE.DATA_FIRST ? (
        <li key={tariffTranslation.plan} className='lists'>
          <span className='titles' title={tariffTranslation.plan}>
            {tariffTranslation.plan}
          </span>
        </li>
      ) : null;

    return (
      <PlansListsWrap>
        <div className='header-stick' ref={this.props.setMultiplePlansWrapRef}>
          <ul className='list-inline clearfix'>
            {config.showPlanName && planLabel}
            {getLabels(this.props, this.state.viewMore)}
            {this.renderPriceLabel()}
          </ul>
        </div>
        <div
          ref={this.props.setMobileAccordionPanelRef}
          className='accordion-panel'
        >
          {cardItems}
        </div>
        <div className='btn-wrapper clearfix'>
          {config.showAddButton ? (
            <Button
              size='small'
              className={'btn btn-primary'}
              onClickHandler={() => this.addAPhoneClickHandler()}
              data-event-id={EVENT_NAME.TARIFF.EVENTS.ADD_TO_PHONE}
              data-event-message={tariffTranslation.addAPhone}
            >
              {tariffTranslation.addAPhone}
            </Button>
          ) : null}
          {config.showBuyButton ? (
            <Button
              size='small'
              className={
                config.showAddButton ? 'btn btn-line' : 'btn btn-primary'
              }
              data-event-id={EVENT_NAME.TARIFF.EVENTS.BUY_PLAN}
              data-event-message={tariffTranslation.buyPlanOnly}
              onClickHandler={() => this.addPlanClickHandler()}
            >
              {tariffTranslation.buyPlanOnly}
            </Button>
          ) : null}
          <div className='terms-panel'>
            <Anchor
              className='linker'
              hreflang={appConstants.LANGUAGE_CODE}
              title={tariffTranslation.termsAndConditions}
              onClickHandler={() => showTermAndConditions(true)}
              data-event-id={EVENT_NAME.TARIFF.EVENTS.TERMS_CONDITION}
              data-event-message={
                this.props.tariffTranslation.termsAndConditions
              }
            >
              <span>{tariffTranslation.termsAndConditions}</span>
            </Anchor>
          </div>
        </div>
      </PlansListsWrap>
    );
  }
}

export default PlansListComponent;
