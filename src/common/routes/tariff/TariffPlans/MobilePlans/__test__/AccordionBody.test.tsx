import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import AccordionBody, {
  IProps
} from '@tariff/TariffPlans/MobilePlans/AccordionBody';
import appState from '@store/states/app';
import { plan, plansProps } from '@mocks/tariff/tariff.mock';

describe('<AccordionBody />', () => {
  const props: IProps = {
    tariffTranslation: appState().translation.cart.tariff,
    tariffProps: plansProps,
    plan,
    saveTariff: jest.fn(),
    getTariffAddonPrice: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <AccordionBody {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <AccordionBody {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('AccordionBodyComponents')
      .instance() as AccordionBody).onClickHandler();
  });
});
