import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import AccordionCardsComponents from '@tariff/TariffPlans/MobilePlans/AccordionCard';
import { plan, plansProps } from '@mocks/tariff/tariff.mock';
import appState from '@store/states/app';

describe('<AccordionCardsComponents />', () => {
  const props = {
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    plan,
    tariffTranslation: appState().translation.cart.tariff,
    planProps: plansProps,
    saveTariff: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <AccordionCardsComponents {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
