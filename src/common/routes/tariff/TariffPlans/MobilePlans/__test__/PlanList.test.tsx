import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import PlansListComponent from '@tariff/TariffPlans/MobilePlans/PlanList';
import appState from '@store/states/app';
import { plansProps } from '@mocks/tariff/tariff.mock';
import { IPlansProps } from '@tariff/TariffPlans/index';

describe('<PlansListComponent />', () => {
  const props: IPlansProps = {
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    ...plansProps,
    saveTariff: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <PlansListComponent {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });

  test('action trigger', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <PlansListComponent {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    (component
      .find('PlansListComponent')
      .instance() as PlansListComponent).cardClickHandler('1234');
    (component
      .find('PlansListComponent')
      .instance() as PlansListComponent).addAPhoneClickHandler();
    (component
      .find('PlansListComponent')
      .instance() as PlansListComponent).addPlanClickHandler();
  });
});
