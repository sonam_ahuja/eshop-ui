import styled from 'styled-components';
import { colors } from '@src/common/variables';
// tslint:disable:max-file-line-count
export const AccordionCardsWrap = styled.div`
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .accordion-title-wrap {
    padding: 0.25rem;
    padding-bottom: 0;
    .accordionTitle {
      display: flex;
      align-items: center;
      justify-content: flex-start;
      margin-bottom: 0.25rem;
      .price-text-app {
        text-align: right;
        font-size: 1rem;
        line-height: 1.5rem;
        color: ${colors.magenta};
        font-weight: 900;
        display: block;
        width: auto;
        padding-right: 0.5rem;
        position: relative; /******* added for top when huge data *******/
        sup {
          vertical-align: super;
          font-size: 0.625rem;
          line-height: 0.75rem;
          font-weight: 500;
        }
      }
      .modify-text {
        font-size: 0.75rem;
        line-height: 1rem;
        text-decoration: line-through;
        letter-spacing: -0.3px;
        color: ${colors.magenta};
        text-decoration: line-through;
        display: block;
        text-align: right;
        width: 2rem;
        .value {
          color: ${colors.darkGray};
          sup {
            vertical-align: super;
            font-size: 0.625rem;
            line-height: 0.75rem;
            font-weight: 500;
          }
        }
      }
      .icon-wrap {
        text-align: right;
        margin-left: auto;
        font-size: 1rem;
        color: ${colors.magenta};
      }
      .tittle-hd {
        border-radius: 0.3rem;
        border: 1px solid ${colors.magenta};
        padding: 0 0.25rem;
        color: ${colors.magenta};
        font-size: 0.65rem;
        font-weight: 600;
        position: relative;
        left: 0.1875rem;
        min-width: 1.625rem;
        text-align: center;
      }
    }
    .icon-wrap {
      float: right;
      line-height: normal;
      font-size: 1.25rem;
    }
    .elements-wrap {
      border-radius: 6px;
      padding: 1.3125rem 0.75rem 0.6875rem;
      background: ${colors.white};
      ul {
        display: flex;
        flex-wrap: wrap;
        word-break: break-word;
        align-items: flex-start;
      }
      .lists {
        padding-right: 0.375rem;
        position: relative;
        flex-grow: 0;
        flex: 1;
        text-align: center;
        &:last-child {
          padding-right: 0;
        }
        .data-text {
          font-size: 1rem;
          line-height: 1.5rem;
          color: ${colors.stoneGray};
          font-weight: 900;
          display: block;
          strong {
            color: ${colors.stoneGray};
          }
        }
        .price-text {
          font-size: 1rem;
          line-height: 1.5rem;
          color: ${colors.magenta};
          font-weight: 900;
          text-align: left;
          display: block;
          position: relative; /******* added for top when huge data *******/
          sup {
            vertical-align: super;
            font-size: 0.625rem;
            line-height: 0.75rem;
            font-weight: 500;
          }
        }
        .select-dropdown {
          display: block;
          font-size: 1rem;
          line-height: 1.5rem;
          .dropdown {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 0;
            i {
              font-size: 0.75rem;
            }
          }
        }
        .social-icons {
          width: 1.125rem;
          margin: auto;
        }
        .modify-text {
          font-size: 0.75rem;
          line-height: 1rem;
          text-decoration: line-through;
          letter-spacing: -0.3px;
          color: ${colors.magenta};
          text-decoration: line-through;
          display: block;
          text-align: right;
          top: -0.9375rem;
          right: 0;
          position: absolute;
          .value {
            color: ${colors.darkGray};
            sup {
              vertical-align: super;
              font-size: 0.625rem;
              line-height: 0.75rem;
              font-weight: 500;
            }
          }
        }
        .price-text-app {
          text-align: right;
          font-size: 1rem;
          line-height: 1.5rem;
          color: ${colors.magenta};
          font-weight: 900;
          display: block;
          position: relative; /******* added for top when huge data *******/
          sup {
            vertical-align: super;
            font-size: 0.625rem;
            line-height: 0.75rem;
            font-weight: 500;
          }
        }
        .empty-value {
          display: flex;
          flex-shrink: 0;
          width: 100%;
          align-items: center;
          justify-content: center;
          margin-top: 0.625rem;
          &:before {
            content: '';
            width: 13px;
            height: 1px;
            background: ${colors.warmGray};
          }
        }
        /********* Worst cases scenerios *********/
        &:last-child {
          text-align: right;
          line-height: 0;
          .price-text {
            text-align: right;
          }
          .right-align {
            display: inline-block;
            position: relative;
            width: 100%;
          }
        }
        &:first-child {
          text-align: left;
          .data-text {
            text-align: left;
            justify-content: flex-start;
          }
          .select-without-border-wrap .styledSelectWrap {
            justify-content: flex-start;
          }
          .price-text-app {
            text-align: left;
            sup {
              /* right: inherit; */
            }
          }
        }
        &:nth-child(2),
        &:nth-child(3) {
          .select-without-border-wrap {
            .imgDropdownWrap {
              justify-content: center;
            }
          }
        }
        .check-icon {
          vertical-align: text-top;
        }
        /********* Worst cases scenerios *********/
      }
      .circle-list {
        display: flex;
        align-items: center;
        justify-content: center;
        li {
          border-radius: 50px;
          height: 2.1875rem;
          width: 2.1875rem;
          border: 1px solid transparent;
          display: flex;
          align-items: center;
          justify-content: flex-start;
          cursor: pointer;
          i {
            font-size: 0.9375rem;
            color: ${colors.lightGray};
          }
          &.active {
            border-color: ${colors.magenta};
            i {
              color: ${colors.black};
            }
            .social-icons {
              filter: grayscale(0%);
            }
          }
          .social-icons {
            border-radius: 50%;
            filter: grayscale(100%);
            width: 100%;
            height: 100%;
            div {
              width: 0.75rem;
              height: 0.75rem;
              margin: auto;
            }
          }
        }
      }
      .select-without-border-wrap {
        .styledSelectWrap {
          justify-content: flex-end;
          align-items: flex-start;
          .caret {
            margin-top: 0.3125rem;
            position: initial;
          }
        }
        .styledSelect {
          font-size: 1rem;
          line-height: 1.5rem;
          color: ${colors.stoneGray};
          margin-right: 0.25rem;
          font-weight: 900;
        }
        .imgDropdownWrap {
          display: flex;
          align-items: center;
          height: 1.5rem;
          .social-icons {
            margin: inherit;
            margin-right: 0.25rem;
          }
          .caret {
            font-size: 10px;
            color: ${colors.magenta};
          }
        }
      }
    }
  }
`;
