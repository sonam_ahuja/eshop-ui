import styled from 'styled-components';
import { colors } from '@src/common/variables';
// tslint:disable:max-file-line-count
export const PlansListsWrap = styled.div`
  padding: 1rem 1.25rem 2.25rem;
  background: ${colors.coldGray};
  .clearfix:after {
    content: ' '; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
  }
  .left {
    float: left !important;
  }
  .right {
    float: right !important;
    text-align: right !important;
  }
  .plans-accordion-wrapper {
    padding: 0;
    margin-bottom: 0.3125rem;
    border-radius: 8px;
    cursor: pointer;
    .panel-header {
      background: ${colors.cloudGray};
      border-radius: 8px;
      padding-bottom: 0.25rem;
      div:only-child:not(.panel-body),
      & > div:first-child:not(.panel-body) {
        border-radius: 8px;
        background: transparent;
      }
      .accordionTitle {
        font-size: 0.875rem;
        line-height: 1.25rem;
        color: ${colors.ironGray};
        padding: 0.375rem 0.75rem;
        font-weight: 500;
        word-break: break-word;
        min-height: 2rem;
        & + i {
          display: none;
        }
        .price-text-app {
          padding-right: 0rem;
        }
      }
      &:focus,
      &.active {
        outline: none;
        background: ${colors.magenta};
        box-shadow: 0 8px 40px 0 rgba(0, 0, 0, 0.2);
        .accordionTitle {
          color: ${colors.white};
          .modify-text {
            color: ${colors.white};
            .value {
              color: ${colors.white};
            }
          }
          .price-text-app {
            color: ${colors.white};
          }
          .tittle-hd {
            border-color: ${colors.white};
            color: ${colors.white};
          }
        }
        .icon-wrap {
          visibility: visible;
          color: ${colors.white};
        }
      }
      &.isOpen {
        .elements-wrap {
          display: none;
        }
      }
    }
    .panel-body {
      background: ${colors.white};
      border-radius: 6px;
      padding: 0.75rem 1rem;
      margin: 0 0.25rem;
      .btn-wrapper {
        margin-top: 0;
        flex-direction: inherit;
      }
      .titles {
        color: ${colors.mediumGray};
        font-size: 0.875rem;
        line-height: 1.25rem;
        text-align: left;
      }
      .data-text {
        font-size: 1.25rem;
        line-height: 1.5rem;
        color: ${colors.mediumGray};
        font-weight: 500;
        display: block;
        word-break: break-word;
        strong {
          color: ${colors.black};
        }
      }
      .circle-list {
        display: flex;
        align-items: center;
        justify-content: center;
        li {
          border-radius: 50px;
          height: 2.1875rem;
          width: 2.1875rem;
          border: 1px solid transparent;
          display: flex;
          align-items: center;
          justify-content: center;
          cursor: pointer;
          i {
            font-size: 0.9375rem;
            color: ${colors.lightGray};
          }
          &.active {
            border-color: ${colors.magenta};
            i {
              color: ${colors.black};
            }
            .social-icons {
              filter: grayscale(0%);
            }
          }
          .social-icons {
            border-radius: 50%;
            filter: grayscale(100%);
            width: 100%;
            height: 100%;
            div {
              width: 0.75rem;
              height: 0.75rem;
              margin: auto;
            }
          }
        }
      }
      .check-icon {
        font-size: 1.25rem;
        color: ${colors.black};
        /* margin-right: 1rem; */
      }
      .cols-4-xs {
        width: 40%;
      }
      .cols-6-xs {
        width: 60%;
        .data-text {
          font-weight: 900;
          color: ${colors.warmGray};
          word-break: break-word;
          strong {
            color: ${colors.stoneGray};
          }
        }
        .lists {
          border: 0;
          padding: 0;
          margin: 0;
          justify-content: flex-end;
          padding-top: 0px !important;
          .styledSelectWrap {
            justify-content: flex-end;
            align-items: flex-start;
            .styledSelect {
              margin-right: 0.75rem;
              &:only-child {
                margin-right: 0;
              }
            }
            .caret {
              margin-top: 0.3125rem;
            }
          }
          .imgDropdownWrap {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: flex-end;
            height: 1.5rem;
            .social-icons {
              width: 1.125rem;
              margin-right: 0.25rem;
            }
            .caret {
              color: ${colors.magenta};
            }
          }
        }
        .empty-value {
          display: flex;
          flex-shrink: 0;
          width: 100%;
          align-items: center;
          justify-content: center;
          &:before {
            content: '';
            width: 13px;
            height: 1px;
            background: ${colors.warmGray};
            margin-left: auto;
          }
        }
        .check-icon {
          color: ${colors.stoneGray};
        }
      }
      .lists {
        border-bottom: 1px solid ${colors.silverGray};
        padding-bottom: 0.95rem;
        margin-bottom: 0.95rem;
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
      .list-panel .lists:first-child {
        padding-top: 0.5rem;
      }
    }
  }
  .header-stick {
    position: relative;

    padding: 0.5rem 1rem 0.75rem;
    ul {
      display: flex;
      flex-wrap: wrap;
      li {
        width: 25%;
        flex: 1;
        text-align: center;
        &:last-child {
          text-align: right;
        }
        &:first-child {
          text-align: left;
        }
      }
    }
    .titles {
      font-size: 0.75rem;
      line-height: 1rem;
      color: ${colors.ironGray};
      letter-spacing: 0.2px;
    }
  }
  .btn-wrapper {
    display: flex;
    align-items: center;
    width: 100%;
    flex-wrap: wrap;
    margin-top: 1.75rem;
    flex-direction: row-reverse;
    .btn-primary {
      font-size: 0.875rem;
      line-height: 1.25rem;
      padding: 0.65rem 1.5rem;
      text-align: center;
      width: calc(50% - 0.5rem);
      min-height: 2.5rem;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .btn-line {
      font-size: 0.875rem;
      line-height: 1.25rem;
      padding: 0.65rem 1.5rem;
      width: calc(50% - 0.5rem);
      text-align: center;
      background: transparent;
      color: ${colors.magenta};
      min-height: 2.5rem;
      display: flex;
      align-items: center;
      justify-content: center;
      margin-right: 1rem;
    }
    .left-panel {
      float: left;
      width: 35%;
      text-align: left;
    }
    .right-panel {
      float: right;
      width: 65%;
      text-align: right;
      margin-left: 35%;
      margin-top: 1.5rem;
      .list-inline {
        float: right;
        li {
          float: left;
          margin-right: 0.25rem;
          line-height: 1.25rem;
          &:last-child {
            margin-right: 0;
          }
        }
      }
    }
  }
  .view-button {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${colors.magenta};
    font-weight: 500;
    padding: 0.65rem 0rem;
    width: 100%;
  }
  .terms-panel {
    margin-top: 3.75rem;
    text-align: right;
    width: 100%;
  }
  .linker {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: #e20074;
    margin-top: 0;
    float: right;
    cursor: pointer;
  }
`;
