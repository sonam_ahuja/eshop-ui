import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import MobileViewOnlyPlans from '@tariff/TariffPlans/MobileViewOnlyPlans';
import { plansProps } from '@mocks/tariff/tariff.mock';
import { IPlansProps } from '@tariff/TariffPlans/index';

describe('<MobileViewOnlyPlans />', () => {
  const componentWrapper = (newProps: IPlansProps) =>
    mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <MobileViewOnlyPlans {...newProps} />
        </ThemeProvider>
      </StaticRouter>
    );

  test('should render properly', () => {
    const component = componentWrapper(plansProps);
    expect(component).toMatchSnapshot();
  });

  test('should render properly, when planSelectOnly is false', () => {
    const newProps: IPlansProps = { ...plansProps, planSelectionOnly: false };
    const component = componentWrapper(newProps);
    (component
      .find('PlansListComponent')
      .instance() as MobileViewOnlyPlans).addAPhoneClickHandler();
    expect(component).toMatchSnapshot();
  });
});
