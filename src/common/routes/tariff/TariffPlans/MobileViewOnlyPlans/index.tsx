import React, { Component, ReactNode } from 'react';
import { IPlansProps } from '@tariff/TariffPlans/index';
import {
  getAllPlansTitles,
  getAllPlansValuesPlanSelectionOnly
} from '@tariff/utils';
import { IPlans } from '@tariff/store/types';
import { Button } from 'dt-components';
import { StyledMobileViewOnlyPlans } from '@tariff/TariffPlans/MobileViewOnlyPlans/styles';
import EVENT_NAME from '@events/constants/eventName';

export default class PlansListComponent extends Component<IPlansProps, {}> {
  constructor(props: IPlansProps) {
    super(props);
    this.renderBenefits = this.renderBenefits.bind(this);
    this.addAPhoneClickHandler = this.addAPhoneClickHandler.bind(this);
  }

  renderBenefits(): ReactNode {
    const { tariff } = this.props;

    return tariff.plans.map((plan: IPlans) => {
      return getAllPlansValuesPlanSelectionOnly(plan, this.props, true);
    });
  }

  addAPhoneClickHandler(): void {
    const { tariff, redirectToListingPage } = this.props;
    const { plans, selectedTariffId } = tariff;
    const selectedPlan = plans.filter(
      (plan: IPlans) => plan.id === selectedTariffId
    )[0];

    if (selectedPlan) {
      redirectToListingPage(selectedPlan.id);
    }
  }

  render(): ReactNode {
    return (
      <StyledMobileViewOnlyPlans>
        <div className='overflowWrapper'>
          <div className='overflowInnerWrap'>
            <div className='title'>
              {getAllPlansTitles(this.props, true, () => null, true)}
            </div>
            <div className='benefits'>{this.renderBenefits()}</div>
          </div>
        </div>
        <div className='btnPanel'>
          <Button
            size='medium'
            className='btn btn-primary btn-fluid'
            type={'complementary'}
            loading={this.props.confirmPlanBtnLoading}
            onClickHandler={this.addAPhoneClickHandler}
            data-event-id={EVENT_NAME.TARIFF.EVENTS.CONFIRM_PLAN}
            data-event-message={this.props.tariffTranslation.confirmPlan}
          >
            {this.props.tariffTranslation.confirmPlan}
          </Button>
        </div>
      </StyledMobileViewOnlyPlans>
    );
  }
}
