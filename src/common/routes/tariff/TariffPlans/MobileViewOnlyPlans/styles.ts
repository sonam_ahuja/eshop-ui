import styled from 'styled-components';
import { colors } from '@src/common/variables';

import { StyledSelectWrapper } from '../Plans/templates/styles';

// tslint:disable:max-file-line-count
export const StyledMobileViewOnlyPlans = styled.div`
  background: ${colors.coldGray};
  .btnPanel {
    padding: 2.5rem 1.25rem 2rem;
  }
  .btn-fluid {
    width: 100%;
  }
  .overflowWrapper {
    position: relative;
    max-width: 600px;
    margin: auto;
    padding: 0 1.25rem;
    .overflowInnerWrap {
      width: 100%;
      overflow-x: scroll;
      overflow-y: hidden;
      display: flex;
      flex-wrap: wrap;
      margin: 1.5rem 0;
      -ms-overflow-style: -ms-autohiding-scrollbar;
      scroll-behavior: smooth;
      -webkit-overflow-scrolling: touch;
      &::-webkit-scrollbar {
        display: none;
        width: 0 !important;
      }

      .title,
      .benefits {
        min-width: 100%;
        flex-shrink: 0;
      }
    }
  }
  .aside-panel {
    display: flex;
    margin-bottom: 0.75rem;
    .plans-header {
      display: flex;
      width: 6.375rem;
      align-items: center;
      justify-content: center;
      position: sticky;
      left: 0;
      background: ${colors.coldGray};
      z-index: 1;
      .title-wrap {
        width: 100%;
        h3 {
          font-size: 0.75rem;
          line-height: 1rem;
          color: ${colors.mediumGray};
          letter-spacing: 0.24px;
          text-align: center;
        }
      }
      &:only-child {
        flex-grow: 1;
        border-radius: 0.5rem;
      }
    }
    .plans-box {
      display: flex;
      flex-direction: row;
      height: 100%;
      li {
        display: flex;
        width: 6.1875rem;
        align-items: center;
        justify-content: center;
        background: ${colors.transparent};
        flex: 1;
        .titles {
          font-size: 0.75rem;
          line-height: 1rem;
          color: ${colors.mediumGray};
          letter-spacing: 0.24px;
          width: 100%;
          text-align: center;
        }
      }
    }
    .plans-body-wrap {
      flex-grow: 1;
      .plans-box {
        li {
          &:last-child {
            justify-content: flex-end;
            padding-right: 0.5rem;
            .titles {
              text-align: right;
            }
          }
        }
      }
    }
  }
  .flex-panel {
    display: flex;
    margin-bottom: 0.25rem;
    border: 1px solid ${colors.transparent};
    border-radius: 0.5rem;
    cursor: pointer;
    .plans-box {
      display: flex;
      flex-direction: row;
      height: 100%;
      li {
        display: flex;
        width: 6.1875rem;
        align-items: center;
        justify-content: center;
        min-height: 5.75rem;
        background: ${colors.white};
        padding: 1rem 0;
        flex: 1;
        .social-icons {
          width: 1.125rem;
          height: 1.125rem;
        }
        .data-text {
          color: ${colors.darkGray};
          font-weight: 500;
          text-align: center;
          word-break: break-word;
          /** psr code added here ***/
          &.empty-value {
            display: flex;
            flex-shrink: 0;
            width: 100%;
            align-items: center;
            justify-content: center;
            &:before {
              content: '';
              width: 13px;
              height: 1px;
              background: ${colors.warmGray};
            }
          }
          .check-icon {
            color: ${colors.darkGray};
          }
          /*** psr code added here ***/
        }
        .data-text,
        ${StyledSelectWrapper} {
          display: flex;
          width: 100%;
          align-items: center;
          justify-content: center;
          border-right: 1px solid ${colors.silverGray};
          /* padding: 0 1.6875rem; */
          flex-wrap: wrap;
          height: 100%;
          line-height: normal;
          padding: 0 0.5rem;
          flex-grow: 0;
          flex-shrink: 0;
        }
        &:last-child {
          border-top-right-radius: 0.5rem;
          border-bottom-right-radius: 0.5rem;

          .data-text,
          ${StyledSelectWrapper} {
            border-right: 0;
            justify-content: flex-end;
          }
        }
        .select-without-border-wrap {
          width: 100%;
          /*** psr code added here ***/
          .imgDropdownWrap {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            .social-icons {
              width: 1.125rem;
              margin-right: 0.25rem;
            }
            .caret {
              color: ${colors.magenta};
            }
          }
          /*** psr code added here ***/
          .styledSelect {
            /* margin-right: 0.375rem; */
            color: ${colors.darkGray};
            font-weight: 500;
            margin-right: 1rem;
            word-break: break-word;
            text-align: center;
          }
          .styledSelectWrap {
            justify-content: flex-end;
          }
        }
      }
    }
    .plans-header {
      display: flex;
      width: 6.375rem;
      align-items: center;
      justify-content: center;
      min-height: 5.75rem;
      position: sticky;
      left: 0;
      background: ${colors.cloudGray};
      z-index: 1;
      border-top-left-radius: 0.5rem;
      border-bottom-left-radius: 0.5rem;
      padding: 1rem 0.5rem;
      &:only-child {
        flex-grow: 1;
        border-radius: 0.5rem;
      }
      .title-wrap {
        text-align: center;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column-reverse;
        /*** psr code added here ***/
        width: 100%;
        /*** psr code added here ***/
        &.data-first-wrap {
          flex-direction: column;
          .price-text {
            font-size: 1.25rem;
            line-height: 1.5rem;
            color: ${colors.magenta};
            font-weight: 900;
          }
          .titles {
            font-size: 0.875rem;
            line-height: 1.25rem;
            color: ${colors.black};
            font-weight: 500;
          }
          .data-first-plan-name {
            color: ${colors.ironGray};
            font-size: 0.875rem;
            line-height: 1.25rem;
            font-weight: 500;
          }
          .tittle-hd {
            border-radius: 0.3rem;
            border: 1px solid ${colors.magenta};
            padding: 0.125rem 0.25rem;
            color: ${colors.magenta};
            font-size: 0.65rem;
            font-weight: 600;
            position: relative;
            left: 0.1875rem;
            top: -0.1875rem;
            min-width: 1.625rem;
            text-align: center;
          }
        }
        &.price-first-wrap {
          flex-direction: column;
          .titles {
            color: ${colors.ironGray};
            font-size: 0.875rem;
            line-height: 1.25rem;
            font-weight: 500;
          }
          .price-text-app {
            color: ${colors.magenta};
            font-size: 1.25rem;
            line-height: 1.5rem;
            font-weight: 900;
          }
        }
        &.name-first-wrap {
          flex-direction: column;
          .price-text {
            color: ${colors.magenta};
            font-size: 1.25rem;
            line-height: 1.5rem;
            font-weight: 900;
          }
          .titles {
            color: ${colors.black};
            font-size: 0.875rem;
            line-height: 1.25rem;
            font-weight: 500;
          }
        }
      }
      .price-text {
        font-size: 0.875rem;
        line-height: 1.25rem;
        color: ${colors.ironGray};
        display: block;
        font-weight: 500;
        /*** psr code added here ***/
        width: 100%;
        /* text-overflow: ellipsis;
        overflow: hidden; */
        /*** psr code added here ***/
        word-break: break-word;
      }
      .titles {
        font-size: 1.25rem;
        line-height: 1.5rem;
        color: ${colors.magenta};
        font-weight: 900;
        /** psr code added here **/
        width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
        /** psr code added here **/
        word-break: break-word;
        sup {
          vertical-align: super;
          font-size: 0.625rem;
          line-height: 0.75rem;
          font-weight: 500;
        }
      }
      .modify-text {
        font-size: 0.75rem;
        line-height: 1rem;
        text-decoration: line-through;
        letter-spacing: -0.3px;
        color: ${colors.magenta};
        text-decoration: line-through;
        /* text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden; */
        display: block;
        width: 100%;
        word-break: break-word;
        .value {
          color: ${colors.darkGray};
        }
      }
      .icon-wrap {
        position: absolute;
        top: 0.75rem;
        left: 0.75rem;
        visibility: visible;
        i {
          font-size: 1em;
        }
      }
    }
    .btn-wrapper {
      display: none;
    }
    &:last-child {
      margin-bottom: 0;
    }
    &.active {
      border: 1px solid ${colors.magenta};
      background: ${colors.magenta};
      .plans-header {
        background: ${colors.magenta};
        .titles,
        .data-text,
        .price-text,
        .dt_icon {
          color: ${colors.white};
        }
        .dt_icon {
          svg {
            path {
              fill: ${colors.white};
            }
          }
        }
        .modify-text {
          .value {
            color: ${colors.white};
          }
        }
        .title-wrap {
          &.data-first-wrap {
            .data-first-plan-name,
            .price-text,
            .titles {
              color: ${colors.white};
            }
            .tittle-hd {
              border-color: ${colors.white};
              color: ${colors.white};
            }
          }
          &.price-first-wrap {
            .titles,
            .price-text-app {
              color: ${colors.white};
            }
          }
          &.name-first-wrap {
            .price-text,
            .titles {
              color: ${colors.white};
            }
          }
        }
      }
      .plans-body-wrap {
        .lists {
          .titles,
          .price-text {
            justify-content: flex-end;
          }
        }
      }
    }
    .plans-body-wrap {
      border-radius: 0.5rem;
      flex: 1;
    }
  }
`;
