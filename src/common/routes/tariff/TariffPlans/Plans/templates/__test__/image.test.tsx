import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import Image from '@tariff/TariffPlans/Plans/templates/image';
import appState from '@store/states/app';
import { ITariffTemplateProps } from '@tariff/types';

describe('<Image />', () => {
  const props: ITariffTemplateProps = {
    tariffValues: [
      {
        unit: 'string',
        value: 'string',
        label: 'string',
        totalPrices: [],
        prices: [],
        isSelected: true,
        attachments: {
          thumbnail: [
            {
              url: 'http://localhost:3000/',
              name: 'name',
              type: 'type'
            }
          ]
        }
      }
    ],
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    toggleBenefitSelection: jest.fn()
  };
  test('should render properly', () => {
    const component = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Image {...props} />
        </ThemeProvider>
      </StaticRouter>
    );
    expect(component).toMatchSnapshot();
  });
});
