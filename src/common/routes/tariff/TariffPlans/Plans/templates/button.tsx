import React from 'react';
import { FilterChips } from 'dt-components';
import { ITariffTemplateProps } from '@tariff/types';
import EVENT_NAME from '@events/constants/eventName';

export default (props: ITariffTemplateProps) => {
  const { tariffValues, toggleBenefitSelection } = props;

  const tariffValuesArr = tariffValues.map((benefitItem, index) => {
    const { value, label, isSelected } = benefitItem;

    return (
      <FilterChips
        cancelable={false}
        onClick={() => toggleBenefitSelection(value, !isSelected)}
        label={label as string}
        key={index}
        active={!!isSelected}
        className='filter-chips'
        data-event-id={EVENT_NAME.TARIFF.EVENTS.ADD_ON_CLICK}
        data-event-message={value}
      />
    );
  });

  return (
    <li className='lists'>
      {props.label !== undefined ? (
        <span className='titles'>{props.label}</span>
      ) : null}
      <ul className='list-inline rectangle-list'>
        <li
          className='rect-list-value'
          // onClick={event => event.stopPropagation()}
        >
          {tariffValuesArr}
        </li>
      </ul>
    </li>
  );
};
