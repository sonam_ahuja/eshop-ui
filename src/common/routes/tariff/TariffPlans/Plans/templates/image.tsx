import React from 'react';
import {
  IBenefitsAttachments,
  ITariffValues,
  IThumbnail
} from '@tariff/store/types';
import { ITariffTemplateProps } from '@tariff/types';
import appkeys from '@common/constants/appkeys';
import PreloadImage from '@src/common/components/Preload';
import { IMAGE_TYPE } from '@common/store/enums';
import EVENT_NAME from '@events/constants/eventName';

export const getFrontImage = (attachments: IBenefitsAttachments) => {
  const imageObj = attachments
    ? (attachments.thumbnail || []).find((thumbnailItem: IThumbnail) => {
        return thumbnailItem.name === appkeys.PRODUCT_IMAGE_FRONT;
      })
    : null;

  return imageObj ? imageObj.url : '';
};
export default (props: ITariffTemplateProps) => {
  const { tariffValues, toggleBenefitSelection, label } = props;

  const tariffValuesArr = tariffValues.map(
    (benefitItem: ITariffValues, index: number) => {
      const { attachments, isSelected, value } = benefitItem;

      return (
        <li key={index} className={`${!!isSelected ? 'active' : ''}`}>
          <div
            className='social-icons'
            onClick={event => {
              event.stopPropagation();
              toggleBenefitSelection(value, !isSelected);
            }}
            data-event-id={EVENT_NAME.TARIFF.EVENTS.ADD_ON_CLICK}
            data-event-message={value}
          >
            <PreloadImage
              isObserveOnScroll={false}
              imageHeight={20}
              width={20}
              mobileWidth={20}
              type={IMAGE_TYPE.WEBP}
              imageWidth={20}
              imageUrl={getFrontImage(attachments)}
              disablePreloadEffect={true}
              intersectionObserverOption={{
                root: null,
                rootMargin: '60px',
                threshold: 0
              }}
            />
          </div>
        </li>
      );
    }
  );

  // return <li className='lists'>{tariffValuesArr}</li>;
  return (
    <li className='lists'>
      {label !== undefined ? <span className='titles'>{label}</span> : null}
      <ul className='ordered-list list-inline circle-list clearfix'>
        {tariffValuesArr}
      </ul>
    </li>
  );
};
