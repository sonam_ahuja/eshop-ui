import styled from 'styled-components';

export const StyledSelectWrapper = styled.div`
  width: 100%;
  /* height: inherit; */

  .select-without-border-wrap {
    height: inherit;
    .styledSelectWrap {
      justify-content: center;
      height: inherit;

      .caret {
        position: absolute;
        right: 0;
      }
    }
  }
`;
