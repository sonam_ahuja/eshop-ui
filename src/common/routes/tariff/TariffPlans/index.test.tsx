import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { plansProps } from '@mocks/tariff/tariff.mock';
import TariffPlans, { IPlansProps } from '@tariff/TariffPlans';

describe('<TariffPlans />', () => {
  const props: IPlansProps = {
    ...plansProps,
    planSelectionOnly: true,
    redirectToListingPage: jest.fn(),
    addPlanToBasket: jest.fn(),
    setMultiplePlansWrapRef: jest.fn(),
    setMobileAccordionPanelRef: jest.fn(),
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <TariffPlans {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });
});
