import React, { Component, ReactNode } from 'react';
import { IProps } from '@tariff/index';
import SinglePlan from '@src/common/routes/tariff/TariffPlans/DesktopPlans/SinglePlan';
import MultiplePlans from '@src/common/routes/tariff/TariffPlans/DesktopPlans/MultiplePlans';
import PlanList from '@tariff/TariffPlans/MobilePlans/PlanList';
import MobileLitePlans from '@tariff/TariffPlans/MobileLitePlans';
import { isMobile } from '@src/common/utils';
import MobileViewOnlyPlans from '@tariff/TariffPlans/MobileViewOnlyPlans';

export interface IPlansProps extends IProps {
  planSelectionOnly: boolean;
  redirectToListingPage(tariffId: string): void;
  addPlanToBasket(planName: string): void;
  setMultiplePlansWrapRef(ref: HTMLElement | null): void;
  setMobileAccordionPanelRef(ref: HTMLElement | null): void;
}
class TariffPlans extends Component<IPlansProps, {}> {
  constructor(props: IPlansProps) {
    super(props);
  }

  renderDesktopTariffPlan = (): ReactNode => {
    const { tariff } = this.props;

    switch (tariff.plans.length) {
      case 0:
        return null;
      case 1:
        return <SinglePlan {...this.props} />;
      default:
        return <MultiplePlans {...this.props} />;
    }
  }
  renderMobileTariffPlan = (): ReactNode => {
    return (
      <>
        {this.props.tariffConfiguration.mobileNewTariffTemplateEnabled ? (
          <MobileLitePlans
            selectedTariffId={this.props.tariff.selectedTariffId}
            translation={this.props.tariffTranslation}
            toggleSelectedBenefit={this.props.toggleSelectedBenefit}
            configuration={this.props.tariffConfiguration}
            getTariffAddonPrice={this.props.getTariffAddonPrice}
            saveTariff={this.props.saveTariff}
            currency={this.props.currency}
            setTariffId={this.props.setTariffId}
            planSelectionOnly={this.props.planSelectionOnly}
            plans={this.props.tariff.plans}
            confirmPlanBtnLoading={this.props.confirmPlanBtnLoading}
            categories={this.props.categories}
            addPlanToBasket={this.props.addPlanToBasket}
            redirectToListingPage={this.props.redirectToListingPage}
            showTermAndConditions={this.props.showTermAndConditions}
          />
        ) : (
          <PlanList {...this.props} />
        )}{' '}
      </>
    );
  }

  render(): React.ReactNode {
    if (isMobile.phone || isMobile.tablet) {
      if (
        this.props.planSelectionOnly &&
        !this.props.tariffConfiguration.mobileNewTariffTemplateEnabled
      ) {
        return <MobileViewOnlyPlans {...this.props} />;
      } else {
        return this.renderMobileTariffPlan();
      }
    } else {
      return this.renderDesktopTariffPlan();
    }
  }
}
export default TariffPlans;
