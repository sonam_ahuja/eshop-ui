import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import Header, { IProps } from '@tariff/TnC/Header';

describe('<Header />', () => {
  const props: IProps = {
    translation: appState().translation.cart.tariff
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Header {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });
});
