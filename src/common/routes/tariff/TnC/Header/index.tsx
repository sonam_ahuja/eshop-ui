import React from 'react';
import { Paragraph, Title } from 'dt-components';
import { ITariffTranslation } from '@common/store/types/translation';
import { StyledTermsHeader } from '@tariff/TnC/Header/styles';

export interface IProps {
  translation: ITariffTranslation;
}
const TermsHeader = (props: IProps) => {
  const { translation } = props;

  return (
    <StyledTermsHeader>
      <div className='terms-header'>
        <Title size='xlarge' weight='ultra'>
          {translation.termsAndConditionsHeaderText}
        </Title>

        <Paragraph size='small' weight='normal'>
          {translation.termsAndConditionsDescriptions}
        </Paragraph>
      </div>
    </StyledTermsHeader>
  );
};

export default TermsHeader;
