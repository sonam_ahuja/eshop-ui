import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledTermsHeader = styled.div`
  .terms-header {
    max-width: 28.35rem;
    margin: 0;
    padding-right: 3rem;
  }
  .dt_title {
    line-height: 2.75rem;
    letter-spacing: -0.25px;
    color: ${colors.darkGray};
    margin-bottom: 0.75rem;
    word-break: break-word;
  }
  .dt_paragraph {
    color: ${colors.ironGray};
    font-size: 0.875rem;
    line-height: 1.25rem;
    word-break: break-word;
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .terms-header {
      margin: 3rem 0 0;
      padding-right: 0;
    }
  }
`;
