import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import TnC from '@tariff/TnC';
import { IProps } from '@tariff/index';

describe('<TnC />', () => {
  const props: IProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    getTariffAddonPrice: jest.fn(),
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    tariff: appState().tariff,
    tariffTranslation: appState().translation.cart.tariff,
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    selectedProductOfferingTerm: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    globalTranslation: appState().translation.cart.global,
    categories: appState().categories.categories,
    selectedProductOfferingBenefit: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    showTermAndConditions: jest.fn(),
    setTariffId: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <TnC {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<TnC>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <TnC {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component.find('TnC').instance() as TnC).onRouteChange('string');
    (component.find('TnC').instance() as TnC).showTermAndConditions();
  });
});
