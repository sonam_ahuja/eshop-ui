import React, { Component, ReactNode } from 'react';
import { IProps } from '@tariff/index';
import htmlKeys from '@common/constants/appkeys';
import { getCharacterstic } from '@category/utils/helper';
import HTMLTemplate from '@common/components/HtmlTemplate';
import { sendPopupCloseEvent } from '@events/common/index';

import { DialogBoxTnc } from './styles';
import TermsHeader from './Header/index';
export default class TnC extends Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);
    this.onRouteChange = this.onRouteChange.bind(this);
  }

  onRouteChange(route: string): void {
    this.props.history.push(route);
  }
  showTermAndConditions = () => {
    sendPopupCloseEvent();
    this.props.showTermAndConditions(false);
    document.body.style.overflow = null;
  }

  render(): ReactNode {
    const { tariff, tariffTranslation } = this.props;
    const termAndConditionHtml = getCharacterstic(
      tariff.categories.characteristics,
      htmlKeys.TARIFF_TERM_AND_CONDITION
    );

    return (
      <DialogBoxTnc
        isOpen={tariff.isTnCModalOpen}
        showCloseButton={true}
        type='fullHeight'
        closeOnEscape={true}
        className=''
        onClose={this.showTermAndConditions}
        onBackdropClick={this.showTermAndConditions}
        closeOnBackdropClick={true}
        onEscape={this.showTermAndConditions}
      >
        <div className='tncWrapper'>
          <div className='tncHeader'>
            <TermsHeader translation={tariffTranslation} />
          </div>
          <div className='tncBody'>
            <div className='terms-accordion-wrap'>
              <HTMLTemplate
                onRouteChange={this.onRouteChange}
                template={termAndConditionHtml}
                templateData={{}}
              />
            </div>
          </div>
        </div>
      </DialogBoxTnc>
    );
  }
}
