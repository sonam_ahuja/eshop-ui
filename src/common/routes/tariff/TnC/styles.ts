import styled from 'styled-components';
import { DialogBox } from 'dt-components';
import { breakpoints, colors } from '@src/common/variables';

export const DialogBoxTnc = styled(DialogBox)`
  .dialogBoxContentWrap {
    padding: 0;
    height: 100vh;
    padding-bottom: 1rem;
    .closeIcon {
      z-index: 10;
    }
    .dialogBoxContent {
      width: 100%;
      height: 100%;
      padding: 2rem 1.25rem 10rem;
      overflow-y: auto;
      -webkit-overflow-scrolling: touch;
      scroll-behavior: smooth;

      .terms-accordion-wrap {
        margin: 3rem 0 0 0rem;
      }
    }
  }
  .dt_overlay .dt_outsideClick .dialogBoxContentWrap {
    background: ${colors.white};
  }
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    .dialogBoxContentWrap {
      width: 58.5rem;
      margin: 4.5rem 0;
      height: 80vh;
      padding: 2rem 2rem 2rem 5rem;

      .dialogBoxContent {
        padding: 0;
        .terms-accordion-wrap {
          margin: 3rem 3rem 0 8.6rem;
        }
      }
    }
    .dt_overlay .dt_outsideClick .dialogBoxContentWrap {
      background: ${colors.coldGray};
    }
  }
`;
