import React, { ReactNode } from 'react';
import { IBenefits } from '@tariff/store/types';
import { Icon } from 'dt-components';
import appConstants from '@common/constants/appConstants';
import store from '@common/store';

const getIconOrLabel = (benefit: IBenefits): ReactNode => {
  const { value, label: labelValue } = benefit.values[0];
  const tariffTranslation = store.getState().translation.cart.tariff;

  const label =
    labelValue === appConstants.UNLIMITED_TARIFF
      ? tariffTranslation.unlimited
      : labelValue;

  const showRightIcon =
    label === 'true' || value === 'true' ? (
      <Icon
        size='inherit'
        color='currentColor'
        name='ec-solid-confirm'
        className='check-icon'
      />
    ) : null;

  const showEmptyIcon =
    label === 'false' || value === 'false' ? (
      <span className='data-text empty-value' />
    ) : null;

  const showLabel = !(showRightIcon || showEmptyIcon) ? label : null;

  return (
    <>
      {showRightIcon}
      {showEmptyIcon}
      {
        <>
          <h4>{showLabel}</h4>
        </>
      }
    </>
  );
};

export default getIconOrLabel;
