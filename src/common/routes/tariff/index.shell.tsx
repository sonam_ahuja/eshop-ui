import React from 'react';
import styled from 'styled-components';
import StyledShell from '@common/components/ShellWrap';
import { breakpoints, colors } from '@src/common/variables';

// Data Breadcrumb

const DataBreadcrumb = styled.div`
  padding: 1.5rem 1.25rem;
  .breadCrumb {
    height: 1rem;
    width: 8.2rem;
    background: ${colors.cloudGray};
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 2rem 4.9rem 3rem;
  }
`;

// Data Title

const DataTitle = styled.div`
  margin-bottom: 0.5rem;
  .title {
    height: 2rem;
    background: ${colors.cloudGray};
  }
`;

// Data Paragraph

const DataParagraph = styled.div`
  margin-bottom: 1rem;
  .paragraph {
    height: 3rem;
    background: ${colors.cloudGray};
  }
  @media (min-width: ${breakpoints.desktop}px) {
    margin-bottom: 0rem;
  }
`;

// Data Dropdown

const DataDropdown = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  .dropdown {
    height: 2rem;
    width: 50%;
    background: ${colors.cloudGray};
    margin-right: 0.25rem;
    &:last-child {
      margin-right: 0;
    }
    @media (min-width: ${breakpoints.desktop}px) {
      width: 8.45rem;
    }
  }
`;

// Data Dropdown

const DataLink = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  align-items: flex-end;
  .link {
    height: 1.5rem;
    width: 7rem;
    background: ${colors.cloudGray};
  }
`;

// Data Panel

const DataPanel = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1.5rem 1.25rem 0;
  @media (min-width: ${breakpoints.desktop}px) {
    padding: 0rem 4.9rem;
    flex-direction: row;
  }
`;

// Data Left Panel

const DataLeftPanel = styled.div`
  width: 100%;
  ${DataLink} {
    display: flex;
    justify-content: flex-start;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: 40%;
    ${DataLink} {
      display: none;
    }
  }
`;

// Data Right Panel

const DataRightPanel = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: 2.25rem;
  ${DataLink} {
    display: none;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: 60%;
    margin-top: 0;
    ${DataLink} {
      display: flex;
    }
  }
`;

// Data Wrapper
const DataWrapper = styled.div`
  padding: 1.5rem 1.25rem;

  @media (min-width: ${breakpoints.desktop}px) {
    padding: 3rem 4.9rem 5rem;
    display: flex;
  }
`;
const DataHeader = styled.ul`
  display: none;
  margin-bottom: 0.75rem;

  li {
    flex: 1;
    margin: 0 0.5rem;
    height: 1rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: column;
    margin-right: 0.75rem;
    margin-bottom: 0;
    justify-content: space-around;
    display: flex;
    li {
      width: 7rem;
      height: 1rem;
      margin: 2.25rem 0;
      flex: inherit;
    }
  }
`;
const DataList = styled.ul`
  display: flex;
  flex-direction: column;

  li {
    margin-bottom: 0.25rem;
    height: 10rem;
  }

  @media (min-width: ${breakpoints.desktop}px) {
    flex-direction: row;
    width: 100%;

    li {
      margin-bottom: 0;
      margin-left: 0.5rem;
      height: auto;
      flex: 1;
    }
  }
`;

// Main Wrapper
const StyledTariffShell = styled.div`
  margin-top: 0;
  min-height: 100vh;
  background: ${colors.coldGray};
  width: 100%;
  @media (min-width: ${breakpoints.tabletPortrait}px) {
    min-height: auto;
  }
`;

const TariffShell = () => {
  return (
    <StyledTariffShell>
      <StyledShell className='primary'>
        <DataBreadcrumb>
          <div className='shine breadCrumb' />
        </DataBreadcrumb>
        <DataPanel>
          <DataLeftPanel>
            <DataTitle>
              <div className='shine title' />
            </DataTitle>
            <DataParagraph>
              <div className='shine paragraph' />
            </DataParagraph>
            <DataLink>
              <div className='shine link' />
            </DataLink>
          </DataLeftPanel>
          <DataRightPanel>
            <DataDropdown>
              <div className='shine dropdown' />
              <div className='shine dropdown' />
            </DataDropdown>
            <DataLink>
              <div className='shine link' />
            </DataLink>
          </DataRightPanel>
        </DataPanel>
        <DataWrapper>
          <DataHeader>
            <li className='shine' />
            <li className='shine' />
            <li className='shine' />
            <li className='shine' />
          </DataHeader>
          <DataList>
            <li className='shine' />
            <li className='shine' />
            <li className='shine' />
            <li className='shine' />
          </DataList>
        </DataWrapper>
      </StyledShell>
    </StyledTariffShell>
  );
};
export default TariffShell;
