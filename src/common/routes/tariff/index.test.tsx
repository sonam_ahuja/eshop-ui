import { ThemeProvider } from 'dt-components';
import { RootState } from '@src/common/store/reducers';
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';
import React from 'react';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router-dom';
import appState from '@store/states/app';
import { histroyParams } from '@mocks/common/histroy';
import { mapDispatchToProps as externalMapDispatchToProps } from '@src/common/routes/tariff/mapProps';

import { mapDispatchToProps, mapStateToProps } from './mapProps';
import { IProps, Tariff } from '.';
import AppShell from './index.shell';

describe('<Tariff />', () => {
  const props: IProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    commonError: {
      code: '',
      retryable: false
    },
    tariff: appState().tariff,
    tariffTranslation: appState().translation.cart.tariff,
    tariffConfiguration: appState().configuration.cms_configuration.modules
      .tariff,
    selectedProductOfferingTerm: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    globalTranslation: appState().translation.cart.global,
    categories: appState().categories.categories,
    selectedProductOfferingBenefit: {
      label: 'label',
      value: 'value',
      isSelected: true
    },
    fetchTariff: jest.fn(),
    fetchCategories: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    getTariffAddonPrice: jest.fn(),
    showTermAndConditions: jest.fn(),
    setTariffId: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn()
  };
  const mockStore = configureStore();
  const initStateValue: RootState = appState();
  window.scrollTo = jest.fn();
  test('should render properly', () => {
    const store = mockStore(initStateValue);
    const wapper = mount(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Tariff {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    expect(wapper).toMatchSnapshot();
  });

  test('should render app shell properly', () => {
    const appShellProps = {};

    const component = shallow(
      <ThemeProvider theme={{}}>
        <AppShell {...appShellProps} />
      </ThemeProvider>
    );
    expect(component).toMatchSnapshot();
  });

  test('handle trigger', () => {
    const store = mockStore(initStateValue);
    const component = mount<Tariff>(
      <StaticRouter context={{}}>
        <ThemeProvider theme={{}}>
          <Provider store={store}>
            <Tariff {...props} />
          </Provider>
        </ThemeProvider>
      </StaticRouter>
    );
    // tslint:disable-next-line: no-object-literal-type-assertion
    (component.find('Tariff').instance() as Tariff).onRouteChange('string');
    (component.find('Tariff').instance() as Tariff).onScroll();
    (component.find('Tariff').instance() as Tariff).setMultiplePlansWrapRef(
      null
    );
    (component.find('Tariff').instance() as Tariff).setMobileAccordionPanelRef(
      null
    );
    (component.find('Tariff').instance() as Tariff).closeModal();
    (component.find('Tariff').instance() as Tariff).addPlanToBasket('id');
    (component.find('Tariff').instance() as Tariff).checkAgeLimit('id');
    (component.find('Tariff').instance() as Tariff).setPlansStickyRef(null);
    (component.find('Tariff').instance() as Tariff).componentWillUnmount();
  });

  test('mapStateToProps test', () => {
    expect(mapStateToProps(initStateValue)).toBeDefined();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();
    externalMapDispatchToProps(dispatch).fetchTariff(
      'productOfferingTerm',
      'productOfferingBenefit',
      true
    );

    externalMapDispatchToProps(dispatch).setLoyalty({
      label: 'label',
      value: 'value',
      isSelected: true
    });
    externalMapDispatchToProps(dispatch).setDiscount({
      label: 'label',
      value: 'value',
      isSelected: true
    });

    externalMapDispatchToProps(dispatch).showMagentaDiscount(true);
    externalMapDispatchToProps(dispatch).showAppShell(true);
    externalMapDispatchToProps(dispatch).showTermAndConditions(true);
    externalMapDispatchToProps(dispatch).setTariffId('true');
    externalMapDispatchToProps(dispatch).buyPlanOnly();
    externalMapDispatchToProps(dispatch).saveTariff();
    externalMapDispatchToProps(dispatch).toggleSelectedBenefit(
      'planName',
      'benefitName',
      'benefitValue',
      'templateType',
      true
    );
    externalMapDispatchToProps(dispatch).resetPlansData();
    externalMapDispatchToProps(dispatch).validateAgeLimit('string');
    externalMapDispatchToProps(dispatch).youngTariffCheck('string');
    externalMapDispatchToProps(dispatch).clickOnAddToBasket(true);
    externalMapDispatchToProps(dispatch).clickOnBuyPlanOnly(true);
    externalMapDispatchToProps(dispatch).ageLimitSuccess();
  });

  test('mapDispatchToProps action test', () => {
    const dispatch = jest.fn();

    mapDispatchToProps(dispatch).fetchTariff(
      'productOfferingTerm',
      'productOfferingBenefit',
      true
    );
    mapDispatchToProps(dispatch).fetchCategories('Devices-Mobiles', 34);
    mapDispatchToProps(dispatch).setLoyalty({
      label: 'label',
      value: 'value',
      isSelected: true
    });
    mapDispatchToProps(dispatch).setDiscount({
      label: 'label',
      value: 'value',
      isSelected: true
    });
    mapDispatchToProps(dispatch).showMagentaDiscount(true);
    mapDispatchToProps(dispatch).showAppShell(true);
    mapDispatchToProps(dispatch).showTermAndConditions(true);
    mapDispatchToProps(dispatch).setTariffId('Tariff');
    mapDispatchToProps(dispatch).buyPlanOnly();
    mapDispatchToProps(dispatch).saveTariff();
    mapDispatchToProps(dispatch).toggleSelectedBenefit(
      'planName',
      'benefitName',
      'benefitValue',
      'templateType',
      true
    );
    mapDispatchToProps(dispatch).setCategoryId('string');
    mapDispatchToProps(dispatch).resetPlansData();
    mapDispatchToProps(dispatch).validateAgeLimit('string');
    mapDispatchToProps(dispatch).youngTariffCheck('string');
    mapDispatchToProps(dispatch).clickOnAddToBasket(true);
    mapDispatchToProps(dispatch).clickOnBuyPlanOnly(true);
    mapDispatchToProps(dispatch).ageLimitSuccess();
  });
});
