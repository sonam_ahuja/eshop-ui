import { IMapDispatchToProps, IMapStateToProps } from '@tariff/types';
import { LandingPageWrapper } from '@tariff/utils/index';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Helmet } from 'react-helmet';
import TnC from '@tariff/TnC';
import { Loader, Utils } from 'dt-components';
import TariffPlans from '@tariff/TariffPlans';
import DiscountPlans from '@tariff/Discounts';
import TariffHeader from '@tariff/TariffHeader';
import { IProps } from '@tariff/index';
import Header from '@common/components/Header';
import React, { Component, Fragment } from 'react';
import BannerPlans from '@tariff/BannerPlans';
import Footer from '@common/components/Footer';
import { ICharacteristic, IPlans } from '@tariff/store/types';
import { RootState } from '@src/common/store/reducers';
import htmlKeys from '@common/constants/appkeys';
import { getCharacterstic } from '@category/utils/helper';
import { isMobile } from '@common/utils';
import {
  getProductOfferingTermAndBenefit,
  getTariffCategorySlug
} from '@tariff/utils';
import TariffShell from '@tariff/index.shell';
import FaqsHtml from '@tariff/Faqs';
import AgeLimit from '@tariff/AgeLimit';
import { StyledContentWrapper, StyledTariff } from '@tariff/styles';
import StickyHeaderPlans from '@tariff/sticky-plans';
import { pageViewEvent } from '@events/index';
import { PAGE_VIEW } from '@events/constants/eventName';
import Error from '@src/common/components/Error';
import showGenricError from '@common/utils/showGenericError';
import BasketStripWrapper from '@common/components/BasketStripWrapper';
import AuxiliaryRoute from '@common/components/AuxiliaryRoute';
import auxillaryRouteConfig from '@common/constants/auxiliaryRouteConfig';

import { mapDispatchToProps, mapStateToProps } from './mapProps';

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

export type IProps = RouteComponentProps<IRouterParams> &
  IMapDispatchToProps &
  IMapStateToProps;

export class Tariff extends Component<IProps, {}> {
  plansStickyRef: HTMLElement | null = null;
  multiplePlansWrapRef: HTMLElement | null = null;
  mobileAccordionPanelRef: HTMLElement | null = null;

  constructor(props: IProps) {
    super(props);
    this.redirectToListingPage = this.redirectToListingPage.bind(this);
    this.addPlanToBasket = this.addPlanToBasket.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.setPlansStickyRef = this.setPlansStickyRef.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.setMultiplePlansWrapRef = this.setMultiplePlansWrapRef.bind(this);
    this.setMobileAccordionPanelRef = this.setMobileAccordionPanelRef.bind(
      this
    );
  }

  onRouteChange(route: string): void {
    this.props.history.push(route);
  }

  componentDidMount(): void {
    window.scrollTo({
      top: 0
    });
    const category = getTariffCategorySlug(this.props.location.pathname);
    pageViewEvent(`${PAGE_VIEW.TARIFF}/${category}`);
    const {
      productOfferingTerm,
      productOfferingBenefit
    } = getProductOfferingTermAndBenefit(this.props.location.search);
    this.props.setCategoryId(category);

    this.props.fetchCategories(category);
    this.props.fetchTariff(
      productOfferingTerm,
      productOfferingBenefit,
      true,
      true,
      undefined,
      true
    );
    window.addEventListener(
      'scroll',
      this.onScroll,
      Utils.passiveEvents
        ? {
            passive: true
          }
        : false
    );
  }

  // tslint:disable-next-line:cognitive-complexity
  onScroll(): void {
    // for Desktop Case!
    if (
      this.plansStickyRef &&
      this.multiplePlansWrapRef &&
      !isMobile.phone &&
      !isMobile.tablet
    ) {
      // 150 height ofplan name of column
      const topScrollDistance = this.multiplePlansWrapRef.offsetTop + 150;
      // 80 height of sticky
      const bottomScrollDistance =
        this.multiplePlansWrapRef.offsetTop +
        this.multiplePlansWrapRef.offsetHeight -
        80;
      if (
        window.scrollY > topScrollDistance &&
        window.scrollY < bottomScrollDistance
      ) {
        if (
          this.plansStickyRef &&
          !this.plansStickyRef.classList.contains('sticky')
        ) {
          this.plansStickyRef.classList.add('sticky');
        }
      } else {
        if (
          this.plansStickyRef &&
          this.plansStickyRef.classList.contains('sticky')
        ) {
          this.plansStickyRef.classList.remove('sticky');
        }
      }
    }
    // for Mobile Case!
    else if (this.plansStickyRef && this.mobileAccordionPanelRef) {
      const topScrollDistance = this.mobileAccordionPanelRef.offsetTop;
      // 80 height of sticky
      const bottomScrollDistance =
        this.mobileAccordionPanelRef.offsetTop +
        this.mobileAccordionPanelRef.offsetHeight -
        80;
      if (
        window.scrollY > topScrollDistance &&
        window.scrollY < bottomScrollDistance
      ) {
        if (
          this.plansStickyRef &&
          !this.plansStickyRef.classList.contains('sticky')
        ) {
          this.plansStickyRef.classList.add('sticky');
        }
      } else {
        if (
          this.plansStickyRef &&
          this.plansStickyRef.classList.contains('sticky')
        ) {
          this.plansStickyRef.classList.remove('sticky');
        }
      }
    }
  }

  componentWillUnmount(): void {
    this.props.resetPlansData();
    this.props.showTermAndConditions(false);
    window.removeEventListener('scroll', this.onScroll);
  }

  redirectToListingPage(tariffId: string): void {
    this.props.setTariffId(tariffId);
    if (this.checkAgeLimit(tariffId)) {
      this.props.clickOnAddToBasket(true);
      this.props.youngTariffCheck(tariffId);
    } else {
      this.props.saveTariff();
      const queryParam = `tariffId=${tariffId}&loyalty=${
        this.props.tariff.selectedProductOfferingTerm.value
      }&discountId=${this.props.tariff.selectedProductOfferingBenefit.value}`;

      this.props.history.push(
        `/${this.props.tariff.linkCategorySlug}?${encodeURIComponent(
          queryParam
        )}`
      );
    }
  }

  setMultiplePlansWrapRef(ref: HTMLElement | null): void {
    this.multiplePlansWrapRef = ref;
  }

  setMobileAccordionPanelRef(ref: HTMLElement | null): void {
    this.mobileAccordionPanelRef = ref;
  }

  closeModal(): void {
    this.props.ageLimitSuccess();
  }

  addPlanToBasket(tariffId: string): void {
    this.props.setTariffId(tariffId);
    if (this.checkAgeLimit(tariffId)) {
      this.props.clickOnBuyPlanOnly(true);
      this.props.youngTariffCheck(tariffId);
    } else {
      this.props.saveTariff();
      this.props.buyPlanOnly();
    }
  }

  checkAgeLimit = (tariffId: string): boolean => {
    const { plans } = this.props.tariff;
    const restrictedPlan = plans.filter(
      (plan: IPlans) => plan.id === tariffId
    )[0];

    const birthDateLimit =
      restrictedPlan &&
      restrictedPlan.characteristics.filter(
        (charecterstic: ICharacteristic) => {
          return charecterstic.name === 'BirthDateLimit';
        }
      )[0];

    return !!birthDateLimit;
  }

  setPlansStickyRef(ref: HTMLElement | null): void {
    this.plansStickyRef = ref;
  }

  // tslint:disable-next-line:cognitive-complexity
  render(): React.ReactNode {
    const {
      tariffTranslation,
      validateAgeLimit,
      tariff,
      commonError
    } = this.props;
    const { categories, isTnCModalOpen, loading } = tariff;
    const {
      showAppShell,
      showYoungTariff,
      showYoungTariffErrorMessage
    } = tariff;

    const plansBannerHtml1 = getCharacterstic(
      categories.characteristics,
      htmlKeys.TARIFF_HYBRID_PLAN_BANNER
    );
    const plansBannerHtml2 = getCharacterstic(
      categories.characteristics,
      htmlKeys.TARIFF_DATA_ONLY_PLAN_BANNER
    );
    const backgroundImage = getCharacterstic(
      categories.characteristics,
      htmlKeys.TARIFF_BACKGROUND_IMAGE
    );
    const metaTagTitle = getCharacterstic(
      categories.characteristics,
      htmlKeys.META_TAG_TITLE
    );
    const metaTagDescription = getCharacterstic(
      categories.characteristics,
      htmlKeys.META_TAG_DESCRIPTION
    );

    const showMetaTitle =
      metaTagTitle && metaTagTitle !== '' && metaTagTitle.length !== 0
        ? metaTagTitle
        : categories.name;

    return (
      <Fragment>
        <Helmet
          title={showMetaTitle ? showMetaTitle : ''}
          meta={[
            {
              name: 'description',
              content: metaTagDescription ? metaTagDescription : ''
            }
          ]}
        />
        <Header showBasketStrip={false} />

        {!showAppShell && loading && (
          <Loader open={loading} label={tariffTranslation.loadingText} />
        )}

        {showAppShell ? <TariffShell /> : null}
        {showGenricError(commonError, true) ? (
          <Error />
        ) : (
          <>
            {showGenricError(commonError, false) && <Error />}
            <StyledTariff>
              <StyledContentWrapper>
                {!showAppShell ? (
                  <>
                    <div className='tariff-container'>
                      <StickyHeaderPlans
                        {...this.props}
                        setRef={this.setPlansStickyRef}
                      />
                      <TariffHeader {...this.props} planSelectionOnly={false} />
                      <TariffPlans
                        {...this.props}
                        planSelectionOnly={false}
                        addPlanToBasket={this.addPlanToBasket}
                        redirectToListingPage={this.redirectToListingPage}
                        setMultiplePlansWrapRef={this.setMultiplePlansWrapRef}
                        setMobileAccordionPanelRef={
                          this.setMobileAccordionPanelRef
                        }
                      />
                      <DiscountPlans {...this.props} />

                      <LandingPageWrapper
                        theme={categories.theme}
                        backgroundImage={backgroundImage || ''}
                        className='tariff-product-container'
                      >
                        {(plansBannerHtml2 || plansBannerHtml1) && (
                          <BannerPlans
                            onRouteChange={this.onRouteChange}
                            specialSpecialOfferHtml1={
                              plansBannerHtml1 ? plansBannerHtml1 : ''
                            }
                            specialSpecialOfferHtml2={
                              plansBannerHtml2 ? plansBannerHtml2 : ''
                            }
                          />
                        )}
                        <FaqsHtml {...this.props} />
                      </LandingPageWrapper>

                      {isTnCModalOpen && <TnC {...this.props} />}
                        <AuxiliaryRoute
                          isChildrenRender={showYoungTariff}
                          hashPath={auxillaryRouteConfig.TARIFF.YOUNG_AGE}
                          isModal={true}
                          onClose={this.closeModal}
                        >
                          {showYoungTariff && <AgeLimit
                            youngTariffTranslation={
                              tariffTranslation.youngTariff
                            }
                            validateAgeLimit={validateAgeLimit}
                            closeModal={this.closeModal}
                            error={showYoungTariffErrorMessage}
                          />
                          }
                        </AuxiliaryRoute>
                    </div>
                  </>
                ) : null}
              </StyledContentWrapper>
              {!showAppShell ? <BasketStripWrapper /> : null}
            </StyledTariff>
            <Footer pageName={PAGE_VIEW.TARIFF} />
          </>
        )}
      </Fragment>
    );
  }
}
export default withRouter<RouteComponentProps>(
  connect<
    IMapStateToProps,
    IMapDispatchToProps,
    RouteComponentProps,
    RootState
  >(
    mapStateToProps,
    mapDispatchToProps
  )(Tariff)
  // tslint:disable-next-line:max-file-line-count
);
