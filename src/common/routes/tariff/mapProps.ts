import { RootState } from '@src/common/store/reducers';
import { Dispatch } from 'redux';
import actions from '@tariff/store/actions';
import { IMapDispatchToProps, IMapStateToProps } from '@tariff/types';
import { ILabelValue, ITariffFilter } from '@tariff/store/types';

export const mapStateToProps = (state: RootState): IMapStateToProps => ({
  commonError: state.common.error,
  tariff: state.tariff,
  currency: state.configuration.cms_configuration.global.currency,
  tariffTranslation: state.translation.cart.tariff,
  tariffConfiguration: state.configuration.cms_configuration.modules.tariff,
  selectedProductOfferingTerm: state.tariff.selectedProductOfferingTerm,
  selectedProductOfferingBenefit: state.tariff.selectedProductOfferingBenefit,
  globalTranslation: state.translation.cart.global,
  categories: state.tariff.categories,
  confirmPlanBtnLoading: state.tariff.confirmPlanBtnLoading,
});
export const mapDispatchToProps = (
  dispatch: Dispatch
): IMapDispatchToProps => ({
  fetchTariff(
    productOfferingTerm: string | null,
    productOfferingBenefit: string | null,
    updateUrl: boolean,
    // tslint:disable-next-line:bool-param-default
    showFullPageError?: boolean,
    filter?: ITariffFilter,
    // tslint:disable-next-line:bool-param-default
    sendEvent?: boolean
  ): void {
    dispatch(
      actions.fetchTariffPlan({
        productOfferingTerm,
        productOfferingBenefit,
        updateUrl,
        showFullPageError,
        filter,
        sendEvent
      })
    );
  },
  fetchCategories(categoryId: string, scrollPosition?: number): void {
    dispatch(actions.fetchCategories({ categoryId, scrollPosition }));
  },
  setLoyalty(payload: ILabelValue): void {
    dispatch(actions.setLoyalty(payload));
  },
  setDiscount(payload: ILabelValue): void {
    dispatch(actions.setDiscount(payload));
  },
  showMagentaDiscount(payload: boolean): void {
    dispatch(actions.showMagentaDiscount(payload));
  },
  showAppShell(payload: boolean): void {
    dispatch(actions.showAppShell(payload));
  },
  showTermAndConditions(status: boolean): void {
    dispatch(actions.showTermAndConditions(status));
  },
  setTariffId(tariffId: string): void {
    dispatch(actions.setTariffId(tariffId));
  },
  buyPlanOnly(): void {
    dispatch(actions.buyPlanOnly());
  },
  saveTariff(callBackFn: () => {}): void {
    dispatch(actions.saveTariff(callBackFn));
  },
  toggleSelectedBenefit(
    id: string,
    benefitName: string,
    benefitValue: string,
    templateType: string,
    newSelected: boolean
  ): void {
    dispatch(
      actions.toggleSelectedBenefit({
        id,
        benefitName,
        benefitValue,
        templateType,
        newSelected
      })
    );
  },
  setCategoryId(categoryId: string): void {
    dispatch(actions.setCategoryId(categoryId));
  },
  resetPlansData(): void {
    dispatch(actions.resetPlansData());
  },
  validateAgeLimit(data: string): void {
    dispatch(actions.validateAgeLimit(data));
  },
  youngTariffCheck(planId: string): void {
    dispatch(actions.checkYoungTariff(planId));
  },
  clickOnAddToBasket(status: boolean): void {
    dispatch(actions.clickOnAddToBasket(status));
  },
  clickOnBuyPlanOnly(status: boolean): void {
    dispatch(actions.clickOnBuyPlanOnly(status));
  },
  ageLimitSuccess(): void {
    dispatch(actions.ageLimitSuccess());
  },
  getTariffAddonPrice(tariffId: string): void {
    dispatch(actions.getTariffAddonPrice(tariffId));
  }
});
