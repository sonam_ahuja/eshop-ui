import React from 'react';
import { mount } from 'enzyme';
import { ThemeProvider } from 'dt-components';
import { StaticRouter } from 'react-router';
import StickyHeaderPlans from '@tariff/sticky-plans';
import { tarrifProps } from '@mocks/tariff/tariff.mock';
import { IProps } from '@tariff/index';

describe('<StickyHeaderPlans />', () => {
  const props: IProps & {
    setRef(ref: HTMLElement | null): void;
  } = { ...tarrifProps, setRef: jest.fn() };
  const componentWrapper = (newProps: IProps & {
    setRef(ref: HTMLElement | null): void;
  }) => mount(
    <StaticRouter context={{}}>
      <ThemeProvider theme={{}}>
        <StickyHeaderPlans {...newProps} />
      </ThemeProvider>
    </StaticRouter>
  );

  test('should render properly', () => {
    const component = componentWrapper(props);
    expect(component).toMatchSnapshot();
  });
});
