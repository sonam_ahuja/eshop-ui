import React from 'react';
import { IProps } from '@tariff/index';
import { getPriceAndCurrency } from '@tariff/store/utils';
import { isMobile } from '@src/common/utils';
import { IPlans } from '@tariff/store/types';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import { getLabels } from '@tariff/utils/getAllPlansTitles';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';

import { StyledStickyTariffPlans } from './styles';

const StickyHeaderPlans = (
  props: IProps & {
    setRef(ref: HTMLElement | null): void;
  }
) => {
  const {
    tariffTranslation: { price, moSymbol },
    currency
  } = props;
  const config = getCharactersticsConfig(
    props.tariff.categories.characteristics
  );
  const plans = props.tariff.plans.map((plan: IPlans) => {
    const planPrice = plan.totalPrices && plan.totalPrices[0];

    if (!planPrice) {
      return;
    }

    let discountPrice = null;
    let discountCurrency = null;
    if (planPrice) {
      const priceObj = getPriceAndCurrency(
        planPrice.discountedValue !== 0
          ? planPrice.discountedValue
          : planPrice.actualValue,
        currency.locale
      );
      discountPrice = priceObj.discountPrice;
      discountCurrency = priceObj.discountCurrency;
    }

    return {
      planName: plan.name,
      discountPrice,
      discountCurrency
    };
  });

  if (isMobile.phone || isMobile.tablet) {
    const { tariffConfiguration, tariffTranslation } = props;

    const renderPriceLabel =
      TARIFF_LIST_TEMPLATE.NAME_FIRST ===
        tariffConfiguration.tariffListingTemplate ||
      TARIFF_LIST_TEMPLATE.DATA_FIRST ===
        tariffConfiguration.tariffListingTemplate
        ? config.showPrice && (
            <li className='lists'>
              <span className='titles'>{tariffTranslation.pricePerMonth}</span>
            </li>
          )
        : null;

    const renderPlanLabel =
      TARIFF_LIST_TEMPLATE.DATA_FIRST ===
      tariffConfiguration.tariffListingTemplate ? (
        <li className='lists'>
          <span className='titles'>
            {config.showPlanName && tariffTranslation.plan}
          </span>
        </li>
      ) : null;

    return (
      <StyledStickyTariffPlans
        plansCount={props.tariff.plans.length}
        ref={props.setRef}
      >
        <div className='header-stick'>
          <ul className='list-inline clearfix'>
            {renderPlanLabel}
            {getLabels(props, false)}
            {renderPriceLabel}
          </ul>
        </div>
      </StyledStickyTariffPlans>
    );
  }

  return (
    <StyledStickyTariffPlans
      plansCount={props.tariff.plans.length}
      ref={props.setRef}
    >
      <div className='multiple-plans'>
        <div className='pricing-plans-wrapper clearfix'>
          <div className='aside-panel'>
            <header className='plans-header clearfix'>
              <div className='title-wrap'>
                <h3>{config.showPrice && `${price} / ${moSymbol}`}</h3>
              </div>
            </header>
          </div>
          <div className='flex-container'>
            {plans &&
              plans.length &&
              plans.map((planItem, index) => {
                return (
                  planItem && (
                    <div key={index} className='flex-panel'>
                      <header className='plans-header clearfix'>
                        <div className='title-wrap'>
                          {config.showPrice && (
                            <span className='price-text'>
                              {planItem.discountPrice}
                              <sup>{planItem.discountCurrency}</sup>
                            </span>
                          )}
                          <span className='titles' title={planItem.planName}>
                            {config.showPlanName && planItem.planName}
                          </span>
                        </div>
                      </header>
                    </div>
                  )
                );
              })}
          </div>
        </div>
      </div>
    </StyledStickyTariffPlans>
  );
};

export default StickyHeaderPlans;
