import styled from 'styled-components';
import { breakpoints, colors } from '@src/common/variables';

export const StyledStickyTariffPlans = styled.div<{ plansCount: number }>`
  display: flex;
  position: fixed;
  top: 0;
  transform: translateY(-102%);
  opacity: 0;
  visibility: hidden;
  z-index: 100;
  padding: 1rem 2.25rem;
  background: ${colors.white};
  align-items: center;
  width: 100%;
  box-shadow: 0 1px 40px 0 rgba(144, 144, 144, 0.2);
  height: 4rem;
  .multiple-plans {
    width: 100%;
    .pricing-plans-wrapper {
      .aside-panel {
        width: 7.25rem;
        float: left;
        .plans-header {
          display: flex;
          align-items: center;
          justify-content: center;
          padding: 0;
          background: transparent;
          .title-wrap {
            float: none;
            width: 100%;
            text-align: center;
            margin-top: 0.6rem;
            h3 {
              color: ${colors.black};
              font-size: 0.75rem;
              line-height: 1rem;
              text-align: left;
            }
          }
        }
      }
      .flex-container {
        width: calc(100% - 8.75rem);
        float: left;
        display: flex;
        padding-left: 0.5rem;
        .flex-panel {
          flex: 1 1 auto;
          margin-right: 0.5rem;
          text-align: center;
          border-radius: 10px;
          border: 1px solid transparent;
          transition: all 0.5s ease;
          cursor: default;
          flex-shrink: 0;
          ${({ plansCount }) =>
            plansCount ? `width:${100 / plansCount}%` : ''};
          &:last-child {
            margin-right: 0;
          }
        }
        header {
          display: flex;
          align-items: center;
          padding: 0 1rem;
          .title-wrap {
            width: 100%;
            h3 {
              font-size: 1.125rem;
              line-height: 1.25rem;
              color: ${colors.ironGray};
            }
          }
          .price-text {
            font-size: 1.5rem;
            line-height: 1.75rem;
            color: ${colors.magenta};
            font-weight: 900;
            sup {
              vertical-align: super;
              font-size: 0.625rem;
              line-height: 0.75rem;
              font-weight: 500;
            }
          }
          .titles {
            font-size: 0.75rem;
            line-height: 1rem;
            color: ${colors.mediumGray};
            display: block;
            font-weight: bold;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
          }
          .modify-text {
            font-size: 0.75rem;
            line-height: 1rem;
            text-decoration: line-through;
            letter-spacing: -0.3px;
            color: ${colors.magenta};
            text-decoration: line-through;
            display: block;
            .value {
              color: ${colors.darkGray};
            }
          }
          .icon-wrap {
            position: absolute;
            top: 0.75rem;
            right: 0.75rem;
            visibility: hidden;
            i {
              font-size: 1.75rem;
            }
          }
        }
      }
    }
  }
  &.sticky {
    transition: transform 0.3s ease 0s, opacity 0.3s ease 0s;
    transform: translateY(0);
    opacity: 1;
    visibility: visible;
  }
  .header-stick {
    position: relative;
    width: 100%;
    ul {
      display: flex;
      flex-wrap: wrap;
      li {
        width: 25%;
        flex: 1;
        &:last-child {
          text-align: right;
        }
      }
    }
    .titles {
      font-size: 0.75rem;
      line-height: 1rem;
      color: ${colors.ironGray};
      letter-spacing: 0.24px;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      display: block;
    }
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: calc(100% - 5.75rem);
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: calc(100% - 4.75rem);
    padding: 0.75rem 4.9rem 0.5rem;
    background: ${colors.silverGray};
    box-shadow: none;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    max-width: calc(${breakpoints.desktopLarge}px - 5.75rem);
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    max-width: ${breakpoints.desktopLarge}px;
  }
`;
