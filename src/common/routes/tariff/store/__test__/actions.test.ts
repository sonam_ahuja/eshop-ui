import tariffActions from '@tariff/store/actions';
import configureMockStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';
import tariffSaga from '@tariff/store/sagas';
import CONSTANTS from '@tariff/store/constants';
import { IError } from '@productDetailed/store/types';
import {
  IFetchTariffPlanPayload
} from '@tariff/store/types';

describe('actions test', () => {
  const sagaMiddleware = createSagaMiddleware();
  const mockStore = configureMockStore([sagaMiddleware]);
  const store = mockStore({});
  sagaMiddleware.run(tariffSaga);

  it('fetchTariffPlan action creator should return a object with expected value', () => {
    const expected: { payload: IFetchTariffPlanPayload, type: string } = {
      payload:
      {
        productOfferingBenefit: 'string', productOfferingTerm: 'string',
        updateUrl: true
      },
      type: CONSTANTS.FETCH_TARIFF_PLAN_REQUESTED
    };
    const payload: IFetchTariffPlanPayload = {
      productOfferingTerm: 'string',
      productOfferingBenefit: 'string',
      updateUrl: true
    };
    const newState = store.dispatch(tariffActions.fetchTariffPlan(payload));
    expect(newState).toEqual(expected);
  });

  it('fetchTariffPlanLoading action creator should return a object with expected value', () => {
    const newState = store.dispatch(tariffActions.fetchTariffPlanLoading());
    expect(newState).toBeDefined();
  });

  it('fetchTariffPlanError action creator should return a object with expected value', () => {
    const expected = {
      payload:
      {
        code: 201,
        message: 'string'
      },
      type: CONSTANTS.FETCH_TARIFF_PLAN_ERROR
    };
    const params: IError = {
      code: 201,
      message: 'string'
    };
    const newState = store.dispatch(tariffActions.fetchTariffPlanError(params));
    expect(newState).toEqual(expected);
  });
});
