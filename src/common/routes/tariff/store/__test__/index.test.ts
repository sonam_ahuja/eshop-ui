import tariffState from '@tariff/store/state';
import tariffReducer from '@tariff/store/reducer';

describe('tariffState Reducer', () => {
  it('tariffState reducer test', () => {
    const action: { type: string } = { type: '' };
    expect(tariffReducer(tariffState(), action)).toEqual(
      tariffState()
    );
  });
});
