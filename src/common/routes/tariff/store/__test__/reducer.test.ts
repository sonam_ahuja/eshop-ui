import tariff from '@tariff/store/reducer';
import tariffState from '@tariff/store/state';
import appState from '@store/states/app';
import {
  IFetchTariffPlanPayload,
  ITariffState,
  ITariffTransformedResponse
} from '@tariff/store/types';
import actions from '@tariff/store/actions';
import { plan } from '@mocks/tariff/tariff.mock';

describe('Tariff Reducer', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ITariffState = tariff(undefined, definedAction);
  let newState: ITariffState = tariffState();
  const initializeValue = () => {
    initialStateValue = tariff(undefined, definedAction);
    newState = tariffState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('initial state', () => {
    expect(initialStateValue).toMatchSnapshot();
  });

  it('FETCH_TARIFF_PLAN_REQUESTED should mutate state', () => {
    const params: IFetchTariffPlanPayload = {
      productOfferingBenefit: 'string',
      productOfferingTerm: 'string',
      updateUrl: true
    };
    const expectedData = { ...newState };
    const action = actions.fetchTariffPlan(params);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SHOW_APP_SHELL should mutate state', () => {
    const params = true;
    const mutation: {
      showAppShell: boolean;
    } = {
      showAppShell: true
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.showAppShell(params);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SHOW_LOADING should mutate state', () => {
    const params = true;
    const mutation: {
      loading: boolean;
      showAppShell: boolean;
    } = {
      loading: true,
      showAppShell: true
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.showLoading(params);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('RESET_PLANS_DATA should mutate state', () => {
    const expectedData = { ...newState };
    const action = actions.resetPlansData();
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_TARIFF_PLAN_SUCCESS should mutate state', () => {
    const response: ITariffTransformedResponse = {
      ...tariffState(),
      plans: [plan],
      linkedCategorySlug: '',
      tariffCategoryId: ''
    };
    const expectedData = { ...newState, plans: [plan] };
    const action = actions.fetchTariffPlanSuccess(response);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_CATEGORIES_REQUESTED should mutate state', () => {
    const mutation: {
      loading: boolean;
    } = {
      loading: true
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.fetchCategories({
      categoryId: 'string',
      scrollPosition: 12
    });
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_CATEGORIES_SUCCESS should mutate state', () => {
    const expectedData = { ...newState };
    const action = actions.fetchCategoriesSuccess();
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('FETCH_CATEGORIES_ERROR should mutate state', () => {
    const expectedData = { ...newState };
    const action = actions.fetchCategoriesError({
      name: 'error',
      stack: 'stack',
      message: 'message'
    });
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_CATEGORIES_DATA should mutate state', () => {
    const expectedData = { ...newState };
    const action = actions.setCategoriesData(appState().categories.categories);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_LOYALTY_PLAN_FILTER should mutate state', () => {
    const mutation: {
      loading: boolean;
      selectedProductOfferingTerm: {
        isSelected: boolean;
        label: string;
        value: string;
      };
    } = {
      loading: true,
      selectedProductOfferingTerm: {
        isSelected: true,
        label: 'string',
        value: 'string'
      }
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setLoyalty({
      label: 'string',
      value: 'string',
      isSelected: true
    });
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_DISCOUNT_PLAN_FILTER should mutate state', () => {
    const mutation: {
      loading: boolean;
      selectedProductOfferingBenefit: {
        label: string;
        value: string;
      };
    } = {
      loading: true,
      selectedProductOfferingBenefit: {
        label: 'string',
        value: 'string'
      }
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.setDiscount({
      label: 'string',
      value: 'string'
    });
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SHOW_MAGENTA_DISCOUNT should mutate state', () => {
    const mutation: {
      showMagentaPlan: boolean;
    } = {
      showMagentaPlan: true
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.showMagentaDiscount(true);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('OPEN_TERM_AND_CONDITIONS_MODAL should mutate state', () => {
    const mutation: {
      isTnCModalOpen: boolean;
      showMagentaPlan: boolean;
    } = {
      isTnCModalOpen: true,
      showMagentaPlan: false
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.showTermAndConditions(true);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
});
