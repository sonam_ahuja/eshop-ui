import tariff from '@tariff/store/reducer';
import tariffState from '@tariff/store/state';
import {
  ITariffState,
  IToggleSelectedBenefitPayload
} from '@tariff/store/types';
import actions from '@tariff/store/actions';
import { plan } from '@mocks/tariff/tariff.mock';

describe('Tariff Reducer 2', () => {
  const definedAction: { type: string } = { type: '' };
  let initialStateValue: ITariffState = tariff(undefined, definedAction);
  let newState: ITariffState = tariffState();
  const initializeValue = () => {
    initialStateValue = tariff(undefined, definedAction);
    newState = tariffState();
  };
  beforeEach(() => {
    initializeValue();
  });

  it('TOGGLE_SELECTED_BENEFIT should mutate state', () => {
    const response: ITariffState = { ...tariffState(), plans: [plan] };
    const payload: IToggleSelectedBenefitPayload = {
      id: 'string',
      benefitName: 'string',
      benefitValue: 'string',
      templateType: 'string',
      newSelected: true
    };
    const expectedData = {
      ...newState,
      plans: [plan]
    };
    const action = actions.toggleSelectedBenefit(payload);
    const newMutatedState = tariff(response, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_CATEGORY_ID should mutate state', () => {
    const mutation: {
      categoryId: string;
    } = {
      categoryId: 'string'
    };
    const expectedData = {
      ...newState,
      ...mutation
    };
    const action = actions.setCategoryId('string');
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('BUY_PLAN_ERROR should mutate state', () => {
    const mutation: {
      error?: string;
    } = {
      error: undefined
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.buyPlanOnlyError();
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('CHECK_YOUNG_TARIFF should mutate state', () => {
    const mutation: {
      showYoungTariff: boolean;
      youngTariffId: string;
    } = {
      showYoungTariff: true,
      youngTariffId: 'string'
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.checkYoungTariff('string');
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('AGE_LIMIT_SUCCESS should mutate state', () => {
    const mutation: {
      isAddAPhoneClick: boolean;
    } = {
      isAddAPhoneClick: false
    };
    const expectedData = { ...newState, ...mutation };
    const action = actions.ageLimitSuccess();
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('AGE_LIMIT_ERROR should mutate state', () => {
    const mutation: {
      isBuyPlanOnlyClick: boolean;
    } = {
      isBuyPlanOnlyClick: false
    };
    const expectedData = {
      ...newState,
      ...mutation,
      showYoungTariffErrorMessage: true
    };
    const action = actions.ageLimitError();
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('SET_TARIFF_ID should mutate state', () => {
    const mutation: {
      isBuyPlanOnlyClick: boolean;
    } = {
      isBuyPlanOnlyClick: false
    };
    const expectedData = {
      ...newState,
      ...mutation,
      selectedTariffId: 'string',
      showYoungTariffErrorMessage: false
    };
    const action = actions.setTariffId('string');
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('CLICK_ON_ADD_TO_BASKET should mutate state', () => {
    const mutation: {
      isBuyPlanOnlyClick: boolean;
    } = {
      isBuyPlanOnlyClick: false
    };
    const expectedData = {
      ...newState,
      ...mutation,
      isAddAPhoneClick: true
    };
    const action = actions.clickOnAddToBasket(true);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });

  it('CLICK_ON_BUY_PLAN_ONLY should mutate state', () => {
    const mutation: {
      isBuyPlanOnlyClick: boolean;
    } = {
      isBuyPlanOnlyClick: false
    };
    const expectedData = {
      ...newState,
      ...mutation,
      isBuyPlanOnlyClick: true
    };
    const action = actions.clickOnBuyPlanOnly(true);
    const newMutatedState = tariff(initialStateValue, action);
    expect(newMutatedState).toEqual(expectedData);
  });
});
