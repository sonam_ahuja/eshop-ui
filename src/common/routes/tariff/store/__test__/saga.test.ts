import sagas, {
  fetchCategories,
  saveTariffPlan,
  updateUrl,
  validateAgeLimit
} from '@tariff/store/sagas';
import { ITariffTransformedResponse } from '@tariff/store/types';
import CONSTANTS from '@tariff/store/constants';

describe('Tariff Saga', () => {
  window.scrollTo = jest.fn();
  it('updateUrl saga test', () => {
    const generator = sagas();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  it('updateUrl saga test', () => {
    const transformedResponse: ITariffTransformedResponse = {
      selectedTariffId: 'string',
      titles: [
        {
          title: 'string',
          type: 'string',
          name: 'string'
        }
      ],
      commonTitles: [
        {
          title: 'string',
          type: 'string',
          name: 'string'
        }
      ],
      plans: [],
      loyalty: [],
      discounts: [],
      selectedProductOfferingTerm: {
        label: 'string',
        value: 'string',
        isSelected: true
      },
      selectedProductOfferingBenefit: {
        label: 'string',
        value: 'string',
        isSelected: true
      },
      linkedCategorySlug: '',
      tariffCategoryId: ''
    };
    const generator = updateUrl(transformedResponse, 'string');
    expect(generator.next().value).toBeUndefined();
  });

  it('fetchCategories saga test', () => {
    const payload: {
      type: string;
      payload: { categoryId: string; scrollPosition: number };
    } = {
      type: CONSTANTS.FETCH_CATEGORIES_REQUESTED,
      payload: {
        categoryId: '1',
        scrollPosition: 34
      }
    };
    const generator = fetchCategories(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });

  it('saveTariffPlan saga test', () => {
    const payload: {
      type: string;
      payload(): void;
    } = {
      type: CONSTANTS.SAVE_TARIFF_PLAN_REQUESTED,
      // tslint:disable-next-line:no-empty
      payload: () => {}
    };
    const generator = saveTariffPlan(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeDefined();
  });

  it('validateAgeLimit saga test', () => {
    const payload: {
      type: string;
      payload: string;
    } = {
      type: CONSTANTS.VALIDATE_AGE_LIMIT,
      payload: 'payload'
    };

    const generator = validateAgeLimit(payload);
    expect(generator.next().value).toBeDefined();
    expect(generator.next().value).toBeUndefined();
  });
});
