import {
  fetchTariffPlanService,
  saveTariffService
} from '@tariff/store/services';
import { apiEndpoints } from '@common/constants';
import tariffState from '@tariff/store/state';
import axios from 'axios';
import { ISaveTariff, ITariffRequest } from '@tariff/store/types';
import MockAdapter from 'axios-mock-adapter';

describe('Tariff Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchTariffPlanService test', async () => {
    const payload: ITariffRequest = {
      category: 'string',
      channel: 'string',
      selectedAttributes: []
    };
    const url: string = apiEndpoints.TARIFF.GET_TARIFF_PLAN.url;
    const expectedData = tariffState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await fetchTariffPlanService(payload);
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });

  it('saveTariffService test', async () => {
    const payload: ISaveTariff = {
      category: 'string',
      selectedProductOfferingTerm: 'string',
      selectedTariff: 'string',
      discount: 'string',
      addons: { type: ['value'] },
    };
    const url: string = apiEndpoints.TARIFF.SAVE_TARIFF_PLAN.url;
    const expectedData = tariffState();
    mock.onPost(url).replyOnce(200, expectedData);
    axios
      .post(url)
      .then(async () => {
        const result = await saveTariffService(payload);
        expect(result).toEqual(expectedData);
      })
      .catch(error => {
        expect(error).toBeDefined();
      });
  });
});
