import * as transformers from '@tariff/store/transformer';
import appState from '@store/states/app';
import {
  Characteristics,
  ProductListResponse
} from '@common/mock-api/productList/productList.mock';
import { plan, tariffResponse } from '@common/mock-api/tariff/tariff.mock';
import { IBenefits } from '@tariff/store/types';

describe('Transformer', () => {
  it('getBestDeviceOffers ::', () => {
    const data = transformers.getBestDeviceOffers(ProductListResponse);
    expect(data).toEqual(data);
  });

  it('getCategorySlugImage ::', () => {
    const data = transformers.getCategorySlugImage(Characteristics);
    expect(data).toEqual(data);
  });

  it('getParentCategorySlug ::', () => {
    const data = transformers.getParentCategorySlug(Characteristics);
    expect(data).toEqual(data);
  });

  it('getCharacteristicValue ::', () => {
    const data = transformers.getCharacteristicValue(Characteristics, 'string');
    expect(data).toEqual(data);
  });

  it('getHighLightProduct ::', () => {
    const data = transformers.getHighLightProduct(Characteristics);
    expect(data).toEqual(data);
  });

  it('transformCategoriesResponse ::', () => {
    const data = transformers.transformCategoriesResponse(
      appState().categories.categories
    );
    expect(data).toEqual(data);
  });

  it('transformTariffResponse ::', () => {
    const data = transformers.transformTariffResponse(
      tariffResponse,
      appState().translation.cart.tariff,
      appState().configuration.cms_configuration.modules.tariff
    );
    expect(data).toEqual(data);
  });

  it('getDataBenefit ::', () => {
    const data = transformers.getDataBenefit(plan);
    expect(data).toEqual(data);
  });

  it('removeDataBenefit ::', () => {
    const data = transformers.removeDataBenefit(plan);
    expect(data).toEqual(data);
  });

  it('removeDataLabel ::', () => {
    const data = transformers.removeDataLabel([
      {
        type: 'string',
        name: 'string',
        title: 'string'
      }
    ]);
    expect(data).toEqual(data);
  });

  it('filledBenefits ::', () => {
    const emptyBenefits: IBenefits = {
      name: '',
      label: '',
      isSelectable: false,
      values: [
        {
          unit: null,
          prices: [],
          label: '',
          totalPrices: [],
          value: '',
          attachments: {}
        }
      ]
    };
    const data = transformers.filledBenefits(
      [emptyBenefits],
      [
        {
          type: '',
          name: '',
          title: ''
        }
      ]
    );
    expect(data).toEqual(data);
  });
});
