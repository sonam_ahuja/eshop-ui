import * as utils from '@tariff/store/utils';
import { IBenefits, ITariffRequestParams } from '@tariff/store/types';
import { PRICE_TYPE, RECURRING_TYPE } from '@tariff/store/enum';
import appState from '@store/states/app';
import { planMock } from '@common/mock-api/tariff/tariff.mock';

describe('Utils', () => {
  it('getBestDeviceOffers ::', () => {
    const params: ITariffRequestParams = {
      productOfferingTerm: 'string',
      productOfferingBenefit: 'string',
      category: 'Mobile'
    };
    const data = utils.getTariffRequestParams(params);
    expect(data).toEqual(data);
  });

  it('getPriceAndCurrency ::', () => {
    const data = utils.getPriceAndCurrency(12, 'USD');
    expect(data).toEqual(data);
  });

  it('getBasketRequest ::', () => {
    const emptyBenefits: IBenefits = {
      name: '',
      label: '',
      isSelectable: false,
      values: [
        {
          unit: null,
          label: '',
          totalPrices: [],
          prices: [],
          value: '',
          attachments: {}
        }
      ]
    };
    const data = utils.getBasketRequest(
      'string',
      [
        {
          priceType: PRICE_TYPE.RECURRING,
          actualValue: 300,
          recurringChargePeriod: RECURRING_TYPE.MONTH,
          discounts: [],
          discountedValue: 200,
          recurringChargeOccurrence: 1,
          dutyFreeValue: 0,
          taxRate: 1,
          taxPercentage: 20
        }
      ],
      [],
      [],
      [emptyBenefits],
      ''
    );
    expect(data).toEqual(data);
  });

  it('getTariffTranslation ::', () => {
    const data = utils.getTariffTranslation(appState());
    expect(data).toEqual(data);
  });

  it('getTariffState ::', () => {
    const data = utils.getTariffState(appState());
    expect(data).toEqual(data);
  });

  it('getTariffState ::', () => {
    expect(utils.getSelectedPlan(planMock, 'id')).toBeDefined();
  });
});
