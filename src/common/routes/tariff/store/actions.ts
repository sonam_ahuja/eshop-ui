import { actionCreator } from '@common/utils/actionCreator';
import CONSTANTS from '@tariff/store/constants';
import {
  IError,
  IFetchTariffPlanPayload,
  ILabelValue,
  ITariffAddonPriceResponse,
  ITariffTransformedResponse,
  IToggleSelectedBenefitPayload
} from '@tariff/store/types';
import * as categoryApi from '@common/types/api/categories';
import { ITariffConfiguration } from '@src/common/store/types/configuration';

export default {
  fetchTariffPlan: actionCreator<IFetchTariffPlanPayload>(
    CONSTANTS.FETCH_TARIFF_PLAN_REQUESTED
  ),
  fetchTariffPlanLoading: actionCreator<void>(
    CONSTANTS.FETCH_TARIFF_PLAN_LOADING
  ),
  fetchTariffPlanError: actionCreator<IError>(
    CONSTANTS.FETCH_TARIFF_PLAN_ERROR
  ),
  fetchTariffPlanSuccess: actionCreator<ITariffTransformedResponse>(
    CONSTANTS.FETCH_TARIFF_PLAN_SUCCESS
  ),

  addPhone: actionCreator<void>(CONSTANTS.ADD_A_PHONE_REQUESTED),
  addPhoneSuccess: actionCreator<void>(CONSTANTS.ADD_A_PHONE_SUCCESS),
  addPhoneError: actionCreator<void>(CONSTANTS.ADD_A_PHONE_ERROR),

  fetchCategories: actionCreator<{
    categoryId: string;
    scrollPosition?: number;
  }>(CONSTANTS.FETCH_CATEGORIES_REQUESTED),
  fetchCategoriesLoading: actionCreator(CONSTANTS.FETCH_CATEGORIES_LOADING),
  fetchCategoriesError: actionCreator<Error>(CONSTANTS.FETCH_CATEGORIES_ERROR),
  fetchCategoriesSuccess: actionCreator<void>(
    CONSTANTS.FETCH_CATEGORIES_SUCCESS
  ),
  setCategoriesData: actionCreator<categoryApi.GET.IResponse>(
    CONSTANTS.SET_CATEGORIES_DATA
  ),

  buyPlanOnly: actionCreator<void>(CONSTANTS.BUY_PLAN_REQUESTED),
  buyPlanOnlyError: actionCreator<void>(CONSTANTS.BUY_PLAN_ERROR),
  buyPlanOnlySuccess: actionCreator<void>(CONSTANTS.BUY_PLAN_SUCCESS),

  setLoyalty: actionCreator<ILabelValue>(CONSTANTS.SET_LOYALTY_PLAN_FILTER),
  setDiscount: actionCreator<ILabelValue>(CONSTANTS.SET_DISCOUNT_PLAN_FILTER),
  setTariffId: actionCreator<string>(CONSTANTS.SET_TARIFF_ID),

  showTermAndConditions: actionCreator<boolean>(
    CONSTANTS.OPEN_TERM_AND_CONDITIONS_MODAL
  ),
  showCommonBenifits: actionCreator<boolean>(CONSTANTS.SHOW_COMMON_BENIFITS),
  showAppShell: actionCreator<boolean>(CONSTANTS.SHOW_APP_SHELL),
  showLoading: actionCreator<boolean>(CONSTANTS.SHOW_LOADING),
  showMagentaDiscount: actionCreator<boolean>(CONSTANTS.SHOW_MAGENTA_DISCOUNT),
  setError: actionCreator<IError>(CONSTANTS.SET_ERROR),
  saveTariffError: actionCreator<IError>(CONSTANTS.SAVE_TARIFF_PLAN_ERROR),
  saveTariff: actionCreator<(() => void) | void>(
    CONSTANTS.SAVE_TARIFF_PLAN_REQUESTED
  ),

  toggleSelectedBenefit: actionCreator<IToggleSelectedBenefitPayload>(
    CONSTANTS.TOGGLE_SELECTED_BENEFIT
  ),
  setCategoryId: actionCreator<string>(CONSTANTS.SET_CATEGORY_ID),
  resetPlansData: actionCreator<void>(CONSTANTS.RESET_PLANS_DATA),

  validateAgeLimit: actionCreator<string>(CONSTANTS.VALIDATE_AGE_LIMIT),
  checkYoungTariff: actionCreator<string>(CONSTANTS.CHECK_YOUNG_TARIFF),
  ageLimitSuccess: actionCreator<void>(CONSTANTS.AGE_LIMIT_SUCCESS),
  ageLimitError: actionCreator<void>(CONSTANTS.AGE_LIMIT_ERROR),

  clickOnAddToBasket: actionCreator<boolean>(CONSTANTS.CLICK_ON_ADD_TO_BASKET),
  clickOnBuyPlanOnly: actionCreator<boolean>(CONSTANTS.CLICK_ON_BUY_PLAN_ONLY),
  getTariffAddonPrice: actionCreator<string>(CONSTANTS.GET_TARIFF_ADD_ON_PRICE),
  updateTariffAddonPrice: actionCreator<{
    response: ITariffAddonPriceResponse;
    configuration: ITariffConfiguration;
  }>(CONSTANTS.UPDATE_TARIFF_ADD_ON_PRICE),
  setConfirmBtnLoading: actionCreator<boolean>(
    CONSTANTS.SET_CONFIRM_BTN_LOADING
  )
};
