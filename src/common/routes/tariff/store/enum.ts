// discuse currency enum ISO 4217 currency codes.
// Need to add currency formatter API

export enum PRICE_TYPE {
  RECURRING = 'recurringFee',
  ACTIVATION = 'activationFee',
  BASE = 'basePrice',
  UPFRONT = 'upfrontPrice',
  DISCOUNT = 'discount',
  PENALTY = 'penalty',
  USAGE = 'usage',
  ROAMING = 'roamingUsage'
}

export enum RECURRING_TYPE {
  MIN = 'min',
  HOUR = 'hour',
  DAY = 'day',
  WEEK = 'week',
  MONTH = 'month'
}

export enum DISCOUNT_TYPES {
  MAGENTA_PLAN = 'magentaPlan',
  FAMILY_PLAN = 'familyPlan'
}

export enum TARIFF_BENEFIT {
  DATA = 'Data',
  MINUTES = 'minutes'
}
