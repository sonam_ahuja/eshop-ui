// tslint:disable:no-commented-code
import { AnyAction, Reducer } from 'redux';
import initialState from '@tariff/store/state';
import {
  IError,
  ILabelValue,
  IPlans,
  ITariffAddonPriceResponse,
  ITariffState,
  ITariffTransformedResponse
} from '@tariff/store/types';
import CONSTANTS from '@tariff/store/constants';
import withProduce from '@src/common/utils/withProduce';
import { ICategory } from '@category/store/types';
import { TARIFF_BENEFIT_TEMPLATE_TYPE } from '@common/constants/appkeys';
import { sendCTAClicks } from '@events/index';
import { ITariffConfiguration } from '@src/common/store/types/configuration';

import { filledBenefits, getAllBenefits } from './transformer';

const reducers = {
  [CONSTANTS.SHOW_APP_SHELL]: (state: ITariffState, payload: boolean) => {
    state.showAppShell = payload;
  },
  [CONSTANTS.SHOW_LOADING]: (state: ITariffState, payload: boolean) => {
    state.loading = payload;
  },
  [CONSTANTS.RESET_PLANS_DATA]: (state: ITariffState) => {
    state.plans = [];
    state.commonTitles = [];
    state.titles = [];
  },
  [CONSTANTS.FETCH_TARIFF_PLAN_SUCCESS]: (
    state: ITariffState,
    payload: ITariffTransformedResponse
  ) => {
    state.loading = false;
    state.plans = payload.plans;
    state.tariffCategoryId = payload.tariffCategoryId;
    state.linkCategorySlug = payload.linkedCategorySlug;
    state.selectedTariffId = payload.selectedTariffId;
    state.commonTitles = payload.commonTitles;
    state.loyalty = payload.loyalty;
    state.discounts = payload.discounts;
    state.titles = payload.titles;
    state.selectedProductOfferingBenefit =
      payload.selectedProductOfferingBenefit;
    state.selectedProductOfferingTerm = payload.selectedProductOfferingTerm;
  },
  [CONSTANTS.FETCH_CATEGORIES_REQUESTED]: (state: ITariffState) => {
    state.loading = true;
  },
  [CONSTANTS.FETCH_CATEGORIES_SUCCESS]: (state: ITariffState) => {
    state.loading = false;
  },
  [CONSTANTS.FETCH_CATEGORIES_ERROR]: (state: ITariffState) => {
    state.loading = false;
  },
  [CONSTANTS.SET_CATEGORIES_DATA]: (
    state: ITariffState,
    payload: ICategory
  ) => {
    const categories = payload;
    state.categories = categories;
  },
  [CONSTANTS.SET_LOYALTY_PLAN_FILTER]: (
    state: ITariffState,
    payload: ILabelValue
  ) => {
    state.loading = true;
    state.selectedProductOfferingTerm = payload;
  },
  [CONSTANTS.SET_DISCOUNT_PLAN_FILTER]: (
    state: ITariffState,
    payload: ILabelValue
  ) => {
    state.loading = true;
    state.selectedProductOfferingBenefit = payload;
  },
  [CONSTANTS.SHOW_MAGENTA_DISCOUNT]: (
    state: ITariffState,
    payload: boolean
  ) => {
    state.showMagentaPlan = payload;
    state.showFaimlyPlan = !payload;
  },
  [CONSTANTS.OPEN_TERM_AND_CONDITIONS_MODAL]: (
    state: ITariffState,
    payload: boolean
  ) => {
    state.isTnCModalOpen = payload;
    if (payload) {
      sendCTAClicks(
        CONSTANTS.OPEN_TERM_AND_CONDITIONS_MODAL,
        window.location.href
      );
    }
  },
  [CONSTANTS.SET_TARIFF_ID]: (state: ITariffState, payload: string) => {
    state.selectedTariffId = payload;
    state.plans =
      state.plans &&
      state.plans.map((plan: IPlans) => {
        return {
          ...plan,
          isSelected: plan.id === payload
        };
      });
  },
  [CONSTANTS.TOGGLE_SELECTED_BENEFIT]: (
    state: ITariffState,
    payload: {
      id: string;
      benefitName: string;
      benefitValue: string;
      templateType: string;
      newSelected: boolean;
    }
  ) => {
    const {
      id,
      benefitName,
      benefitValue,
      templateType,
      newSelected
    } = payload;
    state.plans.forEach(plan => {
      if (plan.id === id) {
        plan.benefits.forEach(benefit => {
          if (benefit.name === benefitName) {
            benefit.values.forEach(benefitValueItem => {
              if (templateType === TARIFF_BENEFIT_TEMPLATE_TYPE.SINGLE_SELECT) {
                // if new newSelected is true make others isSelected false
                if (newSelected) {
                  benefitValueItem.isSelected =
                    benefitValueItem.value === benefitValue;
                }
              } else {
                if (benefitValueItem.value === benefitValue) {
                  benefitValueItem.isSelected = !benefitValueItem.isSelected;

                  return;
                }
              }
            });

            return;
          }
        });

        return;
      }
    });
  },
  [CONSTANTS.SET_CATEGORY_ID]: (state: ITariffState, payload: string) => {
    state.categoryId = payload;
  },
  [CONSTANTS.BUY_PLAN_ERROR]: (state: ITariffState, payload: IError) => {
    state.error = payload;
  },
  [CONSTANTS.CHECK_YOUNG_TARIFF]: (state: ITariffState, payload: string) => {
    state.youngTariffId = payload;
    state.showYoungTariff = true;
  },
  [CONSTANTS.AGE_LIMIT_SUCCESS]: (state: ITariffState) => {
    state.showYoungTariff = false;
    state.showYoungTariffErrorMessage = false;
  },
  [CONSTANTS.AGE_LIMIT_ERROR]: (state: ITariffState) => {
    state.showYoungTariffErrorMessage = true;
  },
  [CONSTANTS.CLICK_ON_ADD_TO_BASKET]: (
    state: ITariffState,
    payload: boolean
  ) => {
    state.isAddAPhoneClick = payload;
    state.isBuyPlanOnlyClick = !payload;
  },
  [CONSTANTS.CLICK_ON_BUY_PLAN_ONLY]: (
    state: ITariffState,
    payload: boolean
  ) => {
    state.isBuyPlanOnlyClick = payload;
    state.isAddAPhoneClick = !payload;
  },

  [CONSTANTS.SET_CONFIRM_BTN_LOADING]: (
    state: ITariffState,
    payload: boolean
  ) => {
    state.confirmPlanBtnLoading = payload;
  },
  [CONSTANTS.UPDATE_TARIFF_ADD_ON_PRICE]: (
    state: ITariffState,
    payload: {
      response: ITariffAddonPriceResponse;
      configuration: ITariffConfiguration;
    }
  ) => {
    state.plans = state.plans.map((plan: IPlans) => {
      if (plan.id === payload.response.id) {
        plan.prices = payload.response.prices;
        plan.totalPrices = payload.response.totalPrices;
        plan.benefits = filledBenefits(payload.response.benefits, state.titles);
        plan.allBenefits = getAllBenefits(
          payload.response.benefits,
          plan.commonBenefits,
          payload.configuration
        );
      }

      return plan;
    });
  }
};

export default withProduce(initialState, reducers) as Reducer<
  ITariffState,
  AnyAction
>;
