import { isMobile, logError } from '@src/common/utils';
import { fetchCategoriesService } from '@category/store/services';
import {
  transformCategoriesResponse,
  transformTariffResponse
} from '@src/common/routes/tariff/store/transformer';
import { isBrowser } from '@common/utils';
import {
  ICharacteristic,
  IFetchTariffPlanPayload,
  ITariffRequestParams,
  ITariffState,
  ITariffTransformedResponse
} from '@tariff/store/types';
import actions from '@tariff/store/actions';
import CONSTANTS from '@tariff/store/constants';
import * as categoryApi from '@src/common/types/api/categories';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { Effect } from 'redux-saga';
import {
  fetchTariffPlanService,
  getTariffAddonPriceService,
  saveTariffService
} from '@tariff/store/services';
import {
  getBasketRequest,
  getSessionTariffRequest,
  getState,
  getTariffAddonPriceRequest,
  getTariffRequestParams
} from '@tariff/store/utils';
import history from '@client/history';
import { addToBasketService } from '@productDetailed/store/services';
import basketActions from '@basket/store/actions';
import store from '@common/store';
import { BIRTH_DATE_LIMIT } from '@src/common/constants/appConstants';
import { getTariffState } from '@common/store/common/index';
import moment from 'moment';
import { sendTariffAddToBasketEvent } from '@events/basket/index';
import {
  sendTariffDiscountDropDownEvent,
  sendTariffDurationDropDownEvent,
  sendTariffImpressionEvent
} from '@events/tariff/index';
import { sendOnBasketLoadEvent, sendonBasketLoadTrigger } from '@events/basket';
import { BFF_ERROR_CODE } from '@src/common/components/Error/constants';
import ROUTES_CONSTANTS from '@src/common/constants/routes';
import { IBasket as IBasketTranslation } from '@src/common/store/types/translation';
import { getBasketTranslation } from '@src/common/store/common';

import {
  getSelectedPlan,
  getTariffConfiguration,
  getTariffTranslation
} from './utils';

export function* updateUrl(
  transformedResponse: ITariffTransformedResponse,
  categoryId: string
): SagaIterator {
  if (isBrowser && history) {
    let benefitValue = null;
    let offeringTermValue = null;
    if (transformedResponse.selectedProductOfferingBenefit) {
      benefitValue = transformedResponse.selectedProductOfferingBenefit.value;
    }
    if (transformedResponse.selectedProductOfferingTerm) {
      offeringTermValue = transformedResponse.selectedProductOfferingTerm.value;
    }
    const loginStep = new URLSearchParams(
      decodeURIComponent(history.location.search)
    ).get('loginStep');
    history.replace(
      `/tariff/${categoryId}?productOfferingBenefit=${
        benefitValue ? benefitValue : ''
      }&productOfferingTerm=${
        offeringTermValue ? offeringTermValue : ''
      }&loginStep=${loginStep ? loginStep : ''}`
    );
  }
}
// tslint:disable-next-line:cognitive-complexity
export function* fetchTariffData(_action: {
  type: string;
  payload: IFetchTariffPlanPayload;
}): Generator {
  const {
    payload: {
      productOfferingTerm,
      productOfferingBenefit,
      showFullPageError,
      filter,
      sendEvent
    }
  } = _action;
  const state: ITariffState = yield select(getState);
  state.plans.length !== 0
    ? yield put(actions.showLoading(true))
    : yield put(actions.showAppShell(true));
  let useProductOfferingTermAndBenefitsFromState = true;

  if (productOfferingTerm !== null || productOfferingBenefit !== null) {
    useProductOfferingTermAndBenefitsFromState = false;
  }

  const requestParams: ITariffRequestParams = {
    category: state.categoryId
  };
  if (filter) {
    if (filter.isDiscount) {
      sendTariffDiscountDropDownEvent(filter.selectedItem.title);
      requestParams.productOfferingBenefit = filter.selectedItem.id as string;
      requestParams.productOfferingTerm =
        state.selectedProductOfferingTerm.value;
    } else {
      sendTariffDurationDropDownEvent(filter.selectedItem.title);
      requestParams.productOfferingTerm = filter.selectedItem.id as string;
      requestParams.productOfferingBenefit =
        state.selectedProductOfferingBenefit.value;
    }
  } else if (useProductOfferingTermAndBenefitsFromState) {
    requestParams.productOfferingTerm = state.selectedProductOfferingTerm.value;
    requestParams.productOfferingBenefit =
      state.selectedProductOfferingBenefit.value;
  } else {
    if (productOfferingTerm !== null) {
      requestParams.productOfferingTerm = productOfferingTerm;
    }
    if (productOfferingBenefit !== null) {
      requestParams.productOfferingBenefit = productOfferingBenefit;
    }
  }

  let result = null;

  const tariffRequestParams = getTariffRequestParams(requestParams);
  const tariffTranslation = yield select(getTariffTranslation);
  const tariffConfiguration = yield select(getTariffConfiguration);

  try {
    result = yield fetchTariffPlanService(
      tariffRequestParams,
      showFullPageError
    );
    const transformedResponse = transformTariffResponse(
      result,
      tariffTranslation,
      tariffConfiguration
    );
    yield put(actions.fetchTariffPlanSuccess(transformedResponse));

    if (sendEvent) {
      sendTariffImpressionEvent();
    }

    if (filter) {
      const { selectedItem, isDiscount } = filter;
      const selectedFilter = {
        label: selectedItem.title,
        value: selectedItem.value
      };
      if (isDiscount) {
        yield put(actions.setDiscount(selectedFilter));
      } else {
        yield put(actions.setLoyalty(selectedFilter));
      }
    }

    if (_action.payload.updateUrl) {
      yield call(updateUrl, transformedResponse, state.categoryId);
    }
    yield put(actions.showAppShell(false));
    yield put(actions.showLoading(false));
  } catch (error) {
    yield put(actions.showAppShell(false));
    yield put(actions.showLoading(false));
    logError(error);
  }
}

export type SagaIterator = IterableIterator<
  Effect | Effect[] | Promise<categoryApi.GET.IResponse | Error>
>;

export function* fetchCategories(action: {
  type: string;
  payload: { categoryId: string; scrollPosition: number };
}): Generator {
  try {
    const {
      payload: { categoryId, scrollPosition }
    } = action;

    const categories = yield fetchCategoriesService(categoryId);

    yield put(
      actions.setCategoriesData(transformCategoriesResponse(categories))
    );

    yield put(actions.fetchCategoriesSuccess());
    if (isBrowser && scrollPosition !== undefined) {
      window.scrollTo({
        top: scrollPosition
      });
    }
  } catch (error) {
    logError(error);
    yield put(actions.fetchCategoriesError(error));
  }
}

export function* saveTariffPlan(action: {
  type: string;
  payload(): void;
}): Generator {
  const state: ITariffState = store.getState().tariff;
  const { tariffCategoryId } = state;
  const selectedPlan = getSelectedPlan(state.plans, state.selectedTariffId);
  if (action && action.payload) {
    yield put(actions.setConfirmBtnLoading(true));
  }
  const benefits =
    selectedPlan && selectedPlan.benefits ? selectedPlan.benefits : [];

  // TODO: Hardcoding category slug only for MVP,
  // it will remove after linking; of categories in the sales catalog
  const requestObj = getSessionTariffRequest(
    state.selectedTariffId,
    tariffCategoryId,
    state.selectedProductOfferingTerm.value,
    state.selectedProductOfferingBenefit.value,
    benefits
  );

  try {
    yield saveTariffService(requestObj);
    if (action && action.payload) {
      action.payload();
      yield put(actions.setConfirmBtnLoading(false));
    }
  } catch (error) {
    if (action && action.payload) {
      yield put(actions.setConfirmBtnLoading(false));
    }
    logError(error);
    yield put(actions.saveTariffError(error));
  }
}

export function* addTariffInBasket(): Generator {
  const state: ITariffState = yield select(getState);
  const basketTranslation: IBasketTranslation = yield select(
    getBasketTranslation
  );
  const priceChangedMessage: string = basketTranslation.priceChanged;
  const selectedPlan = getSelectedPlan(state.plans, state.selectedTariffId);

  const benefits =
    selectedPlan && selectedPlan.benefits ? selectedPlan.benefits : [];
  const basketObj = getBasketRequest(
    state.selectedTariffId,
    selectedPlan && selectedPlan.prices ? selectedPlan.prices : [],
    state.discounts,
    state.loyalty,
    benefits,
    selectedPlan ? selectedPlan.group : null
  );

  try {
    const {
      cartItems,
      cartSummary,
      totalItems,
      priceChanged,
      code
    } = yield addToBasketService(basketObj);
    if (code && code === BFF_ERROR_CODE.DT_DEVICE_TARIFF_RESTRICTION) {
      yield put(basketActions.setCartRestrictionMessage(true));
      if (history) {
        history.push(ROUTES_CONSTANTS.BASKET);

        return;
      }
    }
    yield put(
      basketActions.setBasketData({ cartItems, cartSummary, totalItems })
    );
    yield put(
      basketActions.setPriceNotificationStrip({
        show: priceChanged,
        notification: priceChangedMessage
      })
    );
    sendTariffAddToBasketEvent(state.selectedTariffId, state.plans);
    yield put(basketActions.setCheckoutButtonStatus(cartItems));
    yield put(basketActions.setCartRestrictionMessage(false));
    if (!isMobile.phone || isMobile.tablet) {
      sendOnBasketLoadEvent();
      sendonBasketLoadTrigger();
      yield put(basketActions.showBasketSideBar(true));
    } else {
      if (history) {
        history.push(ROUTES_CONSTANTS.BASKET);
        document.documentElement.scrollTop = 0;
      }
    }
  } catch (error) {
    yield put(actions.buyPlanOnlyError(error));
  }
}

export function* validateAgeLimit(action: {
  type: string;
  payload: string;
}): Generator {
  const state: ITariffState = store.getState().tariff;
  const { plans, youngTariffId, linkCategorySlug } = state;

  const selectedTariff = getSelectedPlan(plans, youngTariffId);

  const selectedCharacteristic =
    selectedTariff &&
    selectedTariff.characteristics.find((item: ICharacteristic) => {
      return item.name === BIRTH_DATE_LIMIT;
    });

  let fromDate = new Date();
  let toDate = new Date();

  if (selectedCharacteristic) {
    fromDate = new Date(selectedCharacteristic.values[0].valueFrom as string);
    toDate = new Date(selectedCharacteristic.values[0].valueTo as string);
  }

  try {
    const enteredData = new Date(action.payload);
    const isBetweenBool = moment(enteredData).isBetween(
      fromDate,
      toDate,
      'seconds',
      '()'
    );
    if (isBetweenBool) {
      yield put(actions.ageLimitSuccess());
      yield put(actions.saveTariff());
      if (state.isBuyPlanOnlyClick) {
        yield put(actions.buyPlanOnly());
      } else if (state.isAddAPhoneClick && history) {
        const query = `tariffId=${youngTariffId}&loyalty=${
          state.selectedProductOfferingTerm.value
        }&discountId=${state.selectedProductOfferingBenefit.value}`;
        history.push(`/${linkCategorySlug}?${encodeURIComponent(query)}`);
      }
      yield put(actions.clickOnAddToBasket(false));
      yield put(actions.clickOnBuyPlanOnly(false));
    } else {
      const { errorMessage } = yield select(getTariffTranslation);

      yield put(actions.ageLimitError(errorMessage));
    }
  } catch (error) {
    logError(error);
  }
}

export function* getTariffAddonPrice(action: {
  type: string;
  payload: string;
}): Generator {
  const tariffState: ITariffState = yield select(getTariffState);
  const tariffId = action.payload;
  const request = getTariffAddonPriceRequest(tariffState, tariffId);
  try {
    const response = yield getTariffAddonPriceService(request, tariffId);
    const tariffConfiguration = yield select(getTariffConfiguration);

    yield put(
      actions.updateTariffAddonPrice({
        response,
        configuration: tariffConfiguration
      })
    );
  } catch (error) {
    logError(error);
  }
}

export default function* watcherSagas(): Generator {
  yield takeLatest(CONSTANTS.FETCH_TARIFF_PLAN_REQUESTED, fetchTariffData);
  yield takeLatest(CONSTANTS.FETCH_CATEGORIES_REQUESTED, fetchCategories);
  yield takeLatest(CONSTANTS.BUY_PLAN_REQUESTED, addTariffInBasket);
  yield takeLatest(CONSTANTS.SAVE_TARIFF_PLAN_REQUESTED, saveTariffPlan);
  yield takeLatest(CONSTANTS.VALIDATE_AGE_LIMIT, validateAgeLimit);
  yield takeLatest(CONSTANTS.GET_TARIFF_ADD_ON_PRICE, getTariffAddonPrice);
  // tslint:disable-next-line:max-file-line-count
}
