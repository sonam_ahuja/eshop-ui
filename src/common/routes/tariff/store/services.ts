import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import * as tariffApi from '@common/types/api/tariff';
import { logError } from '@common/utils';

export const fetchTariffPlanService = async (
  payload: tariffApi.GET.IRequest,
  // tslint:disable-next-line:bool-param-default
  showFullPageError?: boolean
): Promise<tariffApi.GET.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.TARIFF.GET_TARIFF_PLAN.method.toLowerCase()
    ](apiEndpoints.TARIFF.GET_TARIFF_PLAN.url, payload, { showFullPageError });
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const saveTariffService = async (
  payload: tariffApi.POST.IRequest
): Promise<tariffApi.POST.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.TARIFF.SAVE_TARIFF_PLAN.method.toLowerCase()
    ](apiEndpoints.TARIFF.SAVE_TARIFF_PLAN.url, payload);
  } catch (error) {
    logError(error);
    throw error;
  }
};

export const getTariffAddonPriceService = async (
  payload: tariffApi.GET_TARIFF_ADDON_PRICE.IRequest,
  tariffId: string
): Promise<tariffApi.GET_TARIFF_ADDON_PRICE.IResponse | Error> => {
  try {
    return await apiCaller[
      apiEndpoints.TARIFF.GET_TARIFF_ADDON_PRICE.method.toLowerCase()
    ](apiEndpoints.TARIFF.GET_TARIFF_ADDON_PRICE.url(tariffId), payload);
  } catch (error) {
    logError(error);
    throw error;
  }
};
