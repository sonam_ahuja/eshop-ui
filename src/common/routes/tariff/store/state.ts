import { ITariffState } from '@tariff/store/types';
import { PAGE_THEME } from '@src/common/store/enums';

export default (): ITariffState => ({
  loading: false,
  showAppShell: true,
  isTnCModalOpen: false,
  showMagentaPlan: false,
  showFaimlyPlan: false,
  showYoungTariff: false,
  isAddAPhoneClick: false,
  isBuyPlanOnlyClick: false,
  confirmPlanBtnLoading: false,
  youngTariffId: '',
  selectedProductOfferingTerm: {
    label: '',
    value: ''
  },
  selectedProductOfferingBenefit: {
    label: '',
    value: ''
  },
  selectedTariffId: '',
  loyalty: [],
  discounts: [],
  benefits: [],
  commonBenefits: [],
  titles: [],
  tariffCategoryId: '',
  linkCategorySlug: '',
  categoryId: '',
  commonTitles: [],
  plans: [],
  categories: {
    id: '',
    name: '',
    slug: '',
    backgroundImage: '',
    theme: PAGE_THEME.SECONDARY,
    characteristics: [],
    highLightProduct: {
      name: '',
      value: ''
    },
    image: '',
    description: '',
    priority: 0,
    subCategories: [],
    parentSlug: null
  },
  showYoungTariffErrorMessage: false,
  error: {
    code: 0,
    message: ''
  }
});
