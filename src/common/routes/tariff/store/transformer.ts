import { ICategory } from '@category/store/types';
import * as productListApi from '@src/common/types/api/productList';
import htmlKeys from '@common/constants/appkeys';
import { pageThemeType } from '@src/common/store/enums';
import * as tariffApi from '@src/common/types/api/tariff';
import * as categoryApi from '@src/common/types/api/categories';
import { IProductListItem, IVariant } from '@productList/store/types';
import { ITariffTranslation } from '@src/common/store/types/translation';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import constants from '@tariff/common/constants';
import {
  IBenefits,
  IPlans,
  ITariffPlans,
  ITariffTransformedResponse,
  ITitleType
} from '@tariff/store/types';
import store from '@common/store';
import { isMobile } from '@src/common/utils';
import { ITariffConfiguration } from '@src/common/store/types/configuration';

export const getBestDeviceOffers = (
  products: productListApi.POST.IResponse
): IVariant[] => {
  return products.data.reduce(
    (result: IVariant[], product: IProductListItem): IVariant[] => {
      result.push(product.variant);

      return result;
    },
    []
  );
};

export const getCategorySlugImage = (
  characteristics: categoryApi.GET.IResponse['characteristics']
) => {
  const slugObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_SLUG
  )[0];
  const imageObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_IMAGE
  )[0];
  const slug = slugObj && slugObj.values ? slugObj.values[0].value : '';
  const image = imageObj && imageObj.values ? imageObj.values[0].value : '';

  return { slug, image };
};

export const getParentCategorySlug = (
  characteristics: categoryApi.GET.IResponse['characteristics']
) => {
  const slugObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_PARENT_SLUG
  )[0];

  return slugObj && slugObj.values ? slugObj.values[0].value : '';
};

export const getCharacteristicValue = (
  characteristics: categoryApi.GET.IResponse['characteristics'],
  key: string
) => {
  const nameObj = characteristics.filter(
    characteristic => characteristic.name === key
  )[0];

  if (nameObj && nameObj.values) {
    return nameObj.values[0].value;
  }

  return '';
};

export const getHighLightProduct = (
  characteristics: categoryApi.GET.IResponse['characteristics']
) => {
  const nameObj = characteristics.filter(
    characteristic => characteristic.name === htmlKeys.CATEGORY_HIGHLIGHTPRODUCT
  )[0];
  const name = nameObj && nameObj.label ? nameObj.label : '';
  const value = nameObj && nameObj.values ? nameObj.values[0].value : '';

  return { name, value };
};

export const transformCategoriesResponse = (
  categoriesResponse: categoryApi.GET.IResponse
): ICategory => {
  const {
    subCategories,
    characteristics,
    ...categoryRest
  } = categoriesResponse;
  const { slug, image } = getCategorySlugImage(characteristics);
  const highLightProduct = getHighLightProduct(characteristics);
  const parentSlug = getParentCategorySlug(characteristics);
  const theme = getCharacteristicValue(
    characteristics,
    htmlKeys.TARIFF_THEME
  ) as pageThemeType;
  const backgroundImage = getCharacteristicValue(
    characteristics,
    htmlKeys.TARIFF_BACKGROUND_IMAGE
  );

  const mappedSubCategories = subCategories.map(s => {
    const {
      subCategories: SsubCategories,
      characteristics: Scharacteristics,
      ...subcategoryRest
    } = s;
    const slugImage = getCategorySlugImage(Scharacteristics);
    const sHighLightProduct = getHighLightProduct(Scharacteristics);

    const sTheme = getCharacteristicValue(
      Scharacteristics,
      htmlKeys.TARIFF_THEME
    ) as pageThemeType;
    const sbackgroundImage = getCharacteristicValue(
      Scharacteristics,
      htmlKeys.TARIFF_BACKGROUND_IMAGE
    );

    return {
      ...subcategoryRest,
      backgroundImage: sbackgroundImage,
      theme: sTheme,
      slug: slugImage.slug,
      image: slugImage.image,
      characteristics: Scharacteristics,
      highLightProduct: sHighLightProduct,
      subCategories: [],
      parentSlug: null
    };
  });

  return {
    ...categoryRest,
    backgroundImage,
    theme,
    slug,
    image,
    characteristics,
    highLightProduct,
    subCategories: mappedSubCategories,
    parentSlug
  };
};

export const isDataFirstTemplate = (): boolean => {
  const tariffCmsConfig = store.getState().configuration.cms_configuration
    .modules.tariff;

  return (
    tariffCmsConfig.tariffListingTemplate === TARIFF_LIST_TEMPLATE.DATA_FIRST
  );
};

export const getDataBenefit = (plan: IPlans): IBenefits => {
  const data = plan.benefits.filter(
    (benefit: IBenefits) => benefit.name.toLowerCase() === constants.DATA
  )[0];

  return data
    ? data
    : plan.commonBenefits.filter(
        (benefit: IBenefits) => benefit.name.toLowerCase() === constants.DATA
      )[0];
};

export const removeDataLabel = (titles: ITitleType[]): ITitleType[] => {
  return titles.filter(
    (title: ITitleType) => title.name.toLowerCase() !== constants.DATA
  );
};

export const removeDataBenefit = (plan: IPlans) => {
  const updatedPlan: IPlans = { ...plan };
  updatedPlan.benefits = plan.benefits.filter(
    (benefit: IBenefits) => benefit.name.toLowerCase() !== constants.DATA
  );
  updatedPlan.commonBenefits = plan.commonBenefits.filter(
    (benefit: IBenefits) => benefit.name.toLowerCase() !== constants.DATA
  );

  return updatedPlan;
};

export const transformTariffResponse = (
  response: tariffApi.GET.IResponse,
  tariffTranslation: ITariffTranslation,
  tariffConfiguration: ITariffConfiguration
): ITariffTransformedResponse => {
  let titles: ITitleType[] = [];
  let commonTitles: ITitleType[] = [];
  let selectedTariffId = '';
  const isDataFirst = isDataFirstTemplate();
  const tariffResponse = { ...response };
  // @
  // this code added for single tariff in mobile view https://jira.dtoneapp.telekom.net/browse/ECDEV-3528
  if (
    response.tariffs &&
    Array.isArray(response.tariffs) &&
    response.tariffs.length === 1 &&
    isMobile.phone
  ) {
    tariffResponse.tariffs[0].benefits = tariffResponse.tariffs[0].benefits.concat(
      tariffResponse.tariffs[0].commonBenefits
    );
    tariffResponse.tariffs[0].commonBenefits = [];
  }

  tariffResponse.tariffs.forEach((plan: ITariffPlans) => {
    plan.benefits.forEach((benefit: IBenefits) => {
      if (titles.map(titleItem => titleItem.title).indexOf(benefit.label) < 0) {
        titles.push({
          title: benefit.label,
          name: benefit.name,
          type: benefit.isSelectable ? 'addon' : 'benefit'
        });
      }
    });
  });
  tariffResponse.tariffs[0].commonBenefits.forEach(
    (commonBenefit: IBenefits) => {
      if (
        commonTitles
          .map(commonTitlesItem => commonTitlesItem.title)
          .indexOf(commonBenefit.label) < 0
      ) {
        commonTitles.push({
          title: commonBenefit.label,
          name: commonBenefit.name,
          type: 'benefit'
        });
      }
    }
  );

  if (isDataFirst) {
    titles = removeDataLabel(titles);
    commonTitles = removeDataLabel(commonTitles);
  }

  // tslint:disable: no-self-assignment
  titles = titles.sort(compare);
  commonTitles = commonTitles.sort(compare);

  const plans = tariffResponse.tariffs.map((tariffPlan: ITariffPlans) => {
    if (tariffPlan.isSelected) {
      selectedTariffId = tariffPlan.id;
    }
    let data: IBenefits | undefined;
    let plan: IPlans = { ...tariffPlan };
    const allBenefits = getAllBenefits(
      plan.benefits,
      plan.commonBenefits,
      tariffConfiguration
    );
    if (isDataFirst) {
      data = getDataBenefit(plan);
      plan = removeDataBenefit(plan);
    }

    const filledBenefit = filledBenefits(plan.benefits, titles);

    return {
      name: plan.name,
      prices: plan.prices,
      group: plan.group,
      id: plan.id,
      allBenefits,
      totalPrices: plan.totalPrices,
      benefits: filledBenefit
        .map(benefit => {
          const hasPreselectedBenefitValue = benefit.values.some(
            benefitValue => {
              return benefitValue.isSelected === true;
            }
          );
          const values = benefit.values;
          if (!hasPreselectedBenefitValue && values.length > 0) {
            values[0].isSelected = true;
          }

          return {
            ...benefit,
            values,
            selected: false
          };
        })
        .sort(compare),
      commonBenefits: plan.commonBenefits.sort(compare),
      characteristics: plan.characteristics,
      isSelected: plan.isSelected,
      data
    };
  });

  const loyalty = tariffResponse.groups.productOfferingTerm;
  const discounts = tariffResponse.groups.productOfferingBenefit;

  const isTermPresent = tariffResponse.groups.productOfferingTerm.findIndex(
    productOfferingTerm => productOfferingTerm.isSelected
  );

  const isBenefitPresent = tariffResponse.groups.productOfferingBenefit.findIndex(
    productOfferingBenefit => productOfferingBenefit.isSelected
  );

  loyalty.push({
    label: tariffTranslation.noLoyalty,
    value: '0',
    isSelected: isTermPresent === -1
  });

  discounts.push({
    label: tariffTranslation.unfoldedPlans,
    value: '0',
    isSelected: isBenefitPresent === -1
  });

  const selectedProductOfferingBenefit =
    isBenefitPresent === -1
      ? { label: tariffTranslation.unfoldedPlans, value: '0' }
      : {
          label:
            tariffResponse.groups.productOfferingBenefit[isBenefitPresent]
              .label,
          value:
            tariffResponse.groups.productOfferingBenefit[isBenefitPresent].value
        };

  const selectedProductOfferingTerm =
    isTermPresent === -1
      ? { label: tariffTranslation.noLoyalty, value: '0' }
      : {
          label: tariffResponse.groups.productOfferingTerm[isTermPresent].label,
          value: tariffResponse.groups.productOfferingTerm[isTermPresent].value
        };

  return {
    selectedTariffId: selectedTariffId || (plans[0] && plans[0].id),
    titles,
    commonTitles,
    plans,
    linkedCategorySlug: response.linkedCategory,
    tariffCategoryId: response.category,
    loyalty,
    discounts,
    selectedProductOfferingTerm,
    selectedProductOfferingBenefit
  };
};

export function getAllBenefits(
  benefits: IBenefits[],
  commonBenefits: IBenefits[],
  configuration: ITariffConfiguration
): IBenefits[][] {
  const arrRenderingOrder = getRenderingOrder(
    configuration.tariffRenderingOrder
  );
  const allBenefits = [...benefits, ...commonBenefits];
  let startIndex = 0;
  const newAllbenefits: IBenefits[][] = [];
  if (allBenefits && allBenefits.length) {
    arrRenderingOrder.forEach((list: string) => {
      if (Number(list) && Number(list) > 0) {
        const arrTariff = allBenefits.slice(
          startIndex,
          Number(list) + startIndex
        );
        if (Array.isArray(arrTariff) && arrTariff.length) {
          newAllbenefits.push(arrTariff);
        }
        startIndex += Number(list);
      }
    });
  }

  return newAllbenefits;
}

// tslint:disable-next-line:no-any
const getRenderingOrder = (renderingOrder: any) => {
  let arrRenderingOrder: string[] = [];
  if (typeof renderingOrder === 'string' || renderingOrder instanceof String) {
    arrRenderingOrder = renderingOrder.split(',');
  } else {
    arrRenderingOrder.push(renderingOrder);
  }

  return arrRenderingOrder;
};

export function compare(
  benefit1: IBenefits | ITitleType,
  benefit2: IBenefits | ITitleType
): number {
  if (benefit1.name < benefit2.name) {
    return 1;
  }

  if (benefit1.name > benefit2.name) {
    return -1;
  }

  return 0;
}

// function for handling wrong data
export function filledBenefits(
  benefits: IBenefits[],
  titles: ITitleType[]
): IBenefits[] {
  return titles.map((title: ITitleType) => {
    let emptyBenefits: IBenefits = {
      name: title.name,
      label: title.title,
      isSelectable: false,
      values: [
        {
          unit: null,
          label: 'false',
          prices: [],
          totalPrices: [],
          value: '',
          attachments: {}
        }
      ]
    };

    benefits.forEach((benefit: IBenefits) => {
      if (benefit.label === title.title) {
        emptyBenefits = benefit;
      }
    });

    return emptyBenefits;
  });
  // tslint:disable-next-line:max-file-line-count
}
