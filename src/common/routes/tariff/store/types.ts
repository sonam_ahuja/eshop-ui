import { ICategory } from '@category/store/types';
import { IBasketCharacteristic } from '@productDetailed/store/types';
import { IListItem } from 'dt-components/lib/es/components/molecules/select/types';

export interface ITitleType {
  type: string;
  name: string;
  title: string;
}
export interface ITariffState {
  loading: boolean;
  showAppShell: boolean;
  isTnCModalOpen: boolean;
  showMagentaPlan: boolean;
  showFaimlyPlan: boolean;
  confirmPlanBtnLoading: boolean;
  selectedProductOfferingTerm: ILabelValue;
  selectedProductOfferingBenefit: ILabelValue;
  selectedTariffId: string;
  loyalty: IDropDown[];
  discounts: IDropDown[];
  tariffCategoryId: string;
  linkCategorySlug: string;
  categoryId: string;
  benefits: IBenefits[][];
  commonBenefits: IBenefits[][];
  titles: ITitleType[];
  commonTitles: ITitleType[];
  plans: IPlans[];
  error: IError | null;
  categories: ICategory;
  showYoungTariff: boolean;
  showYoungTariffErrorMessage: boolean;
  youngTariffId: string;
  isAddAPhoneClick: boolean;
  isBuyPlanOnlyClick: boolean;
}
export interface IError {
  code?: number;
  message: string;
}
export interface ITariffResponse {
  tariffs: ITariffPlans[];
  groups: IGroups;
  category: string;
  linkedCategory: string;
}
export interface ITariffTransformedResponse {
  selectedTariffId: string;
  titles: ITitleType[];
  commonTitles: ITitleType[];
  plans: IPlans[];
  loyalty: IDropDown[];
  discounts: IDropDown[];
  tariffCategoryId: string;
  linkedCategorySlug: string;
  selectedProductOfferingTerm: ILabelValue;
  selectedProductOfferingBenefit: ILabelValue;
}
export interface IPlans {
  name: string;
  id: string;
  group: string | null;
  prices: IPrices[];
  allBenefits: IBenefits[][];
  totalPrices: IPrices[];
  benefits: IBenefits[];
  commonBenefits: IBenefits[];
  characteristics: ICharacteristic[];
  isSelected?: boolean;
  data?: IBenefits;
}

export interface ITariffRequestParams {
  productOfferingTerm?: string;
  productOfferingBenefit?: string;
  category: string;
}
export interface ITariffPlans {
  name: string;
  isSelected: boolean;
  allBenefits: IBenefits[][];
  description: string;
  totalPrices: IPrices[];
  group: string | null;
  id: string;
  prices: IPrices[];
  benefits: IBenefits[];
  commonBenefits: IBenefits[];
  characteristics: ICharacteristic[];
}

/** USED ON PROLONGATION */
export interface IPrices {
  // priceType: PRICE_TYPE;
  priceType: string;
  actualValue: number;
  // recurringChargePeriod: RECURRING_TYPE;
  recurringChargePeriod?: string;
  discounts: IDiscount[];
  discountedValue: number;
  recurringChargeOccurrence?: number;
  dutyFreeValue?: number;
  taxRate?: number;
  taxPercentage?: number;
}

export interface IDiscount {
  discount: number;
  name: string;
  label: string;
  dutyFreeValue: number;
  taxRate: number;
  taxPercentage: number;
}

export interface IBenefits {
  isSelectable?: boolean;
  label: string;
  name: string;
  values: ITariffValues[];
  selected?: boolean;
}

export interface IBenefitsAttachments {
  thumbnail?: IThumbnail[];
}

export interface IThumbnail {
  url: string;
  name: string;
  type: string;
}

export interface ITariffValues {
  unit: string | null;
  value: string;
  label?: string;
  prices: IPrices[];
  totalPrices: IPrices[];
  isSelected?: boolean;
  attachments: IBenefitsAttachments;
}
export interface IGroups {
  productOfferingTerm: IDropDown[];
  productOfferingBenefit: IDropDown[];
}
export interface ILabelValue {
  label: string;
  value: string;
  isSelected?: boolean;
}

export interface IDropDown {
  name?: string;
  label: string;
  value: string;
  isSelected: boolean;
}

export interface ICharacteristic {
  name: string;
  values: IValue[];
}

export interface IValue {
  value?: string;
  valueTo?: string;
  valueFrom?: string;
}

export interface IRequestCategories {
  categoryId: string;
  scrollPosition?: number;
}
export interface ITariffRequest {
  category: string;
  channel: string;
  selectedAttributes: ISelectedAttributes[];
}
export interface ISelectedAttributes {
  name: string;
  value: string;
}

export interface ISaveTariff {
  category: string;
  selectedProductOfferingTerm: string;
  selectedTariff: string;
  discount: string | null;
  addons: IAddons | null;
}

export interface IAddons {
  [value: string]: string[];
}

export interface IAddToBasketPayload {
  cartItems: ICartItems[];
}

export interface ICartItems {
  action: string;
  quantity: number;
  product: IProduct;
  prices: IPrices[];
  group: string | null;
  cartItems?: ICartItems[];
}

export interface IToggleSelectedBenefitPayload {
  id: string;
  benefitName: string;
  benefitValue: string;
  templateType: string;
  newSelected: boolean;
}

// TODO: update this
export interface IProduct {
  id: string;
  characteristic: IBasketCharacteristic[];
}

export interface IFetchTariffPlanPayload {
  productOfferingTerm: string | null;
  productOfferingBenefit: string | null;
  updateUrl: boolean;
  showFullPageError?: boolean;
  filter?: ITariffFilter;
  sendEvent?: boolean;
}

export interface ICharacteristicConfig {
  showPlanName: boolean;
  showBenefits: boolean;
  showAddButton: boolean;
  showBuyButton: boolean;
  showAddons: boolean;
  showUnlimitedBenefits: boolean;
  showPrice: boolean;
}

export interface ITariffFilter {
  selectedItem: IListItem;
  isDiscount: boolean;
}

export interface ITariffAddonPriceRequest {
  channel: string;
  selectedAttributes: ISelectedAttributes[];
  selectedAddons: {
    [key: string]: string[];
  };
}

export interface ITariffAddonPriceResponse {
  id: string;
  name: string;
  prices: IPrices[];
  totalPrices: IPrices[];
  benefits: IBenefits[];
}
