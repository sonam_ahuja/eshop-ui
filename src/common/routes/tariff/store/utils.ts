import { RootState } from '@src/common/store/reducers';
import {
  IAddons,
  IAddToBasketPayload,
  IBenefits,
  IDropDown,
  IPlans,
  IPrices,
  ITariffAddonPriceRequest,
  ITariffRequest,
  ITariffRequestParams,
  ITariffState
} from '@tariff/store/types';
import APP_CONSTANTS from '@common/constants/appConstants';
import { getFormatedCurrencyValue } from '@common/utils/currency';
import { currencyDetails } from '@src/common/constants/currencyDetails';
import { BASKET_API_ACTION } from '@basket/store/enums';
import { ITariffTranslation } from '@src/common/store/types/translation';
import { logError } from '@src/common/utils';
import { ITariffConfiguration } from '@src/common/store/types/configuration';

export const getTariffRequestTemplate = (): ITariffRequest => {
  return {
    channel: APP_CONSTANTS.CHANNEL,
    category: '',
    selectedAttributes: []
  };
};

export const getState = (state: RootState) => state.tariff;

export const getTariffRequestParams = (
  payload: ITariffRequestParams
): ITariffRequest => {
  const request = getTariffRequestTemplate();
  request.category = payload.category;
  if (payload.productOfferingBenefit && payload.productOfferingBenefit !== '') {
    request.selectedAttributes.push({
      name: 'productOfferingBenefit',
      value: payload.productOfferingBenefit
    });
  }
  if (payload.productOfferingTerm && payload.productOfferingTerm !== '') {
    request.selectedAttributes.push({
      name: 'productOfferingTerm',
      value: payload.productOfferingTerm
    });
  }

  return request;
};

/** USED ON PROLONGATION */
export const getPriceAndCurrency = (
  price: number,
  currencyCode: string
): {
  discountPrice: string;
  discountCurrency: string;
} => {
  const discountedValue = getFormatedCurrencyValue(price);

  return {
    discountPrice: discountedValue,
    discountCurrency:
      currencyCode && currencyDetails[currencyCode]
        ? currencyDetails[currencyCode].symbol
        : ''
  };
};

export const getBasketObject = (): IAddToBasketPayload => {
  return {
    cartItems: [
      {
        action: BASKET_API_ACTION.ADD_ITEM_BASKET,
        quantity: 1,
        group: null,
        product: {
          id: '',
          characteristic: []
        },
        prices: []
      }
    ]
  };
};

export const getBasketRequest = (
  tariffId: string,
  prices: IPrices[],
  discounts: IDropDown[],
  loyalties: IDropDown[],
  benefits: IBenefits[],
  group: string | null
): IAddToBasketPayload => {
  const basketObject = getBasketObject();
  basketObject.cartItems[0].product.id = tariffId;
  basketObject.cartItems[0].prices = prices;
  basketObject.cartItems[0].group = group;
  const addonsArr: { id: string; prices: IPrices[] }[] = [];
  benefits
    .filter(benefit => !!benefit.isSelectable)
    .forEach(benefit => {
      benefit.values.forEach(benefitValueItem => {
        if (benefitValueItem.isSelected) {
          addonsArr.push({
            id: benefitValueItem.value,
            prices: benefitValueItem.prices
          });
        }
      });
    });

  basketObject.cartItems[0].cartItems = [];
  addonsArr.forEach(addonItem => {
    if (basketObject.cartItems[0].cartItems) {
      // TODO: a bad util for this ooperation
      const addonBasketItem = getBasketObject();
      addonBasketItem.cartItems[0].product.id = addonItem.id;
      addonBasketItem.cartItems[0].prices = addonItem.prices;
      basketObject.cartItems[0].cartItems.push(addonBasketItem.cartItems[0]);
    }
  });
  const selectedDiscount = discounts.find(discount => {
    return discount.isSelected;
  });
  const selectedLoyalty = loyalties.find(loyalty => {
    return loyalty.isSelected;
  });
  [selectedDiscount, selectedLoyalty].forEach(item => {
    if (item && item.name) {
      basketObject.cartItems[0].product.characteristic.push({
        name: item.name,
        value: item.value
      });
    }
  });

  return basketObject;
};

export const getSessionTariffRequest = (
  tariffId: string,
  categoryId: string,
  productOfferingTerm: string,
  discount: string,
  benefits: IBenefits[]
) => {
  const addons: IAddons = {};
  benefits
    .filter(benefit => !!benefit.isSelectable)
    .forEach(benefit => {
      addons[benefit.name] = [];
      benefit.values.forEach(benefitValueItem => {
        if (benefitValueItem.isSelected) {
          addons[benefit.name].push(benefitValueItem.value);
        }
      });
    });

  return {
    category: categoryId,
    selectedTariff: tariffId,
    selectedProductOfferingTerm: productOfferingTerm,
    discount,
    addons
  };
};

export const getTariffTranslation = (state: RootState): ITariffTranslation => {
  return state.translation.cart.tariff;
};
export const getTariffConfiguration = (
  state: RootState
): ITariffConfiguration => {
  return state.configuration.cms_configuration.modules.tariff;
};

export const getTariffState = (state: RootState) => {
  return state.tariff;
};

export const getTariffAddonPriceRequest = (
  tariffState: ITariffState,
  tariffId: string
): ITariffAddonPriceRequest => {
  const selectedPlan = getSelectedPlan(tariffState.plans, tariffId);
  const request: ITariffAddonPriceRequest = {
    channel: APP_CONSTANTS.CHANNEL,
    selectedAttributes: [],
    selectedAddons: {}
  };
  try {
    request.selectedAttributes.push({
      name: 'productOfferingBenefit',
      value: tariffState.selectedProductOfferingBenefit.value
    });
    request.selectedAttributes.push({
      name: 'productOfferingTerm',
      value: tariffState.selectedProductOfferingTerm.value
    });

    if (selectedPlan && selectedPlan.benefits && selectedPlan.benefits.length) {
      selectedPlan.benefits
        .filter(benefit => !!benefit.isSelectable)
        .forEach(benefit => {
          if (benefit && benefit.values && benefit.values.length) {
            benefit.values.forEach(benefitValueItem => {
              if (benefitValueItem.isSelected) {
                if (!request.selectedAddons[benefit.name]) {
                  request.selectedAddons[benefit.name] = [];
                }
                request.selectedAddons[benefit.name].push(
                  benefitValueItem.value
                );
              }
            });
          }
        });
    }
  } catch (error) {
    logError(error);
  }

  return request;
};

export const getSelectedPlan = (
  plans: IPlans[],
  tariffId: string
): IPlans | undefined => {
  return plans.find((plan: IPlans) => plan.id === tariffId);
};
