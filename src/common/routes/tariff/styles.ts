import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { StyledFooterPanel } from '@src/common/components/Footer/styles';

export const StyledTariff = styled.div`
  display: flex;
  width: 100%;
  .tariff-container {
    width: 100%;
  }
  ${StyledFooterPanel} {
    width: 100%;
  }
`;

export const StyledContentWrapper = styled.div`
  width: 100%;
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    width: calc(100% - 5.75rem);
  }
  @media (min-width: ${breakpoints.desktop}px) {
    width: calc(100% - 4.75rem);
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    /* width: calc(100% - 5.75rem); */
    width: 100%;
  }
`;
