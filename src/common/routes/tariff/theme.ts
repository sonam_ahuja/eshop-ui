import { colors } from '@src/common/variables';
import { hexToRgbA } from '@src/common/utils';

export const theme = {
  secondary: {
    backgroundColor: colors.black,
    sectionHeading: {
      color: colors.white
    },
    breadcrumb: {
      inactiveColor: hexToRgbA(colors.white, 0.6),
      activeColor: hexToRgbA(colors.white, 1)
    }
  },
  primary: {
    backgroundColor: colors.cloudGray,
    sectionHeading: {
      color: colors.darkGray
    },
    breadcrumb: {
      inactiveColor: colors.mediumGray,
      activeColor: colors.darkGray
    }
  }
};
