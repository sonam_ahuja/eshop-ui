import {
  ILabelValue,
  ITariffFilter,
  ITariffState,
  ITariffValues
} from '@tariff/store/types';
import { IError as IGenricError } from '@common/store/types/common';
import {
  IGlobalTranslation,
  ITariffTranslation
} from '@common/store/types/translation';
import {
  ICurrencyConfiguration,
  ITariffConfiguration
} from '@common/store/types/configuration';
import { ICategory } from '@routes/category/store/types';

export interface IMapDispatchToProps {
  fetchTariff(
    productOfferingTerm: string | null,
    productOfferingBenefit: string | null,
    updateUrl: boolean,
    showFullPageError?: boolean,
    filter?: ITariffFilter,
    sendEvent?: boolean
  ): void;
  fetchCategories(categoryId: string, scrollPosition?: number): void;
  setDiscount(payload: ILabelValue): void;
  setLoyalty(payload: ILabelValue): void;
  showMagentaDiscount(payload: boolean): void;
  showAppShell(payload: boolean): void;
  showTermAndConditions(status: boolean): void;
  setTariffId(tariffId: string): void;
  getTariffAddonPrice(tariffId: string): void;
  buyPlanOnly(): void;
  saveTariff(callBackFn?: () => void): void;
  toggleSelectedBenefit(
    id: string,
    benefitName: string,
    benefitValue: string,
    templateType: string,
    newSelected: boolean
  ): void;
  setCategoryId(categoryId: string): void;
  resetPlansData(): void;
  validateAgeLimit(data: string): void;
  youngTariffCheck(planId: string): void;
  clickOnAddToBasket(status: boolean): void;
  clickOnBuyPlanOnly(status: boolean): void;
  ageLimitSuccess(): void;
}
export interface IMapStateToProps {
  commonError: IGenricError;
  tariff: ITariffState;
  currency: ICurrencyConfiguration;
  tariffTranslation: ITariffTranslation;
  tariffConfiguration: ITariffConfiguration;
  selectedProductOfferingTerm: ILabelValue;
  globalTranslation: IGlobalTranslation;
  categories: ICategory;
  confirmPlanBtnLoading: boolean;
  selectedProductOfferingBenefit: ILabelValue;
}

export interface ITariffTemplateProps {
  label?: string;
  tariffValues: ITariffValues[];
  tariffConfiguration: ITariffConfiguration;
  toggleBenefitSelection(benefitValue: string, newSelected: boolean): void;
}

export interface ITemplate {
  templateName: string;
  templateType: string;
}
