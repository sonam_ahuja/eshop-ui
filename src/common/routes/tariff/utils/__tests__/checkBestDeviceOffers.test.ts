import checkBestDeviceOffers from '@tariff/utils/checkBestDeviceOffers';
import { IPlans } from '@tariff/store/types';
import appKeys from '@common/constants/appkeys';

const plans: IPlans = {
  name: '',
  id: '',
  totalPrices: [],
  allBenefits: [],
  prices: [],
  group: '',
  benefits: [],
  commonBenefits: [],
  characteristics: [
    {
      name: appKeys.TARIFF_BEST_DEVICE_OFFERS,
      values: []
    }
  ],
  isSelected: false
};

describe('getAllPlansValuesPlanSelectionOnly', () => {
  test('getAllPlansValuesPlanSelectionOnly', () => {
    expect(checkBestDeviceOffers(plans)).toEqual(checkBestDeviceOffers(plans));
  });
});
