import getAllPlansValues, {
  cardClickHandler,
  transformedValue
} from '@tariff/utils/getAllPlansValues';
import { histroyParams } from '@mocks/common/histroy';
import store from '@tariff/store/state';
import configuration from '@common/store/states/configuration';
import translation from '@common/store/states/translation';
import { IPlansProps } from '@tariff/TariffPlans';
import appConstants, {
  SHOW_ADDONS,
  SHOW_BENEFITS,
  SHOW_BUY_BUTTON,
  SHOW_PLAN_NAME,
  SHOW_UNLIMITED_BENEFITS
} from '@common/constants/appConstants';
import { plansProps } from '@src/common/mock-api/tariff/tariff.mock';

const translationState = translation();

// tslint:disable-next-line: no-big-function
describe('getAllPlansValues', () => {
  const props: IPlansProps = {
    ...histroyParams,
    confirmPlanBtnLoading: true,
    currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
    location: {
      pathname: '',
      search: '',
      hash: '',
      key: 'hmi41z',
      state: ''
    },
    commonError: {
      code: '',
      httpStatusCode: '',
      retryable: false
    },
    fetchCategories: jest.fn(),
    fetchTariff: jest.fn(),
    setDiscount: jest.fn(),
    setLoyalty: jest.fn(),
    showMagentaDiscount: jest.fn(),
    showAppShell: jest.fn(),
    setTariffId: jest.fn(),
    showTermAndConditions: jest.fn(),
    buyPlanOnly: jest.fn(),
    saveTariff: jest.fn(),
    toggleSelectedBenefit: jest.fn(),
    setCategoryId: jest.fn(),
    resetPlansData: jest.fn(),
    validateAgeLimit: jest.fn(),
    youngTariffCheck: jest.fn(),
    clickOnAddToBasket: jest.fn(),
    clickOnBuyPlanOnly: jest.fn(),
    ageLimitSuccess: jest.fn(),
    getTariffAddonPrice: jest.fn(),
    tariff: store(),
    tariffConfiguration: configuration().cms_configuration.modules.tariff,
    tariffTranslation: translationState.cart.tariff,
    globalTranslation: translationState.cart.global,
    selectedProductOfferingBenefit: {
      label: 'string',
      value: 'string',
      isSelected: false
    },
    selectedProductOfferingTerm: {
      label: '',
      value: ''
    },
    categories: {
      id: '',
      name: '',
      slug: '',
      image: '',
      backgroundImage: '',
      characteristics: [],
      description: '',
      parentSlug: null,
      highLightProduct: {
        name: '',
        value: ''
      },
      priority: 0,
      subCategories: []
    },
    planSelectionOnly: false,
    redirectToListingPage: () => null,
    addPlanToBasket: () => null,
    setMultiplePlansWrapRef: () => null,
    setMobileAccordionPanelRef: () => null
  };

  test('getAllPlansValues', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          id: '',
          group: '',
          totalPrices: [],
          prices: [],
          benefits: [],
          allBenefits: [],
          commonBenefits: [],
          characteristics: []
        },
        props,
        false,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });

  test('getAllPlansValues: SHOW_PLAN_NAME', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          id: '',
          group: '',
          totalPrices: [],
          prices: [
            {
              actualValue: 12000,
              discountedValue: 6000,
              priceType: '',
              discounts: []
            }
          ],
          benefits: [],
          allBenefits: [],
          commonBenefits: [],
          characteristics: [
            {
              name: SHOW_PLAN_NAME,
              values: [
                {
                  value: 'true'
                }
              ]
            }
          ]
        },
        props,
        false,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });

  test('getAllPlansValues: SHOW_BENEFITS', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          id: '',
          group: '',
          allBenefits: [],
          totalPrices: [],
          prices: [
            {
              actualValue: 12000,
              discountedValue: 6000,
              priceType: '',
              discounts: []
            }
          ],
          benefits: [],
          commonBenefits: [],
          characteristics: [
            {
              name: SHOW_BENEFITS,
              values: [
                {
                  value: 'true'
                }
              ]
            }
          ]
        },
        props,
        false,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });

  test('getAllPlansValues: SHOW_BUY_BUTTON', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          id: '',
          group: '',
          allBenefits: [],
          totalPrices: [],
          prices: [
            {
              actualValue: 12000,
              discountedValue: 6000,
              priceType: '',
              discounts: []
            }
          ],
          benefits: [],
          commonBenefits: [],
          characteristics: [
            {
              name: SHOW_BUY_BUTTON,
              values: [
                {
                  value: 'true'
                }
              ]
            }
          ]
        },
        props,
        false,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });

  test('getAllPlansValues: SHOW_ADDONS', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          id: '',
          group: '',
          allBenefits: [],
          totalPrices: [],
          prices: [
            {
              actualValue: 12000,
              discountedValue: 6000,
              priceType: '',
              discounts: []
            }
          ],
          benefits: [],
          commonBenefits: [],
          characteristics: [
            {
              name: SHOW_ADDONS,
              values: [
                {
                  value: 'true'
                }
              ]
            }
          ]
        },
        props,
        false,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });

  test('getAllPlansValues: SHOW_UNLIMITED_BENEFITS', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          group: '',
          id: '',
          totalPrices: [],
          allBenefits: [],
          prices: [
            {
              actualValue: 12000,
              discountedValue: 6000,
              priceType: '',
              discounts: []
            }
          ],
          benefits: [],
          commonBenefits: [],
          characteristics: [
            {
              name: SHOW_UNLIMITED_BENEFITS,
              values: [
                {
                  value: 'true'
                }
              ]
            }
          ]
        },
        props,
        true,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });

  test('getAllPlansValues: ALL', () => {
    expect(
      getAllPlansValues(
        {
          name: '',
          group: '',
          allBenefits: [],
          id: '',
          totalPrices: [],
          prices: [
            {
              actualValue: 12000,
              discountedValue: 6000,
              priceType: '',
              discounts: []
            }
          ],
          benefits: [
            {
              name: 'freeApp',
              label: '',
              values: [
                {
                  unit: '',
                  totalPrices: [],
                  prices: [
                    {
                      actualValue: 12000,
                      discountedValue: 6000,
                      priceType: '',
                      discounts: []
                    }
                  ],
                  value: '',
                  attachments: {}
                }
              ]
            }
          ],
          commonBenefits: [],
          characteristics: [
            {
              name: 'X',
              values: [
                {
                  value: 'false'
                }
              ]
            }
          ]
        },
        props,
        false,
        () => {
          //
        }
      )
    ).toMatchSnapshot();
  });
});

describe('transformedValue', () => {
  test('transformedValue: undefined', () => {
    expect(transformedValue(undefined, translationState.cart.tariff)).toEqual(
      ''
    );
  });
  test('transformedValue: UNLIMITED_TARIFF', () => {
    expect(
      transformedValue(
        appConstants.UNLIMITED_TARIFF,
        translationState.cart.tariff
      )
    ).toEqual(translationState.cart.tariff.unlimited);
  });
  test('transformedValue: else', () => {
    expect(transformedValue('someLabel', translationState.cart.tariff)).toEqual(
      'someLabel'
    );
  });

  test('cardClickHandler: else', () => {
    const result = cardClickHandler(plansProps, '12');
    expect(cardClickHandler(plansProps, '12')).toBe(result);
  });
  // tslint:disable-next-line:max-file-line-count
});
