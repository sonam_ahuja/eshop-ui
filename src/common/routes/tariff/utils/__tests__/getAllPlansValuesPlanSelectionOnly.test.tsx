import getAllPlansValuesPlanSelectionOnly, {
  cardClickHandler,
  transformedValue
} from '@tariff/utils/getAllPlansValuesPlanSelectionOnly';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import { IPlans } from '@tariff/store/types';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { histroyParams } from '@mocks/common/histroy';
import store from '@tariff/store/state';
import configuration from '@common/store/states/configuration';
import translation from '@common/store/states/translation';
import appConstants, {
  SHOW_ADDONS,
  SHOW_BENEFITS,
  SHOW_BUY_BUTTON,
  SHOW_PLAN_NAME,
  SHOW_UNLIMITED_BENEFITS
} from '@common/constants/appConstants';
import { plansProps } from '@src/common/mock-api/tariff/tariff.mock';

const translationState = translation();

const plan: IPlans = {
  name: '',
  id: '',
  group: '',
  allBenefits: [],
  totalPrices: [],
  prices: [],
  benefits: [],
  commonBenefits: [],
  characteristics: [],
  isSelected: false
};

const props: IPlansProps = {
  ...histroyParams,
  confirmPlanBtnLoading: true,
  currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
  location: {
    pathname: '',
    search: '',
    hash: '',
    key: 'hmi41z',
    state: ''
  },
  commonError: {
    code: '',
    httpStatusCode: '',
    retryable: false
  },
  fetchCategories: jest.fn(),
  fetchTariff: jest.fn(),
  setDiscount: jest.fn(),
  setLoyalty: jest.fn(),
  showMagentaDiscount: jest.fn(),
  showAppShell: jest.fn(),
  setTariffId: jest.fn(),
  showTermAndConditions: jest.fn(),
  buyPlanOnly: jest.fn(),
  saveTariff: jest.fn(),
  toggleSelectedBenefit: jest.fn(),
  setCategoryId: jest.fn(),
  resetPlansData: jest.fn(),
  validateAgeLimit: jest.fn(),
  youngTariffCheck: jest.fn(),
  clickOnAddToBasket: jest.fn(),
  clickOnBuyPlanOnly: jest.fn(),
  ageLimitSuccess: jest.fn(),
  getTariffAddonPrice: jest.fn(),
  tariff: store(),
  tariffConfiguration: configuration().cms_configuration.modules.tariff,
  tariffTranslation: translation().cart.tariff,
  globalTranslation: translation().cart.global,
  selectedProductOfferingBenefit: {
    label: 'string',
    value: 'string',
    isSelected: false
  },
  selectedProductOfferingTerm: {
    label: '',
    value: ''
  },
  categories: {
    id: '',
    name: '',
    slug: '',
    image: '',
    backgroundImage: '',
    characteristics: [],
    description: '',
    parentSlug: null,
    highLightProduct: {
      name: '',
      value: ''
    },
    priority: 0,
    subCategories: []
  },
  planSelectionOnly: false,
  redirectToListingPage: () => null,
  addPlanToBasket: () => null,
  setMultiplePlansWrapRef: () => null,
  setMobileAccordionPanelRef: () => null
};

describe('getAllPlansValuesPlanSelectionOnly', () => {
  test('getAllPlansValuesPlanSelectionOnly', () => {
    expect(
      getAllPlansValuesPlanSelectionOnly(plan, props, false)
    ).toMatchSnapshot();
  });
});

describe('getCharactersticsConfig', () => {
  test('getCharactersticsConfig', () => {
    const characteristics = [
      {
        name: SHOW_ADDONS,
        values: [
          {
            value: 'true'
          }
        ]
      },
      {
        name: SHOW_ADDONS,
        values: [
          {
            value: 'false'
          }
        ]
      },
      {
        name: SHOW_PLAN_NAME,
        values: [
          {
            value: 'false'
          }
        ]
      },
      {
        name: SHOW_BENEFITS,
        values: [
          {
            value: 'false'
          }
        ]
      },
      {
        name: SHOW_ADDONS,
        values: [
          {
            value: 'false'
          }
        ]
      },
      {
        name: SHOW_UNLIMITED_BENEFITS,
        values: [
          {
            value: 'false'
          }
        ]
      },
      {
        name: SHOW_BUY_BUTTON,
        values: [
          {
            value: 'true'
          }
        ]
      }
    ];
    expect(getCharactersticsConfig(characteristics)).toMatchSnapshot();
  });
});

describe('transformedValue', () => {
  test('transformedValue: undefined', () => {
    expect(transformedValue(undefined, translationState.cart.tariff)).toEqual(
      ''
    );
  });
  test('transformedValue: UNLIMITED_TARIFF', () => {
    expect(
      transformedValue(
        appConstants.UNLIMITED_TARIFF,
        translationState.cart.tariff
      )
    ).toEqual(translationState.cart.tariff.unlimited);
  });
  test('transformedValue: else', () => {
    expect(transformedValue('someLabel', translationState.cart.tariff)).toEqual(
      'someLabel'
    );
  });

  test('cardClickHandler: else', () => {
    const result = cardClickHandler(plansProps, '12');
    expect(cardClickHandler(plansProps, '12')).toBe(result);
  });
});
