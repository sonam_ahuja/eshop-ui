import getMobileList from '@tariff/utils/getMobileList';
import { IPlans } from '@tariff/store/types';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { histroyParams } from '@mocks/common/histroy';
import store from '@tariff/store/state';
import configuration from '@common/store/states/configuration';
import translation from '@common/store/states/translation';

const plan: IPlans = {
  name: '',
  id: '',
  prices: [],
  benefits: [],
  allBenefits: [],
  totalPrices: [],
  group: '',
  commonBenefits: [],
  characteristics: [],
  isSelected: false
};

const props: IPlansProps = {
  ...histroyParams,
  getTariffAddonPrice: jest.fn(),
  confirmPlanBtnLoading: true,
  currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
  location: {
    pathname: '',
    search: '',
    hash: '',
    key: 'hmi41z',
    state: ''
  },
  commonError: {
    code: '',
    httpStatusCode: '',
    retryable: false
  },
  fetchCategories: jest.fn(),
  fetchTariff: jest.fn(),
  setDiscount: jest.fn(),
  setLoyalty: jest.fn(),
  showMagentaDiscount: jest.fn(),
  showAppShell: jest.fn(),
  setTariffId: jest.fn(),
  showTermAndConditions: jest.fn(),
  buyPlanOnly: jest.fn(),
  saveTariff: jest.fn(),
  toggleSelectedBenefit: jest.fn(),
  setCategoryId: jest.fn(),
  resetPlansData: jest.fn(),
  validateAgeLimit: jest.fn(),
  youngTariffCheck: jest.fn(),
  clickOnAddToBasket: jest.fn(),
  clickOnBuyPlanOnly: jest.fn(),
  ageLimitSuccess: jest.fn(),
  tariff: store(),
  tariffConfiguration: configuration().cms_configuration.modules.tariff,
  tariffTranslation: translation().cart.tariff,
  globalTranslation: translation().cart.global,
  selectedProductOfferingBenefit: {
    label: 'string',
    value: 'string',
    isSelected: false
  },
  selectedProductOfferingTerm: {
    label: '',
    value: ''
  },
  categories: {
    id: '',
    name: '',
    slug: '',
    image: '',
    backgroundImage: '',
    characteristics: [],
    description: '',
    parentSlug: null,
    highLightProduct: {
      name: '',
      value: ''
    },
    priority: 0,
    subCategories: []
  },
  planSelectionOnly: false,
  redirectToListingPage: () => null,
  addPlanToBasket: () => null,
  setMultiplePlansWrapRef: () => null,
  setMobileAccordionPanelRef: () => null
};

describe('getMobileList', () => {
  test('getMobileList', () => {
    expect(getMobileList(plan, props, false)).toMatchSnapshot();
  });
  test('getMobileList: benefits', () => {
    expect(
      getMobileList(
        {
          ...plan,
          benefits: [
            {
              name: 'freeApp',
              label: 'x',
              values: [
                {
                  unit: null,
                  value: 'string',
                  label: 'string',
                  isSelected: true,
                  totalPrices: [],
                  prices: [],
                  attachments: {
                    thumbnail: [
                      {
                        url: 'string',
                        name: 'string',
                        type: 'string'
                      }
                    ]
                  }
                }
              ]
            }
          ]
        },
        props,
        false
      )
    ).toMatchSnapshot();
  });
});
