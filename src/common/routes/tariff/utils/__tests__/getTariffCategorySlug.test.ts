import getTariffCategorySlug from '@tariff/utils/getTariffCategorySlug';

describe('getTariffCategorySlug', () => {
  test('getTariffCategorySlug: IF', () => {
    expect(getTariffCategorySlug('/tariff/tariffId')).toEqual(
      getTariffCategorySlug('/tariff/tariffId')
    );
  });
  test('getTariffCategorySlug: Else', () => {
    expect(getTariffCategorySlug('/')).toEqual(getTariffCategorySlug('/'));
  });
});
