import { default as getAllPlansValues } from '@tariff/utils/getAllPlansValues';
import { default as getAllPlansValuesPlanSelectionOnly } from '@tariff/utils/getAllPlansValuesPlanSelectionOnly';
import { default as getTariffCategorySlug } from '@tariff/utils/getTariffCategorySlug';
import { default as getAllPlansTitles } from '@tariff/utils/getAllPlansTitles';
import { default as LandingPageWrapper } from '@tariff/utils/styles';
import { default as checkBestDeviceOffers } from '@tariff/utils/checkBestDeviceOffers';
import { default as composeTariffRequestPayload } from '@tariff/utils/composeTariffRequestPayload';
import { default as getProductOfferingTermAndBenefit } from '@tariff/utils/getProductOfferingTermAndBenefit';

describe('getAllPlansValues', () => {
  test('modules', () => {
    expect(getAllPlansValues).toBeDefined();
    expect(getAllPlansValuesPlanSelectionOnly).toBeDefined();
    expect(getTariffCategorySlug).toBeDefined();
    expect(getAllPlansTitles).toBeDefined();
    expect(LandingPageWrapper).toBeDefined();
    expect(checkBestDeviceOffers).toBeDefined();
    expect(composeTariffRequestPayload).toBeDefined();
    expect(getProductOfferingTermAndBenefit).toBeDefined();
  });
});
