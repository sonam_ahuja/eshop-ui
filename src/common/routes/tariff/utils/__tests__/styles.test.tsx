import React from 'react';
import LandingPageWrapper from '@tariff/utils/styles';
import { mount } from 'enzyme';

describe('getMobileList', () => {
  test('primary', () => {
    expect(
      mount(<LandingPageWrapper backgroundImage='' theme='primary' />)
    ).toMatchSnapshot();
  });
  // tslint:disable-next-line:no-identical-functions
  test('secondary', () => {
    expect(
      mount(<LandingPageWrapper backgroundImage='' theme='secondary' />)
    ).toMatchSnapshot();
  });
});
