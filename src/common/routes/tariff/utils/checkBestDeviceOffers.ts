import { ICharacteristic, IPlans } from '@tariff/store/types';
import appKeys from '@common/constants/appkeys';

export default function checkBestDeviceOffers(plan: IPlans): boolean {
  return !!plan.characteristics.filter(
    (characteristic: ICharacteristic) =>
      characteristic.name === appKeys.TARIFF_BEST_DEVICE_OFFERS
  ).length;
}
