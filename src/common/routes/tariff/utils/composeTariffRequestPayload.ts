import { IProps } from '@tariff/index';
import { ITariffRequest } from '@tariff/store/types';
import APP_CONSTANTS from '@common/constants/appConstants';

export default function composeTariffRequestPayload(
  props: IProps
): ITariffRequest {
  const {
    selectedProductOfferingTerm,
    selectedProductOfferingBenefit
  } = props.tariff;

  return {
    category: 'tariffMobilePostpaid',
    channel: APP_CONSTANTS.CHANNEL,
    selectedAttributes: [
      {
        name: selectedProductOfferingTerm.label,
        value: selectedProductOfferingTerm.value
      },
      {
        name: selectedProductOfferingBenefit.label,
        value: selectedProductOfferingBenefit.value
      }
    ]
  };
}
