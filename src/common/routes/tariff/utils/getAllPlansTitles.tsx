// Move this file to common folder

import React, { ReactNode } from 'react';
import { IProps } from '@tariff/index';
import { ITitleType } from '@tariff/store/types';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { Anchor } from 'dt-components';
import EVENT_NAME from '@events/constants/eventName';
import { isMobile } from '@src/common/utils';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';
import appConstants from '@src/common/constants/appConstants';

export default function getAllPlansTitles(
  props: IPlansProps,
  viewMore: boolean,
  onClickHandler: () => void,
  forceShowAllTitles: boolean = false
): ReactNode {
  const { tariffTranslation, tariff, tariffConfiguration } = props;
  const config = getCharactersticsConfig(tariff.categories.characteristics);

  // make sure if planSelectionOnly is set as true do not display view more / view less link
  // also this check will toggle design between plan & pricePerMonth
  let headerLabel = '';
  // tslint:disable-next-line:prefer-conditional-expression
  if (
    tariffConfiguration.tariffListingTemplate ===
    TARIFF_LIST_TEMPLATE.DATA_FIRST
  ) {
    headerLabel = tariffTranslation.data;
  } else if (
    props.planSelectionOnly ||
    tariffConfiguration.tariffListingTemplate ===
      TARIFF_LIST_TEMPLATE.NAME_FIRST
  ) {
    headerLabel = tariffTranslation.plan;
  } else {
    headerLabel = tariffTranslation.pricePerMonth;
  }

  return (
    <div className='aside-panel'>
      <header className='plans-header clearfix'>
        <div className='title-wrap'>
          <h3>{headerLabel}</h3>
        </div>
      </header>
      <div className='plans-body-wrap clearfix'>
        <ul className='plans-box list-inline clearfix'>
          {getLabels(props, viewMore, forceShowAllTitles)}
        </ul>
        <div className='btn-wrapper clearfix lists'>
          {tariff.commonTitles.length > 0 &&
          !props.planSelectionOnly &&
          config.showUnlimitedBenefits &&
          config.showBenefits ? (
            <Anchor
              className='linker'
              underline={false}
              onClickHandler={onClickHandler}
              hreflang={appConstants.LANGUAGE_CODE}
              title={
                viewMore
                  ? tariffTranslation.viewLess
                  : tariffTranslation.viewMore
              }
              data-event-id={EVENT_NAME.TARIFF.EVENTS.VIEW_ACTION_BUTTON}
              data-event-message={
                viewMore
                  ? tariffTranslation.viewLess
                  : tariffTranslation.viewMore
              }
              data-event-path={
                viewMore ? 'cart.tariff.viewLess' : 'cart.tariff.viewMore'
              }
            >
              {viewMore
                ? tariffTranslation.viewLess
                : tariffTranslation.viewMore}
            </Anchor>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export function getLabels(
  props: IProps,
  viewMore: boolean,
  forceShowAllTitles: boolean = false
): ReactNode {
  const { tariff, tariffConfiguration, tariffTranslation } = props;

  const config = getCharactersticsConfig(tariff.categories.characteristics);
  let title = config.showBenefits ? [...tariff.titles] : [];
  const commonTitle = config.showUnlimitedBenefits
    ? [...tariff.commonTitles]
    : [];

  if (viewMore || forceShowAllTitles) {
    title = title.concat(commonTitle);
  }
  const listSlice =
    tariffConfiguration.tariffListingTemplate ===
    TARIFF_LIST_TEMPLATE.DATA_FIRST
      ? 2
      : 3;

  if ((isMobile.phone || isMobile.tablet) && !forceShowAllTitles) {
    title = title.slice(0, listSlice);
  }

  if (
    tariffConfiguration.tariffListingTemplate ===
      TARIFF_LIST_TEMPLATE.PRICE_FIRST &&
    isMobile.phone &&
    config.showPlanName &&
    !forceShowAllTitles
  ) {
    title.unshift({
      title: tariffTranslation.plan,
      name: 'plan',
      type: 'plan'
    });
  }

  return title
    .filter(titleItem => {
      if (!config.showAddons) {
        return titleItem.type !== 'addon';
      }

      return true;
    })
    .map((label: ITitleType) => {
      return (
        <li key={label.title} className='lists'>
          <h3 className='titles' title={label.title}>
            {label.title}
          </h3>
        </li>
      );
    });
}
