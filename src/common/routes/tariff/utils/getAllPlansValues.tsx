// Move this file to common folder

import EVENT_NAME from '@events/constants/eventName';
import { isMobile } from '@common/utils';
import { Button, Icon } from 'dt-components';
import { ITariffTranslation } from '@common/store/types/translation';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { IBenefits, IPlans } from '@tariff/store/types';
import React, { ReactNode } from 'react';
import { TARIFF_BENEFIT_TEMPLATES } from '@common/constants/appkeys';
import TariffPrice from '@tariff/TariffPlans/Common/TariffPrice';
import { formatCurrency } from '@common/utils/currency';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import appConstants from '@common/constants/appConstants';
import ButtonTemplate from '@tariff/TariffPlans/Plans/templates/button';
import DropdownTemplate from '@tariff/TariffPlans/Plans/templates/dropdown';
import ImageTemplate from '@tariff/TariffPlans/Plans/templates/image';
import cx from 'classnames';
import getIconOrLabel from '@tariff/common/getIconOrLabel';
import {
  getCharactersticsConfig,
  getHDCharacterstics
} from '@tariff/utils/getCharactersticsConfig';
import { emptyPriceDecoration } from '@tariff/utils';

import checkBestDeviceOffers from './checkBestDeviceOffers';
import getTemplate from './getTemplate';
import getRecurringType from './getRecurringType';

export const cardClickHandler = (props: IPlansProps, tariffId: string) => {
  props.setTariffId(tariffId);
  if (!props.planSelectionOnly) {
    props.saveTariff();
  }
};

// tslint:disable:cyclomatic-complexity
// tslint:disable: cognitive-complexity
// tslint:disable-next-line:no-big-function
export default function getAllPlansValues(
  plan: IPlans,
  props: IPlansProps,
  viewMore: boolean,
  setPlanNameRef: (ref: HTMLElement | null) => void
): ReactNode {
  const {
    tariffConfiguration,
    toggleSelectedBenefit,
    tariffTranslation,
    addPlanToBasket,
    redirectToListingPage,
    categories,
    currency,
    confirmPlanBtnLoading
  } = props;
  const { id, benefits, commonBenefits, totalPrices } = plan;
  const price = totalPrices && totalPrices[0];

  if (!price) {
    return;
  }
  const recurringText = getRecurringType(plan, tariffTranslation);
  const emptyDiscountDecoration = emptyPriceDecoration(props.tariff.plans);
  const config = getCharactersticsConfig(categories.characteristics);
  let hdEnabled = null;
  if (plan.characteristics && plan.characteristics.length) {
    hdEnabled = getHDCharacterstics(plan.characteristics);
  }
  let listEl = benefits.map((benefit: IBenefits, index: number) => {
    const dataClassNames = cx('data-text', {
      'empty-value':
        benefit &&
        benefit.values &&
        benefit.values[0] &&
        !benefit.values[0].label
    });
    const template = getTemplate(tariffConfiguration, benefit.name);

    const templateProps = {
      tariffValues: benefit.values,
      tariffConfiguration,
      toggleBenefitSelection: (benefitValue: string, newSelected: boolean) => {
        props.setTariffId(plan.id);
        toggleSelectedBenefit(
          id,
          benefit.name,
          benefitValue,
          template.templateType,
          newSelected
        );
        // API call to fetch new prices for a tariff -> for a desktop
        props.getTariffAddonPrice(plan.id);
      }
    };

    // to identify addons isSelectable needs to be true
    if (!config.showAddons && benefit.isSelectable) {
      // TODO: need to check for showAddons Case
      return null;
      // return (
      //   <li key={index} className='lists'>
      //     <span className='data-text empty-value' />
      //   </li>
      // );
    }
    // else in other cases isSelectable is false, we will simply render on ui list items
    if (!benefit.isSelectable) {
      return (
        <li key={index} className='lists'>
          <span className={dataClassNames}>
            <strong>{getIconOrLabel(benefit)}</strong>
          </span>
        </li>
      );
    }
    // for addons we need to render according to their templates
    switch (template.templateName) {
      case TARIFF_BENEFIT_TEMPLATES.BUTTON: {
        if (isMobile.phone || isMobile.tablet) {
          return <DropdownTemplate {...templateProps} useImage={false} />;
        }

        return <ButtonTemplate {...templateProps} />;
      }
      case TARIFF_BENEFIT_TEMPLATES.DROPDOWN: {
        return <DropdownTemplate {...templateProps} useImage={false} />;
      }
      case TARIFF_BENEFIT_TEMPLATES.IMAGE: {
        if (isMobile.phone || isMobile.tablet) {
          return <DropdownTemplate {...templateProps} useImage={true} />;
        }

        return <ImageTemplate {...templateProps} />;
      }
      default: {
        return (
          <li key={index} className='lists'>
            <span className={dataClassNames}>
              <h4>{getIconOrLabel(benefit)}</h4>
            </span>
          </li>
        );
      }
    }
  });

  listEl = config.showBenefits ? listEl : [];

  if (viewMore) {
    const commonEl = config.showUnlimitedBenefits
      ? commonBenefits.map((benefit: IBenefits, index: number) => {
          return (
            <li key={index} className='lists'>
              <span className='data-text'>{getIconOrLabel(benefit)}</span>
            </li>
          );
        })
      : [];
    listEl = listEl.concat(commonEl);
  }

  const bestOfferIconEl = checkBestDeviceOffers(plan) ? (
    <div className='icon-wrap special-offer'>
      <Icon size='inherit' name='ec-special-offer' />
    </div>
  ) : null;

  return (
    <div
      className={`flex-panel ${
        id === props.tariff.selectedTariffId ? 'active' : ''
      }`}
      onClick={() => cardClickHandler(props, id)}
    >
      <header className='plans-header clearfix'>
        {tariffConfiguration.tariffListingTemplate ===
          TARIFF_LIST_TEMPLATE.PRICE_FIRST && (
          <div className='title-wrap price-first-wrap' ref={setPlanNameRef}>
            {config.showPrice && (
              <TariffPrice
                totalPrices={plan.totalPrices}
                currency={currency}
                reverseOrder={true}
                priceFirst={true}
                hideActualPriceCurrency={true}
                showEmptySpace={emptyDiscountDecoration}
              />
            )}
            {config.showPlanName && (
              <span
                className='titles multipleTruncateEllipsis'
                title={plan.name}
              >
                {plan.name}
              </span>
            )}
          </div>
        )}
        {tariffConfiguration.tariffListingTemplate ===
          TARIFF_LIST_TEMPLATE.DATA_FIRST && (
          <div className='title-wrap data-first-wrap' ref={setPlanNameRef}>
            <h2 className='price-text'>
              {config.showBenefits && plan.data && plan.data.values[0].label}{' '}
              {hdEnabled &&
              hdEnabled[0] &&
              hdEnabled[0].values &&
              hdEnabled[0].values[0] &&
              hdEnabled[0].values[0].value === 'true' ? (
                <span className='tittle-hd'>{tariffTranslation.hdData}</span>
              ) : null}
            </h2>
            {config.showPrice &&
            price.discounts &&
            price.discounts.length !== 0 ? (
              <span className='modify-text'>
                <span className='value'>
                  {formatCurrency(price.actualValue, currency.locale)}
                  {recurringText}
                </span>
              </span>
            ) : (
              emptyDiscountDecoration && (
                <span className='modify-text'>
                  <span className='value'>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </span>
              )
            )}
            <span className='titles'>
              {config.showPrice &&
                formatCurrency(price.discountedValue, currency.locale)}
              {config.showPrice && recurringText}
            </span>
            {config.showPlanName && (
              <span
                className={
                  'data-first-plan-name data-text multipleTruncateEllipsis'
                }
                title={plan.name}
              >
                {plan.name}
              </span>
            )}
          </div>
        )}
        {tariffConfiguration.tariffListingTemplate ===
          TARIFF_LIST_TEMPLATE.NAME_FIRST && (
          <div className='title-wrap name-first-wrap' ref={setPlanNameRef}>
            {config.showPlanName && (
              <h2
                className='price-text multipleTruncateEllipsis'
                title={plan.name}
              >
                {plan.name}
              </h2>
            )}
            {config.showPrice &&
            price.discounts &&
            price.discounts.length !== 0 ? (
              <span className='modify-text'>
                <span className='value'>
                  {formatCurrency(price.actualValue, currency.locale)}
                  {recurringText}
                </span>
              </span>
            ) : (
              emptyDiscountDecoration && (
                <span className='modify-text'>
                  <span className='value'>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </span>
              )
            )}
            <span className='titles'>
              {config.showPrice &&
                formatCurrency(price.discountedValue, currency.locale)}
              {config.showPrice && recurringText}
            </span>
          </div>
        )}
        {bestOfferIconEl}
      </header>
      <div className='plans-body-wrap clearfix'>
        <ul className='plans-box list-inline clearfix'>{listEl}</ul>
        <div className='btn-wrapper lists'>
          {props.planSelectionOnly ? (
            <Button
              size='small'
              loading={
                confirmPlanBtnLoading && id === props.tariff.selectedTariffId
              }
              className='btn btn-primary'
              onClickHandler={event => {
                event.stopPropagation();
                redirectToListingPage(plan.id);
              }}
              data-event-id={EVENT_NAME.TARIFF.EVENTS.CONFIRM_PLAN}
              data-event-message={tariffTranslation.confirmPlan}
              type={'complementary'}
            >
              {tariffTranslation.confirmPlan}
            </Button>
          ) : null}
          {!props.planSelectionOnly && config.showAddButton ? (
            <Button
              size='small'
              className='btn btn-primary'
              onClickHandler={event => {
                event.stopPropagation();
                redirectToListingPage(plan.id);
              }}
              data-event-id={EVENT_NAME.TARIFF.EVENTS.ADD_TO_PHONE}
              data-event-message={tariffTranslation.addAPhone}
            >
              {tariffTranslation.addAPhone}
            </Button>
          ) : null}
          {!props.planSelectionOnly && config.showBuyButton ? (
            <Button
              size='small'
              className={
                config.showAddButton ? 'btn btn-line' : 'btn btn-primary'
              }
              onClickHandler={event => {
                event.stopPropagation();
                addPlanToBasket(plan.id);
              }}
              data-event-id={EVENT_NAME.TARIFF.EVENTS.BUY_PLAN}
              data-event-message={tariffTranslation.buyPlanOnly}
            >
              {tariffTranslation.buyPlanOnly}
            </Button>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export function transformedValue(
  label: string | undefined,
  tariffTranslation: ITariffTranslation
): string {
  if (label === undefined) {
    return '';
  }

  return label === appConstants.UNLIMITED_TARIFF
    ? tariffTranslation.unlimited
    : label;
}
// tslint:disable-next-line:max-file-line-count
