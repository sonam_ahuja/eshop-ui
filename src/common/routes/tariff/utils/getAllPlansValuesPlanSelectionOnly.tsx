// Move this file to common folder

import { formatCurrency } from '@common/utils/currency';
import { isMobile } from '@common/utils';
import { Button, Icon } from 'dt-components';
import { ITariffTranslation } from '@common/store/types/translation';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { IBenefits, IPlans } from '@tariff/store/types';
import React, { ReactNode } from 'react';
import { TARIFF_BENEFIT_TEMPLATES } from '@common/constants/appkeys';
import TariffPrice from '@tariff/TariffPlans/Common/TariffPrice';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import appConstants from '@common/constants/appConstants';
import {
  getCharactersticsConfig,
  getHDCharacterstics
} from '@tariff/utils/getCharactersticsConfig';
import ButtonTemplate from '@tariff/TariffPlans/Plans/templates/button';
import DropdownTemplate from '@tariff/TariffPlans/Plans/templates/dropdown';
import ImageTemplate from '@tariff/TariffPlans/Plans/templates/image';
import cx from 'classnames';
import getIconOrLabel from '@tariff/common/getIconOrLabel';

import checkBestDeviceOffers from './checkBestDeviceOffers';
import emptyPriceDecoration from './priceDecoration';
import getTemplate from './getTemplate';

export const cardClickHandler = (props: IPlansProps, tariffId: string) => {
  props.setTariffId(tariffId);
  if (!props.planSelectionOnly) {
    props.saveTariff();
  }
};

// tslint:disable: cognitive-complexity
// tslint:disable-next-line:no-big-function
export default function getAllPlansValuesPlanSelectionOnly(
  plan: IPlans,
  props: IPlansProps,
  viewMore: boolean
): ReactNode {
  const {
    tariffConfiguration,
    toggleSelectedBenefit,
    tariffTranslation,
    addPlanToBasket,
    redirectToListingPage,
    categories,
    confirmPlanBtnLoading,
    currency
  } = props;
  const {  id, benefits, commonBenefits, totalPrices } = plan;
  const price = totalPrices && totalPrices[0];

  if (!price) {
    return;
  }
  const emptyDiscountDecoration = emptyPriceDecoration(props.tariff.plans);
  const config = getCharactersticsConfig(categories.characteristics);
  let hdEnabled = null;
  if (plan.characteristics && plan.characteristics.length) {
    hdEnabled = getHDCharacterstics(plan.characteristics);
  }
  let listEl = benefits.map((benefit: IBenefits, index: number) => {
    const dataClassNames = cx('data-text', {
      'empty-value': !benefit.values[0].label
    });
    const template = getTemplate(tariffConfiguration, benefit.name);

    const templateProps = {
      tariffValues: benefit.values,
      tariffConfiguration,
      toggleBenefitSelection: (benefitValue: string, newSelected: boolean) => {
        toggleSelectedBenefit(
          id,
          benefit.name,
          benefitValue,
          template.templateType,
          newSelected
        );
        // API call to fetch new prices for a tariff -> for a mobile in device detailed
        props.getTariffAddonPrice(plan.id);
      }
    };

    // to identify addons isSelectable needs to be true
    if (!config.showAddons && benefit.isSelectable) {
      // TODO: Test for showAddons hidden
      // return (
      //   <li key={index} className='lists'>
      //     <span className='data-text empty-value' />
      //   </li>
      // );

      return null;
    }
    // else in other cases isSelectable is false, we will simply render on ui list items
    if (!benefit.isSelectable) {
      return (
        <li key={index} className='lists'>
          <span className={dataClassNames}>
            <strong>{getIconOrLabel(benefit)}</strong>
          </span>
        </li>
      );
    }
    // for addons we need to render according to their templates
    switch (template.templateName) {
      case TARIFF_BENEFIT_TEMPLATES.BUTTON: {
        if (isMobile.phone || isMobile.tablet) {
          return <DropdownTemplate {...templateProps} useImage={false} />;
        }

        return <ButtonTemplate {...templateProps} />;
      }
      case TARIFF_BENEFIT_TEMPLATES.DROPDOWN: {
        return <DropdownTemplate {...templateProps} useImage={false} />;
      }
      case TARIFF_BENEFIT_TEMPLATES.IMAGE: {
        if (isMobile.phone || isMobile.tablet) {
          return <DropdownTemplate {...templateProps} useImage={true} />;
        }

        return <ImageTemplate {...templateProps} />;
      }
      default: {
        return (
          <li key={index} className='lists'>
            <span className={dataClassNames}>
              <strong>{getIconOrLabel(benefit)}</strong>
            </span>
          </li>
        );
      }
    }
  });
  listEl = config.showBenefits ? listEl : [];
  if (viewMore) {
    const commonEl = config.showUnlimitedBenefits
      ? commonBenefits.map((benefit: IBenefits, index: number) => {
          return (
            <li key={index} className='lists'>
              <span className='data-text'>{getIconOrLabel(benefit)}</span>
            </li>
          );
        })
      : [];
    listEl = listEl.concat(commonEl);
  }

  const bestOfferIconEl = checkBestDeviceOffers(plan) ? (
    <div className='icon-wrap special-offer'>
      <Icon size='inherit' name='ec-special-offer' />
    </div>
  ) : null;

  return (
    <div
      className={`flex-panel ${
        id === props.tariff.selectedTariffId ? 'active' : ''
      }`}
      onClick={() => cardClickHandler(props, id)}
    >
      <header className='plans-header clearfix'>
        {tariffConfiguration.tariffListingTemplate ===
          TARIFF_LIST_TEMPLATE.PRICE_FIRST && (
          <div className='title-wrap price-first-wrap'>
            {config.showPrice && (
              <TariffPrice
                totalPrices={plan.totalPrices}
                reverseOrder={true}
                priceFirst={true}
                currency={currency}
                hideActualPriceCurrency={true}
              />
            )}
            {config.showPlanName && (
              <span className='titles' title={plan.name}>
                {plan.name}
              </span>
            )}
          </div>
        )}
        {tariffConfiguration.tariffListingTemplate ===
          TARIFF_LIST_TEMPLATE.DATA_FIRST && (
          <div className='title-wrap data-first-wrap'>
            <span className='price-text'>
              {config.showBenefits && plan.data && plan.data.values[0].label}
              {hdEnabled &&
              hdEnabled[0] &&
              hdEnabled[0].values &&
              hdEnabled[0].values[0] &&
              hdEnabled[0].values[0].value === 'true' ? (
                <span className='tittle-hd'>{tariffTranslation.hdData}</span>
              ) : null}
            </span>
            <span className='titles'>
              {config.showPrice &&
                formatCurrency(price.discountedValue, currency.locale)}
            </span>
            {config.showPlanName && (
              <span
                className={'data-first-plan-name data-text'}
                title={plan.name}
              >
                {plan.name}
              </span>
            )}
          </div>
        )}
        {tariffConfiguration.tariffListingTemplate ===
          TARIFF_LIST_TEMPLATE.NAME_FIRST && (
          <div className='title-wrap name-first-wrap'>
            {config.showPlanName && (
              <span className='price-text' title={plan.name}>
                {plan.name}
              </span>
            )}
            {config.showPrice &&
            price.discounts &&
            price.discounts.length !== 0 ? (
              <span className='modify-text'>
                <span className='value'>{price.actualValue}</span>
              </span>
            ) : (
              emptyDiscountDecoration && (
                <span className='modify-text'>
                  <span className='value'>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </span>
              )
            )}
            <span className='titles'>
              {config.showPrice &&
                formatCurrency(price.discountedValue, currency.locale)}
            </span>
          </div>
        )}
        {bestOfferIconEl}
      </header>
      <div className='plans-body-wrap clearfix'>
        <ul className='plans-box list-inline clearfix'>{listEl}</ul>
        <div className='btn-wrapper'>
          {props.planSelectionOnly ? (
            <Button
              size='small'
              className='btn btn-primary'
              loading={confirmPlanBtnLoading}
              onClickHandler={() => redirectToListingPage(plan.id)}
              type={'complementary'}
            >
              {tariffTranslation.confirmPlan}
            </Button>
          ) : null}
          {!props.planSelectionOnly ? (
            <Button
              size='small'
              className='btn btn-primary'
              onClickHandler={() => redirectToListingPage(plan.id)}
            >
              {tariffTranslation.addAPhone}
            </Button>
          ) : null}
          {!props.planSelectionOnly && config.showBuyButton ? (
            <Button
              size='small'
              className='btn btn-line'
              onClickHandler={() => addPlanToBasket(plan.id)}
            >
              {tariffTranslation.buyPlanOnly}
            </Button>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export function transformedValue(
  label: string | undefined,
  tariffTranslation: ITariffTranslation
): string {
  if (label === undefined) {
    return '';
  }

  return label === appConstants.UNLIMITED_TARIFF
    ? tariffTranslation.unlimited
    : label;
}
// tslint:disable-next-line:max-file-line-count
