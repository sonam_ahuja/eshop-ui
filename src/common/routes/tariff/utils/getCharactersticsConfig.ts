import { ICharacteristic, ICharacteristicConfig } from '@tariff/store/types';
import {
  SHOW_ADD_PHONE_BUTTON,
  SHOW_ADDONS,
  SHOW_BENEFITS,
  SHOW_BUY_BUTTON,
  SHOW_MONTHLY_PRICE,
  SHOW_PLAN_NAME,
  SHOW_UNLIMITED_BENEFITS
} from '@common/constants/appConstants';
import htmlKeys from '@common/constants/appkeys';

// tslint:disable:no-any
export function getCharactersticsConfig(
  characteristics: ICharacteristic[]
): ICharacteristicConfig {
  const obj = {
    showPlanName: true,
    showBenefits: true,
    showBuyButton: true,
    showAddons: true,
    showUnlimitedBenefits: true,
    showPrice: true,
    showAddButton: true
  };

  characteristics.forEach((charecterstic: ICharacteristic) => {
    const { name, values } = charecterstic;
    // tslint:disable-next-line
    const value = values[0].value === 'false' ? false : true;

    if (name === SHOW_PLAN_NAME) {
      obj.showPlanName = value;
    } else if (name === SHOW_BENEFITS) {
      obj.showBenefits = value;
    } else if (name === SHOW_BUY_BUTTON) {
      obj.showBuyButton = value;
    } else if (name === SHOW_ADDONS) {
      obj.showAddons = value;
    } else if (name === SHOW_UNLIMITED_BENEFITS) {
      obj.showUnlimitedBenefits = value;
    } else if (name === SHOW_MONTHLY_PRICE) {
      obj.showPrice = value;
    } else if (name === SHOW_ADD_PHONE_BUTTON) {
      obj.showAddButton = value;
    }
  });

  return obj;
}

export const getHDCharacterstics = (characteristics: ICharacteristic[]) => {
  return characteristics.filter((chara: ICharacteristic) => {
    return chara.name === htmlKeys.TARIFF_HD_ENABLED;
  });
};
