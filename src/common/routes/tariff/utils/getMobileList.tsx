import { isMobile } from '@src/common/utils';
import React, { ReactNode } from 'react';
import { IPlansProps } from '@tariff/TariffPlans/index';
import { TARIFF_BENEFIT_TEMPLATES } from '@common/constants/appkeys';
import { IBenefits, IPlans } from '@tariff/store/types';
import { TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import TariffPrice from '@tariff/TariffPlans/Common/TariffPrice';
import ButtonTemplate from '@tariff/TariffPlans/Plans/templates/button';
import DropdownTemplate from '@tariff/TariffPlans/Plans/templates/dropdown';
import ImageTemplate from '@tariff/TariffPlans/Plans/templates/image';
import cx from 'classnames';
import getIconOrLabel from '@tariff/common/getIconOrLabel';
import { getCharactersticsConfig } from '@tariff/utils/getCharactersticsConfig';

import getTemplate from './getTemplate';

// tslint:disable-next-line:cognitive-complexity
export default function getListElements(
  plan: IPlans,
  props: IPlansProps,
  viewMore: boolean
): ReactNode {
  const {
    tariffConfiguration,
    toggleSelectedBenefit,
    categories,
    currency
  } = props;
  const { benefits, commonBenefits, id } = plan;
  // @
  // const price = plan.prices[0];
  const config = getCharactersticsConfig(categories.characteristics);

  let listEl = benefits.map((benefit: IBenefits, index: number) => {
    const { label } = benefit.values[0];
    const lableClassName = cx('data-text', { 'empty-value': !label });
    const template = getTemplate(tariffConfiguration, benefit.name);

    const templateProps = {
      tariffValues: benefit.values,
      tariffConfiguration,
      toggleBenefitSelection: (benefitValue: string, newSelected: boolean) => {
        props.setTariffId(plan.id);
        toggleSelectedBenefit(
          id,
          benefit.name,
          benefitValue,
          template.templateType,
          newSelected
        );
        // API call to fetch new prices for a tariff - for accordion card in mobile
        props.getTariffAddonPrice(plan.id);
      }
    };
    // to identify addons isSelectable needs to be true
    if (!config.showAddons && benefit.isSelectable) {
      return null;
    }
    // else in other cases isSelectable is false, we will simply render on ui list items
    if (!benefit.isSelectable) {
      return (
        <li key={index} className='lists'>
          <span className={lableClassName}>{getIconOrLabel(benefit)}</span>
        </li>
      );
    }
    // for addons we need to render according to their templates
    switch (template.templateName) {
      case TARIFF_BENEFIT_TEMPLATES.BUTTON: {
        if (isMobile.phone || isMobile.tablet) {
          return <DropdownTemplate {...templateProps} useImage={false} />;
        }

        return <ButtonTemplate {...templateProps} />;
      }
      case TARIFF_BENEFIT_TEMPLATES.DROPDOWN: {
        return <DropdownTemplate {...templateProps} useImage={false} />;
      }
      case TARIFF_BENEFIT_TEMPLATES.IMAGE: {
        if (isMobile.phone || isMobile.tablet) {
          return <DropdownTemplate {...templateProps} useImage={true} />;
        }

        return <ImageTemplate {...templateProps} />;
      }
      default: {
        return (
          <li key={index} className='lists'>
            <span className={lableClassName}>{getIconOrLabel(benefit)}</span>
          </li>
        );
      }
    }
  });

  if (viewMore && config.showUnlimitedBenefits) {
    const commonEl = commonBenefits.map((benefit: IBenefits, index: number) => {
      const lableClassName = cx('data-text', { 'empty-value': !benefit.label });

      return (
        <li key={index} className='lists'>
          <span className={lableClassName}>{getIconOrLabel(benefit)}</span>
        </li>
      );
    });
    listEl = listEl.concat(commonEl);
  }
  // add price to card
  const showPriceOnCard =
    tariffConfiguration.tariffListingTemplate ===
      TARIFF_LIST_TEMPLATE.NAME_FIRST ||
    tariffConfiguration.tariffListingTemplate ===
      TARIFF_LIST_TEMPLATE.DATA_FIRST;

  const listSlice =
    tariffConfiguration.tariffListingTemplate ===
    TARIFF_LIST_TEMPLATE.DATA_FIRST
      ? 2
      : 3;

  return (
    <>
      {tariffConfiguration.tariffListingTemplate ===
      TARIFF_LIST_TEMPLATE.NAME_FIRST
        ? null
        : config.showPlanName && (
            <li className='lists'>
              <span className='data-text 1'>{plan.name}</span>
            </li>
          )}
      {config.showBenefits && listEl ? listEl.slice(0, listSlice) : null}
      {showPriceOnCard && config.showPrice ? (
        <li className='lists'>
          <div className='right-align'>
            {/* {price.discounts && price.discounts.length !== 0 && (
              <span className='modify-text'>
                <span className='value'>{price.actualValue}</span>
              </span>
            )} */}
            <TariffPrice
              totalPrices={plan.totalPrices}
              currency={currency}
              reverseOrder={true}
              priceFirst={true}
              hideActualPriceCurrency={true}
            />
          </div>
        </li>
      ) : null}
    </>
  );
}
