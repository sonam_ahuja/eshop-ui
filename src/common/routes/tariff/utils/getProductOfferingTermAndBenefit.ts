export default function getTariffCategorySlug(
  search: string
): {
  productOfferingTerm: string | null;
  productOfferingBenefit: string | null;
} {
  const searchParams = new URLSearchParams(search);
  const productOfferingTerm = searchParams.get('productOfferingTerm');
  const productOfferingBenefit = searchParams.get('productOfferingBenefit');

  return {
    productOfferingTerm,
    productOfferingBenefit
  };
}
