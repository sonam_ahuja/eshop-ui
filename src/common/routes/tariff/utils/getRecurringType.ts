import { ITariffTranslation } from '@common/store/types/translation';
import { IPlans } from '@tariff/store/types';
import { MONTH } from '@tariff/constants';

export default function getRecouringType(
  plan: IPlans,
  tariffTranslation: ITariffTranslation
): string | null {
  const totalPrice = plan.totalPrices[0];
  if (totalPrice) {
    if (totalPrice.recurringChargePeriod === MONTH) {
      return ` /${tariffTranslation.perMonth}`;
    }

    return null;
  }

  return null;
}
