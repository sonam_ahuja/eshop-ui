export default function getTariffCategorySlug(pathname: string): string {
  if (pathname && pathname.indexOf('/tariff/') !== -1) {
    return pathname.replace('/tariff/', '');
  } else {
    return '';
  }
}
