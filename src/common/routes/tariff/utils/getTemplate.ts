import { ITariffConfiguration } from '@common/store/types/configuration';
import { ITemplate } from '@routes/tariff/types';

export default function gettemplate(
  config: ITariffConfiguration,
  benefitName: string
): ITemplate {
  return (
    config.tariffBenefitTemplates[benefitName] || {
      templateName: config.defaultTariffBenefitTemplate,
      templateType: config.defaultTariffBenefitTemplateType
    }
  );
}
