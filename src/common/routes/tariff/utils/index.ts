export { default as getAllPlansValues } from './getAllPlansValues';
export {
  default as getAllPlansValuesPlanSelectionOnly
} from './getAllPlansValuesPlanSelectionOnly';
export { default as getTariffCategorySlug } from './getTariffCategorySlug';
export { default as getAllPlansTitles } from './getAllPlansTitles';
export { default as LandingPageWrapper } from './styles';
export { default as checkBestDeviceOffers } from './checkBestDeviceOffers';
export { default as emptyPriceDecoration } from './priceDecoration';
export {
  default as composeTariffRequestPayload
} from './composeTariffRequestPayload';
export {
  default as getProductOfferingTermAndBenefit
} from './getProductOfferingTermAndBenefit';
import { ICategory } from '@category/store/types';
import { ICharacteristic } from '@tariff/store/types';

export const showDropdown = (category: ICategory, key: string): string => {
  const dropdown = category.characteristics.filter((item: ICharacteristic) => {
    return item.name === key;
  });

  if (dropdown && dropdown[0] && dropdown[0].values && dropdown[0].values[0]) {
    return dropdown[0].values[0].value;
  }

  return 'false';
};
