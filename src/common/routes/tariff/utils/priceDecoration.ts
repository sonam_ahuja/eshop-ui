import { IPlans } from '@tariff/store/types';

export default function priceDecoration(plans: IPlans[]): boolean {
  let status = false;
  if (plans && plans.length === 0) {
    return status;
  }

  plans.forEach((plan: IPlans) => {
    if (
      plan.prices &&
      plan.prices[0] &&
      plan.prices[0].discounts.length !== 0
    ) {
      status = true;
    }
  });

  return status;
}
