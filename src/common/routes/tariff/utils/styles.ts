import styled from 'styled-components';
import { breakpoints } from '@src/common/variables';
import { theme as themeObj } from '@tariff/theme';
import { Column, Row } from '@src/common/components/Grid/styles';

const LandingPageWrapper = styled.div<{
  backgroundImage: string;
  theme: 'primary' | 'secondary';
}>`
  background-color: ${({ theme }) => themeObj[theme].backgroundColor};
  background-image: url('${({ backgroundImage }) => backgroundImage}');
  background-size: 100% auto;
  background-repeat: no-repeat;
  background-position: top left;
  padding:6rem 1.25rem 3rem;
  ${Row} {
    margin: -0.125rem;

    ${Column} {
      padding: 0.125rem;
    }
  }

  @media (min-width: ${breakpoints.tabletPortrait}px) {
    padding:6rem 2.25rem 3rem;
  }
  @media (min-width: ${breakpoints.tabletLandscape}px) {
    padding:6rem 3rem 3rem;
  }
  @media (min-width: ${breakpoints.desktop}px) {
    padding:6rem 4.9rem 3rem;
  }
  @media (min-width: ${breakpoints.desktopLarge}px) {
    padding:6rem 5.5rem 3rem;
  }
`;

export default LandingPageWrapper;
