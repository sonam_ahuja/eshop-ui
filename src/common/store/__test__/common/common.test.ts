import {
  getCheckoutState,
  getCommonState,
  getConfiguration,
  getCurrencyCode,
  getProductDetailedState
} from '@src/common/store/common';

describe('common', () => {
  it('getCheckoutState ::', () => {
    const expectedAction = getCheckoutState();
    expect(getCheckoutState()).toEqual(expectedAction);
  });

  it('getConfiguration ::', () => {
    const expectedAction = getConfiguration();
    expect(getConfiguration()).toEqual(expectedAction);
  });

  it('getCommonState ::', () => {
    const expectedAction = getCommonState();
    expect(getCommonState()).toEqual(expectedAction);
  });

  it('getProductDetailedState ::', () => {
    const expectedAction = getProductDetailedState();
    expect(getProductDetailedState()).toEqual(expectedAction);
  });
  it('getCurrencyCode ::', () => {
    const expectedAction = getCurrencyCode();
    expect(getCurrencyCode()).toEqual(expectedAction);
  });
});
