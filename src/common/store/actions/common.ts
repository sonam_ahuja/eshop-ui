import { actionCreator } from '@utils/actionCreator';
import { actionConstants as CONSTANTS } from '@store/constants';
import * as userDetailsApi from '@common/types/api/userDetails';
import * as identityVerificationProvidersApi from '@common/types/api/identityVerification/identityVerificationProviders';
import * as verifyUserUsingOtpApi from '@common/types/api/identityVerification/validateUserUsingOtp';
import { IError as IGenricError } from '@store/types/common';

export default {
  showError: actionCreator<string>(CONSTANTS.SHOW_API_ERROR),
  removeError: actionCreator(CONSTANTS.REMOVE_API_ERROR),
  showLoader: actionCreator(CONSTANTS.SHOW_LOADER),
  removeLoader: actionCreator(CONSTANTS.REMOVE_LOADER),
  setIfLoginButtonClicked: actionCreator<boolean>(
    CONSTANTS.SET_IF_LOGIN_BUTTON_CLICK
  ),
  routeToLoginFromBasket: actionCreator<boolean>(
    CONSTANTS.ROUTE_TO_LOGIN_FROM_BASKET
  ),
  isUserLoggedIn: actionCreator<boolean>(CONSTANTS.IS_USER_LOGGED_IN),
  setMsisdnHeader: actionCreator<string>(CONSTANTS.SET_MSISDN_HEADER),
  // tslint:disable:no-any
  setMegaMenu: actionCreator<any>(CONSTANTS.SET_MEGA_MENU),
  setMegaMenuFooter: actionCreator<any>(CONSTANTS.SET_MEGA_MENU_FOOTER),
  fetchUserDetails: actionCreator<void>(CONSTANTS.FETCH_USER_DETAILS),
  setUserDetails: actionCreator<userDetailsApi.GET.IResponse>(
    CONSTANTS.SET_USER_DETAILS
  ),
  fetchIdentityVerificationDetails: actionCreator<void>(
    CONSTANTS.FETCH_IDENTITY_VERIFICATION_DETAILS
  ),
  setIdentityVerificationDetails: actionCreator<
    identityVerificationProvidersApi.GET.IResponse
  >(CONSTANTS.SET_IDENTITY_VERIFICATION_DETAILS),
  verifyUserIdentity: actionCreator<
    identityVerificationProvidersApi.POST.IRequest
  >(CONSTANTS.VERIFY_USER_IDENTITY),

  // validate user using otp
  verifyUserUsingOtp: actionCreator<verifyUserUsingOtpApi.POST.IRequest>(
    CONSTANTS.VERIFY_USER_USING_OTP
  ),
  // validate user using Bank
  verifyUserUsingBank: actionCreator<
    identityVerificationProvidersApi.GET.IResponse
  >(CONSTANTS.VERIFY_USER_USING_BANK),
  goToNextRoute: actionCreator(CONSTANTS.GO_TO_NEXT_POSSIBLE_ROUTE),
  setCurrency: actionCreator<string>(CONSTANTS.SET_CURRENCY_VALUE),

  setProlongationAuthorizationToken: actionCreator<string | null>(
    CONSTANTS.SET_PROLONGATION_AUTHORIZATION_TOKEN
  ),
  // used for showing Genric error
  showGenricError: actionCreator<IGenricError>(CONSTANTS.SHOW_GENRIC_ERROR),
  removeGenricError: actionCreator<void>(CONSTANTS.REMOVE_GENRIC_ERROR),
  setDeviceList: actionCreator<string>(CONSTANTS.SET_DEVICE_LIST),
  setLandingPageDeviceList: actionCreator<string>(
    CONSTANTS.SET_LANDING_PAGE_DEVICE_LIST
  ),
  setDeviceDetailedList: actionCreator<string>(
    CONSTANTS.SET_DEVICE_DETAILED_LIST
  ),
  setOfflineToast: actionCreator<boolean>(CONSTANTS.SET_OFFLINE_TOAST)
};
