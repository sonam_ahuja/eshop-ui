import { actionCreator } from '@src/common/utils/actionCreator';
import { IConfigurationResponse } from '@store/types/configuration';
import { configurationConstant as CONSTANTS } from '@store/constants';

export default {
  setCMSConfiguration: actionCreator<IConfigurationResponse>(
    CONSTANTS.SET_CMS_CONFIGURATION_DATA
  )
};
