import { actionCreator } from '@src/common/utils/actionCreator';
import { ITranslationState as ITranslationResponse } from '@store/types/translation';
import { translationConstant as CONSTANTS } from '@store/constants';

export default {
  setEnglishCMSTranslation: actionCreator<ITranslationResponse>(
    CONSTANTS.SET_ENGLISH_CMS_TRANSLATION_DATA
  )
};
