import { actionCreator } from '@src/common/utils/actionCreator';
import { ITranslationState as ITranslationResponse } from '@store/types/translation';
import { translationConstant as CONSTANTS } from '@store/constants';

export default {
  setCMSTranslation: actionCreator<ITranslationResponse>(
    CONSTANTS.SET_CMS_TRANSLATION_DATA
  )
};
