import { RootState } from '@src/common/store/reducers';
import { IBasketState } from '@basket/store/types';
import store from '@common/store';
import { ICommonState } from '@common/store/types/common';
import { IConfigurationState } from '@common/store/types/configuration';
import {
  IBasket,
  IGlobalTranslation,
  ITranslation
} from '@common/store/types/translation';
import { IState as ICheckoutState } from '@checkout/store/types';
import { IProductDetailedState } from '@src/common/routes/productDetailed/store/types';
import { ITariffState } from '@src/common/routes/tariff/store/types';
import { ILandingPageState } from '@src/common/routes/home/store/types';

export const getApptState = (): RootState => {
  return store && store.getState();
};

export const getCheckoutState = (): ICheckoutState => {
  const state: RootState = store && store.getState();

  return state && state.checkout;
};

export const getCommonState = (): ICommonState => {
  const state: RootState = store && store.getState();

  return state && state.common;
};

export const getConfiguration = (): IConfigurationState => {
  const state: RootState = store && store.getState();

  return state && state.configuration;
};

export const getBasketState = (): IBasketState => {
  const state: RootState = store.getState();

  return state.basket;
};

export const getProductDetailedState = (): IProductDetailedState => {
  const state: RootState = store.getState();

  return state.productDetailed;
};

export const getGlobalTranslation = (): IGlobalTranslation => {
  const state: RootState = store.getState();

  return state.translation.cart.global;
};

export const getBasketTranslation = (): IBasket => {
  const state: RootState = store.getState();

  return state.translation.cart.basket;
};

export const isCategoryLandingPageRequired = (): boolean => {
  const state: RootState = store.getState();

  return state.configuration.cms_configuration.global
    .categoryLandingPageRequired;
};

export const getCurrencyCode = (): string => {
  const configuration = getConfiguration();

  return configuration.cms_configuration.global.currency
    ? configuration.cms_configuration.global.currency.locale
    : '';
};

export const getTariffState = (): ITariffState => {
  const state: RootState = store.getState();

  return state.tariff;
};

export const getLandingPageState = (): ILandingPageState => {
  const state: RootState = store.getState();

  return state.landingPage;
};

export const getEnglishTranslation = (): ITranslation => {
  const state: RootState = store.getState();

  return state.englishTranslation;
};
