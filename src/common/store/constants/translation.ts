export default {
  SET_CMS_TRANSLATION_DATA: 'SET_CMS_TRANSLATION_DATA',
  SET_ENGLISH_CMS_TRANSLATION_DATA: 'SET_ENGLISH_CMS_TRANSLATION_DATA'
};
