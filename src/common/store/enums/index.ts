export enum IMAGE_POSITION {
  FRONT = 'front',
  BACK = 'back',
  RIGHT = 'right',
  LEFT = 'left',
  BOTTOM = 'bottom',
  TOP = 'top',
  FRONT_AND_BACK = 'frontAndBack',
  OTHERS = 'others'
}

export enum PAGE_THEME {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
/** USED ON PROLONGATION */
export enum IMAGE_TYPE {
  WEBP = 'webp',
  JPEG = 'jpeg',
  PNG = 'png'
}

export enum ITEM_STATUS {
  OUT_OF_STOCK = 'OUT_OF_STOCK',
  IN_STOCK = 'IN_STOCK',
  PRE_ORDER = 'PRE_ORDER'
}

export type pageThemeType = PAGE_THEME.PRIMARY | PAGE_THEME.SECONDARY;

export enum SOCIAL_MEDIA_ICON {
  facebook = 'ec-facebook',
  twitter = 'ec-twitter',
  youtube = 'ec-youtube',
  linkedin = 'ec-linkedin',
  instagram = 'ec-instagram'
}

export enum TARIFF_LIST_TEMPLATE {
  PRICE_FIRST = 'PRICE_FIRST',
  NAME_FIRST = 'NAME_FIRST',
  DATA_FIRST = 'DATA_FIRST'
}
export enum IDENTITY_VERIFICATION_PROVIDERS {
  bank = 'BANK',
  courier = 'COURIER',
  telekom = 'TELEKOM'
}

export enum SCREEN_RESOLUTION {
  MOBILE = 'MOBILE',
  TABLET_PORTRAIT = 'TABLET_PORTRAIT',
  TABLET_LANDSCAPE = 'TABLET_LANDSCAPE',
  DESKTOP = 'DESKTOP',
  DESKTOP_LARGE = 'DESKTOP_LARGE'
}

export enum INPUT_TYPE {
  TEXT = 'text',
  PASSWORD = 'password'
}

export enum IMGAE_WIDTH_RESOLUTION {
  LARGE_DESKTOP_WIDTH = 200,
  LARGE_MOBILE_WIDTH = 150,
  MEDIUM_DESKTOP_WIDTH = 140,
  MEDIUM_MOBILE_WIDTH = 90,
  SMALL_DESKTOP_WIDTH = 140,
  SMALL_MOBILE_WIDTH = 90,
  VERY_SMALL_DESKTOP_WIDTH = 50,
  VERY_SMALL_MOBILE_WIDTH = 30
}
