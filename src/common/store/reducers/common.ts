import { ICommonState, IError } from '@store/types/common';
import { actionConstants as CONSTANTS } from '@common/store/constants';
import { commonState } from '@common/store/states';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import * as userDetailsApi from '@common/types/api/userDetails';
import * as identityVerificationProvidersApi from '@common/types/api/identityVerification/identityVerificationProviders';

const reducers = {
  [CONSTANTS.SHOW_API_ERROR]: (state: ICommonState, payload: string) => {
    state.errorToast = {
      message: payload,
      isOpen: true
    };
  },
  [CONSTANTS.REMOVE_API_ERROR]: (state: ICommonState) => {
    state.errorToast.isOpen = false;
  },
  [CONSTANTS.SHOW_LOADER]: (state: ICommonState) => {
    state.loading = true;
  },
  [CONSTANTS.REMOVE_LOADER]: (state: ICommonState) => {
    state.loading = false;
  },
  [CONSTANTS.SET_IF_LOGIN_BUTTON_CLICK]: (
    state: ICommonState,
    isLoginButtonClicked: boolean
  ) => {
    state.isLoginButtonClicked = isLoginButtonClicked;
    // tslint:disable:no-commented-code
    // state.isLoginButtonClicked = !isLoginButtonClicked
    //   ? isLoginButtonClicked
    //   : isLoginButtonClicked && !state.isLoggedIn;
  },
  [CONSTANTS.ROUTE_TO_LOGIN_FROM_BASKET]: (
    state: ICommonState,
    routeFromBasketToLogin: boolean
  ) => {
    state.routeFromBasketToLogin = routeFromBasketToLogin;
  },
  [CONSTANTS.IS_USER_LOGGED_IN]: (state: ICommonState, isLoggedIn: boolean) => {
    state.isLoggedIn = isLoggedIn;
  },
  [CONSTANTS.SET_MSISDN_HEADER]: (state: ICommonState, msisdnValue: string) => {
    state.msisdnValue = msisdnValue;
  },
  // tslint:disable:no-any
  [CONSTANTS.SET_MEGA_MENU]: (state: ICommonState, megaMenu: any) => {
    state.megaMenu = megaMenu.categories;
  },
  [CONSTANTS.SET_MEGA_MENU_FOOTER]: (
    state: ICommonState,
    megaMenuFooter: any
  ) => {
    state.megaMenuFooter = megaMenuFooter.categories;
  },
  [CONSTANTS.SET_USER_DETAILS]: (
    state: ICommonState,
    userDetails: userDetailsApi.GET.IResponse
  ) => {
    state.userDetails = userDetails;
  },
  [CONSTANTS.SET_IDENTITY_VERIFICATION_DETAILS]: (
    state: ICommonState,
    userIdentityVerificationDetails: identityVerificationProvidersApi.GET.IResponse
  ) => {
    state.userIdentityVerificationDetails = userIdentityVerificationDetails;
  },
  [CONSTANTS.SET_CURRENCY_VALUE]: (state: ICommonState, currency: string) => {
    state.currency = currency;
  },
  [CONSTANTS.SET_PROLONGATION_AUTHORIZATION_TOKEN]: (
    state: ICommonState,
    payload: string | null
  ) => {
    state.prolongation.authorizationToken = payload;
  },
  [CONSTANTS.SHOW_GENRIC_ERROR]: (state: ICommonState, payload: IError) => {
    state.error = payload;
  },
  [CONSTANTS.SET_DEVICE_LIST]: (state: ICommonState, payload: string) => {
    state.deviceList = payload;
  },
  [CONSTANTS.SET_LANDING_PAGE_DEVICE_LIST]: (
    state: ICommonState,
    payload: string
  ) => {
    state.landingPageDeviceList = payload;
  },
  [CONSTANTS.SET_DEVICE_DETAILED_LIST]: (
    state: ICommonState,
    payload: string
  ) => {
    state.deviceDetailedList = payload;
  },
  [CONSTANTS.SET_OFFLINE_TOAST]: (state: ICommonState, payload: boolean) => {
    state.showOfflineToast = payload;
  },
  [CONSTANTS.REMOVE_GENRIC_ERROR]: (state: ICommonState) => {
    state.error = {
      showFullPageError: false,
      retryable: false,
      code: '',
      httpStatusCode: '',
      message: {
        debugMessage: '',
        userMessage: ''
      }
    };
  }
};

export default withProduce(commonState, reducers) as Reducer<
  ICommonState,
  AnyAction
>;
