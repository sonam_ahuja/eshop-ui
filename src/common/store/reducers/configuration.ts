import {
  IConfigurationResponse,
  IConfigurationState
} from '@store/types/configuration';
import { configurationConstant as CONSTANTS } from '@common/store/constants';
import { configurationInitialState } from '@common/store/states';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import { deepMergeObj } from '@utils/index';

const reducers = {
  [CONSTANTS.SET_CMS_CONFIGURATION_DATA]: (
    state: IConfigurationState,
    payload: IConfigurationResponse
  ) => {
    deepMergeObj(state.cms_configuration, payload);
  }
};

export default withProduce(configurationInitialState, reducers) as Reducer<
  IConfigurationState,
  AnyAction
>;
