import { combineReducers } from 'redux';
import { StateType } from 'typesafe-actions';
import { productListReducer as productList } from '@productList/store';
import { orderConfirmationReducer as orderConfirmation } from '@routes/orderConfirmation/store';
import { basketReducer as basket } from '@basket/store';
import { categoriesReducer as categories } from '@category/store';
import checkout from '@checkout/store'; // multiple reducers are combined in case of checkout
import authentication from '@authentication/store/reducer';
import productDetailed from '@productDetailed/store/reducer';
import search from '@search/store/reducer';
import tariff from '@tariff/store/reducer';
import { landingPageReducer as landingPage } from '@home/store';

import { default as translation } from './translation';
import { default as englishTranslation } from './englishTranslation';
import { default as configuration } from './configuration';
import common from './common';
import { IMainState } from './types';

const rootReducer = combineReducers<IMainState>({
  basket,
  categories,
  translation,
  configuration,
  common,
  productList,
  checkout,
  authentication,
  productDetailed,
  orderConfirmation,
  search,
  tariff,
  landingPage,
  englishTranslation
});

export type RootState = StateType<typeof rootReducer>;
export default rootReducer;
