import { ITranslationState } from '@store/types/translation';
import { translationConstant as CONSTANTS } from '@common/store/constants';
import { translationInitialState } from '@common/store/states';
import withProduce from '@utils/withProduce';
import { AnyAction, Reducer } from 'redux';
import { deepMergeObj } from '@utils/index';

const reducers = {
  [CONSTANTS.SET_CMS_TRANSLATION_DATA]: (
    state: ITranslationState,
    payload: ITranslationState
  ) => {
    deepMergeObj(state, payload);
  }
};

export default withProduce(translationInitialState, reducers) as Reducer<
  ITranslationState,
  AnyAction
>;
