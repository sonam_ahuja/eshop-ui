import { ICommonState } from '@common/store/types/common';
import { IConfigurationState } from '@common/store/types/configuration';
import { ITranslationState } from '@common/store/types/translation';
import { IBasketState } from '@src/common/routes/basket/store/types';
import { IAuthenticationState } from '@src/common/routes/authentication/store/types';
import { IState as ICheckoutState } from '@src/common/routes/checkout/store/types';
import { ICategoryState } from '@src/common/routes/category/store/types';
import { IProductListState } from '@productList/store/types';
import { IProductDetailedState } from '@src/common/routes/productDetailed/store/types';
import { IOrderConfirmationState } from '@routes/orderConfirmation/store/types';
import { ISearchState } from '@routes/search/store/types';
import { ITariffState } from '@tariff/store/types';
import { ILandingPageState } from '@home/store/types';

export interface IMainState {
  translation: ITranslationState;
  configuration: IConfigurationState;
  categories: ICategoryState;
  common: ICommonState;
  checkout: ICheckoutState;
  productList: IProductListState;
  basket: IBasketState;
  authentication: IAuthenticationState;
  productDetailed: IProductDetailedState;
  orderConfirmation: IOrderConfirmationState;
  search: ISearchState;
  tariff: ITariffState;
  landingPage: ILandingPageState;
  englishTranslation: ITranslationState;
}
