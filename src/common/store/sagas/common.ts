import * as identityVerificationProvidersApi from '@common/types/api/identityVerification/identityVerificationProviders';
import { put, takeLatest } from 'redux-saga/effects';
import constants from '@store/constants/actionConstants';
import { logError } from '@src/common/utils';
import { commonAction as actions } from '@store/actions';
import authenticationActions from '@authentication/store/actions';
import { fetchUserProfileService } from '@authentication/store/services';
import {
  fetchIdentityVerificationProvidersService,
  verifyUserByBankPostService,
  verifyUserIdentityService,
  verifyUserUsingOtpService
} from '@common/store/services/identityVerification';
import { IDENTITY_VERIFICATION_PROVIDERS } from '@src/common/store/enums';
import store from '@common/store';
import history from '@src/client/history';
import * as verifyUserUsingOtpApi from '@common/types/api/identityVerification/validateUserUsingOtp';
import { removeParamFromQueryString } from '@src/common/utils/removeQueryParam';
import { parseQueryString } from '@src/common/utils/parseQuerySrting';
import { updateQueryStringParameter } from '@src/common/utils/addUpdateQueryParams';
import {
  loginQueryParam,
  loginSteps
} from '@src/common/routes/authentication/constants';
import routeConstant from '@common/constants/routes';
import { sendLoginSuccessEvent } from '@src/common/events/login';
import { setUniqueIdentifier } from '@src/common/utils/uniqueIdentifier';
// tslint:disable

export function* fetchUserProfile(): Generator {
  try {
    const userDetails = yield fetchUserProfileService();
    setUniqueIdentifier(userDetails.id);
    sendLoginSuccessEvent(userDetails.id);
    yield put(actions.setUserDetails(userDetails));
  } catch (error) {
    logError(error);
  }
}

export function* fetchIdentityVerificationDetails(): Generator {
  try {
    const verificationProviders = yield fetchIdentityVerificationProvidersService();
    yield put(actions.setIdentityVerificationDetails(verificationProviders));
  } catch (error) {
    logError(error);
  }
}

export function* verifyUserIdentity({
  payload
}: {
  type: string;
  payload: identityVerificationProvidersApi.POST.IRequest;
}): Generator {
  try {
    const state = store.getState();
    const isProviderTelekom =
      payload.providerType === IDENTITY_VERIFICATION_PROVIDERS.telekom;
    if (isProviderTelekom) {
      yield put(authenticationActions.setLoading());
      const loadingText =
        state.translation.cart.login.loginProgressOtpSent.messageForMsisdnLogin;
      yield put(
        authenticationActions.setLoadingTextForProgressModal(loadingText)
      );
    }
    const response = yield verifyUserIdentityService(payload);
    if (response.providerType === IDENTITY_VERIFICATION_PROVIDERS.courier) {
      const verificationProviders = yield fetchIdentityVerificationProvidersService();
      if (!verificationProviders.verificationRequired) {
        yield put(authenticationActions.closeLoginModal());
        if (state.common.routeFromBasketToLogin && history) {
          history.push(routeConstant.CHECKOUT.PERSONAL_INFO);
        }
      } else {
        if (history) {
          history.push({
            pathname: history.location.pathname,
            search: encodeURIComponent(
              updateQueryStringParameter(
                decodeURIComponent(history.location.search),
                loginQueryParam,
                loginSteps.IDENTITY_VERIFICATION
              )
            )
          });
        }
      }
    } else if (
      response.providerType === IDENTITY_VERIFICATION_PROVIDERS.telekom &&
      response.nonce
    ) {
      yield put(authenticationActions.setLoadingOff());
      yield put(authenticationActions.setLoadingTextForProgressModal(''));
      yield put(authenticationActions.setNonceForMsisdn(response.nonce));
      if (history) {
        history.replace({
          pathname: history.location.pathname,
          search: encodeURIComponent(
            updateQueryStringParameter(
              decodeURIComponent(history.location.search),
              loginQueryParam,
              loginSteps.ENTER_OTP
            )
          )
        });
      }
    } else if (
      response.providerType === IDENTITY_VERIFICATION_PROVIDERS.bank &&
      history &&
      response.requestUrl
    ) {
      yield put(actions.verifyUserUsingBank(response));
    }
  } catch (error) {
    logError(error);
    yield put(authenticationActions.setLoadingOff());
  }
}

export function* verifyUserIdentityUsingOtp({
  payload
}: {
  type: string;
  payload: verifyUserUsingOtpApi.POST.IRequest;
}): Generator {
  try {
    const state = store.getState();
    yield verifyUserUsingOtpService(payload);
    const verificationProviders = yield fetchIdentityVerificationProvidersService();
    if (!verificationProviders.verificationRequired) {
      yield put(authenticationActions.closeLoginModal());
      yield put(authenticationActions.resetAuthenticationState());
      if (state.common.routeFromBasketToLogin && history) {
        history.push(routeConstant.CHECKOUT.PERSONAL_INFO);
      }
    } else {
      if (history) {
        history.push({
          pathname: history.location.pathname,
          search: encodeURIComponent(
            updateQueryStringParameter(
              decodeURIComponent(history.location.search),
              loginQueryParam,
              loginSteps.IDENTITY_VERIFICATION
            )
          )
        });
      }
    }
  } catch (error) {
    logError(error);
  }
}

export function* verifyUserIdentityUsingBank({
  payload
}: {
  type: string;
  payload: identityVerificationProvidersApi.POST.IResponse;
}): Generator {
  try {
    if (
      payload.providerType === IDENTITY_VERIFICATION_PROVIDERS.bank &&
      history &&
      payload.requestUrl &&
      payload.requestType
    ) {
      if (payload.requestType.toLowerCase() === 'get') {
        window.location = payload.requestUrl as any;
      } else if (payload.requestType.toLowerCase() === 'post') {
        yield verifyUserByBankPostService(payload);
      }
    }
  } catch (error) {
    logError(error);
  }
}

export function* goToNextPossibleRoute(): Generator {
  try {
    const state = store.getState();
    if (state.common.isLoggedIn && history) {
      if (
        state.configuration.cms_configuration.modules.checkout
          .identityVerification.enabled
      ) {
        const verificationProviders = yield fetchIdentityVerificationProvidersService();
        yield put(
          actions.setIdentityVerificationDetails(verificationProviders)
        );
        if (verificationProviders.verificationRequired) {
          if (state.authentication.isUserBankAuthenticated) {
            // courier se verify krna hai when bank is giving false
            const identificationProviders =
              state.common.userIdentityVerificationDetails
                .identificationProviders;
            const identificationProvidersDetails =
              identificationProviders &&
              identificationProviders[IDENTITY_VERIFICATION_PROVIDERS.courier]
                ? identificationProviders[
                    IDENTITY_VERIFICATION_PROVIDERS.courier
                  ]
                : null;
            yield put(
              actions.verifyUserIdentity({
                providerId: identificationProvidersDetails
                  ? identificationProvidersDetails.providerSublist &&
                    identificationProvidersDetails.providerSublist[0] &&
                    identificationProvidersDetails.providerSublist[0].providerId
                  : '',
                providerType: IDENTITY_VERIFICATION_PROVIDERS.courier
              })
            );
          } else {
            history.push({
              pathname: history.location.pathname,
              search: encodeURIComponent(
                updateQueryStringParameter(
                  decodeURIComponent(history.location.search),
                  loginQueryParam,
                  loginSteps.IDENTITY_VERIFICATION
                )
              )
            });
          }
        } else {
          history.push(routeConstant.CHECKOUT.PERSONAL_INFO);
        }
      } else {
        history.push(routeConstant.CHECKOUT.PERSONAL_INFO);
      }
    } else {
      yield put(actions.routeToLoginFromBasket(true));
    }
  } catch (error) {
    logError(error);
  }
}

export function* updateQueryParamForLogin({
  payload
}: {
  type: string;
  payload: boolean;
}): Generator {
  try {
    const state = store.getState();
    const isLoginFormOpen = payload;
    if (history) {
      const decodedSearch = decodeURIComponent(history.location.search);
      const parsedQuery = parseQueryString(decodedSearch);
      const loginStep =
        parsedQuery.loginStep && parsedQuery.loginStep.length
          ? parsedQuery.loginStep
          : loginSteps.ENTER_LOGIN_CREDENTIAL;
      if (isLoginFormOpen) {
        if (
          !decodeURIComponent(history.location.search).includes(loginQueryParam)
        ) {
          history.push({
            pathname: history.location.pathname,
            search: encodeURIComponent(
              updateQueryStringParameter(
                decodeURIComponent(history.location.search),
                loginQueryParam,
                loginStep
              )
            )
          });
        }
      } else {
        if (state.common.isLoggedIn) {
          if (
            !history.location.pathname.includes(routeConstant.CHECKOUT.BASE)
          ) {
            history.goBack();
          }
        } else {
          history.replace({
            pathname: history.location.pathname,
            search: encodeURIComponent(
              removeParamFromQueryString(
                loginQueryParam,
                decodeURIComponent(history.location.search)
              )
            )
          });
        }
      }
    }
  } catch (error) {
    logError(error);
  }
}

function* watcherSaga(): Generator {
  yield takeLatest(constants.FETCH_USER_DETAILS, fetchUserProfile);
  yield takeLatest(
    constants.FETCH_IDENTITY_VERIFICATION_DETAILS,
    fetchIdentityVerificationDetails
  );
  yield takeLatest(constants.VERIFY_USER_IDENTITY, verifyUserIdentity);
  yield takeLatest(constants.VERIFY_USER_USING_OTP, verifyUserIdentityUsingOtp);
  yield takeLatest(constants.GO_TO_NEXT_POSSIBLE_ROUTE, goToNextPossibleRoute);
  yield takeLatest(
    constants.SET_IF_LOGIN_BUTTON_CLICK,
    updateQueryParamForLogin
  );
  yield takeLatest(
    constants.ROUTE_TO_LOGIN_FROM_BASKET,
    updateQueryParamForLogin
  );
  yield takeLatest(
    constants.VERIFY_USER_USING_BANK,
    verifyUserIdentityUsingBank
  );
}
export default watcherSaga;
