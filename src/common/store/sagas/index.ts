import { all } from 'redux-saga/effects';
import basketSaga from '@basket/store/sagas';
import productListSaga from '@productList/store/sagas';
import { categoriesSaga } from '@category/store';
import { orderConfirmationSaga } from '@routes/orderConfirmation/store';
import {
  billingSaga,
  checkoutSaga,
  creditCheckSaga,
  numberPortingSaga,
  orderReviewSaga,
  paymentInfoSaga,
  personalInfoSaga,
  shippingSaga
} from '@checkout/store';
import authenticationSaga from '@authentication/store/sagas/index';
import productDetailedSaga from '@productDetailed/store/sagas';
import searchSaga from '@search/store/sagas';
import tariffSaga from '@tariff/store/sagas';
import landingPageSaga from '@home/store/sagas';
import commonSaga from '@common/store/sagas/common';

export default function* rootSaga(): Generator {
  yield all([
    basketSaga(),
    checkoutSaga(),
    personalInfoSaga(),
    shippingSaga(),
    productListSaga(),
    categoriesSaga(),
    paymentInfoSaga(),
    creditCheckSaga(),
    orderReviewSaga(),
    billingSaga(),
    productDetailedSaga(),
    numberPortingSaga(),
    orderConfirmationSaga(),
    searchSaga(),
    tariffSaga(),
    commonSaga(),
    landingPageSaga(),
    ...authenticationSaga
  ]);
}
