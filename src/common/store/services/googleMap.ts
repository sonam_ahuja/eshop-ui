import apiCaller from '@common/utils/apiCaller';
import apiEndpoints from '@src/common/constants/apiEndpoints';
import store from '@common/store';
import { IGeocodeResult, INortheast } from '@src/common/types/common/googleMap';

const googleMapApiKey = store.getState().configuration.cms_configuration.global
  .googleMap.apiKey;

export const getLocationOfAddress = (payload: string): Promise<INortheast> => {
  return apiCaller
    .get<IGeocodeResult>(
      `${
        apiEndpoints.GOOGLE_MAP.GEOCODE.url
      }json?address=${payload}&key=${googleMapApiKey}`
    )
    .then((response: IGeocodeResult) => {
      if (response.status === 'OK') {
        return response.results[0].geometry.location;
      }
      throw new Error('An Error has occured!');
    });
};
