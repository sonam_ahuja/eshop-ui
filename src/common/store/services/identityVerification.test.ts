import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import apiEndpoints from '@src/common/constants/apiEndpoints';
import store from '@common/store';
import {
  fetchIdentityVerificationProvidersService,
  verifyUserIdentityService,
  verifyUserUsingOtpService
} from '@store/services/identityVerification';
import { logError } from '@common/utils';

describe('Identity Verification Service', () => {
  const mock = new MockAdapter(axios);
  it('fetchIdentityVerificationProvidersService success test', async () => {
    const googleMapApiKey = store.getState().configuration.cms_configuration
      .global.googleMap.apiKey;
    const url = `${
      apiEndpoints.GOOGLE_MAP.GEOCODE.url
    }json?address=${'loginPayload'}&key=${googleMapApiKey}`;

    mock.onGet(url).reply(200, {
      status: 'OK',
      results: [
        {
          geometry: {
            location: 'hello'
          }
        }
      ]
    });
    axios
      .get(url)
      .then(async () => {
        const result = await fetchIdentityVerificationProvidersService();
        expect(result).toEqual('hello');
      })
      .catch(error => logError(error));
  });

  it('verifyUserIdentityService error test', async () => {
    const { url } = apiEndpoints.IDENTITY_VERIFICATION.VERIFY_USER_IDENTITY;
    mock.onPost(url).reply(200, {
      status: 'NOK',
      results: [
        {
          geometry: {
            location: 'hello'
          }
        }
      ]
    });
    axios.post(url).then(async () => {
      try {
        await verifyUserIdentityService({
          providerId: 'string',
          bankId: null,
          providerType: 'string',
          serviceId: 'string'
        });
      } catch (e) {
        logError(e);
      }
    });
  });

  it('verifyUserUsingOtpService error test', async () => {
    const { url } = apiEndpoints.IDENTITY_VERIFICATION.OTP_VERIFICATION;
    mock.onPost(url).reply(200, {
      status: 'NOK',
      results: [
        {
          geometry: {
            location: 'hello'
          }
        }
      ]
    });
    axios.post(url).then(async () => {
      try {
        await verifyUserUsingOtpService({
          action: 'string',
          mobileNumber: 'string',
          nonce: 'string',
          otp: 'string',
          otpContext: 'string'
        });
      } catch (e) {
        logError(e);
      }
    });
  });
});
