import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import { logError } from '@src/common/utils';
import * as identityVerificationProvidersApi from '@common/types/api/identityVerification/identityVerificationProviders';
import * as verifyUserUsingOtpApi from '@common/types/api/identityVerification/validateUserUsingOtp';
import * as validateUserUsingBankPostApi from '@common/types/api/identityVerification/validateUserUsingBankPost';

export const fetchIdentityVerificationProvidersService = async (): Promise<
  identityVerificationProvidersApi.GET.IResponse | Error
> => {
  const { url } = apiEndpoints.IDENTITY_VERIFICATION.GET_VERIFICATION_PROVIDERS;
  try {
    return await apiCaller.get(url);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const verifyUserIdentityService = async (
  payload: identityVerificationProvidersApi.POST.IRequest
): Promise<identityVerificationProvidersApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.IDENTITY_VERIFICATION.VERIFY_USER_IDENTITY;
  try {
    return await apiCaller.post(url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const verifyUserUsingOtpService = async (
  payload: verifyUserUsingOtpApi.POST.IRequest
): Promise<verifyUserUsingOtpApi.POST.IResponse | Error> => {
  const { url } = apiEndpoints.IDENTITY_VERIFICATION.OTP_VERIFICATION;
  try {
    return await apiCaller.post(url, payload);
  } catch (e) {
    logError(e);
    throw e;
  }
};

export const verifyUserByBankPostService = async (
  payload: validateUserUsingBankPostApi.POST.IRequest
): Promise<void | Error> => {
  const { requestPayLoadKey, requestPayLoadValue, requestUrl: url } = payload;
  try {
    if (url) {
      const form = document.createElement('form');
      form.method = 'post';
      form.action = url;
      const hiddenField = document.createElement('input');
      hiddenField.type = 'hidden';
      hiddenField.name = requestPayLoadKey as string;
      hiddenField.value = requestPayLoadValue as string;

      form.appendChild(hiddenField);

      document.body.appendChild(form);
      form.submit();
    }
  } catch (e) {
    logError(e);
  }
};
