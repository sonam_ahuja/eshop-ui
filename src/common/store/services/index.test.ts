import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import apiEndpoints from '@src/common/constants/apiEndpoints';
import store from '@common/store';
import { logError } from '@common/utils';

import { getLocationOfAddress } from './googleMap';

// tslint:disable-next-line:no-big-function
describe('GoogleMap Service', () => {
  const mock = new MockAdapter(axios);
  it('getLocationOfAddressService success test', async () => {
    const googleMapApiKey = store.getState().configuration.cms_configuration
      .global.googleMap.apiKey;
    const url = `${
      apiEndpoints.GOOGLE_MAP.GEOCODE.url
    }json?address=${'loginPayload'}&key=${googleMapApiKey}`;

    mock.onGet(url).reply(200, {
      status: 'OK',
      results: [
        {
          geometry: {
            location: 'hello'
          }
        }
      ]
    });
    axios.get(url).then(async () => {
      const result = await getLocationOfAddress('loginPayload');
      expect(result).toEqual('hello');
    });
  });

  // tslint:disable-next-line:no-identical-functions
  it('getLocationOfAddressService error test', async () => {
    const googleMapApiKey = store.getState().configuration.cms_configuration
      .global.googleMap.apiKey;
    const url = `${
      apiEndpoints.GOOGLE_MAP.GEOCODE.url
    }json?address=${'loginPayload'}&key=${googleMapApiKey}`;

    mock.onGet(url).reply(200, {
      status: 'NOK',
      results: [
        {
          geometry: {
            location: 'hello'
          }
        }
      ]
    });
    // tslint:disable-next-line:no-identical-functions
    axios.get(url).then(async () => {
      try {
        await getLocationOfAddress('loginPayload');
      } catch (e) {
        logError(e);
      }
    });
  });
});
