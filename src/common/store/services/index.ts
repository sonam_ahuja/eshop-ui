import { ITariffState } from '@tariff/store/types';
import store from '@common/store';
import { RootState } from '@src/common/store/reducers';
import { IConfigurationState } from '@common/store/types/configuration';

export const getState = (): RootState => {
  return store.getState();
};

export const getTariffState = (): ITariffState => {
  const state: RootState = getState();

  return state.tariff;
};

export const getConfigurationState = (): IConfigurationState => {
  const state: RootState = getState();

  return state.configuration;
};
