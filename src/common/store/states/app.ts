import shippingState from '@checkout/store/shipping/state';
import creditCheckState from '@checkout/store/creditCheck/state';
import personalInfoState from '@checkout/store/personalInfo/state';
import numberPortingState from '@checkout/store/numberPorting/state';
import orderSummaryState from '@checkout/store/orderSummary/state';
import basketState from '@basket/store/state';
import { RootState } from '@common/store/reducers';
import checkoutState from '@checkout/store/state';
import authenticationState from '@authentication/store/state';
import orderReviewState from '@checkout/store/orderReview/state';
import commonState from '@store/states/common';
import translationState from '@store/states/translation';
import configurationState from '@store/states/configuration';
import paymentInfoState from '@checkout/store/payment/state';
import billingState from '@checkout/store/billing/state';
import categoryState from '@category/store/state';
import ProductsListState from '@productList/store/state';
import ProductsDetailedState from '@productDetailed/store/state';
import orderConfirmationState from '@routes/orderConfirmation/store/state';
import searchState from '@routes/search/store/state';
import TariffState from '@tariff/store/state';
import LandingPageState from '@home/store/state';

export default (): RootState => ({
  basket: basketState(),
  translation: translationState(),
  englishTranslation: translationState(),
  configuration: configurationState(),
  common: commonState(),
  authentication: authenticationState(),
  productList: ProductsListState(),
  orderConfirmation: orderConfirmationState(),
  checkout: {
    shippingInfo: shippingState(),
    orderSummary: orderSummaryState(),
    payment: paymentInfoState(),
    creditCheck: creditCheckState(),
    billing: billingState(),
    personalInfo: personalInfoState(),
    checkout: checkoutState(),
    orderReview: orderReviewState(),
    numberPorting: numberPortingState()
  },
  categories: categoryState(),
  productDetailed: ProductsDetailedState(),
  search: searchState(),
  tariff: TariffState(),
  landingPage: LandingPageState()
});
