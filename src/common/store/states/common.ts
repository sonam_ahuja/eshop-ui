import { ICommonState } from '@store/types/common';

// tslint:disable
export default (): ICommonState => ({
  errorToast: {
    message: '',
    isOpen: false
  },
  error: {
    showFullPageError: false,
    skipGenericError: false,
    httpStatusCode: '',
    code: '',
    retryable: false,
    message: {
      debugMessage: '',
      userMessage: ''
    }
  },
  loading: false,
  isLoggedIn: false,
  isLoginButtonClicked: false,
  routeFromBasketToLogin: false,
  msisdnValue: '',
  currency: 'Ft',
  showOfflineToast: false,
  megaMenu: [],
  megaMenuFooter: [],
  deviceList: 'direct',
  landingPageDeviceList: 'home',
  deviceDetailedList: 'direct',
  userDetails: {
    id: '',
    status: '',
    relatedParties: [
      {
        id: '',
        role: '',
        name: ''
      }
    ],
    contactMediums: [
      {
        type: '',
        role: {
          name: ''
        },
        medium: {},
        preferred: false
      }
    ]
  },
  userIdentityVerificationDetails: {
    verificationRequired: false,
    identificationProviders: {
      COURIER: {
        providerType: '',
        providerSublist: [
          {
            providerId: ''
          }
        ]
      },
      TELEKOM: {
        providerType: '',
        providerSublist: [
          {
            providerId: ''
          }
        ]
      },
      BANK: {
        providerType: '',
        providerSublist: [
          {
            providerId: ''
          }
        ]
      }
    }
  },
  prolongation: {
    authorizationToken: null
  }
});
