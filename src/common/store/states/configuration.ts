import {
  IConfigurationState,
  ProlongationDiscountEnum
} from '@store/types/configuration';
import { PAGE_THEME, TARIFF_LIST_TEMPLATE } from '@common/store/enums/index';
import {
  TARIFF_BENEFIT_TEMPLATE_TYPE,
  TARIFF_BENEFIT_TEMPLATES
} from '@common/constants/appkeys';
import { PAYMENT_TYPE } from '@src/common/routes/checkout/store/enums';

// tslint:disable-next-line:no-big-function
export default (): IConfigurationState => ({
  cms_configuration: {
    global: {
      messageStripTimer: 30000,
      showLandingPage: true,
      googleSiteVerification: '',
      enableGoogleAddressSuggestion: false,
      geoCountryCode: 'CZ',
      categoryLandingPageRequired: true,
      prolongation: {
        bffBaseUrl: 'https://bfftest.yo-digital.com/bff/api',
        getRequestTimeout: 60000,
        postPatchRequestTimeout: 60000,
        consentDiscount: 'ignore' as ProlongationDiscountEnum,
        configureConsentDiscount: 5,
        applyConsentDiscount: false,
        isProfilePrefilled: false
      },
      eShop: { getRequestTimeout: 60000, postPatchRequestTimeout: 60000 },
      countryKeyboard: 'en',
      mqtHost: 'eshop-rabbitmq-ws.yo-digital.com',
      mqtPort: 443,
      mqtReconnectTimeout: 100,
      numberOfRetries: 8,
      eventTopicName: 'eshop.pl.event1',
      mqtUserName: 'eshopwo_natco_pl1',
      mqtPassword: 'z)1:!W2T+`kcbgVN',
      mqtVhost: 'eshoppl',
      scriptForGTA: `<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-TCZVJMD');</script>`,
      noScriptForGTA: `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCZVJMD"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`,
      creditCheckRequired: false,
      search: {
        searchDebounce: 400,
        isFilterSticky: false
      },
      braintreeAuthorization: 'sandbox_ndy7h4dn_bpcttkkktqnk9b4r',
      jsonLd: {
        corporateContactAndLogo: {
          show: true,
          telephone: '+1505-998-3793',
          contactType: 'customer service',
          logo: 'https://i.ibb.co/ZYB5VDq/T-mobile.png'
        },
        webPage: {
          show: true,
          id: 'webpage',
          inLanguage: 'en-US',
          name: 'T - Mobile E-commerce',
          isPartOf: 'website',
          about: 'E - Commerce',
          description: 'E - commerce website for T - mobile'
        },
        sitelinksSearchbox: {
          show: true,
          id: 'webpage',
          name: 'T - Mobile E-commerce',
          publisherId: 'organization'
        },
        iosApplication: {
          show: false,
          name: 'T - Mobile ios',
          operatingSystem: 'ios',
          applicationUrl:
            'https://itunes.apple.com/us/app/t-mobile/id561625752?mt=8',
          downloadUrl:
            'https://itunes.apple.com/us/app/t-mobile/id561625752?mt=8',
          aggregateRating: {
            ratingValue: '4.8',
            ratingCount: '5'
          },
          softwareVersion: '4.8.16',
          // tslint:disable-next-line: no-duplicate-string
          releaseNotes: 'Bug fixes and improvements'
        },
        androidApplication: {
          show: false,
          name: 'T - Mobile',
          operatingSystem: 'andriod',
          applicationUrl:
            'https://play.google.com/store/apps/developer?id=T-Mobile+USA',
          downloadUrl:
            'https://play.google.com/store/apps/details?id=com.tmobile.pr.mytmobile&hl=en',
          aggregateRating: {
            ratingValue: '4.3',
            ratingCount: '5'
          },
          softwareVersion: 'Varies with device',
          releaseNotes: 'Bug fixes and improvements'
        },
        webApplication: {
          show: true,
          name: 'T - Mobile',
          operatingSystem: 'Windows',
          applicationUrl:
            'https://eshop-oneshop-nginx-stage.eshop.yo-digital.com/',
          aggregateRating: {
            ratingValue: '4.3',
            ratingCount: '5'
          },
          softwareVersion: 'Varies with device',
          releaseNotes: 'Bug fixes and improvements'
        },
        socialProfile: {
          show: true,
          id: 'author',
          name: 'T -mobile',
          description: 'T - Mobile E -commerce website',
          caption: 'T - mobile'
        },
        sameAs: {
          facebook: 'https://www.facebook.com/TMobile/',
          instagram: 'https://www.instagram.com/tmobile',
          linkedin: 'https://www.linkedin.com/company/t-mobile',
          youtube: 'https://www.youtube.com/tmobile',
          pinterest: 'https://www.pinterest.com/TMobilePZ',
          wikipedia: 'https://en.wikipedia.org/wiki/T-Mobile',
          twitter: 'https://twitter.com/TMobile'
        }
      },
      googleMap: {
        apiKey: 'AIzaSyAquQSuu-Tk-U4rJpCW7b5T0lpx44NxNzU',
        rangeCover: 10,
        zoom: 15,
        center: {
          lat: 50.712319,
          lng: 7.1210148
        }
      },
      termsAndConditionsUrl:
        'https://eshop-oneshop-ui.s3.eu-central-1.amazonaws.com/development/nodejs_tutorial.pdf',
      orderTrackingUrl: 'https://www.google.com',
      shouldTermsAndConditionsOpenInNewTab: true,
      currency: { currencySymbol: 'Ft', isPrecede: false, locale: 'PLN' },
      stickyOrderSummary: {
        mobile: true,
        desktop: false,
        summarySnapshotFrequency: 200
      },
      paymentMethods: {
        availablePaymentMode: {
          maestro: { show: true },
          applePay: { show: true },
          googlePay: { show: true },
          visa: { show: true },
          payPal: { show: true },
          worldPay: { show: true }
        },
        upfront: {
          tokenizedCard: { show: true, labelKey: 'tokenizedCard' },
          payOnDelivery: { show: false, labelKey: 'payOnDelivery' },
          payByLink: { show: false, link: 'string', labelKey: 'payByLink' },
          bankAccount: { show: false, labelKey: 'bankAccount' },
          manualPayments: { show: false, labelKey: 'manualPayments' },
          defaultPaymentMethod: PAYMENT_TYPE.CREDIT_DEBIT_CARD
        },
        monthly: {
          tokenizedCard: { show: false, labelKey: 'tokenizedCard' },
          payOnDelivery: { show: true, labelKey: 'payOnDelivery' },
          payByLink: { show: false, link: 'string', labelKey: 'payByLink' },
          bankAccount: { show: false, labelKey: 'bankAccount' },
          manualPayments: { show: false, labelKey: 'manualPayments' },
          defaultPaymentMethod: PAYMENT_TYPE.CREDIT_DEBIT_CARD
        }
      },
      loginFormRules: {
        password: {
          minLength: 1,
          minNumberOfDigits: 0,
          minLowerCaseLetters: 1,
          minUpperCaseLetters: 0
        },
        phoneNumber: {
          minLength: 8,
          maxLength: 12
        },
        userName: {
          minLength: 1,
          maxLength: 8
        },
        email: {
          regex:
            '^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$'
        }
      },
      loginWithEmailConfirmation: false,
      loginRegistrationMethods: {
        emailOTP: false,
        MSISDNOTP: true,
        usernamePassword: true,
        googleAccount: true,
        automaticMSISDN: true,
        socialAccount: true,
        QRCodeScan: true
      },
      resendOTPWaitingPeriod: {
        mail: 500,
        sms: 300
      },
      authentication: {
        numberofOtp: 6,
        encryption: {
          publicKey: ''
        },
        apiLatency: 3000,
        masking: {
          email: '^(.)(.*)(.@.*)$',
          phone: '^(.)(.*)(.[0-9].*)$'
        }
      },
      footer: {
        customerCareNumber: '0123341',
        storeLocatorLink: 'www.asdfasdf.com',
        newsletterLink: 'www.hello.com',
        playStoreLink: '',
        appStoreLink: '',
        socialMedia: {
          facebook: {
            key: 'facebook',
            show: true,
            link: 'www.facebook.com'
          },
          twitter: {
            key: 'twitter',
            show: true,
            link: 'www.twitter.com'
          },
          linkedin: {
            key: 'linkedin',
            show: true,
            link: 'www.linkedin.com'
          },
          instagram: {
            key: 'instagram',
            show: true,
            link: 'www.instagram.com'
          },
          youtube: {
            key: 'youtube',
            show: true,
            link: 'www.youtube.com'
          }
        },
        footerStaticLinks: {
          aboutUs: '',
          contact: '',
          privacyPolicy: '',
          cookies: '',
          support: '',
          secure: ''
        }
      },
      header: {
        headertype: 1,
        headerColumns: 4,
        showHeaderStrip: false,
        dtLogo: {
          isMobileLogoDifferent: true,
          desktop: {
            url: '',
            imageAltText: 'header image',
            aspectRatio: 1,
            height: '',
            width: ''
          },
          mobile: {
            url: '',
            imageAltText: 'Header Image',
            aspectRatio: 1,
            height: '',
            width: ''
          },
          heightWidthUnit: ''
        },
        privateLink: 'www.fb.com',
        businessLink: 'www.ggl.com',
        touristLink: 'www.apple.com',
        specialOfferImageUrl: 'https://i.ibb.co/6ysRdFg/mobile-In-Hand.png',
        specialOfferLink: '/Devices-Mobile'
      }
    },
    modules: {
      landingPage: {
        itemsPerPage: 4,
        categoryId: 'Devices-Mobile',
        showMessageStrip: false,
        downloadIOSLink: '',
        downloadAndroidLink: ''
      },
      login: {
        social: {
          google: {
            client: {
              id:
                '1002008194487-uakbnfuqqakfh4brcdmfo86v45m259ds.apps.googleusercontent.com',
              scope: 'https://www.googleapis.com/auth/analytics',
              cookiePolicy: 'single_host_origin',
              script: 'https://apis.google.com/js/platform.js'
            }
          }
        }
      },
      basket: {
        continueShoppingUrl: '',
        summarySnapshotFrequency: 20
      },
      productList: {
        itemPerPage: 2,
        itemPerPageList: '8,10,12',
        viewAllLink: '',
        showViewAllLink: true,
        openViewAllInTab: true,
        sortOutStock: false,
        paginationType: 'infinite',
        backgroundImage: '',
        theme: PAGE_THEME.SECONDARY
      },
      productDetailed: {
        showBasketDrawer: true,
        storage: 'storage',
        color: 'colour',
        numberOfInstalments: 'numberOfInstalments',
        theme: PAGE_THEME.SECONDARY,
        showDisabledAttributes: true,
        disabledAttributesClickable: true,
        newProductBeforeLaunchDays: 1,
        buyDeviceWithInstallments: true,
        productDetailedTemplate1: {
          showOnlineStockAvailability: true,
          showVariantSelection: false,
          showDevicePrice: true,
          showTariffInfo: true,
          showHighlightPromo: true,
          // showUnlockDiscount: true,
          // showTodayPrice: true,
          showbadges: true,
          // showQuantity: true,
          // showAddToCompare: true,
          showNotifyMe: true,
          showOverViewSection: true,
          showTechnicalSpecification: true
          // showUpSellSection: true,
          // showEnableZoomEffect: true
        }
      },
      checkout: {
        orderConfirmation: {
          showTrackingLink: true
        },
        form: {
          fields: {
            firstName: {
              order: 1,
              labelKey: 'firstName',
              hidden: true,
              inputType: 'text',
              show: true,
              readOnly: true,
              mandatory: true,
              creditCheckField: false,
              validation: {
                type: 'none',
                value: '0-9',
                // tslint:disable-next-line:no-duplicate-string
                message: 'Length is exceeded'
              }
            },
            lastName: {
              order: 2,
              labelKey: 'lastName',
              hidden: true,
              inputType: 'text',
              show: true,
              readOnly: true,
              mandatory: true,
              creditCheckField: false,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            email: {
              order: 4,
              labelKey: 'email',
              show: true,
              hidden: true,
              inputType: 'text',
              readOnly: true,
              creditCheckField: false,
              mandatory: true,
              validation: {
                type: 'none',
                value: 'abc',
                message: 'Length is exceeded'
              }
            },
            phoneNumber: {
              order: 5,
              labelKey: 'phoneNumber',
              hidden: true,
              inputType: 'number',
              show: true,
              readOnly: true,
              creditCheckField: false,
              mandatory: false,
              validation: {
                type: 'min',
                value: '2',
                message: 'Length is exceeded'
              }
            },
            company: {
              order: 9,
              labelKey: 'company',
              hidden: true,
              inputType: 'text',
              show: true,
              readOnly: true,
              creditCheckField: false,
              mandatory: false,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            streetNumber: {
              order: 12,
              labelKey: 'streetNumber',
              hidden: true,
              inputType: 'text',
              show: true,
              creditCheckField: false,
              readOnly: true,
              mandatory: true,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            flatNumber: {
              order: 10,
              labelKey: 'flatNumber',
              hidden: true,
              inputType: 'number',
              show: true,
              creditCheckField: false,
              readOnly: false,
              mandatory: true,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            city: {
              order: 10,
              labelKey: 'city',
              hidden: true,
              inputType: 'text',
              show: true,
              creditCheckField: false,
              readOnly: false,
              mandatory: true,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            postCode: {
              order: 10,
              labelKey: 'postCode',
              hidden: true,
              inputType: 'text',
              show: true,
              creditCheckField: false,
              readOnly: false,
              mandatory: true,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            address: {
              order: 6,
              labelKey: 'streetAddress',
              hidden: true,
              inputType: 'text',
              show: true,
              creditCheckField: false,
              readOnly: false,
              mandatory: true,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            oibNumber: {
              order: 8,
              labelKey: 'oibNumber',
              hidden: true,
              creditCheckField: true,
              inputType: 'text',
              show: true,
              readOnly: true,
              mandatory: false,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            pesel: {
              order: 8,
              labelKey: 'pesel',
              hidden: true,
              creditCheckField: false,
              inputType: 'text',
              show: true,
              readOnly: true,
              mandatory: false,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            idNumber: {
              order: 7,
              labelKey: 'idNumber',
              hidden: true,
              creditCheckField: true,
              inputType: 'text',
              show: true,
              readOnly: true,
              mandatory: false,
              validation: {
                type: 'none',
                value: '6',
                message: 'Length is exceeded'
              }
            },
            dob: {
              order: 3,
              labelKey: 'dob',
              hidden: true,
              inputType: 'date',
              show: true,
              creditCheckField: false,
              readOnly: false,
              mandatory: false,
              validation: {
                type: 'none',
                value: '',
                message: 'Length is exceeded'
              }
            }
          }
        },
        shipping: {
          form: {
            fields: {
              streetAddress: {
                order: 1,
                labelKey: 'streetAddress',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '10-40',
                  message: 'Length is exceeded'
                }
              },
              city: {
                order: 2,
                labelKey: 'city',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '6-13',
                  message: 'Length is exceeded'
                }
              },
              postCode: {
                order: 3,
                labelKey: 'postCode',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'max',
                  value: '5',
                  message: 'Length is exceeded'
                }
              },
              streetNumber: {
                order: 4,
                labelKey: 'streetNumber',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '0-40',
                  message: 'Length is exceeded'
                }
              },
              flatNumber: {
                order: 1,
                labelKey: 'flatNumber',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: false,
                validation: {
                  type: 'between',
                  value: '1-40',
                  message: 'Length is exceeded'
                }
              },
              unit: {
                order: 5,
                labelKey: 'unit',
                hidden: true,
                inputType: 'text',
                show: false,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '0-40',
                  message: 'Length is exceeded'
                }
              },
              deliveryNote: {
                order: 6,
                labelKey: 'deliveryNote',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '0-40',
                  message: 'Length is exceeded'
                }
              }
            }
          },
          shippingType: {
            deliverToAddress: {
              show: false,
              deliveryOptions: {
                standard: { show: false },
                withinTime: { show: false },
                pickADate: { show: false }
              }
            },
            pickUpAtStore: { show: false },
            parcelLocker: { show: false },
            pickUpPoints: { show: false }
          }
        },
        billingInfo: {
          form: {
            fields: {
              streetAddress: {
                order: 1,
                labelKey: 'streetAddress',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '10-40',
                  message: 'Length is exceeded'
                }
              },
              flatNumber: {
                order: 1,
                labelKey: 'flatNumber',
                hidden: true,
                inputType: 'text',
                show: false,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '10-40',
                  message: 'Length is exceeded'
                }
              },
              deliveryNote: {
                order: 1,
                labelKey: 'deliveryNote',
                hidden: true,
                inputType: 'text',
                show: false,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '10-40',
                  message: 'Length is exceeded'
                }
              },
              streetNumber: {
                order: 2,
                labelKey: 'streetNumber',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '10-40',
                  message: 'Length is exceeded'
                }
              },
              city: {
                order: 3,
                labelKey: 'city',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '6-13',
                  message: 'Length is exceeded'
                }
              },
              postCode: {
                order: 4,
                labelKey: 'postCode',
                hidden: true,
                inputType: 'text',
                show: true,
                readOnly: false,
                mandatory: true,
                validation: {
                  type: 'between',
                  value: '10-40',
                  message: 'Length is exceeded'
                }
              }
            }
          },
          billingType: {
            sameAsPersonalInfo: {
              show: true
            },
            sameAsShippingInfo: {
              show: true
            },
            differentAddress: {
              show: true
            },
            eBill: {
              default: true
            },
            paperBill: {
              default: false
            }
          },
          eBillDiscount: true,
          skipBillingStep: true
        },
        orderNotification: {
          sms: true
        },
        orderReview: {
          termsAndConditionsUrl: 'www.google.com',
          isTermsAndConditionCMSDriven: false,
          shouldTermsAndConditionsOpenInNewTab: true,
          enablePaymentProcess: true,
          expandConsentByDefault: false,
          hideAllConsentBtn: false
        },
        mns: {
          categoryId: 'mns',
          show: false,
          labelKey: 'changeNumber'
        },
        mnp: {
          categoryId: 'mnp',
          disableWeekends: true,
          showDateList: false,
          serviceStatusType: {
            hybrid: {
              show: true,
              migrationOptions: {
                desiredDate: {
                  show: true,
                  labelKey: 'desiredDate',
                  value: 5
                },
                endOfPromotion: {
                  show: true,
                  labelKey: 'endOfPromotion'
                },
                endOfAgreement: {
                  show: true,
                  labelKey: 'endOfAgreement'
                }
              },
              labelKey: 'hybridText'
            },
            postpaid: {
              show: true,
              migrationOptions: {
                desiredDate: {
                  show: true,
                  labelKey: 'desiredDate',
                  value: 10
                },
                endOfPromotion: {
                  show: false,
                  labelKey: 'endOfPromotion'
                },
                endOfAgreement: {
                  show: true,
                  labelKey: 'endOfAgreement'
                }
              },
              labelKey: 'postpaidText'
            },
            prepaid: {
              show: true,
              migrationOptions: {
                desiredDate: {
                  show: true,
                  labelKey: 'desiredDate',
                  value: 15
                },
                endOfPromotion: {
                  show: true,
                  labelKey: 'endOfPromotion'
                },
                endOfAgreement: {
                  show: true,
                  labelKey: 'endOfAgreement'
                }
              },
              labelKey: 'prepaidText'
            }
          },
          show: false,
          labelKey: 'portNumber',
          verificationMethods: {
            MSISDNOTP: true
          }
        },
        identityVerification: {
          enabled: true,
          isPeselfieldNeeded: false
        },
        searchSize: 10
      },
      category: {
        backgroundImage: '',
        theme: PAGE_THEME.SECONDARY
      },
      tariff: {
        tariffListingTemplate: TARIFF_LIST_TEMPLATE.DATA_FIRST,
        defaultTariffBenefitTemplate: TARIFF_BENEFIT_TEMPLATES.BUTTON,
        viewAllLink: '',
        showViewAllLink: true,
        mobileNewTariffTemplateEnabled: true,
        tariffRenderingOrder: '2,2,1',
        openViewAllInTab: true,
        defaultTariffBenefitTemplateType:
          TARIFF_BENEFIT_TEMPLATE_TYPE.SINGLE_SELECT,
        tariffBenefitTemplates: {
          freeApp: {
            templateName: TARIFF_BENEFIT_TEMPLATES.DROPDOWN,
            templateType: TARIFF_BENEFIT_TEMPLATE_TYPE.SINGLE_SELECT
          },
          extraData: {
            templateName: TARIFF_BENEFIT_TEMPLATES.DROPDOWN,
            templateType: TARIFF_BENEFIT_TEMPLATE_TYPE.SINGLE_SELECT
          }
        }
      },
      prolongation: {
        termsAndConditionsUrl:
          'https://eshop-oneshop-ui.s3.eu-central-1.amazonaws.com/development/nodejs_tutorial.pdf',
        form: {
          firstName: {
            maxLength: 150,
            validation: '',
            validationMessage: 'Enter valid first name',
            readOnly: false,
            mandatory: false,
            inputType: 'text'
          },
          lastName: {
            maxLength: 150,
            validation: '',
            validationMessage: 'Enter valid last name',
            readOnly: false,
            mandatory: false,
            inputType: 'text'
          },
          phoneNumber: {
            maxLength: 150,
            validation: '',
            validationMessage: 'Enter valid phone number',
            readOnly: false,
            mandatory: false,
            inputType: 'text'
          },
          email: {
            validation: '',
            validationMessage: 'Email is invalid',
            readOnly: false,
            mandatory: false,
            maxLength: 150,
            inputType: 'text'
          },
          deliveryContact: {
            validation: '',
            validationMessage: 'Enter valid number (Please remove as optional)',
            maxLength: 12,
            readOnly: false,
            mandatory: false,
            inputType: 'text'
          },
          deliveryAddress: {
            maxLength: 150,
            validation: '',
            validationMessage: 'Enter valid address',
            readOnly: false,
            mandatory: false,
            inputType: 'textarea'
          }
        },
        gaCode: 'UA-141175713-1',
        publicKey: `-----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn25Q/GCUtESOZJsYyslY
        6NNmHksnDZN2QDWAyEIiHX+rEFp/e0T7DGLtVy1qjXPp0TwAB8SuGDZsKS2lYSxu
        UTQ7DAQo34KgF7bdNxkxD7+MmdKUwwXxtXsHlosQxo4lTLhgeh1/70wSYQ9Yn9GA
        a9Gehhai/Ad3hkgYPEmMIuD5LgeJFz4yrEgq4WmEQm2X+d8Dhzg1gs8s/M2r0wMy
        B7+kPCox6TdBhY5WfN5MTQAywRdR/gv1eDIgSAqVVey/Rb8d79sompdGp4Lschvi
        ZR6ap+URGOcSAN9iEJHjJRh8lFUzrz2EcOuwTQzYH9MN2xE8zKf3yXZtgX5vvghy
        gQIDAQAB
        -----END PUBLIC KEY-----`,
        landingPageTitle: 'landingPageTitle',
        personalDetailsPageTitle: 'personalDetailsPageTitle',
        orderPageTitle: 'orderPageTitle',
        webviewCloseTime: 2000
      },
      catalog: {
        preorder: {
          considerInventory: true
        }
      }
    }
  }
  // tslint:disable-next-line:max-file-line-count
});
