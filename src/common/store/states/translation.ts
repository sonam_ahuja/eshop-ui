import { ITranslationState } from '@store/types/translation';

// tslint:disable
export default (): ITranslationState => ({
  prolongation: {
    global: {
      accepted: 'Zaakceptowano',
      notAccepted: 'Nie zaakceptowano',
      losingDiscountMessage:
        'Please make sure you click the marketing consents, otherwise your monthly fee would be higher',
      givenDiscountMessage: 'You have unlocked new discount',
      errors: {
        leadAlreadyExist: 'Lead already submitted',
        dtUnknownError: 'something is wrong',
        mandatoryMissing: 'segmentId or categoryId missing',
        variantsNotFound: 'Variants not found',
        handle401: '401 error',
        canceledMsg: 'api timed out',
        canceledStatusCode: '499'
      }
    },
    landing: {
      nowText: 'now local',
      monthlyShort: 'mo',
      heading: 'New phone and more internet'
    },
    checkout: {
      form: {
        firstName: {
          label: 'First Name'
        },
        lastName: {
          label: 'Last Name'
        },
        email: {
          label: 'E-mail'
        },
        phoneNumber: {
          label: 'Phone Number'
        },
        deliveryAddress: {
          label: 'Delivery Address'
        },
        deliveryContact: {
          label: 'Delivery Contact'
        }
      },
      personalInfo: {
        heading: 'personal Info',
        editText: 'edit'
      },
      termsAndConditionText: 'Terms & Conditions',
      agreementText: 'By placing order, you agree to Deutsche Telekom’s',
      placeOrder: 'Place Order',
      orderSummary: 'Order Summary',
      totalMonthly: 'Total Monthly',
      totalNow: 'total now',
      optionalText: '(Optional)'
    },
    orderConfirmation: {
      heading: 'Thank you for the placing you order',
      subHeading: 'You order was received',
      instructionMessage:
        'We have sent an email with the order details and we will contact you if necessary',
      backToHomeText: 'back to home'
    },
    productDetails: {
      highlightText: 'highlights',
      customiseYourPhone: 'Customize your phone',
      totalMonthly: 'total monthly',
      totalUpfront: 'total now',
      buyBundle: 'buy bundle',
      colour: 'Colour',
      storage: 'Storage',
      plan: 'Plan',
      ram: 'RAM',
      showMore: 'Show more',
      showLess: 'Show less'
    }
  },
  cart: {
    global: {
      checkout: {
        personalInfo: {
          title: 'Personal Info',
          description: ''
        },
        shipping: {
          title: 'Shipping',
          description: ''
        },
        payment: {
          title: 'Payment',
          description: ''
        },
        billing: {
          title: 'Billing',
          description: ''
        },
        orderReview: {
          title: 'Order Review',
          description: ''
        }
      },
      search: {
        filterInBetweenMessageText: 'results for',
        viewAll: 'View all',
        filters: {
          all: 'All',
          highlights: 'Highlights',
          devices: 'Devices',
          pages: 'Pages',
          faqs: 'FAQs'
        },
        categories: {
          highlights: 'Highlights',
          devices: 'Devices',
          pagesResults: 'Page Results',
          faqs: 'Frequently Asked Questions'
        },
        notFound: {
          noResultsFound: 'No Results Found',
          message: "Sorry, we couldn't find any results matching"
        },
        searchTips: {
          searchTipsText: 'Search tips',
          spellingText: 'Check your spelling.',
          generalText: 'Try more general words.',
          differentWordsText: 'Try different words that mean the same thing.',
          helpText:
            'If these or links below don’t work you can always contact us we’re glad to help.'
        },
        searchQueries: {
          firstQuery: 'mobiles',
          secondQuery: 'laptops',
          thirdQuery: 'televisions',
          fourthQuery: 'sim',
          fifthQuery: 'accessories',
          sixthQuery: 'tablets',
          seventhQuery: 'fixed services'
        },
        searchField: {
          mobile: {
            startTyping: 'Start typing',
            speakNow: 'Speak now',
            listening: 'Listening',
            doNotUnderstood: 'Didn’t understood',
            voiceSearchHasBeenTurnOff: 'Voice search off',
            checkMicAndAudio: 'Check mic audio levels',
            cancel: 'Cancel'
          },
          desktop: {
            startSpeaking: 'Start Speaking',
            startTyping: 'Start Typing',
            listening: 'Listening...',
            doNotUnderstood: 'Didn’t understood',
            voiceSearchHasBeenTurnOff: 'Voice search has been turned off',
            repeat: 'Repeat',
            checkMicAndAudio: 'Check mic and audio levels'
          }
        }
      },
      errors: {
        400: 'Bad Request',
        404: 'Page Not found',
        408: 'Request Time Out',
        440: 'Session expired',
        401: 'not allowed',
        500: 'Internal server error',
        502: 'Bad Gateway',
        description400: 'Bad Request',
        description404: 'Page Not found',
        description408: 'Request Time Out',
        description440: 'Session expired',
        description401: 'not allowed',
        description500: 'Internal server error description',
        description502: 'Bad Gateway',
        default: 'Something went wrong',
        backToHomePage: 'Back to Home Page',
        back: 'Back',
        retry: 'Retry',
        cancel: 'cancel',
        confirm: 'confirm',
        backToLogin: 'Back To Login',
        paymentMethodNotFound: 'Payment method is not found'
      },
      landScapeMode: {
        heading: 'Please rotate your phone.',
        subHeading: 'Browsing on Telekom is easier in portrait mode.'
      },
      companyName: 'Deutsche Telekom',
      offlineMessage: 'You are currently offline',
      termsAndConditions: 'Terms & Conditions',
      appName: 'T-shop',
      shortName: 'E-shop',
      home: 'home',
      landingPageTitle: 'Landing Page',
      landingPageDescription: 'Landing Page Description',
      products: 'products',
      basket: 'basket',
      footer: {
        storeLocator: 'store locator',
        aboutUs: 'About Us',
        downloadAppText:
          "Don't you have it still? Download our app and manage all of your services.",
        contact: 'Contact',
        privacyPolicy: 'Privacy Policy',
        cookies: 'cookies',
        support: 'support',
        secure: 'secure'
      },
      header: {
        privateText: 'Private',
        businessText: 'Business',
        touristText: 'Tourist',
        signInText: 'Sign In',
        logoutText: 'logout',
        specialOfferTitle: 'Special Offer',
        specialOfferDescription:
          'Special Offer Merge into Magenta 1 and get a 30 000 HUF discount on a new device!'
      },
      days: {
        monday: 'Monday',
        tuesday: 'Tuesday',
        wednesday: 'Wednesday',
        thursday: 'Thursday',
        friday: 'Friday',
        saturday: 'Saturday',
        sunday: 'Sunday'
      },
      categoryLandingPage: 'category landing page',
      ieNotSupported:
        'We are currently not live on Internet Explorer yet! Hang on tight, we will be available soon!'
    },
    landingPage: {
      heading: 'Telekom OneApp',
      downloadText: 'Download',
      subHeading: 'now &amp; get Extra 500MB!'
    },
    basket: {
      noLoyalty: 'no loaylity',
      priceChanged: 'Price of Some of items has changed. Please review',
      outOfStock: 'Out of Stock',
      clearDeviceText:
        'To proceed to Checkout, please change or clear devices ',
      preOrderText: 'Pre-Order',
      viewBasket: 'View Basket',
      startShopping: 'Start Shopping',
      after: 'after',
      months: 'months',
      or: 'or',
      selectOtherDevice: 'select other Device',
      clearBasket: 'clear Basket',
      home: 'Home',
      proceedToCheckout: 'Proceed to Checkout',
      upFront: 'upfront',
      summaryUpfrontHeading: 'summary Upfront heading',
      cardUpfrontHeading: 'card upfront heading',
      emptyBasket: {
        emptyBasketText: 'Your Basket is empty.',
        suggestionText: 'Feel like exploring our',
        bestDealText: 'Best Deals?',
        bestDealLink: 'http://www.google.com'
      },
      popUps: {
        removeButton: 'Remove',
        removeOneItem: 'Are you sure you want to remove this item?',
        deviceProtectionText:
          'To which device you want to add a Device protection?',
        removeAllItem:
          'Are you sure you want to remove all items from your basket?'
      },
      finalPrice: 'final Price',
      discount: 'Discounts',
      discountType: {
        eBillDiscount: 'eBill Discount',
        bundleDiscount: 'Bundle Discount',
        loyaltyDiscount: 'Loyalty Discount',
        otherDiscount: 'Other Discount',
        christmasDiscount: 'Christmas Discount'
      },
      subTotal: 'subTotal',
      subTotalMonthly: 'subTotal mothly',
      subTotalUpfront: 'subTotal upfront',
      delete: {
        allItem: 'all item deleted',
        oneItem: 'cart__basket__delete__one_item'
      },
      remove: 'remove',
      offer: 'offer',
      pageHeading: 'Basket',
      total: 'total',
      undo: 'undo',
      continueShopping: 'Continue Shopping',
      stripMessage: {
        cartRestrictionMessage:
          'Please remove an item if you want to add other item',
        prodOutOfStock: 'There are products out of stock',
        allItemRemove: 'All items were removed from your basket',
        saveItemInBasketMsg: "We\\'ve saved the items in basket.",
        oneItemRemove: 'Item was removed from your basket',
        promoMsg: 'Enjoy 15% off with promocode',
        promoCode: '15DISCOUNT'
      },
      relatedProduct: 'related product',
      installments: 'installments',
      sorryText: 'Sorry, the {0} is currently',
      outOfStockText: 'is out of stock',
      termsAndCondition: 'terms and condition',
      summaryHeading: 'Summary',
      basePrice: 'base Price',
      totalMonthly: 'total Monthly',
      monthlyPrice: 'monthly Price',
      changeDeviceOptions: 'Change Device Options',
      recommended: 'recommended',
      shipping: 'Shipping Fee',
      changeVariant: 'change variant',
      monthly: 'monthly',
      retailPrice: 'retail price',
      totalDueToday: 'total Due Today',
      totalUpFront: 'total UpFront',
      loadingQuantity: 'Updating quantity',
      free: 'Free',
      ebill: 'E-Bill',
      paperBill: 'Paper Bill',
      totalDueDate: 'Total Due Date',
      item: 'item',
      items: 'items'
    },
    productList: {
      preOrderCardTitle: 'Pre-Order',
      noProductText: 'Oops, couldn’t load mobile phones.',
      comeBackText: 'Come back later or reload page. ',
      simplifiedPagination: {
        previous: 'Previous',
        next: 'Next'
      },
      showMorePagination: {
        loadMore: 'Load More',
        loading: 'Loading',
        loaded: 'All Products loaded'
      },
      infinitePagination: {
        loading: 'Loading more devices...',
        loaded: 'All Products Loaded'
      },
      availability: {
        IN_STOCK: 'In Stock',
        OUT_OF_STOCK: 'Out Of Stock',
        PRE_ORDER: 'Pre Order'
      },
      perPage: 'per page',
      selectitemPerPage: 'select item per page',
      filterByTitle: 'Filter By',
      sortByTitle: 'Sort By',
      sortByDropdownLabel: 'sort',
      viewDetails: 'View details',
      notificationAction: 'ok, got it',
      viewResults: 'View {0} results',
      singularViewResults: 'View {0} result',
      viewAllPopularDevice: 'view all',
      filterLoadingText: 'Loading ...',
      outOfStock: 'Out of stock',
      description: 'Displaying {0} devices out of {1} {2} {3} {4}',
      singularDescription: 'Displaying {0} device out of {1} {2} {3} {4}',
      loadingText: 'Fetching data',
      validateButton: 'Validate',
      notificationTitle: "We'll notify you once {0} is back on stock.",
      notificationSuccessMobile:
        "Once the device is back in stock you\\'ll be notified on",
      notificationSuccessEmail:
        "Once the device is back in stock you\\'ll be notified on your email.",
      upFront: 'UpFront',
      priceRangeAboveFilter: 'Above',
      monthly: 'Monthly',
      outOfStockDescription: 'is out of stock',
      preOrder: 'Pre Order',
      launchDate: 'Recent Price',
      popularityIndex: 'most popular',
      discount: 'most discounted',
      lowestPrice: 'Lowest Price',
      bestDeviceOffers: 'Best device offers',
      clearAll: 'Clear All',
      popularAccessories: 'popular accessories',
      viewAll: 'view all',
      installments: 'installments',
      pricePerMonth: 'mo',
      titleSeparator: 'in',
      planTitleSeparator: 'on',
      filterCount: 2
    },
    productDetailed: {
      buyDeviceOnly: 'Buy Device Only',
      storage: 'storage1',
      noInstallments: ' No Installments',
      color: 'colour1',
      moSymbol: 'mo',
      noLoyalty: 'No Loyalty',
      installments: 'Installments',
      installmentLabel: 'Installments',
      withoutInstallmentLabel: 'device upfront',
      description: 'Description',
      totalMonthly: 'Total Monthly',
      upfront: 'Upfront',
      timer: 'timer',
      highlights: 'Highlights',
      preOrder: 'preOrder',
      disabledOutOfStock: 'Out Of Stock',
      technicalSpecification: 'Technical Specification',
      notifyMe: 'Notify me',
      addToBasket: 'Add to Basket',
      inStock: 'In Stock',
      loadingText: 'loading..',
      noImagePreview: 'no image available',
      outOfStock: 'Out of stock',
      tooltipTitle: 'titlee',
      fullDevicePrice: 'Full device price',
      specialDiscount: 'special discount',
      totalUpfront: 'total upfront',
      newDevice: 'New',
      days: 'Days',
      hours: 'Hrs',
      minutes: 'Min',
      seconds: 'sec',
      youngTariff: {
        titleText: 'What is you age',
        ageLimitText: 'Is your age between 18 to 36?',
        enjoyTariff: 'If yes, enjoy this tariff.',
        validate: 'Validate',
        placeHolder: 'Your Birth Date',
        errorMessage: 'Invalid Date Range'
      },
      seeAllPlans: 'See all plans'
    },
    category: {
      preOrderCardTitle: 'Pre-Order',
      categoryTitle: 'The perfect gift?',
      categorySubTitle: 'Find it here.',
      bestDeviceOfferHeading: 'The best device offers.'
    },
    checkout: {
      personalInfo: {
        lastName: 'Last Name',
        updateSmsText: 'Receive also by SMS?',
        consentText: 'By clicking you give us consent to use this data',
        idNumber: 'ID Number',
        personalInfoText: 'Personal Information',
        proceedToCreditButton: 'Proceed',
        postCode: 'Post Code',
        city: 'City',
        flatNumber: 'Flat number',
        streetNumber: 'Street No',
        firstName: 'First Name',
        proceedToShipping: 'Proceed to Shipping',
        phoneNumber: 'Phone Number',
        // tslint:disable-next-line:no-duplicate-string
        streetAddress: 'Street Address',
        oibNumber: 'OIB Number',
        pesel: 'PESEL',
        company: 'Company',
        updateEmailText: 'Updates will be sent by email',
        email: 'E-mail',
        dob: 'Birth Date',
        sideNavText: 'OIB number should be right to check your credit',
        changeOrPortNumberText: 'Change or Port Number',
        changeNumber: 'Change Number',
        portNumber: 'Port Number',
        mnpInitialText: `your new phone`,
        numberIs: `number is`,
        mnpPortedText: 'your phone number {0} is being ported on ',
        editPort: 'Edit Port',
        deliveryNoteText: 'Delivery Note',
        cancel: 'Cancel',
        cancelPortText: 'Yes, Cancel Port',
        cancelMessage: 'Are you sure want to cancel the scheduled port of {0}'
      },
      mnp: {
        migrationHeaderText: 'Your current plan for ',
        is: 'is',
        portOn: 'Port on',
        desiredDate: 'Desired Date',
        otpHeadingText: 'We have sent OTP to {0} number',
        prepaidText: 'Prepaid',
        nextText: 'Next',
        endOfPromotionText: 'End of Promotion',
        portOnEndOfAgreement: 'Port on Withdrawal',
        helloText: 'Hello !',
        portMyNumberText: 'Port My Number',
        postpaidText: 'Postpaid',
        phoneNumberLabelText: 'Enter your phone number',
        portOnEndOfAgreementMessage:
          '{0} will be transfered on the last day of current full bill cycle',
        headingText: 'Port your current number',
        hybridText: 'Hybrid',
        portOnDesiredDate: 'Port on Desired Date',
        portOnEndOfPromotion: 'Port on End of Promotion',
        portOnDesiredDateMessage:
          'Premature contract termination may genrate fees from donating operator',
        otpLoadingMessage:
          'We will send SMS for this Number with an authentication code',
        validateNumberText: 'Validate Number',
        sendAgainText: 'Send Again',
        endOfAgreementText: 'Withdrawal',
        portOnEndOfPromotionMessage:
          '{0} will be transferred on the last day of current contract validity with donating operator',
        invalidNumberText: 'Phone Number is invalid',

        message: 'We will send SMS for this Number with an authentication code',

        consentMessageText: `I accept that Telekom will handle all
          formalities with donating operator on my behalf.`,
        otpSendingMessage:
          'We are now sending you an SMS to validate your number',
        newNumberText: 'New Number',
        portNumberText: 'Port Number',
        mnpBannerText: 'Port your existing mobile phone number'
      },
      mns: {
        headingText: 'Select your new Phone Number',
        confirmNumber: 'Confirm Number',
        otherNumbers: 'Draw Other Numbers'
      },
      billingInfo: {
        loadingText: 'Loading',
        optionalText: '(Optional)',
        flatNumber: 'Flat Number',
        deliveryNoteText: 'Delivery note',
        upfrontText: 'UPFRONT',
        monthlyText: 'MONTHLY',
        streetAddress: 'Street Address',
        city: 'City',
        postCode: 'Post Code',
        addressType: {
          sameAsShippingInfo: 'Same as Shipping info',
          sameAsPersonalInfo: 'Same as Personal info',
          differentAddress: 'Different Address'
        },
        eBill: {
          typeText: 'eBill',
          discountText: 'discount on your monthly bill.',
          infoText: 'Get the bill on your personal email and ONE app'
        },
        discountDisableTitle: 'Discount title',
        discountDisableDesc: 'Your invoices will be delivered electronically ',
        address: 'Address',
        streetNumber: 'Street No',
        reviewOrder: 'Review Order',
        billingAddress: 'Billing Address',
        billingAddressChangingInfo:
          'Changing billing address will not affect the delivery address declared in previous step.',
        paperBill: {
          typeText: 'Paper Bill',
          discountText: 'No Discounts',
          infoText: 'Get the printed bill in your mailbox'
        },
        cardDetail: {
          cardNumber: 'Card number',
          // tslint:disable-next-line:no-duplicate-string
          nameOnCard: 'Name on Card',
          expirationDate: 'Expiration Date',
          edit: 'Edit'
        }
      },
      creditCheck: {
        selectOtherDevice:
          'There is an issue with your credit, but you can always select a different device.',
        creditCheckInProcess:
          'Hi {0}, please take a moment while we do your credit check.',
        payFullAndDifferentDevice:
          'There is an issue with your credit, but you can always pay in full or select a different device.',
        creditSuccess:
          'Congratulations, your credit score seems to be in order.',
        requestSupportCall: 'Request Support Call',
        creditIssue:
          'Due to an issue with your credit you are not able to proceed, but we can help.',
        idNotMatch:
          'Your information does not match your OIB ID, please review it to proceed.',
        reviewYourInformation: 'Review your information',
        selectDifferentDeviceText: 'Select a different device',
        payFull:
          'There is an issue with your credit, but you can always pay in full.',
        okGotIt: 'Ok, got it.',
        payFullButtonText: 'Pay device in full',
        requestSupportCallButtonText: 'Request Support Call',
        idNotMatchButtonText: 'Renew your information'
      },
      upfront: 'Upfront',
      optionalText: '(Optional)',
      shippingInfo: {
        openHourDescription:
          'You may encounter the line in store as you arrive, please be aware.',
        evening: 'Evening',
        showWorkingHours: 'Show Working Hours',
        hideWorkingHours: 'Hide Working Hours',
        deliveryType: {
          deliverToAddress: 'Deliver to Address', // deliver type
          pickUpStore: 'Pick up at Store',
          parcelLocker: 'Parcel Locker',
          pickUpPoints: 'Pick up Points'
        },
        optionalText: ' (Optional)',
        flatNumber: 'Flat Number',
        immediatelyInStock: 'Immediately when in stock',
        streetAddress: 'Street Address',
        city: 'City',
        postCode: 'Post Code',
        streetNumber: 'Street No',
        deliveryOptions: {
          deliveryOptionTxt: 'Delivery options',
          standard: 'Standard',
          withIn24: 'With in 24h',
          pickADate: 'Pick A Date',
          nextDay: 'Next Day'
        },
        unit: 'Unit',
        notes: 'Note on my order',
        pickUpOrderText: "Who\\'s going to pick up the order?",
        otherPerson: 'Other person',
        businessDays: '2-3 Business days',
        pickUp: 'Pick up in Store',
        selectStore: 'Select Store',
        selectMachine: 'Select Machine',
        selectPoint: 'Select Point',
        deliveryNoteText: 'Notes on my order (optional)',
        chooseOther: 'Choose other',
        samePerson: 'Me, {0}',
        shippingFee: 'Shipping Fee',
        proceedToPayment: 'Proceed to Payment',
        sameAddressText: 'Use same Address as Personal Info.',
        sameDay: 'Same Day',
        next24Hour: 'Next 24 hour',
        shippingText: 'Shipping',
        free: 'Free',
        deliveryNote: 'Note on my order',
        searchErrorMsg: 'We couldnt find any locations for {0}',
        parcelLockerText:
          // tslint:disable:max-line-length
          'Once the order is ready you will receive the mail with pickup code to insert on the parcewl locker,Government issued ID may be 				       required upon pick up',
        pickUpStoreText:
          'Once the order is ready you will receive the mail, Government issued ID may be required upon pick up',
        findStore: 'Find Store',
        findOtherStore: 'Find Other Store'
      },
      orderConfirmation: {
        trackYourOrder: 'Track your order',
        orderDetails: 'Order Details',
        orderId: 'Order ID',
        email: 'E-mail',
        contact: 'Contact',
        shippingTo: 'Shipping To',
        estimateDelivery: 'Est. Delivery',
        estimateDeliveryValue: '24 Hours',
        viewDetails: 'View Details',
        shippingDetails: 'Shipping Details',
        thankYouMsg: '{0}, thank you for placing your order.',
        orderReceived: 'Your order was received.',
        emailSent:
          'We have sent an email with the order details and tracking information',
        getMoreTitleText: ' Get more from it',
        getMoreDescriptionText:
          'Add two or more mobile subscriptions and we offer a 20% discount on your monthly fee.',
        deliveryType: {
          standard: 'Standard',
          withinTime: 'Same Day',
          pickADate: 'Exact Day',
          pickUpStore: 'Pick up At Store',
          parcelLocker: 'Parcel Locker',
          pickUpPoints: 'Pick Up Point'
        },
        sorryText: 'Sorry!',
        paymentUnsuccessText: 'Your payment was unsuccessful.',
        pleaseWaitText: 'Please wait!',
        paymentPendingText: 'Your payment is pending.',
        nameText: '{0}, the order',
        orderOnTheWayText: 'is on the way.',
        orderCanceledText: 'has to be canceled.',
        cancelDescription:
          'Thanks for your recent order. We are really sorry, but we have canceled the order. We couldn’t verify some or all of your payment information. Please sign in to your account and be sure your billing address, phone number and credit card information is correct and updated.',
        pendingDescription:
          'Your payment is pending because it’s waiting for your payment provider validation. The order progress will be available as soon as the payment is validated.',
        tryAgain: 'Try Again',
        paymentFailText: 'Your payment couldn’t be processed.',
        p24ButtonText: 'Pay',
        makePaymentText: 'Make payment by clicking below link'
      },
      monthly: 'Monthly',
      payment: 'Payment',
      loadingText: 'Loading...',
      orderReview: {
        personalInformation: {
          personalInfoTxt: 'Personal Info',
          fullName: 'Full Name',
          birthDate: 'Birth Date',
          phoneNumber: 'Phone Number',
          email: 'E-mail',
          firstName: 'First Name',
          lastName: 'Last Name',
          streetAddress: 'Street Address',
          idNumber: 'ID Number',
          oibNumber: 'OIB Number',
          company: 'Company',
          pesel: 'PESEL',
          flatNumber: 'Flat Number',
          city: 'City',
          postCode: 'Post Code',
          streetNumber: 'Street Number',
          deliveryNote: 'Delivery Note'
        },
        viewMore: 'View More',
        viewLess: 'View Less',
        shipping: {
          shippingTxt: 'Shipping',
          pickUpInStore: 'PICKUP IN STORE',
          storeName: 'Store Name',
          storeAddress: 'Store Address',
          addressLabel: 'Address'
        },
        paymentBilling: {
          paymentBillingTxt: 'Payment & Billing',
          upfront: 'UPFORNT',
          monthly: 'MONTHLY',
          billing: 'BILLING',
          address: 'Address',
          billingOption: 'Billing Option',
          cardNumber: 'Card Number',
          nameOnCard: 'Name on Card',
          expirationDate: 'Expiration Date',
          paymentModeSelected: 'Payment Mode Selected',
          paymentModeText: {
            payOnDelivery: 'Pay on Delivery',
            payByLink: 'Pay Via Link',
            bankAccount: 'Bank Account'
          }
        },
        edit: 'Edit',
        deliverToAddress: 'Deliver To Address',
        pickUpStore: 'Pick Up At store',
        parcelLocker: 'ParcelLocker',
        pickUpPoints: 'Pick Up Points ',
        order: 'Order',
        placeOrder: 'Place Order',
        orderReviewText: 'Order Review',
        marketingAgreements: 'Marketing agreements',
        productAndServiceInfo:
          'Receive professional product and service information from Telekom.',
        promotionalFromThirdParties:
          'Receive promotional product and service information from third parties.',
        agree: 'I Agree to the',
        termsAndCondition: 'Terms & Conditions',
        placeOrderModals: {
          placeOrderInProcess:
            'Hi {0}, please take a moment while your payment is processed.',
          paymentDeclined:
            'Sorry {0}, the payment transaction was declined by your Card provider/Bank.',
          insufficientFunds:
            'Sorry {0}, it looks like the Card provided has insufficient funds.',
          somethingWrong:
            'Oops, something went wrong with the payment transaction.',
          chooseOtherMethod: 'Choose other method',
          tryAgain: 'Try Again'
        },
        termsAndConditionsDescriptions:
          'Thanks for choosing T-Mobile. Please read these Terms &amp; Conditions \n , which contain important information about your relationship with T-Mobile.',
        paymentProcessMessage:
          'Payment will be put on hold and processed once customer receives his SIM card'
      },
      orderTracking: {
        reschedule: 'Reschedule',
        requestCall: 'Request a call.',
        outForDeliveryText: 'is out for delivery.'
      },
      paymentInfo: {
        proceedToMonthly: 'Proceed to Monthly',
        payOnDelivery: 'Pay on Delivery',
        securityCode: 'Security Code',
        saveThisCard: 'Save this Card',
        cardEnding: 'Ending in',
        proceedToBilling: 'Proceed to Billing',
        nameOnCard: 'Name on Card',
        payByLink: 'Pay by Link',
        paymentAndBilling: 'Payment & Billing',
        noUpfrontText: 'There is no upfront value to be paid.',
        noMonthlyText: 'There is no monthly value to be paid.',
        credit: 'Credit',
        debit: 'Debit',
        addNewCard: 'Add new card',
        card: 'Card',
        cardNumber: 'Card Number',
        expirationDate: 'Expiration date',
        useSameCard: 'Use Same Card',
        edit: 'Edit',
        creditDebitCard: 'Credit/Debit Card',
        tokenizedCard: 'Credit/Debit Card',
        bankAccount: ' Bank Account',
        manualPayments: ' Manual Payments',
        monthly: 'Monthly',
        upfront: 'Upfront',
        payOnDeliveryInfoText:
          'You will get the bills by the end the month with the necessary information to proceed the manual payment. Payment can be done through netbanking, atm or at any telekom store until the due date of each invoice.',
        manualPaymentInfoText:
          'You will get the bills by the end the month with the necessary information to proceed the manual payment. Payment can be done through netbanking, atm or at any telekom store until the due date of each invoice.',
        creditCardForm: {
          cardNumberValidationMsg: 'Card Number is not valid',
          cardNumberPlaceHolder: 'xxxx xxxx xxxx xxxx',
          cvvValidationMsg: 'Cvv is not valid',
          cvvPlaceHolder: 'xxx',
          expirationDateValidationMsg: 'Expiry is not right',
          expirationDatePlaceHolder: 'xx/xxxx',
          cardHolderNamePlaceHolder: 'Enter Card Holder Name',
          cardHolderNameValidationMsg: 'Enter Card Holder Name'
        }
      }
    },
    login: {
      loginUserNamePassword: {
        register: 'Register',
        speedUp: 'Speed up',
        yourCheckout: 'your checkout.',
        signIn: 'Sign In',
        manageServicesAndBills: 'Manage Services & Bills',
        generalInfo1: 'Continue using one of your authentication IDs.',
        generalInfo2: 'Noncustomers can register with email or mobile number',
        next: 'Next',
        email: 'Email',
        userName: 'Username',
        or: 'or',
        mobileNumber: 'Mobile Number',
        placeHolderForInput: 'Email, Username or Mobile Number',
        emailInvalid: 'This email is not valid',
        mobileNumberInvalid: 'This mobile is not valid',
        userNameInvalid: 'Your username is invalid',
        userNameInvalidMinLength:
          'Your username should be atleast {0} characters',
        userNameInvalidMaxLength:
          'Your username should be maximum {0} characters',
        minCredentialsCharacterRequired: 4,
        linkTextKey: 'Forgot Credentials?'
      },
      loginEmailConfirmation: {
        creatingNew: 'Creating new',
        account: 'account',
        speedUpCheckout: 'to speed up checkout',
        placeHolderForEmailInput: 'Enter your e-mail address',
        placeHolderForConfirmInput: 'Retype your e-mail address',
        errorMessageForEmailInput: 'This email is not valid',
        errorMessageForConfirmInput: 'This retype email is not valid'
      },
      loginEnterOtp: {
        otpSentMessage: `We've sent an OTP to`,
        validateOtp: 'Validate OTP',
        otpInvalid: 'Your otp is invalid'
      },
      common: {
        otherOptionsHeading: 'Or sign in with',
        sendAgain: 'Send again',
        forgotCredentials: 'Forgot Credentials?',
        signIn: 'Sign in',
        next: 'Next'
      },
      loginProgressOtpSent: {
        hello: 'Hello!',
        messageForMsisdnLogin:
          // tslint:disable-next-line:no-duplicate-string
          'We are now sending an SMS to validate your number.',
        messageForEmailLogin:
          'We are now sending an SMS to validate your number.'
      },
      loginProgressFacebookSignIn: {
        hi: 'Hi',
        message: 'We are now sending an SMS to validate your number.'
      },
      loginEnterPassword: {
        forgotPassword: 'forgot Password recent',
        inputLabel: 'Enter password trasnlation',
        minPasswordCharacterRequired: 3,
        enterPasswordMessage: 'Enter password for',
        validatePassword: 'Validate Password',
        passwordInvalidMinLength:
          // tslint:disable-next-line:no-hardcoded-credentials
          'Password should be minimum {0} characters long',
        passwordInvalidMaxLength:
          // tslint:disable-next-line:no-hardcoded-credentials
          'Password should be maximum {0} characters long',
        passwordInvalidNumberLowercase:
          // tslint:disable-next-line:no-hardcoded-credentials
          'Password should have {0} uppercase characters',
        passwordInvalidNumberUppercase:
          // tslint:disable-next-line:no-hardcoded-credentials
          'Password should have {0} lowercase characters',
        // tslint:disable-next-line:no-hardcoded-credentials
        passwordInvalidNumberDigits: 'Password should have atleast {0} digits',
        passwordInvalidNumberCharacters:
          // tslint:disable-next-line:no-hardcoded-credentials
          'Password should have atleast {0} characters',
        // tslint:disable-next-line:no-hardcoded-credentials
        passwordInvalid: 'Password is invalid'
      },
      recoveryCredentials: {
        minRecoveryEmailCharacterRequired: 5,
        title: 'recover your credentials',
        sentInstructions: `We've sent you instructions to the email`,
        next: 'Next',
        sentInstructionForUsernameRecovery: 'we have sent you an email {0}',
        inputLabel: 'Enter Email for translation'
      },
      loginWithProceedAsGuest: {
        speedUp: 'Speed Up',
        yourCheckout: 'your checkout.',
        next: 'Next',
        yourFirst: 'Your first',
        telekomProduct: 'Telekom  Product',
        proceedAsGuest: 'Proceed as Guest',
        firstTime:
          'First time as Telekom? You can proceed with a guest account.',
        newCustomerAsk: 'New costumer?',
        guestCheckout: 'Guest Checkout',
        subTitle: 'Proceed to checkout quickly, safely and easily',
        continue: 'Continue'
      },
      logout: {
        signedOutMessage: 'You’re now signed out.',
        thankYouMessage: 'Thank you for being with us! See you soon.',
        singInAgain: 'Sign In Again',
        goToHomePage: 'Go To Home Page'
      },
      identityVerification: {
        hi: 'hi',
        modalMessage: 'We are not signing in you with you {0} account',
        next: 'next',
        confirm: 'confirm',
        yourIndetity: 'your identity',
        selectVerificationMessage:
          'Before proceeding, select an identity verification; procedure.',
        method: 'mehtod',
        courierDeliveryMessage:
          'You must show your identification at the delivery moment',
        bank: 'bank',
        phoneNumberLabel: 'Phone Number',
        peselLabel: 'Enter Personal Id Number'
      },
      loginWithHeaderEnrichment: {
        hiText: 'Hi',
        loginMessage: 'We are signing in you',
        cancelationButtonText: 'Not You?'
      }
    },
    tariff: {
      getBestRateText: 'Get the best rates !',
      selectBestPlansText: 'Select the best plan for you.',
      viewAll: 'view all',
      bestRateSubHeaderText:
        ' All plans include Best network with Lte Max,  Hotspot flat rate, Unlimited SMS, EU roaming.',
      aboutDiscounts: 'About Discounts',
      comparePlans: 'Compare Plans',
      addAPhone: 'Add a Phone',
      buyPlanOnly: 'Buy Plan Only',
      unlimited: 'Unlimited',
      termsAndConditions: 'Term & Conditions',
      monthly: 'monthly',
      termsAndConditionsHeaderText: 'Terms and Conditions',
      termsAndConditionsDescriptions:
        'Thanks for choosing T-Mobile. Please read these Terms &amp; Conditions \n , which contain important information about your relationship with T-Mobile.',
      magentaDiscountsText: 'How to get Magenta Discounts',
      familyDiscountsText: 'How to get Family Benifits ',
      viewMore: 'View More',
      viewLess: 'View Less',
      viewDetails: 'View All Details',
      noLoyalty: 'No loyalty',
      unfoldedPlans: 'unfolded plans',
      pricePerMonth: 'price / month',
      pleaseSelect: 'please select',
      perMonth: 'mo',
      loadingText: 'loading data',
      faqHeaderText: 'Frequently Asked Questions',
      data: 'Data',
      hdData: 'HD',
      youngTariff: {
        titleText: 'What is you age',
        ageLimitText: 'Is your age between 18 to 36?',
        enjoyTariff: 'If yes, enjoy this tariff.',
        validate: 'Validate',
        placeHolder: 'Your Birth Date',
        errorMessage: 'Invalid Date Range'
      },
      confirmPlan: 'Confirm Plan',
      plan: 'Plan',
      moSymbol: 'mo',
      price: 'Price'
    }
  }
  // tslint:disable-next-line:max-file-line-count
});
