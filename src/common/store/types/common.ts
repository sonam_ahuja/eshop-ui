export interface ICommonState {
  errorToast: {
    message: string;
    isOpen: boolean;
  };
  error: IError;
  currency: string;
  loading: boolean;
  showOfflineToast: boolean;
  isLoggedIn: boolean;
  isLoginButtonClicked: boolean;
  deviceList: string;
  landingPageDeviceList?: string;
  deviceDetailedList: string;
  routeFromBasketToLogin: boolean;
  msisdnValue: string;
  // tslint:disable:no-any
  megaMenu: any[];
  megaMenuFooter: any[];
  userDetails: IUserDetails;
  userIdentityVerificationDetails: IThirdPartyVerificaation;
  prolongation: ICommonStateProlongation;
  closeToast?(): void;
}

export interface ICommonStateProlongation {
  authorizationToken: string | null;
}
export interface ILoading {
  errorToast: { isOpen: boolean; message: string };
  isLoggedIn: boolean;
  isLoginButtonClicked: boolean;
  loading: boolean;
  routeFromBasketToLogin: boolean;
}

export interface IMegaMenu {
  categories: IMenusCategory[];
}

export interface IMenusCategory {
  id: string;
  currentNodeMetaData: ICurrentNodeMetaData;
  childNodes?: IMenusCategory[];
}

interface ICurrentNodeMetaData {
  type: string;
  tags: string[];
  distanceFromResolution: number;
  data: IData;
}

interface IData {
  title: string;
  description: string;
  deeplink: string;
  openNewTab: boolean;
}

// TYPES for profile API

export interface IUserDetails {
  id: string;
  status: string;
  relatedParties: IRelatedPartiesItem[];
  contactMediums: IContactMediumsItem[];
}
export interface IRelatedPartiesItem {
  id: string;
  role: string;
  name: string;
}
export interface IContactMediumsItem {
  type: string;
  role: IRole;
  medium: IMedium;
  preferred: boolean;
}
interface IRole {
  name: string;
}
interface IMedium {
  emailAddress?: string;
  number?: string;
}

// types for third party identity verification of user

export interface IThirdPartyVerificaation {
  verificationRequired: boolean;
  identificationProviders?: IIdentificationProviders;
}

interface IIdentificationProviders {
  COURIER?: IIdentityProvider;
  BANK?: IIdentityProvider;
  TELEKOM?: IIdentityProvider;
}

export interface IIdentityProvider {
  providerType: string;
  providerSublist: IProviderSublist[];
}

export interface IProviderSublist {
  providerId: string;
  bankId?: string;
  displayValue?: string;
}

// Post api types for verification

export interface IVerifyUsingProvidersRequest {
  providerId: string;
  bankId?: string | null;
  providerType: string;
  serviceId?: string | null;
  personalId?: string;
}

export interface IVerifyUsingProvidersResponse {
  providerType?: string;
  requestUrl?: string;
  requestType?: string;
  nonce?: string;
  requestPayLoadKey?: string;
  requestPayLoadValue?: string;
}

// Validate user using OTP
// POST API types

export interface IValidateUserUsingOtpRequest {
  action: string;
  mobileNumber: string;
  nonce: string;
  otp: string;
  otpContext: string;
}

// Error --> generic error type
export interface IError {
  showFullPageError?: boolean;
  skipGenericError?: boolean;
  httpStatusCode?: string | number;
  code: string;
  retryable: boolean;
  message?: {
    debugMessage?: string;
    userMessage?: string;
  };
}
