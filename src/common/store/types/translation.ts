export interface IEmptyBasket {
  emptyBasketText: string;
  suggestionText: string;
  bestDealText: string;
  bestDealLink: string;
}

export interface IPopUps {
  removeButton: string;
  deviceProtectionText: string;
  removeAllItem: string;
  removeOneItem: string;
}

export interface IDelete {
  allItem: string;
  oneItem: string;
}

export interface IDiscount {
  eBillDiscount: string;
  bundleDiscount: string;
  otherDiscount: string;
  loyaltyDiscount: string;
  christmasDiscount: string;
}

export interface IStripMessage {
  cartRestrictionMessage: string;
  saveItemInBasketMsg: string;
  prodOutOfStock: string;
  oneItemRemove: string;
  allItemRemove: string;
  promoMsg: string;
  promoCode: string;
}

export interface ICategoryTranslation {
  categoryTitle: string;
  categorySubTitle: string;
  bestDeviceOfferHeading: string;
  preOrderCardTitle: string;
}

export interface ICart {
  basket: IBasket;
  global?: IGlobalTranslation;
}

export interface IProductListTranslation {
  preOrderCardTitle: string;
  perPage: string;
  noProductText: string;
  comeBackText: string;
  selectitemPerPage: string;
  loadingText: string;
  viewAllPopularDevice: string;
  description: string;
  singularViewResults: string;
  singularDescription: string;
  simplifiedPagination: {
    previous: string;
    next: string;
  };
  showMorePagination: {
    loadMore: string;
    loading: string;
    loaded: string;
  };
  infinitePagination: {
    loading: string;
    loaded: string;
  };
  availability: {
    IN_STOCK: string;
    OUT_OF_STOCK: string;
    PRE_ORDER: string;
  };
  filterByTitle: string;
  sortByDropdownLabel: string;
  sortByTitle: string;
  viewDetails: string;
  notificationAction: string;
  viewResults: string;
  filterLoadingText: string;
  outOfStock: string;
  preOrder: string;
  outOfStockDescription: string;
  notificationTitle: string;
  validateButton: string;
  notificationSuccessMobile: string;
  notificationSuccessEmail: string;
  upFront: string;
  monthly: string;
  // SpecialOffer: string;
  lowestPrice: string;
  launchDate: string;
  popularityIndex: string;
  bestDeviceOffers: string;
  discount: string;
  popularAccessories: string;
  clearAll: string;
  viewAll: string;
  installments: string;
  pricePerMonth: string;
  titleSeparator: string;
  planTitleSeparator: string;
  priceRangeAboveFilter: string;
  filterCount: number;
}

/** USED ON PROLONGATION */
export interface IProlongation {
  global: IProlongationGlobalTranslation;
  landing: IProlongationListing;
  checkout: IProlongationCheckout;
  orderConfirmation: IProlongationOrderConfirmation;
  productDetails: IProlongationProductDetails;
}

export interface IProlongationGlobalTranslation {
  errors: IError;
  givenDiscountMessage: string;
  losingDiscountMessage: string;
  accepted: string;
  notAccepted: string;
}

export interface ILandScapeMode {
  heading: string;
  subHeading: string;
}

/** USED ON PROLONGATION */
export interface IProlongationProductDetails {
  highlightText: string;
  customiseYourPhone: string;
  totalMonthly: string;
  totalUpfront: string;
  buyBundle: string;
  colour: string;
  storage: string;
  plan: string;
  ram: string;
  showMore: string;
  showLess: string;
}
/** USED ON PROLONGATION */
export interface IProlongationCheckout {
  form: IProlongationForm;
  personalInfo: IProlongationPersonalInfo;
  termsAndConditionText: string;
  agreementText: string;
  placeOrder: string;
  orderSummary: string;
  totalMonthly: string;
  totalNow: string;
  optionalText: string;
}

/** USED ON PROLONGATION */
interface IProlongationPersonalInfo {
  heading: string;
  editText: string;
}
/** USED ON PROLONGATION */
interface IProlongationForm {
  email: IProlongationFormField;
  phoneNumber: IProlongationFormField;
  deliveryAddress: IProlongationFormField;
  deliveryContact: IProlongationFormField;
  firstName: IProlongationFormField;
  lastName: IProlongationFormField;
}
/** USED ON PROLONGATION */
interface IProlongationFormField {
  label: string;
}
/** USED ON PROLONGATION */
export interface IProlongationOrderConfirmation {
  heading: string;
  subHeading: string;
  instructionMessage: string;
  backToHomeText: string;
}

/** USED ON PROLONGATION */
export interface IProlongationListing {
  nowText: string;
  monthlyShort: string;
  heading: string;
}

export interface ITranslationState {
  prolongation: IProlongation;
  cart: {
    basket: IBasket;
    productList: IProductListTranslation;
    global: IGlobalTranslation;
    category: ICategoryTranslation;
    checkout: ICheckoutTranslation;
    login: ILoginTranslation;
    productDetailed: IProductDetailedTranslation;
    tariff: ITariffTranslation;
    landingPage: ILandingPageTranslation;
  };
}
export interface IError {
  [key: string]: string;
}

export interface ILandingPageTranslation {
  heading: string;
  downloadText: string;
  subHeading: string;
}
export interface ITariffTranslation {
  getBestRateText: string;
  selectBestPlansText: string;
  bestRateSubHeaderText: string;
  aboutDiscounts: string;
  viewAll: string;
  comparePlans: string;
  addAPhone: string;
  buyPlanOnly: string;
  unlimited: string;
  termsAndConditions: string;
  magentaDiscountsText: string;
  familyDiscountsText: string;
  viewMore: string;
  viewLess: string;
  viewDetails: string;
  noLoyalty: string;
  unfoldedPlans: string;
  pricePerMonth: string;
  pleaseSelect: string;
  monthly: string;
  perMonth: string;
  loadingText: string;
  termsAndConditionsHeaderText: string;
  termsAndConditionsDescriptions: string;
  youngTariff: IYoungTariff;
  faqHeaderText: string;
  confirmPlan: string;
  plan: string;
  hdData: string;
  moSymbol: string;
  price: string;
  data: string;
}
export interface IProductDetailedTranslation {
  storage: string;
  color: string;
  noInstallments: string;
  moSymbol: string;
  noLoyalty: string;
  installments: string;
  installmentLabel: string;
  withoutInstallmentLabel: string;
  description: string;
  upfront: string;
  totalMonthly: string;
  timer: string;
  highlights: string;
  technicalSpecification: string;
  buyDeviceOnly: string;
  notifyMe: string;
  addToBasket: string;
  inStock: string;
  loadingText: string;
  noImagePreview: string;
  outOfStock: string;
  disabledOutOfStock: string;
  tooltipTitle: string;
  fullDevicePrice: string;
  preOrder: string;
  specialDiscount: string;
  totalUpfront: string;
  newDevice: string;
  days: string;
  hours: string;
  minutes: string;
  seconds: string;
  youngTariff: IYoungTariff;
  seeAllPlans: string;
}

export interface IYoungTariff {
  titleText: string;
  ageLimitText: string;
  enjoyTariff: string;
  validate: string;
  placeHolder: string;
  errorMessage: string;
}

export interface ITranslationResponse {
  cart: {
    basket: IBasket;
  };
}

export interface IEmptyBasket {
  emptyBasketText: string;
  suggestionText: string;
  bestDealText: string;
  bestDealLink: string;
}
export interface IPopUps {
  removeButton: string;
  deviceProtectionText: string;
  removeAllItem: string;
  removeOneItem: string;
}

export interface IDelete {
  allItem: string;
  oneItem: string;
}
export interface IDiscount {
  eBillDiscount: string;
  bundleDiscount: string;
  otherDiscount: string;
  loyaltyDiscount: string;
  christmasDiscount: string;
}

export interface IStripMessage {
  saveItemInBasketMsg: string;
  prodOutOfStock: string;
  oneItemRemove: string;
  allItemRemove: string;
  promoMsg: string;
  promoCode: string;
}

export interface IBasket {
  noLoyalty: string;
  priceChanged: string;
  outOfStock: string;
  clearDeviceText: string;
  viewBasket: string;
  startShopping: string;
  after: string;
  months: string;
  or: string;
  pageHeading: string;
  summaryHeading: string;
  clearBasket: string;
  recommended: string;
  relatedProduct: string;
  continueShopping: string;
  changeDeviceOptions: string;
  changeVariant: string;
  selectOtherDevice: string;
  remove: string;
  monthly: string;
  upFront: string;
  cardUpfrontHeading: string;
  summaryUpfrontHeading: string;
  totalMonthly: string;
  totalUpFront: string;
  subTotal: string;
  subTotalMonthly: string;
  subTotalUpfront: string;
  discountType: IDiscount;
  shipping: string;
  ebill: string;
  paperBill: string;
  totalDueToday: string;
  offer: string;
  total: string;
  retailPrice: string;
  finalPrice: string;
  installments: string;
  basePrice: string;
  monthlyPrice: string;
  undo: string;
  termsAndCondition: string;
  emptyBasket: IEmptyBasket;
  stripMessage: IStripMessage;
  popUps: IPopUps;
  delete: IDelete;
  home: string;
  proceedToCheckout: string;
  discount: string;
  loadingQuantity: string;
  free: string;
  outOfStockText: string;
  preOrderText: string;
  sorryText: string;
  totalDueDate: string;
  item: string;
  items: string;
}

export interface ILoginUserNamePassword {
  register: string;
  speedUp: string;
  yourCheckout: string;
  signIn: string;
  manageServicesAndBills: string;
  generalInfo1: string;
  generalInfo2: string;
  next: string;
  email: string;
  userName: string;
  or: string;
  mobileNumber: string;
  emailInvalid: string;
  mobileNumberInvalid: string;
  userNameInvalid: string;
  userNameInvalidMinLength: string;
  userNameInvalidMaxLength: string;
  placeHolderForInput: string;
  minCredentialsCharacterRequired: number;
  linkTextKey: string;
}

export interface ILoginEmailConfirmation {
  creatingNew: string;
  account: string;
  speedUpCheckout: string;
  placeHolderForEmailInput: string;
  placeHolderForConfirmInput: string;
  errorMessageForEmailInput: string;
  errorMessageForConfirmInput: string;
}
export interface ILoginEnterOtp {
  otpSentMessage: string;
  validateOtp: string;
  otpInvalid: string;
}

export interface ILoginEnterPassword {
  forgotPassword: string;
  inputLabel: string;
  minPasswordCharacterRequired: number;
  enterPasswordMessage: string;
  validatePassword: string;
  passwordInvalidMinLength: string;
  passwordInvalidMaxLength: string;
  passwordInvalidNumberLowercase: string;
  passwordInvalidNumberUppercase: string;
  passwordInvalidNumberDigits: string;
  passwordInvalidNumberCharacters: string;
  passwordInvalid: string;
}

export interface IRecoveryCredentials {
  inputLabel: string;
  minRecoveryEmailCharacterRequired: number;
  sentInstructions: string;
  next: string;
  title: string;
  sentInstructionForUsernameRecovery: string;
}
export interface ILoginWithProceedAsGuest {
  speedUp: string;
  yourCheckout: string;
  next: string;
  yourFirst: string;
  telekomProduct: string;
  proceedAsGuest: string;
  firstTime: string;
  newCustomerAsk: string;
  guestCheckout: string;
  subTitle: string;
  continue: string;
}
export interface ILoginProgressFacebookSignIn {
  hi: string;
  message: string;
}

export interface ILoginCommon {
  otherOptionsHeading: string;
  sendAgain: string;
  forgotCredentials: string;
  signIn: string;
  next: string;
}

export interface ILoginProgressOtpSent {
  hello: string;
  messageForMsisdnLogin: string;
  messageForEmailLogin: string;
}

export interface ILoginTranslation {
  loginUserNamePassword: ILoginUserNamePassword;
  loginEmailConfirmation: ILoginEmailConfirmation;
  loginEnterOtp: ILoginEnterOtp;
  loginEnterPassword: ILoginEnterPassword;
  recoveryCredentials: IRecoveryCredentials;
  loginWithProceedAsGuest: ILoginWithProceedAsGuest;
  loginProgressOtpSent: ILoginProgressOtpSent;
  common: ILoginCommon;
  loginProgressFacebookSignIn: ILoginProgressFacebookSignIn;
  logout: ILogout;
  identityVerification: IIdentityVerification;
  loginWithHeaderEnrichment: ILoginWithHeaderEnrichment;
}

export interface ILoginWithHeaderEnrichment {
  hiText: string;
  loginMessage: string;
  cancelationButtonText: string;
}

export interface IIdentityVerification {
  next: string;
  confirm: string;
  yourIndetity: string;
  selectVerificationMessage: string;
  method: string;
  courierDeliveryMessage: string;
  bank: string;
  hi: string;
  modalMessage: string;
  phoneNumberLabel: string;
  peselLabel: string;
}
export interface ILogout {
  signedOutMessage: string;
  thankYouMessage: string;
  singInAgain: string;
  goToHomePage: string;
}
export interface ICart {
  basket: IBasket;
  global?: IGlobalTranslation;
  checkout: ICheckoutTranslation;
}

export interface ITranslation {
  cart?: ICart;
}

export interface IGlobalTranslation {
  errors: IError;
  companyName: string;
  offlineMessage: string;
  appName: string;
  shortName: string;
  termsAndConditions: string;
  home: string;
  products: string;
  basket: string;
  categoryLandingPage: string;
  footer: IFooterTranslation;
  search: ISearchTranslation;
  header: IHeaderTranslation;
  landScapeMode: ILandScapeMode;
  days: IDays;
  landingPageTitle: string;
  landingPageDescription: string;
  checkout: ICheckoutSEO;
  ieNotSupported: string;
}

export interface ICheckoutSEO {
  personalInfo: ISEO;
  shipping: ISEO;
  payment: ISEO;
  billing: ISEO;
  orderReview: ISEO;
}

export interface ISEO {
  title: string;
  description: string;
}

export interface IDays {
  monday: string;
  tuesday: string;
  wednesday: string;
  thursday: string;
  friday: string;
  saturday: string;
  sunday: string;
}

export interface ISearchTranslation {
  filters: IFiters;
  categories: ICategories;
  filterInBetweenMessageText: string;
  notFound: INotFound;
  searchTips: ISearchTips;
  viewAll: string;
  searchField: ISearchField;
  searchQueries: ISearchQueries;
}

export interface ISearchQueries {
  firstQuery: string;
  secondQuery: string;
  thirdQuery: string;
  fourthQuery: string;
  fifthQuery: string;
  sixthQuery: string;
  seventhQuery: string;
}
export interface ISearchField {
  mobile: IFieldMobile;
  desktop: IFieldDesktop;
}

export interface IFieldMobile {
  startTyping: string;
  speakNow: string;
  doNotUnderstood: string;
  voiceSearchHasBeenTurnOff: string;
  checkMicAndAudio: string;
  listening: string;
  cancel: string;
}

export interface IFieldDesktop {
  startSpeaking: string;
  startTyping: string;
  listening: string;
  doNotUnderstood: string;
  voiceSearchHasBeenTurnOff: string;
  repeat: string;
  checkMicAndAudio: string;
}

export interface ISearchTips {
  searchTipsText: string;
  spellingText: string;
  generalText: string;
  differentWordsText: string;
  helpText: string;
}
export interface INotFound {
  noResultsFound: string;
  message: string;
}
export interface ICategories {
  highlights: string;
  devices: string;
  pagesResults: string;
  faqs: string;
}

export interface IFiters {
  all: string;
  highlights: string;
  devices: string;
  pages: string;
  faqs: string;
}

export interface IHeaderTranslation {
  privateText: string;
  businessText: string;
  touristText: string;
  signInText: string;
  logoutText: string;
  specialOfferTitle: string;
  specialOfferDescription: string;
}
export interface IFooterTranslation {
  downloadAppText: string;
  storeLocator: string;
  aboutUs: string;
  contact: string;
  privacyPolicy: string;
  cookies: string;
  support: string;
  secure: string;
}

export interface ICheckoutTranslation {
  personalInfo: IPersonalInfoTranslation;
  creditCheck: ICreditCheckTranslation;
  billingInfo: IBillingInfoTranslation;
  shippingInfo: IShippingInfoTranslation;
  orderConfirmation: IOrderConfirmationTranslation;
  orderReview: IOrderReviewTranslation;
  orderTracking: IOrderTrackingTranslation;
  paymentInfo: IPaymentInfoTranslation;
  mnp: IMNPTranslation;
  mns: IMNSTranslation;
  monthly: string;
  payment: string;
  upfront: string;
  loadingText: string;
  optionalText: string;
}
export interface IPersonalInfoTranslation {
  lastName: string;
  updateSmsText: string;
  consentText: string;
  idNumber: string;
  personalInfoText: string;
  postCode: string;
  streetNumber: string;
  city: string;
  flatNumber: string;
  proceedToCreditButton: string;
  firstName: string;
  proceedToShipping: string;
  phoneNumber: string;
  streetAddress: string;
  oibNumber: string;
  pesel: string;
  company: string;
  updateEmailText: string;
  email: string;
  dob: string;
  deliveryNoteText: string;
  sideNavText: string;
  changeNumber: string;
  portNumber: string;
  mnpInitialText: string;
  numberIs: string;
  mnpPortedText: string;
  editPort: string;
  changeOrPortNumberText: string;
  cancel: string;
  cancelPortText: string;
  cancelMessage: string;
}
export interface IMNSTranslation {
  headingText: string;
  confirmNumber: string;
  otherNumbers: string;
}
export interface IMNPTranslation {
  migrationHeaderText: string;
  is: string;
  otpHeadingText: string;
  nextText: string;
  endOfPromotionText: string;
  helloText: string;
  portMyNumberText: string;
  phoneNumberLabelText: string;
  headingText: string;
  hybridText: string;
  otpLoadingMessage: string;
  validateNumberText: string;
  sendAgainText: string;
  endOfAgreementText: string;
  invalidNumberText: string;
  prepaidText: string;
  postpaidText: string;
  desiredDate: string;
  message: string;
  consentMessageText: string;
  portOnEndOfPromotion: string;
  portOnEndOfPromotionMessage: string;
  portOnEndOfAgreement: string;
  portOnEndOfAgreementMessage: string;
  portOnDesiredDate: string;
  portOnDesiredDateMessage: string;
  otpSendingMessage: string;
  portOn: string;
  newNumberText: string;
  portNumberText: string;
  mnpBannerText: string;
}
export interface ICreditCheckTranslation {
  requestSupportCall: string;
  requestSupportCallButtonText: string;
  idNotMatch: string;
  idNotMatchButtonText: string;
  okGotIt: string;
  selectDifferentDeviceText: string;
  selectOtherDevice: string;
  payFullAndDifferentDevice: string;
  creditIssue: string;
  creditSuccess: string;
  payFullButtonText: string;
  creditCheckInProcess: string;
  reviewYourInformation: string;
  payFull: string;
}

export interface IBillingInfoTranslation {
  optionalText: string;
  loadingText: string;
  billingAddress: string;
  billingAddressChangingInfo: string;
  upfrontText: string;
  monthlyText: string;
  postCode: string;
  address: string;
  city: string;
  flatNumber: string;
  addressType: IAddressType;
  streetNumber: string;
  streetAddress: string;
  reviewOrder: string;
  eBill: IBill;
  paperBill: IBill;
  discountDisableTitle: string;
  discountDisableDesc: string;
  deliveryNoteText: string;
  cardDetail: ICardDetail;
}

export interface IAddressType {
  sameAsShippingInfo: string;
  sameAsPersonalInfo: string;
  differentAddress: string;
}

export interface ICardDetail {
  cardNumber: string;
  nameOnCard: string;
  expirationDate: string;
  edit: string;
}
export interface IBill {
  typeText: string;
  discountText: string;
  infoText: string;
}

export interface IShippingInfoTranslation {
  deliveryType: {
    deliverToAddress: string;
    pickUpStore: string;
    parcelLocker: string;
    pickUpPoints: string;
  };
  openHourDescription: string;
  showWorkingHours: string;
  hideWorkingHours: string;
  streetAddress: string;
  city: string;
  postCode: string;
  streetNumber: string;
  flatNumber: string;
  unit: string;
  notes: string;
  proceedToPayment: string;
  deliveryNoteText: string;
  immediatelyInStock: string;
  deliveryOptions: {
    deliveryOptionTxt: string;
    standard: string;
    withIn24: string;
    pickADate: string;
    nextDay: string;
  };
  pickUp: string;
  sameAddressText: string;
  shippingFee: string;
  sameDay: string;
  selectStore: string;
  selectMachine: string;
  selectPoint: string;
  shippingText: string;
  next24Hour: string;
  chooseOther: string;
  businessDays: string;
  otherPerson: string;
  free: string;
  pickUpOrderText: string;
  samePerson: string;
  optionalText: string;
  deliveryNote: string;
  searchErrorMsg: string;
  parcelLockerText: string;
  pickUpStoreText: string;
  findStore: string;
  findOtherStore: string;
  evening: string;
}

export interface IOrderConfirmationTranslation {
  orderDetails: string;
  trackYourOrder: string;
  thankYouMsg: string;
  shippingDetails: string;
  viewDetails: string;
  orderReceived: string;
  emailSent: string;
  orderId: string;
  email: string;
  contact: string;
  shippingTo: string;
  estimateDelivery: string;
  getMoreTitleText: string;
  getMoreDescriptionText: string;
  estimateDeliveryValue: string;
  sorryText: string;
  paymentUnsuccessText: string;
  pleaseWaitText: string;
  paymentPendingText: string;
  deliveryType: IOrderDeliveryType;
  nameText: string;
  orderOnTheWayText: string;
  orderCanceledText: string;
  cancelDescription: string;
  pendingDescription: string;
  tryAgain: string;
  paymentFailText: string;
  p24ButtonText: string;
  makePaymentText: string;
}

export interface IOrderDeliveryType {
  standard: string;
  withinTime: string;
  pickADate: string;
  pickUpStore: string;
  parcelLocker: string;
  pickUpPoints: string;
}

export interface IOrderReviewTranslation {
  personalInformation: IOrderReviewPersonalInfo;
  shipping: IOrderReviewShipping;
  paymentBilling: IOrderReviewPaymentAndBilling;
  order: string;
  placeOrder: string;
  marketingAgreements: string;
  orderReviewText: string;
  productAndServiceInfo: string;
  promotionalFromThirdParties: string;
  agree: string;
  termsAndCondition: string;
  edit: string;
  deliverToAddress: string;
  pickUpStore: string;
  parcelLocker: string;
  pickUpPoints: string;
  placeOrderModals: IPlaceOrderModals;
  viewMore: string;
  viewLess: string;
  termsAndConditionsDescriptions: string;
  paymentProcessMessage: string;
}

export interface IPlaceOrderModals {
  placeOrderInProcess: string;
  paymentDeclined: string;
  insufficientFunds: string;
  somethingWrong: string;
  chooseOtherMethod: string;
  tryAgain: string;
}

export interface IOrderReviewPersonalInfo {
  personalInfoTxt: string;
  fullName: string;
  firstName: string;
  lastName: string;
  birthDate: string;
  phoneNumber: string;
  email: string;
  streetAddress: string;
  idNumber: string;
  oibNumber: string;
  company: string;
  flatNumber: string;
  city: string;
  postCode: string;
  streetNumber: string;
  pesel: string;
  deliveryNote: string;
}
export interface IOrderReviewShipping {
  shippingTxt: string;
  pickUpInStore: string;
  storeName: string;
  storeAddress: string;
  addressLabel: string;
}

export interface IOrderReviewPaymentAndBilling {
  paymentBillingTxt: string;
  upfront: string;
  monthly: string;
  billing: string;
  address: string;
  billingOption: string;
  cardNumber: string;
  nameOnCard: string;
  expirationDate: string;
  paymentModeSelected: string;
  paymentModeText: {
    payOnDelivery: string;
    payByLink: string;
    bankAccount: string;
  };
}

export interface IOrderTrackingTranslation {
  reschedule: string;
  requestCall: string;
  outForDeliveryText: string;
}
export interface IPaymentInfoTranslation {
  nameOnCard: string;
  saveThisCard: string;
  proceedToBilling: string;
  addNewCard: string;
  expirationDate: string;
  paymentAndBilling: string;
  payByLink: string;
  proceedToMonthly: string;
  payOnDelivery: string;
  cardEnding: string;
  cardNumber: string;
  credit: string;
  debit: string;
  noUpfrontText: string;
  noMonthlyText: string;
  card: string;
  securityCode: string;
  useSameCard: string;
  edit: string;
  creditDebitCard: string;
  tokenizedCard: string;
  bankAccount: string;
  manualPayments: string;
  monthly: string;
  upfront: string;
  payOnDeliveryInfoText: string;
  manualPaymentInfoText: string;
  creditCardForm: ICreditCardForm;
}

export interface ICreditCardForm {
  cardNumberValidationMsg: string;
  cardNumberPlaceHolder: string;
  cvvValidationMsg: string;
  cvvPlaceHolder: string;
  expirationDateValidationMsg: string;
  expirationDatePlaceHolder: string;
  cardHolderNameValidationMsg: string;
  cardHolderNamePlaceHolder: string;
}
// tslint:disable-next-line:max-file-line-count
