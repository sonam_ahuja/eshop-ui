import React, { FunctionComponent, ReactNode } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Loadable, { LoadingComponentProps } from 'react-loadable';

const loading: FunctionComponent<LoadingComponentProps> = () => null;

const ProductList = Loadable({
  loading,
  loader: () => import('./routes/productList')
});

const Search = Loadable({
  loading,
  loader: () => import('./routes/search')
});

const ProductDetailed = Loadable({
  loading,
  loader: () => import('./routes/productDetailed')
});

const Tariff = Loadable({
  loading,
  loader: () => import('./routes/tariff')
});

export interface IProps extends RouteComponentProps<IRouterParams> {}

export interface IRouterParams {
  history?: string;
  location?: string;
  match?: string;
}

interface IState {
  isProductListRoutePresent: boolean;
  isSearchRoutePresent: boolean;
  isProductDetailedRoutePresent: boolean;
  isTariffPresent: boolean;
}

const NotFound = Loadable({
  loading,
  loader: () => import('./routes/notFound')
});

export class SwitchRenderRoute extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      isSearchRoutePresent: false,
      isProductListRoutePresent: false,
      isProductDetailedRoutePresent: false,
      isTariffPresent: false
    };
    this.routeCheck = this.routeCheck.bind(this);
    this.removeLastExtraCharacter = this.removeLastExtraCharacter.bind(this);
  }

  componentWillMount(): void {
    this.routeCheck();
  }

  removeLastExtraCharacter(URL: string): string {
    if (URL.substring(URL.length - 1) === '/') {
      URL = URL.substring(0, URL.length - 1);
      this.props.history.push(URL);
    }

    return URL;
  }

  routeCheck(): void {
    const { history } = this.props;
    if (history.location.pathname) {
      const updatedURL = this.removeLastExtraCharacter(
        history.location.pathname
      );
      const arrPaths = updatedURL.split('/');
      if (arrPaths.includes('search')) {
        this.setState({
          isSearchRoutePresent: true,
          isProductListRoutePresent: false,
          isProductDetailedRoutePresent: false,
          isTariffPresent: false
        });
      }
      if (arrPaths.includes('tariff')) {
        // We are adding this check because for the listin,
        // we always have routes like www.eshop.com/device/mobile-device or www.eshop.com/tvs
        this.setState({
          isProductListRoutePresent: false,
          isProductDetailedRoutePresent: false,
          isTariffPresent: true
        });
      } else if (arrPaths.length === 2) {
        // We are adding this check because for the listing page,
        // we always have routes like www.eshop.com/device/mobile-device or www.eshop.com/tvs
        this.setState({
          isProductListRoutePresent: true,
          isProductDetailedRoutePresent: false,
          isTariffPresent: false
        });
      } else if (arrPaths.length === 5) {
        // We are adding this check because for the product detailed page,
        // we always have routes like www.eshop.com/variant-name/product-id/variant-id
        this.setState({
          isProductListRoutePresent: false,
          isProductDetailedRoutePresent: true,
          isTariffPresent: false
        });
      }
    }
  }

  componentDidUpdate(prevProps: IProps): void {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.routeCheck();
    }
  }

  render(): ReactNode {
    const {
      isSearchRoutePresent,
      isProductListRoutePresent,
      isProductDetailedRoutePresent,
      isTariffPresent
    } = this.state;

    return (
      <>
        {isSearchRoutePresent && <Search />}
        {isProductListRoutePresent && <ProductList />}
        {isProductDetailedRoutePresent && <ProductDetailed />}
        {isTariffPresent && <Tariff />}
        {!isProductListRoutePresent &&
          !isProductDetailedRoutePresent &&
          !isTariffPresent && <NotFound />}
      </>
    );
  }
}

export default withRouter(SwitchRenderRoute);
