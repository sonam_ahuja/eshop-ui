import { IRecoveryWithEmailRequest } from '@authentication/store/types';

export namespace POST {
  export type IRequest = IRecoveryWithEmailRequest;
  export type IResponse = void;
}
