import {
  IEmailVerificationRequest,
  IEmailVerificationResponse
} from '@authentication/store/types';

export namespace GET {
  export type IRequest = IEmailVerificationRequest;
  export type IResponse = IEmailVerificationResponse;
}
