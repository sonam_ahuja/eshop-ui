import { IGoogleLoginParams as IGoogleLoginRequest } from '@authentication/store/types';

export namespace POST {
  export type IRequest = IGoogleLoginRequest;
  export type IResponse = void;
}
