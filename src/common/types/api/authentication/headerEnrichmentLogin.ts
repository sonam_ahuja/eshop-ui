import { ILoginWithHeaderEnrichmentRequest } from '@authentication/store/types';

export namespace POST {
  export type IRequest = ILoginWithHeaderEnrichmentRequest;
  export type IResponse = void;
}
