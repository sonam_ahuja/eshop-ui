import { ILoginOtpRequest, ILoginResponse } from '@authentication/store/types';

export namespace POST {
  export type IRequest = ILoginOtpRequest;
  export type IResponse = void;
}

export namespace PUT {
  export type IRequest = ILoginOtpRequest;
  export type IResponse = ILoginResponse;
}
