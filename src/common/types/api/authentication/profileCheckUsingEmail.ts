import { IProfileCheckRequest, IProfileCheckResponse } from '@authentication/store/types';

export namespace POST {
  export type IRequest = IProfileCheckRequest;
  export type IResponse = IProfileCheckResponse;
}
