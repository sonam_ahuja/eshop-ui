import { IEmailVerificationRequest } from '@authentication/store/types';

export namespace POST {
  export type IRequest = IEmailVerificationRequest;
  export type IResponse = void;
}
