import {
  ILoginResponse,
  ILoginUsernameRequest
} from '@authentication/store/types';

export namespace POST {
  export type IRequest = ILoginUsernameRequest;
  export type IResponse = ILoginResponse;
}
