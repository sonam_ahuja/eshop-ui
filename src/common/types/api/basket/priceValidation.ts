/**
 * This file is used to typesafe API contracts
 */
import { IValidateCartPriceResponse } from '@basket/store/types';

export namespace PATCH {
  export type IRequest = void;
  export type IResponse = IValidateCartPriceResponse;
}
