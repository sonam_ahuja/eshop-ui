import { ICategory as ICategoryResponse } from '@category/store/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = ICategoryResponse;
}
