import { IAddressResponse } from '@checkout/store/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IAddressResponse;
}

export namespace PUT {
  export type IRequest = void;
  export type IResponse = void;
}

export namespace POST {
  export type IRequest = void;
  export type IResponse = void;
}

export namespace DELETE {
  export type IRequest = void;
  export type IResponse = void;
}
