import { IBillTypeResponse } from '@checkout/store/billing/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IBillTypeResponse;
}
