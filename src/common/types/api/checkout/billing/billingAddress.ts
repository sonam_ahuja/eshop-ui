import {
  IPatchBillingAddressPayload,
  IPostBillingAddressPayload
} from '@checkout/store/billing/types';

export namespace POST {
  export type IRequest = IPostBillingAddressPayload;
  export type IResponse = void;
}

export namespace PATCH {
  export type IRequest = IPatchBillingAddressPayload;
  export type IResponse = void;
}
