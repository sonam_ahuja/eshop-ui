import {
  IAddDeleteCartRequest,
  IAddDeleteCartResponse,
  IResourcePoolResponse,
  ITermConditionResponse,
  IValidateNumberRequest,
  IValidateNumberResponse,
  IVerifyOTPRequest,
  IVerifyOTPResponse
} from '@checkout/store/numberPorting/types';

export namespace POST {
  export type IRequest = IVerifyOTPRequest;
  export type IResponse = IVerifyOTPResponse;
}

export namespace PHONE_POST {
  export type IRequest = IValidateNumberRequest;
  export type IResponse = IValidateNumberResponse;
}

export namespace PATCH {
  export type IRequest = IAddDeleteCartRequest;
  export type IResponse = IAddDeleteCartResponse;
}
export namespace RESOURCE_POOL_GET {
  export type IRequest = string;
  export type IResponse = IResourcePoolResponse;
}

export namespace TERM_CONDITION_GET {
  export type IRequest = void;
  export type IResponse = ITermConditionResponse;
}
