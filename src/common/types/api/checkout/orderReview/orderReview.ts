import {
  IMarketDiscountRequest,
  IOrderReviewResponse
} from '@checkout/store/orderReview/types';
import { ICartSummary } from '@src/common/routes/basket/store/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IOrderReviewResponse;
}

export namespace CONSENT_POST {
  export type IRequest = IMarketDiscountRequest;
  export type IResponse = ICartSummary;
}
