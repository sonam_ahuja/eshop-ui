import {
  IOrderTrackingError,
  IOrderTrackingResponse
} from '@checkout/store/orderReview/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IOrderTrackingResponse | IOrderTrackingError;
}
