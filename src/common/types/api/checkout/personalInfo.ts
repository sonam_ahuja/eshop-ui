/**
 * This file is used to typesafe API contracts
 */
import {
  IPersonalInfoConverterResponse,
  IPersonalInfoPatchRequest,
  IPersonalInfoPostRequest,
  IPersonalInfoState
} from '@checkout/store/personalInfo/types';

export namespace GET {
  export type IRequest = IPersonalInfoState;
  export type IResponse = IPersonalInfoConverterResponse;
}

export namespace PUT {
  export type IRequest = void;
  export type IResponse = void;
}

export namespace POST {
  export type IRequest = IPersonalInfoPostRequest;
}

export namespace PATCH {
  export type IRequest = IPersonalInfoPatchRequest;
}

export namespace DELETE {
  export type IRequest = void;
  export type IResponse = void;
}
