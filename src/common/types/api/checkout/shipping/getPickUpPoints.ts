import {
  IPickUpPointsRequest,
  ISelectedStoreAddress
} from '@src/common/routes/checkout/store/shipping/types';

export namespace POST {
  export type IRequest = IPickUpPointsRequest;
  export type IResponse = ISelectedStoreAddress[];
}
