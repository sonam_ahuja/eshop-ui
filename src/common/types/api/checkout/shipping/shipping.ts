/**
 * This file is used to typesafe API contracts
 */
import {
  IShippingAddressParams,
  IShippingAddressResponse
} from '@checkout/store/shipping/types';

export namespace GET {
  export type IRequest = string;
  export type IResponse = IShippingAddressResponse;
}

export namespace PUT {
  export type IRequest = void;
  export type IResponse = void;
}

export namespace POST {
  export type IRequest = IShippingAddressParams;
  export type IResponse = IShippingAddressResponse;
}

export namespace PATCH {
  export type IRequest = IShippingAddressParams;
  export type IResponse = IShippingAddressResponse;
}

export namespace DELETE {
  export type IRequest = void;
  export type IResponse = void;
}
