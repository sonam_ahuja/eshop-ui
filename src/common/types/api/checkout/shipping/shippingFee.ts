import {
  IShippingFeeRequest,
  IShippingFeeResponse
} from '@checkout/store/shipping/types';

export namespace POST {
  export type IRequest = IShippingFeeRequest;
  export type IResponse = IShippingFeeResponse;
}
