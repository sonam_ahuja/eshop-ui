import {
  IThirdPartyVerificaation,
  IVerifyUsingProvidersRequest,
  IVerifyUsingProvidersResponse
} from '@common/store/types/common';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IThirdPartyVerificaation;
}

export namespace POST {
  export type IRequest = IVerifyUsingProvidersRequest;
  export type IResponse = IVerifyUsingProvidersResponse;
}
