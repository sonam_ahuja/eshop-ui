import { IVerifyUsingProvidersResponse as IValidateUserUsingBankPostRequest } from '@common/store/types/common';

export namespace POST {
  export type IRequest = IValidateUserUsingBankPostRequest;
  export type IResponse = void;
}
