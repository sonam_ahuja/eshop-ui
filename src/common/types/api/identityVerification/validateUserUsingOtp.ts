import { IValidateUserUsingOtpRequest } from '@common/store/types/common';

export namespace POST {
  export type IRequest = IValidateUserUsingOtpRequest;
  export type IResponse = void;
}
