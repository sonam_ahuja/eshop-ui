import { IProlongationPlaceOrderRequest } from '@productList/store/types';

export namespace POST {
  export type IRequest = IProlongationPlaceOrderRequest;
  export type IRespnse = void;
}
