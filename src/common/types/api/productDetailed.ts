import {
  IFetchProductDetailsPayload as IProductDetailsRequest,
  IJsonLdProductPayload,
  IJsonLdProductResponse,
  INotifyMePayload as INotifyMeRequest,
  IProductAttributesResponse,
  IProductDetailedData as IProductDetailedResponse,
  IProductSpecification as IProductSpecificationResponse,
} from '@productDetailed/store/types';
import { IAddToBasketPayload } from '@tariff/store/types';
export namespace GET_CATEGORY_ATTRIBUTES {
  export type IRequest = string;
  export type IResponse = IProductAttributesResponse;
}
export namespace POST {
  export type IRequest = IProductDetailsRequest;
  export type IResponse = IProductDetailedResponse;
}

export namespace NOTIFY_ME {
  export type IRequest = INotifyMeRequest;
  export type IResponse = void;
}

export namespace GET_PRODUCT_SPECIFICATION {
  export type IRequest = string;
  export type IResponse = IProductSpecificationResponse[];
}

export namespace ADD_TO_BASKET {
  export type IRequest = IAddToBasketPayload;
  export type IResponse = void;
}

export namespace JSON_LD {
  export type IRequest = IJsonLdProductPayload;
  export type IResponse = IJsonLdProductResponse;
}
