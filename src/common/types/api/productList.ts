import {
  IProductListRequest,
  IProductListResponse
} from '@productList/store/types';

export namespace POST {
  export type IRequest = IProductListRequest;
  export type IResponse = IProductListResponse;
}
