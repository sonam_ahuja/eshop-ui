import {
  IProlongationCheckoutRequestPayload as IProlongationCheckoutRequest,
  IProlongationCheckoutResponse
} from '@productList/store/types';

export namespace POST {
  export type IRequest = IProlongationCheckoutRequest;
  export type IResponse = IProlongationCheckoutResponse;
}
