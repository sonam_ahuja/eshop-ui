import { IMegaMenu } from '@src/common/store/types/common';

export namespace GET {
  export type IResponse = IMegaMenu;
}
