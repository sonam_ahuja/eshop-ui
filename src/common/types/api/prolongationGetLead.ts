import { IGetLead } from '@productList/store/types';

export namespace GET {
  export type IResponse = IGetLead[];
}
