import { IRSAKeyResponse } from '@productList/store/types';

export namespace GET {
  export type IResponse = IRSAKeyResponse;
}
