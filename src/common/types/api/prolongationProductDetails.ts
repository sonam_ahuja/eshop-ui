import {
  IFetchProlongationProductListPayload as IProlongationProductDetailRequest,
  IProlongationProductListResponse as ProlongationProductDetailResponse
} from '@productList/store/types';

export namespace POST {
  export type IRequest = IProlongationProductDetailRequest;
  export type IResponse = ProlongationProductDetailResponse;
}
