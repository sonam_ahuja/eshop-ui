import {
  IFetchProlongationProductListPayload as IProlongationProductListRequest,
  IProlongationProductListResponse
} from '@productList/store/types';

export namespace POST {
  export type IRequest = IProlongationProductListRequest;
  export type IResponse = IProlongationProductListResponse;
}
