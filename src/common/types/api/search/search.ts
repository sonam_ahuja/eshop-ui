import { ISearchResponse } from '@search/store/types';

export namespace GET {
  export type IRequest = void;
  export type IResponse = ISearchResponse;
}
