import { ISearchListRequest, ISearchListResponse } from '@search/store/types';

export namespace POST {
  export type IRequest = ISearchListRequest;
  export type IResponse = ISearchListResponse;
}
