import { IOutOfStockNotification } from '@productList/store/types';

export namespace POST {
  export type IRequest = IOutOfStockNotification;
  export type IResponse = void;
}
