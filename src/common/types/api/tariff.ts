import {
  ISaveTariff,
  ITariffAddonPriceRequest,
  ITariffAddonPriceResponse,
  ITariffRequest,
  ITariffResponse
} from '@tariff/store/types';
export namespace GET {
  export type IRequest = ITariffRequest;
  export type IResponse = ITariffResponse;
}

export namespace POST {
  export type IRequest = ISaveTariff;
  export type IResponse = null;
}

export namespace GET_TARIFF_ADDON_PRICE {
  export type IRequest = ITariffAddonPriceRequest;
  export type IResponse = ITariffAddonPriceResponse;
}
