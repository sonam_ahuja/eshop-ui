import { IUserDetails } from '@common/store/types/common';

export namespace GET {
  export type IRequest = void;
  export type IResponse = IUserDetails;
}
