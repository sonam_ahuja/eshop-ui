export interface IGeocodeResult {
  results: IResult[];
  status: string;
}

export interface IResult {
  address_components: IAddresscomponent[];
  formatted_address: string;
  geometry: IGeometry;
  place_id: string;
  types: string[];
}

interface IGeometry {
  bounds: IBounds;
  location: INortheast;
  location_type: string;
  viewport: IBounds;
}

interface IBounds {
  northeast: INortheast;
  southwest: INortheast;
}

export interface INortheast {
  lat: number;
  lng: number;
}

interface IAddresscomponent {
  long_name: string;
  short_name: string;
  types: string[];
}

export interface IGeoAutoCompleteResult {
  predictions: IGeoPrediction[];
  status: string;
}

interface ITerms {
  offset: number;
  value: string;
}

export interface IGeoPrediction {
  terms: ITerms[];
  description: string;
  geometry: IGeometry;
  place_id: string;
  types: string[];
  id: string;
}

export interface IGeocoderAddressComponent {
  long_name: string;
  short_name: string;
  types: string[];
}

export interface IGeocoderGeometry {
  location: {
    // tslint:disable:no-any
    lat: any;
    lng: any;
  };
}

export interface IGeocoderResult {
  address_components: IGeocoderAddressComponent[];
  formatted_address: string;
  geometry: IGeocoderGeometry;
  place_id: string;
  postcode_localities: string[];
  types: string[];
}
