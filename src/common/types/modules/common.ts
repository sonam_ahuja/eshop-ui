export enum PAYMENT_TYPES {
  MONTHLY = 'monthly',
  UPFRONT = 'upfront'
}

export enum VALIDATION_TYPE {
  MIN = 'min',
  MAX = 'max',
  REGEX = 'regex',
  BETWEEN = 'between',
  NONE = 'none',
  MIN_UPPERCASE = 'min-uppercase',
  MIN_LOWERCASE = 'min-lowercase',
  MIN_DIGIT = 'min-digit',
  EXACT = 'exact'
}
