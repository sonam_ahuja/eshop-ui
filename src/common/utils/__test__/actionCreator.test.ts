import { actionCreator } from '@utils/actionCreator';
import constants from '@store/constants/configuration';

describe('Action Creator', () => {
  it('Action Creator should create action', () => {
    const expectedAction = {
      type: constants.SET_CMS_CONFIGURATION_DATA,
      payload: undefined
    };
    expect(actionCreator(constants.SET_CMS_CONFIGURATION_DATA)()).toEqual(
      expectedAction
    );
  });
});
