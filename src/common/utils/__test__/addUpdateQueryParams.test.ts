import { updateQueryStringParameter } from '@utils/addUpdateQueryParams';

describe('parseQueryString Utils', () => {
  it('should call remove query params from URL successfully', () => {
    expect(
      updateQueryStringParameter(
        '?hello=world',
        'newQueryKey',
        'NewQueryKeyValue'
      )
    ).toBeDefined();
    expect(
      updateQueryStringParameter('', 'newQueryKey', 'NewQueryKeyValue')
    ).toBeDefined();
  });
});
