import apiCaller from '@utils/apiCaller';

describe('apiCaller', () => {
  it('apiCaller', () => {
    expect(apiCaller.get('')).toBeDefined();
    expect(apiCaller.post('', {})).toBeDefined();
    expect(apiCaller.put('', {})).toBeDefined();
    expect(apiCaller.patch('', {})).toBeDefined();
    expect(apiCaller.delete('')).toBeDefined();
  });
});
