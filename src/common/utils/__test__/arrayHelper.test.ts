import { findIndexOnKey, findOnKey } from '@utils/arrayHelper';

describe('Maths Utils', () => {
  it('findIndexOnKey method test', () => {
    const arrayData = [
      {
        nameKey: 'valueKey1'
      },
      {
        nameKey: 'valueKey2'
      }
    ];
    expect(findIndexOnKey(arrayData, 'nameKey', 'valueKey2')).toBe(1);
  });
  it('findOnKey method test', () => {
    const arrayData = [
      {
        nameKey: 'valueKey1'
      },
      {
        nameKey: 'valueKey2'
      }
    ];
    expect(findOnKey(arrayData, 'nameKey', 'valueKey2')).toBeDefined();
  });
});
