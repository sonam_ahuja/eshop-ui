import { formatCurrency, getFormatedCurrencyValue } from '@utils/currency';

describe('CurrencyTest', () => {
  it('formatCurrency symbol should return defined value', () => {
    expect(formatCurrency(12, 'USD')).toBeDefined();
  });
  it('formatCurrency symbol should return defined value', () => {
    expect(getFormatedCurrencyValue(12)).toBeDefined();
  });

  it('formatCurrency symbol should return defined value if value is decimal', () => {
    expect(getFormatedCurrencyValue(42.22)).toBeDefined();
  });
});
