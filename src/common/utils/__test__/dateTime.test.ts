import { getDateDifference } from '@utils/dateTime';

describe('Time difference', () => {
  it('getDateDifference method test', () => {
    expect(getDateDifference(new Date(), new Date())).toBeDefined();
  });
});
