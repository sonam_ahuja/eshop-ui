import { downloadPdfFile } from '@utils/downloadBlob';

describe('DownloadBlob', () => {
  it('downloadPdfFile method test', () => {
    const result = downloadPdfFile('http://wwww.google.com/demo');
    expect(downloadPdfFile('http://wwww.google.com/demo')).toBe(result);
  });
});
