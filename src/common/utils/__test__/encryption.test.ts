import { prolongationEncryption } from '@utils/encryptions';

describe('Encryption', () => {
  it('should call the encryption function', () => {
    expect(
      prolongationEncryption(
        'Hello world',
        `-----BEGIN PUBLIC KEY-----
    randomPemKey
    -----END PUBLIC KEY-----`
      )
    ).toBeDefined();
  });
});
