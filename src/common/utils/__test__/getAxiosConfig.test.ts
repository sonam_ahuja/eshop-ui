import getAxiosConfig from '@utils/getAxiosRequestConfig';
import APP_CONSTANTS from '@common/constants/appConstants';
import { apiEndpoints } from '@src/common/constants';

describe('getAxiosConfig Utils', () => {
  // tslint:disable-next-line:no-duplicate-string
  it('should call getAxiosConfig successfully', () => {
    expect(
      getAxiosConfig({
        uid: '',
        withBaseUrl: true,
        loader: false,
        errorToast: false,
        url: `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
          apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN.url
        }`
      })
    ).toBeDefined();
  });

  it('should call getAxiosConfig successfully', () => {
    expect(
      getAxiosConfig({
        uid: '',
        withBaseUrl: false,
        loader: false,
        errorToast: true,
        url: 'hello'
      })
    ).toBeDefined();
  });

  it('should call getAxiosConfig successfully', () => {
    expect(
      getAxiosConfig({
        uid: '',
        withBaseUrl: false,
        loader: true,
        errorToast: true,
        timeout: 0,
        url: `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
          apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN.url
        }/checkout`
      })
    ).toBeDefined();
  });

  it('should call getAxiosConfig successfully', () => {
    jest.mock('@utils/index', () => ({
      isBrowser: true
    }));

    expect(
      getAxiosConfig({
        uid: '',
        withBaseUrl: false,
        loader: true,
        errorToast: true,
        timeout: 0,
        url: `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
          apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN.url
        }/checkout`
      })
    ).toBeDefined();
  });

  it('withCredentials set to false', () => {
    expect(
      getAxiosConfig({
        uid: '',
        withBaseUrl: false,
        loader: true,
        errorToast: true,
        timeout: 0,
        url: `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
          apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN.url
        }/checkout`,
        withCredentials: false
      })
    ).toBeDefined();
  });
});
