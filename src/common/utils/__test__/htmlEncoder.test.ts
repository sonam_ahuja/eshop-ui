import { htmlEntityDecode, htmlEntityEncode } from '@utils/htmlEncoder';

describe('Html Encoder', () => {
  it('getValueFromLocalStorage method test', () => {
    expect(htmlEntityDecode('nameKey')).toBeDefined();
  });

  it('setValueInLocalStorage method test', () => {
    expect(htmlEntityEncode('nameKey')).toBeDefined();
  });

  it('getValueFromLocalStorage method test, error', () => {
    expect(htmlEntityDecode('[ssdsdsd}')).toBeDefined();
  });

  it('setValueInLocalStorage method test, error', () => {
    expect(htmlEntityEncode('[ssdsdsd}')).toBeDefined();
  });
});
