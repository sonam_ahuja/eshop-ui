import {
  deepMergeObj,
  hexToRgbA,
  logError,
  logInfo,
  noopFn,
  updatedImageURL
} from '@utils/index';

describe('Deep Merge objects', () => {
  it('Merge of objects with 1st level keys does not match will return targetObject', () => {
    const targetObj = {
      key1: {
        name: 'p1'
      }
    };
    const sourceObj = {
      key2: {
        name: 'p1'
      }
    };
    expect(deepMergeObj(targetObj, sourceObj)).toEqual(targetObj);
  });
  it(`Merge of objects with some 2nd level keys does not match /\
  will return targetObject at 2nd level keys at overriden values at 2nd level `, () => {
    const targetObj = {
      key1: {
        key11: {
          key111: {
            name: 'Ramesh'
          },
          key112: {
            name: 'Sourav'
          },
          key113: {
            name: 'Gaurav'
          }
        }
      }
    };
    const sourceObj = {
      key1: {
        key11: {
          key111: {
            name: 'Suresh'
          },
          key114: {
            name: 'Kamlesh'
          }
        }
      }
    };
    const expectedObj = {
      key1: {
        key11: {
          key111: {
            name: 'Suresh'
          },
          key112: {
            name: 'Sourav'
          },
          key113: {
            name: 'Gaurav'
          }
        }
      }
    };
    expect(deepMergeObj(targetObj, sourceObj)).toEqual(expectedObj);
  });
  it('Merge of objects with nth level keys merging ', () => {
    const targetObj = {
      key1: {
        key11: {
          key111: 'Sourav',
          key112: {
            name: 'Ramesh'
          },
          key113: 'Soumitra'
        }
      }
    };
    const sourceObj = {
      key1: {
        key11: {
          key111: {
            key1111: {
              name: 'gaurav'
            },
            key1112: {
              name: 'Suresh'
            }
          },
          key112: {
            name: 'RameshJi'
          }
        }
      }
    };
    const expectedObj = {
      key1: {
        key11: {
          key111: {
            key1111: {
              name: 'gaurav'
            },
            key1112: {
              name: 'Suresh'
            }
          },
          key112: {
            name: 'RameshJi'
          },
          key113: 'Soumitra'
        }
      }
    };
    expect(deepMergeObj(targetObj, sourceObj)).toEqual(expectedObj);
  });

  test('noopFn', () => {
    expect(noopFn()).toBeUndefined();
  });

  test('hexToRgbA with alpha', () => {
    expect(hexToRgbA('#cccccc', '1')).toBeDefined();
  });

  test('hexToRgbA without alpha', () => {
    expect(hexToRgbA('#cccccc', 0)).toBeDefined();
  });

  test('hexToRgbA with alpha', () => {
    expect(logInfo('#cccccc')).toBeUndefined();
  });

  test('hexToRgbA without alpha', () => {
    expect(logError('#cccccc')).toBeUndefined();
  });

  test('updatedImageURL should be called', () => {
    expect(updatedImageURL('http://dummy.com/img')).toBeDefined();
  });
});
