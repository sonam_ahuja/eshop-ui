import {
  getValueFromLocalStorage,
  removeItemFromLocalStorage,
  setValueInLocalStorage
} from '@utils/localStorage';

describe('Local Storage', () => {
  it('getValueFromLocalStorage method test', () => {
    expect(getValueFromLocalStorage('nameKey')).toBeDefined();
  });

  it('setValueInLocalStorage method test', () => {
    expect(setValueInLocalStorage('key', 'value')).toBeUndefined();
  });

  it('removeItemFromLocalStorage method test', () => {
    expect(removeItemFromLocalStorage('key')).toBeUndefined();
  });
});
