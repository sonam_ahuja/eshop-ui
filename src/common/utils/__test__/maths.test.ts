import { convertToFloat } from '@utils/maths';

describe('Maths Utils', () => {
  it('convertToFloat method test', () => {
    expect(convertToFloat(12345)).toBe('12345.00000');
  });
});
