import {
  createQueryString,
  parseQueryString,
  removeQueryParam,
  updateQueryParam
} from '@utils/parseQuerySrting';

describe('parseQueryString Utils', () => {
  it('should call parseQueryString successfully', () => {
    expect(parseQueryString('?hello=world&world=hello')).toBeDefined();
  });
  it('should call parseQueryString successfully', () => {
    expect(removeQueryParam('', '')).toBeDefined();
  });
});

describe('createQueryString Utils', () => {
  it('should call createQueryString successfully', () => {
    expect(
      createQueryString({ firstQuery: 'first', secondQuery: 'second' })
    ).toBeDefined();
  });
});

describe('updateQueryParam Utils', () => {
  it('should call updateQueryParam successfully', () => {
    expect(updateQueryParam(' firstQuery', false)).toBeDefined();
  });
});
