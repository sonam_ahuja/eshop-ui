import { removeParamFromQueryString } from '@utils/removeQueryParam';

describe('parseQueryString Utils', () => {
  it('should call remove query params from URL successfully', () => {
    expect(
      removeParamFromQueryString('hello', '?hello=world&world=hello')
    ).toBeDefined();

    expect(
      removeParamFromQueryString('hello', 'https://google.com')
    ).toBeDefined();
  });
});
