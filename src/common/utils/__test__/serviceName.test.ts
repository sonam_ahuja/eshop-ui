import { getServiceName } from '@utils/serviceName';

describe('getServiceName Utils', () => {
  it('should call getServiceName successfully', () => {
    expect(getServiceName('hello')).toBeDefined();
  });

  it('should call getServiceName successfully with other parameter', () => {
    expect(getServiceName('hello/')).toBeDefined();
  });

  it('should call getServiceName successfully with other parameter', () => {
    expect(getServiceName('/hello/')).toBeDefined();
  });
});
