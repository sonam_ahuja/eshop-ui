import {
  getValueFromSessionStorage,
  removeValueFromSessionStorage,
  setValueInSessionStorage
} from '@utils/sessionStorage';

describe('Session storage', () => {
  it('getValueFromSessionStorage method test', () => {
    expect(getValueFromSessionStorage('nameKey')).toBeDefined();
  });

  it('removeValueFromSessionStorage method test', () => {
    expect(removeValueFromSessionStorage('key')).toBeUndefined();
  });
  it('setValueInSessionStorage method test', () => {
    expect(setValueInSessionStorage('key', 'value')).toBeUndefined();
  });
});
