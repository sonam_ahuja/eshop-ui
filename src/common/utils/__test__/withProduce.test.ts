import withProduce from '@utils/withProduce';

describe('withProduce Utils', () => {
  it('should call withProduce successfully', () => {
    expect(withProduce(() => ({}), {})).toBeDefined();
  });

  it('should call withProduce successfully with undefined inisital state', () => {
    expect(
      withProduce(() => ({}), {
        dummy: () => {
          //
        }
      })(undefined, {
        type: 'dummy',
        payload: 'dummy'
      })
    ).toBeDefined();
  });

  it('should call withProduce successfully defined inisital state', () => {
    expect(
      withProduce(
        () => ({
          dummy: 'dummy'
        }),
        {
          dummy: () => {
            //
          }
        }
      )(
        { dummy: 'dummy' },
        {
          type: 'dummy',
          payload: 'dummy'
        }
      )
    ).toBeDefined();
  });
});
