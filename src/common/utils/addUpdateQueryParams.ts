export const updateQueryStringParameter = (
  queryString: string,
  key: string,
  value: string
): string => {
  const re = new RegExp(`([?&])${key}=.*?(&|$)`, 'i');
  const separator =
    decodeURIComponent(queryString).indexOf('?') !== -1 ? '&' : '?';
  const updatedQueryString =
    queryString.indexOf('?') !== -1 ? queryString.split('?')[1] : '';
  if (updatedQueryString.match(re)) {
    return updatedQueryString.replace(re, `$1${key}=${value}$2`);
  } else {
    return separator === '?'
      ? `${key}=${value}`
      : `${updatedQueryString}${separator}${key}=${value}`;
  }
};
