export const findIndexOnKey = (
  objectArray: object[],
  key: string | number,
  value: string | number
) => {
  return objectArray
    ? objectArray.findIndex(obj => {
        return obj[key] === value;
      })
    : -1;
};

export const findOnKey = (
  objectArray: object[],
  key: string | number,
  value: string | number
) => {
  return objectArray
    ? objectArray.find(obj => {
        return obj[key] === value;
      })
    : null;
};
