import history from '@src/client/history';
import axios, { AxiosError, AxiosResponse } from 'axios';
import store from '@store/index';
import { commonAction } from '@store/actions';
import authAction from '@authentication/store/actions';
import basketActions from '@basket/store/actions';
import { isBrowser } from '@utils/index';
import { IApiCallerConfig } from '@utils/apiCaller';
import axiosInterceptorConfig from '@common/utils/getAxiosRequestConfig';
import { ERROR_CODE_BFF, ONEAPP_PARAMS } from '@common/constants/appConstants';
import APP_TYPE from '@common/constants/appType';
import ROUTES_CONSTANTS from '@common/constants/routes';
import { getValueFromParams } from '@productList/utils';
import { sendForApiError } from '@src/common/oneAppRoutes/utils/googleTracking';
import { IError as IGenricError } from '@common/store/types/common';
import { BFF_ERROR_CODE } from '@common/components/Error/constants';

import { encodeRFC5987ValueChars } from './encodeURL';
import { updateQueryParam } from './parseQuerySrting';

interface ILoaderMap {
  [key: string]: null | number | NodeJS.Timer;
}
const loaderMap: ILoaderMap = {};

axios.interceptors.request.use(
  (config: IApiCallerConfig = {}): IApiCallerConfig => {
    return axiosInterceptorConfig(config);
  },
  (error: AxiosError): Promise<object> => {
    if (isBrowser && error && error.config) {
      const { loader, uid } = error.config as IApiCallerConfig;
      if (loader) {
        clearTimeout(loaderMap[uid as string] as number);
        loaderMap[uid as string] = null;
        store.dispatch(commonAction.removeLoader());
      }
    }

    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response): AxiosResponse => {
    if (isBrowser && response && response.config) {
      const { loader, uid } = response.config as IApiCallerConfig;
      if (loader) {
        clearTimeout(loaderMap[uid as string] as number);
        loaderMap[uid as string] = null;
        store.dispatch(commonAction.removeLoader());
      }
    }

    return response.data;
  },
  // tslint:disable-next-line:cyclomatic-complexity
  (error: AxiosError): Promise<object> => {
    if (isBrowser && error && error.config) {
      const { errorToast, loader, uid } = error.config as IApiCallerConfig;
      const state = store.getState();

      if (loader) {
        clearTimeout(loaderMap[uid as string] as number);
        loaderMap[uid as string] = null;
        store.dispatch(commonAction.removeLoader());
      }
      if (
        errorToast ||
        (error.response &&
          (error.response.status >= 400 && error.response.status <= 510))
      ) {
        let errorsObj = state.translation.cart.global.errors;
        let isPPP = false;
        if (history) {
          const channel = getValueFromParams(
            history.location.search,
            ONEAPP_PARAMS.CHANNEL
          );
          if (String(channel).toLowerCase() === APP_TYPE.ONEAPP) {
            isPPP = true;
            errorsObj = state.translation.prolongation.global.errors;
            store.dispatch(
              commonAction.showError(
                errorsObj[
                  error.response &&
                  error.response.data &&
                  error.response.data.code
                    ? ERROR_CODE_BFF[error.response.data.code]
                    : error.response && error.response.status === 401
                    ? ERROR_CODE_BFF.HANDLE_401
                    : 500
                ] || error.message
              )
            );
          } else {
            // e-shop error screen condition!
            const errorTranslation = state.translation.cart.global.errors;
            const httpStatusCode = error.response && error.response.status;
            const {
              code,
              debugMessage = '',
              errorMessage: message,
              retryable = false
            } = error.response && error.response.data;

            const userMessage =
              (message && message.userMessage) ||
              errorTranslation[ERROR_CODE_BFF[code]] ||
              errorTranslation[`description${httpStatusCode}`] ||
              errorTranslation.default;

            const payload: IGenricError = {
              code,
              retryable,
              showFullPageError: !!(error.config as IApiCallerConfig)
                .showFullPageError,
              skipGenericError: !!(error.config as IApiCallerConfig)
                .skipGenericError,
              // need to update when new contract will implement contact @Ankit Banka
              // https://confluence.dtoneapp.telekom.net/display/EC/Genric+Error+API+Contract
              message:
                message && message.userMessage
                  ? message
                  : {
                      debugMessage,
                      userMessage
                    },
              httpStatusCode
            };
            if (history && error.response) {
              if (error.response.status === 401) {
                store.dispatch(authAction.logoutSuccess());
              } else {
                switch (true) {
                  case code === BFF_ERROR_CODE.DT_DEVICE_TARIFF_RESTRICTION:
                    store.dispatch(
                      basketActions.setCartRestrictionMessage(true)
                    );
                    history.push(ROUTES_CONSTANTS.BASKET);
                    break;
                  case code === BFF_ERROR_CODE.DT_NO_CART_AVAILABLE:
                    history.push(ROUTES_CONSTANTS.BASKET);
                    break;
                  default:
                    if (
                      !state.common.error.httpStatusCode ||
                      (!state.common.error.showFullPageError &&
                        payload.showFullPageError)
                    ) {
                      store.dispatch(commonAction.showGenricError(payload));
                    }
                    const newURL = updateQueryParam(
                      history.location.search,
                      false
                    );
                    history.replace({
                      pathname: history.location.pathname,
                      search: newURL
                    });
                    break;
                }
              }
            }
          }
        }

        if (isPPP) {
          const errorCode = error.response && error.response.status;

          let errorMsg =
            error.response && error.response.data && error.response.data.code
              ? errorsObj[ERROR_CODE_BFF[error.response.data.code]]
              : error.response && error.response.statusText;

          if (
            error.response &&
            error.response.status &&
            error.response.status === 401
          ) {
            errorMsg = errorsObj[ERROR_CODE_BFF.HANDLE_401];
          }

          const errorUrl = encodeRFC5987ValueChars(error.config.url as string);
          const decodedUrl = error.config.url;

          if (
            !(
              decodedUrl &&
              (decodedUrl.includes('dps') || decodedUrl.includes('refresh'))
            )
          ) {
            sendForApiError(errorMsg as string, errorCode as number, errorUrl);
          }
        }
      } else {
        const errorsObj = state.translation.prolongation.global.errors;
        const errorUrl = encodeRFC5987ValueChars(error.config.url as string);

        sendForApiError(
          errorsObj[ERROR_CODE_BFF.CANCELED_MSG],
          Number(errorsObj[ERROR_CODE_BFF.CANCELED_STATUS_CODE]),
          errorUrl
        );
      }
    }

    return Promise.reject(
      error.response
        ? error.response.status === 401 ||
          error.response.status === 301 ||
          error.response.status === 302
          ? error.response
          : error.response.data
        : { error: 'An unknown error has occured!' }
    );
  }
);
