export const calculateTruncation = (el: HTMLElement): void => {
  let text;
  while (el.clientHeight < el.scrollHeight) {
    text = el.innerHTML.trim();
    if (text.split(' ').length <= 1) {
      break;
    }
    el.innerHTML = text.replace(/\W*\s(\S)*$/, '...');
  }
};
