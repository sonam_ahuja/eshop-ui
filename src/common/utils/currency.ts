import store from '@common/store';
import { currencyDetails } from '@common/constants/currencyDetails';
import { logError } from '@utils/index';

export const getCurrencyLocale = () =>
  store.getState().configuration.cms_configuration.global.currency.locale;

export const formatCurrency = (currency: number, currencyCode: string) => {
  return `${getFormatedPriceValue(currency, currencyCode)} ${
    currencyCode && currencyDetails[currencyCode]
      ? currencyDetails[currencyCode].symbol
      : ''
  }`;
};

export const getCurrencySymbol = (currencyCode: string) => {
  return currencyCode && currencyDetails[currencyCode]
    ? currencyDetails[currencyCode].symbol
    : '';
};

/** USED ON E-shop */
export const getFormatedPriceValue = (
  currency: number,
  currencyCode: string
) => {
  try {
    if (String(currency).includes('.')) {
      return new Intl.NumberFormat(currencyCode, {}).format(currency);
    }

    return new Intl.NumberFormat(currencyCode, {
      minimumFractionDigits: 0
    }).format(currency);

    // tslint:disable-next-line:no-empty
  } catch (error) {
    logError(error);
  }

  return '';
};

/** USED ON PROLONGATION */
export const getFormatedCurrencyValue = (currency: number) => {
  if (String(currency).includes('.')) {
    return new Intl.NumberFormat(getCurrencyLocale(), {}).format(currency);
  } else {
    return new Intl.NumberFormat(getCurrencyLocale(), {
      minimumFractionDigits: 0
    }).format(currency);
  }
};
