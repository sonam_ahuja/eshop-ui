export const getDateDifference = (
  startDate: Date,
  endDate: Date
) => {
  return (startDate.getTime() - endDate.getTime()) / (1000 * 60 * 60 * 24);
};
