export const downloadPdfFile = (url: string) => {
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', 'privacyPolicy.pdf');
  document.body.appendChild(link);
  link.click();
};
