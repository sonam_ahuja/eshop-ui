export const prolongationEncryption = (
  payload: string,
  publicKeyPem: string
) => {
  return import('node-forge').then(forge => {
    const publicKey = forge.default.pki.publicKeyFromPem(publicKeyPem);
    const aesKey = forge.default.random.getBytesSync(32);
    const iv = forge.default.random.getBytesSync(16);
    const cipher = forge.default.cipher.createCipher('AES-CBC', aesKey);
    payload = payload
      .split('')
      .map(item => item.charCodeAt(0))
      .join(',');

    cipher.start({ iv });
    cipher.update(
      forge.default.util.createBuffer(
        unescape(encodeURIComponent(payload)),
        'utf8'
      )
    );
    cipher.finish();

    // encrypt base64 aes key with ssl public key
    const aesBuffer = forge.default.util.createBuffer(
      `${forge.default.util.encode64(iv)}:${forge.default.util.encode64(
        aesKey
      )}`,
      'utf8'
    );
    const encryptedAesBytes = aesBuffer.getBytes();

    // tslint:disable-next-line: no-any
    const encryptedAesKey = (publicKey as any).encrypt(
      encryptedAesBytes,
      'RSA-OAEP',
      {
        md: forge.md.sha1.create(),
        mgf1: {
          md: forge.md.sha1.create()
        }
      }
    );

    return {
      encryptedAesData: forge.default.util.encode64(cipher.output.data),
      encryptedAesKey: forge.default.util.encode64(encryptedAesKey)
    };
  });
};
