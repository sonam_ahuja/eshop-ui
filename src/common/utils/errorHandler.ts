import store from '@store/index';

const translation = store && store.getState().translation.cart.global;

const getErrorMessage = (code: string): string => {
  if (translation && translation.errors) {
    return translation.errors.code;
  }

  return code;
};

export default getErrorMessage;
