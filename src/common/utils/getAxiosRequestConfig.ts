import APP_CONSTANTS, { ONEAPP_PARAMS } from '@common/constants/appConstants';
import APP_TYPE from '@common/constants/appType';
import { commonAction } from '@store/actions';
import { getUID } from '@utils/maths';
import { isBrowser } from '@utils/index';
import { getServiceName } from '@utils/serviceName';
import store from '@store/index';
import { IApiCallerConfig } from '@utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import history from '@src/client/history';
import uuidV4 from 'uuid/v4';
import { getValueFromParams } from '@productList/utils';
import { getUniqueIdentifier } from '@utils/uniqueIdentifier';

interface ILoaderMap {
  [key: string]: null | number | NodeJS.Timer;
}

const loaderMap: ILoaderMap = {};
export default (config: IApiCallerConfig) => {
  const {
    withBaseUrl = true,
    loader = false,
    headers,
    timeout: configTimeout,
    withCredentials = true,
    includeHeaders = true,
    ...restConfig
  } = config;

  const baseURL = withBaseUrl ? APP_CONSTANTS.ESHOP_BASE_URL : '';
  let channel: null | string = null;
  if (history) {
    const channelParam = getValueFromParams(
      history.location.search,
      ONEAPP_PARAMS.CHANNEL
    );
    if (String(channelParam).toLocaleLowerCase() === APP_TYPE.ONEAPP) {
      channel = channelParam;
    }
  }
  const Channel = channel || APP_CONSTANTS.CHANNEL || 'US_WEB';
  let timeout = configTimeout;

  const {
    prolongation,
    eShop
  } = store.getState().configuration.cms_configuration.global;

  let getTimeout = eShop.getRequestTimeout;
  let postPatchTimeout = eShop.postPatchRequestTimeout;

  if (channel && String(channel).toLocaleLowerCase() === APP_TYPE.ONEAPP) {
    getTimeout = prolongation.getRequestTimeout;
    postPatchTimeout = prolongation.postPatchRequestTimeout;
  }

  if (timeout === 0) {
    timeout =
      String(config.method).toLowerCase() === 'get'
        ? getTimeout
        : postPatchTimeout;
  }

  const uid = getUID();
  if (isBrowser && loader) {
    loaderMap[uid] = setTimeout(
      () => store.dispatch(commonAction.showLoader()),
      APP_CONSTANTS.LOADER_MIN_DURATION
    );
  }

  if (
    // PPP Specific RequestConfig
    restConfig.url ===
      `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.GENERATE_TOKEN.url
      }` ||
    restConfig.url ===
      `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.REFRESH_TOKEN.url
      }` ||
    restConfig.url ===
      `${APP_CONSTANTS.MOENGAGE_BASE_URL}${
        apiEndpoints.PRODUCTLIST.PROLONGATION.MOENGAGE_PUSH_EVENTS.url
      }` ||
    (restConfig.url && restConfig.url.includes('hns'))
  ) {
    return {
      uid,
      withBaseUrl,
      loader,
      ...restConfig,
      baseURL,
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
        ['x-request-tracking-id']: uuidV4(),
        ...headers
      },
      timeout
    };
  }

  if (!!includeHeaders) {
    config.headers = {
      'Content-Type': 'application/json',
      Language: APP_CONSTANTS.LANGUAGE_CODE,
      Country: APP_CONSTANTS.COUNTRY_CODE,
      serviceName: restConfig.url ? getServiceName(restConfig.url) : '',
      'content-language': APP_CONSTANTS.CONTENT_LANGUAGE || 'en_US',
      Channel,
      ['x-request-tracking-id']: uuidV4(),
      ['x-request-unique-identifier']: getUniqueIdentifier(),
      ...headers
    };
  }

  const requestConfig: IApiCallerConfig = {
    uid,
    withBaseUrl,
    loader,
    ...restConfig,
    baseURL,
    headers: config.headers,
    timeout
  };

  if (channel && String(channel).toLocaleLowerCase() === APP_TYPE.ONEAPP) {
    requestConfig.withCredentials = true;
  } else if (withCredentials) {
    requestConfig.withCredentials = true;
    requestConfig.headers.Authorization = 'Auth';
  }

  return requestConfig;
};
