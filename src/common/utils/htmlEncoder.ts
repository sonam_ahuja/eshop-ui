export const htmlEntityDecode = (htmlToDecode: string) => {
  try {
    return atob(htmlToDecode);
  } catch (error) {
    return '';
  }
};

export const htmlEntityEncode = (htmlToEncode: string) => {
  try {
    return btoa(htmlToEncode);
  } catch (error) {
    return '';
  }
};
