import isMobilejs from 'ismobilejs';
import { checkSafariAndIEBrowser } from '@src/client/DOMUtils';
import { IMAGE_TYPE } from '@common/store/enums';
import { getGlobalTranslation } from '@common/store/common/index';

export const isBrowser = (() => {
  let browser = false;
  try {
    if (!!window && !!document && !!document.location) {
      browser = true;
    }
    // tslint:disable-next-line: no-empty
  } catch (e) {}

  return browser;
})();

export const isObj = (obj: object) => obj instanceof Object && obj !== null;

export const deepMergeObj = (target: object, source: object) => {
  if (!isObj(target)) {
    return source;
  }
  for (const key of Object.keys(target)) {
    if (source[key] !== undefined) {
      target[key] = deepMergeObj(target[key], source[key]);
    }
  }

  return target;
};

export function noopFn(): void {
  // This is a noop function
}

// tslint:disable-next-line:no-any
export function logError(...error: any): void {
  // tslint:disable-next-line:no-console
  console.error(...error);
}
// tslint:disable-next-line:no-any
export function logSuccess(success: any): void {
  // tslint:disable-next-line:no-console
  console.info(success);
}
// tslint:disable-next-line:no-any
export function logInfo(...info: any): void {
  // tslint:disable-next-line:no-console
  console.log(...info);
}
/** USED ON PROLONGATION */
export const hexToRgbA = (hex: string, alpha: string | number) => {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);

  return alpha ? `rgba(${r}, ${g}, ${b}, ${alpha})` : `rgba(${r}, ${g}, ${b})`;
};

export const isMobile = {
  phone: false,
  tablet: false,
  androidPhone: false
};

if (isBrowser && isMobilejs.phone) {
  isMobile.phone = true;
}
if (isBrowser && isMobilejs.tablet) {
  isMobile.tablet = true;
}

if (isBrowser && isMobilejs.android && isMobilejs.android.phone) {
  isMobile.androidPhone = true;
}

export const updatedImageURL = (imageURL: string) => {
  const isSAfariBrowser = checkSafariAndIEBrowser();

  !isSAfariBrowser
    ? (imageURL = `${imageURL}&t=${IMAGE_TYPE.WEBP}`)
    : (imageURL = `${imageURL}&t=${IMAGE_TYPE.PNG}`);

  return imageURL;
};

export const addWebSiteManifest = () => {
  const translation = getGlobalTranslation();
  if (translation && translation.appName) {
    const myDynamicManifest = {
      name: translation.appName,
      short_name: translation.shortName,
      start_url: window.location.origin,
      display: 'standalone',
      orientation: 'portrait',
      theme_color: '#e20074',
      background_color: '#e20074',
      icons: [
        {
          src:
            'https://eshop-oneshop-ui-static.s3.eu-central-1.amazonaws.com/images/Logo_192.png',
          sizes: '192x192',
          type: 'image/png'
        },
        {
          src:
            'https://eshop-oneshop-ui-static.s3.eu-central-1.amazonaws.com/images/Logo_512.png',
          sizes: '512x512',
          type: 'image/png'
        }
      ]
    };
    try {
      const stringManifest = JSON.stringify(myDynamicManifest);
      const blob = new Blob([stringManifest], {
        type: 'application/javascript'
      });
      const manifestURL = URL.createObjectURL(blob);
      if (document && document.querySelector) {
        const querySelector = document.querySelector(
          '#my-manifest-placeholder'
        );
        if (querySelector) {
          querySelector.setAttribute('href', manifestURL);
        }
      }
    } catch (error) {
      logError(error);
    }
  }
};
