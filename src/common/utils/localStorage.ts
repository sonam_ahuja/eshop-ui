import { isBrowser } from '@common/utils';

export const setValueInLocalStorage = (key: string, value: string) => {
  if (isBrowser) {
    localStorage.setItem(key, value);
  }
};

export const getValueFromLocalStorage = (key: string) => {
  return isBrowser && localStorage.getItem(key);
};

export const removeItemFromLocalStorage = (key: string) => {
  return isBrowser && localStorage.removeItem(key);
};
