export const convertToFloat = (num: number) => {
  const parsedNum = parseFloat(num.toString());

  return parsedNum.toFixed(5);
};

export const getUID = () =>
  Math.random()
    .toString(36)
    .substr(2, 9);
