interface IWindow extends Window {
  // tslint:disable-next-line:no-any
  navigator: any;
}

export const getImageQuality = (): number => {
  if (
    (window as IWindow).navigator &&
    (window as IWindow).navigator.connection
  ) {
    const connection =
      (window as IWindow).navigator.connection ||
      (window as IWindow).navigator.mozConnection ||
      (window as IWindow).navigator.webkitConnection;

    if (connection && connection.effectiveType) {
      const networkSpeed = connection.effectiveType.toLocaleLowerCase();
      if (networkSpeed === '4g') {
        return 80;
      } else if (networkSpeed === '3g') {
        return 50;
      } else if (networkSpeed === '2g') {
        return 30;
      }
    }
  }

  return 70;
};
