import history from '@src/client/history';
import { REGEX, VALIDATIONS } from '@src/common/constants/appConstants';
import { SyntheticEvent } from 'react';

export const openTargetLink = (href: string): void => {
  if (history && href) {
    if (
      (href.replace(REGEX.EXTRACT_BASE_URL, '') !== origin &&
        href.indexOf('http://') === 0) ||
      href.indexOf('https://') === 0
    ) {
      window.open(href, '_blank');
    } else if (href.replace(REGEX.EXTRACT_BASE_URL, '') === origin) {
      history.push(href.slice(location.origin.length));
    } else if (href.startsWith('/')) {
      history.push(href);
    } else if (href.match(VALIDATIONS.url)) {
      window.open(`https://${href}`, '_blank');
    }
  }
};

export const getValidTargetLink = (
  href: string,
  event: SyntheticEvent
): boolean => {
  if (history && href) {
    if (
      (href.replace(REGEX.EXTRACT_BASE_URL, '') !== origin &&
        href.indexOf('http://') === 0) ||
      href.indexOf('https://') === 0
    ) {
      return true;
    } else if (href.replace(REGEX.EXTRACT_BASE_URL, '') === origin) {
      history.push(href.slice(location.origin.length));
      event.preventDefault();

      return false;
    } else if (href.startsWith('/')) {
      history.push(href);
      event.preventDefault();

      return false;
    } else if (href.match(VALIDATIONS.url)) {
      return true;
    }
  }

  return true;
};

export const getLink = (href: string): string => {
  if (history && href) {
    if (
      (href.replace(REGEX.EXTRACT_BASE_URL, '') !== origin &&
        href.indexOf('http://') === 0) ||
      href.indexOf('https://') === 0
    ) {
      return href;
    } else if (href.replace(REGEX.EXTRACT_BASE_URL, '') === origin) {
      return href.slice(location.origin.length);
    } else if (href.startsWith('/')) {
      return href;
    } else if (href.match(VALIDATIONS.url)) {
      return `https://${href}`;
    }
  }

  return '#';
};
