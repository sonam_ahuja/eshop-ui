/** USED ON PROLONGATION */
export interface IParsedQueryString {
  [key: string]: string;
}

/** USED ON PROLONGATION */
export const parseQueryString = (queryString: string): IParsedQueryString => {
  const params: IParsedQueryString = {};
  if (queryString.length) {
    const queries = queryString.substring(1).split('&');
    const queryStringLength = queries.length;
    for (let i = 0, l = queryStringLength; i < l; i++) {
      const temp = queries[i].split('=');
      params[temp[0]] = temp[1];
    }
  }

  return params;
};

export const createQueryString = (params: IParsedQueryString) => {
  return Object.keys(params)
    .map(key => `${key}=${params[key]}`)
    .join('&');
};

export const updateQueryParam = (query: string, removeError: boolean) => {
  const decodeURI = decodeURIComponent(query);
  const searchParams = new URLSearchParams(decodeURI);
  let keyExist = false;
  const arrURL = [];
  for (const entry of Object(searchParams).entries()) {
    const key = entry[0];
    const value = entry[1];
    if (key !== 'e') {
      arrURL.push(`${key}=${value}`);
    } else {
      keyExist = true;
    }
  }
  if (!removeError && !keyExist) {
    arrURL.push('e=t');
  }

  return encodeURIComponent(arrURL.join('&'));
};

export const removeQueryParam = (url: string, parameter: string) => {
  const urlparts = url.split('?');
  if (urlparts.length >= 2) {
    const prefix = `${encodeURIComponent(parameter)}=`;
    const pars = urlparts[1].split(/[&;]/g);

    for (let i = pars.length; i-- > 0;) {
      if (pars[i].lastIndexOf(prefix, 0) !== -1) {
        pars.splice(i, 1);
      }
    }

    url = pars.length ? `${urlparts[0]}?${pars.join('&')}` : urlparts[0];

    return url;
  } else {
    return url;
  }
};
