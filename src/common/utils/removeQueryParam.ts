export const removeParamFromQueryString = (
  key: string,
  // tslint:disable-next-line:no-any
  sourceURL: any
): string => {
  let queryParams = [];
  const queryString =
    sourceURL.indexOf('?') !== -1 ? sourceURL.split('?')[1] : '';
  if (queryString !== '') {
    queryParams = queryString.split('&');
    for (let i = queryParams.length - 1; i >= 0; i -= 1) {
      const param = queryParams[i].split('=')[0];
      if (param === key) {
        queryParams.splice(i, 1);
      }
    }

    return `${queryParams.join('&')}`;
  }

  return '';
};
