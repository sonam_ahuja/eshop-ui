import { isBrowser } from '@common/utils';

export const setValueInSessionStorage = (key: string, value: string) => {
  if (isBrowser) {
    sessionStorage.setItem(key, value);
  }
};

export const getValueFromSessionStorage = (key: string) => {
  return isBrowser && sessionStorage.getItem(key);
};

export const removeValueFromSessionStorage = (key: string) => {
  return isBrowser && sessionStorage.removeItem(key);
};
