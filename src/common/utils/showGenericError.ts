import { IError } from '@store/types/common';

export default function showGenricError(
  commonError: IError,
  checkFullpage: boolean
): boolean {
  const { showFullPageError, skipGenericError, httpStatusCode } = commonError;
  if (skipGenericError) {
    return false;
  } else if (checkFullpage) {
    if (httpStatusCode && showFullPageError) {
      return true;
    }

    return false;
  } else if (httpStatusCode) {
    return true;
  }

  return false;
}
