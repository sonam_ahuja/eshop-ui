import { logError } from '@src/common/utils';

export default (template: string, data: object) => {
  try {
    return template.replace(/\{([\w\.]*)\}/g, (_str: string, key: string) => {
      const keys = key.split('.');
      let v = data[keys.shift() as string];

      for (let i = 0, l = keys.length; i < l; i++) {
        v = v[keys[i]];
      }

      return typeof v !== 'undefined' && v !== null ? v : '';
    });
  } catch (error) {
    logError(error);

    return '';
  }
};
