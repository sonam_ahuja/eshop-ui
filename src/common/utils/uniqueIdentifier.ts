import {
  getValueFromLocalStorage,
  setValueInLocalStorage
} from '@utils/localStorage';
import appConstants from '@src/common/constants/appConstants';
import uuidV4 from 'uuid/v4';
import { isBrowser } from '@src/common/utils';

export const createUniqueIdentifier = () => {
  if (!getValueFromLocalStorage(appConstants.UNIQUE_IDENTIFIER)) {
    setValueInLocalStorage(appConstants.UNIQUE_IDENTIFIER, uuidV4());
  }
};

export const getUniqueIdentifier = () => {
  if (isBrowser) {
    return getValueFromLocalStorage(appConstants.UNIQUE_IDENTIFIER);
  }

  return uuidV4();
};

export const setUniqueIdentifier = (uniqueId?: string) => {
  if (!uniqueId) {
    uniqueId = uuidV4();
  }
  setValueInLocalStorage(appConstants.UNIQUE_IDENTIFIER, uniqueId as string);
};
