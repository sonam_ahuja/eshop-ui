import serialize from 'serialize-javascript';

import { IProps } from './types';

export default ({
  secrets,
  data,
  children,
  head,
  style,
  scriptForGTA,
  noScriptForGTA,
  scripts,
  bodyAttrs,
  bootstrapOneApp,
  jsonLdData
}: IProps) => {
  return `<html className="no-js" lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="theme-color" content="#e20074" id="themeColor">
      <meta name="apple-mobile-web-app-status-bar-style" content="#e20074">
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="theme-color" content="#536878">
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
      <link rel='favicon' type='image/png' href='/favicon.png' />
      ${
        bootstrapOneApp
          ? ''
          : '<link rel="manifest" id="my-manifest-placeholder">'
      }
      <link rel="preload" as="font" href="/assets/fonts/fonts.css" />
      <script>
      window.dataLayer = window.dataLayer || [];
      </script>
      <!-- Google Tag Manager -->
      ${scriptForGTA}
     <!-- End Google Tag Manager -->
      ${head}
      <script type='application/ld+json'>${JSON.stringify(jsonLdData)}</script>
      <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
      <link rel="preload" as="script" href="https://static-eshop-oneshop-ui.eshop.yo-digital.com/libdevelopment/js/mqtt-paho.min.js" />
      ${scripts
        .map(script => `<link rel="preload" as="script" href="${script}" />`)
        .join('')}
      <link rel="apple-touch-icon" href='/favicon.png' />
      ${style}
    </head>
    <body ${bodyAttrs}>
    <!-- Google Tag Manager (noscript) -->
    ${noScriptForGTA}
    <!-- End Google Tag Manager (noscript) -->
    <div id="app">${children}</div>
    <script>
      window.__SECRETS__ =  ${serialize(secrets, {
        isJSON: true
      })}
      </script>
      <script>
      window.__INITIAL_STATE__ = ${serialize(data, {
        isJSON: true
      })}</script>
      <link rel="stylesheet" type="text/css" href="/assets/fonts/fonts.css" async />
      <script rel="preload" src="https://static-eshop-oneshop-ui.eshop.yo-digital.com/libdevelopment/js/mqtt-paho.min.js"></script>
      ${scripts.map(script => `<script src="${script}" ></script>`).join('')}
      </body>
  </html>`;
};
