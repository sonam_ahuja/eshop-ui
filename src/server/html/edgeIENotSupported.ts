export default ({
  ieNotSupported,
  cdnBaseUrl
}: // tslint:disable-next-line:no-big-function
{
  cdnBaseUrl: string;
  ieNotSupported: string;
}) => {
  return `<html className="no-js" lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="theme-color" content="#e20074" id="themeColor">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="#e20074">
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
      <link rel="apple-touch-startup-image" href="/Logo_512.png">
      <link rel='favicon' type='image/png' href='/favicon.png?v=3' />
      <link rel="manifest" id="my-manifest-placeholder">
      <link rel="apple-touch-icon" sizes="192x192" href='/Logo_192.png' />
      <link rel="apple-touch-icon" sizes="512x512" href="/Logo_512.png">
    </head>
    <body>
    <link rel="stylesheet" type="text/css" href="${cdnBaseUrl}/assets/fonts/fonts.css" async />
    <style>
    body {
      font-family: 'TeleGrotesk Next',sans-serif,Arial;
      padding: 0;
      margin: 0;
      color: #fff;
    }
    .ie-not-supported {
      position: fixed;
      width: 100%;
      height: 100%;
      background: #000;
      z-index: 200;
    }
    .ie-not-supported-msg {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      font-size: 30px;
      line-height: 50px;
      text-align: center;
      font-weight: 500;
    }
    </style>
      <div class='ie-not-supported'>
          <div class='ie-not-supported-msg'>${ieNotSupported}</div>
      </div>
    </body>
  </html>`;
};
