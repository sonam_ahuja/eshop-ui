import devHtml from './dev';
import prodHtml from './prod';
import pppHtml from './ppp';
import { IProps } from './types';

declare const __DEV__: boolean;

export default (props: IProps) => {
  if (__DEV__) {
    return devHtml(props);
  } else {
    if (props.bootstrapOneApp) {
      return pppHtml(props);
    } else {
      return prodHtml(props);
    }
  }
};
