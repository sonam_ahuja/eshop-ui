import serialize from 'serialize-javascript';

import { IProps } from './types';

export default ({
  secrets,
  data,
  children,
  head,
  style,
  scripts,
  bodyAttrs,
  googleSiteVerification,
  jsonLdData,
  cdnBaseUrl
}: // tslint:disable-next-line:no-big-function
IProps) => {
  return `<html className="no-js" lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="google-site-verification" content="${googleSiteVerification}" />
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="theme-color" content="#e20074" id="themeColor">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="#e20074">
      <meta name="theme-color" content="#536878">
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
      <link rel="apple-touch-startup-image" href="/Logo_512.png">
      <link rel='favicon' type='image/png' href='/favicon.png?v=3' />
      <link rel="apple-touch-icon" sizes="192x192" href='/Logo_192.png' />
      <link rel="apple-touch-icon" sizes="512x512" href="/Logo_512.png">
      <link rel='favicon' type='image/png' href='/favicon.png' />
      ${head}
      <meta name="viewport" content="width=device-width,minimum-scale=1,user-scalable=no">

      <script type='application/ld+json'>${JSON.stringify(jsonLdData)}</script>
      ${scripts
        .map(
          script =>
            `<link rel="preload" as="script" href="${cdnBaseUrl}${script}" />`
        )
        .join('')}
      <style>
      @font-face {
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-bold.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-bold.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-bold.ttf') format('truetype');
        font-weight: bold;
        font-style: normal;
        font-display: block;
      }
      @font-face {
        font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-bolditalic.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-bolditalic.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-bolditalic.ttf') format('truetype');
        font-weight: bold;
        font-style: italic;
      }
      @font-face {
       font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-medium.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-medium.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-medium.ttf') format('truetype');
        font-weight: 500;
        font-style: normal;
      }
      @font-face {
        font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-mediumitalic.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-mediumitalic.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-mediumitalic.ttf') format('truetype');
        font-weight: 500;
        font-style: italic;
      }
      @font-face {
        font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-regular.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-regular.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-regular.ttf') format('truetype');
        font-weight: normal;
        font-style: normal;
      }
      @font-face {
        font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-regularitalic.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-regularitalic.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-regularitalic.ttf') format('truetype');
        font-weight: normal;
        font-style: italic;
      }
      @font-face {
        font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-thin.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-thin.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-thin.ttf') format('truetype');
        font-weight: 100;
        font-style: normal;
      }
      @font-face {
        font-display: block;
        font-family: 'TeleGrotesk Next';
        src: url('${cdnBaseUrl}/assets/fonts/telegrotesknext-ultra.woff2') format('woff2'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-ultra.woff') format('woff'),
          url('${cdnBaseUrl}/assets/fonts/telegrotesknext-ultra.ttf') format('truetype');
        font-weight: 900;
        font-style: normal;
      }
      </style>
      ${style}
    </head>
    <body ${bodyAttrs}>
      <div id="app">${children}</div>
      <script>
      window.__SECRETS__ =  ${serialize(secrets, {
        isJSON: true
      })}
      </script>
      <script>
      window.__INITIAL_STATE__ = ${serialize(data, {
        isJSON: true
      })}</script>
      ${scripts
        .map(script => `<script src="${cdnBaseUrl}${script}" ></script>`)
        .join('')}
      <script>
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register('/sw.js?cdnBaseUrl=${cdnBaseUrl}').then(function(registration) {
              console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
              console.log('ServiceWorker registration failed: ', err);
            });
          });
        } else {
          console.log('service worker not installed');
        }
      </script>
    </body>
  </html>`;
};
