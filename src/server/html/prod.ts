import serialize from 'serialize-javascript';

import { IProps } from './types';

export default ({
  secrets,
  data,
  children,
  head,
  style,
  scriptForGTA,
  noScriptForGTA,
  scripts,
  bodyAttrs,
  jsonLdData,
  googleSiteVerification,
  cdnBaseUrl,
  staticBaseUrl,
  s3ImageUrl
}: // tslint:disable-next-line:no-big-function
IProps) => {
  return `<html className="no-js" lang="en">
    <head>
     <link href="${staticBaseUrl}" rel="dns-prefetch" />
     <link href="${cdnBaseUrl}" rel="dns-prefetch" />
     <link href="${s3ImageUrl}" rel="dns-prefetch" />
      <meta charSet="utf-8" />
      <meta name="google-site-verification" content="${googleSiteVerification}" />
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="theme-color" content="#e20074" id="themeColor">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="#e20074">
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
      <link rel="apple-touch-startup-image" href="/Logo_512.png">
      <link rel='favicon' type='image/png' href='/favicon.png?v=3' />
      <link rel="manifest" id="my-manifest-placeholder">
      <link rel="apple-touch-icon" sizes="192x192" href='/Logo_192.png' />
      <link rel="apple-touch-icon" sizes="512x512" href="/Logo_512.png">
      <script>
      window.dataLayer = window.dataLayer || [];
      </script>
      <!-- Google Tag Manager -->
      ${scriptForGTA}
      <!-- End Google Tag Manager -->
      <link rel='favicon' type='image/png' href='/favicon.png' />
      ${head}
      <meta name="viewport" content="width=device-width,minimum-scale=1,user-scalable=no">
      <link rel="preload" as="script" href="${staticBaseUrl}/js/mqtt-paho.min.js" />

      <script type='application/ld+json'>${JSON.stringify(jsonLdData)}</script>
      ${scripts
        .map(
          script =>
            `<link rel="preload" as="script" href="${cdnBaseUrl}${script}" />`
        )
        .join('')}
      <link rel="preload" as="font" href="${cdnBaseUrl}/assets/fonts/fonts.css" />
      ${style}
    </head>
    <body ${bodyAttrs}>
    <!-- Google Tag Manager (noscript) -->
    ${noScriptForGTA}
    <!-- End Google Tag Manager (noscript) -->
      <div id="app">${children}</div>
      <script>
      window.__SECRETS__ =  ${serialize(secrets, {
        isJSON: true
      })}
      </script>
      <script>
      window.__INITIAL_STATE__ = ${serialize(data, {
        isJSON: true
      })}</script>
      <link rel="stylesheet" type="text/css" href="${cdnBaseUrl}/assets/fonts/fonts.css" async />
      <script src="${staticBaseUrl}/js/mqtt-paho.min.js"></script>

      ${scripts
        .map(script => `<script src="${cdnBaseUrl}${script}" ></script>`)
        .join('')}
      <script>
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register('/sw.js?cdnBaseUrl=${cdnBaseUrl}').then(function(registration) {
              console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
              console.log('ServiceWorker registration failed: ', err);
            });
          });
        } else {
          console.log('service worker not installed');
        }
      </script>
    </body>
  </html>`;
};
