import { IMainState } from '@common/store/reducers/types';
import { IReturnJSONLdType } from '@common/JsonLd/types';
export interface IProps {
  head: string;
  style: string;
  scripts: string[];
  secrets: object;
  data: IMainState | null;
  children: string;
  googleSiteVerification: string;
  scriptForGTA: string;
  noScriptForGTA: string;
  bodyAttrs: string;
  bootstrapOneApp: boolean;
  jsonLdData?: IReturnJSONLdType;
  cdnBaseUrl: string;
  staticBaseUrl: string;
  s3ImageUrl: string;
}
