import ErrorPage from '@common/routes/error';
import store from '@common/store';
import { Provider } from 'react-redux';
import React from 'react';
import express from 'express';
import cookieParser from 'cookie-parser';
import { matchPath, StaticRouter } from 'react-router-dom';
import dotenv from 'dotenv';
import Loadable from 'react-loadable';
import Helmet from 'react-helmet';
import { ThemeProvider } from 'dt-components';
import ReactDOM from 'react-dom/server';
import routes from '@common/routes';
import oneAppRoutes from '@common/oneAppRoutes';
import { all, call, put } from 'redux-saga/effects';
import App from '@common/App';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import OneApp from '@common/OneApp';
import path from 'path';
import appConstants, { ONEAPP_PARAMS } from '@common/constants/appConstants';
import APP_TYPE from '@common/constants/appType';
import { getBundles } from 'react-loadable/webpack';
import mime from 'mime-types';
import { ESHOP_SERVICE_CONSTANT } from '@server/constants/appConstants';
import { IChunk, IError, IRouterContext } from '@server/types';
import isMobilejs from 'ismobilejs';
import { isMobile } from '@common/utils';
import actions from '@common/store/actions/common';
import { getProlongationMarketing } from '@src/common/routes/productList/store/sagas';
import { jsonLdData } from '@common/JsonLd';
import { getStaticContentRequested } from '@src/common/routes/home/store/sagas';
import logger from '@server/middleware/logger';
import { fetchConfiguration } from '@server/services/fetchConfiguration';
import { fetchTranslation } from '@server/services/fetchTranslation';
import { fetchMegaMenu } from '@server/services/fetchMegaMenu';

import getBasicSettings from './middleware/basicSettings';
import getExitHandler from './middleware/exitHandler';
import setBaseURL from './middleware/addBaseURL';
import HealthRoutes from './routes/health';
import getHtml from './html';
import edgeIENotSupportedHTML from './html/edgeIENotSupported';
import { IProps as IHTMLProps } from './html/types';
import chunks from './chunk-manifest.json';
import loadableModulesJson from './react-loadable.json';
import { getIeNotSupportedMessage, setBaseUrlFromQuery } from './utils';

declare const __DEV__: boolean;

dotenv.config();

setBaseURL();
Loadable.preloadAll();

getExitHandler();
const app = express();
app.use(cookieParser());
getBasicSettings(app);
app.use('/health', HealthRoutes);
// ---------------------------------------------------------------------
// Register Node.js middleware
// ---------------------------------------------------------------------
app.use(
  express.static(path.resolve(__dirname, 'public'), {
    maxAge: '30d',
    setHeaders(res, filePath: string): void {
      if (mime.lookup(filePath) === 'text/html') {
        res.setHeader('Cache-Control', 'public, max-age=0');
      } else if (mime.lookup(filePath) === 'font/opentype') {
        res.setHeader('Cache-Control', 'public, max-age=1yr');
      }
    }
  })
);

app.get('/sw.js', (_, res) => {
  res.sendFile(path.resolve(__dirname, 'public/assets/sw.js'));
});

app.get('*', async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
  // tslint:disable-next-line: no-big-function
) => {
  let bootstrapOneApp = false;
  const prolongationAuthorizationToken = req.headers.authorization
    ? req.headers.authorization
    : null;

  if (
    String(req.query[ONEAPP_PARAMS.CHANNEL]).toLowerCase() === APP_TYPE.ONEAPP
  ) {
    bootstrapOneApp = true;
  }
  try {
    isMobile.phone = isMobilejs(req.headers['user-agent']).phone;
    isMobile.tablet = isMobilejs(req.headers['user-agent']).tablet;
    const sagas: Generator[] = [];

    (bootstrapOneApp ? oneAppRoutes : routes).forEach(route => {
      const match = matchPath(req.url, route);
      if (match && route && route.loadData) {
        sagas.push(route.loadData());
      }
    });

    setBaseUrlFromQuery(req, res);

    logger.info(
      `
      Eshop running for language ${appConstants.LANGUAGE_CODE},
      country ${appConstants.COUNTRY_CODE},
      and CMS base url for translation is
      ${appConstants.TRANSLATION_BASE_URL}
      and configuration is
      ${appConstants.CONFIGURATION_BASE_URL} `
    );

    const msisdnValue =
      req.headers['x-msisdn'] ||
      req.headers.x_msisdn ||
      req.headers['X-MSISDN'];

    await store.runSaga(function*(): Generator {
      yield all([
        call(fetchConfiguration, {
          type: '',
          payload: { oneApp: bootstrapOneApp }
        }),
        call(fetchTranslation, {
          type: '',
          payload: { oneApp: bootstrapOneApp }
        }),
        call(fetchMegaMenu, {
          type: '',
          payload: { oneApp: bootstrapOneApp }
        }),
        bootstrapOneApp
          ? call(getProlongationMarketing, {
              type: '',
              payload: {
                oneApp: bootstrapOneApp,
                callbackFn: logger.info.bind(logger),
                languageParam: req.query.language
              }
            })
          : [],
        sagas,
        bootstrapOneApp
          ? yield put(
              actions.setProlongationAuthorizationToken(
                prolongationAuthorizationToken
              )
            )
          : [],
        call(getStaticContentRequested, {
          type: '',
          payload: { oneApp: bootstrapOneApp }
        }),
        yield put(actions.setMsisdnHeader(msisdnValue as string))
      ]);
    }).done;
    const scripts = new Set();
    const htmlData: IHTMLProps = {
      head: '',
      style: '',
      scripts: [],
      children: '',
      secrets: {},
      googleSiteVerification: '',
      data: null,
      bodyAttrs: '',
      scriptForGTA: '',
      noScriptForGTA: '',
      bootstrapOneApp,
      jsonLdData: await jsonLdData(req.url, req.path),
      cdnBaseUrl: process.env.ESHOP_CDN_BASE_URL
        ? process.env.ESHOP_CDN_BASE_URL
        : '',
      staticBaseUrl: process.env.ESHOP_CDN_STATIC_URL
        ? process.env.ESHOP_CDN_STATIC_URL
        : '',
      s3ImageUrl: process.env.S3_IMAGE_URL ? process.env.S3_IMAGE_URL : ''
    };
    htmlData.data = store.getState();
    if (
      !bootstrapOneApp &&
      req.useragent &&
      (req.useragent.isIE || req.useragent.isEdge)
    ) {
      const ieHtml = edgeIENotSupportedHTML({
        cdnBaseUrl: process.env.ESHOP_CDN_BASE_URL
          ? process.env.ESHOP_CDN_BASE_URL
          : '',
        ieNotSupported: getIeNotSupportedMessage(htmlData.data)
      });
      res.status(200);
      res.send(`<!doctype html>${ieHtml}`);

      return;
    }

    const context: IRouterContext = {};
    const sheet = new ServerStyleSheet();
    const modules: string[] = [];
    const addChunk = (chunkName: string) => {
      if ((chunks as IChunk)[chunkName]) {
        chunks[chunkName].forEach((asset: string) => scripts.add(asset));
      } else if (__DEV__) {
        throw new Error(`Chunk with name '${chunkName}' cannot be found`);
      }
    };
    const getModules = (moduleName: string) => {
      return modules.push(moduleName);
    };
    htmlData.children = ReactDOM.renderToString(
      <Provider store={store}>
        <Loadable.Capture report={getModules}>
          <StyleSheetManager sheet={sheet.instance}>
            <StaticRouter location={req.url} context={context}>
              <ThemeProvider theme={{}}>
                {bootstrapOneApp ? <OneApp /> : <App />}
              </ThemeProvider>
            </StaticRouter>
          </StyleSheetManager>
        </Loadable.Capture>
      </Provider>
    );

    if (context.status === 301 || context.status === 302) {
      return res.redirect(context.status, context.url as string);
    }

    htmlData.head = `
    ${Helmet.renderStatic().title.toString()}
    ${Helmet.renderStatic().meta.toString()}
    `;

    htmlData.bodyAttrs = Helmet.renderStatic().bodyAttributes.toString();

    htmlData.style = sheet.getStyleTags();

    logger.info(
      `deployed for language
      ${appConstants.LANGUAGE_CODE}
      and country
      ${appConstants.COUNTRY_CODE}
      with translation key
      ${appConstants.TRANSLATION_BASE_URL} `
    );

    htmlData.secrets = {
      ESHOP_BASE_URL: appConstants.ESHOP_BASE_URL,
      COUNTRY_CODE: appConstants.COUNTRY_CODE,
      LANGUAGE_CODE: appConstants.LANGUAGE_CODE,
      CONTENT_LANGUAGE: appConstants.CONTENT_LANGUAGE,
      CHANNEL: appConstants.CHANNEL,
      ENV: process.env.DTNODE_ENV,
      RUM_SERVER_URL: process.env.RUM_SERVER_URL,
      RUM_SERVER_NAME: process.env.RUM_SERVER_NAME,
      RUM_VERSION: process.env.RUM_VERSION,
      WEB_VIEW_CMS_BASE_URL: process.env.WEB_VIEW_CMS_BASE_URL,
      WEB_VIEW_TRANSLATION_API_KEY: process.env.WEB_VIEW_TRANSLATION_API_KEY,
      WEB_VIEW_CONFIGURATION_API_KEY:
        process.env.WEB_VIEW_CONFIGURATION_API_KEY,
      ONEAPP_BASE_URL: process.env.ONEAPP_BASE_URL,
      MOENGAGE_BASE_URL: process.env.MOENGAGE_BASE_URL,
      S3_IMAGE_URL: process.env.S3_IMAGE_URL,
      CLIENT_ID: process.env.CLIENT_ID,
      SECRET_KEY: process.env.SECRET_KEY,
      WEB_VIEW_MARKETING_URL: process.env.WEB_VIEW_MARKETING_URL,
      ESHOP_CDN_BASE_URL: process.env.ESHOP_CDN_BASE_URL
        ? process.env.ESHOP_CDN_BASE_URL
        : '',
      ESHOP_CDN_STATIC_URL: process.env.ESHOP_CDN_STATIC_URL
    };

    logger.info('updated html secrets');

    logger.info(`Process ${process.env.DTNODE_ENV}`);

    // tslint:disable-next-line:no-any
    const bundles = getBundles(loadableModulesJson as any, modules);

    logger.info(`Bundles created`);

    bundles.forEach(bundle => {
      scripts.add(bundle.publicPath);
    });
    addChunk('client');

    htmlData.scripts = [...(Array.from(scripts) as string[])];

    htmlData.scriptForGTA =
      htmlData.data && htmlData.data.configuration
        ? htmlData.data.configuration.cms_configuration.global.scriptForGTA
        : '';
    htmlData.noScriptForGTA =
      htmlData.data && htmlData.data.configuration
        ? htmlData.data.configuration.cms_configuration.global.noScriptForGTA
        : '';

    htmlData.googleSiteVerification =
      htmlData.data && htmlData.data.configuration
        ? htmlData.data.configuration.cms_configuration.global
            .googleSiteVerification
        : '';

    const html = getHtml(htmlData);

    res.status(context.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});
// ---------------------------------------------------------------------
// Error handling
// ---------------------------------------------------------------------

app.use(
  (
    err: IError,
    _req: express.Request,
    res: express.Response,
    _next: express.NextFunction
  ) => {
    let bootstrapOneApp = false;
    if (
      String(_req.query[ONEAPP_PARAMS.CHANNEL]).toLowerCase() ===
      APP_TYPE.ONEAPP
    ) {
      bootstrapOneApp = true;
    }
    logger.error(err);

    const htmlData: IHTMLProps = {
      head: `
    <title>${err.message}</title>
    <meta name="description">Error</meta>
  `,
      style: '',
      scripts: [],
      secrets: {},
      data: null,
      noScriptForGTA: '',
      scriptForGTA: '',
      // bodyAttributes: '',
      googleSiteVerification: '',
      bodyAttrs: '',
      children: ReactDOM.renderToString(<ErrorPage error={err} />),
      bootstrapOneApp,
      cdnBaseUrl: process.env.ESHOP_CDN_BASE_URL
        ? process.env.ESHOP_CDN_BASE_URL
        : '',
      staticBaseUrl: process.env.ESHOP_CDN_STATIC_URL
        ? process.env.ESHOP_CDN_STATIC_URL
        : '',
      s3ImageUrl: process.env.S3_IMAGE_URL ? process.env.S3_IMAGE_URL : ''
    };
    const html = getHtml(htmlData);
    res.status(err.status || 500);
    res.send(`<!doctype html>${html}`);
  }
);
// ---------------------------------------------------------------------
// Launch the server
// ---------------------------------------------------------------------
if (!module.hot) {
  // tslint:disable-next-line:no-any
  const port = (process.env as any).PORT;
  Loadable.preloadAll()
    .then(() => {
      app.listen(port, () => {
        const runMessage = `The server is running at http://localhost:${port}/`;
        // tslint:disable-next-line: no-console
        console.log(runMessage);
        logger.info(runMessage);
      });
    })
    .catch(error => {
      logger.error(error);
    });
}
// ---------------------------------------------------------------------
// Hot Module Replacement
// ---------------------------------------------------------------------
if (module.hot) {
  // tslint:disable-next-line:no-string-literal
  app['hot'] = module.hot;
  module.hot.accept('./index', () => {
    logger.info('hot reloading...');
  });
}

// tslint:disable-next-line:max-file-line-count
export default app;
