import appConstants from '@common/constants/appConstants';

export default (
  langugageCode: string = `${process.env.LANGUAGE_CODE}`,
  countryCode: string = `${process.env.COUNTRY_CODE}`,
  translationKey: string = `${process.env.TRANSLATION_API_KEY}`,
  configurationKey: string = `${process.env.CONFIGURATION_API_KEY}`,
  contentLanguage: string = `${process.env.CONTENT_LANGUAGE}`,
  channel: string = `${process.env.CHANNEL}`
) => {
  const webViewTranslationKey = `${process.env.WEB_VIEW_TRANSLATION_API_KEY}`;
  const webViewConfigurationKey = `${
    process.env.WEB_VIEW_CONFIGURATION_API_KEY
  }`;

  appConstants.TRANSLATION_BASE_URL = `${
    process.env.CMS_BASE_URL
  }/${translationKey}/${langugageCode}`;
  appConstants.CONFIGURATION_BASE_URL = `${
    process.env.CMS_BASE_URL
  }/${configurationKey}`;
  appConstants.CONFIGURATION_API_KEY = configurationKey;
  appConstants.ESHOP_BASE_URL = `${process.env.ESHOP_BASE_URL}`;
  appConstants.COUNTRY_CODE = countryCode;
  appConstants.LANGUAGE_CODE = langugageCode;
  appConstants.CONTENT_LANGUAGE = contentLanguage;
  appConstants.CHANNEL = channel;
  appConstants.RUM_SERVER_URL = process.env.RUM_SERVER_URL || '';
  appConstants.RUM_SERVER_NAME = process.env.RUM_SERVER_NAME || '';
  appConstants.RUM_VERSION = process.env.RUM_VERSION || '';

  appConstants.WEB_VIEW_TRANSLATION_BASE_URL = `${
    process.env.WEB_VIEW_CMS_BASE_URL
  }/${webViewTranslationKey}/${langugageCode}`;
  appConstants.WEB_VIEW_CONFIGURATION_BASE_URL = `${
    process.env.WEB_VIEW_CMS_BASE_URL
  }/${webViewConfigurationKey}`;

  appConstants.ONEAPP_BASE_URL = process.env.ONEAPP_BASE_URL || '';
  appConstants.MOENGAGE_BASE_URL = process.env.MOENGAGE_BASE_URL || '';
  appConstants.CLIENT_ID = process.env.CLIENT_ID || '';
  appConstants.SECRET_KEY = process.env.SECRET_KEY || '';
  appConstants.MEGA_MENU_CMS_BASE_URL =
    process.env.MEGA_MENU_CMS_BASE_URL || '';
  appConstants.MEGA_MENU_HEADER_KEY = process.env.MEGA_MENU_HEADER_KEY || '';
  appConstants.MEGA_MENU_FOOTER_KEY = process.env.MEGA_MENU_FOOTER_KEY || '';
  appConstants.WEB_VIEW_MARKETING_URL =
    process.env.WEB_VIEW_MARKETING_URL || '';
  appConstants.PROLONGATION_INSTANCE_KEY =
    process.env.PROLONGATION_INSTANCE_KEY || '';
  appConstants.ESHOP_CDN_BASE_URL = process.env.ESHOP_CDN_BASE_URL || '';
};
