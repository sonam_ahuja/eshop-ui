import bunyan, { LoggerOptions } from 'bunyan';
import log from 'express-bunyan-logger';
import express from 'express';

export interface ICustomBunyan extends bunyan {
  getRequestLogger: express.RequestHandler;
  getBasicLogger(name: string): void;
}

const config = {
  name: 'eshop-oneshop-ui',
  country: process.env.COUNTRY_CODE,
  language: process.env.LANGUAGE_CODE,
  streams: [
    {
      level: 'info',
      path: `logs/eshop-oneshop-ui-${process.env.LOG_ENV}-out-app.log`
    },
    {
      level: 'error',
      path: `logs/eshop-oneshop-ui-${process.env.LOG_ENV}-error-app.log`
    }
  ]
};
const logger = bunyan.createLogger(config as LoggerOptions) as ICustomBunyan;

logger.getBasicLogger = (
  name = 'init',
  country = process.env.COUNTRY_CODE,
  language = process.env.LANGUAGE_CODE
) => {
  return bunyan.createLogger({ name, country, language });
};

logger.getRequestLogger = log({
  excludes: ['user-agent', 'res-headers'],
  obfuscate: ['body.password', 'body.confirmPassword']
});

export default logger;
