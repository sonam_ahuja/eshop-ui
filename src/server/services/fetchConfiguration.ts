import { put } from 'redux-saga/effects';
import apiCaller from '@common/utils/apiCaller';
import { apiEndpoints } from '@common/constants';
import CONSTANTS from '@common/constants/appConstants';
import { commonAction, configurationAction as actions } from '@store/actions';
import logger from '@server/middleware/logger';

export function* fetchConfiguration(action: {
  type: string;
  payload: { oneApp: boolean };
}): Generator {
  try {
    const {
      payload: { oneApp }
    } = action;
    const baseURL = oneApp
      ? CONSTANTS.WEB_VIEW_CONFIGURATION_BASE_URL
      : CONSTANTS.CONFIGURATION_BASE_URL;

    logger.info(
      `configuration base URL ${baseURL}${
        apiEndpoints.CONFIG.GET_CMS_CONFIGURATION.url
      }`
    );

    const response = yield apiCaller.get(
      `${baseURL}${apiEndpoints.CONFIG.GET_CMS_CONFIGURATION.url}`,
      { withBaseUrl: false, withCredentials: false }
    );
    const data = response;
    yield put(actions.setCMSConfiguration(data));
    logger.info(`configuration response ${JSON.stringify(response)}`);
    try {
      const currencySymbol = data.global.currency.currencySymbol;
      if (currencySymbol) {
        yield put(commonAction.setCurrency(currencySymbol));
      }
    } catch (error) {
      logger.error(`set currecny from CMS to common state ${error}`);
    }
  } catch (error) {
    logger.error(`error in configuration ${error}`);
  }
}
