import { put } from 'redux-saga/effects';
import { apiEndpoints } from '@common/constants';
import APP_CONFIG from '@common/constants/appConstants';
import apiCaller from '@common/utils/apiCaller';
import { commonAction as actions } from '@store/actions';
import logger from '@server/middleware/logger';

export function* fetchMegaMenu(action: {
  type: string;
  payload: { oneApp: boolean };
}): Generator {
  try {
    if (!action.payload.oneApp) {
      const baseURL = APP_CONFIG.MEGA_MENU_CMS_BASE_URL;
      const applanguage = APP_CONFIG.LANGUAGE_CODE;
      const cmsHeaderApiKey = APP_CONFIG.MEGA_MENU_HEADER_KEY;
      const cmsFooterApiKey = APP_CONFIG.MEGA_MENU_FOOTER_KEY;

      logger.info(
        `Mega menu URL ${baseURL}/${apiEndpoints.CONFIG.GET_MEGA_MENU.url(
          cmsHeaderApiKey,
          applanguage
        )}`
      );

      const headerResponse = yield apiCaller.get(
        `${baseURL}/${apiEndpoints.CONFIG.GET_MEGA_MENU.url(
          cmsHeaderApiKey,
          applanguage
        )}`,
        { withBaseUrl: false, withCredentials: false }
      );

      if (headerResponse && headerResponse.categories) {
        yield put(actions.setMegaMenu(headerResponse));
      }

      logger.info(`header response ${JSON.stringify(headerResponse)}`);

      const footerResponse = yield apiCaller.get(
        `${baseURL}/${apiEndpoints.CONFIG.GET_MEGA_MENU.url(
          cmsFooterApiKey,
          applanguage
        )}`,
        { withBaseUrl: false, withCredentials: false }
      );

      if (footerResponse && footerResponse.categories) {
        yield put(actions.setMegaMenuFooter(footerResponse));
      }
      logger.info(`footer response ${JSON.stringify(footerResponse)}`);
    }
  } catch (error) {
    logger.error(`error in mega menu ${error}`);
  }
}
