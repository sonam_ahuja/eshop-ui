import { put } from 'redux-saga/effects';
import apiCaller from '@common/utils/apiCaller';
import CONSTANTS from '@common/constants/appConstants';
import { apiEndpoints } from '@common/constants';
import { englishTranslationAction, translationAction } from '@store/actions';
import logger from '@server/middleware/logger';

export function* fetchTranslation(action: {
  type: string;
  payload: { oneApp: boolean };
}): Generator {
  try {
    const {
      payload: { oneApp }
    } = action;
    const baseURL = oneApp
      ? CONSTANTS.WEB_VIEW_TRANSLATION_BASE_URL
      : CONSTANTS.TRANSLATION_BASE_URL;
    // @ url for testing
    // 'https://eshop-cms-ui-stage.eshop.yo-digital.com/cdn/ONmYZUYiGdfBtYy4l1ejqUHZg2VwIpwN/pl';
    logger.info(
      `translation base url ${baseURL}${
        apiEndpoints.CONFIG.GET_CMS_TRANSLATION.url
      }`
    );

    const response = yield apiCaller.get(
      `${baseURL}${apiEndpoints.CONFIG.GET_CMS_TRANSLATION.url}`,
      { withBaseUrl: false, withCredentials: false }
    );
    const data = response;
    yield put(translationAction.setCMSTranslation(data));
    logger.info(`translation response ${JSON.stringify(response)}`);

    if (!oneApp) {
      const engBaseUrl = `${baseURL.substring(0, baseURL.length - 2)}en`;
      const engResponse = yield apiCaller.get(
        `${engBaseUrl}${apiEndpoints.CONFIG.GET_CMS_TRANSLATION.url}`,
        { withBaseUrl: false, withCredentials: false }
      );
      yield put(englishTranslationAction.setEnglishCMSTranslation(engResponse));
    }
  } catch (error) {
    logger.error(`error in translation ${error}`);
  }
}
