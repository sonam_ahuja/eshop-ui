import { IMainState } from '@src/common/store/reducers/types';
import { ESHOP_SERVICE_CONSTANT } from '@server/constants/appConstants';
import appConstants from '@common/constants/appConstants';
import express from 'express';
import setBaseURL from '@server/middleware/addBaseURL';

export const getIeNotSupportedMessage = (state: IMainState | null) => {
  if (state && state.translation.cart.global.ieNotSupported) {
    return state.translation.cart.global.ieNotSupported;
  } else {
    // fallback of fallback!
    return 'We are currently not live on Internet Explorer yet! Hang on tight, we will be available soon!';
  }
};

export const setBaseUrlFromQuery = (
  req: express.Request,
  res: express.Response
) => {
  if (
    (process.env.NODE_ENV === ESHOP_SERVICE_CONSTANT.DEVELOPMENT ||
      process.env.NODE_ENV === ESHOP_SERVICE_CONSTANT.STAGE ||
      __DEV__) &&
    req &&
    res
  ) {
    if (
      req.query &&
      (req.query.language ||
        req.query.country ||
        req.query.translationKey ||
        req.query.configurationKey ||
        req.query.contentLanguage ||
        req.query.channel)
    ) {
      if (req.query.language) {
        res.cookie(appConstants.LANGUAGE, req.query.language);
      }
      if (req.query.country) {
        res.cookie(appConstants.COUNTRY, req.query.country);
      }

      setBaseURL(
        req.query.language,
        req.query.country,
        req.query.translationKey,
        req.query.configurationKey,
        req.query.contentLanguage,
        req.query.channel
      );
    } else if (
      req.cookies &&
      (req.cookies[appConstants.LANGUAGE] || req.cookies[appConstants.COUNTRY])
    ) {
      setBaseURL(
        req.cookies[appConstants.LANGUAGE],
        req.cookies[appConstants.COUNTRY]
      );
    } else {
      setBaseURL();
    }
  }
};
