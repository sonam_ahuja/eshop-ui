/*eslint-disable */
const registrationUrl = new URL(location.href);
const cdnBaseUrlFromQueryParam = registrationUrl.searchParams.get('cdnBaseUrl');
let cdnDomain;
if (cdnBaseUrlFromQueryParam) {
  cdnDomain = cdnBaseUrlFromQueryParam;
} else {
  cdnDomain = location.origin;
}

self.__precacheManifest = [].concat(self.__precacheManifest || []);

workbox.core.setCacheNameDetails({
  prefix: 'e-shop',
  precache: 'install-time',
  suffix: 'v1',
  runtime: 'run-time',
  googleAnalytics: 'ga'
});

// active new service worker as long as it's installed
workbox.clientsClaim();
workbox.skipWaiting();

// suppress warnings if revision is not provided
workbox.precaching.suppressWarnings();

// precahce and route asserts built by webpack
workbox.precaching.precacheAndRoute(
  self.__precacheManifest.map(routeItem => {
    return {
      ...routeItem,
      url: `${cdnDomain}${routeItem.url}`
    };
  }),
  {}
);

// // return app shell for all navigation requests
// workbox.routing.registerNavigationRoute('/app-shell');

// routing for cloud served images
workbox.routing.registerRoute(
  /^(https:\/\/cdn\.eshop-image-resize\.yo-digital\.com\/(.*))|(https:\/\/.+\.(jpe?g|png|gif|webp|svg|woff2|ico)$)/i,
  workbox.strategies.cacheFirst({
    cacheName: 'e-shop-image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        maxAgeSeconds: 7 * 24 * 60 * 60,
        maxEntries: 20
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  })
);

/*eslint-enable */
